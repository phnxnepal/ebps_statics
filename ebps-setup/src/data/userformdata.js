export const userdata={
    us_data:{
        heading:{
            heading1:'add user'
        },
        formdata:{
            formdata1: 'नाम , थर ',
            formdata2: 'ठेगाना',
            formdata3: 'फोन',
            formdata4: 'मोबाईल ',
            formdata5: 'इमेल ',
            formdata6: 'पासवर्ड ',
            formdata7: 'पिता, पति, पत्नी को नाम ',
            formdata8: 'नागरिकता प्रमाण पत्र न. ',
            formdata9: 'जारी  जिल्ला',
            formdata10: 'प्रयोगकर्ताको प्रकार ',
            formdata11: 'जारी मिति ' ,

        },
        tableheads:{
            head1:' मोबाईल नंबर',
            head2:' इमेल ',
            head3:' पूरा नाम',
            head4:' ठेगाना',
            head5:' नागरिकता नंबर',
            head6:' जारी जिल्ला',
            head7:' जारी मिति',
            head8:' पिता/पति',
            head9:' फाेन',
            head10:' प्रयाेगकर्ताकाे किसिम',
            head11:'प्रयाेगकर्ताकाे किसिम छान्नुहोस',
        },
        sel:{
            val1:'राजस्व ',
            val2:'सव इन्न्जिनियर ',
            val3:'इन्न्जिनियर',
            val4:'प्रमुख प्रशसकीय अधिकृत ',
            
        }
            

    },
}