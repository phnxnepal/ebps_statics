import React from 'react';
// import logo from './logo.svg';
import './assets/scss/app.scss';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import AddUser from './components/setup/AddUser';
import HeaderComponent from './components/HeaderComponent';
import Footer from './components/Footer';
import BuildingClass from './components/setup/BuildingClass';
import Designer from './components/setup/Designer';
import Downloads from './components/setup/Downloads';
  
class App extends React.Component {
  render() {
    return (
      <Router>
        <HeaderComponent />
        <main className="site-main">
          <Switch>
            <Route path="/add-user" component={AddUser} />
            <Route path="/building-class" component={BuildingClass} />
            <Route path="/designer" component={Designer} />
            <Route path="/downloads" component={Downloads} />
          </Switch>
        </main>
        <Footer />
      </Router>
    );
  }
}

export default App;
