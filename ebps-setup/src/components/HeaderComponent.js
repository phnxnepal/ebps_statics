import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Dropdown, Header, Icon } from 'semantic-ui-react';
// import { logoutUser } from '../../store/actions/loginForm';
// import Logo from './../../assets/images/phoenix_logo.png';

import { connect } from 'react-redux';
// import { getUserInfo } from '../../store/actions/SurjaminMuchulkaAction';
// import { getUserInfoObj, getUserTypeValue } from '../../utils/functionUtils';

class HeaderComponent extends Component {
  //@ts-ignore
  constructor(props) {
    super(props);
    this.state = { activeItem: '', username: '' };
  }

  componentDidMount() {
    // this.setState(() => ({
    //@ts-ignore
    //   username: localStorage.getItem('user')
    // }))
  }

  componentDidUpdate() {
    //@ts-ignore
    // if (!this.state.username) {
    // this.setState(() => ({ username: localStorage.getItem('username') }));
    // }
  }

  //@ts-ignore
  handleItemClick = e => {
    e.preventDefault();
    const id = e.target.id;

    this.setState(() => ({ activeItem: id }));
    // console.log("heaer id", e.target);

    //@ts-ignore
    // this.props.history.push(`/user/${id}`);
  };

  handleLogout = () => {
    // //@ts-ignore
    // this.props.logoutUser();
    // //@ts-ignore
    // this.props.history.push('/');
  };
  render() {
    //@ts-ignore
    const { activeItem } = this.state;

    // console.log("userinfo", getUserInfoObj());

    //@ts-ignore
    // const { userName: username, userType } = getUserInfoObj() || { username: '', userType: '' }

    //@ts-ignore
    // const { login: { username } } = this.props;

    return (
      <React.Fragment>
        <Menu inverted className="ebpsMain-header fixed">
          <Menu.Item>
            <Header style={{ color: 'white' }}>
              <Link to="/user">
                <span className="comp-logo">
                  {/* <img src={Logo} alt="phoenix-logo"/> */}
                </span>{' '}
                EBPS
              </Link>
            </Header>
          </Menu.Item>
          <Menu.Menu position="right">
            <Menu.Item
              name="Processing List"
              id="processing-list"
              active={activeItem === 'processing-list'}
              onClick={this.handleItemClick}
              // component={NavLink}
              // to={'/login'}
            />
            <Menu.Item
              name="Completed List"
              active={activeItem === 'Completed List'}
              onClick={this.handleItemClick}
            />
            <Dropdown compact item text="Setup">
              <Dropdown.Menu>
                <Dropdown.Item icon="sign-out" text="Add User" />
                <Dropdown.Item icon="sign-out" text="Building Class" />
                <Dropdown.Item icon="sign-out" text="Designer" />
                <Dropdown.Item icon="sign-out" text="Downloads" />
                <Dropdown.Item icon="sign-out" text="Floor Rate" />
                <Dropdown.Item icon="sign-out" text="Add Fiscal Year" />
              </Dropdown.Menu>
            </Dropdown>
            <Menu.Item
              name="Contact List"
              active={activeItem === 'Contact List'}
              onClick={this.handleItemClick}
            />
            <Dropdown compact item text="Profile">
              <Dropdown.Menu>
                {/* <Dropdown.Item><span id="profile" onClick={this.handleItemClick}><Icon name="user" />{username} - {getUserTypeValue(userType)} </span></Dropdown.Item> */}
                <Dropdown.Item
                  icon="sign-out"
                  text="Logout"
                  onClick={this.handleLogout}
                />
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Menu>
        </Menu>
      </React.Fragment>
    );
  }
}

// //@ts-ignore
// const mapStateToProps = state => {

//   return {
//   login: state.root.login
// }};

// const mapDispatchToProps = { logoutUser };

export default // connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(
HeaderComponent;
// );
