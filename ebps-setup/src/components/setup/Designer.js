import React, { Component } from 'react';
import { Formik } from "formik";
import { designer } from '../../data/designerformdata';
import { Form, Button, Table } from 'semantic-ui-react';
const colors = [
    'red'
]
const des = designer.designer_data;
class Designer extends Component {
    render() {
        return (
            <div>
                <Formik
                    onSubmit={(values, { setSubmitting }) => {
                        console.log(values);
                        setSubmitting(false);
                    }}
                    render={({ handleChange, values, handleSubmit }) => (
                        <div>
                            <Form onChange={handleSubmit} className="NJ-right">
                                <div>
                                    <span>{des.header.header_1}</span>
                                </div>
                                <br />
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label>{des.data_1.data_1_1}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_1.data_1_2}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_1.data_1_3}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_1.data_1_4}</label>
                                        <input></input>
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label>{des.data_2.data_2_1}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_2.data_2_2}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_2.data_2_3}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_2.data_2_4}</label>
                                        <input></input>
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label>{des.data_3.data_3_1}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_3.data_3_2}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_3.data_3_3}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_3.data_3_4}</label>
                                        <input></input>
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label>{des.data_4.data_4_1}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_4.data_4_2}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_4.data_4_3}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_4.data_4_4}</label>
                                        <input></input>
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label>{des.data_5.data_5_1}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_5.data_5_2}</label>
                                        <input></input>
                                    </Form.Field>
                                    <Form.Field>
                                        <label>{des.data_5.data_5_3}</label>
                                        <input></input>
                                    </Form.Field>
                                </Form.Group>
                                <Button floated='right' color='red'>Save</Button>
                            </ Form>
                        </div>
                    )}
                />
                <br />
                <br />
                <br />
                <div>
                    <div>
                        <span >{des.second_header.second_header_1}</span>
                    </div>
                    {colors.map((color) => (
                        <Table color={color} key={color} inverted>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>   <span >{des.table.table_1}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_2}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_3}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_4}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_5}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_6}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_7}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_8}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_9}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_10}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_11}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_12}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_13}</span></Table.HeaderCell>
                                    <Table.HeaderCell>   <span >{des.table.table_14}</span></Table.HeaderCell>
                                </Table.Row>

                            </Table.Header>

                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                    <Table.Cell></Table.Cell>
                                </Table.Row>
                            </Table.Body>
                        </Table>
                    ))}
                </div>
            </div>
        );
    }
}
export default Designer
