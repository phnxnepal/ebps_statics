import React, { Component } from 'react'
import { Formik } from 'formik';
import { Form, Button, Divider, Segment, Table, Header, Select } from 'semantic-ui-react';
import { userdata } from '../../data/userformdata';

const colors = [
	'grey']
const us = userdata.us_data;
const opt = [
	{ key: 'a', value: '1', text: 'राजस्व' }, 
	{ key: 'b', value: '2', text: 'सव इन्न्जिनियर' }, 
	{ key: 'c', value: '3', text: 'इन्न्जिनियर' }, 
	{ key: 'd', value: '4', text: 'प्रमुख प्रशसकीय अधिकृत' }
];

export class AddUser extends Component {
	render() {
		return (
			<React.Fragment>
				 <Formik
                onSubmit={(values, { setSubmitting }) => {
                    console.log(values);
                    setSubmitting(false);
                }}
                render={({ handleChange, values, handleSubmit }) => (
                    <Form onSubmit={handleSubmit} className="NJ-right">
					<div style={{ backgroundColor: '#b84d64' }}>
						<label color='White'>
							<center>
								<h2>Add user</h2>
							</center>
						</label>
					</div>
					<div id="ui container">
						<Segment>
							<Form.Group widths='equal'>
								<Form.Field required>
									<label>{us.formdata.formdata1}</label>
									<input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata2}</label>
									<input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata3}</label>
									<input placeholder='' />
								</Form.Field>
							</Form.Group>
							<Form.Group widths='equal'>
								<Form.Field required>
									<label>{us.formdata.formdata4}</label>
									<input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata5}</label>
									<input placeholder='' foc />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata6}</label>
								<input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata7}</label>
									<input placeholder='' />
								</Form.Field>
							</Form.Group>
							<Form.Group widths='equal'>
								<Form.Field required>
									<label>{us.formdata.formdata8}</label><input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata9}</label><input placeholder='' />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata10}</label><Select placeholder={us.tableheads.head11} options={opt} />
								</Form.Field>
								<Form.Field required>
									<label>{us.formdata.formdata11}</label><input placeholder='' />
								</Form.Field>
							</Form.Group>
							<Button type='submit'>Submit</Button>
						</Segment>
					</div>
				</Form>
				  )}
				  />
				<Divider />
				<div style={{ backgroundColor: '#b84d64' }}><label><center><h2>Sign up table </h2></center></label></div>
				{colors.map((color) => (
				<Table celled padded  color={color} key={color} inverted>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>{us.tableheads.head1}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head2}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head3}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head4}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head5}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head6}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head7}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head8}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head9}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head10}</Table.HeaderCell>
							<Table.HeaderCell>{us.tableheads.head10}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> A </Header> </Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
							<Table.Cell > <Header as='h5' textAlign='center'> B </Header> </Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>))}
			</React.Fragment>
			);
		}
}
	
	
export default AddUser;
