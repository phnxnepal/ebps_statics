import React from "react";

const Footer = () => {
  return (
    <React.Fragment>
      <footer className="site-footer">
        &copy; 2019 EBPS
      </footer>
    </React.Fragment>
  );
};

export default Footer