import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPermitAndUserData, getMultipleFormDataByUrl, postFormDataByUrl, postFormDataWithFileByUrl } from '../../store/actions/formActions';

import { setPrint } from '../../store/actions/general';
import FallbackComponent from '../../components/shared/FallbackComponent';
// import ApprovalStatusDisplay from '../../components/shared/ApprovalStatusDisplay';
// import HistorySidebar from '../../components/shared/HistoryComponent';
import {
	// hasPermission,
	isEmpty,
	// getPreviousFormStatus,
	// makeprintformat,
	// makespecialprintformat
} from '../../utils/functionUtils';
// import ApprovalGroup from '../../components/shared/ApprovalGroup';
import FormWrapper from '../../components/shared/FormWrapper';
// import { ENTER_BY, APPROVE_BY } from '../../utils/constants';
import ActionBar from '../../components/shared/ActionBar';
import { getUploadedFiles } from '../../store/actions/fileActions';
import { withRouter } from 'react-router';
import Loading from '../../components/shared/Loading';
import { TASK_IDS } from '../../utils/constants';
import { getLocalStorage } from '../../utils/secureLS';
import { getDisableStatus } from '../../utils/urlUtils';
import Helmet from 'react-helmet';
import { getLastUrl } from '../../utils/urlUtils';

// import { checkError } from '../../utils/dataUtils';
// import { List } from 'semantic-ui-react';
// import { getDocUrl } from '../../utils/config';

class FormContaner extends Component {
	constructor(props) {
		super(props);
		this.formRef = React.createRef();
		this.state = {
			formRef: '',
			formUrl: '',
			prevData: {},
			hasSavePermission: false,
			hasApprovePermission: false,
			// printcontent: false
		};
	}

	componentDidMount() {
		//set to redux(calling all apis)

		if (isEmpty(this.props.permitData) || isEmpty(this.props.userData)) {
			this.props.getPermitAndUserData();
		}

		this.props.getMultipleFormDataByUrl(this.props.api, this.props.prepareData);
		this.props.getUploadedFiles();

		const formUtility = this.props.formUtility;
		this.setState({
			formUrl: formUtility.getUrl(),
			hasSavePermission: formUtility.savePermission(),
			hasApprovePermission: formUtility.approvePermission(),
		});
	}

	componentDidUpdate(prevProps) {
		if (prevProps.formData !== this.props.formData) {
			const prevDataName = this.props.api.find(url => url.form).objName;
			const prevData = this.props.formData[prevDataName];
			this.setState({ prevData });
		}
	}

	//ref passed by individual component
	setRef = ref => {
		// console.log(ref);
		this.setState({ formRef: ref });
		// this.props.parentProps.setPrintAllRef(ref);
	};

	//print status globally
	setPrintContent = value => {
		this.props.setPrint(value);
	};

	setFileUploadRef = ref => {
		this.setState({ fileUploadRef: ref });
	};

	render() {
		const { hasSavePermission, hasApprovePermission, formUrl, prevData } = this.state;
		const otherDataName = this.props.api.filter(url => !url.form);

		let otherData = {};
		let hasLoaded = [];

		// console.log('this.props', this.props.formData);

		otherDataName.forEach((obj, index) => {
			//componentDidMount ma redux ma load gareko data
			if (!isEmpty(this.props.formData[obj.objName]) || Array.isArray(this.props.formData[obj.objName])) {
				hasLoaded[index] = true;
				Object.assign(otherData, {
					[obj.objName]: this.props.formData[obj.objName],
				});
			} else {
				hasLoaded[index] = false;
			}
		});

		// console.log('has loaded?', hasLoaded);

		const allLoaded = hasLoaded.reduce((loaded, next) => loaded && next, true);

		// console.log('gall loaded?', allLoaded);

		if (
			this.props.userData &&
			this.props.permitData &&
			// !isEmpty(otherData) &&
			allLoaded &&
			!isEmpty(prevData)
		) {
			let erStatus = 'P';
			let serStatus = 'P';
			let rwStatus = '';
			let chiefStatus = '';
			let aminiStatus = '';
			if (prevData) {
				erStatus = prevData.erStatus || 'P';
				serStatus = prevData.serStatus || 'P';
				rwStatus = this.props.needsRajaswo ? prevData.rwStatus : '';
				aminiStatus = prevData.aminiStatus || '';
				chiefStatus = prevData.chiefStatus || '';
			}

			let entrBy = prevData.enterBy;


			const formApi = this.props.api.find(api => api.form).api;

			/**
			 * @todo remove
			 * DONT COMMIT!!!!
			 */
			// DON'T COMMIT !!!! const hasSavePermission = true; // DON'T COMMIT

			const isDisabled = this.props.hasFileView
				? getDisableStatus(this.props.parentProps.location.pathname, { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus }) ||
				!this.props.hasFileView
				: getDisableStatus(this.props.parentProps.location.pathname, { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus });

			const permitData = this.props.permitData;
			//@ts-ignore
			const taskIds = getLocalStorage(TASK_IDS);
			let isVisible = taskIds && JSON.parse(taskIds).some(each => each === permitData.applicantNo);

			return (
				<div>
					<Helmet>
						<title>{getLastUrl(formUrl)}</title>
					</Helmet>
					<Loading loading={this.props.printcontent}> </Loading>
					{!this.props.parentProps.setRef && (
						<ActionBar
							stickycontent={this.formRef}
							content={this.props.useInnerRef ? this.state.formRef : this.formref}
							stickycontext={this.formref}
							setFileUploadRef={this.setFileUploadRef}
							setPrintContent={this.setPrintContent}
							approvalUrl={`${formApi}${this.props.permitData.applicantNo}`}
							parentHistory={this.props.parentProps.history}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasApprovePermission={hasApprovePermission}
							historyList={this.props.formHistory}
							comments={this.props.comment}
							erStatus={erStatus}
							serStatus={serStatus}
							rwStatus={rwStatus}
							chiefStatus={chiefStatus}
							aminiStatus={aminiStatus}
							enterBy={entrBy}
							needsRajaswo={this.props.needsRajaswo}
							fileCategories={this.props.fileCategories}
							url={this.props.match.url}
							onBeforeGetContent={this.props.onBeforeGetContent}
							permitData={this.props.permitData}
							prevData={prevData}
							// otherData={otherData}
							isVisible={isVisible}
						// printAllref={this.props.parentProps.setPrintAllRef}
						/>
					)}

					<FormWrapper
						hasPermission={this.props.hasFileView ? hasSavePermission || this.props.hasFileView : hasSavePermission}
						isFormDisabled={isDisabled}
						isVisible={isVisible}
					>
						<div ref={this.props.parentProps.setprintref} className="main-content">
							<div ref={el => (this.formref = el)}>
								{this.props.render({
									permitData: this.props.permitData,
									userData: this.props.userData,
									prevData: prevData,
									otherData: otherData,
									postAction: this.props.hasFile ? this.props.postFormDataWithFileByUrl : this.props.postFormDataByUrl,
									loading: this.props.loading,
									errors: this.props.errors,
									success: this.props.success,
									fileCategories: this.props.fileCategories,
									files: this.props.files,
									hasSavePermission: hasSavePermission,
									fileUploadRef: this.state.fileUploadRef,
									setRef: this.props.parentProps.setRef ? this.props.parentProps.setRef : this.setRef,

									printcontent: this.props.printcontent,
								})}
							</div>
						</div>
					</FormWrapper>

					{/* {this.props.match.url === '/user/forms/rajaswo-voucher' &&
						!isEmpty(checkError(prevData)) &&
						prevData.voucherUrl && (
							<List relaxed>
								<List.Header>Existing File</List.Header>
								<List.Item
									key={prevData.voucherUrl}
									content={prevData.voucherUrl}
									icon="file alternate outline"
									target="_blank"
									href={`${getDocUrl()}${prevData.voucherUrl}`}
								></List.Item>
							</List>
						)} */}
					{/* {hasPermission(
            this.props.parentProps.location.pathname,
            APPROVE_BY
          ) && (
            <ApprovalGroup
              history={this.props.parentProps.history}
              url={`${formApi}${this.props.permitData.applicantNo}`}
            />
          )}

          <ApprovalStatusDisplay
            erStatus={erStatus || 'P'}
            serStatus={serStatus || 'P'}
            comments={this.props.comment}
          />
          <HistorySidebar history={this.props.formHistory} /> */}
				</div>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getMultipleFormDataByUrl,
	postFormDataByUrl,
	postFormDataWithFileByUrl,
	getUploadedFiles,
	setPrint: res => setPrint(res),
};
const mapStateToProps = state => ({
	permitData: state.root.formData.permitData,
	userData: state.root.formData.userData,
	formData: state.root.formData,
	comment: state.root.formData.comment,
	formHistory: state.root.formData.history,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
	printcontent: state.root.general.printcontent,
	fileCategories: state.root.formData.fileCategories,
	files: state.root.formData.files,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormContaner));
