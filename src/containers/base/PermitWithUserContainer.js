import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPermitAndUserData } from '../../store/actions/formActions';
import FallbackComponent from '../../components/shared/FallbackComponent';

class PermitWithUserContainer extends Component {
  componentDidMount() {
    this.props.getPermitAndUserData();
  }

  render() {
    if (this.props.userData) {
      return (
        <div>
          {this.props.render({
            permitData: this.props.permitData,
            userData: this.props.userData
          })}
        </div>
      );
    } else {
      return (
        <FallbackComponent
          errors={this.props.errors}
          loading={this.props.loading}
        />
      );
    }
  }
}

const mapDispatchToProps = { getPermitAndUserData };
const mapStateToProps = state => ({
  permitData: state.root.formData.permitData,
  userData: state.root.formData.userData,
  loading: state.root.ui.loading,
  errors: state.root.ui.errors
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PermitWithUserContainer);
