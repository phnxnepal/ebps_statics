import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getPermitAndUserData, getMultipleFormDataByUrl, postFormDataByUrl, postFormDataWithFileByUrl } from '../../store/actions/formActions';

import { setPrint } from '../../store/actions/general';
import FallbackComponent from '../../components/shared/FallbackComponent';
// import ApprovalStatusDisplay from '../../components/shared/ApprovalStatusDisplay';
// import HistorySidebar from '../../components/shared/HistoryComponent';
import {
	// hasPermission,
	isEmpty,
	getUserRole,
	getBuildPermitObj,
	showWarnToast,
	// getPreviousFormStatus,
	// makeprintformat,
	// makespecialprintformat
} from '../../utils/functionUtils';
// import ApprovalGroup from '../../components/shared/ApprovalGroup';
import FormWrapper from '../../components/shared/FormWrapper';
// import { ENTER_BY, APPROVE_BY } from '../../utils/constants';
import ActionBar from '../../components/shared/ActionBar';
import { getUploadedFiles } from '../../store/actions/fileActions';
import { withRouter } from 'react-router';
import Loading from '../../components/shared/Loading';
import { TASK_IDS } from '../../utils/constants';
import { getLocalStorage } from '../../utils/secureLS';
import { getDisableStatus, getLastUrl } from '../../utils/urlUtils';
import { UserType } from '../../utils/userTypeUtils';
import { Form, Label } from 'semantic-ui-react';
import { buildingPermitApplicationForm } from '../../utils/data/mockLangFile';
import { FormUrlFull } from '../../utils/enums/url';
import { validateNumberWithDash } from '../../utils/validationUtils';
import { numberMessages } from '../../utils/data/validationData';
import Helmet from 'react-helmet';
import { FormUtility, getStaticFiles } from '../../utils/formUtils';
import { permitErrors } from '../../utils/data/errorMessages';
// import { ApprovalComments } from '../../components/shared/ApprovalComments';
// import { checkError } from '../../utils/dataUtils';
// import { List } from 'semantic-ui-react';
// import { getDocUrl } from '../../utils/config';

class JSONDataMultipleGetFormContainer extends Component {
	constructor(props) {
		super(props);
		this.formRef = React.createRef();
		this.state = {
			formRef: '',
			formUrl: '',
			approvalAppNo: undefined,
			appNoError: '',
			isSubApproving: getUserRole() === UserType.SUB_ENGINEER && this.props.parentProps.location.pathname === FormUrlFull.BUILD_PERMIT,
			prefixes: {},
			formUtility: new FormUtility(this.props.parentProps.location.pathname),
			hasSavePermission: false,
			hasApprovePermission: false,
			// printcontent: false
		};
	}

	componentDidMount() {
		// const formUtility = new FormUtility(this.props.parentProps.location.pathname);
		const permitData = getBuildPermitObj();
		if (!(permitData && permitData.applicantNo)) {
			showWarnToast(permitErrors.noPermitSet)
			this.props.history.push('/user/task-list');
		}
		const { formUtility } = this.state;

		this.setState({
			formUrl: formUtility.getUrl(),
			hasSavePermission: formUtility.getSavePermission(),
			hasApprovePermission: formUtility.getApprovePermission(),
			prefixes: formUtility.getPrefixes(),
			formUtility,
		});
		//set to redux(calling all apis)

		if (isEmpty(this.props.permitData) || isEmpty(this.props.userData)) {
			this.props.getPermitAndUserData();
		}

		this.props.getMultipleFormDataByUrl(this.props.api, this.props.prepareData);
		this.props.getUploadedFiles();
	}

	componentDidUpdate(prevProps) {
		if (this.props.permitData && this.props.permitData !== prevProps.permitData && this.state.isSubApproving) {
			if (this.props.permitData.regNo) {
				this.setState({ approvalAppNo: this.props.permitData.regNo });
			} else {
				this.setState({ approvalAppNo: this.props.permitData.applicantNo });
			}
		}
	}

	//ref passed by individual component
	setRef = (ref) => {
		// console.log(ref);
		this.setState({ formRef: ref });
		// this.props.parentProps.setPrintAllRef(ref);
	};

	setApprovalRef = (ref) => {
		this.setState({ approvalRef: ref });
		// this.props.parentProps.setPrintAllRef(ref);
	};

	setApprovalAppNo = (e) => {
		const approvalAppNo = e.target.value;

		validateNumberWithDash.isValid(approvalAppNo).then((valid) => {
			if (!valid) {
				this.setState({ appNoError: numberMessages.number });
			} else {
				this.setState({ appNoError: '' });
			}
		});

		this.setState({ approvalAppNo });
	};

	//print status globally
	setPrintContent = (value) => {
		this.props.setPrint(value);
	};

	setFileUploadRef = (ref) => {
		this.setState({ fileUploadRef: ref });
	};

	render() {
		const prevDataName = this.props.api.find((url) => url.form).objName;
		const prevFormDataName = this.props.api.find((url) => url.prevForm);
		const otherDataName = this.props.api.filter((url) => !url.form);
		const {
			// formUrl,
			formUtility,
			hasSavePermission,
			hasApprovePermission,
		} = this.state;

		//componentDidMount ma redux ma load gareko data

		const prevData = this.props.formData[prevDataName];
		let prevFormData;

		if (prevFormDataName) {
			prevFormData = this.props.formData[prevFormDataName.objName];
		}

		const prevFormCondition = prevFormDataName ? (!isEmpty(prevFormData) ? true : false) : true;
		let otherData = {};
		let hasLoaded = [];

		// console.log('this.props', this.props.formData);

		otherDataName.forEach((obj, index) => {
			//componentDidMount ma redux ma load gareko data
			if (!isEmpty(this.props.formData[obj.objName]) || Array.isArray(this.props.formData[obj.objName])) {
				hasLoaded[index] = true;
				Object.assign(otherData, {
					[obj.objName]: this.props.formData[obj.objName],
				});
			} else {
				hasLoaded[index] = false;
			}
		});

		// console.log('has loaded?', hasLoaded);

		const allLoaded = hasLoaded.reduce((loaded, next) => loaded && next, true);

		// console.log('gall loaded?', allLoaded);

		if (
			this.props.userData &&
			this.props.permitData &&
			// !isEmpty(otherData) &&
			allLoaded &&
			!isEmpty(prevData) &&
			prevFormCondition
		) {
			let erStatus = 'P';
			let serStatus = 'P';
			let rwStatus = '';
			let chiefStatus = '';
			let aminiStatus = '';
			if (prevData) {
				erStatus = prevData.erStatus || 'P';
				serStatus = prevData.serStatus || 'P';
				rwStatus = this.props.needsRajaswo ? prevData.rwStatus : '';
				aminiStatus = prevData.aminiStatus || '';
				chiefStatus = prevData.chiefStatus || '';
			}

			// console.log('logging', !prevFormDataName, getPreviousFormStatus(prevFormData));
			// const isPreviousFormCompleted = getPreviousFormStatus(prevFormData, prevFormDataName);

			let entrBy = prevData.enterBy;

			// const hasSavePermission = getSavePermission(this.props.parentProps.location.pathname);
			//  && isPreviousFormCompleted;
			const { prefixValues: statuses, isRejected } = formUtility.getStatusNameValues(prevData);

			const formApi = this.props.api.find((api) => api.form).api;

			// const hasApprovePermission = getApprovePermission(this.props.parentProps.location.pathname);
			//  && isPreviousFormCompleted;
			/**
			 * @todo remove
			 * DONT COMMIT!!!!
			 */
			// DON'T COMMIT !!!! const hasSavePermission = true; // DON'T COMMIT

			const isDisabled = this.props.hasFileView
				? getDisableStatus(this.props.parentProps.location.pathname, { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus }) ||
				  !this.props.hasFileView
				: getDisableStatus(this.props.parentProps.location.pathname, { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus });

			const permitData = this.props.permitData;
			//@ts-ignore
			const taskIds = getLocalStorage(TASK_IDS);
			let isVisible = taskIds && JSON.parse(taskIds).some((each) => each === permitData.applicantNo);

			return (
				<div>
					<Helmet>
						<title>{getLastUrl(this.state.formUrl)}</title>
					</Helmet>
					<Loading loading={this.props.printcontent}> </Loading>
					{!this.props.parentProps.setRef && (
						<ActionBar
							stickycontent={this.formRef}
							content={this.props.useInnerRef ? this.state.formRef : this.formref}
							approvalRef={this.state.approvalRef}
							stickycontext={this.formref}
							setFileUploadRef={this.setFileUploadRef}
							setPrintContent={this.setPrintContent}
							approvalUrl={`${formApi}${this.props.permitData.applicantNo}`}
							parentHistory={this.props.parentProps.history}
							formUrl={this.props.parentProps.location.pathname}
							hasSavePermission={hasSavePermission}
							hasApprovePermission={hasApprovePermission}
							historyList={this.props.formHistory}
							comments={this.props.comment}
							statuses={statuses}
							isRejected={isRejected}
							enterBy={entrBy}
							needsRajaswo={this.props.needsRajaswo}
							fileCategories={this.props.fileCategories}
							url={this.props.match.url}
							onBeforeGetContent={this.props.onBeforeGetContent}
							permitData={this.props.permitData}
							prevData={prevData}
							// otherData={otherData}
							isVisible={isVisible}
							approvalAppNo={this.state.approvalAppNo}
							appNoError={this.state.appNoError}
							// printAllref={this.props.parentProps.setPrintAllRef}
						/>
					)}

					{this.state.isSubApproving && (
						<Form>
							<div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '50px' }}>
								<Form.Field width="4" inline error={!!this.state.appNoError}>
									<label>{buildingPermitApplicationForm.permitApplicationFormView.form_sakha_darta_no}</label>
									<input name="approvalAppNo" value={this.state.approvalAppNo} onChange={this.setApprovalAppNo} />
									{this.state.appNoError && (
										<Label pointing="left" prompt size="large" basic color="red">
											{this.state.appNoError}
										</Label>
									)}
								</Form.Field>
							</div>
						</Form>
					)}

					<FormWrapper
						hasPermission={this.props.hasFileView ? hasSavePermission || this.props.hasFileView : hasSavePermission}
						isFormDisabled={isDisabled}
						isVisible={isVisible}
					>
						<div ref={this.props.parentProps.setprintref} className="main-content">
							<div ref={(el) => (this.formref = el)}>
								{this.props.render({
									permitData: this.props.permitData,
									userData: this.props.userData,
									prevData: prevData,
									otherData: otherData,
									postAction: this.props.hasFile ? this.props.postFormDataWithFileByUrl : this.props.postFormDataByUrl,
									loading: this.props.loading,
									errors: this.props.errors,
									success: this.props.success,
									fileCategories: this.props.fileCategories,
									files: this.props.files,
									hasSavePermission: hasSavePermission,
									fileUploadRef: this.state.fileUploadRef,
									setRef: this.props.parentProps.setRef ? this.props.parentProps.setRef : this.setRef,
									staticFiles: this.props.files ? getStaticFiles(this.props.files) : {},
									printcontent: this.props.printcontent,
								})}
							</div>
						</div>
					</FormWrapper>

					{/* {this.props.match.url === '/user/forms/rajaswo-voucher' &&
						!isEmpty(checkError(prevData)) &&
						prevData.voucherUrl && (
							<List relaxed>
								<List.Header>Existing File</List.Header>
								<List.Item
									key={prevData.voucherUrl}
									content={prevData.voucherUrl}
									icon="file alternate outline"
									target="_blank"
									href={`${getDocUrl()}${prevData.voucherUrl}`}
								></List.Item>
							</List>
						)} */}
					{/* {hasPermission(
            this.props.parentProps.location.pathname,
            APPROVE_BY
          ) && (
            <ApprovalGroup
              history={this.props.parentProps.history}
              url={`${formApi}${this.props.permitData.applicantNo}`}
            />
          )}

          <ApprovalStatusDisplay
            erStatus={erStatus || 'P'}
            serStatus={serStatus || 'P'}
            comments={this.props.comment}
          />
          <HistorySidebar history={this.props.formHistory} /> */}
				</div>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getMultipleFormDataByUrl,
	postFormDataByUrl,
	postFormDataWithFileByUrl,
	getUploadedFiles,
	setPrint: (res) => setPrint(res),
};
const mapStateToProps = (state) => ({
	permitData: state.root.formData.permitData,
	userData: state.root.formData.userData,
	formData: state.root.formData,
	comment: state.root.formData.comment,
	formHistory: state.root.formData.history,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
	printcontent: state.root.general.printcontent,
	fileCategories: state.root.formData.fileCategories,
	files: state.root.formData.files,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(JSONDataMultipleGetFormContainer));
