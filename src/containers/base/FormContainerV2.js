import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';
import { getPermitAndUserData, getFormContainerData, postFormDataByUrl, postFormDataWithFileByUrl } from '../../store/actions/formActions';

import { setPrint } from '../../store/actions/general';
import FallbackComponent from '../../components/shared/FallbackComponent';
import { isEmpty, getBuildPermitObj, showWarnToast } from '../../utils/functionUtils';
import FormWrapper from '../../components/shared/FormWrapper';
import ActionBar from '../../components/shared/ActionBar';
import { withRouter } from 'react-router';
import Loading from '../../components/shared/Loading';
import { TASK_IDS, DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } from '../../utils/constants';
import { getLocalStorage } from '../../utils/secureLS';
import { getLastUrl, getDeleteDisabledStatus } from '../../utils/urlUtils';
import { FormUtility, getStaticFiles } from '../../utils/formUtils';
import Helmet from 'react-helmet';
import { Message, Sticky, Button } from 'semantic-ui-react';
import { addPermitPrompt } from '../../utils/data/genericData';
import { ApprovalComments, RayaForms } from '../../components/shared/ApprovalComments';
import { checkError } from '../../utils/dataUtils';
import { checkClient, Client } from '../../utils/clientUtils';
import { UNITS } from '../../utils/enums/unit';
import { LSKey } from '../../utils/enums/localStorageKeys';
import { permitErrors } from '../../utils/data/errorMessages';
import Printthis from '../../components/shared/Printthis';

class FormContaner extends Component {
	constructor(props) {
		super(props);
		this.formRef = React.createRef();
		this.state = {
			formRef: '',
			formUrl: '',
			prevData: {},
			userData: {},
			orgCode: '',
			useSignatureImage: false,
			userType: '',
			permitData: {},
			otherData: {},
			localData: {},
			hasSavePermission: false,
			hasApprovePermission: false,
			prefixes: {},
			formUtility: {},
			isSaveDisabled: false,
			hasDeletePermission: false,
			allLoaded: false,
			hasPreviousFiles: true,
			DEFAULT_UNIT_LENGTH: DEFAULT_UNIT_LENGTH,
			DEFAULT_UNIT_AREA: DEFAULT_UNIT_AREA,
			hasDesignerChanged: false,
		};
	}

	componentDidMount() {
		// if (isEmpty(this.props.permitData) || isEmpty(this.props.userData)) {
		// 	this.props.getPermitAndUserData();
		// }

		const permitData = getBuildPermitObj();
		if (!(permitData && permitData.applicantNo)) {
			showWarnToast(permitErrors.noPermitSet);
			this.props.history.push('/user/task-list');
		}

		const formUtility = new FormUtility(this.props.parentProps.location.pathname);
		this.props.getFormContainerData(
			this.props.api,
			this.props.prepareData,
			formUtility,
			this.props.localData,
			this.props.checkPreviousFiles || this.props.fetchFiles
		);

		this.setState({
			formUrl: formUtility.getUrl(),
			hasSavePermission: formUtility.getSavePermission(),
			hasApprovePermission: formUtility.getApprovePermission(),
			prefixes: formUtility.getPrefixes(),
			formUtility,
		});
	}

	componentDidUpdate(prevProps, prevState) {
		if (JSON.stringify(prevProps.formData) !== JSON.stringify(this.props.formData)) {
			const prevDataName = this.props.api.find((url) => url.form).objName;
			const prevData = this.props.formData[prevDataName];
			const otherDataNames = this.props.api.filter((url) => !url.form);

			let otherData = {};
			let localDataObject = {};
			let hasLoaded = [];

			if (this.props.localData && Array.isArray(this.props.localData)) {
				for (const localKey of this.props.localData) {
					localDataObject[localKey.getObjName()] = this.props.formData[localKey.getObjName()];
				}
			}

			otherDataNames.forEach((obj, index) => {
				if (!isEmpty(this.props.formData[obj.objName]) || Array.isArray(this.props.formData[obj.objName])) {
					hasLoaded[index] = true;
					Object.assign(otherData, {
						[obj.objName]: this.props.formData[obj.objName],
					});
				} else {
					hasLoaded[index] = false;
				}
			});

			const allLoaded = hasLoaded.reduce((loaded, next) => loaded && next, true);

			if (allLoaded && this.props.formData.permitData && this.props.formData.userData) {
				this.setState({
					prevData,
					otherData,
					allLoaded,
					localDataObject,
					hasPreviousFiles: !this.props.checkPreviousFiles ? true : this.props.formData.hasPreviousFiles,
				});
			}
		}

		if (JSON.stringify(prevProps.userData) !== JSON.stringify(this.props.userData) && this.props.userData) {
			const orgCode = this.props.userData.organization && this.props.userData.organization.organizationCode;
			this.setState({
				userData: this.props.userData,
				useSignatureImage: this.props.userData.organization && this.props.userData.organization.showSignatureImage === 'Y',
				userType: this.props.userData.userType,
				orgCode,
				DEFAULT_UNIT_LENGTH: checkClient(orgCode, Client.BIRTAMOD) ? UNITS.METRE.LENGTH : DEFAULT_UNIT_LENGTH,
				DEFAULT_UNIT_AREA: checkClient(orgCode, Client.BIRTAMOD) ? UNITS.METRE.AREA : DEFAULT_UNIT_AREA,
				hasDesignerChanged: getLocalStorage(LSKey.HAS_DESIGNER_CHANGED) || false,
				newDesignerName: getLocalStorage(LSKey.NEW_DESIGNER_NAME) || null,
			});
		}

		if (JSON.stringify(prevProps.permitData) !== JSON.stringify(this.props.permitData) && this.props.permitData) {
			const updatedPermit = fromJS(this.props.permitData);
			const { permitData } = this.props;
			if (getLocalStorage(LSKey.NAMSARI_STATUS) === 'C') {
				const certificateNoteName = getLocalStorage(LSKey.CERTIFICATE_NOTE_DATA);
				const newName = certificateNoteName || permitData.applicantName;
				this.setState({ permitData: updatedPermit.set('applicantName', newName).toJS() });
			} else {
				this.setState({ permitData: this.props.permitData });
			}
		}

		if (
			(JSON.stringify(prevState.prevData) !== JSON.stringify(this.state.prevData) ||
				JSON.stringify(prevProps.userData) !== JSON.stringify(this.props.userData)) &&
			!!(this.state.prevData && this.props.userData)
		) {
			const { hasDeletePermission, isSaveDisabled } = getDeleteDisabledStatus(this.state.formUrl, this.state.prevData, this.props.userData);
			if (hasDeletePermission !== undefined && isSaveDisabled !== undefined) {
				this.setState({ hasDeletePermission, isSaveDisabled });
			}
		}
	}

	//ref passed by individual component
	setRef = (ref) => {
		// console.log(ref);
		this.setState({ formRef: ref });
		// this.props.parentProps.setPrintAllRef(ref);
	};

	setApprovalRef = (ref) => {
		this.setState({ approvalRef: ref });
		// this.props.parentProps.setPrintAllRef(ref);
	};
	//print status globally
	setPrintContent = (value) => {
		this.props.setPrint(value);
	};

	setFileUploadRef = (ref) => {
		this.setState({ fileUploadRef: ref });
	};

	render() {
		const {
			hasSavePermission,
			hasApprovePermission,
			hasDeletePermission,
			isSaveDisabled,
			formUrl,
			formUtility,
			prevData,
			otherData,
			permitData,
			userData,
			allLoaded,
			orgCode,
			useSignatureImage,
			userType,
			localDataObject,
			hasPreviousFiles,
			DEFAULT_UNIT_AREA,
			DEFAULT_UNIT_LENGTH,
			hasDesignerChanged,
			newDesignerName,
		} = this.state;
		const { loading, errors, viewOnly } = this.props;

		if (allLoaded) {
			let entrBy = '';
			if (prevData) {
				entrBy = prevData.enterBy;
			}

			const { prefixValues: statuses, isRejected } = formUtility.getStatusNameValues(prevData);

			const formApi = this.props.api.find((api) => api.form).api;

			/**
			 * @todo remove
			 * DONT COMMIT!!!!
			 */
			// DON'T COMMIT !!!! const hasSavePermission = true; // DON'T COMMIT

			const isDisabled = this.props.hasFileView ? isSaveDisabled || !this.props.hasFileView : isSaveDisabled;

			const taskIds = getLocalStorage(TASK_IDS);
			let isVisible = taskIds && JSON.parse(taskIds).some((each) => each === permitData.applicantNo);

			const approveByUsers = formUtility.getApproveByUserDetails(prevData);

			return (
				<div>
					<Helmet>
						<title>{getLastUrl(formUrl)}</title>
					</Helmet>
					<Loading loading={this.props.printcontent}> </Loading>
					{!this.props.parentProps.setRef &&
						(viewOnly ? (
							<Sticky offset={50} style={{ marginTop: '-30px' }}>
								<Printthis
									ui={<Button className="primary-btn" content="Print" icon="print" />}
									content={() => (this.props.useInnerRef ? this.state.formRef : this.formref)}
									onBeforeGetContent={() => {
										this.setPrintContent(true);
									}}
									onBeforePrint={() => {
										this.setPrintContent(false);
									}}
								/>
							</Sticky>
						) : (
							<ActionBar
								stickycontent={this.formRef}
								content={this.props.useInnerRef ? this.state.formRef : this.formref}
								approvalRef={this.state.approvalRef}
								stickycontext={this.formref}
								setFileUploadRef={this.setFileUploadRef}
								setPrintContent={this.setPrintContent}
								approvalUrl={`${formApi}${permitData.applicantNo}`}
								parentHistory={this.props.parentProps.history}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasApprovePermission={hasApprovePermission}
								historyList={this.props.formHistory}
								comments={this.props.comment}
								statuses={statuses}
								isRejected={isRejected}
								files={this.props.files}
								enterBy={entrBy}
								needsRajaswo={this.props.needsRajaswo}
								fileCategories={this.props.fileCategories}
								onBeforeGetContent={this.props.onBeforeGetContent}
								permitData={permitData}
								prevData={prevData}
								// otherData={otherData}
								isVisible={isVisible}
								// printAllref={this.props.parentProps.setPrintAllRef}
							/>
						))}

					<br />
					{!hasPreviousFiles && (
						<Message
							floating
							negative
							className="renew"
							icon="warning circle"
							header={addPermitPrompt.previousFileMissing.title}
							content={addPermitPrompt.previousFileMissing.content}
						/>
					)}
					<FormWrapper
						hasPermission={this.props.hasFileView ? hasSavePermission || this.props.hasFileView : hasSavePermission}
						isFormDisabled={isDisabled || !hasPreviousFiles}
						isVisible={isVisible}
					>
						<div ref={this.props.parentProps.setprintref} className="main-content">
							<div ref={(el) => (this.formref = el)}>
								{this.props.render({
									permitData: permitData,
									userData: userData,
									prevData: prevData,
									otherData: otherData,
									localDataObject,
									postAction: this.props.hasFile ? this.props.postFormDataWithFileByUrl : this.props.postFormDataByUrl,
									loading: loading,
									errors: errors,
									success: this.props.success,
									fileCategories: this.props.fileCategories,
									files: this.props.files,
									hasSavePermission: hasSavePermission,
									hasDeletePermission,
									isSaveDisabled,
									fileUploadRef: this.state.fileUploadRef,
									setRef: this.props.parentProps.setRef ? this.props.parentProps.setRef : this.setRef,
									orgCode: orgCode,
									useSignatureImage,
									userType: userType,
									formUrl: formUrl,
									enterByUser: formUtility.getEnterByUser(),
									approveByUsers,
									printcontent: this.props.printcontent,
									hasPreviousFiles,
									staticFiles: this.props.files ? getStaticFiles(this.props.files) : {},
									/**
									 * Prop getter for @see getApproveByObject()
									 */
									getApproveByProps: () => ({ approveByUsers, userData, orgCode, formUrl }),
									/**
									 * Prop getter for @see SaveButtonValidation
									 */
									getSaveButtonProps: () => ({
										formUrl,
										hasSavePermission,
										hasDeletePermission,
										isSaveDisabled,
										prevData: checkError(prevData),
									}),
									DEFAULT_UNIT_AREA,
									DEFAULT_UNIT_LENGTH,
									hasDesignerChanged,
									newDesignerName,
								})}
								<br />
								{RayaForms.includes(formUrl) && (
									<ApprovalComments setApprovalRef={this.setApprovalRef} comments={this.props.comment} />
								)}
							</div>
						</div>
					</FormWrapper>
				</div>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getFormContainerData,
	postFormDataByUrl,
	postFormDataWithFileByUrl,
	setPrint: (res) => setPrint(res),
};
const mapStateToProps = (state) => {
	return {
		permitData: state.root.formData.permitData,
		userData: state.root.formData.userData,
		formData: state.root.formData,
		comment: state.root.formData.comment,
		formHistory: state.root.formData.history,
		loading: state.root.ui.loading,
		errors: state.root.ui.errors,
		success: state.root.formData.success,
		printcontent: state.root.general.printcontent,
		fileCategories: state.root.formData.fileCategories,
		files: state.root.formData.files,
	};
};

FormContaner.propTypes = {
	api: PropTypes.array.isRequired,
	render: PropTypes.func.isRequired,
	parentProps: PropTypes.object.isRequired,
	fetchFiles: PropTypes.bool,
	checkPreviousFiles: PropTypes.bool,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FormContaner));
