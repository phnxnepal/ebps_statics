import React, { Component } from 'react';

//Redux
import {
	deleteAdminDataByUrl,
	getAdminDataByUrl,
	postAdminDataByUrl,
	patchAdminDataByUrl,
	putAdminDataByUrl,
	getAfterUpdateAdminData,
	getAdminLocalDataByUrl,
} from '../../store/actions/AdminAction';
import FallbackComponent from '../../components/shared/FallbackComponent';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { isEmpty } from '../../utils/functionUtils';

class AdminContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			allLoaded: false,
		};
	}

	componentDidMount() {
		this.props.getAdminDataByUrl(this.props.api);
		if (this.props.localApi) {
			this.props.getAdminLocalDataByUrl(this.props.localApi);
		}
	}

	componentDidUpdate(prevProps) {
		if (this.props.adminData !== prevProps.adminData) {
			const dataNames = this.props.api;
			let hasLoaded = [];
			let data = {};

			dataNames.forEach((obj, index) => {
				if (!isEmpty(this.props.adminData[obj.objName]) || Array.isArray(this.props.adminData[obj.objName])) {
					hasLoaded[index] = true;
					Object.assign(data, {
						[obj.objName]: this.props.adminData[obj.objName],
					});
				} else {
					hasLoaded[index] = false;
				}
			});

			this.setState({ allLoaded: hasLoaded.reduce((loaded, next) => loaded && next, true) });
		}

		if (this.props.success !== prevProps.success) {
			this.props.getAfterUpdateAdminData(this.props.updateApi);
		}
	}

	render() {
		if (this.state.allLoaded) {
			return <div>{this.props.render({ ...this.props })}</div>;
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getAdminDataByUrl,
	getAfterUpdateAdminData,
	postAdminDataByUrl,
	patchAdminDataByUrl,
	putAdminDataByUrl,
	deleteAdminDataByUrl,
	getAdminLocalDataByUrl,
};

const mapStateToProps = state => ({
	adminData: state.root.admin,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdminContainer));
