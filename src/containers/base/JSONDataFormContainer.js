import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  getFormDataWithPermitAndUser,
  postFormDataByUrl
} from '../../store/actions/formActions';
import FallbackComponent from '../../components/shared/FallbackComponent';
import ApprovalStatusDisplay from '../../components/shared/ApprovalStatusDisplay';
import HistorySidebar from '../../components/shared/HistoryComponent';
import { hasPermission } from '../../utils/functionUtils';
import ApprovalGroup from '../../components/shared/ApprovalGroup';
import FormWrapper from '../../components/shared/FormWrapper';
import { ENTER_BY, APPROVE_BY } from '../../utils/constants';

class JSONDataFormContainer extends Component {
  componentDidMount() {
    this.props.getFormDataWithPermitAndUser(
      this.props.api,
      this.props.prepareData
    );
  }

  render() {
    if (this.props.userData && this.props.permitData && this.props.prevData) {
      let erStatus = 'P';
      let serStatus = 'P';
      if (this.props.prevData) {
        erStatus = this.props.prevData.erStatus;
        serStatus = this.props.prevData.serStatus;
      }

      const hasSavePermission = hasPermission(
        this.props.parentProps.location.pathname,
        ENTER_BY
      );

      //   const hasSavePermission = true;

      return (
        <div>
          <FormWrapper hasPermission={hasSavePermission} erStatus={erStatus}>
            {this.props.render({
              permitData: this.props.permitData,
              userData: this.props.userData,
              prevData: this.props.prevData,
              postAction: this.props.postFormDataByUrl,
              loading: this.props.loading,
              errors: this.props.errors,
              success: this.props.success,
              hasSavePermission: hasSavePermission
            })}
          </FormWrapper>
          {hasPermission(
            this.props.parentProps.location.pathname,
            APPROVE_BY
          ) && (
            <ApprovalGroup
              history={this.props.parentProps.history}
              url={`${this.props.api}${this.props.permitData.applicantNo}`}
            />
          )}

          <ApprovalStatusDisplay
            erStatus={erStatus || 'P'}
            serStatus={serStatus || 'P'}
            comments={this.props.comment}
          />
          <HistorySidebar history={this.props.formHistory} />
        </div>
      );
    } else {
      return (
        <FallbackComponent
          errors={this.props.errors}
          loading={this.props.loading}
        />
      );
    }
  }
}

const mapDispatchToProps = { getFormDataWithPermitAndUser, postFormDataByUrl };
const mapStateToProps = state => ({
  permitData: state.root.formData.permitData,
  userData: state.root.formData.userData,
  prevData: state.root.formData.prevData,
  comment: state.root.formData.comment,
  formHistory: state.root.formData.history,
  loading: state.root.ui.loading,
  errors: state.root.ui.errors,
  success: state.root.formData.success
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JSONDataFormContainer);
