import React from 'react';
// import { Route } from 'react-router-dom';
// import Home from '../components/publicComponents/Home';
// import PublicHeader from '../components/publicComponents/PublicHeader';
// import Contact from '../components/publicComponents/Contact';
import MainSideBar from '../components/loggedInComponents/sideBar';
// import Footer from '../components/loggedInComponents/Footer';

const FormRoutes = ({ dashboardRoutes, sidebarRoutes, handleSearch, isSidebarToggle }) => {
	return (
		<>
			{isSidebarToggle && <MainSideBar routes={sidebarRoutes} handleSearch={handleSearch} />}
			{dashboardRoutes}
		</>
	);
};

export default FormRoutes;
