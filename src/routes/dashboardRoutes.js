import MapPermintApplicationView from '../components/loggedInComponents/forms/MapPermintApplicationView.js';
import MapPermintApplicationEdit from '../components/loggedInComponents/forms/MapPermintApplicationEdit.js';
import FloorDetails from './../components/loggedInComponents/forms/FloorDetailsView';
import DesignApproval from '../components/loggedInComponents/forms/anusuchi_ka/DesignApproval';
import ArchitectureDesignClassCForm from '../components/loggedInComponents/forms/anusuchiKha/ArchitectureDesignClassCForm';
import StructureDesignClassCForm from '../components/loggedInComponents/forms/anusuchiKha/StructureDesignClassCForm';
import ArchitecturalDesignRequirementsClassBForm from './../components/loggedInComponents/forms/anusuchiKha/ArchitectureDesignClassBForm';
import Anushuchiga from './../components/loggedInComponents/forms/anusuchi_ga/AnusuchiGa';
import Anushuchigha from './../components/loggedInComponents/forms/anusuchi_gha/AnusuchiGha';
import MapTechnicalDescriptionView from './../components/loggedInComponents/forms/MapTechnicalDescriptionView';
import RajaswoDetailView from './../components/loggedInComponents/forms/RajaswoDetail';
import NeighboursNameApplicationView from './../components/loggedInComponents/forms/NeighboursNameApplicationView';
import NoticePaymentApplicationView from './../components/loggedInComponents/forms/NoticePaymentApplicationView';
import SurjaminMuchulkaView from './../components/loggedInComponents/forms/ForSurjaminMuchulkaView';
import GharNakshaSurjaminMuchulkaView from './../components/loggedInComponents/forms/GharNakshaSurjaminMuchulkaView';
import PrabhidikPratibedhanPeshView from './../components/loggedInComponents/forms/PrabhidikPratibedhanPeshView';
import NoteOrderPilenthLevelView from './../components/loggedInComponents/forms/NoteOrderPilenthLevelView';
import AllowancePaperView from './../components/loggedInComponents/forms/AllowancePaperView';
import SuperStructureBuildView from './../components/loggedInComponents/forms/SuperStructureBuildView';
import PlinthLevelOwnerRepresentationView from './../components/loggedInComponents/forms/PlinthLevelOwnerRepresentationView';
import PlinthLevelTechApplicationView from './../components/loggedInComponents/forms/PlinthLevelTechApplicationView';
import PlinthLevelTechView from './../components/loggedInComponents/forms/PlinthLevelTechView';
import SuperStructureNoteOrderView from './../components/loggedInComponents/forms/SuperStructureNoteOrderView';
import SuperstructureConstructionView from './../components/loggedInComponents/forms/SuperstructureConstructionView';
import DosrocharanAbedan from './../components/loggedInComponents/forms/DosrocharanAbedan';
import DosrocharanSupervisor from './../components/loggedInComponents/forms/DosrocharanSupervisor';
import DosrocharanPrabidhikView from './../components/loggedInComponents/forms/DosrocharanPrabidhikView';
import CertificateInstructionView from './../components/loggedInComponents/forms/CertificateInstructionView';
import BuildingFinishCertificateView from './../components/loggedInComponents/forms/BuildingFinishCertificateView';
import CertificateNoteView from './../components/loggedInComponents/forms/CertificateNoteView';
import NamsariTippaniAdesView from './../components/loggedInComponents/forms/NamsariTippaniAdesView';
import BuildingBuildCertificateView from './../components/loggedInComponents/forms/BuildingBuildCertificateView';
import ManjurinamaView from './../components/loggedInComponents/forms/ManjurinamaView';
import WarersnamaView from './../components/loggedInComponents/forms/WaresnamaView';
import KabuliyatnamaView from './../components/loggedInComponents/forms/KabuliyatnamaView';
import ElectricalDesign from './../components/loggedInComponents/forms/ElectricalDesign';

import { mainSideBar_menu } from '../utils/data/sideBarLangFile';
import MapCheckReportView from '../components/loggedInComponents/forms/otherForms/MapCheckReportView';
import MapCheckReportKageyshowriView from '../components/loggedInComponents/forms/kageyshowri/MapCheckReportKageyshowriView';
import StructureDesignClassBForm from '../components/loggedInComponents/forms/anusuchiKha/StructureDesignClassBForm';
import ForwardToNextForm from '../components/loggedInComponents/forms/ForwardToNextForm';
import GharNaksakoKabuliyatnama from './../components/loggedInComponents/forms/GharNaksakoKabuliyatnama';
import AminSthalgatPratibedan from './../components/loggedInComponents/forms/AminSthalgatPratibedan';
import SanitaryPlumbingDesignRequirements from '../components/loggedInComponents/forms/SanitaryPlumbingDesignRequirements';
import GharCompondWallNaap from '../components/loggedInComponents/forms/GharCompondWallNaap';
import SansodhanSuperStructure from '../components/loggedInComponents/forms/SansodhanSuperStructure';
import SansodhanGariNibedan from '../components/loggedInComponents/forms/SansodhanGariNibedan';
import SamsodhitSuperStructurePratibedan from '../components/loggedInComponents/forms/SamsodhitSuperStructurePratibedan';
import SansodhanBibaranPahilo from '../components/loggedInComponents/forms/SansodhanBibaranPahilo';
import SuperStructureKoSansodhanBiberan from '../components/loggedInComponents/forms/SuperstructureKoSansodhanBiberan';
import FileUpload from '../components/loggedInComponents/forms/FileUpload.js';
import ListFiles from '../components/loggedInComponents/forms/ListFiles.js';
import RajaswoVoucher from '../components/loggedInComponents/RajaswoVoucher.js';
import DosroCharanBillVuktani from '../components/loggedInComponents/forms/DosroCharanBillVuktani.js';
import PahiloCharanBillVuktani from '../components/loggedInComponents/forms/PahiloCharanBillVuktani';
import SamsodhanBillVuktani from '../components/loggedInComponents/forms/SamsodhanBillVuktani';
import PuranoGharNirmanTippani from '../components/loggedInComponents/forms/PuranoGharNirmanTippani';
import PuranoGharSampannaPramanPatra from '../components/loggedInComponents/forms/PuranoGharNirmanSampanna';
import { FormUrl } from '../utils/enums/url';
import SuperStructurekoNirmaanKarya from '../components/loggedInComponents/forms/SuperStructurekoNirmaanKarya';
import PrabhidikPratibedhanKankai from '../components/loggedInComponents/forms/kankai/PrabhidikPratibedhanPeshView.js';
import DosrocharanPrabidhikKankaiView from '../components/loggedInComponents/forms/kankai/DosrocharanPrabidhikKankaiView.js';
import NoteOrderPlinthLevelKankaiView from '../components/loggedInComponents/forms/kankai/NoteOrderPlinthLevelKankaiView.js';
import PuranoGharNirmanSampannaKankai from '../components/loggedInComponents/forms/kankai/PuranoGharNirmanSampannaKankai.js';
import BuildingFinishCertificateKankaiView from '../components/loggedInComponents/forms/kankai/BuildingFinishCertificateKankaiView.js';
import BuildingFinishCertificateIllamView from '../components/loggedInComponents/forms/illam/BuildingFinishCertificateIllamView.js';
import { isKankai, isIllam, isBirtamod, isSundarHaraicha, isInaruwa, isKamalamai, isBiratnagar, isKageyshowri } from '../utils/clientUtils';
import PuranoGharNirmanSampannaBirtamod from '../components/loggedInComponents/forms/birtamod/PuranoGharNirmanSampannaBirtamod.js';
import BuildingFinishViewBirtamod from '../components/loggedInComponents/forms/birtamod/BuildingFinishCertificateViewBirtamod.js';
import AllowancePaperBirtamod from '../components/loggedInComponents/forms/birtamod/AllowancePaperBirtamod.js';
import AllowancePaperViewKageyshowri from '../components/loggedInComponents/forms/kageyshowri/AllowancePaperViewKageyshowri';
import SuperstructureConstructionBirtamod from '../components/loggedInComponents/forms/birtamod/SuperstructureConstructionBirtamod.js';
import BuildingFinishViewSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/BuildingFinishCertificateViewSundarHaraicha.js';
import NoObjectionView from '../components/loggedInComponents/forms/otherForms/NoObjectionSheet.js';
import TallaThapIjajatRequestView from '../components/loggedInComponents/forms/sundarHaraicha/TallaThapIjajatRequest';
import TallaThapTippaniAdesh from '../components/loggedInComponents/forms/sundarHaraicha/TallaThapIjajatTippaniAdes.js';
import GharNakshaSurjaminSundarHaraichaView from '../components/loggedInComponents/forms/sundarHaraicha/GharNaksaSurjaminSundarHaraicha.js';
import TallaThapIjajatPatra from '../components/loggedInComponents/forms/sundarHaraicha/TallaThapIjajatPatra.js';
import AminKoPratibedan from '../components/loggedInComponents/forms/AminKoPratibedan.js';
import AllowancePaperSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/AllowancePaperSundarHaraicha.js';
import PlinthLevelOwnerSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/PlinthLevelOwnerSundarHaraicha.js';
import NamsariBuildFinishCertificate from '../components/loggedInComponents/forms/sundarHaraicha/NamsariBuildFinishCertificate.js';
import PlinthLevelTechAppSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/PlinthLevelTechAppSundarHaraicha.js';
import SuperStructureNoteOrderSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/SuperStructureNoteOrderSundarHaraicha';
import PuranoGharNirmanTippaniSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/PuranoGharNirmanTippaniSundarHaraicha.js';
import NaksaFilePathayekoBare from '../components/loggedInComponents/forms/NaksaFilePathayekoBare.js';
import LayoutGariPratibedanIjajat from '../components/loggedInComponents/forms/LayoutGariPratibedanIjajat.js';
import LayoutGariPratibedanRequest from '../components/loggedInComponents/forms/LayoutGariPratibedanRequest.js';
import DosrocharanAbedanSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/DosrocharanAbedanSundarHaraicha.js';
import DosrocharanAbedanKageyshowri from '../components/loggedInComponents/forms/kageyshowri/DosrocharanAbedanKageyshowri';
import SampannaPrabidhikPratibedanKageshowri from '../components/loggedInComponents/forms/kageyshowri/SampannaPrabidhikPratibedanKageshowri';
import SuperstructurePrabidhikPratibedanKageshowri from '../components/loggedInComponents/forms/kageyshowri/SuperstructurePrabidhikPratibedanKageshowri';
import CertificatePratibedanSambandhamaaKageshowri from '../components/loggedInComponents/forms/kageyshowri/CertificatePratibedanSambandhamaaKageshowri';
import GharNaksaSambandhamaaKageshowri from '../components/loggedInComponents/forms/kageyshowri/GharNaksaSambandhamaaKageshowri';
import DosrocharanSupervisorSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/DosrocharanSupervisorSundarHaraicha.js';
import SuperStructureNirmanKaryaSundarHaraicha from '../components/loggedInComponents/forms/sundarHaraicha/SuperStructureNirmanKaryaSundarHaraicha.js';
import SuperStructureNirmanKaryaKageyshowri from '../components/loggedInComponents/forms/kageyshowri/SuperStructureNirmanKaryaKageyshowri';
import NamsariBillVuktani from '../components/loggedInComponents/forms/NamsariBillVuktani.js';
import MyadAddRequest from '../components/loggedInComponents/forms/MyadAddRequest.js';
import MyadThapTippani from '../components/loggedInComponents/forms/MyadThapTippani.js';
import MyadThapApproval from '../components/loggedInComponents/forms/MyadThapApproval.js';
import PuranoNirmanSampannaInaruwa from './../components/loggedInComponents/forms/inaruwa/PuranoNirmanSampannaInaruwa';
import NamsariPramanpatraInaruwa from '../components/loggedInComponents/forms/inaruwa/NamsariPramanpatraInaruwa.js';
// import PahiloCharanBillVuktani from '../components/loggedInComponents/forms/PahiloCharanBillVuktani.js';
import BuildingFinishCertificateInaruwa from './../components/loggedInComponents/forms/inaruwa/BuildingFinishCertificateInaruwa';
import AllowancePaperInaruwa from './../components/loggedInComponents/forms/inaruwa/AllowancePaperInaruwa';
import NayaRecordPathayeko from '../components/loggedInComponents/forms/inaruwa/NayaRecordPathayeko.js';
import SansodhanBibaranPahiloInaruwa from '../components/loggedInComponents/forms/inaruwa/SansodhanBibaranPahiloInaruwa.js';
import NaksaFilePathayekoInaruwa from './../components/loggedInComponents/forms/inaruwa/NaksaFilePathayekoInaruwa';
import NirmanPramanPatraDharauti from '../components/loggedInComponents/forms/dharautiPhirtaForms/NayaNirmanPramanPatraDharauti';
import DharautiPhirtaGarneBare from '../components/loggedInComponents/forms/dharautiPhirtaForms/DharautiPhirtaGarneBare.js';
import GharNakshaSurjaminInaruwaView from '../components/loggedInComponents/forms/inaruwa/GharNaksaSurjaminInaruwa.js';
import DharautiFirtaTippaniAadesh from '../components/loggedInComponents/forms/inaruwa/DharautiFirtaTippaniAadesh.js';
import NeighboursNameApplicationInaruwa from './../components/loggedInComponents/forms/inaruwa/NeighboursNameApplicationInaruwa';
import NeighboursNameApplicationKageyshowri from './../components/loggedInComponents/forms/kageyshowri/NeighboursNameApplicationKageyshowri';
import AllowancePaperViewKamalamai from '../components/loggedInComponents/forms/kamalamai/AllowancePaperViewKamalamai.js';
import SuperstructureConstructionKamalamai from './../components/loggedInComponents/forms/kamalamai/SuperstructureConstructionKamalamai';
import ArchitectureDesignerManjuriPatra from '../components/loggedInComponents/forms/manjuriPatra/ArchitectureDesignerManjuriPatra.js';
import StructuralDesignerManjuriPatra from '../components/loggedInComponents/forms/manjuriPatra/StructuralDesignerManjuriPatra.js';
import NapiNirikshyanPratibedan from '../components/loggedInComponents/forms/NapiNirikshyanPratibedan.js';
import GharNakshaSurjaminKamalamaiView from '../components/loggedInComponents/forms/kamalamai/GharNaksaSurjaminKamalamai.js';
import PlinthLevelTechAppKamalamai from '../components/loggedInComponents/forms/kamalamai/PlinthLevelTechAppKamalamai.js';
import PlinthLevelTechAppKageyshowri from '../components/loggedInComponents/forms/kageyshowri/PlinthLevelTechAppKageyshowri';
import DosrocharanKaryaSampannaPrabidhikPratibedan from '../components/loggedInComponents/forms/kamalamai/DosrocharanKaryaSampannaPrabidhikPratibedan';
import SuperStructureNoteOrderKamalamaiView from '../components/loggedInComponents/forms/kamalamai/SuperStructureNoteOrderView.js';
import SuperStructureNoteOrderViewKageyshowri from '../components/loggedInComponents/forms/kageyshowri/SuperStructureNoteOrderViewKageyshowri';
import PrathamCharanAnumatiRequest from '../components/loggedInComponents/forms/biratnagar/PrathamCharanAnumatiRequest.js';
import NaksaPassCertificate from '../components/loggedInComponents/forms/biratnagar/NaksaPassCertificate.js';
import NoteOrderPlinthLevelBiratnagar from '../components/loggedInComponents/forms/biratnagar/NoteOrderPlinthLevelBiratnagar.js';
import NoteOrderPlinthLevelKageyshowri from '../components/loggedInComponents/forms/kageyshowri/NoteOrderPlinthLevelKageyshowri';
import LayoutGariPratibedanIjajatBiratnagar from '../components/loggedInComponents/forms/biratnagar/LayoutGariPratibedanIjajatBiratnagar.js';
import CertificatePratibedanSambandhama from '../components/loggedInComponents/forms/biratnagar/CertificatePratibedanSambandhama.js';
import SuperstructurePrabhidikPratibedanPesh from '../components/loggedInComponents/forms/biratnagar/SuperstructurePrabhidikPratibedanPesh.js';
import CertificatePratibedanPeshBare from '../components/loggedInComponents/forms/biratnagar/CertificatePratibedanPeshBare.js';
import { getComponent } from './routeUtils';
import FormToBeFilledByDesigner from '../components/loggedInComponents/forms/biratnagar/FormToBeFilledByDesigner.js';
import SuperStructureSupervisionReport from '../components/loggedInComponents/forms/supervisionReports/SuperStructureSupervisionReport.js';
import SansodhitSuperStructureIjajatSambandhama from '../components/loggedInComponents/forms/kamalamai/SansodhitSuperStructureIjajatSambandhama.js';
import CertificateSupervisionReport from '../components/loggedInComponents/forms/supervisionReports/CertificateSupervisionReport.js';
import EarthquakeSafetyNoObjectionSheet from '../components/loggedInComponents/forms/EarthquakeSafetyNoObjectionSheet.js';
import StructureDesignClassBBiratnagar from '../components/loggedInComponents/forms/biratnagar/StructureDesignClassBBiratnagar.js';
import ForHighRisedBuilding from '../components/loggedInComponents/forms/biratnagar/high_rised_building/ForHighRisedBuilding.js';
import ApplicableForMultiStoreyForensicEngineering from '../components/loggedInComponents/forms/biratnagar/ApplicableForMultiStorey/ApplicableForMultiStoreyForensicEngineering.js';
import NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaram from '../components/loggedInComponents/forms/biratnagar/NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaram.js';
import SuchanaTasGariMuchulkaPathaidina from '../components/loggedInComponents/forms/biratnagar/SuchanaTasGariMuchulkaPathaidina.js';
import SurjaminMuchulkaViewBiratnagar from '../components/loggedInComponents/forms/biratnagar/SurjaminMuchulkaViewBiratnagar.js';
import SurjaminMuchulkaViewKageyshowri from '../components/loggedInComponents/forms/kageyshowri/SurjaminMuchulkaViewKageyshowri';
import LayoutTathaSarjminGariFileFirta from '../components/loggedInComponents/forms/biratnagar/LayoutTathaSarjminGariFileFirta.js';
import LandAreaCalculationOfSitePlan from '../components/loggedInComponents/forms/biratnagar/LandAreaCalculationOfSitePlan.js';
import GharNakshaNamsariSambandhama from '../components/loggedInComponents/forms/biratnagar/GharNakshNamsariSambandhama.js';
import KabuliuatnamaBiratnagarView from '../components/loggedInComponents/forms/biratnagar/KabuliuatnamaBiratnagarView.js';
import Notice15DaysKaryalaPrayojan from '../components/loggedInComponents/forms/biratnagar/Notice15DaysKaryalayaPrayojan.js';
import SansodhitSuperStructureIjajatSambandhamaBiratnagar from '../components/loggedInComponents/forms/biratnagar/SansodhitSuperStructureIjajatSambandhamaBiratnagar.js';
import SuperStructureConstructionBiratnagar from '../components/loggedInComponents/forms/biratnagar/SuperStructureConstructionBiratnagar.js';
import SuperStructureConstructionKageyshowri from '../components/loggedInComponents/forms/kageyshowri/SuperStructureConstructionKageyshowri';
import SamsodhitSuperStructurePratibedanBiratnagar from '../components/loggedInComponents/forms/biratnagar/SamsodhitSuperStructurePratibedanBiratnagar.js';
import NoObjectionCertificateBiratnagar from '../components/loggedInComponents/forms/biratnagar/NoObjectionCertificateBiratnagar.js';
import SthalgatNirikshyanGariPeshBiratnagar from '../components/loggedInComponents/forms/biratnagar/SthalgatNirikshyanGariPeshBiratnagar.js';
import SthalgatNirikshyanGariPeshKageyshowri from '../components/loggedInComponents/forms/kageyshowri/SthalgatNirikshyanGariPeshKageyshowri';
import StructureDesignClassCFormBiratnagar from '../components/loggedInComponents/forms/biratnagar/StructureDesignClassCFormBiratnagar/StructureDesignClassCFormBiratnagar.js';
import BuildingFinishCertificateViewBiratnagar from '../components/loggedInComponents/forms/biratnagar/BuildingFinishCertificateViewBiratnagar.js';
import BuildingFinishCertificateKageyshowri from '../components/loggedInComponents/forms/kageyshowri/BuildingFinishCertificateKageyshowri';
import NamsariHistoryAppilcation from '../components/loggedInComponents/forms/NamsariHistoryApplication.js';

const navigationItem = mainSideBar_menu;

const dashboardRoutes = [
	{
		path: '/forms/map-permit-application-edit',
		name: navigationItem.menu_item1,
		component: MapPermintApplicationEdit,
		layout: '/user',
	},
	{
		path: '/forms/map-permit-application-view',
		name: navigationItem.menu_item31,
		component: MapPermintApplicationView,
		layout: '/user',
	},
	{
		path: '/forms/floor-details',
		name: navigationItem.menu_item2,
		component: FloorDetails,
		layout: '/user',
	},
	{
		// name: navigationItem.menu_itemHasChild.menu_item,
		// hasChild: true,
		// layout: '/user',
		// children: [
		//   {
		path: '/forms/design-approval',
		name: navigationItem.menu_itemHasChild.menu_item,
		component: DesignApproval,
		layout: '/user',
		//   }
		// ]
	},
	{
		name: navigationItem.menu_itemHasChild_2.menu_item1,
		hasChild: true,
		layout: '/user',
		children: [
			{
				path: '/forms/architecture-design-classB-form',
				name: navigationItem.menu_itemHasChild_2.hasChild1.child_item1,
				component: ArchitecturalDesignRequirementsClassBForm,
				layout: '/user',
			},
			{
				path: '/forms/structure-design-classB-form',
				name: navigationItem.menu_itemHasChild_2.hasChild1.child_item2,
				component: isBiratnagar ? StructureDesignClassBBiratnagar : StructureDesignClassBForm,
				layout: '/user',
			},
		],
	},
	{
		name: navigationItem.menu_itemHasChild_2.menu_item2,
		hasChild: true,
		layout: '/user',
		children: [
			{
				path: '/forms/architecture-design-classc-form',
				name: navigationItem.menu_itemHasChild_2.hasChild2.child_item1,
				component: ArchitectureDesignClassCForm,
				layout: '/user',
			},
			{
				path: '/forms/structure-design-classc-form',
				name: navigationItem.menu_itemHasChild_2.hasChild2.child_item2,
				component: isBiratnagar ? StructureDesignClassCFormBiratnagar : StructureDesignClassCForm,
				layout: '/user',
			},
		],
	},
	{
		path: '/forms/electrical-design',
		name: navigationItem.menu_item33,
		component: ElectricalDesign,
		layout: '/user',
	},
	{
		path: '/forms/sanitary-design',
		name: navigationItem.menu_item34,
		component: SanitaryPlumbingDesignRequirements,
		layout: '/user',
	},
	{
		path: '/forms/architecture-designer-manjuri-patra',
		name: navigationItem.menu_archi_design_manjuri,
		component: ArchitectureDesignerManjuriPatra,
		layout: '/user',
	},
	{
		path: '/forms/structural-designer-manjuri-patra',
		name: navigationItem.menu_structure_design_manjuri,
		component: StructuralDesignerManjuriPatra,
		layout: '/user',
	},
	{
		path: '/forms/anusuchiga-view',
		name: navigationItem.menu_item3,
		component: Anushuchiga,
		layout: '/user',
	},
	{
		path: '/forms/anusuchigha-view',
		name: navigationItem.menu_item4,
		component: Anushuchigha,
		layout: '/user',
	},
	{
		path: '/forms/map-technical-description-view',
		name: navigationItem.menu_item5,
		component: MapTechnicalDescriptionView,
		layout: '/user',
	},
	{
		path: '/forms/map-check-report-view',
		name: navigationItem.menu_item6,
		component: isKageyshowri ?  MapCheckReportKageyshowriView : MapCheckReportView,
		layout: '/user',
	},
	{
		path: '/forms/no-objection-sheet',
		name: navigationItem.menu_item6,
		component: NoObjectionView,
		layout: '/user',
	},
	{
		path: '/forms/rajaswo-detail-view',
		name: navigationItem.menu_item7,
		component: RajaswoDetailView,
		layout: '/user',
	},
	{
		path: '/forms/rajaswo-voucher',
		name: navigationItem.menu_item7_1,
		component: RajaswoVoucher,
		layout: '/user',
	},

	{
		path: '/forms/notice-period-for-15-days-view',
		name: navigationItem.menu_item8,
		component: isInaruwa || isBiratnagar ? NeighboursNameApplicationInaruwa : isKageyshowri ? NeighboursNameApplicationKageyshowri : NeighboursNameApplicationView,
		layout: '/user',
	},
	{
		path: '/forms/notice-payment-application-view',
		name: navigationItem.menu_item9,
		component: NoticePaymentApplicationView,
		layout: '/user',
	},
	{
		path: '/forms/surjamin-muchulka-view',
		name: navigationItem.menu_item10,
		component: isBiratnagar ? SurjaminMuchulkaViewBiratnagar : isKageyshowri ? SurjaminMuchulkaViewKageyshowri : SurjaminMuchulkaView,
		layout: '/user',
	},
	{
		path: '/forms/layout-tatha-sarjmin-gari-file-firta',
		name: navigationItem.menu_itemDefault,
		component: LayoutTathaSarjminGariFileFirta,
		layout: '/user',
	},
	{
		path: '/forms/land-area-calculation-of-site-plan',
		name: navigationItem.menu_itemDefault,
		component: LandAreaCalculationOfSitePlan,
		layout: '/user',
	},
	{
		path: FormUrl.GHAR_NAKSA_SURJAMIN,
		name: navigationItem.menu_item11,
		component: isSundarHaraicha
			? GharNakshaSurjaminSundarHaraichaView
			: isInaruwa
			? GharNakshaSurjaminInaruwaView
			: isKamalamai
			? GharNakshaSurjaminKamalamaiView
			: GharNakshaSurjaminMuchulkaView,
		layout: '/user',
	},
	{
		path: '/forms/ghar-compoundwallko-naap',
		name: navigationItem.menu_item36,
		component: GharCompondWallNaap,
		layout: '/user',
	},
	{
		path: '/forms/ghar-naksako-kabuliyatnama',
		name: navigationItem.menu_item31,
		component: GharNaksakoKabuliyatnama,
		layout: '/user',
	},
	{
		path: '/forms/aminko-pratibedan',
		name: navigationItem.menu_item32,
		component: AminKoPratibedan,
		layout: '/user',
	},
	{
		path: '/forms/aminko-sthalgat-pratibedan',
		name: navigationItem.menu_item32,
		component: isKamalamai ? NapiNirikshyanPratibedan : AminSthalgatPratibedan,
		layout: '/user',
	},
	{
		path: '/forms/prabhidik-pratibedhan-pesh-view',
		name: navigationItem.menu_item12,
		component: isKankai || isBirtamod || isSundarHaraicha || isKamalamai || isKageyshowri ? PrabhidikPratibedhanKankai : PrabhidikPratibedhanPeshView,
		layout: '/user',
	},
	{
		path: FormUrl.NOTE_ORDER_PLINTH_LEVEL,
		name: navigationItem.menu_item13,
		component: isKankai ? NoteOrderPlinthLevelKankaiView : isBiratnagar ? NoteOrderPlinthLevelBiratnagar : isKageyshowri ? NoteOrderPlinthLevelKageyshowri : NoteOrderPilenthLevelView,
		// component: NoteOrderPilenthLevelView,
		layout: '/user',
	},
	{
		path: '/forms/allowance-paper-view',
		name: navigationItem.menu_item14,
		component: isBirtamod
			? AllowancePaperBirtamod
			: isSundarHaraicha
			? AllowancePaperSundarHaraicha
			: isInaruwa
			? AllowancePaperInaruwa
			: isKamalamai
			? AllowancePaperViewKamalamai
			: isKageyshowri
			? AllowancePaperViewKageyshowri
			: AllowancePaperView,
		layout: '/user',
	},
	{
		path: '/forms/super-structure-build-view',
		name: navigationItem.menu_item15,
		component: SuperStructureBuildView,
		layout: '/user',
	},
	{
		path: '/forms/talla-thap-ijajat-request',
		name: navigationItem.menu_item15,
		component: TallaThapIjajatRequestView,
		layout: '/user',
	},
	{
		path: '/forms/plinthLevel-OwnerRepresentation-view',
		name: navigationItem.menu_item16,
		component: isSundarHaraicha ? PlinthLevelOwnerSundarHaraicha : PlinthLevelOwnerRepresentationView,
		layout: '/user',
	},
	{
		path: '/forms/plinthLevel-techapplication-view',
		name: navigationItem.menu_item17,
		component: 
			isSundarHaraicha 
			? PlinthLevelTechAppSundarHaraicha 
			: isKamalamai 
			? PlinthLevelTechAppKamalamai
			: PlinthLevelTechApplicationView,
		layout: '/user',
	},
	{
		path: '/forms/superStructure-noteOrder-view',
		name: navigationItem.menu_item18,
		component: isSundarHaraicha
			? SuperStructureNoteOrderSundarHaraicha
			: isKamalamai
			? SuperStructureNoteOrderKamalamaiView
			: isKageyshowri
			? SuperStructureNoteOrderViewKageyshowri
			: SuperStructureNoteOrderView,
		layout: '/user',
	},
	{
		path: '/forms/talla-thap-ijajat-tippani-ades',
		name: navigationItem.menu_item18,
		component: TallaThapTippaniAdesh,
		layout: '/user',
	},
	{
		path: '/forms/superStructure-construction-view',
		name: navigationItem.menu_item19,
		component: isBirtamod
			? SuperstructureConstructionBirtamod
			: isKamalamai
			? SuperstructureConstructionKamalamai
			: isBiratnagar
			? SuperStructureConstructionBiratnagar
			: isKageyshowri
			? SuperStructureConstructionKageyshowri
			: SuperstructureConstructionView,
		layout: '/user',
	},
	{
		path: '/forms/talla-thap-ijajat-patra',
		name: navigationItem.menu_item19,
		component: TallaThapIjajatPatra,
		layout: '/user',
	},
	{
		path: '/forms/sampanna-prabidhik-pratibedan',
		name: navigationItem.menu_item51,
		component: SampannaPrabidhikPratibedanKageshowri,
		layout: '/user',
	},
	{
		path: '/forms/superstructure-prabidhik-pratibedan',
		name: navigationItem.menu_item52,
		component: SuperstructurePrabidhikPratibedanKageshowri,
		layout: '/user',
	},
	{
		path: '/forms/ghar-naksa-sambandhamaa',
		name: navigationItem.menu_item54,
		component: GharNaksaSambandhamaaKageshowri,
		layout: '/user',
	},
	{
		path: '/forms/dosrocharan-abedan',
		name: navigationItem.menu_item20,
		component: isSundarHaraicha ? DosrocharanAbedanSundarHaraicha : isKageyshowri ? DosrocharanAbedanKageyshowri : DosrocharanAbedan,
		layout: '/user',
	},
	{
		path: '/forms/dosrocharan-supervisor',
		name: navigationItem.menu_item21,
		component: isSundarHaraicha ? DosrocharanSupervisorSundarHaraicha : DosrocharanSupervisor,
		layout: '/user',
	},
	{
		path: '/forms/dosrocharan-prabidhik-view',
		name: navigationItem.menu_item22,
		component: isKankai ? DosrocharanPrabidhikKankaiView : DosrocharanPrabidhikView,
		// component: DosrocharanPrabidhikView,
		layout: '/user',
	},
	{
		path: '/forms/certificate-instruction-view',
		name: navigationItem.menu_item23,
		component: CertificateInstructionView,
		layout: '/user',
	},
	{
		path: '/forms/building-finish-certificate-view',
		name: navigationItem.menu_item24,
		component: isIllam
			? BuildingFinishCertificateIllamView
			: isKankai
			? BuildingFinishCertificateKankaiView
			: isBirtamod
			? BuildingFinishViewBirtamod
			: isSundarHaraicha
			? BuildingFinishViewSundarHaraicha
			: isInaruwa
			? BuildingFinishCertificateInaruwa
			: isBiratnagar
			? BuildingFinishCertificateViewBiratnagar
			: isKageyshowri
			? BuildingFinishCertificateKageyshowri
			: BuildingFinishCertificateView,
		// component: BuildingFinishCertificateView,

		layout: '/user',
	},
	{
		path: '/forms/certificate-note',
		name: navigationItem.menu_item25,
		component: isBiratnagar ? GharNakshaNamsariSambandhama : CertificateNoteView,
		layout: '/user',
	},
	{
		path: '/forms/namsari-tippani-ades-view',
		name: navigationItem.menu_item26,
		component: isBiratnagar ? CertificateNoteView : NamsariTippaniAdesView,
		layout: '/user',
	},
	{
		path: '/forms/namsari-bill-vuktani',
		name: navigationItem.menu_item26,
		component: NamsariBillVuktani,
		layout: '/user',
	},
	{
		path: '/forms/building-build-certificate-view',
		name: navigationItem.menu_item27,
		component: isSundarHaraicha ? NamsariBuildFinishCertificate : isInaruwa ? NamsariPramanpatraInaruwa : BuildingBuildCertificateView,
		layout: '/user',
	},
	{
		path: '/forms/manjurinama-view',
		name: navigationItem.menu_item28,
		component: ManjurinamaView,
		layout: '/user',
	},
	{
		path: '/forms/waresnama-view',
		name: navigationItem.menu_item29,
		component: WarersnamaView,
		layout: '/user',
	},
	{
		path: '/forms/notice-15-days-karyalaya-prayojan',
		name: navigationItem.menu_item30,
		component: Notice15DaysKaryalaPrayojan,
		layout: '/user',
	},
	{
		path: '/forms/kabuliyatnama-view',
		name: navigationItem.menu_item30,
		component: isBiratnagar ? KabuliuatnamaBiratnagarView : KabuliyatnamaView,
		layout: '/user',
	},
	{
		path: '/forms/forward-to-next',
		name: navigationItem.menu_item_forward_to_next,
		component: ForwardToNextForm,
		layout: '/user',
	},
	{
		path: '/forms/sansodhan-super-structure-ijjaat',
		name: navigationItem.menu_item35,
		component: isBiratnagar ? SansodhitSuperStructureIjajatSambandhamaBiratnagar : SansodhanSuperStructure,
		layout: '/user',
	},
	{
		path: '/forms/sansodhan-gari-nibedan',
		name: navigationItem.menu_item38,
		component: SansodhanGariNibedan,
		layout: '/user',
	},
	{
		path: FormUrl.SANSODHAN_TIPPANI,
		name: navigationItem.menu_item39,
		component: getComponent(FormUrl.SANSODHAN_TIPPANI),
		layout: '/user',
	},
	{
		path: '/forms/sansodhan-bibaran-bill-bhuktani',
		name: navigationItem.menu_item44,
		component: SansodhanGariNibedan,
		layout: '/user',
	},
	{
		path: '/forms/samsodhit-Super-structure-Ijajat-Pratibedan',
		name: navigationItem.menu_item37,
		component: isBiratnagar ? SamsodhitSuperStructurePratibedanBiratnagar 
		: isKageyshowri 
		? PlinthLevelTechAppKageyshowri	
		: SamsodhitSuperStructurePratibedan,
		layout: '/user',
	},
	{
		path: FormUrl.SANSODHAN_BIBARAN_PAHILO,
		name: navigationItem.menu_item40,
		component: isInaruwa ? SansodhanBibaranPahiloInaruwa : SansodhanBibaranPahilo,
		layout: '/user',
	},
	{
		path: '/forms/sansodhanko-tippani-adesh',
		name: navigationItem.menu_item41,
		component: getComponent(FormUrl.SANSODHANKO_TIPPANI_ADES),
		layout: '/user',
	},
	{
		path: FormUrl.SANSODHAN_SUPER_STRUCTURE,
		name: navigationItem.menu_item42,
		component: SuperStructureKoSansodhanBiberan,
		layout: '/user',
	},
	{
		path: FormUrl.SANSODHAN_SUPER_TIPPANI_ADESH,
		name: navigationItem.menu_item43,
		// component: isInaruwa ? SansodhanSuperTippaniInaruwa : SansodhanSuperTippaniAdesh,
		component: getComponent(FormUrl.SANSODHAN_SUPER_TIPPANI_ADESH),
		layout: '/user',
	},
	{
		path: FormUrl.DOSRO_CHARAN_BILL_VUKTANI,
		name: navigationItem.menu_item48,
		component: DosroCharanBillVuktani,
		layout: '/user',
	},
	{
		path: '/forms/plinthLevel-tech-view',
		name: navigationItem.menu_item44,
		component: PlinthLevelTechView,
		layout: '/user',
	},
	{
		path: FormUrl.PAHILO_CHARAN_BILL_VUKTANI,
		name: navigationItem.menu_item47,
		component: PahiloCharanBillVuktani,
		layout: '/user',
	},
	{
		path: "sampanna-bill-vuktani",
		name: navigationItem.menu_item55,
		component: PahiloCharanBillVuktani,
		layout: '/user',
	},
	{
		path: FormUrl.SAMSODHAN_BILL_VUKTANI,
		name: navigationItem.menu_item46,
		component: SamsodhanBillVuktani,
		layout: '/user',
	},
	{
		path: FormUrl.PURANO_GHAR_NIRMAN_TIPPANI,
		name: navigationItem.menu_item49,
		component: isSundarHaraicha ? PuranoGharNirmanTippaniSundarHaraicha : PuranoGharNirmanTippani,
		layout: '/user',
	},
	{
		path: '/forms/purano-ghar-sampanna-pramanpatra',
		name: navigationItem.menu_item50,
		component: isKankai
			? PuranoGharNirmanSampannaKankai
			: isBirtamod
			? PuranoGharNirmanSampannaBirtamod
			: isInaruwa
			? PuranoNirmanSampannaInaruwa
			: PuranoGharSampannaPramanPatra,
		// component: PuranoGharSampannaPramanPatra,
		layout: '/user',
	},
	{
		path: '/forms/superstructureko-nirmaan-karya',
		name: navigationItem.menu_item51,
		component: isSundarHaraicha 
			? SuperStructureNirmanKaryaSundarHaraicha 
			: isKageyshowri 
			? SuperStructureNirmanKaryaKageyshowri 
			: SuperStructurekoNirmaanKarya,
		layout: '/user',
	},
	{
		path: FormUrl.NAYA_RECORD_PATHAYEKO_BARE,
		name: navigationItem.naya_record_pathayeko,
		component: NayaRecordPathayeko,
		layout: '/user',
	},
	{
		path: FormUrl.DHARAUTI_FIRTA_TIPPANI_AADESH,
		name: navigationItem.dharauti_phirta_tippani,
		component: DharautiFirtaTippaniAadesh,
		layout: '/user',
	},
	{
		path: FormUrl.NAKSA_FILE_PATHAYEKO_BARE,
		name: navigationItem.menu_item51,
		component: isInaruwa || isBiratnagar ? NaksaFilePathayekoInaruwa : NaksaFilePathayekoBare,
		layout: '/user',
	},
	{
		path: FormUrl.NIRMAN_PRAMANPATRA_DHARAUTI,
		name: navigationItem.menu_item46,
		component: isBiratnagar ? NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaram : NirmanPramanPatraDharauti,
		layout: '/user',
	},
	{
		path: FormUrl.DHARAUTI_RAKAM_PHIRTA,
		name: navigationItem.dharauti_phirta,
		component: DharautiPhirtaGarneBare,
		layout: '/user',
	},
	{
		path: FormUrl.LAYOUT_GARI_PRATIBEDAN_IJAJAT,
		name: navigationItem.menu_item51,
		component: isBiratnagar ? LayoutGariPratibedanIjajatBiratnagar : LayoutGariPratibedanIjajat,
		layout: '/user',
	},
	{
		path: FormUrl.LAYOUT_GARI_PRATIBEDAN_REQUEST,
		name: navigationItem.menu_item51,
		component: LayoutGariPratibedanRequest,
		layout: '/user',
	},
	{
		// Not in use, Not Recorded in Backend FormNameMasterk
		path: FormUrl.NAPI_NIRIKSHYAN_PRATIBEDAN,
		name: navigationItem.menu_napi_nirikshyan_pratibedan,
		component: NapiNirikshyanPratibedan,
		layout: '/user',
	},
	{
		// Added as extra dosrocharan-prabidhik-view for kamalamai
		path: FormUrl.DOSRO_CHARAN_SAMPANNAKO_PRABIDHIKKO_PRATIBEDAN,
		name: navigationItem.menu_dosro_charan_kamalamai_prabidhik,
		component: DosrocharanKaryaSampannaPrabidhikPratibedan,
		layout: '/user',
	},
	{
		path: FormUrl.PRATHAM_CHARAN_ANUMATI_REQUEST,
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: PrathamCharanAnumatiRequest,
		layout: '/user',
	},
	{
		path: FormUrl.NAKSA_PASS_CERTIFICATE,
		name: navigationItem.menu_naksa_pass_certificate,
		component: NaksaPassCertificate,
		layout: '/user',
	},
	{
		path: FormUrl.CERTIFICATE_PRATIBEDAN_SAMBANDHAMA,
		name: navigationItem.menu_naksa_pass_certificate,
		component: isKageyshowri ? CertificatePratibedanSambandhamaaKageshowri : CertificatePratibedanSambandhama,
		layout: '/user',
	},
	{
		path: FormUrl.SUPERSTRUCTURE_PRABHIDIK_PRATIBEDAN_PESH,
		name: navigationItem.menu_naksa_pass_certificate,
		component: SuperstructurePrabhidikPratibedanPesh,
		layout: '/user',
	},
	{
		path: FormUrl.SUCHANA_TAS_GARI_MUCHULKA_PATHAIDINA,
		name: navigationItem.menu_suchana_tas_gari_muchulka_pathaidina,
		component: SuchanaTasGariMuchulkaPathaidina,
		layout: '/user',
	},
	{
		path: FormUrl.CERTIFICATE_PRATIBEDAN_PESH_BARE,
		name: navigationItem.menu_naksa_pass_certificate,
		component: CertificatePratibedanPeshBare,
		layout: '/user',
	},
	{
		path: '/forms/super-structure-supervision-report',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: SuperStructureSupervisionReport,
		layout: '/user',
	},
	{
		path: '/forms/certificate-supervision-report',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: CertificateSupervisionReport,
		layout: '/user',
	},
	{
		path: '/forms/sansodhit-super-structure-ijajat-sambandhama',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: SansodhitSuperStructureIjajatSambandhama,
		layout: '/user',
	},
	{
		path: '/forms/form-to-be-filled-by-designer',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: FormToBeFilledByDesigner,
		layout: '/user',
	},
	{
		path: '/forms/earthquake-safety-no-objection-sheet',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: EarthquakeSafetyNoObjectionSheet,
		layout: '/user',
	},
	{
		path: '/forms/no-objection-certificate',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: NoObjectionCertificateBiratnagar,
		layout: '/user',
	},
	{
		path: '/forms/sthalgat-nirikshyan-gari-pesh',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: isKageyshowri ? SthalgatNirikshyanGariPeshKageyshowri : SthalgatNirikshyanGariPeshBiratnagar,
		layout: '/user',
	},
	{
		path: '/forms/for-high-rised-buildings',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: ForHighRisedBuilding,
		layout: '/user',
	},
	{
		path: '/forms/for-multi-storey',
		name: navigationItem.menu_pratham_charan_anumati_request,
		component: ApplicableForMultiStoreyForensicEngineering,
		layout: '/user',
	},
	{
		path: '/forms/file-upload',
		name: 'File Upload',
		component: FileUpload,
		layout: '/user',
	},
	{
		path: '/forms/file-list',
		name: 'File List',
		component: ListFiles,
		layout: '/user',
	},
	{
		path: '/forms/myadThap-request',
		name: navigationItem.menu_item45,
		component: MyadAddRequest,
		layout: '/user',
	},
	{
		path: '/forms/myadThap-tippani',
		name: navigationItem.menu_item46,
		component: MyadThapTippani,
		layout: '/user',
	},
	{
		path: '/forms/myadThap-approval',
		name: navigationItem.menu_item46,
		component: MyadThapApproval,
		layout: '/user',
	},
	{
		path: '/forms/namsari-history',
		name: navigationItem.menu_item46,
		component: NamsariHistoryAppilcation,
		layout: '/user',
	},
	// {
	//   path: '/forms/file-list/:id',
	//   name: 'File List',
	//   component: ListFiles,
	//   layout: '/user'
	// },
];

export default dashboardRoutes;
