import React, { Component, ReactComponentElement } from 'react';
import { FormUrl } from "../utils/enums/url"
import { isSundarHaraicha, isInaruwa, isKamalamai } from "../utils/clientUtils"

const SansodhanTippaniPahiloCharanKamalamai = React.lazy(() => import('../components/loggedInComponents/forms/kamalamai/SansodhanTippaniPahiloCharanKamalamai'));
const SansodhanTippaniDosroCharanKamalamai = React.lazy(() => import('../components/loggedInComponents/forms/kamalamai/SansodhanTippaniDosroCharanKamalamai'));
const SansodhanSuperTippaniInaruwa = React.lazy(() => import('../components/loggedInComponents/forms/inaruwa/SansodhanSuperTippaniInaruwa'));
const SansodhanKoTippaniAdesh = React.lazy(() => import('../components/loggedInComponents/forms/SansodhanKoTippaniAdesh'));
const SansodhanSuperTippaniAdesh = React.lazy(() => import('../components/loggedInComponents/forms/SansodhanSuperTippaniAdesh'));

const SansodhanTippaniKamalamai = React.lazy(() => import('../components/loggedInComponents/forms/kamalamai/SansodhanTippaniKamalamai'));
const SansodhanTippani = React.lazy(() => import('../components/loggedInComponents/forms/SansodhanTippani'));

const GharNakshaSurjaminSundarHaraichaView = React.lazy(() => import("../components/loggedInComponents/forms/sundarHaraicha/GharNaksaSurjaminSundarHaraicha"));
const GharNakshaSurjaminInaruwaView = React.lazy(() => import("../components/loggedInComponents/forms/inaruwa/GharNaksaSurjaminInaruwa"));
const GharNakshaSurjaminMuchulkaView = React.lazy(() => import("../components/loggedInComponents/forms/GharNakshaSurjaminMuchulkaView"));
const GharNakshaSurjaminKamalamaiView = React.lazy(() => import("../components/loggedInComponents/forms/kamalamai/GharNaksaSurjaminKamalamai"));

let SansodhanTippaniComponent = SansodhanTippani;
let SansodhanKoTippaniComponent1 = SansodhanKoTippaniAdesh;
let SansodhaSuperTippaniComponent = SansodhanSuperTippaniAdesh;
let GharNaksaSurjaminComponent = GharNakshaSurjaminMuchulkaView;

if (isKamalamai) {
    SansodhanTippaniComponent = SansodhanTippaniKamalamai;
    SansodhanKoTippaniComponent1 = SansodhanTippaniPahiloCharanKamalamai;
    SansodhaSuperTippaniComponent = SansodhanTippaniDosroCharanKamalamai;
    GharNaksaSurjaminComponent = GharNakshaSurjaminKamalamaiView;
} else if (isInaruwa) {
    SansodhaSuperTippaniComponent = SansodhanSuperTippaniInaruwa;
    GharNaksaSurjaminComponent = GharNakshaSurjaminInaruwaView;
} else if (isSundarHaraicha) {
    GharNaksaSurjaminComponent = GharNakshaSurjaminSundarHaraichaView;
}

export const getComponent = (formUrl: FormUrl) => {

    if (formUrl === FormUrl.GHAR_NAKSA_SURJAMIN) {
        return GharNaksaSurjaminComponent;
    } else if (formUrl === FormUrl.SANSODHAN_TIPPANI) {
        return SansodhanTippaniComponent;
    } else if (formUrl === FormUrl.SANSODHANKO_TIPPANI_ADES) {
        return SansodhanKoTippaniComponent1;
    } else if (formUrl === FormUrl.SANSODHAN_SUPER_TIPPANI_ADESH) {
        return SansodhaSuperTippaniComponent;
    } else return null;

}