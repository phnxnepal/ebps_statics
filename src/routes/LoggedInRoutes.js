import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Dashboard from '../components/Dashboard';
import HeaderComponent from '../components/loggedInComponents/HeaderComponent';
import Footer from '../components/loggedInComponents/Footer';
// import Profile from '../components/loggedInComponents/Profile';
// import History from '../components/loggedInComponents/allHistory';
// import Comment from '../components/loggedInComponents/comment';
// import Changepass from './../components/loggedInComponents/changePassword';
import PrintAll from '../components/loggedInComponents/PrintAll';
// import BuildingPermit from './../components/BuildingPermit';
import MainSideBar from './../components/loggedInComponents/sideBar';
import dashboardRoutes from './dashboardRoutes';
import { MENU_LIST } from '../utils/constants';
import { isStringEmpty } from '../utils/stringUtils';
import { getTempUrls } from '../utils/config';
import Tasks from '../components/Tasks';
// import Report from '../components/Report';
import { getLocalStorage } from '../utils/secureLS';
import { dynamicUrlFilter } from '../utils/urlUtils';
import { connect } from 'react-redux';
// import ReportList from './../components/ReportList';
// import CompletedList from '../components/CompletedList';
// import ThapTallaGhar from '../components/ThapTallaGhar';
// import Authorization from './../utils/Authorization';
import ActivityLog from '../components/loggedInComponents/ActivityLog';
import TaskSearch from '../components/TaskSearch';
import { LSKey } from '../utils/enums/localStorageKeys';
// import { TallaThapSetup } from '../components/ebps-setup/TallaThapSetup';
// import { NamsariSetup } from '../components/ebps-setup/NamsariSetup';
// import { DiscardSetup } from '../components/ebps-setup/DiscardSetup';
// import { MyadSetup } from '../components/ebps-setup/MyadSetup';
import { ProcessMenuObject, MenuId } from '../utils/menuUtils';
// import { MyadApproveSetup } from './../components/ebps-setup/MyadApproveSetup';
import { ErrorBoundary } from '../components/shared/ErrorBoundary';

const Profile = React.lazy(() => import('./../components/loggedInComponents/Profile'));
const History = React.lazy(() => import('./../components/loggedInComponents/allHistory'));
const Comment = React.lazy(() => import('./../components/loggedInComponents/comment'));
const Changepass = React.lazy(() => import('./../components/loggedInComponents/changePassword'));

const BuildingPermit = React.lazy(() => import('./../components/BuildingPermit'));
const CompletedList = React.lazy(() => import('./../components/CompletedList'));
const ThapTallaGhar = React.lazy(() => import('./../components/ThapTallaGhar'));
const ReportList = React.lazy(() => import('./../components/ReportList'));
const Report = React.lazy(() => import('./../components/Report'));

const MyadApproveSetup = React.lazy(() => import('./../components/ebps-setup/MyadApproveSetup'));
const MyadSetup = React.lazy(() => import('./../components/ebps-setup/MyadSetup'));
const NamsariSetup = React.lazy(() => import('./../components/ebps-setup/NamsariSetup'));
const DiscardSetup = React.lazy(() => import('./../components/ebps-setup/DiscardSetup'));
const TallaThapSetup = React.lazy(() => import('./../components/ebps-setup/TallaThapSetup'));

const exclude = [
	'/user/application-list',
	'/user/task-list',
	'/user/report',
	'/user/building-permit',
	'/user/building-permit-purano',
	'/user/profile',
	'/user/change-password',
	'/user/',
	'/user',
	'/user/printall',
	'/user/completed-list',
	ProcessMenuObject[MenuId.ADD_STOREY_ADD].url,
	ProcessMenuObject[MenuId.DISCARD_ADD].url,
	ProcessMenuObject[MenuId.NAMSARI_ADD].url,
	ProcessMenuObject[MenuId.MYAD_ADD].url,
	ProcessMenuObject[MenuId.MYAD_APPROVE].url,
];

// const Admin = Authorization(['A', 'C']);

const idExclusionUrls = ['/user/report-list', '/user/building-permit-tallathap', '/user/task-search'];

const needsSidebar = (url) => {
	const inIdUrl = idExclusionUrls.some((row) => url.match(new RegExp(row, 'gi')));
	const inExclude = exclude.includes(url);
	return !(inIdUrl || inExclude);
};

class LoggenInRoutes extends Component {
	state = {
		isSidebarToggle: true,
		searchString: '',
	};

	// componentDidMount() {

	//   console.log('menu did mount', this.props.menu);
	//   if (this.props.menu) {
	//     this.setState({ menu: this.props.menu })
	//   }
	// }

	// componentDidUpdate() {
	//   console.log('menu did update', this.props.menu);
	// }

	getRoutes = (dashboardRoutes) => {
		return dashboardRoutes.map((prop, key) => {
			if (prop.layout === '/user') {
				if (prop.hasChild) {
					return prop.children.map((child, key) => <Route path={`${child.layout}${child.path}`} component={child.component} key={key} />);
				} else {
					return <Route path={`${prop.layout}${prop.path}`} component={prop.component} key={key} />;
				}
			} else {
				return null;
			}
		});
	};

	handleSidebarToggle = () => this.setState((prev) => ({ isSidebarToggle: !prev.isSidebarToggle }));

	handleSearch = (e) => this.setState({ searchString: e.target.value });

	render() {
		const { isSidebarToggle } = this.state;

		const showSidebar = isSidebarToggle && needsSidebar(this.props.location.pathname);

		let requiredRoutes = [];

		let sidebarRoutes = [];

		//get all menu sent from backend
		// const menu =
		//   !isStringEmpty(getLocalStorage(MENU_LIST)) &&
		//   JSON.parse(getLocalStorage(MENU_LIST));
		const { menu } = this.props;

		if (menu) {
			const viewURLs = [];
			Object.values(menu).forEach((row) => {
				if (!isStringEmpty(row.viewURL)) {
					viewURLs.push({
						url: row.viewURL.trim(),
						id: row.id,
						name: row.formName,
						groupId: row.groupId,
						position: row.position,
					});
					// add temp urls for development process.
					if (process.env.NODE_ENV === 'development') {
						const tempUrls = getTempUrls();
						if (!isStringEmpty(tempUrls)) {
							if (tempUrls.includes(',')) {
								tempUrls.split(',').forEach((url) => viewURLs.push(url.trim()));
							} else {
								viewURLs.push(tempUrls);
							}
						}
					}
				}
			});

			// requiredRoutes = dashboardRoutes.filter(item => {
			//   if (item.hasChild) {
			//     item.children = item.children.filter(child =>
			//       viewURLs.includes(`${child.layout}${child.path}`)
			//     );
			//     if (item.children.length > 0) {
			//       return true;
			//     }
			//   }
			//   return viewURLs.includes(`${item.layout}${item.path}`);
			// });

			// To arrange routes by index in menu list

			//viewURL ma menulist bata viewurl aayo

			/**
			 * @todo make better
			 */

			let filteredUrl = [];

			const searchString = this.state.searchString || '';

			if (!isStringEmpty(searchString)) {
				filteredUrl = Object.values(viewURLs).filter((row, index) => {
					const searchRegex = new RegExp(searchString, 'gi');
					return (
						row.name &&
						(row.name.match(searchRegex) ||
							// ( row.id && row.id.toString().match(searchRegex) ) ||
							(row.position && row.position.toString().match(searchRegex)))
					);
				});
			}

			if (filteredUrl.length < 1) {
				filteredUrl = viewURLs;
			}

			/**
			 * remove building class forms according to anusuchi ka values.
			 */
			// const buildingClass = getLocalStorage(BUILDING_CLASS);

			// if (buildingClass === 'C') {
			//   filteredUrl = filteredUrl.filter(
			//     url =>
			//       !(
			//         url.url &&
			//         (url.url.includes('classB') ||
			//           url.url.includes('electrical') ||
			//           url.url.includes('sanitary'))
			//       )
			//   );
			// } else if (buildingClass === 'B') {
			//   filteredUrl = filteredUrl.filter(
			//     url => !(url.url && url.url.includes('classc'))
			//   );
			// } else if (buildingClass === 'A' || buildingClass === 'D') {
			//   filteredUrl = filteredUrl.filter(
			//     url =>
			//       !(url.url && url.url.includes('classc')) &&
			//       !(
			//         url.url &&
			//         (url.url.includes('classB') ||
			//           url.url.includes('electrical') ||
			//           url.url.includes('sanitary'))
			//       )
			//   );
			// }

			filteredUrl = dynamicUrlFilter(filteredUrl);

			/**
			 * @todo Remove requiredRoutes. Not sure what I am doing here
			 * But works for now. Refactor later.
			 */
			Object.values(filteredUrl).forEach((row) => {
				// console.log('url ', row.url);
				const route = dashboardRoutes.find((item) => {
					if (item.hasChild) {
						return item.children.find((child) => row.url === `${child.layout}${child.path}`);
					} else {
						return row.url === `${item.layout}${item.path}`;
					}
				});

				if (route) {
					if (route.hasChild) {
						route.children.forEach((child) => {
							if (row.url === `${child.layout}${child.path}`) {
								child.id = row.id;
								const parentRow = sidebarRoutes.find((obj) => obj.hasChild && obj.name.slice(-1) === row.name.slice(-1));
								if (parentRow === undefined) {
									const parentNameSplit = row.name.split(/\s+/);
									parentNameSplit.splice(2, 1, 'Class');
									sidebarRoutes.push({
										hasChild: true,
										layout: '/user',
										name: parentNameSplit.join(' '),
										children: [
											{
												...row,
												layout: '/user',
												path: row.url,
												component: child.component,
											},
										],
									});
								} else {
									parentRow.children.push({
										...row,
										layout: '/user',
										path: row.url,
										component: child.component,
									});
								}
							}
						});
					} else {
						route.id = row.id;
						sidebarRoutes.push({
							...row,
							component: route.component,
							layout: '/user',
							path: row.url,
						});
					}
				}
				requiredRoutes.push(route);
			});

			//now requiredRoutes ma tyo viewurl ko corresponding sappai detail aayo(components to load haru)

			// requiredRoutes = [
			//   ...new Set(requiredRoutes.filter(row => row !== undefined))
			// ];

			requiredRoutes = requiredRoutes.filter((row) => row !== undefined);
			sidebarRoutes = sidebarRoutes.filter((row) => row !== undefined);
		} else {
			requiredRoutes = dashboardRoutes;
		}

		return (
			<>
				<HeaderComponent history={this.props.history} onToggle={this.handleSidebarToggle} userMenu={this.props.userMenu} />
				{/* dont render sidebar when these urls are current route */}
				{showSidebar && <MainSideBar {...this.props} routes={sidebarRoutes} handleSearch={this.handleSearch} />}
				<main className="site-main">
					{this.getRoutes(dashboardRoutes)}
					<div className="sitee-main">
						<ErrorBoundary>
							<Route exact path="/user" component={Dashboard} />
							<Route path="/user/application-list" component={Dashboard} />
							<Route path="/user/task-list" component={Tasks} />
							<Route path="/user/task-search/:tag" component={TaskSearch} />
							<Route path="/user/report" component={Report} />
							<Route path="/user/building-permit" component={BuildingPermit} />
							<Route path="/user/building-permit-purano" component={BuildingPermit} />
							<Route path="/user/building-permit-tallathap/:id" component={ThapTallaGhar} />
							<Route path="/user/profile" component={Profile} />
							<Route path="/user/change-password" component={Changepass} />
							<Route path="/user/report-list/:code/:wardNo/:constructionType/:id" component={ReportList} />
							<Route path="/user/forms/all-history" component={History} />
							<Route path="/user/forms/all-comment" component={Comment} />
							<Route path="/user/forms/activity-log" component={ActivityLog} />
							<Route path="/user/forms/print-all" render={(props) => <PrintAll {...props} routes={sidebarRoutes} />} />
							<Route path="/user/completed-list" component={CompletedList} />
							<Route path={ProcessMenuObject[MenuId.ADD_STOREY_ADD].url} component={TallaThapSetup} />
							<Route path={ProcessMenuObject[MenuId.NAMSARI_ADD].url} component={NamsariSetup} />
							<Route path={ProcessMenuObject[MenuId.DISCARD_ADD].url} component={DiscardSetup} />
							<Route path={ProcessMenuObject[MenuId.MYAD_ADD].url} component={MyadSetup} />
							<Route path={ProcessMenuObject[MenuId.MYAD_APPROVE].url} component={MyadApproveSetup} />
						</ErrorBoundary>
					</div>
				</main>
				<Footer />
			</>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		menu: state.root.dashboard.menu || JSON.parse(getLocalStorage(MENU_LIST)),
		userMenu: state.root.dashboard.userMenu || JSON.parse(getLocalStorage(LSKey.USER_MENU)),
	};
};

export default connect(mapStateToProps)(LoggenInRoutes);
