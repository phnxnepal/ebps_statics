import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Home from '../components/publicComponents/Home';
import PublicHeader from '../components/publicComponents/PublicHeader';
import Contact from '../components/publicComponents/Contact';
import Faq from '../components/publicComponents/Faq';
import Footer from '../components/loggedInComponents/Footer';
import Reg_designer from '../components/publicComponents/Reg_designer';
import Downloads from '../components/publicComponents/Downloads';
import Reg_mason from '../components/publicComponents/Reg_mason';
import ForgotPassword from '../components/ForgotPassword';
import CreateAccount from '../components/CreateAccount';
import { connect } from 'react-redux';
import { getOrg } from './../store/actions/dashboardActions';
import { selectOrganizationName, selectNeedsPublicSignup } from '../store/selectors/organizationSelectors';
import { isStringEmpty } from '../utils/stringUtils';
import { getLocalStorage } from '../utils/secureLS';
import { ORG_IDS } from '../utils/constants';
import NotFound from '../components/NotFound';

class PublicRoutes extends Component {
	componentDidMount() {
		if (isStringEmpty(getLocalStorage(ORG_IDS))) {
			this.props.getOrg();
		}
	}

	render() {
		const { needsPublicSignup, organization } = this.props;
		return (
			<div>
				<PublicHeader orgName={organization} />
				<div className="public-main">
					<Route exact path="/" component={Home} />
					<Route path="/contact" component={Contact} />
					<Route path="/faq" component={Faq} />
					<Route path="/downloads" component={Downloads} />
					<Route path="/designer" component={Reg_designer} />
					<Route path="/mason" component={Reg_mason} />
					<Route path="/forgotPassword" component={ForgotPassword} />
					<Route path="/createAccount" component={needsPublicSignup ? CreateAccount : NotFound} />
				</div>
				<Footer />
			</div>
		);
	}
}

const mapDispatchToProps = {
	getOrg,
};

const mapStateToProps = state => ({
	organization: selectOrganizationName(state.root.dashboard),
	needsPublicSignup: selectNeedsPublicSignup(state.root.dashboard),
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(PublicRoutes);
