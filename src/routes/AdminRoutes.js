import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import HeaderComponent from '../components/loggedInComponents/HeaderComponent';
import Footer from '../components/loggedInComponents/Footer';
import AddUser from '../components/ebps-setup/AddUser';
import Downloads from '../components/ebps-setup/Downloads';
import FiscalYear from '../components/ebps-setup/FiscalYear';
import FileStorage from '../components/ebps-setup/FileStorage';
import FormGroup from '../components/ebps-setup/FormGroup';
import FormName from '../components/ebps-setup/FormName';
import Forward from '../components/ebps-setup/Forward';
import Organization from '../components/ebps-setup/Organization';
import OtherSetBack from '../components/ebps-setup/OtherSetBack';
import RoadSetBack from '../components/ebps-setup/RoadSetBack';
import FAQ from '../components/ebps-setup/FAQ';
import Contacts from '../components/ebps-setup/Contacts';
import { ClassGroup } from '../components/ebps-setup/ClassGroup';
import { ChangeDesigner } from '../components/ebps-setup/ChangeDesigner';
import { getMessage } from '../components/shared/getMessage';
import { headfoot } from '../utils/data/headFootLangFile';
import { DesignerSetup } from '../components/ebps-setup/DesignerSetup';
import { ArchitectureBMaster } from '../components/ebps-setup/ArchitectureBMaster';
import { MapCheckReportMaster } from '../components/ebps-setup/MapCheckReportMaster';
import { HasRevised } from '../components/ebps-setup/HasRevised';
import { ArchitectureCMaster } from '../components/ebps-setup/ArchitectureCMaster';
import { ElectricalDesignMaster } from '../components/ebps-setup/ElectricalDesignMaster';
import { MasonList } from '../components/ebps-setup/MasonList';
import SetupSidebar from '../components/loggedInComponents/SetupSidebar';
import { MenuId } from '../utils/menuUtils';
import { getUserRole } from '../utils/functionUtils';
import { MenuSetup } from '../components/ebps-setup/MenuSetup';
import { MenuAccessSetup } from '../components/ebps-setup/MenuAccessSetup';
import { RajasowSetup } from '../components/ebps-setup/RajasowSetup';
import { connect } from 'react-redux';
import { getLocalStorage } from '../utils/secureLS';
import { LSKey } from '../utils/enums/localStorageKeys';
import { WardSetup } from '../components/ebps-setup/WardSetup';
import { UserTypeSetup } from './../components/ebps-setup/UserTypeSetup';

const hf_data = headfoot.headfoot_data;
const messageId = 'headfoot.headfoot_data';

export const adminDropdownData = [
	{
		id: 'add-user',
		path: '/add-user',
		name: getMessage(`${messageId}.setup_lang.addUser`, hf_data.setup_lang.addUser),
		icon: 'add user',
		layout: '/admin',
		tag: 'user',
	},
	{
		id: 'designer',
		path: '/designer',
		name: getMessage(`${messageId}.setup_lang.addDesigner`, hf_data.setup_lang.addDesigner),
		icon: 'add user',
		layout: '/admin',
		tag: 'user',
	},
	{
		id: 'change-designer',
		path: '/change-designer',
		name: getMessage(`${messageId}.setup_lang.changeDesigner`, hf_data.setup_lang.changeDesigner),
		icon: 'exchange',
		layout: '/admin',
		tag: 'user',
	},
	{
		id: 'add-faq',
		path: '/add-faq',
		name: getMessage(`${messageId}.setup_lang.addFAQ`, hf_data.setup_lang.addFAQ),
		icon: 'question',
		layout: '/admin',
		tag: 'public',
	},
	{
		id: 'add-contacts',
		path: '/add-contacts',
		name: getMessage(`${messageId}.setup_lang.contacts`, hf_data.setup_lang.contacts),
		icon: 'address book',
		layout: '/admin',
		tag: 'public',
	},
	{
		id: 'downloads',
		path: '/downloads',
		name: getMessage(`${messageId}.setup_lang.downloads`, hf_data.setup_lang.downloads),
		icon: 'download',
		layout: '/admin',
		tag: 'public',
	},
	{
		id: 'masons-list',
		path: '/masons-list',
		name: getMessage(`${messageId}.setup_lang.masons`, hf_data.setup_lang.masons),
		icon: 'clipboard list',
		layout: '/admin',
		tag: 'public',
	},
	{
		id: MenuId.ADD_STOREY_ADD,
		path: '/talla-thap-setup',
		name: getMessage(`${messageId}.setup_lang.tallaThapSetup`, hf_data.setup_lang.tallaThapSetup),
		layout: '/admin',
		icon: 'building',
		tag: 'form',
	},
	{
		id: MenuId.NAMSARI_ADD,
		path: '/namsari-setup',
		name: getMessage(`${messageId}.setup_lang.namsariSetup`, hf_data.setup_lang.namsariSetup),
		layout: '/admin',
		icon: 'exchange',
		tag: 'form',
	},
	{
		id: 'file-storage-category',
		path: '/file-storage-category',
		name: getMessage(`${messageId}.setup_lang.fileStorage`, hf_data.setup_lang.fileStorage),
		layout: '/admin',
		icon: 'file alternate outline',
		tag: 'form',
	},
	{
		id: 'form-group',
		path: '/form-group',
		name: getMessage(`${messageId}.setup_lang.formGrp`, hf_data.setup_lang.formGrp),
		layout: '/admin',
		icon: 'group',
		tag: 'form',
	},
	{
		id: 'form-name',
		path: '/form-name',
		name: getMessage(`${messageId}.setup_lang.formName`, hf_data.setup_lang.formName),
		layout: '/admin',
		icon: 'file alternate outline',
		tag: 'form',
	},
	{
		id: 'forward',
		path: '/forward',
		name: getMessage(`${messageId}.setup_lang.forward`, hf_data.setup_lang.forward),
		layout: '/admin',
		icon: 'arrow alternate circle right',
		tag: 'form',
	},
	{
		id: 'road-set-back',
		path: '/road-set-back',
		name: getMessage(`${messageId}.setup_lang.roadSetBack`, hf_data.setup_lang.roadSetBack),
		layout: '/admin',
		icon: 'road',
		tag: 'form',
	},
	{
		id: 'other-set-back',
		path: '/other-set-back',
		name: getMessage(`${messageId}.setup_lang.otherSetBack`, hf_data.setup_lang.otherSetBack),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'class-group',
		path: '/class-group',
		name: getMessage(`${messageId}.setup_lang.classGroup`, hf_data.setup_lang.classGroup),
		layout: '/admin',
		icon: 'pin',
		tag: 'form',
	},
	{
		id: 'has-revised',
		path: '/has-revised',
		name: getMessage(`${messageId}.setup_lang.hasRevised`, hf_data.setup_lang.hasRevised),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'archi-b-master',
		path: '/archi-b-master',
		name: getMessage(`${messageId}.setup_lang.archiBMaster`, hf_data.setup_lang.archiBMaster),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'archi-c-master',
		path: '/archi-c-master',
		name: getMessage(`${messageId}.setup_lang.archiCMaster`, hf_data.setup_lang.archiCMaster),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'electrical-design-master',
		path: '/electrical-design-master',
		name: getMessage(`${messageId}.setup_lang.elecDesignMaster`, hf_data.setup_lang.elecDesignMaster),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'map-check-master',
		path: '/map-check-master',
		name: getMessage(`${messageId}.setup_lang.mapCheckMaster`, hf_data.setup_lang.mapCheckMaster),
		layout: '/admin',
		icon: 'wpforms',
		tag: 'form',
	},
	{
		id: 'organization',
		path: '/organization',
		name: getMessage(`${messageId}.setup_lang.organization`, hf_data.setup_lang.organization),
		layout: '/admin',
		icon: 'building outline',
		tag: 'other',
	},
	{
		id: 'add-fiscal-year',
		path: '/add-fiscal-year',
		name: getMessage(`${messageId}.setup_lang.addFY`, hf_data.setup_lang.addFY),
		layout: '/admin',
		icon: 'calendar alternate outline',
		tag: 'other',
	},
	{
		id: 'menu-setup',
		path: '/menu-setup',
		name: getMessage(`${messageId}.setup_lang.menuSetup`, hf_data.setup_lang.menuSetup),
		layout: '/admin',
		icon: 'settings',
		tag: 'other',
	},
	{
		id: 'menu-access-setup',
		path: '/menu-access-setup',
		name: getMessage(`${messageId}.setup_lang.menuAccessSetup`, hf_data.setup_lang.menuAccessSetup),
		layout: '/admin',
		icon: 'settings',
		tag: 'other',
	},
	{
		id: 'rajasow-setup',
		path: '/rajasow-setup',
		name: getMessage(`${messageId}.setup_lang.rajasowSetup`, hf_data.setup_lang.rajasowSetup),
		layout: '/admin',
		icon: 'money bill alternate outline',
		tag: 'form',
	},
	{
		id: 'discard-setup',
		path: '/discard-setup',
		name: getMessage(`${messageId}.setup_lang.discardSetup`, hf_data.setup_lang.discardSetup),
		layout: '/admin',
		icon: 'user cancel',
		tag: 'form',
	},
	{
		id: 'ward-setup',
		path: '/ward-setup',
		name: getMessage(`${messageId}.setup_lang.wardSetup`, hf_data.setup_lang.wardSetup),
		layout: '/admin',
		icon: 'settings',
		tag: 'other',
	},
	{
		id: 'user-type-setup',
		path: '/user-type-setup',
		name: getMessage(`${messageId}.setup_lang.userTypeSetup`, hf_data.setup_lang.userTypeSetup),
		layout: '/admin',
		icon: 'settings',
		tag: 'other',
	},

];

export const setupTags = [
	{ tag: 'user', label: 'User Setup', icon: 'user' },
	{ tag: 'public', label: 'Public Setup', icon: 'tags' },
	{ tag: 'form', label: 'Form Setup', icon: 'file' },
	{ tag: 'other', label: 'Other Setup', icon: 'tags' },
];

// const adminRoutes = [
// 	{
// 		path: '/add-user',
// 		name: hf_data.setup_lang.addUser,
// 		layout: '/admin',
// 	},
// 	// {
// 	//     path: '/building-class',
// 	//     name: 'Building Class',
// 	//     layout: '/admin'
// 	// },
// 	{
// 		path: '/designer',
// 		name: hf_data.setup_lang.addDesigner,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/add-faq',
// 		name: hf_data.setup_lang.addFAQ,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/add-contacts',
// 		name: hf_data.setup_lang.contacts,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/downloads',
// 		name: hf_data.setup_lang.downloads,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/masons-list',
// 		name: hf_data.setup_lang.masons,
// 		layout: '/admin',
// 	},
// 	// {
// 	//     path: '/floor-rate',
// 	//     name: 'Floor Rate',
// 	//     layout: '/admin'
// 	// },
// 	{
// 		path: '/add-fiscal-year',
// 		name: hf_data.setup_lang.addFY,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/file-storage-category',
// 		name: hf_data.setup_lang.fileStorage,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/form-group',
// 		name: hf_data.setup_lang.formGrp,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/form-name',
// 		name: hf_data.setup_lang.formName,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/forward',
// 		name: hf_data.setup_lang.forward,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/organization',
// 		name: hf_data.setup_lang.organization,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/road-set-back',
// 		name: hf_data.setup_lang.roadSetBack,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/other-set-back',
// 		name: hf_data.setup_lang.otherSetBack,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/class-group',
// 		name: hf_data.setup_lang.classGroup,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/has-revised',
// 		name: hf_data.setup_lang.hasRevised,
// 		layout: '/admin',
// 	},
// 	{
// 		id: 'archi-b-master',
// 		path: '/archi-b-master',
// 		name: hf_data.setup_lang.archiBMaster,
// 		layout: '/admin',
// 		icon: 'wpforms',
// 		tag: 'form',
// 	},
// 	{
// 		path: '/archi-c-master',
// 		name: hf_data.setup_lang.archiCMaster,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/electrical-design-master',
// 		name: hf_data.setup_lang.elecDesignMaster,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/map-check-master',
// 		name: hf_data.setup_lang.mapCheckMaster,
// 		layout: '/admin',
// 	},
// 	{
// 		path: '/change-designer',
// 		name: hf_data.setup_lang.changeDesigner,
// 		layout: '/admin',
// 	},
// ];

class AdminRoutes extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isSideBarToggle: true,
			userType: getUserRole(),
			userMenus: [],
		};
	}

	componentDidMount() {
		const userMenuUrls = this.props.userMenu.map(menu => menu.url);
		const requiredMenu = adminDropdownData.filter(menu => userMenuUrls.includes(`${menu.layout}${menu.path}`));
		// console.log('req', requiredMenu, adminDropdownData,  userMenuUrls);
		this.setState({ userMenus: requiredMenu });
	}

	handleSidebarToggle = () => this.setState(prev => ({ isSidebarToggle: !prev.isSidebarToggle }));

	render() {
		const { userMenus } = this.state;
		return (
			<>
				<HeaderComponent history={this.props.history} onToggle={this.handleSidebarToggle} userMenu={this.props.userMenu} />
				{/**
				 * @todo Make toggle button visible for admin.
				 */
					this.state.isSideBarToggle && <SetupSidebar userType={this.state.userType} routes={userMenus} setupTags={setupTags} />}
				<main className="site-main-setup">
					<Route path="/admin/add-user" component={AddUser} />
					{/* <Route
                        path="/admin/building-class"
                        component={BuildingClass}
                    /> */}
					<Route path="/admin/designer" component={DesignerSetup} />
					<Route path="/admin/downloads" component={Downloads} />
					<Route path="/admin/masons-list" component={MasonList} />
					<Route path="/admin/add-faq" component={FAQ} />
					<Route path="/admin/add-contacts" component={Contacts} />
					{/* <Route path="/admin/floor-rate" component={Floorrate} /> */}
					<Route path="/admin/add-fiscal-year" component={FiscalYear} />
					<Route path="/admin/file-storage-category" component={FileStorage} />
					<Route path="/admin/form-group" component={FormGroup} />
					<Route path="/admin/form-name" component={FormName} />
					<Route path="/admin/forward" component={Forward} />
					<Route path="/admin/organization" component={Organization} />
					<Route path="/admin/road-set-back" component={RoadSetBack} />
					<Route path="/admin/other-set-back" component={OtherSetBack} />
					<Route path="/admin/class-group" component={ClassGroup} />
					<Route path="/admin/has-revised" component={HasRevised} />
					<Route path="/admin/change-designer" component={ChangeDesigner} />
					<Route path="/admin/archi-b-master" component={ArchitectureBMaster} />
					<Route path="/admin/archi-c-master" component={ArchitectureCMaster} />
					<Route path="/admin/electrical-design-master" component={ElectricalDesignMaster} />
					<Route path="/admin/map-check-master" component={MapCheckReportMaster} />
					<Route path="/admin/menu-setup" component={MenuSetup} />
					<Route path="/admin/menu-access-setup" component={MenuAccessSetup} />
					<Route path="/admin/rajasow-setup" component={RajasowSetup} />
					<Route path="/admin/ward-setup" component={WardSetup} />
					<Route path="/admin/user-type-setup" component={UserTypeSetup} />
				</main>
				<Footer />
			</>
		);
	}
}

const mapStateToProps = state => {
	return {
		userMenu: state.root.dashboard.userMenu || JSON.parse(getLocalStorage(LSKey.USER_MENU)),
	};
};

export default connect(mapStateToProps)(AdminRoutes);
