import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Footer from '../components/loggedInComponents/Footer';
import HeaderComponent from '../components/loggedInComponents/HeaderComponent';

const LoggedInLayout = ({ children, ...rest }) => {
  return (
    <div className="page page-dashboard">
      <HeaderComponent />
      <div className="main">{children}</div>
      <Footer />
    </div>
  );
};

const LoggedInLayoutRoute = ({
  component: Component,
  authenticated,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <LoggedInLayout>
          {authenticated === true ? (
            <Redirect exact to="/dashboard" />
          ) : (
              <Component {...matchProps} />
            )}
        </LoggedInLayout>
      )}
    />
  );
};

const mapStateToProps = state => {
  // console.log('state-- ', state);
  return {
    authenticated: state.root.login.authenticated
  };
};

export default connect(mapStateToProps)(LoggedInLayoutRoute);
