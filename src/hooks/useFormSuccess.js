import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { handleSuccessIm } from '../utils/dataUtils';

export const useFormSuccess = (formUrl, success, statuses) => {
	const [formSubmitted, setFormSubmitted] = useState(false);
	const history = useHistory();

	useEffect(() => {
		const successObj = success ? JSON.parse(success) : {};
		if (successObj && successObj.success && formSubmitted) {
			handleSuccessIm(JSON.parse(statuses), formUrl, history, successObj);
		}

		return () => {
			setFormSubmitted(false);
		};
	}, [success, statuses, history, formUrl, formSubmitted]);

	return [setFormSubmitted];
};
