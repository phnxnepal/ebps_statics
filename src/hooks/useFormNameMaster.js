import { useEffect, useState } from 'react';
import { isStringEmpty } from '../utils/stringUtils';

export const useFormNameMaster = formNameMaster => {
	const [formNameMasterOption, setFormNameMasterOption] = useState([]);
	const [formName, setFormName] = useState(1);

	useEffect(() => {
		if (formNameMaster && formNameMaster.length > 0) {
			setFormNameMasterOption(
				formNameMaster.filter(rw => rw && !isStringEmpty(rw.name)).map(row => {
					return { value: row.id, text: row.name };
				})
			);
			setFormName(formNameMaster[0].id);
		}
	}, [formNameMaster]);

	return [formNameMasterOption, formName];
};
