import { useState, useCallback, useEffect } from 'react';
import { isStringEmpty } from '../utils/stringUtils';
import { translate, getInputType, translateWithSlash, translateNum, translateWithSlashV2 } from '../utils/langUtils';
import { getClassName } from '../utils/formUtils';

export const useTranslator = (initialValue, setFieldValue, error, name) => {
	const [valueState, setValueState] = useState(initialValue);

	// Reset state on initial value change.
	useEffect(() => {
		if (!initialValue) setValueState(undefined);
	}, [initialValue]);

	const handleChange = useCallback(
		(e) => {
			// As some bug is preventing the first letter from being set in value.
			const value1 = translate(e, getInputType());
			// const value1 = e.target.value
			setValueState(value1);
			setFieldValue(name, value1);
		},
		[setValueState, setFieldValue, name]
	);

	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	return [className, valueState, handleChange];
};

export const useTranslatorIm = (initialValue, setFieldValue, error, name, hasSlash = false) => {
	const [valueState, setValueState] = useState(initialValue);

	// Reset state on initial value change.
	useEffect(() => {
		if (!initialValue) setValueState(undefined);
		else setValueState(initialValue);
	}, [initialValue]);

	const handleChange = useCallback(
		(e) => {
			// As some bug is preventing the first letter from being set in value.
			const value1 = hasSlash ? translateWithSlashV2(e, getInputType()) : translate(e, getInputType());
			// const value1 = e.target.value
			setValueState(value1);
			setFieldValue(name, value1);
			return value1;
		},
		[setValueState, setFieldValue, name, hasSlash]
	);

	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	return [className, valueState, handleChange];
};

export const useTranslatorCoupled = (initialValue, setFieldValue, error, name, otherField) => {
	const [valueState, setValueState] = useState(initialValue);

	// Reset state on initial value change.
	useEffect(() => {
		if (!initialValue) setValueState(undefined);
		else setValueState(initialValue);
	}, [initialValue]);

	const handleChange = useCallback(
		(e) => {
			// As some bug is preventing the first letter from being set in value.
			const value1 = translate(e, getInputType());
			// const value1 = e.target.value
			setValueState(value1);
			setFieldValue(name, value1);
			setFieldValue(otherField, value1);
		},
		[setValueState, setFieldValue, name, otherField]
	);

	const className = getClassName(valueState, error);

	return [className, valueState, handleChange];
};

export const useTranslatorWithSlash = (initialValue, setFieldValue, error, name) => {
	const [valueState, setValueState] = useState(initialValue);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleChange = (e) => {
		// As some bug is preventing the first letter from being set in value.
		const value1 = translateWithSlash(e, getInputType());
		// const value1 = e.target.value
		setValueState(value1);
		setFieldValue(name, value1);
	};

	return [className, valueState, handleChange];
};

export const useNumTranslator = (initialValue, setFieldValue, error, name) => {
	const [valueState, setValueState] = useState(initialValue);

	// Reset state on initial value change.
	useEffect(() => {
		if (!initialValue) setValueState(undefined);
		else setValueState(initialValue);
	}, [initialValue]);

	const handleChange = useCallback(
		(e) => {
			// As some bug is preventing the first letter from being set in value.
			const value1 = translateNum(e, getInputType());
			// const value1 = e.target.value
			setValueState(value1);
			setFieldValue(name, value1);
		},
		[setValueState, setFieldValue, name]
	);

	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	return [className, valueState, handleChange];
};
