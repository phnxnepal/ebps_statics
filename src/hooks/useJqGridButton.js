import { translate, getInputType } from '../utils/langUtils';
import { useEffect } from 'react';

// export const useJqxGridButton = (nepaliFilterIndices = [2, 3, 4, 5], buttons, data) => {
// 	const [dataChanged, setDataChanged] = useState(false);
// 	useEffect(() => {
// 		buttons.forEach(buttonObj => {
// 			const { buttonClass, buttonHandler } = buttonObj;
// 			const changeButtons = document.querySelectorAll(`.${buttonClass}`);
// 			if (changeButtons.length > 0) {
// 				Array.from(changeButtons).forEach(button => {
// 					const row = button.dataset.row;
// 					const application = data[row];
// 					button.addEventListener('click', () => buttonHandler(application));
// 				});
// 			}
// 		});

// 		return () => {
// 			buttons.forEach(buttonObj => {
// 				const { buttonClass, buttonHandler } = buttonObj;
// 				const changeButtons = document.querySelectorAll(`.${buttonClass}`);
// 				if (changeButtons.length > 0) {
// 					Array.from(changeButtons).forEach(button => {
// 						const row = button.dataset.row;
// 						const application = data[row];
// 						button.removeEventListener('click', () => buttonHandler(application));
// 					});
// 				}
// 			});
// 		};
// 	}, [data, dataChanged, buttons]);

// 	useEffect(() => {
// 		const filterFields = document.querySelectorAll('.jqx-filter-input');
// 		if (filterFields.length > 0) {
// 			Array.from(filterFields).forEach((field, index) => {
// 				if (index === 0) {
// 					field.style.display = 'none';
// 				} else if (nepaliFilterIndices.includes(index)) {
// 					field.addEventListener('input', e => {
// 						e.target.value = translate(e, getInputType());
// 					});
// 				}
// 			});
// 		}
// 		return () => {
// 			const filterFields = document.querySelectorAll('.jqx-filter-input');
// 			if (filterFields.length > 0) {
// 				Array.from(filterFields).forEach((field, index) => {
// 					if (index === 0) {
// 						field.style.display = 'none';
// 					} else if (nepaliFilterIndices.includes(index)) {
// 						field.removeEventListener('input', e => {
// 							e.target.value = translate(e, getInputType());
// 						});
// 					}
// 				});
// 			}
// 		};
// 	}, [dataChanged, nepaliFilterIndices]);

// 	const generateButtonRenderer = row => {
// 		setDataChanged(!dataChanged);
// 		return (
// 			'<button title="Assign Designer" class="ui primary-table-single-btn tiny icon button assign-designer" data-row=' +
// 			row +
// 			'><i aria-hidden="true" class="add user icon"></i></button>'
// 		);
// 	};

// 	return { generateButtonRenderer };
// };

export const useJqxNepaliFilter = (dataChanged, nepaliFilterIndices = [2, 3, 4, 5], noFilterFields = [0]) => {
	useEffect(() => {
		const filterFields = document.querySelectorAll('.jqx-filter-input');
		if (filterFields.length > 0) {
			Array.from(filterFields).forEach((field, index) => {
				if (noFilterFields.includes(index)) {
					field.style.display = 'none';
				} else if (nepaliFilterIndices.includes(index)) {
					field.addEventListener('input', e => {
						e.target.value = translate(e, getInputType());
					});
				}
			});
		}
		return () => {
			const filterFields = document.querySelectorAll('.jqx-filter-input');
			if (filterFields.length > 0) {
				Array.from(filterFields).forEach((field, index) => {
					if (nepaliFilterIndices.includes(index)) {
						field.removeEventListener('input', e => {
							e.target.value = translate(e, getInputType());
						});
					}
				});
			}
		};
	}, [dataChanged, nepaliFilterIndices, noFilterFields]);
};
