import { useEffect, useState } from 'react';
import { getUserRole, isEmpty } from '../utils/functionUtils';
import { prepareMultiInitialValues, getJsonData } from '../utils/dataUtils';
import { getCurrentDate } from '../utils/dateUtils';
import { UserType } from '../utils/userTypeUtils';

export const useRajasowBillForm = (prevData, userName, rajasowData) => {
	const [initialValues, setinitialValues] = useState({
		receiveSign: '',
		vuktaniRakam: '',
		rashidNumber: '',
		miti: '',
	});

	useEffect(() => {
		let RInfo = '';
		let amount = '';
		const rajasowDataParsed = getJsonData(rajasowData);

		if (getUserRole() === UserType.RAJASWO) {
			RInfo = userName;
		}

		if (!isEmpty(rajasowDataParsed)) {
			amount = rajasowDataParsed.totalAmt || ''
		}
		const initVal = prepareMultiInitialValues(
			{ obj: { receiveSign: RInfo, miti: getCurrentDate(true), vuktaniRakam: amount, rashidNumber: '' }, reqFields: [] },
			{ obj: JSON.parse(prevData), reqFields: [] }
		);
		setinitialValues(initVal);
	}, [prevData, userName, rajasowData]);

	return { initialValues };
};
