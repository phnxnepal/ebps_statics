import { useRef, useEffect, useState } from 'react';

export const useCursorReset = (valueState) => {
	const inputRef = useRef(null);
	const [cursorPosition, setCursorPosition] = useState();

	useEffect(() => {
		if (valueState && inputRef.current) {
			inputRef.current.setSelectionRange(cursorPosition, cursorPosition);
		}
	}, [valueState, cursorPosition]);

	return [inputRef, setCursorPosition];
};
