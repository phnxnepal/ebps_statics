import { useState, useEffect, useCallback, useReducer } from 'react';
import { showToast } from '../utils/functionUtils';

export const useAdminSetup = (initialState, success) => {
	const [openAddModal, setOpenAddModal] = useState(false);
	const [openDeleteModal, setOpenDeleteModal] = useState(false);
	const [isEditing, setIsEditing] = useState(false);
	const [isDeleting, setIsDeleting] = useState(false);
	const [formState, setFormState] = useState(initialState);

	useEffect(() => {
		if (success && success.success) {
			if (isDeleting) {
				showToast('Data deleted successfully');
				setOpenDeleteModal(false);
				setIsDeleting(false);
			} else {
				showToast('Data saved successfully');
				setOpenAddModal(false);
				setIsEditing(false);
			}
		}
	}, [success]);

	const handleAdd = () => {
		setFormState({ ...initialState });
		setIsEditing(false);
		setIsDeleting(false);
		setOpenAddModal(true);
	};

	const handleEdit = (row) => {
		setOpenAddModal(true);
		setIsEditing(true);
		setIsDeleting(false);
		setFormState({ ...row });
	};

	const handleDelete = (row) => {
		setOpenDeleteModal(true);
		setIsDeleting(true);
		setIsEditing(false);
		setFormState({ ...row });
	};

	return {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		setOpenAddModal,
		setOpenDeleteModal,
	};
};

export const useSetup = (initialState, success) => {
	const [openAddModal, setOpenAddModal] = useState(false);
	const [openDeleteModal, setOpenDeleteModal] = useState(false);
	const [isEditing, setIsEditing] = useState(false);
	const [isDeleting, setIsDeleting] = useState(false);
	const [formState, setFormState] = useState(initialState);

	useEffect(() => {
		if (success && success.success) {
			if (isDeleting) {
				showToast('Data deleted successfully');
				setOpenDeleteModal(false);
				setIsDeleting(false);
			} else {
				showToast('Data saved successfully');
				setOpenAddModal(false);
				setIsEditing(false);
			}
		}
	}, [success]);

	const handleAdd = () => {
		setFormState({ ...initialState });
		setIsEditing(false);
		setIsDeleting(false);
		setOpenAddModal(true);
	};

	const handleEdit = (row) => {
		setOpenAddModal(true);
		setIsEditing(true);
		setIsDeleting(false);
		setFormState({ ...row });
	};

	const handleClose = (row) => {
		setOpenDeleteModal(false);
		setOpenAddModal(false);
		setIsDeleting(false);
		setIsEditing(false);
		setFormState({ ...initialState });
	};

	const handleDelete = (row) => {
		setOpenDeleteModal(true);
		setIsDeleting(true);
		setIsEditing(false);
		setFormState({ ...row });
	};

	return {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		handleClose,
	};
};

export const useUserSetup = (initialState, initialRenewState, success) => {
	const [openAddModal, setOpenAddModal] = useState(false);
	const [openDeleteModal, setOpenDeleteModal] = useState(false);
	const [openRenewModal, setOpenRenewModal] = useState(false);

	const [isEditing, setIsEditing] = useState(false);
	const [isViewing, setIsViewing] = useState(false);
	const [isAdding, setIsAdding] = useState(false);
	const [isDeleting, setIsDeleting] = useState(false);
	const [isRenewing, setIsRenewing] = useState(false);

	const [formState, setFormState] = useState(initialState);
	const [renewState, setRenewState] = useState(initialRenewState);
	const [latestRenew, setLatestRenew] = useState(initialRenewState);

	useEffect(() => {
		if (success && success.success) {
			if (isDeleting) {
				showToast('Data deleted successfully');
				setOpenDeleteModal(false);
				setIsDeleting(false);
			} else if (isRenewing && !isAdding) {
				showToast('Designer renewed successfully');
				setOpenRenewModal(false);
				setIsRenewing(false);
				setIsAdding(false);
			} else {
				showToast('Data saved successfully');
				setOpenAddModal(false);
				setIsEditing(false);
			}
		}
	}, [success]);

	useEffect(() => {
		if (isAdding || isEditing || isViewing) {
			setOpenAddModal(true);
		} else if (isDeleting) {
			setOpenDeleteModal(true);
		} else if (isRenewing) {
			setOpenRenewModal(true);
		} else {
			setOpenDeleteModal(false);
			setOpenAddModal(false);
			setOpenRenewModal(false);
		}
	}, [isAdding, isEditing, isDeleting, isRenewing, isViewing]);

	const handleAdd = () => {
		setFormState({ ...initialState });
		setIsAdding(true);
		setIsEditing(false);
		setIsDeleting(false);
	};

	const handleEdit = useCallback((row) => {
		setFormState({ ...row });
		setIsEditing(true);
		setIsAdding(false);
		setIsDeleting(false);
	}, []);

	const handleView = useCallback((row) => {
		setFormState({ ...row });
		setIsEditing(false);
		setIsAdding(false);
		setIsDeleting(false);
		setIsViewing(true);
	}, []);

	const handleDelete = (row) => {
		setFormState({ ...row });
		setIsDeleting(true);
		setIsAdding(false);
		setIsEditing(false);
	};

	const handleRenew = useCallback((row, renewalData) => {
		const renewData = renewalData.filter((rData) => rData.designerId === parseInt(row.id)) || [];
		const latestRenew =
			renewData && renewData.length > 0 && renewData.reduce((prev, current) => (prev.renewDate > current.renewDate ? prev : current));
		setFormState({ ...row });
		setRenewState(renewData);
		setLatestRenew(latestRenew);
		setIsRenewing(true);
		setIsDeleting(false);
		setIsAdding(false);
		setIsEditing(false);
	}, []);

	const handleClose = () => {
		setIsDeleting(false);
		setIsAdding(false);
		setIsEditing(false);
		setIsRenewing(false);
		setIsViewing(false);
	};

	return {
		isEditing,
		isDeleting,
		isRenewing,
		isViewing,
		formState,
		renewState,
		latestRenew,
		openAddModal,
		openDeleteModal,
		openRenewModal,
		handleAdd,
		handleClose,
		handleEdit,
		handleDelete,
		handleRenew,
		handleView,
	};
};

const ACTIONS = {
	DELETE_SUCCESS: '__delete_success__',
	EDIT_SUCCESS: '__edit_success__',
	HANDLE_ADD: '__handle_add__',
	HANDLE_EDIT: '__handle_edit__',
	HANDLE_CLOSE: '__handle_close__',
	HANDLE_DELETE: '__handle_delete__',
};

const initialDeleteState = { isDeleting: false, openDeleteModal: false };
const initialEditState = { isEditing: false, openAddModal: false };

export const useSetupV2 = (initialState, success) => {
	const initialSetupState = {
		...initialDeleteState,
		...initialEditState,
		formState: initialState,
	};
	const [{ isEditing, isDeleting, formState, openAddModal, openDeleteModal }, dispatch] = useReducer((state, action) => {
		switch (action.type) {
			case ACTIONS.DELETE_SUCCESS: {
				return { ...state, ...initialDeleteState };
			}
			case ACTIONS.EDIT_SUCCESS: {
				return { ...state, ...initialEditState };
			}
			case ACTIONS.HANDLE_ADD: {
				return { ...state, formState: { ...initialState }, isEditing: false, isDeleting: false, openAddModal: true };
			}
			case ACTIONS.HANDLE_EDIT: {
				return { ...state, formState: { ...action.payload }, isEditing: true, isDeleting: false, openAddModal: true };
			}
			case ACTIONS.HANDLE_DELETE: {
				return { ...state, formState: { ...action.payload }, ...initialEditState, isDeleting: true, openDeleteModal: true };
			}
			case ACTIONS.HANDLE_CLOSE: {
				return initialSetupState;
			}
			default: {
				return state;
			}
		}
	}, initialSetupState);

	useEffect(() => {
		if (success && success.success) {
			if (isDeleting) {
				dispatch({ type: ACTIONS.DELETE_SUCCESS });
				showToast('Data deleted successfully');
			} else if (isEditing) {
				dispatch({ type: ACTIONS.EDIT_SUCCESS });
				showToast('Data saved successfully');
			}
		}
	}, [success, isDeleting, isEditing]);

	const handleAdd = () => {
		dispatch({ type: ACTIONS.HANDLE_ADD });
	};

	const handleEdit = (row) => {
		dispatch({ type: ACTIONS.HANDLE_EDIT, payload: row });
	};

	const handleClose = () => {
		dispatch({ type: ACTIONS.HANDLE_CLOSE });
	};

	const handleDelete = (row) => {
		dispatch({ type: ACTIONS.HANDLE_DELETE, payload: row });
	};

	return {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		handleClose,
	};
};
