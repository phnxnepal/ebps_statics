import { useEffect, useState } from 'react';
import { getFormattedNumber } from '../utils/mathUtils';

export const useFiscalYear = fiscalYear => {
	const [fyOption, setFyOption] = useState([]);
	const [fy, setFy] = useState('');

	useEffect(() => {
		const validFiscalYears = fiscalYear.filter(fy => fy.status === 'Y');
		if (validFiscalYears && validFiscalYears.length > 0) {
			setFyOption(
				validFiscalYears.map(fy => {
					return { value: fy.yearName, text: fy.yearName };
				})
			);
			setFy(validFiscalYears[0].yearName);
		}
	}, [fiscalYear]);

	return { fy, fyOption };
};

export const useFiscalYearAll = (fiscalYear, objName = 'yearName', isCode = false) => {
	const [fyOption, setFyOption] = useState([]);
	const [fy, setFy] = useState('');

	useEffect(() => {
		if (fiscalYear && fiscalYear.length > 0) {
			const validFiscalYear = fiscalYear.find(fy => fy.status === 'Y');
			setFyOption(
				fiscalYear.map(fy => {
					return { value: isCode ? getFormattedNumber(fy[objName]) : fy[objName], text: fy.yearName };
				})
				);
				
				if (validFiscalYear) {
					setFy(validFiscalYear[objName]);
				} else {
					setFy(fiscalYear[0][objName]);
				}
			}
		}, [fiscalYear, objName, isCode]);
	return { fy, fyOption };
};
