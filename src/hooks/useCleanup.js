import { store } from '..';
import { clearErrors } from '../store/actions/loginForm';
import { useEffect } from 'react';

/**
 * Clean up hook to clear errors and loading while unmounting
 */
export const useCleanup = () =>
	useEffect(() => {
		return () => {
			store.dispatch(clearErrors());
		};
	}, []);
