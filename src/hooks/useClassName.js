import { useState, useEffect } from 'react';
import { isStringEmpty } from '../utils/stringUtils';

/**
 * Hook to get classname of field
 * @param {string} value Field value
 * @param {error} error Field error
 */
export const useClassName = (value, error) => {
	const [className, setClassName] = useState('text-input');
	useEffect(() => {
		const classState = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
		setClassName(classState);
    }, [value, error]);
    
    return className;
};
