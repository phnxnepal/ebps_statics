let nepToEng = {
  '\u0966': 0, // 0 -> ०
  '\u0967': 1, // 1 -> १
  '\u0968': 2, // 2 -> २
  '\u0969': 3, // 3 -> ३
  '\u096A': 4, // 4 -> ४
  '\u096B': 5, // 5 -> ५
  '\u096C': 6, // 6 -> ६
  '\u096D': 7, // 7 -> ७
  '\u096E': 8, // 8 -> ८
  '\u096F': 9 // 9 -> ९
};

// Return the unicode of the key passed
function mapper(keyCode, array) {
  return array[keyCode - 32];
}

// Wrapper function for the keymap function
window.unicodify = keyCode => {
  return mapper(keyCode, unicode);
};
window.preetify = keyCode => {
  return mapper(keyCode, preeti);
};
