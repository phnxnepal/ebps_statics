import { concatValues } from '../../components/shared/layout/layoutUtils';
import { olddosrocharanPrabidhikview } from '../data/mockLangFile';
import { LayoutTypes } from '../../components/shared/layout/layoutConstants';
import { chaChainaNepaliOptions } from '../optionUtils';
import { Client } from '../clientUtils';

const dpv = olddosrocharanPrabidhikview.dosrocharanPrabidhikview_data;

export const commonParagraph = permitData =>
	concatValues([
		dpv.content.content_1,
		permitData.nibedakName,
		dpv.content.content_2,
		permitData.oldWardNo,
		dpv.content.content_3,
		permitData.buildingJoinRoad,
		dpv.content.content_4,
	]);

export const getParagraph = (client, permitData) => {
	switch (client) {
		case Client.INARUWA:
			return [commonParagraph(permitData)];
		default:
			return [{ data: dpv.subhead.subhead_1 }, commonParagraph(permitData)];
	}
};

export const getYesNoSection = () => {
	return [
		{
			type: LayoutTypes.YES_NO_RADIO_LIST,
			options: chaChainaNepaliOptions,
			list: [
				{
					name: 'sadakMapdandaPalana',
					otherName: 'sadakMapdandaPalanaAdditional',
					label: dpv.subcontent.subcontent_1,
					placeholder: dpv.plor_else_placeholder,
				},
				{
					name: 'setBackPalana',
					otherName: 'setBackPalanaAdditional',
					label: dpv.subcontent.subcontent_2,
					placeholder: dpv.plor_else_placeholder,
				},
				{
					name: 'groundCoverage',
					otherName: 'groundCoverageAdditional',
					label: dpv.subcontent.subcontent_3,
					placeholder: dpv.plor_else_placeholder,
				},
				{
					name: 'mulMapdandaPalana',
					otherName: 'mulMapdandaPalanaAdditional',
					label: dpv.subcontent.subcontent_4,
					placeholder: dpv.plor_else_placeholder,
				},
				{
					name: 'bhawanNirmanSanghita',
					otherName: 'bhawanNirmanSanghitaAdditional',
					label: dpv.subcontent.subcontent_5,
					placeholder: dpv.plor_else_placeholder,
				},
				{
					name: 'ferbadalAdditional',
					label: dpv.subcontent.subcontent_6,
				},
			],
		},
	];
};
