import { Client } from '../clientUtils';
import { noticePaymentLang, sundarHaraichaNotice } from '../data/noticePaymentApplicationLang';

const noticeAppLang = noticePaymentLang;
export const getSalutation = (key, organization) => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				needsSalutation: false,
			};
		case Client.BIRTAMOD:
			return {
				needsSalutation: true,
				lines: [
					`${organization.name} ${noticeAppLang.subTitle_birtamod_karyalaya} ${organization.address} ${noticeAppLang.subTitle_birtamod_purnabiram}`,
					noticeAppLang.subTitle_3,
				],
				boldLines: [0],
			};
		default:
			return {
				needsSalutation: true,
				lines: [organization.name, organization.officeName, noticeAppLang.subTitle_3],
				boldLines: [],
			};
	}
};

export const getTitle = key => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				heading: sundarHaraichaNotice.heading,
				letterHead: true,
				subTitle: sundarHaraichaNotice.subTitle,
			};
		default:
			return {
				heading: noticeAppLang.heading,
				letterHead: false,
			};
	}
};
