import { FormUrlFull } from '../enums/url';
import { Client } from '../clientUtils';
import { UserType } from '../userTypeUtils';

export const DEFAULT = 'default';

export const ApproveFormMap = {
	[FormUrlFull.PRABIDHIK_PRATIBEDAN]: {
		[Client.KAMALAMAI]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER],
		},
		[DEFAULT]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER],
		},
	},
	[FormUrlFull.NO_OBJECTION_SHEET]: {
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.ALLOWANCE_PAPER]: {
		[Client.KAMALAMAI]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER, UserType.POSTF],
		},
		[DEFAULT]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER, UserType.POSTF],
		},
	},
	[FormUrlFull.SUPERSTRUCTURE_NOTE_ORDER]: {
		[Client.MECHI]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.SANSODHAN_SUPERSTRUCTURE_IJAJAT]: {
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.DHARAUTI_RAKAM_PHIRTA]: {
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.PLINTH_LEVEL_TECH_APP]: {
		[Client.KAMALAMAI]: {
			approveBy: [UserType.AMIN, UserType.SUB_ENGINEER, UserType.ENGINEER],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.SUPERSTRUCTURE_KO_NIRMAN_KARYA]: {
		[Client.KAMALAMAI]: {
			approveBy: [UserType.AMIN, UserType.SUB_ENGINEER, UserType.ENGINEER],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.NOTICE_15_DAYS]: {
		[Client.MECHI]: {
			approveBy: [UserType.ENGINEER],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.PURANO_GHAR_NIRMAN_TIPPANI]: {
		[Client.BIRTAMOD]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[FormUrlFull.NOTE_ORDER_PLINTH_LEVEL]: {
		[Client.BIRTAMOD]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
		[DEFAULT]: {
			approveBy: [UserType.ENGINEER, UserType.ADMIN],
		},
	},
	[DEFAULT]: {
		[Client.KAMALAMAI]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER, UserType.ADMIN],
		},
		[DEFAULT]: {
			approveBy: [UserType.SUB_ENGINEER, UserType.ENGINEER, UserType.ADMIN],
		},
	},
};
