import { Client } from '../clientUtils';
import { BuildingFinishCertificateData } from '../data/BuildingFinishCertificateData';
import { DatePosition, TextSize } from '../constants/formComponentConstants';
import { patraChalani } from '../data/formCommonData';
import { LayoutTypes } from '../../components/shared/layout/layoutConstants';
import { concatValues } from '../../components/shared/layout/layoutUtils';

const Detail = BuildingFinishCertificateData.details;
export const getSubHeading = key => {
	switch (key) {
		case Client.BIRTAMOD:
			return {
				titles: [
					{
						title: BuildingFinishCertificateData.details.titleSplit1,
						className: TextSize.MEDIUM,
					},
					{
						title: BuildingFinishCertificateData.details.titleSplit2,
						className: `${TextSize.BIG} red`,
					},
				],
				datePosition: DatePosition.BOTTOM,
				serialNumberFields: [
					{ label: BuildingFinishCertificateData.details.pasa, fieldName: 'patraSankhya' },
					{ label: BuildingFinishCertificateData.details.cana, fieldName: 'chalaniNumber' },
				],
			};
		case Client.SUNDARHARAICHA:
			return {
				titles: [
					{
						title: BuildingFinishCertificateData.details.titleSundarHaraicha1,
						className: TextSize.TINY,
					},
					{
						title: BuildingFinishCertificateData.details.titleSundarHaraicha2,
						className: `${TextSize.TINY} underline`,
					},
				],
				datePosition: DatePosition.TOP,
				serialNumberFields: [
					{ label: BuildingFinishCertificateData.details.pasaFull, fieldName: 'patraSankhya' },
					{ label: BuildingFinishCertificateData.details.canaFull, fieldName: 'chalaniNumber' },
				],
			};

		default:
			return {
				titles: [
					{
						title: BuildingFinishCertificateData.details.title,
						className: `${TextSize.SMALL} underline`,
					},
				],
				datePosition: DatePosition.BOTTOM,
				serialNumberFields: [
					{ label: BuildingFinishCertificateData.details.pasa, fieldName: 'patraSankhya' },
					{ label: BuildingFinishCertificateData.details.cana, fieldName: 'chalaniNumber' },
					{ label: BuildingFinishCertificateData.details.prana, fieldName: 'prashasanikNumber' },
				],
			};
	}
};

export const getPatraSankhya = key => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				serialNumberFields: [
					{ label: patraChalani.pasaFull, fieldName: 'patraSankhya' },
					{ label: patraChalani.canaFull, fieldName: 'chalaniNumber' },
				],
			};
		case Client.INARUWA:
			return {
				serialNumberFields: [
					{ label: patraChalani.pasaFull, fieldName: 'patraSankhya' },
					{ label: patraChalani.canaFull, fieldName: 'chalaniNumber' },
					{ label: patraChalani.gharNo, fieldName: 'gharNo' },
					{ label: patraChalani.sanketNo, fieldName: 'sanketNo' },
				],
			}
		default:
			return {
				serialNumberFields: [
					{ label: patraChalani.patraSankhya, fieldName: 'patraSankhya' },
					{ label: patraChalani.chalaniSankyha, fieldName: 'chalaniNumber' },
				],
			};
	}
};

export const getParagraph = (client, userData, permitData) => {
	switch (client) {
		case Client.SUNDARHARAICHA:
			break;

		default:
			return [
				concatValues([
					userData.organization.name,
					Detail.wda,
					permitData.newWardNo,
					Detail.data1,
					permitData.applicantName,
					Detail.data2,
					permitData.oldMunicipal,
					Detail.wda,
					permitData.oldWardNo,
					Detail.data4,
					userData.organization.name,
					Detail.wda,
					permitData.newWardNo,
					Detail.data5,
					permitData.sadak,
					Detail.data6,
					permitData.buildingJoinRoadType,
					Detail.data7,
					permitData.kittaNo,
					Detail.data8,
					permitData.landArea,
					permitData.landAreaType,
					Detail.data9,
				]),
				{ type: LayoutTypes.INPUT_LANG, name: 'tapasil' },
				{ data: Detail.data10 },
			];
	}
};