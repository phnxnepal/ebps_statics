import { Client } from '../clientUtils';
import { sundarHariacha, kankaiprabidhik } from '../data/prabidhikData';

export const getSubHeading = key => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				heading: sundarHariacha.heading,
				salutation: sundarHariacha.salutation,
			};
		case Client.KANKAI:
			return {
				heading: kankaiprabidhik.heading,
				salutation: kankaiprabidhik.salutation,
			};
		default:
			return {
				heading: kankaiprabidhik.heading,
				salutation: kankaiprabidhik.salutation,
			};
	}
};
