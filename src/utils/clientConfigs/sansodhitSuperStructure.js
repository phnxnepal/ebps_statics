import { Client } from '../clientUtils';
import { letterSalutation, footerSignature as fs } from '../data/genericFormData';

export const getSalutation = (key, userData) => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				salutations: [letterSalutation.shreemanEngineer, userData.organization.name],
			};
		default:
			return {
				salutations: [userData.organization.name, userData.organization.officeName, userData.organization.address],
			};
	}
};

export const getSansodhanIjajatFooter = key => {
	switch (key) {
		case Client.SUNDARHARAICHA:
			return {
				designations: [
					[fs.bhawanNaksaPass, fs.upasakha],
					[fs.janchGarne, fs.er],
					[fs.chief, fs.pramanPatraPradhanGarne],
				],
			};
		case Client.INARUWA:
			return {
				designations: [
					[fs.naksaUpasakha],
					[fs.er],
					[fs.chief],
				],
			};
		case Client.BIRTAMOD:
			return {
				designations: [fs.sthalgatNirikshyan, fs.er, fs.chief],
			};
		default:
			return {
				designations: [fs.ser, fs.er, fs.chief],
			};
	}
};
