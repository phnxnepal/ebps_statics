import { FormUtility, getSaveByUserDetails } from './formUtils';
import { FormUrlFull } from './enums/url';

test('should return corrent values for null url', () => {
	const formUtility = new FormUtility(null);
	expect(formUtility.getUrl()).toBe('');
	expect(formUtility.savePermission()).toBe(false);
	expect(formUtility.approvePermission()).toBe(false);
});

test('should return corrent values for undefined url', () => {
	const formUtility = new FormUtility(undefined);
	expect(formUtility.getUrl()).toBe('');
	expect(formUtility.savePermission()).toBe(false);
	expect(formUtility.approvePermission()).toBe(false);
});

test('should return corrent values for valid url', () => {
	const formUtility = new FormUtility(FormUrlFull.GHAR_NAKSA_SURJAMIN);
	expect(formUtility.getUrl()).toBe(FormUrlFull.GHAR_NAKSA_SURJAMIN);
	expect(formUtility.savePermission()).toBe(false);
	expect(formUtility.approvePermission()).toBe(false);
});

test('should return valid serInfo for valid enterByUser and userData', () => {
	const enterByUser = {
		userType: 'F',
		name: 'नमुना प्रशासन',
		designation: 'प्रशासन शाखा',
	};

	const userData = {
		userName: 'नमुना प्रशासन',
		userType: 'F',
	};

	const serInfo = getSaveByUserDetails(enterByUser, userData);
	expect(serInfo.subDesignation).toBe('प्रशासन शाखा');
	expect(serInfo.subName).toBe('नमुना प्रशासन');
});
