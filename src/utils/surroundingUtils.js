export class SurroundingArray {
	constructor(surrounding) {
		this.surrounding = surrounding || [];
	}

	// Getters
	getSurrounding() {
		return this.surrounding;
	}

	getAllSurrounding() {
		const completeObject = [1, 2, 3, 4].map(side => {
			const surr = this.surrounding.find(s => s.side === side);
			if (surr) {
				return surr;
			} else {
				return { side };
			}
		});
		return completeObject;
	}
}

export class Surrounding {
	constructor(side, kittaNo, sandhiyar, feet, sideUnit) {
		this.side = side;
		this.kittaNo = kittaNo;
		this.sandhiyar = sandhiyar;
		this.feet = feet;
		this.sideUnit = sideUnit;
	}

	getSide() {
		return this.side;
	}

	getSurroundingObject() {
		return {
			side: this.side,
			kittaNo: this.kittaNo,
			sandhiyar: this.sandhiyar,
			feet: this.feet,
			sideUnit: this.sideUnit,
		};
	}
}
