import { FORM_NAME_MASTER, FORM_GROUP_MASTER } from './constants';
import { LocalAPIArray, LocalAPI, areUrlsEqual } from './urlUtils';
import { userTypeHierarchy, getApprovalFieldName, UserType } from './userTypeUtils';

test('should return empty array for empty apis', () => {
	const localAPIArray = new LocalAPIArray([]);
	expect(localAPIArray.getLocalApis()).toEqual([]);
});

test('should return empty array for undefined floor input', () => {
	const localAPIArray = new LocalAPIArray(undefined);
	expect(localAPIArray.getLocalApis()).toEqual([]);
});

test('should return sorted floors', () => {
	const localAPIArray = new LocalAPIArray([new LocalAPI(FORM_NAME_MASTER, 'formNameMaster'), new LocalAPI(FORM_GROUP_MASTER, 'formGroupMaster')]);

	expect(localAPIArray.getLocalApis()).toEqual([
		new LocalAPI(FORM_NAME_MASTER, 'formNameMaster'),
		new LocalAPI(FORM_GROUP_MASTER, 'formGroupMaster'),
	]);

	expect(localAPIArray.getLocalApis()[0].getKey()).toBe(FORM_NAME_MASTER);
	expect(localAPIArray.getLocalApis()[0].getObjName()).toBe('formNameMaster');
	expect(localAPIArray.getLocalApis()[1].getKey()).toBe(FORM_GROUP_MASTER);
	expect(localAPIArray.getLocalApis()[1].getObjName()).toBe('formGroupMaster');
});

test('isLastForm should produce correct result', () => {
	const formMaster = {
		aminApproval: 'N',
		designerApproval: 'N',
		enterBy: 'B',
		engrApproval: 'Y',
		name: 'पुरानो घर भवन निर्माण सम्पन्‍न प्रमाण पत्र',
		viewUrl: '/user/forms/purano-ghar-sampanna-pramanpatra',
		chiefApproval: 'Y',
		rajasowApproval: 'N',
		id: 51,
		tableName: 'purano_ghar_sampanna_praman_patra',
	};

	let highestUser, highestApprovalField;

	const currentUserRole = 'A';

	const sortedUsers = userTypeHierarchy.sort((a, b) => (a.priority < b.priority ? 1 : -1));

	for (const user of sortedUsers) {
		const approvalField = getApprovalFieldName(user.user);
		if (formMaster[approvalField] && formMaster[approvalField] === 'Y') {
			highestUser = user.user;
			highestApprovalField = approvalField;
			break;
		}
	}

	expect(highestUser).toBe(UserType.ADMIN);
	expect(highestApprovalField).toBe('chiefApproval');
	expect(currentUserRole).toBe('A');
});

test('should return true if the urls are equal', () => {
	const equalUrls = areUrlsEqual(
		'http://stg.phoenixsolutions.com.np:8080/EBPS/api/Utility/OrganizationUser/DesignerRenewStatus',
		'api/Utility/OrganizationUser/DesignerRenewStatus/'
	);
	const equalUrls1 = areUrlsEqual('http://stg.phoenixsolutions.com.np:8080/EBPS/api/Utility/OrganizationUser/', 'api/Utility/OrganizationUser');

	const notEqualUrls = areUrlsEqual(
		'http://stg.phoenixsolutions.com.np:8080/EBPS/api/Utility/OrganizationUser/DesignerRenewStatus/',
		'api/Utility/OrganizationUser'
	);

	const notEqualUrls2 = areUrlsEqual(
		'http://stg.phoenixsolutions.com.np:8080/EBPS/api/Utility/OrganizationUser//',
		'api/Utility/OrganizationUser/DesignerRenewStatus'
	);
	expect(equalUrls).toBe(true);
	expect(equalUrls1).toBe(true);
	expect(notEqualUrls).toBe(false);
	expect(notEqualUrls2).toBe(false);
});
