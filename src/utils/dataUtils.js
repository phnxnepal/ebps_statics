import { ARRAY_FIELD_DELIMITER, FORM_GROUP_MASTER, DAYS_TO_NOTIFY, DAYS_TO_URGENCY, DEFAULT_UNIT_LENGTH } from './constants';
import { isStringEmpty } from './stringUtils';
import { getUniqueFromArray, replaceDot, isEmpty, getUserTypeValueNepali, showToast, getNextUrl } from './functionUtils';
import { getLocalStorage } from './secureLS';
import { UserType } from './userTypeUtils';
import { getNepaliDate, getDateCount, getCurrentDate } from './dateUtils';
import { translateEngToNep } from './langUtils';
import { getNumber, getIntNumber } from './mathUtils';

export const prepareInitialValues = (otherData, prevData) => {
	let initialValues = {
		...getRequiredFields(checkError(otherData.obj), otherData.reqFields),
		...getRequiredFields(checkError(prevData.obj), prevData.reqFields),
	};
	return initialValues;
};

export const prepareInitialValue = (prevData) => {
	let initialValues = {
		...getRequiredFields(checkError(prevData.obj), prevData.reqFields),
	};
	return initialValues;
};

export const prepareMultiInitialValues = (...data) => {
	let initialValues = {};

	data.forEach((row) => {
		initialValues = {
			...initialValues,
			...getRequiredFields(row.obj, row.reqFields),
		};
	});

	return initialValues;
};

export const prepareComplexInitialValues = (...data) => {
	let initialValues = {};

	data.forEach((row) => {
		if (row.complex) {
			initialValues = {
				...initialValues,
				...getRequiredComplexFields(row.obj, row.reqFields),
			};
		} else {
			initialValues = {
				...initialValues,
				...getRequiredFields(row.obj, row.reqFields),
			};
		}
	});

	return initialValues;
};

export const getRequiredFields = (raw, reqFields) => {
	if (reqFields.length === 0) {
		return raw;
	} else {
		return Object.keys(raw)
			.filter((key) => reqFields.includes(key))
			.reduce((obj, key) => Object.assign(obj, { [key]: raw[key] }), {});
	}
};

export const getRequiredComplexFields = (raw, reqFields) => {
	if (reqFields.length === 0) {
		return raw;
	} else {
		return Object.keys(raw)
			.filter((key) => reqFields.includes(key))
			.reduce((obj, key) => {
				if (typeof raw[key] === 'string' && raw[key].includes(ARRAY_FIELD_DELIMITER)) {
					let ret = [];
					raw[key].split(ARRAY_FIELD_DELIMITER).forEach((row) => {
						ret.push(isStringEmpty(row) ? '' : JSON.parse(row));
					});
					return Object.assign(obj, { [key]: ret });
				} else if (typeof raw[key] === 'string' && raw[key].includes('{')) {
					let ret = [];
					ret.push(JSON.parse(raw[key]));
					return Object.assign(obj, { [key]: ret });
				} else {
					return Object.assign(obj, { [key]: raw[key] });
				}
			}, {});
	}
};

export const checkError = (obj) => {
	if (obj && obj.error) {
		return {};
	} else if (!obj) {
		return {};
	} else {
		return obj;
	}
};

export const cleanFormData = (rawObj, reqFields) => {
	if (isEmpty(rawObj)) return {};
	const retObj = Object.keys(rawObj)
		.filter((key) => {
			if (reqFields && reqFields.length > 0) {
				return !isStringEmpty(rawObj[key]) && reqFields.includes(key);
			} else {
				return !isStringEmpty(rawObj[key]);
			}
		})
		.reduce((obj, key) => Object.assign(obj, { [key]: rawObj[key] }), {});
	return retObj;
};

export const getJsonData = (prevData) => {
	const errChecked = checkError(prevData);
	try {
		return JSON.parse(errChecked.jsonData);
	} catch {
		return {};
	}
};

export const getJsonDataV2 = (prevData, fieldName) => {
	const errChecked = checkError(prevData);
	try {
		return JSON.parse(errChecked[fieldName]);
	} catch {
		return {};
	}
};

export class FloorArray {
	constructor(floors) {
		this.floors = floors
			? floors
					.slice()
					.sort((a, b) => {
						return parseInt(a.floor) > parseInt(b.floor) ? 1 : -1;
					})
					.map((floor) => new Floor(floor.length, floor.height, floor.width, floor.area, floor.floor))
			: [];
		this.floorsWithUnit = floors
			? floors
					.slice()
					.sort((a, b) => {
						return parseInt(a.floor) > parseInt(b.floor) ? 1 : -1;
					})
					.map((floor) => {
						return {
							length: floor.length,
							height: floor.height,
							width: floor.width,
							area: floor.area,
							floor: floor.floor,
							floorUnit: floor.floorUnit,
						};
					})
			: [];
		this.floorUnit = floors && floors.length > 0 ? floors[0].floorUnit : DEFAULT_UNIT_LENGTH;
		this.heightRelevantFloors = this.fetchHeightRelevantFloors();
		this.areaRelevantFloors = this.fetchAreaRelevantFloors();
		this.length = this.floors.length;
	}

	// Getters
	getFloors() {
		return this.floors;
	}

	getLength() {
		return this.length;
	}

	getFloorsWithUnit() {
		return this.floorsWithUnit;
	}

	getFloorUnit() {
		return this.floorUnit;
	}

	getNepaliFloorUnit() {
		return { unit: getOptionText(this.floorUnit, distanceOptions), areaUnit: getOptionText(this.floorUnit, squareUnitOptions) };
	}

	getHeightRelevantFloors() {
		return this.heightRelevantFloors;
	}

	getAreaRelevantFloors() {
		return this.areaRelevantFloors;
	}

	getFloorByFloor(floor) {
		const requiredFloor = this.floors.find((fl) => fl.getFloor() === floor);
		if (requiredFloor) {
			return requiredFloor.getFloorObject();
		} else {
			return {};
		}
	}

	getAllFields(excludeField, prefix = 'floor') {
		return this.floors
			.map((floor, index) => [`${prefix}.${index}.length`, `${prefix}.${index}.width`, `${prefix}.${index}.height`, `${prefix}.${index}.area`])
			.flat()
			.filter((row) => row !== excludeField);
	}

	getAllCustomsFields(excludeField, prefix = 'floor', suffix = []) {
		return this.floors
			.map((floor, index) => suffix.map((sfx) => `${prefix}.${index}.${sfx}`))
			.flat()
			.filter((row) => row !== excludeField);
	}

	getNepaliHighestFloorValue() {
		const heightRelevantFloors = this.heightRelevantFloors;
		return (
			heightRelevantFloors &&
			heightRelevantFloors.length > 0 &&
			floorMappingNepali.find(
				(fl) => fl.floor === heightRelevantFloors.reduce((prev, current) => (prev.floor > current.floor ? prev : current)).floor
			).value
		);
	}

	getNepaliHighestFloorObject() {
		return this.heightRelevantFloors && this.heightRelevantFloors.length > 0
			? floorMappingAll.find(
					(fl) => fl.floor === this.heightRelevantFloors.reduce((prev, current) => (prev.floor > current.floor ? prev : current)).floor
			  )
			: {};
	}

	getSumOfAreas() {
		try {
			return this.areaRelevantFloors.reduce((acc, curr) => acc + getNumber(parseFloat(curr.area)), 0.0);
		} catch {
			return 0;
		}
	}

	getSumOfHeights() {
		try {
			return this.heightRelevantFloors.reduce((acc, curr) => acc + getNumber(parseFloat(curr.height)), 0.0);
		} catch {
			return 0;
		}
	}

	getPlinthFloor() {
		const gFloor = this.getFloorByFloor(1);
		if (isEmpty(gFloor)) {
			return {};
		} else {
			return {
				plinthDetails: gFloor.area,
				plinthLength: gFloor.length,
				plinthWidth: gFloor.width,
			};
		}
	}

	fetchHeightRelevantFloors() {
		return this.floors.filter((fl) => fl.floor < 11).map((flr) => flr.getFloorObject());
	}

	fetchAreaRelevantFloors() {
		return this.floors.filter((fl) => fl.floor < 11).map((flr) => flr.getFloorObject());
	}

	getTopAndBottomFloors() {
		// const heightRelevantFloors = this.heightRelevantFloors;
		// const topFlr = heightRelevantFloors.reduce((prev, current) => (prev.floor > current.floor ? prev : current)).floor;
		// const bottomFlr = heightRelevantFloors.reduce((prev, current) => (prev.floor < current.floor ? prev : current)).floor;

		// const topFloor = floorMappingAll.find(fl => fl.floor === topFlr);
		// const bottomFloor = floorMappingAll.find(fl => fl.floor === bottomFlr);

		return { topFloor: this.getTopFloor(), bottomFloor: this.getBottomFloor() };
	}

	getTopFloor() {
		const topFlr = this.heightRelevantFloors.reduce((prev, current) => (prev.floor > current.floor ? prev : current), {}).floor;
		const topFloor = floorMappingAll.find((fl) => fl.floor === topFlr);
		return topFloor || {};
	}

	getBottomFloor() {
		const bottomFlr = this.heightRelevantFloors.reduce((prev, current) => (prev.floor < current.floor ? prev : current), {}).floor;
		const bottomFloor = floorMappingAll.find((fl) => fl.floor === bottomFlr);
		return bottomFloor;
	}
}

export class Floor {
	constructor(length, height, width, area, floor) {
		this.length = length;
		this.height = height;
		this.width = width;
		this.area = area;
		this.floor = floor;
	}

	getFloor() {
		return this.floor;
	}

	getFloorObject() {
		return {
			length: this.length,
			height: this.height,
			width: this.width,
			area: this.area,
			floor: this.floor,
		};
	}
}

export const getGroundFloor = (floorArray) => {
	const groundFloor = floorArray.find((fl) => fl.floor === 1);
	if (groundFloor) {
		return {
			plinthDetails: groundFloor.area,
			plinthLength: groundFloor.length,
			plinthWidth: groundFloor.width,
		};
	} else {
		return {};
	}
};

export const getFloorValue = (inputFloor) => {
	try {
		return floorMappingFlat.find((floor) => floor.floor === parseInt(inputFloor)).value;
	} catch {
		return 'पहिलो तला';
	}
};

export const getNameFormat = (name) => {
	return name.replace(/[.,:\s+]/g, '');
};

export const floorMappingFlat = [
	{ floor: 0, value: 'भुमिगत तला' },
	{ floor: 1, value: 'भुइँ तला' },
	{ floor: 2, value: 'पहिलो तला' },
	{ floor: 3, value: 'दोस्रो तला' },
	{ floor: 4, value: 'तेस्रो तला' },
	{ floor: 5, value: 'चौथो तला' },
	{ floor: 6, value: 'पाँचौं तला' },
	{ floor: 7, value: 'छैठौं तल्ला' },
	{ floor: 8, value: 'सातौं तला' },
	{ floor: 9, value: 'आठौं तला' },
	{ floor: 10, value: 'नवौं तला' },
	// otherFloors: {
	{ floor: 11, value: 'शौचालय' },
	{ floor: 12, value: 'कम्पाउण्डवाल' },
	{ floor: 13, value: 'Running Feet' },
	// }
];

export const getFloorEnglish = (inputFloor) => {
	try {
		return floorMappingFlatEnglish.find((floor) => floor.floor === parseInt(inputFloor)).value;
	} catch {
		return 'Ground floor';
	}
};

export const floorMappingFlatEnglish = [
	{ floor: 0, value: 'Underground floor' },
	{ floor: 1, value: 'Ground floor' },
	{ floor: 2, value: 'First floor' },
	{ floor: 3, value: 'Second floor' },
	{ floor: 4, value: 'Third floor' },
	{ floor: 5, value: 'Fourth floor' },
	{ floor: 6, value: 'Fifth floor' },
	{ floor: 7, value: 'Sixth floor' },
	{ floor: 8, value: 'Seventh floor' },
	{ floor: 9, value: 'Eighth floor' },
	{ floor: 10, value: 'Ninth floor' },
];

export const floorMappingCount = [
	{ floor: 0, value: 'से.मी. / वेसमेन्ट' },
	{ floor: 1, value: 'जमिन/भुई तला' },
	{ floor: 2, value: '१' },
	{ floor: 3, value: '२' },
	{ floor: 4, value: '३' },
	{ floor: 5, value: '४' },
	{ floor: 6, value: '५' },
	{ floor: 7, value: '६' },
	{ floor: 8, value: '७' },
	{ floor: 9, value: '८' },
	{ floor: 10, value: '९' },
	// otherFloors: {
	{ floor: 11, value: 'शौचालय' },
	{ floor: 12, value: 'कम्पाउण्डवाल' },
	{ floor: 13, value: 'Running Feet' },
	// }
];

export const floorMappingNepali = [
	{ floor: 0, value: 'से.मी. / वेसमेन्ट' },
	{ floor: 1, value: '१' },
	{ floor: 2, value: '२' },
	{ floor: 3, value: '३' },
	{ floor: 4, value: '४' },
	{ floor: 5, value: '५' },
	{ floor: 6, value: '६' },
	{ floor: 7, value: '७' },
	{ floor: 8, value: '८' },
	{ floor: 9, value: '९' },
	{ floor: 10, value: '१०' },
	// otherFloors: {
	{ floor: 11, value: 'शौचालय' },
	{ floor: 12, value: 'कम्पाउण्डवाल' },
	{ floor: 13, value: 'Running Feet' },
	// }
];

export const floorMappingCountEng = [
	{ floor: 0, value: 'Underground' },
	{ floor: 1, value: '1' },
	{ floor: 2, value: '2' },
	{ floor: 3, value: '3' },
	{ floor: 4, value: '4' },
	{ floor: 5, value: '5' },
	{ floor: 6, value: '6' },
	{ floor: 7, value: '7' },
	{ floor: 8, value: '8' },
	{ floor: 9, value: '9' },
	{ floor: 10, value: '10' },
	// otherFloors: {
	{ floor: 11, value: 'शौचालय' },
	{ floor: 12, value: 'कम्पाउण्डवाल' },
	{ floor: 13, value: 'Running Feet' },
	// }
];

export const floorMappingAll = [
	{
		floor: 0,
		englishName: 'Underground floor',
		englishCount: 'Underground',
		englishCountName: 'Underground',
		nepaliName: 'भुमिगत तला',
		nepaliCountName: 'से.मी. / वेसमेन्ट',
		nepaliCount: 'से.मी. / वेसमेन्ट',
	},
	{
		floor: 1,
		englishName: 'Ground floor',
		englishCount: '1',
		englishCountName: 'Ground Floor',
		nepaliName: 'भुइँ तला',
		nepaliCountName: 'जमिन/भुई तला',
		nepaliCount: '१',
	},
	{
		floor: 2,
		englishName: 'First floor',
		englishCount: '2',
		englishCountName: '2',
		nepaliName: 'पहिलो तला',
		nepaliCountName: '१',
		nepaliCount: '२',
	},
	{
		floor: 3,
		englishName: 'Second floor',
		englishCount: '3',
		englishCountName: '3',
		nepaliName: 'दोस्रो तला',
		nepaliCountName: '२',
		nepaliCount: '३',
	},
	{
		floor: 4,
		englishName: 'Third floor',
		englishCount: '4',
		englishCountName: '4',
		nepaliName: 'तेस्रो तला',
		nepaliCountName: '३',
		nepaliCount: '४',
	},
	{
		floor: 5,
		englishName: 'Fourth floor',
		englishCount: '5',
		englishCountName: '5',
		nepaliName: 'चौथो तला',
		nepaliCountName: '४',
		nepaliCount: '५',
	},
	{
		floor: 6,
		englishName: 'Fifth floor',
		englishCount: '6',
		englishCountName: '6',
		nepaliName: 'पाँचौं तला',
		nepaliCountName: '५',
		nepaliCount: '६',
	},
	{
		floor: 7,
		englishName: 'Sixth floor',
		englishCount: '7',
		englishCountName: '7',
		nepaliName: 'छैठौं तल्ला',
		nepaliCountName: '६',
		nepaliCount: '७',
	},
	{
		floor: 8,
		englishName: 'Seventh floor',
		englishCount: '8',
		englishCountName: '8',
		nepaliName: 'सातौं तला',
		nepaliCountName: '७',
		nepaliCount: '८',
	},
	{
		floor: 9,
		englishName: 'Eighth floor',
		englishCount: '9',
		englishCountName: '9',
		nepaliName: 'आठौं तला',
		nepaliCountName: '८',
		nepaliCount: '९',
	},
	{
		floor: 10,
		englishName: 'Ninth floor',
		englishCount: '10',
		englishCountName: '10',
		nepaliName: 'नवौं तला',
		nepaliCountName: '९',
		nepaliCount: '१०',
	},
	{
		floor: 11,
		englishName: 'Toilet',
		englishCount: 'Toilet',
		englishCountName: 'Toilet',
		nepaliName: 'शौचालय',
		nepaliCountName: 'शौचालय',
		nepaliCount: 'शौचालय',
	},
	{
		floor: 12,
		englishName: 'Compound Wall',
		englishCount: 'Compound Wall',
		englishCountName: 'Compound Wall',
		nepaliName: 'कम्पाउण्डवाल',
		nepaliCountName: 'कम्पाउण्डवाल',
		nepaliCount: 'कम्पाउण्डवाल',
	},{
		floor: 13,
		englishName: 'Running Feet',
		englishCount: 'Running Feet',
		englishCountName: 'Running Feet',
		nepaliName: 'Running Feet',
		nepaliCountName: 'Running Feet',
		nepaliCount: 'Running Feet',
	},
];

export const surroundingMappingFlat = [
	{ side: 1, value: 'पूर्व' },
	{ side: 2, value: 'पश्चिम' },
	{ side: 3, value: 'उत्तर' },
	{ side: 4, value: 'दक्षिण' },
];

export const groupNameDefault = [
	{ id: 1, name: 'पहिलो चरण' },
	{ id: 2, name: 'दोस्रो चरण' },
	{ id: 3, name: 'तेस्रो चरण' },
	{ id: 4, name: 'चौथो चरण' },
];

export const getGroupName = () => {
	try {
		return JSON.parse(getLocalStorage(FORM_GROUP_MASTER));
	} catch {
		return groupNameDefault;
	}
};
// Architecture form

export const prepareArchiData = (data) => {
	let prevData = {};
	const listData = data ? data.listData : [];
	// console.log('New Data -- ', listData);

	const groups = {};

	// Get unique groups
	getUniqueFromArray(listData, 'groupId').forEach((groupId) => {
		const groupName = listData.filter((row) => row.groupId === groupId)[0].groupName;
		Object.assign(groups, { [groupId]: groupName });
	});

	// Retrieve previously populated data
	listData
		.filter((row) => row.qty !== null || row.remarks !== null)
		.forEach((filtered) => {
			const keyQty = `${replaceDot(filtered.classId)}_qty`;
			const keyRemark = `${replaceDot(filtered.classId)}_remarks`;
			const keyUsedUnit = `${replaceDot(filtered.classId)}_usedUnit`;
			Object.assign(prevData, {
				[keyQty]: filtered.qty,
				[keyRemark]: filtered.remarks,
				[keyUsedUnit]: filtered.usedUnit === null ? '' : filtered.usedUnit,
			});
		});

	return { prevData, listData, groups };
};

export const prepareElecData = (data) => {
	let prevData = {};
	const listData = data ? data.details : [];
	const groups = {};

	// Get unique groups
	getUniqueFromArray(listData, 'groupName').forEach((grpName, index) => {
		const groupName = listData.find((row) => row.groupName === grpName).groupName;
		Object.assign(groups, { [index]: groupName });
	});

	// Retrieve previously populated data
	listData
		.filter((row) => (row.qty !== null || row.remark !== null) && !isStringEmpty(row.element))
		.forEach((filtered) => {
			const keyQty = `${replaceDot(filtered.elementId)}_qty`;
			const keyRemark = `${replaceDot(filtered.elementId)}_remark`;
			const keyUsedUnit = `${replaceDot(filtered.elementId)}_usedUnit`;
			Object.assign(prevData, {
				[keyQty]: filtered.qty,
				[keyRemark]: filtered.remark,
				[keyUsedUnit]: filtered.usedUnit === null || filtered.usedUnit === undefined ? '' : filtered.usedUnit,
			});
		});
	return { prevData, listData, groups };
};

export const filterKeys = (allowed, raw) =>
	!!raw
		? Object.keys(raw)
				.filter((key) => allowed.includes(key))
				.reduce((obj, key) => {
					obj[key] = raw[key];
					return obj;
				}, {})
		: raw;

export const areaUnitOptions = [
	{ key: 1, value: 'SQUARE METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'SQUARE FEET', text: 'वर्ग फिट' },
];

export const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

export const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

/**
 *
 * @param {} prevData Form previous data object
 * @param  {...UserType} userTypes User types to fetch approval status from (UserType.ADMIN, UserType.ENGINEER...)
 *
 * Used to fetch name, designation and date of person who approved the form
 * Usage: const { chiefInfo, erInfo } = getApprovalDataa(prevData, UserType.ADMIN, UserType.ENGINEER)
 * @returns Object with corresponding userType info objects { chiefInfo, erInfo, aminInfo, serInfo, rwInfo }
 */
export const getApprovalData = (prevData, ...userTypes) => {
	let returnObj = {};
	if (!prevData) {
		return returnObj;
	}
	userTypes.forEach((userType) => {
		switch (userType) {
			case UserType.ADMIN:
				returnObj.chiefInfo = {};
				if (prevData.chiefStatus === 'A') {
					returnObj.chiefInfo.chiefName = prevData.chiefName;
					returnObj.chiefInfo.chiefDesignation = getUserTypeValueNepali(UserType.ADMIN);
					returnObj.chiefInfo.chiefDate = getNepaliDate(prevData.chiefDate);
				}
				break;
			case UserType.ENGINEER:
				returnObj.erInfo = {};
				if (prevData.erStatus === 'A') {
					returnObj.erInfo.erName = prevData.erName;
					returnObj.erInfo.erDesignation = getUserTypeValueNepali(UserType.ENGINEER);
					returnObj.erInfo.erDate = getNepaliDate(prevData.erDate);
				}
				break;
			case UserType.SUB_ENGINEER:
				returnObj.serInfo = {};
				if (prevData.serStatus === 'A') {
					returnObj.serInfo.serName = prevData.serName;
					returnObj.serInfo.serDesignation = getUserTypeValueNepali(UserType.SUB_ENGINEER);
					returnObj.serInfo.serDate = getNepaliDate(prevData.serDate);
				}
				break;
			case UserType.AMIN:
				returnObj.aminInfo = {};
				if (prevData.aminStatus === 'A') {
					returnObj.aminInfo.serName = prevData.aminiName;
					returnObj.aminInfo.serDesignation = getUserTypeValueNepali(UserType.AMIN);
					returnObj.aminInfo.aminDate = getNepaliDate(prevData.aminiDate);
				}
				break;
			case UserType.RAJASWO:
				returnObj.rwInfo = {};
				if (prevData.rwStatus === 'A') {
					returnObj.rwInfo.rwName = prevData.rwName;
					returnObj.rwInfo.rwDesignation = getUserTypeValueNepali(UserType.RAJASWO);
					returnObj.rwInfo.rwDate = getNepaliDate(prevData.rwDate);
				}
				break;
			default:
				break;
		}
	});

	return returnObj;
};

export const getFormSaveUserData = (prevData, userName, ...userTypes) => {
	let returnObj = {};

	userTypes.forEach((userType) => {
		switch (userType) {
			case UserType.ADMIN:
				returnObj.chiefInfo = {};
				returnObj.chiefInfo.chiefName = userName;
				returnObj.chiefInfo.chiefDesignation = getUserTypeValueNepali(UserType.ADMIN);
				returnObj.chiefInfo.chiefDate = getCurrentDate(true);
				break;
			case UserType.ENGINEER:
				returnObj.erInfo = {};
				returnObj.erInfo.erName = userName;
				returnObj.erInfo.erDesignation = getUserTypeValueNepali(UserType.ENGINEER);
				returnObj.erInfo.erDate = getCurrentDate(true);
				break;
			case UserType.SUB_ENGINEER:
				returnObj.serInfo = {};
				returnObj.serInfo.serName = userName;
				returnObj.serInfo.serDesignation = getUserTypeValueNepali(UserType.SUB_ENGINEER);
				returnObj.serInfo.serDate = getCurrentDate(true);
				break;
			case UserType.AMIN:
				returnObj.aminInfo = {};
				returnObj.aminInfo.serName = userName;
				returnObj.aminInfo.serDesignation = getUserTypeValueNepali(UserType.AMIN);
				returnObj.aminInfo.aminDate = getCurrentDate(true);
				break;
			case UserType.RAJASWO:
				returnObj.rwInfo = {};
				returnObj.rwInfo.rwName = userName;
				returnObj.rwInfo.rwDesignation = getUserTypeValueNepali(UserType.RAJASWO);
				returnObj.rwInfo.rwDate = getCurrentDate(true);
				break;
			default:
				break;
		}
	});

	return returnObj;
};

export const handleSuccess = (prevData, parentProps, success, nextUrl = undefined) => {
	let statusArray = [];

	if (prevData) {
		statusArray.push(prevData.erStatus || 'P');
		statusArray.push(prevData.serStatus || 'P');
		statusArray.push(prevData.rwStatus || '');
		statusArray.push(prevData.aminiStatus || '');
		statusArray.push(prevData.chiefStatus || '');
		statusArray.push(prevData.eStatus || '');
		statusArray.push(prevData.fStatus || '');
		statusArray.push(prevData.gStatus || '');
	}

	showToast(success.data.message || 'Data saved successfully');
	if (!statusArray.includes('R')) {
		setTimeout(() => {
			const url = nextUrl || getNextUrl(parentProps.location.pathname);
			parentProps.history.push(url);
		}, 2000);
	}
};

export const handleSuccessIm = (prevData, formUrl, history, success, nextUrl = undefined) => {
	let statusArray = [];

	if (prevData) {
		statusArray.push(prevData.erStatus || 'P');
		statusArray.push(prevData.serStatus || 'P');
		statusArray.push(prevData.rwStatus || '');
		statusArray.push(prevData.aminiStatus || '');
		statusArray.push(prevData.chiefStatus || '');
		statusArray.push(prevData.eStatus || '');
		statusArray.push(prevData.fStatus || '');
		statusArray.push(prevData.gStatus || '');
	}

	showToast(success.data.message || 'Data saved successfully');
	if (!statusArray.includes('R')) {
		setTimeout(() => {
			const url = nextUrl || getNextUrl(formUrl);
			history.push(url);
		}, 2000);
	}
};

export function getRenewInfo(renewData) {
	try {
		const { tilldate } = renewData;

		let needsRenew = false,
			urgent = false;

		const daysRemaining = getIntNumber(getDateCount(tilldate));
		if (daysRemaining < DAYS_TO_NOTIFY && daysRemaining > DAYS_TO_URGENCY) {
			needsRenew = true;
		} else if (daysRemaining <= DAYS_TO_URGENCY) {
			needsRenew = true;
			urgent = true;
		}
		return { needsRenew, tilldate: translateEngToNep(tilldate), urgent, daysRemaining };
	} catch (err) {
		console.log('err', err);
		return { needsRenew: false, tilldate: '', urgent: false, daysRemaining: 0 };
	}
}

export const toArray = (field) => {
	if (field.includes('[')) {
		const returnVal = field.replace(/[[\]]+/g, '');
		const retArr = returnVal.split(',');
		return retArr.map((val) => val.trim());
	} else return field;
};

export const getOptionText = (value, option) => {
	try {
		return option.find((row) => String(row.value) === String(value)).text;
	} catch (err) {
		return '';
	}
};

export const filterFields = (excludeField, fields) => {
	return fields.filter((row) => row !== excludeField);
};

export const getRelatedFields = (excludeField, dataArray, prefix, suffix = []) => {
	return dataArray
		.map((_, index) => suffix.map((sfx) => `${prefix}.${index}.${sfx}`))
		.flat()
		.filter((row) => row !== excludeField);
};

// export const floors = [
// 	{ value: 0, text: 'भुमिगत तला', value: 'भुमिगत तला' },
// 	{ value: 1, text: 'भुइँ तला', value: 'भुइँ तला' },
// 	{ value: 2, text: 'पहिलो तला', value: 'पहिलो तला' },
// 	{ value: 3, text: 'दोस्रो तला', value: 'दोस्रो तला' },
// 	{ value: 4, text: 'तेस्रो तला', value: 'तेस्रो तला' },
// 	{ value: 5, text: 'चौथो तला', value: 'चौथो तला' },
// 	{ value: 6, text: 'पाँचौं तला', value: 'पाँचौं तला' },
// 	{ value: 7, text: 'छैठौं तल्ला', value: 'छैठौं तल्ला' },
// 	{ value: 8, text: 'सातौं तला', value: 'सातौं तला' },
// 	{ value: 9, text: 'आठौं तला', value: 'आठौं तला' },
// 	{ value: 10, text: 'नवौं तला', value: 'नवौं तला' },
// 	// othervalues: {
// 	{ value: 11, text: 'शौचालय', value: 'शौचालय' },
// 	{ value: 12, text: 'कम्पाउण्डवाल', value: 'कम्पाउण्डवाल' },
// 	// }
// ];

export const floorOptions = floorMappingFlat.map((floor) => ({ value: floor.floor, text: floor.value, disabled: false }));

export const blocks = [
	{ key: 0, text: 'A', value: 'A' },
	{ key: 1, text: 'B', value: 'B' },
	{ key: 2, text: 'C', value: 'C' },
	{ key: 3, text: 'D', value: 'D' },
];
