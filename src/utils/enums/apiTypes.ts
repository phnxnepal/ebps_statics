export type MenuList = [{
    menu: string;
    menu_type: 'S' | 'P';
    url: string;
}]

export type NamsariStatus = 'C' | 'N' | 'R' | '';

export type TPermitMenu = {
    approveBy: string | null;
    enterBy: string | null;
    formName: string;
    groupId: number | null;
    id: number | null;
    position: number | null;
    viewURL: string;
}

export type TCertificateNoteData = {
    nameTransferId: number;
    jsonData: string;
}

export type TPermitGetResponse = {
    data: {
        buildingClass: string;
        naamsariStatus: NamsariStatus;
        certificateNoteData?: TCertificateNoteData[];
        menu: TPermitMenu[];
        data: {}
    }
}

export type TTaskListRow = {
    designerChangeStatus: string;
    nameTransaferId: number;
    newDesignerName: string;
    yourStatus: string;
}