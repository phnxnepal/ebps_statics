export enum AreaUnitNepali {
    MountainousCustomaryUnit = 'रोपनी–आना–पैसा–दाम',
    TeraiCustomaryUnit = 'बिघा–कठ्ठा–धुर',
    TeraiCustomaryPaiUnit = 'बिघा–कठ्ठा–धुर–पाई',
    Feet = 'वर्ग फिट',
    Metre = 'वर्ग मिटर',
}

export const UNITS = {
    FEET: {
        AREA: 'SQUARE FEET',
        LENGTH: 'FEET',
    },
    METRE: {
        AREA: 'SQUARE METRE',
        LENGTH: 'METRE',
    },
}