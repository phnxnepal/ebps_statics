export enum LSKey {
    USER_MENU = "PHOENIX_USER_MENU",
    USER_TYPE_ALL = "PHOENIX_USER_TYPE_ALL",
    FORM_GROUP = "PHOENIX_FORM_GROUP",
    USER_DETAILS = "PHOENIX_USER_DETAILS",
    HAS_DESIGNER_CHANGED = "PHOENIX_HAS_DESIGNER_CHANGED",
    NEW_DESIGNER_NAME = "PHOENIX_NEW_DESIGNER_NAME",
    NAMSARI_STATUS = "PHOENIX_NAMSARI_STATUS",
    CERTIFICATE_NOTE_DATA = "PHOENIX_CERTIFICATE_NOTE",
}