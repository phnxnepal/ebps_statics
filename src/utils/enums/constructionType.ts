import { getOptionText } from "../dataUtils";
import { constructionTypeOptionsTextAll } from "../optionUtils";

export enum ConstructionType {
    NAYA_NIRMAN = 'नयाँ निर्माण',
    PURANO_GHAR = 'पुरानो घर',
    TALLA_THAP = 'तला थप',
}

export enum ConstructionTypePermit {
    NAYA_NIRMAN = 'नयाँ',
    PURANO_GHAR = 'पुरानो',
    TALLA_THAP = 'तला थप',
}

export enum ConstructionTypeValue {
    NAYA_NIRMAN = '1',
    PURANO_GHAR = '2',
    TALLA_THAP = '3',
    NAYA_NIRMAN_EARTHQUAKE = 'EQ',
}

export const NAYA_NIRMAN_EARTHQUAKE = 'नयाँ निर्माण भूकम्प प्रतिरोधक';

export const getConstructionTypeValue = (data: string): string => {
    switch (data) {
        case ConstructionType.NAYA_NIRMAN:
            return ConstructionTypeValue.NAYA_NIRMAN
        case ConstructionType.PURANO_GHAR:
            return ConstructionTypeValue.PURANO_GHAR
        case ConstructionType.TALLA_THAP:
            return ConstructionTypeValue.TALLA_THAP
        default:
            return data;
    }
}

export const getConstructionTypeValueProcess = (data: string): string => {
    switch (data) {
        case ConstructionType.NAYA_NIRMAN:
            return ConstructionTypeValue.NAYA_NIRMAN
        case ConstructionType.PURANO_GHAR:
            return ConstructionTypeValue.PURANO_GHAR
        case ConstructionType.TALLA_THAP:
            return ConstructionTypeValue.TALLA_THAP
        default:
            return ConstructionTypeValue.NAYA_NIRMAN;
    }
}

export const getCertificateConstructionType = (data: string): string => {
    switch (getConstructionTypeValue(data)) {
        case ConstructionTypeValue.NAYA_NIRMAN:
            return ConstructionType.NAYA_NIRMAN.split(' ')[0]
        case ConstructionTypeValue.PURANO_GHAR:
            return ConstructionType.PURANO_GHAR
        case ConstructionTypeValue.TALLA_THAP:
            return ConstructionType.TALLA_THAP
        default:
            return data;
    }
}

export const getCertificateConstructionTypeAll = (data: string): string => {
    switch (getConstructionTypeValue(data)) {
        case ConstructionTypeValue.NAYA_NIRMAN:
            return ConstructionType.NAYA_NIRMAN.split(' ')[0]
        case ConstructionTypeValue.PURANO_GHAR:
            return ConstructionType.PURANO_GHAR
        case ConstructionTypeValue.TALLA_THAP:
            return ConstructionType.TALLA_THAP
        default:
            return getOptionText(data, constructionTypeOptionsTextAll);
    }
}

export const getDataConstructionType = (data: string): string => {
    switch (getConstructionTypeValue(data)) {
        case ConstructionTypeValue.NAYA_NIRMAN:
            return ConstructionType.NAYA_NIRMAN;
        case ConstructionTypeValue.PURANO_GHAR:
            return ConstructionType.PURANO_GHAR
        case ConstructionTypeValue.TALLA_THAP:
            return ConstructionType.TALLA_THAP
        default:
            return data;
    }
}

export const getDataConstructionTypeAll = (data: string): string => {
    switch (getConstructionTypeValue(data)) {
        case ConstructionTypeValue.NAYA_NIRMAN:
            return ConstructionType.NAYA_NIRMAN;
        case ConstructionTypeValue.PURANO_GHAR:
            return ConstructionType.PURANO_GHAR
        case ConstructionTypeValue.TALLA_THAP:
            return ConstructionType.TALLA_THAP
        default:
            return getOptionText(data, constructionTypeOptionsTextAll);
    }
}

export const getConstructionTypeText = (value: string, isCertificate = false) => {
    switch (value) {
        case ConstructionTypeValue.NAYA_NIRMAN:
            return isCertificate ? ConstructionType.NAYA_NIRMAN.split(' ')[0] : ConstructionType.NAYA_NIRMAN
        case ConstructionTypeValue.PURANO_GHAR:
            return ConstructionType.PURANO_GHAR
        case ConstructionTypeValue.TALLA_THAP:
            return ConstructionType.TALLA_THAP
        default:
            return getOptionText(value, constructionTypeOptionsTextAll) || value;
    }
}