import { FloorArray, cleanFormData } from './dataUtils';
import { DEFAULT_UNIT_LENGTH } from './constants';

test('should return empty array for empty floors', () => {
	const floorArray = new FloorArray([]);
	expect(floorArray.getFloors()).toEqual([]);
	expect(floorArray.getFloorUnit()).toBe(DEFAULT_UNIT_LENGTH);
	expect(floorArray.getHeightRelevantFloors()).toEqual([]);
	expect(floorArray.getNepaliHighestFloorObject()).toEqual({});
	expect(floorArray.getLength()).toBe(0);
});

test('should return empty array for undefined floor input', () => {
	const floorArray = new FloorArray(undefined);
	expect(floorArray.getFloors()).toEqual([]);
	expect(floorArray.getFloorUnit()).toBe(DEFAULT_UNIT_LENGTH);
	expect(floorArray.getHeightRelevantFloors()).toEqual([]);
	expect(floorArray.getNepaliHighestFloorObject()).toEqual({});
	expect(floorArray.getSumOfHeights()).toBe(0);
	expect(floorArray.getSumOfAreas()).toBe(0);
	expect(floorArray.getPlinthFloor()).toEqual({});
	expect(floorArray.getLength()).toBe(0);
});

test('should return sorted floors', () => {
	const floorArray = new FloorArray([
		{ floorUnit: 'FEET', floor: 1, length: 12, width: 12, area: 144, height: 12 },
		{ floorUnit: 'FEET', floor: 0, length: 12, width: 12, area: 144, height: 12 },
		{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12 },
	]);
	expect(floorArray.getFloorUnit()).toBe('FEET');
	expect(floorArray.getLength()).toBe(3);
	expect(floorArray.getSumOfHeights()).toBe(36);
	expect(floorArray.getSumOfAreas()).toBe(144 + 144 + 144);
	expect(floorArray.getPlinthFloor()).toEqual({
		plinthDetails: 144,
		plinthLength: 12,
		plinthWidth: 12,
	});
	expect(floorArray.getFloorsWithUnit()).toEqual([
		{ floorUnit: 'FEET', floor: 0, length: 12, width: 12, area: 144, height: 12 },
		{ floorUnit: 'FEET', floor: 1, length: 12, width: 12, area: 144, height: 12 },
		{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12 },
	]);
});

test('should return correct nepali floor units', () => {
	const floorArray = new FloorArray([
		{ floorUnit: 'FEET', floor: 0, length: 12, width: 12, area: 144, height: 12 },
		{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12 },
	])

	expect(floorArray.getFloorUnit()).toBe('FEET')
	expect(floorArray.getNepaliFloorUnit()).toEqual({ unit: 'फिट', areaUnit: 'वर्ग फिट'})
})

describe('cleanFormData unit test', () => {
	const obj = { name: 'Roger', age: 29, edu: undefined };
	const objNull = null;

	test('should return clean object', () => {
		const cleanObj = cleanFormData(obj, ['name', 'edu']);
		expect(cleanObj).toEqual({ name: 'Roger' });
	});

	test('should return clean object from different object', () => {
		const obj1 = { name1: 'Roger', age1: 29, edu1: undefined };
		const cleanObj = cleanFormData(obj1, ['name', 'edu']);
		expect(cleanObj).toEqual({});
	});

	test('should return clean object for undefined reqFields', () => {
		const cleanObj = cleanFormData(obj);
		expect(cleanObj).toEqual({ name: 'Roger', age: 29 });
	});

	test('should return clean object for [] reqFields', () => {
		const cleanObj = cleanFormData(obj, []);
		expect(cleanObj).toEqual({ name: 'Roger', age: 29 });
	});

	test('should return empty object for null or undefined', () => {
		const objUndefined = undefined;
		const cleanObjNull = cleanFormData(objNull, ['name', 'edu']);
		const cleanObjUndefined = cleanFormData(objUndefined, ['name', 'edu']);
		expect(cleanObjNull).toEqual({});
		expect(cleanObjUndefined).toEqual({});
	});
});
