import { NepaliNumberToWord } from "./nepaliAmount"

test('should output valid amount in figure', () => {
    expect(NepaliNumberToWord.getNepaliWord(1000)).toBe('एक हजार रुपैँया मात्र।')
})

test('should output valid amount for 1', () => {
    expect(NepaliNumberToWord.getNepaliWord(1)).toBe('एक रुपैँया मात्र।')
})

test('should output valid amount for 0', () => {
    expect(NepaliNumberToWord.getNepaliWord(0)).toBe('सुन्य रुपैँया मात्र।')
})

test('should output valid amount for 4008', () => {
    expect(NepaliNumberToWord.getNepaliWord(4008)).toBe('चार हजार आठ रुपैँया मात्र।')
})

test('should output valid amount for 1217463.78', () => {
    expect(NepaliNumberToWord.getNepaliWord(1217463.78)).toBe('बाह्र लाख सत्र हजार चार सय त्रिसट्ठी रुपैँया अठहत्तर पैसा मात्र।')
})