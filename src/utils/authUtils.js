import { getUserInfoObj } from './functionUtils';
import decode from 'jwt-decode';

export const isAuthenticated = () => {
  try {
    const userInfoObj = getUserInfoObj();
    const { exp } = decode(userInfoObj.token);
    if (Date.now() >= exp * 1000) {
      return false;
    }
    return true;
  } catch {
    return false;
  }
};
