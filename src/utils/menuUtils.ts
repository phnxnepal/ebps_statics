import { UserType, allUsers } from './userTypeUtils';

export enum MenuId {
	PERMIT_ADD = 'building-permit',
	PERMIT_ADD_TASK = 'naya-nirman-task',
	PERMIT_OLD_ADD = 'building-permit-purano',
	PERMIT_OLD_ADD_TASK = 'purano-ghar-task',
	ADD_STOREY_ADD = 'talla-thap-setup',
	ADD_STOREY_TASK = 'completed-list',
	ADD_STOREY_SEARCH = 'talla-thap-search',
	NAMSARI_ADD = 'namsari-setup',
	NAMSARI_TASK = 'namsari-task',
	DISCARD_ADD = 'discard-setup',
	DISCARD_SEARCH = 'discard-search',
	MYAD_ADD = 'myad-setup',
	MYAD_APPROVE = 'myadApprove-setup',
}

export const ProcessMenuObject = {
	[MenuId.PERMIT_ADD]: { id: 'building-permit', url: '/user/building-permit' },
	[MenuId.PERMIT_OLD_ADD]: { id: 'building-permit-purano', url: '/user/building-permit-purano' },
	[MenuId.ADD_STOREY_TASK]: { id: 'completed-list', url: '/user/completed-list' },
	[MenuId.PERMIT_ADD_TASK]: { id: 'naya-nirman-task', url: '/user/task-search/naya-nirman' },
	[MenuId.PERMIT_OLD_ADD_TASK]: { id: 'purano-ghar-task', url: '/user/task-search/purano-ghar' },
	[MenuId.ADD_STOREY_ADD]: { id: 'talla-thap-setup', url: '/user/talla-thap-setup' },
	[MenuId.ADD_STOREY_SEARCH]: { id: 'talla-thap-search', url: '/user/task-search/talla-thap' },
	[MenuId.NAMSARI_ADD]: { id: 'namsari-setup', url: '/user/namsari-setup' },
	[MenuId.NAMSARI_TASK]: { id: 'namsari-task', url: '/user/task-search/namsari' },
	[MenuId.DISCARD_ADD]: { id: 'discard-setup', url: '/user/discard-setup' },
	[MenuId.MYAD_ADD]: { id: 'myad-setup', url: '/user/myad-setup' },
	[MenuId.MYAD_APPROVE]: { id: 'myadApprove-setup', url: '/user/myadApprove-setup' },
};

export const getMenuUsers = (menuId: MenuId) => {
	switch (menuId) {
		case MenuId.PERMIT_ADD:
			return [UserType.DESIGNER];
		case MenuId.PERMIT_OLD_ADD:
			return [UserType.DESIGNER];
		case MenuId.PERMIT_ADD_TASK:
			return allUsers;
		case MenuId.PERMIT_OLD_ADD_TASK:
			return allUsers;
		case MenuId.ADD_STOREY_ADD:
			return [UserType.ENGINEER];
		case MenuId.ADD_STOREY_TASK:
			return [UserType.DESIGNER];
		case MenuId.ADD_STOREY_SEARCH:
			return allUsers;
		case MenuId.NAMSARI_ADD:
			return [UserType.ENGINEER];
		case MenuId.NAMSARI_TASK:
			return allUsers;
		default:
			return [];
	}
};

export const getSetupMenuByUser = (user: UserType) => {
	switch (user) {
		case UserType.ENGINEER:
			return [MenuId.ADD_STOREY_ADD, MenuId.NAMSARI_ADD];
		default:
			return [];
	}
};

export const MENU_GROUPS = {
	NAYA_NIRMAN: 'naya-nirman',
	PURANO_GHAR: 'purano-ghar',
	TALLA_THAP: 'talla-thap',
	NAMSARI: 'namsari',
	DISCARD: 'discard',
	MYAD: 'myad',
};
