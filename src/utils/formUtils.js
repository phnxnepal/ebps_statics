import { isStringEmpty } from './stringUtils';
import { getLocalStorage } from './secureLS';
import { MENU_LIST, FORM_NAME_MASTER, DEFAULT_UNIT_LENGTH } from './constants';
import { formApprovalFields, approvalFieldPrefixMapping } from './userTypeUtils';
import { LSKey } from './enums/localStorageKeys';
import { isEmpty, getUserInfoObj, getUserRole, getUserTypeValueNepali } from './functionUtils';
import { ApproveFormMap, DEFAULT } from './clientConfigs/approveFormMapping';
import { getCurrentDate, getNepaliDate } from './dateUtils';
import { checkError } from './dataUtils';
import { isArray } from 'util';

export const getClassName = (value, error) => {
	return !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
};

/**
 * @see postFormDataByUrl()
 * Parameter obj for postFormDataByUrl Action
 */
export class PostActionParams {
	constructor({ api, data, withAppId = false, concatAppId = true, fetchMenu = false, bClass = null }) {
		this.api = api;
		this.data = data;
		this.withAppId = withAppId;
		this.concatAppId = concatAppId;
		this.fetchMenu = fetchMenu;
		this.bClass = bClass;
	}

	getParams() {
		return [this.api, this.data, this.withAppId, this.concatAppId, this.fetchMenu, this.bClass];
	}
}

export class FormUtility {
	constructor(url) {
		this.url = url ? url.trim() : '';
		const formInfo = getFormInfo(url);
		this.savePermission = formInfo.enterBy === 'Y';
		this.approvePermission = formInfo.approveBy === 'Y';
		this.enterByUser = formInfo.enterByUser;
		this.prefixes = formInfo.prefixes;
		this.approveByUsers = formInfo.approveByUsers;
		this.userDetailsMaster = formInfo.userDetailsMaster;
	}

	getUrl = () => {
		return this.url;
	};

	getSavePermission = () => {
		return this.savePermission;
	};

	getApprovePermission = () => {
		return this.approvePermission;
	};

	getPrefixes = () => {
		return this.prefixes;
	};

	getEnterByUser = () => {
		return this.enterByUser;
	};

	getApproveByUsers = () => {
		return this.approveByUsers;
	};

	getStatusNameValues = (prevData) => {
		try {
			let isRejected = false;
			const prefixValues = this.prefixes.map(({ prefix, userType, userDesignation }) => {
				const status = prevData && prevData[`${prefix}Status`] ? prevData[`${prefix}Status`] : 'P';
				const name = prevData && prevData[`${prefix}Name`] ? prevData[`${prefix}Name`] : '';
				if (status === 'R') isRejected = true;
				return { statusField: `${prefix}Status`, status, userType, name, userDesignation };
			});
			return { prefixValues, isRejected };
		} catch (err) {
			console.log('Error', err);
			return { prefixValues: {}, isRejected: false };
		}
	};

	getPrefixValues = (prevData, suffix) => {
		const prefixValues = this.prefixes.map(({ prefix, userType }) => {
			const value = prevData && prevData[`${prefix}${suffix}`] ? prevData[`${prefix}${suffix}`] : '';
			return { field: `${prefix}${suffix}`, value, userType };
		});
		return prefixValues;
	};

	getApproveByUserDetails = (prevData) => {
		const data = this.approveByUsers.map((row) => {
			if (prevData) {
				const name = prevData[`${row.prefix}Name`] ? prevData[`${row.prefix}Name`] : '';
				let signature = '';
				if (!isStringEmpty(name) && this.userDetailsMaster && this.userDetailsMaster.length > 0) {
					const userDetailRow = this.userDetailsMaster.find((ud) => ud.usertype === row.userType);
					if (userDetailRow) {
						signature = userDetailRow.signature;
					}
				}
				return {
					...row,
					name,
					date: prevData[`${row.prefix}Date`] ? getNepaliDate(prevData[`${row.prefix}Date`]) : '',
					signature,
				};
			} else {
				return row;
			}
		});
		// return [{
		// 	date: getNepaliDate('2020-07-21'),
		// 	designation: "सब-इन्जिनियर",
		// 	name: "subjim ",
		// 	prefix: "ser",
		// 	signature: "/userdocument/id8/signature.png",
		// 	userType: "B",
		// },{
		// 	date: getNepaliDate('2020-07-21'),
		// 	designation: "सब-इन्जिनियर",
		// 	name: "er jim ",
		// 	prefix: "ser",
		// 	signature: "/userdocument/id8/signature.png",
		// 	userType: "A",
		// },{
		// 	date: getNepaliDate('2020-07-21'),
		// 	designation: "सब-इन्जिनियर",
		// 	name: "er jim ",
		// 	prefix: "ser",
		// 	signature: "/userdocument/id8/signature.png",
		// 	userType: "C",
		// }];
		return data;
	};
}

const getFormInfo = (url) => {
	try {
		const menuList = JSON.parse(getLocalStorage(MENU_LIST));
		const formMaster = JSON.parse(getLocalStorage(FORM_NAME_MASTER));
		const userTypeMaster = JSON.parse(getLocalStorage(LSKey.USER_TYPE_ALL));
		const userDetailsMaster = JSON.parse(getLocalStorage(LSKey.USER_DETAILS));
		const userInfo = getUserInfoObj();

		const thisFormRow = formMaster.find((form) => form.viewUrl && form.viewUrl.trim() === url);

		const approveByUsers = [];

		const prefixes = Object.keys(thisFormRow)
			.filter((field) => formApprovalFields.includes(field) && thisFormRow[field] === 'Y')
			.map((row) => {
				const userRow = approvalFieldPrefixMapping[row];
				const userDesignation = userTypeMaster ? userTypeMaster.find((uT) => uT.userType === userRow.userType).designation : '';
				approveByUsers.push({ designation: userDesignation, userType: userRow.userType, prefix: userRow.prefix });
				return { ...userRow, userDesignation };
			});

		let enterByUser = {
			designation: userTypeMaster ? userTypeMaster.find((user) => user.userType === thisFormRow.enterBy).designation : '',
			userType: thisFormRow.enterBy,
			name: '',
		};

		if (userInfo.userType === thisFormRow.enterBy) {
			enterByUser = {
				designation: userTypeMaster ? userTypeMaster.find((user) => user.userType === thisFormRow.enterBy).designation : '',
				userType: thisFormRow.enterBy,
				name: userInfo.userName,
			};
		}

		const menuRow = menuList.find((menu) => menu.viewURL && menu.viewURL.trim() === url);
		const { approveBy, enterBy } = menuRow ? menuRow : { approveBy: 'N', enterBy: 'N' };

		return { approveBy, enterBy, prefixes, enterByUser, approveByUsers, userDetailsMaster };
	} catch (err) {
		console.log('err', err);
		return { approveBy: 'N', enterBy: 'N', prefixes: [], enterByUser: {}, approveByUsers: [], userDetailsMaster: [] };
	}
};

export const getApprovalDate = (url, prevData) => {
	if (isEmpty(checkError(prevData))) return '';

	try {
		const formMaster = JSON.parse(getLocalStorage(FORM_NAME_MASTER));
		const thisFormRow = formMaster.find((form) => form.viewUrl && form.viewUrl.trim() === url);
		const prefixes = Object.keys(thisFormRow)
			.filter((field) => formApprovalFields.includes(field) && thisFormRow[field] === 'Y')
			.map((row) => {
				const userRow = approvalFieldPrefixMapping[row];
				return userRow;
			})
			.sort((a, b) => (a.priority < b.priority ? 1 : -1));
		return getNepaliDate(prevData[`${prefixes[0].prefix}Date`]);
	} catch (err) {
		console.log('err', err);
		return '';
	}
};

export const formatPermitValuesForPost = (values, marker) => {
	const dataToSend = Object.assign({}, values);

	if (marker) {
		dataToSend.lat = marker.lat;
		dataToSend.lng = marker.lng;
	}

	if (isEmpty(dataToSend.floor)) {
		dataToSend.floor = [];
	}

	dataToSend.floor.forEach((row, index) => {
		if (row) {
			row.floorUnit = dataToSend.floorUnit || DEFAULT_UNIT_LENGTH;
			row.floor = isStringEmpty(row.floor) ? index : row.floor;
			if (!row.block) {
				row.block = '';
			}
		}
	});

	dataToSend.floor = dataToSend.floor.filter((row, index) => !isEmpty(row) && row.length && row.width && row.height);

	dataToSend.surrounding = dataToSend.surrounding.filter((row, index) => {
		return !isEmpty(row) && !isStringEmpty(row.feet) && row.kittaNo;
	});

	if (dataToSend.member) {
		dataToSend.member = dataToSend.member.filter((row) => {
			return !isEmpty(row) && row.memberName && row.relation;
		});
	}

	dataToSend.floor = dataToSend.floor.filter((fl) => !isEmpty(fl));

	dataToSend.kittaNo = dataToSend.kittaNo && isArray(dataToSend.kittaNo) ? dataToSend.kittaNo.join(', ') : dataToSend.kittaNo;
	dataToSend.landArea = dataToSend.landArea && isArray(dataToSend.landArea) ? dataToSend.landArea.join(', ') : dataToSend.landArea;
	delete dataToSend.landDetails;

	return dataToSend;
};

export const getSaveByUserDetails = (enterByUser, userData) => {
	let serInfo = {
		subName: '',
		subDesignation: '',
		subSignature: '',
	};

	serInfo.subName = enterByUser.name;
	serInfo.subDesignation = enterByUser.designation;
	serInfo.subSignature = enterByUser.signature;

	if (getUserRole() === enterByUser.userType) {
		serInfo.subName = isStringEmpty(serInfo.subName) ? userData.userName : serInfo.subName;
		serInfo.subDesignation = isStringEmpty(serInfo.subDesignation) ? getUserTypeValueNepali(userData.userType) : serInfo.subDesignation;
		serInfo.subSignature = isStringEmpty(serInfo.subSignature) ? userData.info.signature : serInfo.subDesignation;
		serInfo.subDate = isStringEmpty(serInfo.subDate) ? getCurrentDate(true) : serInfo.subDate;
	}

	return serInfo;
};

export const getApproveByUserDetails = (approveByUsers, userData) => {
	if (Array.isArray(approveByUsers)) {
		return approveByUsers.map((row) => {
			if (userData.userType === row.userType) {
				return {
					name: userData.userName,
					designation: row.designation,
					signature: userData.info.signature,
					userType: row.userType,
					date: getCurrentDate(true),
				};
			} else {
				return row;
			}
		});
	} else {
		return [];
	}
};

export function getApproveByObject({ approveByUsers, userData, orgCode, formUrl }) {
	const approveInfo = getApproveByUserDetails(approveByUsers, userData);

	function getApproveBy(index) {
		const approveByIndex = getApproveIndex(index, formUrl, orgCode, approveInfo);
		if (approveInfo[approveByIndex]) {
			return approveInfo[approveByIndex];
		} else {
			return {};
		}
	}

	return { approveInfo, getApproveBy };
}

export const getApproveIndex = (index, formUrl, client, approveInfo) => {
	try {
		const user = ApproveFormMap[formUrl]
			? ApproveFormMap[formUrl][client]
				? ApproveFormMap[formUrl][client].approveBy[index]
				: ApproveFormMap[formUrl][DEFAULT].approveBy[index]
			: ApproveFormMap[DEFAULT][DEFAULT].approveBy[index];
		const approveIndex = approveInfo.findIndex((row) => row.userType === user);
		// if (approveIndex < 0) return 0;
		return approveIndex;
	} catch (err) {
		console.log('err', err);
		return 0;
	}
};

export const getStaticFiles = (files) => {
	const staticFiles = files.filter((row) => row.fileType.staticCategoryIdentifier);
	if (staticFiles) {
		return staticFiles.reduce((acc, file) => ({ ...acc, [file.fileType.staticCategoryIdentifier]: file.fileUrl }), {});
	} else return {};
};

export const getHighTensionData = (mapTech, prabidhik) => {
	const highTension = prabidhik.isHighTensionLineDistance
		? [prabidhik.isHighTensionLineDistance, mapTech.highTension && mapTech.highTension[1] ? mapTech.highTension[1] : 0]
		: isEmpty(mapTech.highTension)
		? [0, 0]
		: mapTech.highTension;

	return highTension;
};
