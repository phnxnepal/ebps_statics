import { getNumber, getFormattedNumber, getFormattedIntNumber, getIntNumber, convertLandArea } from './mathUtils';
import { AreaUnitNepali } from './enums/unit';

test('should return corrent values for string', () => {
	const value = 'asdf';
	expect(getNumber(value)).toBe(0);
	expect(getIntNumber(value)).toBe(0);
});

test('should return corrent values for undefined', () => {
	const value = undefined;
	expect(getNumber(value)).toBe(0);
	expect(getIntNumber(value)).toBe(0);
});

test('should return corrent values for int number', () => {
	const value = '01';
	expect(getNumber(value)).toBe('01');
	expect(getIntNumber(value)).toBe('01');
});

test('should return corrent values for float number', () => {
	const value = '01.123';
	expect(getNumber(value)).toBe('01.123');
	expect(getIntNumber(value)).toBe('01.123');
});

test('should return corrent values for string', () => {
	const value = 'asdf';
	expect(getFormattedNumber(value)).toBe(0);
	expect(getFormattedIntNumber(value)).toBe(0);
});

test('should return corrent values for undefined', () => {
	const value = undefined;
	expect(getFormattedNumber(value)).toBe(0);
	expect(getFormattedIntNumber(value)).toBe(0);
});

test('should return corrent values for int number', () => {
	const value = '01';
	expect(getFormattedNumber(value)).toBe(1);
	expect(getFormattedIntNumber(value)).toBe(1);
});

test('should return corrent values for float number', () => {
	const value = '01.123';
	expect(getFormattedNumber(value)).toBe(1.123);
	expect(getFormattedIntNumber(value)).toBe(1);
});

test('should convert land area correctly', () => {
	const value = '22-12-12-22';

	const convertedFeet = convertLandArea(AreaUnitNepali.Feet, AreaUnitNepali.MountainousCustomaryUnit, value); 
	const convertedTerai = convertLandArea(AreaUnitNepali.TeraiCustomaryUnit, AreaUnitNepali.MountainousCustomaryUnit, value); 
	const reversed = convertLandArea(AreaUnitNepali.Feet, AreaUnitNepali.TeraiCustomaryUnit, convertedTerai); 

	expect(convertedFeet).toBe(126072.66);
	expect(convertedTerai).toBe('1-14-11.76');
	expect(reversed).toBeCloseTo(126072.66, -1);
});
