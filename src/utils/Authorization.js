import React from 'react';
import NotFound from '../components/NotFound';
import { isAuthenticated } from './authUtils';
import { getUserInfoObj } from './functionUtils';

const Authorization = (allowedUserTypes = []) => WrappedComponents => {
    return class withAuthorization extends React.Component {
        constructor(props) {
            super(props);

            let userType = '';

            try {
                userType = getUserInfoObj().userType;
            } catch {
                userType = '';
            }
            this.state = {
                isAuthenticated: isAuthenticated(),
                userType: userType
            };
        }
        render() {
            if (
                this.state.isAuthenticated &&
                allowedUserTypes.includes(this.state.userType)
            ) {
                return <WrappedComponents {...this.props} />;
            } else {
                return <NotFound />;
            }
        }
    };
};

export default Authorization;
