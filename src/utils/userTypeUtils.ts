export enum UserType {
    SUB_ENGINEER = 'B',
    ENGINEER = 'A',
    DESIGNER = 'D',
    ADMIN = 'C',
    AMIN = 'AD',
    RAJASWO = 'R',
    POSTE = 'E',
    POSTF = 'F',
    POSTG = 'G',
    ORGANIZATION_ADMIN = 'ADM',
    TECH_ADMIN = 'TADM',
}

export const getApprovalFieldName = (userType: UserType): string => {
    switch (userType) {
        case UserType.SUB_ENGINEER:
            return 'subEngrApproval';
        case UserType.ENGINEER:
            return 'engrApproval';
        case UserType.DESIGNER:
            return 'designerApproval';
        case UserType.ADMIN:
            return 'chiefApproval';
        case UserType.AMIN:
            return 'aminApproval';
        case UserType.RAJASWO:
            return 'rajasowApproval';
        case UserType.POSTE:
            return 'postefApproval';
        case UserType.POSTF:
            return 'postfApproval';
        case UserType.POSTG:
            return 'postgApproval';
        default:
            return '';
    }
}

export const approvalFieldPrefixMapping = {
    'subEngrApproval': { prefix: 'ser', userType: UserType.SUB_ENGINEER, priority: 2 },
    'engrApproval': { prefix: 'er', userType: UserType.ENGINEER, priority: 4},
    'designerApproval': { prefix: 'designer', userType: UserType.DESIGNER, priority: 0 },
    'chiefApproval': { prefix: 'chief', userType: UserType.ADMIN, priority: 5 },
    'aminApproval': { prefix: 'amini', userType: UserType.AMIN, priority: 1 },
    'rajasowApproval': { prefix: 'rw', userType: UserType.RAJASWO, priority: 3 },
    'posteApproval': { prefix: 'e', userType: UserType.POSTE, priority: 4 },
    'postfApproval': { prefix: 'f', userType: UserType.POSTF, priority: 4 },
    'postgApproval': { prefix: 'g', userType: UserType.POSTG, priority: 4 },
}

export const needsFormAccess = (userType: UserType) => {
    return userType !== UserType.ORGANIZATION_ADMIN && userType !== UserType.TECH_ADMIN
}

export const allUsers = [
    UserType.DESIGNER,
    UserType.AMIN,
    UserType.SUB_ENGINEER,
    UserType.RAJASWO,
    UserType.ENGINEER,
    UserType.ADMIN,
]

export const userTypeHierarchy = [
    { user: UserType.DESIGNER, priority: 0 },
    { user: UserType.AMIN, priority: 1 },
    { user: UserType.SUB_ENGINEER, priority: 2 },
    { user: UserType.RAJASWO, priority: 3 },
    { user: UserType.ENGINEER, priority: 4 },
    { user: UserType.ADMIN, priority: 5 },
]

export const formApprovalFields = [
    'subEngrApproval',
    'engrApproval',
    'designerApproval',
    'chiefApproval',
    'aminApproval',
    'rajasowApproval',
    'posteApproval',
    'postfApproval',
    'postgApproval',
]

export const getStatusFieldName = (userType: UserType): string => {
    switch (userType) {
        case UserType.SUB_ENGINEER:
            return 'serStatus';
        case UserType.ENGINEER:
            return 'erStatus';
        case UserType.ADMIN:
            return 'chiefStatus';
        case UserType.AMIN:
            return 'aminiStatus';
        case UserType.RAJASWO:
            return 'rwStatus';
        default:
            return '';
    }
}

const getLabelColor = (statusCode: string): { statusLabel: string, color: string, icon: string } => {
    if (statusCode === 'A') {
        return { statusLabel: 'Approved', color: 'green', icon: 'check' };
    } else if (statusCode === 'R') {
        return { statusLabel: 'Rejected', color: 'red', icon: 'warning sign' };
    } else {
        return { statusLabel: 'Pending', color: 'orange', icon: 'wait' };
    }
};

export const getApprovalStatusObject = (userType: UserType, statusCode: string): {} => {
    const { statusLabel, color, icon } = getLabelColor(statusCode);
    switch (userType) {
        case UserType.SUB_ENGINEER:
            return { approvalFieldName: getApprovalFieldName(UserType.SUB_ENGINEER), nameField: 'serName', label: 'Sub-Engineer', className: 'sengCol', statusLabel, color, icon }
        case UserType.ENGINEER:
            return { approvalFieldName: getApprovalFieldName(UserType.ENGINEER), nameField: 'erName', label: 'Engineer', className: 'engCol', statusLabel, color, icon }
        case UserType.ADMIN:
            return { approvalFieldName: getApprovalFieldName(UserType.ADMIN), nameField: 'chiefName', label: 'Chief', className: 'sengCol', statusLabel, color, icon }
        case UserType.AMIN:
            return { approvalFieldName: getApprovalFieldName(UserType.AMIN), nameField: 'aminiName', label: 'Amin', className: 'sengCol', statusLabel, color, icon }
        case UserType.DESIGNER:
            return { approvalFieldName: getApprovalFieldName(UserType.DESIGNER), label: 'Designer', className: 'sengCol', statusLabel, color, icon }
        case UserType.RAJASWO:
            return { approvalFieldName: getApprovalFieldName(UserType.RAJASWO), label: 'Rajasow', className: 'rajosCol', statusLabel, color, icon }
        default:
            return { approvalFieldName: getApprovalFieldName(userType), className: 'rajosCol', statusLabel, color, icon }
    }
}

export const approveColumnMapping = {
    "amini_": "aminApproval",
    "rw_": "rajasowApproval",
    "poste_": "posteApproval",
    "postf_": "postfApproval",
    "postg_": "postgApproval",
    "ser_": "subEngrApproval",
    "er_": "engrApproval",
    "chief_": "chiefApproval",
}

export function getAdminFilteredOptions(userTypeMaster: [{ id: string, designationNepali: string, designation: string }]) {
    return userTypeMaster
        .filter(user => ![UserType.ORGANIZATION_ADMIN.toString(), UserType.TECH_ADMIN.toString()].includes(user.id))
        .map((user, index) => ({ key: index, value: user.id, text: user.designationNepali || user.designation }));
}

export type UserTypeMaster = {
    designation: string;
    userType: UserType;
}