import { UserType } from './userTypeUtils';
import { ConstructionType } from './enums/constructionType';
import { options } from './data/genericFormData';
import { floorMappingFlat } from './dataUtils';
import { AreaUnitNepali } from './enums/unit';

export const landAreaTypeOptions = [
	{ key: 1, value: AreaUnitNepali.MountainousCustomaryUnit, text: AreaUnitNepali.MountainousCustomaryUnit },
	{ key: 2, value: AreaUnitNepali.TeraiCustomaryUnit, text: AreaUnitNepali.TeraiCustomaryUnit },
	{ key: 3, value: AreaUnitNepali.Feet, text: AreaUnitNepali.Feet },
	{ key: 4, value: AreaUnitNepali.Metre, text: AreaUnitNepali.Metre },
];

export const landAreaTypeOptionsKamalamai = [
	{ key: 1, value: AreaUnitNepali.MountainousCustomaryUnit, text: AreaUnitNepali.MountainousCustomaryUnit },
	{ key: 2, value: AreaUnitNepali.TeraiCustomaryPaiUnit, text: AreaUnitNepali.TeraiCustomaryPaiUnit },
	{ key: 3, value: AreaUnitNepali.Feet, text: AreaUnitNepali.Feet },
	{ key: 4, value: AreaUnitNepali.Metre, text: AreaUnitNepali.Metre },
];

export const areaCleaveOption = [
	{ index: 0, key: 'रोपनी–आना–पैसा–दाम', option: { delimiter: '-', blocks: [2, 2, 2, 2] } },
	{ index: 1, key: 'बिघा–कठ्ठा–धुर', option: { delimiter: '-', blocks: [2, 2, 2] } },
	{ index: 2, key: 'वर्ग फिट', option: { delimiter: '', blocks: [1] } },
	{ index: 3, key: 'वर्ग मिटर', option: { delimiter: '', blocks: [1] } },
	{ index: 4, key: 'बिघा–कठ्ठा–धुर–पाई', option: { delimiter: '-', blocks: [2, 2, 2, 2] } },
];

export const statusOption = [
	{ value: 'Y', text: 'Yes' },
	{ value: 'N', text: 'No' },
];

export const pendingOption = [
	{ value: 'P', text: 'Pending' },
	{ value: 'A', text: 'Approved' },
];

export const statusNepaliOption = [
	{ value: 'Y', text: 'छ' },
	{ value: 'N', text: 'छैन' },
];

export const hasAccessOption = [
	{ value: 'Y', text: 'Yes' },
	{ value: 'N', text: 'No' },
];

export const statusNepaliTallaThapOption = [
	{ value: 'Y', text: 'संसोधन छ' },
	{ value: 'T', text: 'तला थप छ' },
	{ value: 'N', text: 'छैन' },
];

export const nirmanTypeAnyaOptions = [
	{ value: 'फ्रेम स्ट्रक्चर', text: 'फ्रेम स्ट्रक्चर' },
	{ value: 'लोड वियरिङ्ग', text: 'लोड वियरिङ्ग' },
	{ value: 'अन्य', text: 'अन्य' },
];

export const nirmanTypeOptions = [
	{ value: 'पक्की', text: 'पक्की' },
	{ value: 'लोड वियरिङ्ग', text: 'लोड वियरिङ्ग' },
	{ value: 'फ्रेम स्ट्रक्चर', text: 'फ्रेम स्ट्रक्चर' },
	{ value: 'कच्ची', text: 'कच्ची' },
];

export const constructionTypeOption = [
	{ value: 'नयाँ निर्माण', text: 'नयाँ निर्माण' },
	{ value: 'तला थप', text: 'तला थप' },
	{ value: 'पुरानो घर', text: 'पुरानो घर' },
];

export const constructionTypeOptions = [
	{ value: '1', label: ConstructionType.NAYA_NIRMAN },
	{ value: '2', label: ConstructionType.PURANO_GHAR },
	{ value: '3', label: ConstructionType.TALLA_THAP },
];

export const constructionTypeEnglishOptions = [
	{ value: '1', label: 'New' },
	{ value: '2', label: 'Extension' },
	{ value: '3', label: 'Addition of Storey' },
];

export const constructionTypeOptionsBiratnagar = [
	{ value: '1', label: ConstructionType.NAYA_NIRMAN },
	{ value: '2', label: ConstructionType.PURANO_GHAR },
	{ value: '3', label: ConstructionType.TALLA_THAP },
	{ value: '4', label: 'मोहोडा फेर्ने' },
	{ value: '5', label: 'साविक घर भत्काई पुनः निर्माण' },
	{ value: '6', label: 'छाना फेर्ने' },
	{ value: '7', label: 'थप घर निर्माण विस्तार' },
	{ value: '8', label: 'कम्पाउन्डवाल' },
	{ value: '9', label: 'अन्य' },
];

export const constructionTypeOptionsTextAll = [
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
	{ value: '3', text: ConstructionType.TALLA_THAP },
	{ value: '4', text: 'मोहोडा फेर्ने' },
	{ value: '5', text: 'साविक घर भत्काई पुनः निर्माण' },
	{ value: '6', text: 'छाना फेर्ने' },
	{ value: '7', text: 'थप घर निर्माण विस्तार' },
	{ value: '8', text: 'कम्पाउन्डवाल' },
	{ value: '9', text: 'अन्य' },
];

export const constructionTypeSelectOptions = [
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
	{ value: '3', text: ConstructionType.TALLA_THAP },
];

export const consTypeWithDefaultSelectOptions = [
	{ value: '', text: options.defaultConstructionType },
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
	{ value: '3', text: ConstructionType.TALLA_THAP },
];

export const applicationStatusOptions = [
	{ value: '', text: options.applicationStatus.default },
	{ value: 'C', text: options.applicationStatus.completed },
	{ value: 'P', text: options.applicationStatus.pending },
];

export const applicationStatusReportOptions = [
	{ value: 'A', text: options.applicationStatus.pending },
	{ value: 'C', text: options.applicationStatus.completed },
];

export const userTypeDesignerOption = [{ value: 'D', text: 'डिजाइनर' }];

export const userTypeOption = [
	{ value: UserType.RAJASWO, text: 'राजस्व' },
	{ value: UserType.SUB_ENGINEER, text: 'सब-इन्जिनियर' },
	{ value: UserType.ENGINEER, text: 'इन्जिनियर' },
	{ value: UserType.ADMIN, text: 'प्रमुख प्रशासकीय अधिकृत' },
	{ value: UserType.DESIGNER, text: 'डिजाइनर' },
	{ value: UserType.AMIN, text: 'अमिन' },
];

export const userTypeWOTechOption = [
	{ value: UserType.RAJASWO, text: 'राजस्व' },
	{ value: UserType.SUB_ENGINEER, text: 'सब-इन्जिनियर' },
	{ value: UserType.ENGINEER, text: 'इन्जिनियर' },
	{ value: UserType.ADMIN, text: 'प्रमुख प्रशासकीय अधिकृत' },
	{ value: UserType.DESIGNER, text: 'डिजाइनर' },
	{ value: UserType.AMIN, text: 'अमिन' },
	{ value: UserType.ORGANIZATION_ADMIN, text: 'प्रशासक' },
];

export const userTypeOptionAll = [
	{ value: UserType.RAJASWO, text: 'राजस्व' },
	{ value: UserType.SUB_ENGINEER, text: 'सब-इन्जिनियर' },
	{ value: UserType.ENGINEER, text: 'इन्जिनियर' },
	{ value: UserType.ADMIN, text: 'प्रमुख प्रशासकीय अधिकृत' },
	{ value: UserType.DESIGNER, text: 'डिजाइनर' },
	{ value: UserType.AMIN, text: 'अमिन' },
	{ value: UserType.ORGANIZATION_ADMIN, text: 'प्रशासक' },
	{ value: UserType.TECH_ADMIN, text: 'प्राविधिक प्रशासक' },
];

export const buildingStructureOption = [
	{ value: 'P', text: 'पिलर' },
	{ value: 'G', text: 'गारो' },
	{ value: '', text: 'अन्य' },
];

export const certificateBuildingClassOptions = [
	{ value: 'A', text: '(क)' },
	{ value: 'B', text: '(ख)' },
	{ value: 'C', text: '(ग)' },
	{ value: 'D', text: '(घ)' },
];

export const genderOptions = [
	{ key: 1, value: 'श्री', text: 'श्री' },
	{ key: 2, value: 'श्रीमती', text: 'श्रीमती' },
	{ key: 3, value: 'सुश्री', text: 'सुश्री' },
];

export const shreeOptions = [
	{ key: 1, value: 'श्रीमान्', text: 'श्रीमान्' },
	{ key: 2, value: 'श्रीमती', text: 'श्रीमती' },
	{ key: 3, value: 'सुश्री', text: 'सुश्री' },
];

export const gharDhaniWaresOption = [
	{ key: 'gd', value: 'gd', text: 'घरधनीको' },
	{ key: 'war', value: 'war', text: 'वारेशको' },
];

export const areaUnitOptions = [
	{ key: 1, value: 'SQUARE METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'SQUARE FEET', text: 'वर्ग फिट' },
];

export const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

export const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

export const buildingRoadOptions = ['पिच', 'ग्राभेल', 'मोटर जाने कच्ची', 'गोरेटो', 'बाटो नभएको', 'अन्य'];

export const apiMethodOptions = [
	{ key: 0, value: '', text: 'Select a Method' },
	{ key: 1, value: 'GET', text: 'GET' },
	{ key: 2, value: 'POST', text: 'POST' },
	{ key: 3, value: 'PUT', text: 'PUT' },
	{ key: 4, value: 'DELETE', text: 'DELETE' },
];

export const purposeOfConstructionOptions = ['आवासीय', 'व्यापारिक', 'अन्य'];
export const purposeOfConstructionEnglishOptions = [
	{ value: 'आवासीय', text: 'Residential' },
	{ value: 'व्यापारिक', text: 'Commercial' },
	{ value: 'अन्य', text: 'Other' },
];

export const purposeOfConstructionEnglishOptionsV2 = [
	{ value: 'आवासीय', text: 'Residential' },
	{ value: 'व्यापारिक', text: 'Commercial' },
	{ value: 'Institutional', text: 'Institutional' },
	{ value: 'अन्य', text: 'Misc' },
];

export const spouseTypeOptions = ['पिता', 'पति', 'पत्नी'];
export const spouseTypeSelectOptions = [
	{ value: 'पिता', text: 'पिता' },
	{ value: 'पति', text: 'पति' },
	{ value: 'पत्नी', text: 'पत्नी' },
];

export const chaBhayekoOptions = [
	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
];

export const bhayekoOptions = [
	{ key: 1, value: 'Y', text: 'भएको' },
	{ key: 2, value: 'N', text: 'नभएको' },
];

export const chaChainaOptions = [
	{ key: 1, value: 'Y', text: 'छ' },
	{ key: 2, value: 'N', text: 'छैन' },
];

export const chaChainaWithNValueOptions = [
	{ key: 1, value: 'N', text: 'छ' },
	{ key: 2, value: 'N', text: 'छैन' },
];

export const chaChainaNepaliOptions = [
	{ key: 1, value: 'छ', text: 'छ' },
	{ key: 2, value: 'छैन', text: 'छैन' },
];

export const bhayekoPartialOptions = [
	{ key: 1, value: 'Y', text: 'भएको' },
	{ key: 2, value: 'N', text: 'नभएको' },
	{ key: 3, value: 'P', text: 'आशिक पालना भएको' },
];

export const parekoOptions = [
	{ key: 1, value: 'N', text: 'नपरेको' },
	{ key: 2, value: 'Y', text: 'परेको' },
];

export const tiedOptions = [
	{ key: 1, value: 'Y', text: 'बाँधिएको' },
	{ key: 2, value: 'N', text: 'नबाँधिएको' },
];

export const buildingTypeOptions = ['लोडबेरीङ्ग', 'फ्रेमस्ट्रक्चर', 'अन्य'];

export const floorOptions = floorMappingFlat.map((floor) => ({ key: floor.floor, value: floor.floor, text: floor.value }));

export const wardNoOptions = [...Array(11)].map((_, idx) => ({
	key: String(idx + 1).padStart(2, '0'),
	value: String(idx + 1).padStart(2, '0'),
	text: String(idx + 1).padStart(2, '0'),
}));

export const areaTypeOptions = [{ key: 'U', value: 'U', text: 'Urban' }];
export const floorTypeOptions = [
	{ key: 'J', value: 'J', text: 'Jasta' },
	{ key: 'O', value: 'O', text: 'Other' },
	{ key: 'F', value: 'F', text: 'Floor' },
];

export const menuTypeOptions = [
	{ key: 'P', value: 'P', text: 'Process Menu' },
	{ key: 'S', value: 'S', text: 'Setup Menu' },
];

export const designerSignupTypeOptions = [
	{ key: 1, value: 'private', text: 'Private' },
	{ key: 2, value: 'public', text: 'Public' },
];

export const setBackOptions = [
	{ key: 1, value: 'सेट व्याक', text: ' सेट व्याक' },
	{ key: 2, value: 'सडक सेटव्याक', text: 'सडक सेटव्याक' },
];

export const landscapeTypeOptions = [
	{ value: 'मुख्य सडक', text: 'मुख्य सडक' },
	{ value: 'सडक', text: 'सडक' },
	{ value: 'सडक बन्द भएको (dead end road)', text: 'सडक बन्द भएको (dead end road)' },
	{ value: 'जग्गा', text: 'जग्गा' },
	{ value: 'पर्खाला भएको जम्गा', text: 'पर्खाला भएको जम्गा' },
	{ value: 'सावजनिक जग्गा', text: 'सावजनिक जग्गा' },
	{ value: 'स्कूल', text: 'स्कूल' },
	{ value: 'मन्दिर', text: 'मन्दिर' },
	{ value: 'पार्टी प्यालेस', text: 'पार्टी प्यालेस' },
	{ value: 'चोक', text: 'चोक' },
	{ value: 'पोखरी', text: 'पोखरी' },
	{ value: 'नदी कुलो', text: 'नदी कुलो' },
	{ value: 'गुम्बा', text: 'गुम्बा' },
	{ value: 'कुवा', text: 'कुवा' },
	{ value: 'GLD रोडा बाटो नखुलेको', text: 'GLD रोडा बाटो नखुलेको' },
];
