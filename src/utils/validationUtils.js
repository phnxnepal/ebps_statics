import * as Yup from 'yup';
import { commonMessages, numberMessages, dateMessages, fileMessages } from './data/validationData';
import { translateEngToNep, translateNepToEng } from './langUtils';
import { FILE_SIZE, SUPPORTED_FORMATS, SUPPORTED_FORMATS_FILE } from './constants/fileConstants';
import { isStringEmpty } from './stringUtils';
import { toArray } from './dataUtils';

/**
 * @deprecated Since this has been implemented in @see validateNumber
 * function
 * @param {string} value - field value to check for Nepali number
 */
export const validateNepaliNumber = (value) => {
	let error;
	if (!value) {
		error = commonMessages.required;
	} else if (!/[\u{0966}-\u{096f}0-9]/u.test(value)) {
		error = numberMessages.number;
	}

	return error;
};

/**
 * @returns Yup validation schema for Nepali number as well as normal
 *  number validation.
 */
export const validateNumber = Yup.string()
	.typeError(numberMessages.number)
	.matches(/^[\u{0966}-\u{096f}0-9.]+$/u, numberMessages.number)
	.required(commonMessages.required);

// export const validateArrayNumber = Yup.string()
// 	.required(commonMessages.required)
// 	.matches(/^[\u{0966}-\u{096f}0-9.]+$/u, numberMessages.number)
// .typeError(numberMessages.number);

export const validateNumberWithDash = Yup.string()
	.matches(/^[\u{0966}-\u{096f}0-9\-.]+$/u, numberMessages.number)
	.required(commonMessages.required);

export const validateNullableNumber = Yup.string()
	.matches(/^[\u{0966}-\u{096f}0-9.]+$/u, numberMessages.number)
	.nullable();

export const validateArrayNullableNumber = Yup.string()
	.matches(/^[\u{0966}-\u{096f}0-9.]*$/u, numberMessages.number)
	.nullable();

export const validateNullableOfficialNumbers = Yup.string()
	.matches(/^[\u{0966}-\u{096f}0-9/-]+$/u, numberMessages.number)
	.nullable();

export const validateNullableOfficialReqNumbers = Yup.string()
	.matches(/^[\u{0966}-\u{096f}0-9/-]+$/u, numberMessages.number)
	.required(commonMessages.required);

/**
 * Field wise validator for Formik Field validate prop
 *
 * @param {string} value - @see validateNumber Yup validation to
 * validate field wise value
 * @returns error
 */
export const validateNumberField = (value) => {
	let error;
	const schema = validateNumber;
	error = schema
		.validate(value)
		.then(() => {})
		.catch((err) => {
			throw err.message;
		});
	return error;
};

export const validateNullableNUmberField = (value) => {
	return validateFieldWrapper(validateNullableNumber, value);
};

/**
 * @returns Yup validation schema for normal number validation.
 */
export const validateNormalNumber = Yup.number().typeError(numberMessages.number).positive(numberMessages.positive).required(commonMessages.required);

/**
 * Field wise validator for Formik Field validate prop
 *
 * @param {number} value - Uses validateNormalNumber() Yup validation
 * to validate field wise value @see validateNormalNumber
 * @returns error
 */
export const validateNormalNumberField = (value) => {
	let error;
	const schema = validateNormalNumber;
	error = schema
		.validate(value)
		.then(() => {})
		.catch((err) => {
			throw err.message;
		});
	return error;
};

/**
 * @returns Yup validation schema for normal string validation.
 */
export const validateString = Yup.string().typeError(commonMessages.required).required(commonMessages.required);

/**
 * Field wise validator for Formik Field validate prop
 *
 * @param {number} value - @see validateString Yup validation
 * to validate field wise value
 * @returns error
 */
export const validateStringField = (value) => {
	let error;
	const schema = validateString;
	error = schema
		.validate(value)
		.then(() => {})
		.catch((err) => {
			throw err.message;
		});
	return error;
};

export const validateFieldWrapper = (fieldSchema, value) => {
	let error;
	const schema = fieldSchema;
	error = schema
		.validate(value)
		.then(() => {})
		.catch((err) => {
			throw err.message;
		});
	return error;
};

export const validateFileField = (value) => {
	return validateFieldWrapper(validateFile, value);
};

export const validateOptionalFileField = (value) => {
	return validateFieldWrapper(validateFileOptional, value);
};

export const validateMoreThanZeroNumberField = (value) => {
	let error;
	const schema = validateNotLessThanOneNumber;
	error = schema
		.validate(value)
		.then(() => {})
		.catch((err) => {
			throw err.message;
		});
	return error;
};

export const validateZeroOrMoreNumberField = (value) => {
	return validateFieldWrapper(validateMoreThanZeroNumber, value);
};

export const validateNullableNormalNumberField = (value) => {
	return validateFieldWrapper(validateNullableNormalNumber, value);
};

export const validateNullableNormalNumber = Yup.number().typeError(numberMessages.number).positive(numberMessages.positive).nullable();

export const validateMoreThanZeroNumber = Yup.number().moreThan(-1, numberMessages.positive).typeError(numberMessages.number).nullable();

export const validateNotLessThanOneNumber = Yup.number().moreThan(0, numberMessages.positive).typeError(numberMessages.number);

/**
 * This schema will accept:
 * 	- validate number with decimals
 *  - This will also accept null string
 * This schema will reject:
 *  - Any string value or invalid number like 1.2.
 */
export const validateNullableZeroNumber = Yup.string()
	.matches(/^(\d+.\d+|\d*)$/, numberMessages.number)
	.nullable();

export const checkFileExtension = (filename) => {
	const split = filename.split('.');
	const ext = split[split.length - 1];
	return SUPPORTED_FORMATS_FILE.includes(ext);
};

export const validateFile = Yup.array()
	.of(
		Yup.mixed()
			.test('fileSize', fileMessages.fileSize, (value) => {
				return value && value.size <= FILE_SIZE;
			})
			.test(
				'fileFormat',
				'Unsupported File Format ',
				(value) => value && (SUPPORTED_FORMATS.includes(value.type) || checkFileExtension(value.name))
			)
	)
	.required(fileMessages.required);

export const validateExistingSingleFile = (existingFileField) =>
	Yup.mixed().when(existingFileField, (existingFile, schema) => {
		if (existingFile) {
			return;
		} else {
			return Yup.array()
				.of(
					Yup.mixed()
						.test('fileSize', fileMessages.fileSize, (value) => {
							return value && value.size <= FILE_SIZE;
						})
						.test(
							'fileFormat',
							'Unsupported File Format ',
							(value) => value && (SUPPORTED_FORMATS.includes(value.type) || checkFileExtension(value.name))
						)
				)
				.required(fileMessages.required);
		}
	});

export const validateExistingMultiFile = (existingFileField) =>
	Yup.mixed().when(existingFileField, (existingFile, schema) => {
		if (existingFile && Array.isArray(toArray(existingFile))) {
			return;
		} else {
			return Yup.array()
				.of(
					Yup.mixed()
						.test('fileSize', fileMessages.fileSize, (value) => {
							return value && value.size <= FILE_SIZE;
						})
						.test(
							'fileFormat',
							'Unsupported File Format ',
							(value) => value && (SUPPORTED_FORMATS.includes(value.type) || checkFileExtension(value.name))
						)
				)
				.required(fileMessages.required);
		}
	});

export const validateFileOptional = Yup.array().of(
	Yup.mixed()
		.test('fileSize', 'File too large ', (value) => {
			return value && value.size <= FILE_SIZE;
		})
		.test(
			'fileFormat',
			'Unsupported File Format ',
			(value) => value && (SUPPORTED_FORMATS.includes(value.type) || checkFileExtension(value.name))
		)
);

// export const validatePhoneNumber = Yup.string()
//   .matches(/^[\u{0966}-\u{096f}0-9]+$/u, numberMessages.number)
//   .max(10, numberMessages.max)
//   .min(9, numberMessages.min)
//   .required(commonMessages.required);

// export const validateNotReqdPhone = Yup.string()
//   .matches(/^[\u{0966}-\u{096f}0-9]+$/u, numberMessages.number)
//   .max(10, numberMessages.max)
//   .min(9, numberMessages.min)
//   .nullable();

const validateNumberWrapper = (nullable = false) => (min, max) => {
	if (nullable) {
		return Yup.string()
			.matches(/^[\u{0966}-\u{096f}0-9]+$/u, numberMessages.number)
			.max(max, `${translateEngToNep(max)} ${numberMessages.max}`)
			.min(min, `${translateEngToNep(min)} ${numberMessages.min}`)
			.nullable();
	} else {
		return Yup.string()
			.matches(/^[\u{0966}-\u{096f}0-9]+$/u, numberMessages.number)
			.max(max, `${translateEngToNep(max)} ${numberMessages.max}`)
			.min(min, `${translateEngToNep(min)} ${numberMessages.min}`)
			.required(commonMessages.required);
	}
};

const validateNumberNullable = validateNumberWrapper(true);
const validateNumberRequired = validateNumberWrapper(false);

export const validateNullableWardNo = validateNumberNullable(1, 2);
export const validateWardNo = validateNumberRequired(1, 2);

export const validateNotReqdPhone = validateNumberNullable(9, 10);
export const validatePhoneNumber = validateNumberRequired(9, 10);

export const validateRequiredNullableNormalNumber = Yup.number()
	.typeError(numberMessages.number)
	.required(commonMessages.required)
	.moreThan(-1, numberMessages.positive)
	.nullable();

export const validateMinDistanceField = (requiredDistanceField) => {
	return Yup.number()
		.when(requiredDistanceField, (requiredDistance, schema) => {
			if (requiredDistance) {
				return Yup.string()
					.matches(/^(\d+.\d+|\d*)$/, numberMessages.number)
					.test('sadak', numberMessages.moreThan, (value) => parseFloat(value) >= parseFloat(requiredDistance));
			}
		})
		.typeError(numberMessages.number);
};

/**
 * @see https://calendars.wikia.org/wiki/Bikram_Samwat
 */
const dayMonth = [
	{ day: [30, 31], month: 1 },
	{ day: [31, 32], month: 2 },
	{ day: [31, 32], month: 3 },
	{ day: [31, 32], month: 4 },
	{ day: [31, 32], month: 5 },
	{ day: [30, 31], month: 6 },
	{ day: [29, 30], month: 7 },
	{ day: [29, 30], month: 8 },
	{ day: [29, 30], month: 9 },
	{ day: [29, 30], month: 10 },
	{ day: [29, 30], month: 11 },
	{ day: [30, 31], month: 12 },
];

export const validateNepaliDate = Yup.string()
	.typeError(commonMessages.required)
	.test('date', dateMessages.incorrect, (value) => {
		try {
			const dateSplit = value.split('-');
			const [year, month, day] = dateSplit;
			const engYear = parseInt(translateNepToEng(year));
			const engMonth = parseInt(translateNepToEng(month));
			const engDay = parseInt(translateNepToEng(day));

			const validDays = dayMonth.find((row) => row.month === engMonth).day;

			return (
				engYear > 2000 && engYear < 2099 && engMonth < 13 && engMonth > 0 && (engDay <= validDays[0] || engDay <= validDays[1]) && engDay > 0
			);
		} catch (err) {
			return false;
		}
	})
	.required(commonMessages.required);

export const normalNepaliDate = Yup.string().test('date', dateMessages.incorrect, (value) => {
	try {
		if (isStringEmpty(value)) return true;
		const dateSplit = value.split('-');
		const [year, month, day] = dateSplit;
		const engYear = parseInt(year);
		const engMonth = parseInt(month);
		const engDay = parseInt(day);

		const validDays = dayMonth.find((row) => row.month === engMonth).day;

		return engYear > 2000 && engYear < 2099 && engMonth < 13 && engMonth > 0 && (engDay <= validDays[0] || engDay <= validDays[1]) && engDay > 0;
	} catch (err) {
		return false;
	}
});

export const validateNormalNepaliDate = normalNepaliDate.required(commonMessages.required);

export const validateNormalNepaliDateWithRange = Yup.string()
	.test('date', dateMessages.incorrectRange, (value) => {
		try {
			const dateSplit = value.split('-');
			const [year, month, day] = dateSplit;
			const engYear = parseInt(year);
			const engMonth = parseInt(month);
			const engDay = parseInt(day);

			const validDays = dayMonth.find((row) => row.month === engMonth).day;

			return (
				engYear > 2060 && engYear < 2099 && engMonth < 13 && engMonth > 0 && (engDay <= validDays[0] || engDay <= validDays[1]) && engDay > 0
			);
		} catch (err) {
			return false;
		}
	})
	.required(commonMessages.required);

export const validateNullableNepaliDate = Yup.string().test('nullable-date', dateMessages.incorrect, (value) => {
	try {
		if (isStringEmpty(value)) return true;

		const dateSplit = value.split('-');
		const [year, month, day] = dateSplit;
		const engYear = parseInt(translateNepToEng(year));
		const engMonth = parseInt(translateNepToEng(month));
		const engDay = parseInt(translateNepToEng(day));

		const validDays = dayMonth.find((row) => row.month === engMonth).day;

		return engYear > 2000 && engYear < 2099 && engMonth < 13 && engMonth > 0 && (engDay <= validDays[0] || engDay <= validDays[1]) && engDay > 0;
	} catch (err) {
		return false;
	}
}).nullable();

export const validatePassword = Yup.string()
	.required(commonMessages.required)
	.matches(
		/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
		'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character'
	);
