import { getIn } from 'formik';
import { AreaUnitNepali } from './enums/unit';
import { translateNepToEng } from './langUtils';

export const calculateAmount = (otherField, value, index, valuesArr, parentObj) => {
	if (valuesArr[parentObj] && valuesArr[parentObj][index]) {
		const otherFieldVal = getIn(valuesArr, `${parentObj}.${index}.${otherField}`);
		if (otherFieldVal) {
			return round(parseFloat(valuesArr[parentObj][index][otherField]) * parseFloat(value || 0));
		} else {
			return 0.0;
		}
	} else {
		return 0.0;
	}
};

export const calculateAmount1 = (area = 0.0, rate = 0.0) => {
	return getNumber(round(parseFloat(area) * parseFloat(rate)));
};

export const calculateTotal = (...fields) => {
	return round(fields.reduce((totalAmount, next) => totalAmount + parseFloat(next || 0.0), 0.0));
};

export const calculateArea = (length, width) => {
	try {
		const len = typeof parseFloat(length || 0.0) === 'number' ? parseFloat(length || 0.0) : 0.0;
		const wid = typeof parseFloat(width || 0.0) === 'number' ? parseFloat(width || 0.0) : 0.0;
		return round(len * wid);
	} catch {
		return 0.0;
	}
};

export const calculateHighestFloor = (floors, name) => {
	return floors.reduce((max, value) => (max[name] > value[name] ? max : value));
};

export const round = (value) => {
	return Math.round(value * 100) / 100;
};

export const lengthconvertor = (value, unit, prevunit) => {
	let curVal = 0.0;
	try {
		curVal = value ? parseFloat(value) : 0.0;
	} catch {
		curVal = 0.0;
	}

	if (prevunit === 'FEET' && unit === 'METRE') {
		const retVal = parseFloat(curVal) / 3.281;
		return round(retVal);
	} else if (prevunit === 'METRE' && unit === 'FEET') {
		const retVal = 3.281 * parseFloat(curVal);
		return round(retVal);
	} else {
		return value;
	}
};

export const areaconvertor = (value, unit, prevunit) => {
	let curVal = 0.0;
	try {
		curVal = value ? parseFloat(value) : 0.0;
	} catch {
		curVal = 0.0;
	}

	if (prevunit === 'SQUARE FEET' && unit === 'SQUARE METRE') {
		const retVal = parseFloat(curVal) / 10.764;
		return round(retVal);
	} else if (prevunit === 'SQUARE METRE' && unit === 'SQUARE FEET') {
		const retVal = 10.764 * parseFloat(curVal);
		return round(retVal);
	} else {
		return value;
	}
};

/**
 *
 * @param {string} unit
 * Adds SQUARE to normal unit values if not present
 * Used for area conversion @see areaconvertor
 */
export const getAreaUnit = (unit) => {
	return unit.includes('SQUARE') ? unit : `SQUARE ${unit}`;
};

/**
 *
 * @param {string} unit
 * Removes SQUARE from area unit to  normal unit values if needed
 * Used for length conversion @see lengthconvertor
 */
export const getLengthUnit = (unit) => {
	return unit.replace('SQUARE', '').trim();
};

/**
 *
 * @param {number} value
 * @param {string} unit
 * @param {string} prevUnit
 *
 * Area converter with unit name formatting. @see getAreaUnit
 */
export const convertArea = (value, unit, prevUnit) => {
	let curVal = 0.0;
	try {
		curVal = value ? parseFloat(value) : 0.0;
	} catch {
		curVal = 0.0;
	}

	const cleanUnit = getAreaUnit(unit);
	const cleanPrevUnit = getAreaUnit(prevUnit);

	if (cleanPrevUnit === 'SQUARE FEET' && cleanUnit === 'SQUARE METRE') {
		const retVal = parseFloat(curVal) / 10.764;
		return round(retVal);
	} else if (cleanPrevUnit === 'SQUARE METRE' && cleanUnit === 'SQUARE FEET') {
		const retVal = 10.764 * parseFloat(curVal);
		return round(retVal);
	} else {
		return value;
	}
};

/**
 *
 * @param {number} value
 * @param {string} unit
 * @param {string} prevUnit
 *
 * Length converter with unit name formatting. @see getLengthUnit
 */
export const convertLength = (value, unit, prevUnit) => {
	let curVal = 0.0;
	try {
		curVal = value ? parseFloat(value) : 0.0;
	} catch {
		curVal = 0.0;
	}

	const cleanUnit = getLengthUnit(unit);
	const cleanPrevUnit = getLengthUnit(prevUnit);

	if (cleanPrevUnit === 'FEET' && cleanUnit === 'METRE') {
		const retVal = parseFloat(curVal) / 3.281;
		return round(retVal);
	} else if (cleanPrevUnit === 'METRE' && cleanUnit === 'FEET') {
		const retVal = 3.281 * parseFloat(curVal);
		return round(retVal);
	} else {
		return value;
	}
};

export const calculateDiscount = (input, retArr, detailsTotal) => {
	const discountPer = parseFloat(input || 0.0);
	const discountAmt = (discountPer / 100) * detailsTotal;

	const totalAmt = calculateTotal(...retArr, detailsTotal) - discountAmt;

	return [getNumber(round(discountAmt)), getNumber(round(totalAmt))];
};

export const getNumber = (x) => (isNaN(x) ? 0.0 : x);
export const getIntNumber = (x) => (isNaN(x) ? 0 : x);
export const getFormattedNumber = (x) => parseFloat(getNumber(x));
export const getFormattedIntNumber = (x) => parseInt(getIntNumber(x));

export const getFloatInput = (x) => {
	const parsed = parseFloat(x);
	return isNaN(parsed) ? '' : parsed;
};

export const feetToRegionWise = (toUnit, feetArea) => {
	function format(...values) {
		return values.map((value) => String(value).padStart(2, '0')).join('-');
	}

	function calculate(area, divider) {
		const remainder = area % divider;
		return [(area - remainder) / divider, remainder];
	}

	if (toUnit === AreaUnitNepali.MountainousCustomaryUnit) {
		const [ropani, remainder] = calculate(feetArea, 5476);
		const [aana, aanaRemainder] = calculate(remainder, 342.25);
		const [paisa, paisaRemainder] = calculate(aanaRemainder, 85.56);
		const daam = parseInt(paisaRemainder / 21.39);
		return format(ropani, aana, paisa, daam);
	} else if (toUnit === AreaUnitNepali.TeraiCustomaryUnit) {
		const [bigha, remainder] = calculate(feetArea, 72900);
		const [katha, kathaRemainder] = calculate(remainder, 3645);
		const dhur = round(kathaRemainder / 182.25);
		return format(bigha, katha) + '-' + dhur;
	} else if (toUnit === AreaUnitNepali.Metre) {
		return round(feetArea / 3.281);
	} else {
		return feetArea;
	}
};

export const convertLandArea = (currentUnit, previousUnit, areaValue) => {
	if (!areaValue) return areaValue;

	try {
		const value = translateNepToEng(areaValue);
		const convertToSmallestUnit = (individuals) => {
			if (previousUnit === AreaUnitNepali.MountainousCustomaryUnit) {
				// ropani
				return +individuals[0] * 16 * 4 * 4 + +individuals[1] * 4 * 4 + +individuals[2] * 4 + +individuals[3];
			} else if (previousUnit === AreaUnitNepali.TeraiCustomaryUnit) {
				return +individuals[0] * 20 * 20 + +individuals[1] * 20 + +individuals[2];
			} else return +individuals;
		};

		if (previousUnit === AreaUnitNepali.Feet) {
			if (currentUnit === AreaUnitNepali.Metre) {
				return feetToRegionWise(AreaUnitNepali.Metre, value);
			} else if (currentUnit === AreaUnitNepali.MountainousCustomaryUnit) {
				return feetToRegionWise(AreaUnitNepali.MountainousCustomaryUnit, value);
			} else if (currentUnit === AreaUnitNepali.TeraiCustomaryUnit) {
				return feetToRegionWise(AreaUnitNepali.TeraiCustomaryUnit, value);
			} else {
				return value;
			}
		} else if (previousUnit === AreaUnitNepali.Metre) {
			if (currentUnit === AreaUnitNepali.Feet) {
				return round(value * 3.281);
			} else if (currentUnit === AreaUnitNepali.MountainousCustomaryUnit) {
				return feetToRegionWise(AreaUnitNepali.MountainousCustomaryUnit, value * 3.281);
			} else if (currentUnit === AreaUnitNepali.TeraiCustomaryUnit) {
				return feetToRegionWise(AreaUnitNepali.TeraiCustomaryUnit, value * 3.281);
			} else {
				return value;
			}
		} else {
			const values = value.split('-');
			if (previousUnit === AreaUnitNepali.MountainousCustomaryUnit) {
				if (currentUnit === AreaUnitNepali.Feet) {
					const area = convertToSmallestUnit(values) * 21.39;
					return round(area);
				} else if (currentUnit === AreaUnitNepali.Metre) {
					const area = convertToSmallestUnit(values) * 1.99;
					return round(area);
				} else if (currentUnit === AreaUnitNepali.TeraiCustomaryUnit) {
					const feetArea = convertToSmallestUnit(values) * 21.39;
					return feetToRegionWise(AreaUnitNepali.TeraiCustomaryUnit, feetArea);
				} else {
					return value;
				}
			} else if (previousUnit === AreaUnitNepali.TeraiCustomaryUnit) {
				if (currentUnit === AreaUnitNepali.Feet) {
					const area = convertToSmallestUnit(values) * 182.25;
					return round(area);
				} else if (currentUnit === AreaUnitNepali.Metre) {
					const area = convertToSmallestUnit(values) * 16.93;
					return round(area);
				} else if (currentUnit === AreaUnitNepali.MountainousCustomaryUnit) {
					const feetArea = convertToSmallestUnit(values) * 182.25;
					return feetToRegionWise(AreaUnitNepali.MountainousCustomaryUnit, feetArea);
				} else {
					return value;
				}
			}
		}

		return 0;
	} catch (error) {
		return 0;
	}
};

export const feetToRopani = (areaInSqFeet) => {
	return round(getFormattedNumber(areaInSqFeet) / 5476);
};

export const getRopaniAndFeet = (area, unit) => {
	let ropani = 0;
	let feet = 0;
	try {
		if (unit === AreaUnitNepali.Feet) {
			feet = area;
			ropani = feetToRopani(area);
		} else {
			feet = convertLandArea(AreaUnitNepali.Feet, unit);
			ropani = feetToRopani(feet);
		}
	} catch (error) {
		console.log('Error in conversion', error);
	}

	return { ropani, feet };
};
