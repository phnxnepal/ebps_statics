import { translateDate, translateEngToNepWithZero } from './langUtils';

// var nepali = require('nepali-calendar-js');
var adbs = require('ad-bs-converter');

export const getCurrentDate = (setLang = false) => {
	const today = new Date();
	const dd = today.getDate();
	const mm = today.getMonth() + 1; //January is 0!
	const yyyy = today.getFullYear();

	const nepaliDate = adbs.ad2bs(`${parseInt(yyyy)}/${parseInt(mm)}/${parseInt(dd)}`);
	// console.log('today', today, parseInt(yyyy), parseInt(mm), parseInt(dd), nepaliDate);

	if (setLang) {
		const neResponse = nepaliDate.ne;
		// return translateDate(`${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(nepaliDate.nd).padStart(2, '0')}`);
		return `${neResponse.year}-${String(neResponse.month).padStart(2, '०')}-${String(neResponse.day).padStart(2, '०')}`;
	} else {
		// return `${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(nepaliDate.nd).padStart(2, '0')}`;
		const neResponse = nepaliDate.en;
		// return translateDate(`${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(nepaliDate.nd).padStart(2, '0')}`);
		return `${neResponse.year}-${String(neResponse.month).padStart(2, '0')}-${String(neResponse.day).padStart(2, '0')}`;
	}
};

export const getPriorDate = ({ month = 0, day = 0, year = 0 }, setLang = false) => {
	const today = new Date();
	const dd = today.getDate() - day;
	const mm = today.getMonth() - month + 1; //January is 0!
	const yyyy = today.getFullYear() - year;

	const nepaliDate = adbs.ad2bs(`${parseInt(yyyy)}/${parseInt(mm)}/${parseInt(dd)}`);

	if (setLang) {
		const neResponse = nepaliDate.ne;
		return `${neResponse.year}-${String(neResponse.month).padStart(2, '०')}-${String(neResponse.day).padStart(2, '०')}`;
	} else {
		const neResponse = nepaliDate.en;
		return `${neResponse.year}-${String(neResponse.month).padStart(2, '0')}-${String(neResponse.day).padStart(2, '0')}`;
	}
};

//2019-12-12
// [2019,12,12]
export const getNepaliDate = dateRaw => {
	const date = new Date(dateRaw);
	const dd = date.getDate();
	const mm = date.getMonth() + 1; //January is 0!
	const yyyy = date.getFullYear();

	const nepaliDate = adbs.ad2bs(`${parseInt(yyyy)}/${parseInt(mm)}/${parseInt(dd)}`);
	// const nepaliDate = nepali.toNepali(parseInt(yyyy), parseInt(mm), parseInt(dd));
	const neResponse = nepaliDate.ne;
	// return translateDate(`${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(nepaliDate.nd).padStart(2, '0')}`);
	return `${neResponse.year}-${String(neResponse.month).padStart(2, '०')}-${String(neResponse.day).padStart(2, '०')}`;
	// if (setLang) {
	// return translateDate(`${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(nepaliDate.nd).padStart(2, '0')}`);
	// } else {
	//   return `${nepaliDate.ny}-${String(nepaliDate.nm).padStart(2, '0')}-${String(
	//     nepaliDate.nd
	//   ).padStart(2, '0')}`;
	// }
};

export const convertDateBS = dateRaw => {
	const date = new Date(dateRaw);
	const dd = date.getDate();
	const mm = date.getMonth() + 1; //January is 0!
	const yyyy = date.getFullYear();

	const nepaliDate = adbs.ad2bs(`${parseInt(yyyy)}/${parseInt(mm)}/${parseInt(dd)}`);
	const neResponse = nepaliDate.en;
	return `${neResponse.year}-${String(neResponse.month).padStart(2, '0')}-${String(neResponse.day).padStart(2, '0')}`;
};

export const getADDate = () => {
	const today = new Date();
	const dd = String(today.getDate()).padStart(2, '0');
	const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
	const yyyy = today.getFullYear();

	return `${yyyy}-${mm}-${dd}`;
};

export const getDateCount = tillDate => {
	let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
	let firstDate = new Date(getADDate());
	let secondDate = new Date(tillDate);

	// const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));
	const diffDays = Math.round((secondDate - firstDate) / oneDay);

	return diffDays;
};

export const getCurrentTime = (setLang = false) => {
	const today = new Date();

	const time = today.getHours().toString() + today.getMinutes().toString();

	if (setLang) {
		return translateDate(time);
	} else {
		return time;
	}
};

export const displayNepaliDate = date => {
	try {
		return translateEngToNepWithZero(date.split(' ')[0]);
	} catch (error) {
		return date;
	}
};
