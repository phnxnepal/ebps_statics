import { getLocalStorage } from './secureLS';
import {
	MENU_LIST,
	FORM_NAME_MASTER,
	BUILDING_CLASS,
	// PLOR_SANSODHAN,
	FORM_GROUP_MASTER,
	// PRABIDHIK_SANSODHAN,
	// DOSRO_CHARAN_SANSODHAN,
} from './constants';
import { getBuildingClass, isEmpty, getUserRole } from './functionUtils';
import { UserType, getStatusFieldName, getApprovalFieldName, userTypeHierarchy } from './userTypeUtils';
import { FormUrlFull } from './enums/url';
import { isStringEmpty } from './stringUtils';
// import { FormUrl, FormUrlFull } from './enums/url';
// import { ConstructionType, ConstructionTypeValue } from './enums/constructionType';

export const lastForms = [FormUrlFull.BUILDING_FINISH_CERTIFICATE, FormUrlFull.PURANO_GHAR_SAMPANNA_PRAMANPATRA, FormUrlFull.BUILDING_BUILD_CERTIFICATE];

export const getNextUrl = (currentUrl) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let url = '';

	if (lastForms.includes(currentUrl.trim())) {
		if (isHighestFormUser(currentUrl)) return '/user/task-list';
	}

	menuList.forEach((row, index) => {
		if (row.viewURL && row.viewURL.trim() === currentUrl) {
			if (menuList.length > index + 1) {
				url = menuList[index + 1].viewURL;
				if (url === '/user/forms/forward-to-next') {
					url += `/${menuList[index + 1].groupId}`;
				}
			}
		}
	});
	return url ? url.trim() : '/';
};

export const isHighestFormUser = (currentUrl) => {
	const formMaster = getFormMasterObj(currentUrl);
	const currentUserRole = getUserRole();

	let highestUser;

	const sortedUsers = userTypeHierarchy.sort((a, b) => (a.priority < b.priority ? 1 : -1));

	for (const user of sortedUsers) {
		const approvalField = getApprovalFieldName(user.user);
		if (formMaster[approvalField] && formMaster[approvalField] === 'Y') {
			highestUser = user.user;
			break;
		}
	}

	return currentUserRole === highestUser;
};

export const getPreviousUrl = (currentUrl) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let url = '';
	menuList.forEach((row, index) => {
		if (row.viewURL && row.viewURL.trim() === currentUrl) {
			if (menuList.length < index - 1) {
				url = menuList[index - 1];
			}
		}
	});
	return url ? url : {};
};

export const getCurrentUrlObj = (mainUrl) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let url = menuList.find((row) => row.viewURL.trim() === mainUrl.trim());
	return url ? url : {};
};

export const getCurrentUrl = (mainUrl) => {
	const url = getCurrentUrlObj(mainUrl);
	return url ? url.id : '/';
};

export const isInMenuList = (url) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let isPresent = menuList.some((row) => row.viewURL.trim() === url.trim());
	return isPresent ? isPresent : false;
};

// const variableUrls = [
// 	{
// 		currentUrl: '/user/forms/structure-design-classB-form',
// 		nextUrl: '/user/forms/sanitary-design',
// 	},
// 	{
// 		currentUrl: '/user/forms/structure-design-classc-form',
// 		nextUrl: '/user/forms/anusuchiga-view',
// 	},
// 	{
// 		currentUrl: '/user/forms/electrical-design',
// 		nextUrl: '/user/forms/anusuchiga-view',
// 	},
// ];

export const computeClassWiseNextUrl = (currentUrl, bClass) => {
	// const permitData = getBuildPermitObj();

	// console.log('gcueue', bClass);
	// if (currentUrl.trim() === '/user/forms/design-approval') {
	// 	// if (permitData.constructionType === ConstructionType.PURANO_GHAR) {
	// 	//   return getNextUrl(currentUrl);
	// 	// } else {
	// 	if (permitData.constructionType === ConstructionTypeValue.PURANO_GHAR || permitData.constructionType === ConstructionType.PURANO_GHAR) {
	// 		// console.log('bClass', bClass, isInMenuList(FormUrlFull.ARCHITECTURE_B), isInMenuList(FormUrlFull.ARCHITECTURE_C));
	// 		if (bClass === 'B' && isInMenuList(FormUrlFull.ARCHITECTURE_B)) {
	// 			return FormUrlFull.ARCHITECTURE_B;
	// 		} else if (bClass === 'C' && isInMenuList(FormUrlFull.ARCHITECTURE_C)) {
	// 			return FormUrlFull.ARCHITECTURE_C;
	// 		} else if (isInMenuList(FormUrlFull.MAP_TECH)) {
	// 			return FormUrlFull.MAP_TECH;
	// 		} else {
	// 			return getNextUrl(currentUrl);
	// 		}
	// 	} else {
	// 		return bClass === 'B'
	// 			? '/user/forms/architecture-design-classB-form'
	// 			: bClass === 'C'
	// 			? '/user/forms/architecture-design-classc-form'
	// 			: '/user/forms/anusuchiga-view';
	// 	}

	// 	// }
	// }

	// else if (currentUrl.trim() === `/user${FormUrl.STRUCTURE_B}`) {
	// 	if (permitData.constructionType === ConstructionTypeValue.PURANO_GHAR || permitData.constructionType === ConstructionType.PURANO_GHAR) {
	// 		return `/user${FormUrl.MAP_TECH}`;
	// 	}
	// } else if (currentUrl.trim() === `/user${FormUrl.STRUCTURE_C}`) {
	// 	if (permitData.constructionType === ConstructionTypeValue.PURANO_GHAR || permitData.constructionType === ConstructionType.PURANO_GHAR) {
	// 		return `/user${FormUrl.MAP_TECH}`;
	// 	}
	// }

	// const variableUrl = variableUrls.find(url => url.currentUrl === currentUrl.trim());
	// if (variableUrl && isInMenuList(variableUrl.nextUrl)) {
	// 	return variableUrl.nextUrl;
	// } else {
	return getNextUrl(currentUrl);
	// }
};

export const classUrlCurry = (bClass) => (url) => {
	return computeClassWiseNextUrl(url, bClass);
};

export const getClassWiseNextUrl = (url) => {
	const bClass = getBuildingClass();
	return computeClassWiseNextUrl(url, bClass);
};

export const getFormMasterObj = (currentUrl) => {
	const formMasterList = JSON.parse(getLocalStorage(FORM_NAME_MASTER));
	let url = formMasterList && formMasterList.find((row) => row.viewUrl && currentUrl && row.viewUrl.trim() === currentUrl.trim());
	return url ? url : {};
};

export const getFormMasterId = (currentUrl) => {
	const formMasterObj = getFormMasterObj(currentUrl);
	return !isEmpty(formMasterObj) ? formMasterObj.id : 1;
};

export const getFormMasterById = (id) => {
	const formMasterList = JSON.parse(getLocalStorage(FORM_NAME_MASTER));
	let url = formMasterList && formMasterList.find((row) => String(row.id) === String(id));
	return url ? url : {};
};

export const getFormMasterUrl = (id) => {
	const formObject = getFormMasterById(id);
	try {
		return formObject.viewUrl.trim();
	} catch {
		return '/';
	}
};

export const getGroupMasterObj = () => {
	const groupMasterObj = JSON.parse(getLocalStorage(FORM_GROUP_MASTER));
	return groupMasterObj ? groupMasterObj : {};
};

export const approvalFields = [
	{ userType: UserType.SUB_ENGINEER, field: 'subEngrApproval' },
	{ userType: UserType.ENGINEER, field: 'engrApproval' },
	{ userType: UserType.DESIGNER, field: 'designerApproval' },
	{ userType: UserType.ADMIN, field: 'chiefApproval' },
	{ userType: UserType.AMIN, field: 'aminApproval' },
	{ userType: UserType.RAJASWO, field: 'rajasowApproval' },
];

export const getDisableStatus = (currentUrl, prevData) => {
	const formObj = getFormMasterObj(currentUrl);
	let isDisabled = false;
	try {
		if (isEmpty(formObj)) {
			return true;
		} else {
			const rejected = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'R');
			if (rejected) {
				isDisabled = false;
			} else {
				isDisabled = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'A');
			}
		}
	} catch {
		isDisabled = true;
	}
	return isDisabled;
};

export const getDeleteStatus = (currentUrl, prevData, userData) => {
	const formObj = getFormMasterObj(currentUrl);
	let isDisabled = false;
	try {
		if (isEmpty(formObj) && isEmpty(prevData)) {
			return true;
		} else {
			const rejected = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'R');
			if (rejected) {
				isDisabled = false;
			} else {
				isDisabled = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'A');
			}
		}
	} catch {
		isDisabled = true;
	}
	let name = isDisabled === false && userData && formObj.enterBy === userData.userType ? true : false;
	return name;
};

export const getDeleteDisabledStatus = (currentUrl, prevData, userData) => {
	const formObj = getFormMasterObj(currentUrl);
	let isSaveDisabled = false;
	try {
		if (isEmpty(formObj) && isEmpty(prevData)) {
			return true;
		} else {
			const rejected = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'R');
			if (rejected) {
				isSaveDisabled = false;
			} else {
				isSaveDisabled = approvalFields.some((row) => formObj[row.field] === 'Y' && prevData[getStatusFieldName(row.userType)] === 'A');
			}
		}
	} catch {
		isSaveDisabled = true;
	}
	let hasDeletePermission = isSaveDisabled === false && userData && formObj.enterBy === userData.userType ? true : false;
	return { hasDeletePermission, isSaveDisabled };
};

export const dynamicUrlFilter = (inputUrl) => {
	const buildingClass = getLocalStorage(BUILDING_CLASS);
	// const plorSansodhan = getLocalStorage(PLOR_SANSODHAN);
	// const prabidhikSansodhan = getLocalStorage(PRABIDHIK_SANSODHAN);
	// const dosroCharanSansodhan = getLocalStorage(DOSRO_CHARAN_SANSODHAN);

	const filterKeys = [];

	// console.log('dynamic - ', buildingClass);

	if (buildingClass === 'C') {
		filterKeys.push('classB', 'electrical', 'sanitary');
	} else if (buildingClass === 'B') {
		filterKeys.push('classc');
	} else if (buildingClass === 'A' || buildingClass === 'D') {
		filterKeys.push('classB', 'electrical', 'sanitary', 'classc');
	}

	// if (plorSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_BIBARAN_PAHILO, FormUrl.PAHILO_CHARAN_BILL_VUKTANI);
	// }

	// if (prabidhikSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_TIPPANI, FormUrl.SAMSODHAN_BILL_VUKTANI);
	// }

	// if (dosroCharanSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_SUPER_STRUCTURE, FormUrl.DOSRO_CHARAN_BILL_VUKTANI)
	// }

	return inputUrl.filter((url) => !filterKeys.some((key) => url.url && url.url.includes(key)));
};

export const dynamicUrlFilterParam = (inputUrl, buildingClass) => {
	// const plorSansodhan = getLocalStorage(PLOR_SANSODHAN);
	// const prabidhikSansodhan = getLocalStorage(PRABIDHIK_SANSODHAN);
	// const dosroCharanSansodhan = getLocalStorage(DOSRO_CHARAN_SANSODHAN);

	const filterKeys = [];

	// console.log("dynamic - ", buildingClass);

	if (buildingClass === 'C') {
		filterKeys.push('classB', 'electrical', 'sanitary');
	} else if (buildingClass === 'B') {
		filterKeys.push('classc');
	} else if (buildingClass === 'A' || buildingClass === 'D') {
		filterKeys.push('classB', 'electrical', 'sanitary', 'classc');
	}

	// if (plorSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_BIBARAN_PAHILO, FormUrl.PAHILO_CHARAN_BILL_VUKTANI);
	// }

	// if (prabidhikSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_TIPPANI, FormUrl.SAMSODHAN_BILL_VUKTANI);
	// }

	// if (dosroCharanSansodhan !== 'Yes') {
	// 	filterKeys.push(FormUrl.SANSODHAN_SUPER_STRUCTURE, FormUrl.DOSRO_CHARAN_BILL_VUKTANI)
	// }

	return inputUrl.filter((url) => !filterKeys.some((key) => url.url && url.url.includes(key)));
};

export const getLastUrl = (url) => {
	const split = url.split('/');
	return split[split.length - 1];
};

export class LocalAPIArray {
	constructor(localApis) {
		this.localApis = localApis ? localApis : [];
	}

	getLocalApis() {
		return this.localApis;
	}
}

export class LocalAPI {
	constructor(key, objName) {
		this.key = key;
		this.objName = objName;
	}

	getKey() {
		return this.key;
	}

	getObjName() {
		return this.objName;
	}
}

export const areUrlsEqual = (url1, url2) => {
	try {
		const url1Array = url1.split('/').filter((url) => !isStringEmpty(url));
		const url2Array = url2.split('/').filter((url) => !isStringEmpty(url));

		return url1Array[url1Array.length - 1] === url2Array[url2Array.length - 1];
	} catch {
		return false;
	}
};

export const getRejectedBy = (taskObject) => {
	const rejectedFields = taskObject
		? (({ rejectedA, rejectedB, rejectedC, rejectedD, rejectedAD, rejectedE, rejectedF, rejectedG }) => ({ rejectedA, rejectedB, rejectedC, rejectedD, rejectedAD, rejectedE, rejectedF, rejectedG }))(taskObject)
		: {};

	const rejected = Object.keys(rejectedFields).find((field) => rejectedFields[field] === 'Y');

	if (rejected) {
		switch (rejected) {
			case 'rejectedC':
				return UserType.ADMIN;
			case 'rejectedB':
				return UserType.SUB_ENGINEER;
			case 'rejectedD':
				return UserType.DESIGNER;
			case 'rejectedA':
				return UserType.ENGINEER;
			case 'rejectedAD':
				return UserType.AMIN;
			case 'rejectedE':
				return UserType.POSTE;
			case 'rejectedF':
				return UserType.POSTF;
			case 'rejectedG':
				return UserType.POSTG;
			default:
				return '';
		}
	} else {
		return '';
	}
};
