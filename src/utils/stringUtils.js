export const isStringEmpty = value =>
  value === '' || value === undefined || value === null || value === 'undefined';

export const isNumberEmpty = (value) => {
  return isStringEmpty(value) || String(value) === '0' 
}
