import groupBy from 'lodash/groupBy';
import uniq from 'lodash/uniq';
import sortBy from 'lodash/sortBy';
import startCase from 'lodash/startCase';
import toLower from 'lodash/toLower';
import { floorMappingAll, distanceOptions, getOptionText, squareUnitOptions, floorMappingFlat } from './dataUtils';
import { getNumber, areaconvertor, lengthconvertor } from './mathUtils';
import { DEFAULT_UNIT_LENGTH } from './constants';
import { isEmpty, getUnitValue, getAreaUnitValue } from './functionUtils';
import { floorData } from './data/genericData';

export const mappingLetter = ['क) ', 'ख) ', 'ग) ', 'घ) ', 'ङ) ', 'च) ', 'ज) ', 'झ) ', 'ञ) ', 'ट) ', 'ठ) ', 'ड) ', 'ढ) ', 'ण) '];

const getPrefixedField = (fieldName, prefix) => {
	if (prefix) return `${prefix}.${fieldName}`;
	else return fieldName;
};

/**
 * Utility Class to handle permitData floor with block related functions  
 */
export class FloorBlockArray {
	/**
	 * Initialize permit data floor
	 * This formats the floor data into array of @see {@link FloorBlock} 
	 * @param {Object[]} floors permit data floor usually found within permitData.floor
	 */
	constructor(floors) {
		this.floors = floors
			? sortBy(
					floors.slice().filter((fl) => fl.floor <= 12),
					['block', 'floor'],
					['asc', 'asc']
			  ).map((floor) => new FloorBlock(floor.length, floor.height, floor.width, floor.area, floor.floor, floor.block))
			: [];
		this.floorsWithUnit = floors
			? sortBy(
					floors.slice().filter((fl) => fl.floor <= 12),
					['block', 'floor'],
					['asc', 'asc']
			  ).map((floor) => {
					return {
						length: floor.length,
						height: floor.height,
						width: floor.width,
						area: floor.area,
						floor: floor.floor,
						block: floor.block,
						floorUnit: floor.floorUnit,
						unitNepali: getUnitValue(floor.floorUnit),
						areaUnitNepali: getAreaUnitValue(floor.floorUnit),
					};
			  })
			: [];
		this.hasBlocks = this.floors.some((fl) => fl.block);
		this.blocks = uniq(this.floors.map((fl) => fl && fl.block));
		this.floorUnit = floors && floors.length > 0 ? floors[0].floorUnit : DEFAULT_UNIT_LENGTH;
		this.heightRelevantFloors = this.fetchHeightRelevantFloors();
		this.areaRelevantFloors = this.fetchAreaRelevantFloors();
		this.length = this.floors.length;
	}

	// Getters
	getFloors() {
		return this.floors;
	}

	getHasBlocks() {
		return this.hasBlocks;
	}

	getLength() {
		return this.length;
	}

	getFloorsWithUnit() {
		return this.floorsWithUnit;
	}

	getFloorUnit() {
		return this.floorUnit;
	}

	getBlocks() {
		return this.blocks;
	}

	getFloorsWithLabels(heightRelevant = false) {
		return this.floorsWithUnit
			.filter((fl) => (heightRelevant ? fl.floor < 11 : true))
			.map((floor) => {
				try {
					const floorName = floorMappingFlat.find((row) => String(row.floor) === String(floor.floor)).value;
					return {
						...floor,
						label: {
							sn: (idx) => mappingLetter[idx],
							floorName,
							floorBlockName: this.hasBlocks ? `${floorData.block} ${floor.block} ${floorName}` : floorName,
						},
					};
				} catch (err) {
					console.log('err', err);
					return {
						...floor,
						label: { sn: '', floorName: '', floorBlockName: '' },
					};
				}
			});
	}

	getNepaliFloorUnit() {
		return { unit: getOptionText(this.floorUnit, distanceOptions), areaUnit: getOptionText(this.floorUnit, squareUnitOptions) };
	}

	getEnglishFloorUnit() {
		if (this.floorUnit) {
			return { unit: startCase(toLower(this.floorUnit)), areaUnit: `Sq. ${startCase(toLower(this.floorUnit))}` };
		} else {
			return { unit: 'Feet', areaUnit: 'Sq. Feet' };
		}
	}

	getHeightRelevantFloors() {
		return this.heightRelevantFloors;
	}

	getAreaRelevantFloors() {
		return this.areaRelevantFloors;
	}

	getFloorByFloor(floor) {
		if (this.hasBlocks) {
			const requiredFloor = this.floors.filter((fl) => fl.getFloor() === floor);

			const returnObj = {};
			Object.entries(groupBy(requiredFloor, 'block')).forEach(([key, value]) => {
				returnObj[key] = (value && Array.isArray(value) && value[0]) || {};
			});
			return returnObj;
		} else {
			const requiredFloor = this.floors.find((fl) => fl.getFloor() === floor);
			if (requiredFloor) {
				return requiredFloor.getFloorObject();
			} else {
				return {};
			}
		}
	}

	getAllFields(excludeField, prefix = 'floor') {
		const suffix = ['length', 'width', 'height', 'area'];
		if (this.hasBlocks) {
			return this.floors
				.map((floor, index) => this.blocks.map((block) => suffix.map((sfx) => `${prefix}.${index}.${block}_${sfx}`)))
				.flat()
				.flat()
				.filter((row) => row !== excludeField);
		} else {
			return this.floors
				.map((floor, index) => [
					`${prefix}.${index}.length`,
					`${prefix}.${index}.width`,
					`${prefix}.${index}.height`,
					`${prefix}.${index}.area`,
				])
				.flat()
				.filter((row) => row !== excludeField);
		}
	}

	getAllCustomsFields(excludeField, prefix = 'floor', suffix = []) {
		if (this.hasBlocks) {
			return this.floors
				.map((floor, index) => this.blocks.map((block) => suffix.map((sfx) => `${prefix}.${index}.${block}_${sfx}`)))
				.flat()
				.flat()
				.filter((row) => row !== excludeField);
		} else {
			return this.floors
				.map((floor, index) => suffix.map((sfx) => `${prefix}.${index}.${sfx}`))
				.flat()
				.filter((row) => row !== excludeField);
		}
	}

	getAllReducedBlockFields(suffix, excludeField, prefix = null) {
		if (this.hasBlocks) {
			return this.blocks
				.map((block) => suffix.map((sfx) => getPrefixedField(`${block}_${sfx}`, prefix)))
				.flat()
				.filter((row) => row !== excludeField);
		} else {
			return getPrefixedField(suffix, prefix);
		}
	}

	getSumOfAreas() {
		try {
			if (this.hasBlocks) {
				return this.formatReduceBlock(
					this.areaRelevantFloors,
					(accValue, valueObject) => accValue + getNumber(parseFloat(valueObject.area)),
					0.0
				);
			} else {
				return this.areaRelevantFloors.reduce((acc, curr) => acc + getNumber(parseFloat(curr.area)), 0.0);
			}
		} catch {
			return 0;
		}
	}

	getSumOfHeights() {
		try {
			if (this.hasBlocks) {
				return this.formatReduceBlock(
					this.heightRelevantFloors,
					(accValue, valueObject) => accValue + getNumber(parseFloat(valueObject.height)),
					0.0
				);
			} else {
				return this.heightRelevantFloors.reduce((acc, curr) => acc + getNumber(parseFloat(curr.height)), 0.0);
			}
		} catch {
			return 0;
		}
	}

	prepareFloorObject(floor, index, block = null) {
		const floorLabels = floorMappingAll.find((f) => String(f.floor) === String(floor.floor));
		return {
			...floor,
			fieldName: (prefix, suffix) => this.getFieldName(prefix, index, suffix, block),
			label: {
				sn: (idx) => mappingLetter[idx],
				floorName: floorLabels.nepaliName,
				floorBlockName: this.hasBlocks ? `${floorData.block} ${floor.block} ${floorLabels.nepaliName}` : floorLabels.nepaliName,
				...floorLabels,
			},
		};
	}

	formatReduceBlock(floors, callback, accumulatorInitialValue) {
		const returnObj = {};
		Object.entries(groupBy(floors, 'block')).forEach(([key, value]) => {
			returnObj[key] = value.reduce(callback, accumulatorInitialValue);
		});
		return returnObj;
	}

	formatBlock = (floors) => {
		return groupBy(floors, 'block');
	};

	getPlinthFloor() {
		const gFloor = this.getFloorByFloor(1);
		if (isEmpty(gFloor)) {
			return {};
		} else {
			return {
				plinthDetails: gFloor.area,
				plinthLength: gFloor.length,
				plinthWidth: gFloor.width,
			};
		}
	}

	fetchHeightRelevantFloors() {
		return this.floors.filter((fl) => fl.floor < 11).map((flr) => flr.getFloorObject());
	}

	fetchAreaRelevantFloors() {
		return this.floors.filter((fl) => fl.floor < 11).map((flr) => flr.getFloorObject());
	}

	getTopAndBottomFloors() {
		// const heightRelevantFloors = this.heightRelevantFloors;
		// const topFlr = heightRelevantFloors.reduce((prev, current) => (prev.floor > current.floor ? prev : current)).floor;
		// const bottomFlr = heightRelevantFloors.reduce((prev, current) => (prev.floor < current.floor ? prev : current)).floor;

		// const topFloor = floorMappingAll.find(fl => fl.floor === topFlr);
		// const bottomFloor = floorMappingAll.find(fl => fl.floor === bottomFlr);

		return { topFloor: this.getTopFloor(), bottomFloor: this.getBottomFloor() };
	}

	getTopFloor() {
		return this.formatFloorReduceObject(this.heightRelevantFloors, (prev, current) => (prev.floor > current.floor ? prev : current), {});
	}

	getBottomFloor() {
		return this.formatFloorReduceObject(this.heightRelevantFloors, (prev, current) => (prev.floor < current.floor ? prev : current), {});
	}

	getFormattedFloors = () => {
		return this.formatFloorObject(this.floors);
	};

	getAllFieldBlockNames = (suffix) => {
		return this.blocks.map((block) => `${block}_${suffix}`);
	};

	formatBlockFloorData = (floorObject, { prefix = '', mid = '', suffix = '' }) => {
		if (this.hasBlocks) {
			return Object.entries(floorObject)
				.map(([block, obj]) => `${prefix} ${block} ${mid} ${obj.floor} ${suffix}`)
				.join(', ');
		} else {
			return `${floorObject.floor} ${suffix}`;
		}
	};

	formatFloorReduceObject = (floors, callback, accumulatorInitialValue) => {
		try {
			let formattedFloors,
				returnFloors = {};
			if (this.hasBlocks) {
				formattedFloors = this.formatReduceBlock(floors, callback, accumulatorInitialValue);
				Object.entries(formattedFloors).forEach(([key, value]) => {
					returnFloors[key] = {
						...floorMappingAll.find((f) => String(f.floor) === String(value.floor)),
						...value,
						fieldName: (suffix, prefix = null) => this.getReducedFieldName(suffix, key, prefix),
					};
				});
			} else {
				formattedFloors = floors.reduce(callback);
				returnFloors = {
					...floorMappingAll.find((f) => String(f.floor) === String(formattedFloors.floor)),
					...formattedFloors,
					fieldName: (suffix, prefix = null) => this.getReducedFieldName(suffix, null, prefix),
				};
			}
			return returnFloors;
		} catch {
			return {};
		}
	};

	formatFloorObject = (floors) => {
		try {
			let formattedFloors,
				returnFloors = {};
			if (this.hasBlocks) {
				formattedFloors = this.formatBlock(floors);
				Object.entries(formattedFloors).forEach(([key, floors]) => {
					returnFloors[key] = floors.map((value, index) => this.prepareFloorObject(value, index, key));
				});
			} else {
				returnFloors = floors.map((value, index) => this.prepareFloorObject(value, index));
			}
			return returnFloors;
		} catch (err) {
			console.log('Error', err);
			return [];
		}
	};

	getInitialValues = (valueField, suffix, floors) => {
		const initialValueArray = [];

		if (this.hasBlocks) {
			Object.entries(floors).forEach(([block, floorObj]) => {
				floorObj.forEach((fl, index) => {
					if (!initialValueArray[index]) {
						initialValueArray[index] = {};
					}
					initialValueArray[index][`${block}_${suffix}`] = fl[valueField];
				});
			});
		} else {
			floors.forEach((fl, index) => {
				if (!initialValueArray[index]) {
					initialValueArray[index] = {};
				}
				initialValueArray[index][suffix] = fl[valueField];
			});
		}

		return initialValueArray;
	};

	getFloorWiseInitialValues(valueField, suffix, floors) {
		const initialValueArray = [];

		if (this.hasBlocks) {
			Object.entries(floors).forEach(([block, floorObj]) => {
				floorObj.forEach((fl, index) => {
					if (!initialValueArray[fl.floor]) {
						initialValueArray[fl.floor] = {};
					}
					initialValueArray[fl.floor][`${block}_${suffix}`] = fl[valueField];
				});
			});
		} else {
			floors.forEach((fl, index) => {
				if (!initialValueArray[fl.floor]) {
					initialValueArray[fl.floor] = {};
				}
				initialValueArray[fl.floor][suffix] = fl[valueField];
			});
		}

		return initialValueArray;
	}

	getMultipleInitialValues = (valueFields, floors, blockGrouping = true) => {
		const initialValueArray = [];

		if (this.hasBlocks && blockGrouping) {
			Object.entries(floors).forEach(([block, floorObj]) => {
				floorObj.forEach((fl, index) => {
					if (!initialValueArray[index]) {
						initialValueArray[index] = {};
					}
					valueFields.forEach((field) => {
						initialValueArray[index][`${block}_${field}`] = fl[field];
					});
				});
			});
		} else {
			floors.forEach((fl, index) => {
				if (!initialValueArray[index]) {
					initialValueArray[index] = {};
				}

				valueFields.forEach((field) => {
					initialValueArray[index][field] = fl[field];
				});
			});
		}

		return initialValueArray;
	};

	getInitialValue = (valueField, suffix, floor) => {
		let initialValue = {};

		const setValue = (key, floorObject, block = null) => {
			if (Array.isArray(valueField)) {
				for (const field of valueField) {
					if (block !== null) {
						initialValue[`${block}_${field}`] = floorObject[field];
					} else if (key) {
						initialValue[`${key}_${field}`] = floorObject[field];
					} else {
						initialValue[field] = floorObject[field];
					}
				}
			} else {
				if (valueField) initialValue[key] = floorObject[valueField];
				else if (key) initialValue[key] = floorObject;
				else initialValue = floorObject;
			}
		};

		if (this.hasBlocks) {
			Object.entries(floor).forEach(([block, floorObj]) => {
				setValue(`${block}_${suffix}`, floorObj, block);
			});
		} else {
			setValue(suffix, floor);
		}
		return initialValue;
	};

	getFloorSchema = (fields, schema) => {
		const formattedSchema = {};
		if (this.hasBlocks) {
			this.blocks.forEach((block) => {
				fields.forEach((field) => {
					formattedSchema[`${block}_${field}`] = schema;
				});
			});
		} else {
			fields.forEach((field) => {
				formattedSchema[field] = schema;
			});
		}
		return formattedSchema;
	};

	getFieldName = (prefix, index, suffix, block) => {
		if (block !== undefined && block !== null) return `${prefix}.${index}.${block}_${suffix}`;
		else return `${prefix}.${index}.${suffix}`;
	};

	getReducedFieldName = (suffix, block, prefix = null) => {
		if (block !== undefined && block !== null) return getPrefixedField(`${block}_${suffix}`, prefix);
		else return getPrefixedField(suffix, prefix);
	};
}

/**
 * Class representing each Floor with blocks
 */
export class FloorBlock {
	constructor(length, height, width, area, floor, block) {
		this.length = length;
		this.height = height;
		this.width = width;
		this.area = area;
		this.floor = floor;
		this.block = block;
	}

	getFloor() {
		return this.floor;
	}

	getFloorObject() {
		return {
			length: this.length,
			height: this.height,
			width: this.width,
			area: this.area,
			floor: this.floor,
			block: this.block,
		};
	}
}

export const converToFeet = (floors) => {
	const unit = 'FEET';
	const unitArea = 'SQUARE FEET';
	const prevUnit = 'METRE';
	const prevUnitArea = 'SQUARE METRE';
	return floors
		? floors.map((floor) => ({
				...floor,
				area: areaconvertor(floor.area, unitArea, prevUnitArea),
				height: lengthconvertor(floor.height, unit, prevUnit),
				length: lengthconvertor(floor.length, unit, prevUnit),
				width: lengthconvertor(floor.width, unit, prevUnit),
				floorUnit: unit,
		  }))
		: [];
};
