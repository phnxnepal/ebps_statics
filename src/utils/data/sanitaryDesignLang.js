export const sanitaryDesignLang = {
    heading_1: 'प्राबिधिक विवरण फारमहरु',
    heading_2: '(ख​) स्यानिटरी डिजाइन सम्बन्धि',
    heading_3: '(सम्बन्धित प्राबिधिक वा परामर्शदाताबाट भराउनु पर्ने)',
    heading_4: 'Checklist for NBC 208:2003 - Sanitary and Plumbing Design Requirements',
    sub_desc: '(to be fill up only for selected buildings like Commercial Complexed, Educational/Institutional Building, Industrial Buildings, Assemble Buildings, Hotels, Hostels & other buildings having mass public movement for "Class A & B")',
    table1: {
        thead_1: 'Description',
        thead_2: 'Design Capacity',
        thead_3: 'Water consumption per capital per day as per submitted design',
        thead_4: 'Water Storage Capacity',
        thead_5: 'Remarks',
        section1: {
            description: 'Underground Water Tank',
            description_2: '1. Type of building',
            td_cell_1: '1.2 Auditorium',
            td_cell_2: 'A.1.2 Hospital including laundry per bed',
            td_cell_3: 'a) Number of beds <= 100 Bed',
            td_cell_4: 'b) Number of bed >100 Bed',
            td_cell_5: '1.3 Office building',
        },
        section2: {
            description: '2. Overhead water tank for Lavatory',
            td_cell_1: 'a) Auditorium/Office Building',
            td_cell_2: 'b) Hospital',
        }
    },
    table2: {
        thead_1: 'Description',
        thead_2: 'Design Capacity',
        thead_3: 'Fixture provided as per submitted design',
        thead_4: 'Total',
        thead_5: 'Remarks',
        section1: {
            description: '2.1 Fire Hydrant System. Hospital/Auditorium (Indoor)',
            td_cell_1: '2.2 No of floors',
            td_cell_2: '2.3 Floor area',
            td_cell_3: '2.4 Capacity of wet riser for underground water tank',
        },
        section2: {
            description: '2.2 Type of buildings',
            td_cell_1: 'Office building',
        },
        section3: {
            description: 'Gents Toilet: Nos of users',
            td_cell_1: 'a) Water closet',
            td_cell_2: 'b) Urinal',
            td_cell_3: 'c) Basin',
        },
        section4: {
            description: 'Ladies Toilet: Nos of users',
            td_cell_1: 'a) Water closet'
        },
        section5: {
            description: 'Auditorium',
            description_2: 'Public toilet (Gents Toilet): Nos of users',
            td_cell_1: 'a) Water closet',
            td_cell_2: 'b) Urinal',
            td_cell_3: 'c) Basin',
        },
        section6: {
            description: 'Ladies Toilet: Nos of users',
            td_cell_1: 'a) Water closet'
        },
        section7: {
            description: 'Staff toilet (Ladies/Gents Toilet): Nos of users',
            td_cell_1: 'a) Water closet'
        },
        section8: {
            description: 'Hospital indoor patient ward (For Ladies and Gents Toilet): Nos of users',
            td_cell_1: 'a) Water closet',
            td_cell_2: 'b) Wash basin',
            td_cell_3: 'c) Bath (shower)',
            td_cell_4: 'd) Cleaner sink (Kitchen sink)',
        },
    }
}