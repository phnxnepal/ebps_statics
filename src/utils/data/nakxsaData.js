export const naksaData = {
	structureDesign: {
		heading: 'श्री प्रमुख प्रशासकीय अधिकृत ज्यु',
		//----------------------------
		subheading1: 'विषय : भवन/घरनक्सा जाँच गराई प्रतिवेदन पेश गरेको बारे ।',
		subheading2: '( कार्यालय प्रयोजनको लागि मात्र )',
		//-------------------------------------------
		date: 'मिति : ',
		ward_no: 'वडा नं.',
		sadak: 'सडक',
		kitta_no: 'कि.नं.',
		area: 'को जम्मा क्षेत्रफल',
		//-------------------------------------------
		main_head: 'श्रीमान्,',
		main_data: 'को जग्गामा भवन निर्माणको लागि घरधनी श्री',
		main_data1:
			'द्वारा नक्सा/डिजाईन पेश गरी नक्सा पासको लागि निवेदन पेश भएको हुनाले उक्त भवन/घरनक्सा कार्याविधि २०६९ अनुरूप जाँच गरी, निवेदन पेश गरेको छु । घरनक्सा मापदण्ड तथा कार्याविधि अनुसारको प्राविधिक विवरणहरू निम्नानुसार छन् ।',
		//-------------------------------------------
		table: {
			heading: 'भवनको प्राविधिक विवरणहरु',
			rows: {
				row1: {
					sn: 'क्र.सं.',
					name: 'विवरण',
					input_1: 'डिजाईन अनुसार',
					input_2: 'कैफियत',
				},
				row2: {
					sn: '१',
					name: 'भवनको वर्गीकरण',
					input_1: ['A', 'B', 'C', 'D'],
					input_2: [],
				},
				row3: {
					sn: '२',
					name: 'भवनको प्रयोग',
					input_1: ['आवासीय', 'व्यापारिक', 'अन्य'],
					input_2: [],
				},
				row4: {
					sn: '३',
					name: 'प्लिन्थ एरिया',
					input_1: [],
					input_2: [],
				},
				row5: {
					sn: '४',
					name: 'तला संख्या',
					input_1: ['से.मी. / वेसमेन्ट', 'जमिन/भुई तला', '१', '२', '३', '४', '५', '६', '७', '८', '९'],
					input_2: [],
				},
				row6: {
					sn: '५',
					name: 'कुल उचाइ',
					input_1: ['साविक', 'प्रस्तावित'],
					input_2: [],
				},
				row7: {
					sn: '६',
					name: 'भवन निर्माणको किसिम',
					input_1: ['नयाँ निर्माण', 'तला थप', 'पुरानो घर'],
					input_2: [],
				},
				row8: {
					sn: '७',
					name: 'भवनको स्ट्रक्चरल सिस्टम',
					input_1: ['पिलर', 'गारो', 'अन्य'],
					input_2: [],
				},
				row9: {
					sn: '८',
					name: 'माटोको प्रकार',
					input_1: [],
					input_2: [],
				},
				row10: {
					sn: '९',
					name: 'माटोको भार वहन क्षमता (सोयल वियरिङ क्यापासिटि)',
					input_1: [],
					input_2: [],
				},
				row11: {
					sn: '(क)',
					name: 'पिलर सिस्टम भएमा',
					input_1: [],
					input_2: [],
				},
				row12: {
					sn: '१',
					name: 'जगको प्रकार',
					input_1: [],
					input_2: [],
				},
				row13: {
					sn: '२',
					name: 'जगको गहिराई',
					input_1: [],
					input_2: [],
				},
				row14: {
					sn: '३',
					name: 'जगको साइजहरु',
					input_1: [],
					input_2: [],
				},
				row15: {
					sn: '४',
					name: 'पिलरका साइजहरु',
					input_1: [{ prefix: '३००cm X ३००cm' }],
					input_2: [],
				},
				row16: {
					sn: '५',
					name: 'पिलरको प्रयोग गर्ने डणडीका साइज र संख्या',
					input_1: [],
					input_2: [],
				},
				row17: {
					sn: '६',
					name: 'बिमको स्पान',
					input_1: ['१६ मीमी व्यास', 'रिंग - ८ मीमी', '१२ मीमी व्यास', 'अन्य'],
					input_2: [],
				},
				row18: {
					sn: '७',
					name: 'बिमको साइजहरु',
					input_1: [],
					input_2: [],
				},
				row19: {
					sn: '८',
					name: 'स्लायाबको मोटाइ',
					input_1: [],
					input_2: [],
				},
				row20: {
					sn: '९',
					name: 'कंग्क्रीटको ग्रेड (सीमेन्ट: बालुवा, रोडा)',
					input_1: ['M15 (1:2:4)', 'M20 (1:1.5:3)', 'M25 (1:1:2)', 'अन्य'],
					input_2: [],
				},
				row21: {
					sn: '१०',
					name: 'अन्य',
				},
				row22: {
					sn: '(ख)',
					name: 'गारो सिस्टम भएमा',
					input_1: [],
					input_2: [],
				},
				row23: {
					sn: '१',
					name: 'जगको गहिराइ',
					input_1: [],
					input_2: [],
				},
				row24: {
					sn: '२',
					name: 'जगको चौडाई',
					input_1: [],
					input_2: [],
				},
				row25: {
					sn: '३',
					name: 'इट्टाको क्रस्सिङ्ग स्ट्रेंग्थ',
					input_1: [],
					input_2: [],
				},
				row26: {
					sn: '४',
					name: 'कंग्क्रीटको ग्रेड (सीमेन्ट: बालुवा, रोडा)',
					input_1: ['M15 (1:2:4)', 'M20 (1:1.5:3)', 'M25 (1:1:2)'],
					input_2: [],
				},
				row27: {
					sn: '५',
					name: 'स्लायाबको मोटाइ',
					input_1: [],
					input_2: [],
				},
				row28: {
					sn: '६',
					name: 'फ्लोर बिमको साइज',
					input_1: [],
					input_2: [],
				},
				row29: {
					sn: '७',
					name: 'गारोमा सिमेन्ट, बालुवाको भाग',
					input_1: [{ prefix: 'M15 (1:2:4)' }],
					input_2: [],
				},
				row30: {
					sn: '८',
					name: 'गारोको विवरण',
					input_1: ['गारोको उचाई', 'गारोको मोटाई', 'गारोको लम्बाई'],
					input_2: [],
					name1: 'भुइँ तल्ला',
					input_1_1: ['', '', ''],
					input_2_1: [],
					name2: 'पहिलो तल्ला',
					input_1_2: ['', '', ''],
					input_2_2: [],
					name3: 'दोस्स्रो तल्ला',
					input_1_3: ['', '', ''],
					input_2_3: [],
				},
				row31: {
					sn: '९',
					name: 'कंग्क्रीट ब्याण्डहरु राखिएको छ/छैन',
					input_1: ['प्लिन्थ लेभलमा', 'शिल लेभलमा', 'लिन्टेल लेभलमा', 'छानाको लेभलमा'],
					input_2: [],
				},
				row32: {
					sn: '१०',
					name: 'भर्टिकल डणडीको साइज(कुन र कर्नर ज्वैनतहरुमा)',
					input_1: [],
					input_2: [],
				},
				row33: {
					sn: '११',
					name: 'कर्नर स्तिचंग कंग्क्रीट ब्याण्डहरु राखिएको छ/छैन',
					input_1: ['छ', 'छैन'],
					input_2: [],
				},
				row34: {
					sn: '१२',
					name: 'अन्य',
				},
			},
		},
		customInputs: {
			buildingClass: 'भवनको वर्गिकरण',
			purposeOfConstruction: 'भवनको प्रयोग',
			// plinthArea: 'प्लिन्थ एरिया',
			floorNumber: 'तला संख्या',
			buildingHeight: 'घरको कुल उचाई',
			constructionType: 'भवन निर्माणको किसिम',
			// beamSpan: 'विमको स्थान',
			structuralType: 'भवनको स्ट्रक्चरल सिस्टम',
			pillarDetails: 'पिलरमा प्रयोग गर्ने डण्डीका साइज र संख्या',
			concreteGrade: 'कंक्रिटको ग्रेड (सिमेन्टःबालुवाःरोडा)',
			concreteBand: 'कंक्रिट ब्याण्डहरु राखिएको छ/छैन',
			concreteGradeGaro: 'कंक्रिटको ग्रेड (सिमेन्टःबालुवाःरोडा)',
			concreteBandGaro: 'कंक्रिट ब्याण्डरु राखिएको छ/छैन',
			garoBibaran: 'गारोको विवरण',
			cornerStiching: 'कर्नर स्टिचिङ कंक्रिट व्याण्डहरु राखिएको छ/छैन',
			plinthArea: 'प्लिन्थ एरिया',
			landLength: 'जग्गाको लम्बाई',
			landWidth: 'जग्गाको चौडाई',
		},
		//--------------------------------------------
		footer: {
			recommendtitle: 'सिफारिस पेश गर्नेको',
			fullname: 'नाम, थर : ',
			level: 'पद : ',
			signature: 'हस्ताक्षर : ',
			miti: 'मिति : ',
		},
	},
};

export const noObjectionData = {
	title: 'NO OBJECTION SHEET',
	salutation1: 'श्रीमान् शाखा प्रमुख ज्यू',
	salutation2: 'बस्ती विकास तथा घरनक्सा शाखा',
	body: {
		mahodaya: 'महोदय',
		sabik: 'तपाईले साविक',
		halWada: 'हालको वडा नं.',
		sadak: 'सडक',
		kitta: 'को कि.न.',
		chetraphal: 'को जम्मा क्षेत्रफल',
		letter:
			'भवन निर्माणको लागि नक्सा/डिजाइन पेश गर्न पर्ने भएकोमा सो माथि कारवाही हुँदा पेश गरिएको नक्सा/डिजाइन स्वीकृत मापदण्ड र राष्ट्रिय भवन निर्माण संहिता बमोजिम भएकोले यो सिफारिस प्रमाण-पत्र (No Objection Certificate) प्रदान गरिएका छ । स्वीकृत नक्सा र भवन निर्माण संहिता बमोजिम निर्माण कार्यमा भूकंप सुरक्षात्मक प्रविधि अवलम्बन गरी दिन भई सरक्षित नगर निर्माण गर्ने प्रयासमा सहयोग गरिदिनुहुन हार्दिक अनुरोध गरिन्छ ।',
	},
	bodyInaruwa: {
		title_1: 'भूकम्प शाखा',
		title_2: 'भवन डिजाईन सिफारिस प्रमाण-पत्र',
		title_3: 'NO OBJECTION CERTIFICATE',
		address: 'ठेगाना ',
		basney: 'बस्ने',
		pramanPatra: 'प्रमाणपत्र नं.',
		content_1: 'महाशय',
		content_2: 'तपाईले ',
		content_3: ' वडा नं.',
		content_4: 'साविक',
		content_5: 'हाल',
		content_6:
			'राष्ट्रिय भवन निर्माण संहिता अनुरुप नक्शा/डिजाईन पेश गर्नु भएकोमा यो सिफारिस प्रमाण पत्र (No Objection Certificate) दिईएको छ। स्वीकृत नक्शा र भवन निर्माण संहिता बमोजिम निर्माण कार्यमा भूकम्प सुरक्षात्मक प्रविधि अवलम्बन गरी दिनु भई सुरक्षित नगर निर्माण गर्ने प्रयासमा सहयोग गरिदिनु हुन हार्दिक अनुरोध गरिन्छ ।',
	},
	table: {
		heading: 'भवनको प्राविधिक विवरणहरु',
	},
	footer: {
		signatureInaruwa: [
			['पेश गर्ने', 'जु.इन्जिनियर', 'भूकम्प सुरक्षा शाखा'],
			['सिफारिस गर्ने', 'इन्जिनियर', 'भूकम्प सुरक्षा शाखा'],
			['सिफारिस गर्ने', 'इन्जिनियर', 'नक्सा शाखा'],
			['सदर गर्ने', 'प्रमुख प्रशासकिय अधिकृत'],
		],
		signature: [
			['सिफारिस गर्ने', 'सब इन्जिनियर'],
			['जारी गर्ने', 'इन्जिनियर'],
		],
		note:
			'स्वीकृत नक्सा भवन निर्माण संहिता विपरीत निर्माण कार्य गरिएको पाइएमा न.पा. को नियम बमोजिम कारबाही हुने तथा निर्माण सम्पन्न प्रमाण-पत्र दिन यस कार्यालय वाध्य हुने छैन ।',
		section1: 'स्वीकृत नक्सा अनुसार हाललाई',
		section2: 'तल्ला निर्माण गरिनेछ । भविष्यमी मापदणड अनुसार तल्ला निमाण गर्ने योजना छ ।',
		gharDhani: 'घर धनीको नाम',
		date: 'मिति',
		address: 'ठेगाना',
		sahi: 'सहि',
	},
};
export const requiredData = {
	miti: ' मिति ',
	shriman: ' श्रीमान ',
	shrimati: ' श्रीमती ',
	sushri: ' सुश्री ',
	thegana: ' ठेगाना ',
	basne: ' बस्ने ',
	mahasaye: ' महासय, ',
	sabik: ' तपाइले बिराटनगर महानगरपालिका वडा न. साविक ',
	hal: ' हाल ',
	ko: ' को ',
	kittaNo: ' कि. न. ',
	rastryiabhawan: ' राष्ट्रिय भवन सहिता ',
	naksha: ' नक्शा ',
	design: ' डिजाइन ',
	dataLine:
		' पेश गर्नु भएकोमा यो सिफारिस  प्रमाण पत्र (NO Objection Certificate) दिइएको छ। स्वीकृत नक्शा र भवन निर्माण सहित बमोजिम निर्माण कार्यमा भुकम्प सुरक्षात्मक प्रबिधि अबलम्बन गरि दिनु भै सुरक्षित नगर निर्माण गर्ने प्रयासमा सहयोग गरिदिनु हुन हार्दिक अनुरोध गरिन्छ। ',

	swikritNakshaAnusar: ' स्वीकृत नक्शा अनुसार हाललाई ',
	tallaNiramanGarineCha: ' तल्ला निर्माण गरिनेछ। ',
	bhabisyamaMapdanda: ' भविष्यमा मापदण्ड अनुसार थप ',
	tallaNirman: ' तल्ला निर्माण गर्ने योजना छ। ',
	gharDhaniKoNaam: ' घर धनीको नाम ',
	sahi: ' सहि ',

	peshGarne: ' पेश गर्ने ',
	sifarisGarne: ' सिफारिस गर्ने ',
	sadarGarne: ' सदर गर्ने ',
	juEngineer: ' जु इन्जिनीयर ',
	engineer: '  इन्जिनीयर ',
	pramukhPrasasakiyaAdhikrit: ' प्रमुख प्रसासकिय अधिकृत ',
	bhukampaSurakshyaSakha: ' भुकम्प सुरक्षा साखा ',
	gharNakshaSakhaPramukh: ' घर नक्शा साखा प्रमुख ',
	pramanPatraNumber: ' प्रमाण पत्र न. ',
	footer:
		' स्वीकृत नक्शा र भवन निर्माण सहिता बिपरित कार्य गरिएको पाइएमा न पा को नियम बमोजिम कारबाही हुने तथा निर्माण सम्पन्न प्रमाण पत्र दिन एस कार्यालय बाध्य हुने छै।  ',
};

export const sthalgatNirikshyanGariPratibedanReqData = {
	shrimanSakhaPramukh: ' श्रीमान शाखा प्रमुख ज्यु ',
	bhukampaSurakshyaSakha: ' भुकम्प सुरक्षा शाखा ',
	miti: ' मिति ',
	bisaye: ' विषय : स्थलगत निरिक्षण  गरि प्रतिवेदन पेश गरेको बारे।  ',
	karyalayaPrayojanKoLagiMatra: '  (कार्यालय प्रयोजनको लागि मात्र) ',
	shriman: ' श्रीमान ',
	wadaNo: ' वडा न. ',
	sthan: ' स्थान  ',
	kiNo: ' कि. न. ',
	koJammaXetraFal: ' को जम्मा क्षे. फ. ',
	koJaggamaBhawan: ' को जग्गामा भवन निर्माणका लागि घर धनि ',
	dwaraNakshaDesignMiti: ' द्वारा नक्शा र डीजाइन पेश गरि No-Objection Certificate ( भवन डीजाइन सिफारिस प्रमाणपत्र ) को लागि निबेदन परेकोमा मिति',
	chana: ' च. न. ',
	data: ' पत्रको तोक आदेश अनुसार स्थलगत निरिक्षण गरि प्रतिबेदन पेश गरेको छ।  भवन निर्माण सहित अनुसार प्राबिधिक विवरणहरु निम्ननुसार छन्। ',
	kaufiyat: ' कौफियत ',
	peshGarne: ' पेश गर्ने ',
	juEKoNaam: ' जु इ को नाम ',
	sahi: ' सहि ',
	engineerKoNaam: ' इन्जिनीयरको नाम ',
	engineerKoSahi: ' इन्जिनीयरको सहि ',
	engineerKoRaya: ' इन्जिनीयरको राय  ',
};
