import { tiedOptions, parekoOptions, bhayekoPartialOptions } from '../optionUtils';

export const pillarGaraTable = {
	table: {
		indices: [0, 1, 2, 3, 4],
		pillar: {
			title: 'पिल्लर वाला भएमा',
			lines: [
				{ line: 'पिल्लर १२"-१२"', name: 'pillar' },
				{ line: 'पिल्लर ग्रिडमा', name: 'pillarGrid' },
				{ line: 'वेश विम', name: 'baseBeam' },
				{ line: 'स्ट्रायपबीम', name: 'strapBeam' },
				{ line: 'बीमा र पिल्लरको जोर्नी पर्याप्त मात्रामा', name: 'jointValid', options: tiedOptions },
			],
		},
		gara: {
			title: 'गारो वाला घर भएमा',
			lines: [
				{ line: 'पिल्लर १०"-१०"', name: 'pillarGara' },
				{ line: 'प्रत्येक गारो २ फिटमा स्टिच', name: 'garaTwoFeet' },
				{ line: 'सबै कुनामा ठाडो रड', name: 'cornerRod' },
				{ line: 'झ्याल र भेन्टिलेशनहरु गारोको २ फिट टढा', name: 'windowGaraSpace' },
			],
		},
	},
};

export const yesNoSection = {
	title: 'भवन तथा योजना मापदण्ड',
	yesNoLines: [
		{ line: 'सड़क अधिकार क्षेत्र सम्बन्धी मापदण्ड (सडक सेटव्याक) पालना', name: 'mapdandaPalanaBhayeko', otherName: 'mapdandaPalana' },
		{ line: 'साइड प्लानमा देखाइए बमोजिम साईड सेटव्याक पालना', name: 'setBackPlanBhayeko', otherName: 'setBackPlan' },
		{
			line: 'ग्राउण्ड कभरेज फरक',
			name: 'groundCoverageBhayeko',
			otherName: 'groundCoverage',
			options: parekoOptions,
			placeholder: 'फरक परे विवरण खुलाउने',
		},
		{
			line: 'फिल्ड र नक्सामा फेरबदल',
			name: 'changesMadeYes',
			otherName: 'changesMade',
		},
		{
			line: 'नेपाल राष्ट्रिय भवन संहिता २०६० को पालना',
			name: 'buildingConstFollowBhayeko',
			otherName: 'buildingConstFollow',
			options: bhayekoPartialOptions,
			suffix: ' (तपसिलको विवरणमा मार्क लगाउने)',
		},
	],
	anya: 'अन्य कुनै कुरा भए खुलाउने',
};

export const plinthLevelOwnerData = {
	userInfo: {
		le: 'ले',
		end: 'निर्माण गर्ने क्रममा प्लिन्थ लेभलसम्मको निर्माण कार्य सम्पन्न भएको देहाय बमोजिम प्रतिवेदन पेश गरेको छु।',
	},
	signatureSection: {
		paragraph: 'उपरोक्त बमोजिमको प्रतिवेदन म आफैले सुपरिवेक्षण गरी पेश गरेको हूँ। अन्यथा भएमा नियमानुसार सहुला बुझाउँला।',
		sign: 'हस्ताक्षर',
		superibechyak: 'सुपरीवेक्षकको नाम',
		nepalId: 'ने.इ.प.द.नं.',
		organizationId: 'सुन्दरहरैचा द.नं',
		firmName: 'फर्मको नाम',
		firmStamp: 'फर्मको छाप',
		paragraph2:
			'निर्माण कार्य नेपाल राष्ट्रिय भवन संहिता २०६० भवन तथा योजना मापदण्ड अनुसार भएको छ। विपरित वा अन्यथा भएमा कानुन बमोजिम सहुँला बुझाउँला भनी सहीछाप गर्ने ।',
		nirmanVyabasayi: 'निर्माण व्यवसायी को नाम',
		address: 'ठेगाना',
		mobile: 'मोबाईल नं.',
	},
};
