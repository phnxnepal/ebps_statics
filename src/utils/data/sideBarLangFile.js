/**
 * @deprecated no longer in use
 */
export const mainSideBar_menu = {
	menu_item1: 'ईजाजतपत्र सम्बन्धि निवेदन',
	menu_item2: 'तला विवरण',
	menu_itemHasChild: {
		menu_item: 'अनुसुची (क)',
		hasChild: {
			child_item1: 'डिजाईन स्वीकृतिको दरखास्त',
		},
	},
	menu_itemHasChild_2: {
		menu_item1: 'अनुसुची (ख) Class B',
		hasChild1: {
			child_item1: 'आर्किटेक्चरल डिजाईन सम्बन्धि',
			child_item2: 'स्टक्चरल डिजाईन सम्बन्धि',
		},
		menu_item2: 'अनुसुची (ख) Class C',
		hasChild2: {
			child_item1: 'आर्किटेक्चरल डिजाईन सम्बन्धि',
			child_item2: 'स्टक्चरल डिजाईन सम्बन्धि',
		},
	},
	menu_item3: 'अनुसुची (ग)',
	menu_item4: 'अनुसुची (घ)',
	menu_item5: 'नक्सा बनाउने प्राविधिकले उल्लेख गर्नुपर्ने विवरण',
	menu_item6: 'नक्सा जाँच पछि पेश गरिएको प्रतिवेदन',
	menu_item7: 'राजश्व विवरण',
	menu_item7_1: 'राजस्व विभागले राजश्‍वको भौचर राख्ने',
	menu_item8: 'संधियारको नाममा जारी भएको १५ दिने सूचना बारे',
	menu_item9: 'सूचना बुझाएको भर्पाई तथा टाँस मुचुल्का बारे',
	menu_item10: 'सर्जमिन गरी पेश गर्ने सम्बन्धमा',
	menu_item11: 'घर नक्सा सरजमिन मुचुल्का',
	menu_item12: 'प्राविधिक प्रतिवेदन पेश सम्बन्धमा',
	menu_item13: 'प्लिन्थ लेभलसम्म निर्माण निमित्त इजाजत पत्र',
	menu_item14: 'प्रथम चरणको अस्थायी निर्माण कार्यको इजाजत पत्र',
	menu_item15: 'सुपरस्ट्रक्चरको निर्माण कार्यकोलागि इजाजत बारे',
	menu_item16: 'प्लिन्थ लेभलसम्मको निर्माण कार्य सम्पन्नबारे घरधनीको तर्फबाट राखिएको सुपरिवेक्षकको प्रतिवेदन',
	menu_item17: 'प्लिन्थ लेभलसम्मको निर्माण कार्य सम्पन्नबारे स्थलगत निरीक्षण गर्ने न.पा. प्राविधिकको राय',
	menu_item18: 'सुपरस्ट्रक्चर निर्माणका निमित्त इजाजत प्रदान गर्ने टिप्पणी आदेश',
	menu_item19: 'दोस्रो चरणको इजाजत पत्र',
	menu_item20: 'निर्माण कार्य सम्पन्न प्रमाण पत्र बारे',
	menu_item21: 'सुपरस्ट्रक्चरको निर्माण कार्य सम्पन्नबारे घरधनीको तर्फबाट राखिएको सुपरिवेक्षकका प्रतिवेदन',
	menu_item22: 'भवन निर्माण सम्पन्न प्रतिवेदन सम्बन्धमा',
	menu_item23: 'निर्माण कार्य सम्पन्न प्रमाणपत्र दिने टिप्पणी आदेश',
	menu_item24: 'निर्माण कार्य सम्पन्न प्रमाण पत्र',
	menu_item25: 'घर नक्सा नामसारी सम्बन्धमा',
	menu_item26: 'नामसारी टिप्पणी आदेश',
	menu_item27: 'नामसारी प्रमाण पत्र',
	menu_item28: 'मन्जुरीनामा',
	menu_item29: 'वारेसनामा',
	menu_item30: 'कवुलियतनामा',
	menu_item_forward_to_next: 'माथिको तह मा स्वीकृत को लागि पठाउनु',
	menu_item31: 'घर नक्शाको कवुलियतनामा',
	menu_item32: 'अमिन सरजमिन',
	menu_item33: 'इलेक्ट्रिकल डिजाईन',
	menu_item34: 'स्यानिटरी डिजाईन',
	menu_item35: 'संसोधन सुपरस्ट्रक्चर इजाजत सम्बन्धमा',
	menu_item38: 'संसोधन गरी निवेदन',
	menu_item39: 'संसोधन टिप्पणी र आदेश',
	menu_item44: 'संसोधन विवरण बिल भुक्तानी',
	menu_item36: 'घर/कम्पाउण्डवालको नाप',
	menu_item37: 'संसोधन/सुपरस्ट्रक्चर इजाजत प्रतिवेदन पेश गरेको बारे',
	menu_item40: 'संसोधन विवरण (पहिलो चरण)',
	menu_item41: 'संसोधनको टिप्पणी आदेश',
	menu_item42: 'सुपरस्ट्रक्चरको संसोधन विवरण',
	menu_item43: 'संसोधन सुपरस्ट्रक्चर टिप्पणी आदेश',
	menu_item45: 'म्याद थप गरी दिनु हुन',
	menu_item46: 'म्याद थप सम्बन्धमा',
	menu_item47: 'म्याद थप गरिएको सम्बन्धमा',
	menu_item48: 'नयाँ रेकर्ड पठाएको/विवरण पठाएको/नामसारी सम्बन्धमा',
	menu_item49: 'निर्माण कार्य सम्पन्न प्रमाण पत्र तथा धरौटी फिर्ता बारे',
	menu_item50: 'निर्माण कार्य सपन्नको प्रमाण पत्र सहित धरौटी फिर्ता दिने टिप्पणी आदेश',
	menu_item51: 'सम्पन्न  प्राविधिक प्रतिवेदन पेश गर्ने बारे',
	menu_item52: 'सुपरस्ट्रक्चर प्राविधिक प्रतिवेदन पेश गर्ने वारे',
	menu_item53: 'प्राविधिक प्रतिवेदन पेश गर्ने बारे',
	menu_item54: 'घर भवन नक्सा सम्बन्धमा',
	menu_item55: 'सम्पन्न बिल्ल वुक्तनी ',
	menu_item56: 'सम्पन्न बिल्ल वुक्तनी ',
	naya_record_pathayeko: 'नयाँ रेकर्ड पठाएको/विवरण पठाएको/नामसारी सम्बन्धमा',
	dharauti_phirta: 'निर्माण कार्य सम्पन्न प्रमाण पत्र तथा धरौटी फिर्ता बारे',
	dharauti_phirta_tippani: 'निर्माण कार्य सपन्नको प्रमाण पत्र सहित धरौटी फिर्ता दिने टिप्पणी आदेश',
	menu_archi_design_manjuri: 'आर्किटेक्चरल डिजाईन मन्जुरि पतर',
	menu_structure_design_manjuri: 'आर्किटेक्चरल डिजाईन मन्जुरि पतर',
	menu_napi_nirikshyan_pratibedan: 'नापी निरीक्षण प्रतिवेदन',
	menu_dosro_charan_kamalamai_prabidhik: 'दोश्रो चरणको कार्य सम्पन्नको न.पा. प्राविधिकको प्रतिवेदन',
	menu_pratham_charan_anumati_request: 'पर्थम चरणको काम गर्ने अनुमति पाउँ',
	menu_naksa_pass_certificate: 'नक्सा पास प्रमाण-पत्र',
	menu_suchana_tas_gari_muchulka_pathaidina: 'सुचना टाँस गरी मुचुल्का पठाई दिन',
	menu_itemDefault: '',
};
//@TODO FIX
