import { ConstructionType } from '../enums/constructionType';

export const saveButton = {
	uploadFileReminder: 'फाईल् हाल्न नबिर्सिनु होला',
	fileUploading: 'फाईलहरू अपलोड हुँदैछन्। कृपया पर्खनुहोस्',
	approveConfirm: 'फाईल् हेर्न नबिर्सिनुहोस्',
};

export const taskList = {
	taskListError: 'यो फाईल् तपाइँको कार्य सूचीमा छैन।',
	renewalReminder: {
		heading: 'नवीकरण अनुस्मारक',
		contentPrefix: 'तपाईंको खाताको म्याद',
		contentSuffix: 'दिनमा सकिन्छ। कृपया समय मै नवीकरण गर्नुहोला।',
	},
	headings: {
		nayaDarta: 'नयाँ दर्ता सूची',
		puranoDarta: 'पुरानो दर्ता सूची',
		tallaThap: 'तला थप सूची',
		namsari: 'नाम सारी सूची',
		noData: 'यस सूचीमा कुनै फाईलहरू छैनन्',
	},
};

export const forgotPassword = {
	title: 'के तपाई पासवर्ड रिसेट गर्न चाहानुहुन्छ ?',
	yes: 'चाहन्छु',
	no: 'चाहन्न',
};

export const approveReject = {
	approve: {
		title: 'फारम स्वीकृति गर्नुहोस्',
		content: 'के तपाई यो फारम स्वीकृत गर्न चाहानुहुन्छ ?',
		comment: 'टिप्पणी',
		close: 'रद्द गर्नुहोस्',
		save: 'स्वीकृत गर्नुहोस्',
		rayaHaru: 'रायहरु'
	},
	reject: {
		title: 'फारम अस्वीकृत गर्नुहोस्',
		content: 'के तपाइँ यो फारमलाई अस्वीकृत गर्न चाहानुहुन्छ ?',
		comment: 'टिप्पणी',
		remarks: 'कैफियत',
		close: 'रद्द गर्नुहोस्',
		save: 'अस्वीकृत गर्नुहोस्',
	},
	delete: {
		title: 'फाईल हटाउनु होस्',
		content: 'के तपाईं यो फाईल हटाउन चाहानुहुन्छ?',
		close: 'रद्द गर्नुहोस्',
		save: 'हटाउनु होस्',
	},
};

export const fileData = {
	fileUpload: 'फाईल अपलोड',
	selectForm: 'फारम छनौट गर्नुहोस्',
	fileList: 'फाईल सूची',
	noFilesUploaded: 'कुनै फाईल अपलोड गरिएको छैन',
	noFileCategory: 'यस फारमको लागि कुनै फाईलको प्रकार भेटिएन। कृपया प्रशासकलाई सम्पर्क गर्नुहोला।',
	noFileCategoryForm: 'यस फारमको लागि कुनै फाईलको प्रकार भेटिएन।',
	noFormWithFile: 'तपाईंले कुनै फाईलहरू अपलोड गर्नुपर्ने छेन।'
};

/**
 * @TODO move to option utils
 */
export const constructionTypeOptions = [
	{ value: '1', label: ConstructionType.NAYA_NIRMAN },
	{ value: '2', label: ConstructionType.PURANO_GHAR },
	{ value: '3', label: ConstructionType.TALLA_THAP },
];

export const constructionTypeSelectOptions = [
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
	{ value: '3', text: ConstructionType.TALLA_THAP },
];

export const approvalStatus = {
	user: 'प्रयोगकर्ता',
	status: 'स्थिति',
};

export const floorData = {
	talla: 'तला',
	block: 'बल्क',
	ma: 'मा',
};

export const optionDefaults = {
	allWards: 'सबै वडा',
};

export const addPermitPrompt = {
	confirmationMessage: 'के तपाइँ निश्चित आवश्यक फाईलहरू अपलोड नगरीकन पृष्ठ छोड्न चाहनुहुन्छ। आवश्यक फाइलहरू बिना भवन अनुमति रद्द हुन सकिन्च।',
	previousFileMissing: {
		title: 'फाइल अपलोड अनुस्मारक',
		content: 'अघिल्लो फारमको लागि फाइलहरू अझै अपलोड गरिएको छैन। कृपया अघिल्लो फारमको लागि फाइलहरू अपलोड गर्नुहोस्।'
	}
}
