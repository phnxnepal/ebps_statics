export const formData = {
	//header
	header: 'FORM TO BE FILLED UP BY DESIGNER',
	//table 1
	landOwner: 'Land Owner',
	houseOwner: 'House Owner',
	address: 'Address',
	location: 'Location',
	landArea: 'Land Area',
	lalpurja: 'As per Lal Purja',
	sitePlan: 'As per Site Plan',
	wardNo: 'Ward No.',
	plotNo: 'Plot No.',
	ropani: 'In Ropani',
	inSquareFeet: 'In Sq.Ft',

	//table 2
	purposeOfBuilding: 'Purpose of Building',
	residential: 'Residential',
	commerical: 'Commercial',
	institutional: 'Institutional',
	misc: 'Misc',
	natureOfConstruction: 'Nature Of Construction',
	row: 'Row',
	semiAttached: 'Semi-attached',
	deattached: 'Deattached',
	typesOfBuilding: 'Types of Building',
	new: 'New',
	additionOfStorey: 'Addition of Storey',
	extension: 'Extension',

	//table 3

	drawingSize: 'Drawing Size',
	drawingScale: 'Drawing Scale',
	a1: 'A1',
	a2: 'A2',
	sitePlanScale: 'Site plan scale',
	floorPlans: "Floor plan's section elevation",
	oneEqualToEight: '1" = 8\'0"',
	oneEqualToSixteen: '1" = 16\'0"',
	oneEqualToThirtyTwo: '1" = 32\'0"',
	oneEqualToFour: '1" = 4\'0"',

	//table 4
	designers: "Designer's",
	//use address of table 1
	phoneNumber: 'Phone No.',
	regNo: 'Reg.No.',
	class: 'Class',
	a: 'A',
	b: 'B',

	//table 5

	sn: 'SN',
	description: 'Description',
	byeLaws: 'Bye Laws',
	AccToDrawg: 'Acc. to drawg.',
	remarks: 'Remarks',
	zone: 'Zone',
	groundCoverage: 'Ground Coverage',
	heightOfBuilding: 'Height of building',
	floorAreaFAR: 'Floor Area (F.A.R.) @',
	nameOfRoad: 'Name of Road',
	rightOfWay: 'Right of way (ROW) + Set Back',
	highTensionLine: 'High Tension Line',
	riverBank: 'River Bank',
	numberOfStorey: 'No. of storey/Light plane',

	//table 6
	//serial number is used form table 5
	floorName: 'Floor Name',
	floorArea: 'Floor Area',
	totalFloorArea: 'Total Floor Area',
	existing: 'Existing',
	proposedNewConstruction: 'Proposed New Construction',
	otherBuilding: 'Other Building',
	purposedStoreyAdded: 'Purposed Storey Added',
	FARnonCountable: 'FAR Non Countable',
	FARcountable: 'FAR Countable',
	basementSemibasement: 'Basement/Semi-Basement',
	groundFloor: 'Ground Floor',
	firstFloor: 'First Floor',
	secondFloor: 'Second Floor',
	thirdFloor: 'Third Floor',
	fourthFloor: 'Fourth Flroor',
	fifthFloor: 'Fifth Floor',
	miscellaneous: 'Miscellaneous',
	perSqFt: 'per Sq.Ft.',
	tax: 'Tax @',
	deposit: 'Deposite @',
	totalAmount: 'Total Amount @',
	rs: 'Rs.',
	dataLine: 'All the above detalis have been filled up correctly in according to the prevailing Building by laws',
	designersSignature: "Designer' Signature",

	//table 7
	officeUse: 'For office use only',
	reportedBy: 'Reported By',
	//remarks used from table 5
	signature: 'Signature',
	documentCheck: 'Document Check',
	amin: 'Amin',
	batoField: 'बाटो फिल्ड अनुसार फीटमा:',
	batoNaksha: 'बाटो नक्शा अनुसार फीटमा:',
	juniorEngineer: 'Junior Engineer',
	approvedBy: 'Approved By Engineer',

	//table 8
	taxSection: 'Tax Section',
	taxReceiptNo: 'Tax from Receipt No.',
	depositNo: 'Deposite From Receipt No.',
	total: 'Total',
	//rs is used from table 6
};
