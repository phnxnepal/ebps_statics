export const rajaswoFormLang = {
  heading: 'नक्सापास उपशाखा राजस्व उपशाखाको प्रयोजनको लागि',
  // sub_heading:
  //   'नगरपालिकाको मापदन्द बमोजिम नक्सा तथा दर्खास्त फारम पेश भएको छ / छैन ?, मापदन्द बमोजिम नक्सा तथा दर्खास्त फारम पेश भएको भए आवशयेक् सङ्शोधन् विवरण........................',

  table_heading1: 'राजस्व  उपशाखाको प्रयोजनको लागि',
  rajaswaFirstPara: {
    content_line_1: 'बनिने भवन रहेको क्षेत्र :',
    //  आवासिय  क्षेत्र / व्यापारिक क्षेत्र / कृषि क्षेत्र / औद्योगिक क्षेत्र  / शैक्षिक क्षेत्र / संस्थागत क्षेत्र',
    content_line_2: 'निर्माण हुने घरको लागि स्वीकृत दिन सकिने जम्मा उचाई ',
    content_line_3: 'निर्माण हुने घरको उचाई ',
    content_line_4: 'निर्माण हुने घरको भूमिगत तलाको  लम्बाई: ',
    content_line_5: 'चौडाई ',
    content_line_6: 'क्षेत्रफल ',
    content_line_7: 'वर्गफिट ',
    content_line_8: 'निर्माण हुने घरको एक  तलाको  लम्बाई: ',
    content_line_9: 'निर्माण हुने घरको दोस्रो तलाको  लम्बाई: ',
    content_line_10: 'निर्माण हुने घरको तेस्रो तलाको  लम्बाई: ',
    content_line_11: 'निर्माण हुने घरको चौथो  तलाको  लम्बाई: ',
    content_line_12: 'निर्माण हुने घरको कम्पाउण्डवालको  लम्बाई: ',
    prefix: 'निर्माण हुने घरको ',
    length: 'लम्बाई: ',
    width: ' चौडाई ',
    area: ' क्षेत्रफल ',
    unit: ' वर्ग ',

  },
  rajaswo_table_1: {
    header: 'नामाकरण भएको सडक',
    table_head_1: 'सडकको नाम',
    table_head_2: 'मापदण्ड',
    table_head_3: 'हालको नाप',
    table_head_4: 'अधिकार क्षेत्र',
    table_head_5: 'सडकको रेखाबाट छाड्नु पर्ने दुरी',
    table_head_6: 'हाल छाडिएको दुरी',
    table_head_7: 'कैफियत'
  },
  rajaswo_table_2: {
    header: 'नामाकरण नभएको सडक',
    table_head_1: 'प्रस्तावित सडकको नाम',
    table_head_2: 'प्रस्तावित मापदण्ड',
    table_head_3: 'सडकको केन्द्र रेखाबाट छाड्नु पर्ने दुरी',
  },
  table_head: {
    thead_1: 'विवरण',
    // thead_2: "प्रस्तावित निर्माणको क्षेत्रफल (वर्ग फिट / वर्ग मिटर)",
    thead_2: 'प्रस्तावित निर्माणको क्षेत्रफल',
    thead_3: 'नक्सा दस्तुर रु',
    thead_3_hasChild: {
      theadChild_1: 'दर',
      theadChild_2: 'रकम'
    },
    // thead_4: "प्रस्तावित निर्माणको क्षेत्रफल (वर्ग फिट / वर्ग मिटर)",
    thead_4: 'प्रस्तावित निर्माणको क्षेत्रफल',
    thead_5: 'नक्सा दस्तुर रु',
    thead_5_hasChild: {
      theadChild_1: 'दर',
      theadChild_2: 'रकम'
    },
    thead_6: 'धरौटी रु.',
    thead_6_hasChild: {
      theadChild_1: 'दर',
      theadChild_2: 'रकम'
    },
    thead_type: {
      dhalan: 'ढलान',
      other: 'जस्ता'
    }
  },
  table_jasta: 'जस्ता छ / छैन',
  table_body: {
    tbody_1: 'भुमिगत तला',
    tbody_2: 'भुइँ तला',
    tbody_3: 'पहिलो तला'
    // tbody_4: 'दोस्रो तला',
    // tbody_5: 'तेस्रो तला',
    // tbody_6: 'चौथो तला',
    // tbody_7: 'पाचौ तला',
    // tbody_8: 'छैठौं तला',
    // tbody_9: 'सातौ तला'
  },
  table_total: 'जम्मा रकम रु.',
  table_foot: {
    Child_3: 'अमिन शुल्क रु.',
    Child_4: 'अन्य शुल्क रु.',
    child_wildcard:"सेवा शुल्क रु.",
    jaribanaDastur: 'जरिवाना दस्तुर रु.',
    sarsaphaiDastur: 'सरसफाई दस्तुर रु.',
    batabaranDastur: 'वतावरण दस्तुर रु.',
    Child_1: 'फारम दस्तुर रु.',
    Child_2: 'निवेदन दर्ता दस्तुर रु.',
    discount_row: 'छुट रकम',
    discount_percent: 'छुट प्रतिशत (%)',
    discount_amount: 'छुट रकम रु.',
    totalAmt: 'कुल जम्मा रु.',
    totalAmtWithDharauti: 'जम्मा रु.',
    dharautiAmt: 'जम्मा धरौटी रु.',
    Child_6: 'राजस्व उपशाखामा बुझाउने'
  },
  formSecond: {
    heading: 'राजस्व उपशाखाको प्रयोजनको लागि',
    content_lin1: 'निवेदनको नक्सा पास दस्तुर वापत रु.',
    content_lin2: 'अक्षरेपी',
    amountInWordsTotal: 'अक्षरेपी जम्मा',
    content_lin3: 'बाट प्राप्त भयो | मिति​',
    content_lin4: 'रसिद न.',
    content_lin5: 'रकम बुझने'
  },
  formThird: {
    heading: 'नक्सा पास / कानुन उपशाखाको प्रयोजनको लागि',
    content_lin1: 'आवश्यक कागजातहरु पेश भएको छ / छैन |​',
    content_lin2: 'आवश्यक कागजातहरु नपुग भएको भए सो को विवरण'
  },
  aminTitle: 'अमिन को राय​',
  sangsodhanTitle: 'अन्य भुल सुधार संशोधनको विवरण',
  mulDarta: {
    heading: 'मुल दर्ताको तोक आदेश​',
    dartaNo: 'मुल दर्ता न.',
    sakhaNo: 'उप / शाखा दर्ता न.',
    date: 'मिति'
  },
  floorMapping: {
    floor0: { floor: 0, value: 'भुमिगत तला' },
    floor1: { floor: 1, value: 'भुइँ तला' },
    floor2: { floor: 2, value: 'पहिलो तला' },
    floor3: { floor: 3, value: 'दोस्रो तला' },
    floor4: { floor: 4, value: 'तेस्रो तला' },
    floor5: { floor: 5, value: 'चौथो तला' },
    floor6: { floor: 6, value: 'पाँचौं तला' },
    floor7: { floor: 7, value: 'छैठौं तला' },
    floor8: { floor: 8, value: 'सातौं तला' },
    floor9: { floor: 9, value: 'आठौं तला' },
    floor10: { floor: 10, value: 'नवौं तला' },
    otherFloors: {
      sauchalaya: { floor: 11, value: 'शौचालय' },
      compoundWall: { floor: 12, value: 'कम्पाउण्डवाल' }
    }
  },
  floorMappingFlat: {
    floor0: { floor: 0, value: 'भुमिगत तला' },
    floor1: { floor: 1, value: 'भुइँ तला' },
    floor2: { floor: 2, value: 'पहिलो तला' },
    floor3: { floor: 3, value: 'दोस्रो तला' },
    floor4: { floor: 4, value: 'तेस्रो तला' },
    floor5: { floor: 5, value: 'चौथो तला' },
    floor6: { floor: 6, value: 'पाँचौं तला' },
    floor7: { floor: 7, value: 'छैठौं तला' },
    floor8: { floor: 8, value: 'सातौं तला' },
    floor9: { floor: 9, value: 'आठौं तला' },
    floor10: { floor: 10, value: 'नवौं तला' },
    // otherFloors: {
    sauchalaya: { floor: 11, value: 'शौचालय' },
    compoundWall: { floor: 12, value: 'कम्पाउण्डवाल' }
    // }
  },
  rajaswoVoucer: {
    formTitle: 'राजस्व विभागले राजश्‍वको भौचर राख्ने',
    voucherTitle: 'राजस्वको भौचर'
  },

};
