export const longFormDate = {
	talmelMiti: 'काम तामेल मिति',
	ad: 'इति सम्बत्',
	date: 'मिति',
	year: 'साल',
	month: 'महिना रोज',
	end: 'शुभम् ।',
	monthOnly: 'महिना',
	gate: 'गते',
	roj: 'रोज',
	suvam: 'शुभम्',
	endRojSuvam: 'रोज शुभम्',
};

export const footerSignature = {
	ser: 'सब इन्जिनियर ',
	er: 'इन्जिनियर',
	structureEr: "स्ट्रक्चरल ईन्जिनियर",
	chief: 'प्रमुख प्रशासकिय अधिकृत',
	pramanPatraPradhanGarne: 'प्रमाण-पत्र प्रदान गर्ने',
	sthalgatNirikshyan: 'स्थलगत निरिक्षण गर्ने',
	fieldNirikshan: 'फ़िल्ड निरीक्षण गरि पेश गर्ने',
	nirikshanGariPesh: 'निरीक्षण गरि पेश गर्ने',
	sipharis: 'सिफारिस गर्ने',
	swikrit: 'स्वीकृत गर्ने',
	naksaPassSakha: 'नक्सा पास शाखा',
	bhawanNaksaPass: 'भवन नक्सा पास',
	upasakha: 'उपशाखा',
	janchGarne: 'जाँच गर्ने',
	wadaAdhaksya: 'वडा अध्यक्ष',
	wadaSachib: 'वडा सचिव',
	naksaUpasakha: 'नक्सा पास उपशाखा',
};

export const footerInputSignature = {
	pes: 'पेश गर्ने :-',
	ruju: 'रुजु गर्ने :-',
	swikrit: 'स्वीकृत गर्ने :-',
	sipharis: 'सिफारिस गर्ने :-',
	tayar: 'तयार गर्ने :-',
	talmelGarne: 'काम तामेल गर्ने',
	talmelGarneKoDastakhat: 'काम तामेल गर्नेको दस्तखत',
	sadar: 'सदर गर्ने :-',
	sahi: 'सही :',
	dashtakhat: 'दस्तखत',
	name: 'नाम :',
	pad: 'पद :',
	date: 'मिति :',
	paramarsa: 'परामर्शदाता:',
};

export const fingerPrintData = {
	left: 'बायाँ',
	right: 'दायाँ',
	lyapche: 'ल्यापचे',
	signature: 'दस्तखत',
};

export const options = {
	defaultConstructionType: 'सबै किसिम',
	applicationStatus: {
		completed: 'पूर्ण भएको ',
		pending: 'चलिरहेको',
		default: 'पूर्ण अवस्था',
	},
};

export const letterSalutation = {
	shreemanChief: 'श्रीमान् कार्यकारी अधिकृत ज्यु,',
	nagarKaryalaya: 'नगर कार्यापालिकाको कार्यालय',
	karyalaya: 'कार्यालय।',
	shreemanEngineer: 'श्रीमान् इन्जिनियर ज्यू,',
	prashasakhiya: "श्रीमान् प्रमुख प्रशासकीय अधिकृतज्यू,",
};

export const designerSansodhan = {
	sansodhanBibaranPahilo: {
		topic: 'संसोधन विवरण (पहिलो चरण)',
	},
	sansodhanBibaranSuperStructure: {
		topic: 'सुपरस्ट्रक्चरको संसोधन विवरण',
	},
};

export const genericTippaniAdes = {
	title: 'टिप्पणी/आदेश',
	sansodhanTippani: {
		topic: 'संसोधन टिप्पणी र आदेश ',
	},
	sansodhanSuperStructureTippani: {
		topic: 'संसोधन सुपरस्ट्रक्चर टिप्पणी आदेश',
	},
	sansodhanGariNibedan: {
		topic: 'संसोधन गरी निवेदन',
	}
};

export const permitInfo = {
	sabik: 'साविक',
	haal: 'हाल',
	kitaa: 'कि.नं.',
	area: 'क्षेत्रफल',
	wardNo: 'वडा नं.',
	sadak: 'सडक',
	basne: 'बस्ने',
	uparoktaSambandha: 'उपरोक्त सम्बन्धमा,',
};

export const yesNoData = {
	nabhayeko: 'नभएको विवरण खलाउने',
};

export const certificateSubHeading = {
	miti: 'मिति',
};
