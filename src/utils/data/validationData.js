export const commonMessages = {
	required: 'यो भर्नु आवश्यक छ',
};

export const numberMessages = {
	number: 'संख्या हुनुपर्दछ',
	positive: 'सकरात्मक संख्या हुनुपर्दछ',
	max: 'भन्दा बढि संख्या हुनु हुँदैन',
	min: 'भन्दा कम संख्या हुनु हुँदैन',
	moreThan: 'छोड्नुपर्ने न्युनतम दुरी भन्दा कम हुनु हुँदैन',
};

export const unitMessages = {
	required: 'कृपया एकाइ छान्नुहोस',
};

export const numberValueMessages = {
	more: 'संख्या भन्दा ठूलो हुनुपर्दछ',
	lessOrEqual: 'संख्या भन्दा कम वा बराबर हुनुपर्छ',
};

export const dateMessages = {
	incorrect: 'मितिको ढाँचा गलत भयो',
	incorrectRange: 'मितिको ढाँचा गलत भयो। कृपया बिक्रम सम्बतमा मिति हाल्नुहोस्।',
};

export const emailMessage = {
	incorrect: 'इमेल गलत भयो',
};

export const fileMessages = {
	required: 'कृपया फाईल छान्नुहोस।',
	photoError: 'कृपया फोटो मात्र छान्नुहोस।',
	fileSize: 'फाईल धेरै ठूलो भयो।',
};

export const classCplinthArea = {
	max: '1000 sq.ft. भन्दा बढि हुनु हुँदैन',
};

export const classCStorey = {
	max: '३ तला भन्दा बढि हुनु हुँदैन',
};

export const classCBuildingHeight = {
	max: '11m भन्दा बढि हुनु हुँदैन',
};

export const classCBeamSpan = {
	max: '4.5 m भन्दा बढि हुनु हुँदैन',
};

export const classCPanelArea = {
	max: '13.5 sq.m भन्दा बढि हुनु हुँदैन',
};

export const classCOverallDimension = {
	max: 'भवनको लम्बाइ भवनको चौडाई को ३ गुणा भन्दा बढी हुनु हुँदैन',
};

export const classCRedundancy = {
	min: 'न्यूनतम २ वटा हुनुपर्दछ',
};

export const classCCantilever = {
	max: '1m भन्दा बढि कुनै क्यान्टिल्भर प्रोजेक्शन हुनु हुदैन',
};

export const classCGradeofConcrete = {
	min: 'कंक्रीटको न्यूनतम ग्रेड M20 (20 MPa) हुनुपर्छ',
};

export const classCTraverseReinfo = {
	min: 'न्यूनतम व्यास 8 mm हुनुपर्छ',
};

export const classCJointReinfo = {
	max: '8db वा त्यो भन्दा कम हुनुपर्छ',
};

export const blockError = {
	blockExists: 'यो ब्लक पहिले नै अवस्थित छ',
};

export const surroundingErrors = {
	kittaNoRequired: 'यो दिशा तर्फ जानकारी भएकाले यो कित्ता नं. भर्नु आवश्यक छ',
};

export const passwordErrors = {
	passwordStrength: 'पासवर्ड मा',
	passwordMustMatch: 'दुबै पासवर्डहरू समान हुनुपर्दछ',
};

export const memberErrors = {
	atLeastFour: 'कम्तिमा चार सदस्य हुनुपर्छ',
};

export const landDetailsErrors = {
	atLeastOne: 'कम्तिमा १ विबरण हुनुपर्छ',
};

export const genericErrors = {
	technicalError: 'केही प्राविधिक कठिनाइहरू छन्। कृपया पुर्न प्रयास गर्नुहोस',
};

export const floorErrors = {
	atLeastOne: 'कम्तिमा १ तला विबरण हुनुपर्छ'
}
