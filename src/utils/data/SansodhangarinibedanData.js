export const SansodhanGariNibedanStructure = {
    structureDesign: {
        heading: {
            head1: ' मेची नगरपालिका ',
            head2: ' नगर कार्यपालिकाको कार्यालय ',
            subhead1: ' इँटाभट्टा, झापा  ',
            subhead2: '१ न. पदेश नेपाल',
        },
        faxno: {
            faxname: 'फ्याक्स :',
            faxn1: '०२३-५६२२१३',
            faxn2: '०२३-५६२२१२',
            faxn3: '०२३-५६२४३७',
            faxn4: '०२३-५६२२३३',
            faxn5: '०२३-५६२०७७',
        },
        tip: 'टिप्पणी / आदेश',
        topic: ' संसोधन  गरी निवेदन ',
    }
}