import { ConstructionType } from '../enums/constructionType';

export const anushuchiKha = {
	architectureDesign: {
		heading: 'अनुसूचि “ख”',
		subheading1: 'प्राविधिक विवरण फारम',
		subheading2: '(क) आर्किटेक्चरल डिजाईन सम्बन्धि',
		subheading3: '(सम्बन्धित प्राविधिक वा परामर्शदाताबाट भराउनुपर्ने)',
		subheading4: 'Checklist For MRT Buildings (Class "C")',
		subheading5: 'NB Code 206:2003-Architectural Design Requirements.',
		note: '(In case of many buildings, fill up the form for main building only)',
		buildingType: 'Type of building',
		radiobutton_1: ['Yes', 'No'],
	},
	architectureDesignRequirements: {
		heading: 'अनुसूचि “ख”',
		subheading1: 'प्राविधिक विवरण फारम',
		subheading2: '(क) आर्किटेक्चरल डिजाईन सम्बन्धि',
		subheading3: '(सम्बन्धित प्राविधिक वा परामर्शदाताबाट भराउनुपर्ने)',
		subheading4: 'Professionally Engineered Buildings (Class "B")',
		subheading5: 'NB Code 206:2003-Architectural Design Requirements.',
		note: '(In case of many buildings, fill up the form for main building only)',
		buildingType: 'Type of building',
		radiobutton_1: ['Inside', 'Outside'],
		radiobutton_2: ['Yes', 'No'],
		radiobutton_3: ['Yes', 'No'],
	},
	structureDesign: {
		heading1: 'प्राविधिक विवरण फाराम',
		heading2: '(ख) स्ट्रक्चरल डिजाईन सम्बन्धि',
		heading3: '(सम्बन्धित प्राविधिक वा परामर्शदाताबाट भराउनु पर्ने)',
		subheading1: 'Checklist for MRT Buliding (Class"C")',
		subheading2: 'NBC 000:1994 to NBC 114:1994',
		subheading3: '(Structure/ Design Description)',
		note1: '(In case of many units, fill up the form for main unit only)',
		note2: 'Information sheet to be filled by the designer',
		table1: {
			row1: 'Name of client',
			row2: 'Address',
			row3: 'Contact No',
		},
		table2: {
			heading: '1. For RCC Building',
			sectionA: {
				heading: 'A. Configuration Related Checks',
				rows: {
					row1: {
						sn: '1',
						name: 'Plinth Area',
						description: 'The plinth area shell not exceed 1000sq.ft.',
						inputs: [{ suffix: 'sq.m.' }],
					},
					row2: {
						sn: '2',
						name: 'Number of Stories',
						description: 'The maximum height of the structure is 11m or 3 storeys, whichever is less.',
						inputs: [
							{ prefix: 'no. of storey', floorRelated: true },
							{ prefix: 'total height of building', floorRelated: true, suffix: 'm' },
						],
					},
					row3: {
						sn: '3',
						name: 'Bean span',
						description: 'The maximum possible span of beam is 4.5 m.',
						inputs: [{ suffix: 'max. beam span :' }],
					},
					row4: {
						sn: '4',
						name: 'Panel Area',
						description: ' The maximum panel area is 13.5 sq.m(145.31sq.ft).',
						inputs: [{ suffix: 'sq.m.' }],
					},
					row5: {
						sn: '5',
						name: 'Overall Dimension',
						description: 'The length of building shall be not greater than three times the breadth of the building.',
						inputs: [
							{ prefix: 'Length:', suffix: 'm' },
							{ prefix: 'Breadth:', suffix: 'm' },
						],
					},
					row6: {
						sn: '6',
						name: 'Redundancy',
						description: ' There shall be at-least 2 bays in each direction',
						inputs: [{ prefix: 'Min. No. of Bays' }],
					},
					row7: {
						sn: '7',
						name: 'Length of Wings',
						description:
							'The length of wings on the structure shall be restricted such that the length of wing shall be less than 0.15 times the smaller plan dimension.',
						inputs: [{ suffix: 'm' }],
					},
					row8: {
						sn: '8',
						name: 'Column Layout All columns shall be in grid lines',
						inputs: [{ suffix: '' }],
					},
					row9: {
						sn: '9',
						name: 'Beam Discontinuity',
						description: ' There shall not be discontinuity in beams in any frame system.',
						inputs: [{ suffix: '' }],
					},
					row10: {
						sn: '10',
						name:
							'Vertical Discontinuity All vertical elements in the lateral force resisting system shall be continuous from the root to the foundation.',
						inputs: [{ suffix: '' }],
					},
					row11: {
						sn: '11',
						name:
							'Cantilever There shall be no cantilever projection exceeding 1m. No walls except a parapet wall shall be built on a cantilevered slab',
						inputs: [{ suffix: 'm' }],
					},
					row12: {
						sn: '12',
						name: 'Short Column',
						description:
							'The reduced height of a column due to surrounding parapet, infill wall, etc. shall not be less than five times the dimension of the column in the direction of he parapet, infill wall, etc. or 50% of the nominal height of the typical columns in that storey',
						inputs: [{ suffix: '' }],
					},
					row13: {
						sn: '13',
						name:
							'Soft Storey The stiffness of the vertical lateral load resisting system in any storey shall not be less than 60% of the stiffness in an adjacent storey or less than 70% of the average stiffness of the stories above.',
						inputs: [{ suffix: '' }],
					},
				},
			},
			sectionB: {
				heading: 'B. Strength Related Checks',
				rows: {
					row14: {
						sn: '14',
						name: 'Grade of Concrete',
						description: 'The minimum grade of concrete shall be M20 (20 MPa)',
						inputs: [{ suffix: '' }],
					},
					row15: {
						sn: '15',
						name: 'Size of Column',
						description:
							'The minimum dimension of the member shall not be less than (a) 15 times the largest beam bar dimeter of the longitudinal reinforcement in the beam passing through or anchoring into the column joint and (b) 300 mm.',
						inputs: [{ suffix: '' }],
					},
					row16: {
						sn: '16',
						name: 'Minimum Number of Bars in Column',
						description: 'At least one intermediate bar shall be provided between corner bars along each column face.',
						inputs: [{ suffix: '' }],
					},
					row17: {
						sn: '17',
						name: 'Traverse Reinforcement in Column',
						description: 'The minimum diameter of the traverse reinforcement shall be 8 mm.',
						inputs: [{ suffix: '' }],
					},
					row18: {
						sn: '18',
						name: 'Column Bar Splices',
						description:
							'Lap splices shall be located only in the central half of the member length. Hoops shall be located over the entire splice length at spacing not exceeding 100 mm centre to centre. Not more than 50% of the bars shall preferably be spliced at one section.',
						inputs: [{ suffix: 'm from the beam column joint' }],
					},
					row19: {
						sn: '19',
						name:
							'Column Tie Spacing Frame columns shall have ties spaced at or less than d/4 throughout their length and at or less than 8 db at all the potential plastic hinge locations',
						inputs: [{ suffix: 'mm' }],
					},
					row20: {
						sn: '20',
						name: 'Size of Beam',
						description:
							' The width of the member shall not be less than 200 mm. The depth D of the member shall preferably be not more than 1/4 of the clear span. Width of beam shall not be more than 1/4 of the clear span. Width of beam shall not be more than the width of the column(measures on a plane perpendicular to the longitudinal axis of the beam) plus distance on each side of the column not exceeding half of the depth of the beam.',
						inputs: [{ suffix: '' }],
					},
					row21: {
						sn: '21',
						name: 'Beam Column Joint',
						description:
							'In an external joint, both the top and the bottom bars of the beam shall be provided with anchorage length beyond the inner face of the column, equal to the development length in tension plus 10 times the bar diameter minus the allowance for 90 degree bends(i.e 60 dia of longitudinal bar). In an internal joint, both face bars of the beam shall be taken continuously through the column.',
						inputs: [{ prefix: 'development length', suffix: 'm' }],
					},
					row22: {
						sn: '22',
						name: 'Beam Stirrup Spacing',
						description:
							' The spacing of the stirrups over a length of 2d at either end of a beam shall not exceed (a)d/4, or (b)8 times the diameter of the smallest longitudinal bar; however, it need not be less than 100 mm',
						inputs: [
							{ prefix: 'End of beam', suffix: 'mm' },
							{ prefix: 'Mid span:', suffix: 'mm' },
						],
					},
					row23: {
						sn: '23',
						name: 'Joint Reinforcement',
						description: ' Beam-column joints shall have ties spaced at or less than 8db.',
						inputs: [{ prefix: 'no. of reinforcement at joint:' }],
					},
					row24: {
						sn: '24',
						name: 'Stirrup and Tie Hooks',
						description:
							' The beans stirrups and columns ties shall be anchored into the member cores with hooks of 135° with 3 inch length',
						inputs: [{ suffix: '' }],
					},
					row25: {
						sn: '25',
						name: 'Strong Column Weak Beam',
						description:
							' The sum of the moments of resistance of the beams at each frame joint. (This clause only applicable for category A and category B building)',
						inputs: [{ suffix: '' }],
					},
					row26: {
						sn: '26',
						name: 'Wall connection',
						description: ' All infill walls shall have a positive connection to the frame to resist out-of-plane forces.',
						inputs: [
							{
								suffix: 'Sill band and Lintel band:, no. of, mm, bars with, mm, C-hook@, mm c-c',
							},
						],
					},
				},
			},
			sectionC: {
				heading: 'C. Site Related Checks',
				rows: {
					row27: {
						sn: '27',
						name: 'Terrain',
						description: 'The foundation shall be at uniform level',
						inputs: [],
					},
					row28: {
						sn: '28',
						name: 'Site Building shall not be constructed of the proposed site is',
						description:
							'water logged, a rock-falling area, a landslide prone area, a subsidence and/or fill area and a river bed swamp area',
						inputs: [],
					},
				},
			},
		},
		table3: {
			heading: '(2) FOR UNREINFORCED MASONARY BUILDING',
			sections: {
				section1: {
					sn: '1',
					rows: {
						row1: {
							name: 'Concrete Guide',
							inputs: [],
						},
					},
				},
				section2: {
					sn: '2',
					rows: {
						row1: {
							name: 'Brick Crushing Strength',
							inputs: [],
						},
					},
				},
				section3: {
					sn: '3',
					rows: {
						row1: {
							name: 'Mortar ratio for load bearing masonry',
							inputs: [],
						},
					},
				},
				section4: {
					sn: '4',
					rows: {
						row1: {
							heading: true,
							name: 'Floor',
							inputs: ['Wallheight', 'Wallthickness', 'MaximumLength'],
						},
						row2: {
							name: 'Ground floor',
							inputs: [],
							inputLength: 3,
						},
						row3: {
							name: 'First floor',
							inputs: [],
							inputLength: 3,
						},
						row4: {
							name: 'Second floor',
							inputs: [],
							inputLength: 3,
						},
					},
				},
				section5: {
					sn: '5',
					rows: {
						row1: {
							heading: true,
							name: 'Opening Details',
						},
						row2: {
							name: 'Least distance from inside corner',
						},
						row3: {
							isRadioInput: true,
							name: 'Does the total length of opening in any wall exceed 50% of its length',
							inputs: ['Yes', 'No'],
						},
						row4: {
							isRadioInput: true,
							name: 'Does the horizontal distance between any two opening less than 600 mm or 1/2 of height of shorter opening',
							inputs: ['Yes', 'No'],
						},
						row5: {
							isRadioInput: true,
							name: 'Does the Vertical distance between two opening less than 600 mm or 1/2 of width of smaller opening',
							inputs: ['Yes', 'No'],
						},
						row6: {
							isRadioInput: true,
							name: 'If any of above mentioned cases do not comply, do you have provisions for strengthening around opening?',
							inputs: ['Yes', 'No'],
						},
					},
				},
				section6: {
					sn: '6',
					rows: {
						row1: {
							isRadioInput: true,
							name: 'Bands provided:',
							inputs: ['Plinth level', 'Lintel level', 'Roof level', 'Gable band'],
						},
					},
				},
				section7: {
					sn: '7',
					rows: {
						row1: {
							heading: true,
							name: 'Vertical steel reinforcement diameters at corner/tee joints:',
						},
						row2: {
							name: 'Ground Floor:',
						},
						row3: {
							name: 'First Floor:',
						},
						row4: {
							name: 'Second Floor:',
						},
					},
				},
				section8: {
					sn: '8',
					rows: {
						row1: {
							name: 'C/C distance of corner/tee strengthening Horizontal dowel bars',
						},
					},
				},
			},
		},
		table4: {
			heading: 'Affidavit',
			subheading:
				'I/We hereby certify that the proposed design of building and its various component Comply all the requirement of prevailing National Building Code of Nepal',
			form: [
				{ display: 'Name: ', name: 'userName' },
				{ display: 'Post: ', name: 'Designation' },
				{ display: 'Name of consulting form: ', name: 'orgName' },
				{ display: 'Address: ', name: 'orgAddress' },
				{ display: 'Date: ', name: 'affDate' },
			],
			// input1: 'Name:',
			// input2: 'Post:',
			// input3: 'Name of consulting form:',
			// input4: 'Address:',
			// input5: 'Date:'
		},
	},
};

export const anushuchi_ka = {
	designApproval: {
		ka_heading: 'अनुसूचि “क”',
		ka_subheading1: 'भवन/घरनक्सा मापदण्ड तथा कार्यविधि २०६९ अनुसार नक्सा/डिजाईन',
		ka_subheading2: 'स्वीकृतिको लागि दरखास्त',
		// ------------------
		ka_municipal: 'श्री ',
		ka_municipalAddr: 'नेपाल',
		// -------------------
		ka_subject: 'विषय : भवन/घरनक्सा मापदण्ड तथा कार्यविधि अनुसार घर निर्माण/नक्सा/डिजाईन पेश गरेको बारे ।',
		sundarHaraicha_subject: 'विषय : भवन निर्माण संहिता २०६० अनुसार नक्सा/डिजाईन पेश गरेको बारे ।',
		//   -------------------
		ka_data_1: ' साविक ',
		ka_data_2: ' हाल ',
		ka_data_3: 'वडा नं ',
		sadak: 'सडक',
		ka_data_4: ' मा अवस्थित जग्गाको कित्ता नं ',
		ka_data_5: ' को क्षेत्रफल ',
		ka_data_6: ' मा ',
		ka_maincontent:
			'गर्न प्रस्ताव गरिएको संरचना भूकम्प सुरक्षात्मक बनाउन आवश्यक नक्सा, डिजाईन र अन्य आवश्यक कागजात सहित यो निवेदन पेश गरेको छु । प्राविधिकले डिजाईन गरे अनुरुप निर्माण गर्न सहमत भएको व्यहोरा समेत जानकारीको लागि अनुरोध गर्दछु । यो डिजाईन तथा निर्माणबाट भूकम्पीय वा साधारण सुरक्षाको कमिले हुन सक्ने सम्पूर्ण जोखिम प्रति जिम्मेवार छु । यस न.पा बाट समय समयमा दिईने निर्देशन पालना गर्नेछु तथा प्राविधिक शाखामा आवश्यक परेको बेला उपस्थित हुनेछु ।',
		// --------------------
		ownerName: 'घरधनीकोे नाम : ',
		ownerAddress: 'ठेगाना : ',
		ownerPhone: 'फोन नं. : ',
		ownerSignature: 'सही : ',
		ka_date: 'मिति : ',
		// ----------------
		ka_footer1:
			'“राष्ट्रिय भवन संहिता कार्यान्वयन–कार्यविधि, २०६०” अनुसार डिजाईन प्रयोजनको निम्ति प्रयोग गरिएको भवन संहिताको किसिम कुन हो सो मा चिन्ह लगाउनुहोस् ।',
		areaLimitHeader: 'भवनको क्षेत्रफल बारे सुचना',
		cancelText: 'रद्द गर्नुहोस्',
		goForward: 'अघि बढ्नुहोस्',
		areaLimitMessage: 'तपाईंको भवनको भुईं तलाको क्षेत्रफल',
		areaLimitMessage2: 'भन्दा बढि छ। के तपाइँ निश्चय हुनुहुन्छ कि भवनको वर्ग B नराखे हुन्छ?',
	},
	kageyshowriData: {
		wadaNo: "वडा नं",
		data_1 : " मा अवस्थित कित्ता नं ",
		chhetrafal: "क्षेत्रफल",
		data_2 : "व.मि. मा नयाँ घर/तल्ला थप/टहरा/पर्खाल निर्माण गर्न प्रस्ताव गरिएको संचरना भूकम्प सुरक्षात्मक बनाउन आवश्यक नक्शा, डिजाईन र अन्य आवश्यकता कागजात सहित यो निवेदन पेश गरेको छु/छौं । प्राविधिकले डिजाईन गरे अनुरुप निर्माण गर्न सहमत भएको व्यहोरा समेत जानकारीको लागि अनुरोध गर्दछु/गर्दछौं । यो डिजाईन तथा निर्माणबाट भूकम्पीय वा साधारण सुरक्षाको कमीले हुन सक्ने सम्पूर्ण जोखिम प्रति म/हामी घरधनी जिम्मेवार छु/छौं । यस नगरपालिकाको कार्यालयबाट समय समयमा दिइने निर्देशन पूर्ण रुपमा पालना गर्नेछु/गर्नेछौं तथा शहरी विकास शाखामा आवश्यक परेको बेला उपस्थित हुनेछु/हुनेछौं ।",
		
	}
};

export const anushuchi_ga = {
	anushuchiGaView: {
		ga_heading: 'अनुसूचि “ग“',
		// ------------------
		ga_head: 'श्री प्रमुख प्रशासकिय अधिकृत ज्यु',
		// -------------------
		ga_title: 'राष्ट्रिय भवन संहिता अनुसार भवन र सरंचना डिजाईन गरिएको सम्बन्धमा',
		ga_subtitle: '(साविक गा.वि.स कालमा निर्माण सुरुभएको र सम्पन्न भएका लाई लागु नहुने)',
		// ------------------------------
		ga_data_1: ' यो प्रमाणित ',
		ga_data_1_1: ' कि ',
		ga_data_2: 'वडा नं ',
		ga_data_3: ' सडक ',
		ga_data_4: ' मा अवस्थित जग्गाको कित्ता नं ',
		ga_data_5: ' क्षेत्रफल ',
		ga_data_6: ' मा घरधनी श्री ',
		ga_data_7: ' द्वारा निर्माण गर्न प्रस्ताव गरीएको भवन संरचना ',
		ga_maindata1: ' वर्गमा पर्ने भएकोले यसको स्ट्रक्चरल डिजाईन',
		ga_maindata1_1: ' गरेको ',
		ga_maindata2:
			'यसमा घरनक्सा कम्पाउण्ड कार्यान्वयन तथा कार्याविधि २०६९ नियमद्वारा प्रतिपादित समस्त नियमहरू पालना गर्दै आवश्यक भुकम्प सुरक्षात्मक डिजाईन तथा प्रविधि अपनाएको ',
		// ------------------------------
		ga_designerName: 'डिजाईनरको नाम : ',
		ga_designerDesignation: 'डिजाईनेशन (योग्यता) : ',
		ga_engineeringCouncil: 'नेपाल ईन्जिनीयरीङ्ग परिषद दर्ता नं. : ',
		ga_designerAdd: 'ठेगाना : ',
		ga_designerPh: 'फोन नं. : ',
		ga_designerMobile: 'फोन नं./मोबाइल नं : ',
		ga_municipalRegNo: 'न.पा. रजिष्ट्रेशन नं. : ',
		ga_designerSignature: 'सही : ',
		ga_date: 'मिति : ',
		ga_consultancyStamp: 'कन्सल्टेन्सीको छाप : ',
		ma: {
			first: 'गर्दैछु ',
			second: ' हो| ',
			third: 'छु |',
			fourth: ' मैले',
			fifth: 'गरेको',
		},
		hami: {
			first: 'गर्दछौ ',
			second: ' हौ | ',
			third: ' छौ |',
			fourth: ' हामीले ',
			fifth: 'गरेका',
		},
	},
};

export const anushuchi_gha = {
	anushuchiGhaView: {
		gha_heading: 'अनुसूचि “घ”',
		gha_subheading1: 'निर्माण सुपरीवेक्षण गर्ने बारे दरखास्त',
		gha_subheading2: '( C वर्गका लागिमात्र)',
		// ------------------
		gha_data_1: ' म घरधनी ',
		gha_data_2: ' यो प्रमाणित गर्दछु कि ',
		gha_data_3: ' साविक ',
		gha_data_4: ' हाल वडा नं ',
		gha_data_5: ' सडक ',
		gha_data_6: '  मा अवस्थित जग्गाको कित्ता नं  ',
		gha_data_7: ' क्षेत्रफल ',
		gha_data_8: ' मा निर्माण गर्न प्रस्ताव गरिएको भवन संरचना ',
		gha_data_9: ' वर्गमा पर्ने भएकोले सो निर्माण कार्य सुपरीवेक्षण प्राविधिक श्री ',
		gha_data_10: ' बाट हुनेछ । ',
		gha_data_11:
			' म यो पनि प्रमाणित गर्दछु कि निर्माण गरिने भवन संरचना स्वीकृत नक्सा, डिजाईन निर्माण सामाग्री आदिको आधारमा निर्माण गरिने छ । साथै उक्त भवन स्वीकृत नक्सा अनुसार हाललाई ',
		gha_data_12: ' निर्माण गरिने छ । ',
		gha_maindata: ' साथै निर्माण कार्यमा संलग्न सम्बन्धित ठेकेदार र मुख्य डकर्मी तपसील बमोजिम हुनेछन् ।',
		// ------------------
		gha_ownerName: 'घरधनीको नाम : ',
		gha_ownerAdd: 'ठेगाना : ',
		gha_ownerPh: 'फोन नं./मोवाइल नं. : ',
		gha_ownerSignature: 'सही : ',
		gha_odate: 'मिति : ',
		// -------------------
		gha_supervisorChoose: 'सम्बन्धित सुपरीवेक्षक छान्नुहोस',
		gha_supervisor: 'सम्बन्धित सुपरीवेक्षक',
		gha_supName: 'नाम : ',
		gha_supDesignation: 'डेजिग्नेशन (योग्यता) : ',
		gha_supEngineeringCouncil: 'नेपाल ईन्जिनीयरीङ्ग परिषद दर्ता नं. : ',
		gha_consultancyName: 'कन्सल्टिङ फर्म',
		gha_supAdd: 'ठेगाना : ',
		gha_supPh: 'फोन नं./मोबाइल नं. : ',
		gha_supMunicipalRegNo: 'न.पा. रजिष्ट्रेशन नं. : ',
		gha_supSignature: 'सही : ',
		gha_sdate: 'मिति : ',
		// ----------------------------------
		// gha_contractor: 'सम्बन्धित ठेकेदार (संलग्न भए)',
		gha_contractor: 'निर्मान व्यवसाय (संलग्न भए)',
		gha_contractorName: 'नाम : ',
		gha_contractorAdd: 'ठेगाना : ',
		gha_contractorPh: 'फोन नं./मोवाइल नं. : ',
		gha_contractorRegNo: 'दर्ता नं. : ',
		gha_contractorSignature: 'सही : ',
		// -------------------------------------
		gha_dakarmi: 'सम्बन्धित मुख्य डकर्मी',
		gha_dakarmiName: 'नाम : ',
		gha_dakarmiAdd: 'ठेगाना : ',
		gha_dakarmiPh: 'फोन नं./मोवाइल नं. : ',
		gha_dakarmiRegNo: 'दर्ता नं. : ',
		gha_dakarmiSignature: 'सही : ',
	},
};

export const buildingPermitApplicationForm = {
	permitApplicationFormView: {
		maLambai: 'मा लम्बाई : ',
		form_title: 'श्रीमान् प्रमुख प्रशासकिय अधिकृत ज्यू',
		form_naksa: 'नक्सा पास तथा भवन निर्माण',
		form_subject: 'विषयः नक्सा पास तथा भवन निर्माण कार्यको इजाजत सम्बन्धमा निवेदन ।',
		form_mahodaya: 'महोदय,',
		form_sakha_darta_no: 'शाखा दर्ता नं.: ',
		form_content:
			'देहायमा लेखिए बमोजिम निर्माण कार्य गर्ने भएकोले उक्त निर्माण कार्यको विवरण तपसिलमा खुलाई आफ्नो हक भोगको निस्साको नक्कल प्रमाण कित्ता नापी नक्साको सक्कल र घरको नक्सा लगायत आवश्यक कागजात सहित निवेदन पेश गरेको ',
		form_content_1: ' प्रस्तावित निर्माण कार्यको प्राविधिक विवरण पनि यसै निवेदनसाथ संलग्न गरेको ',
		form_content_2: ' निर्माण कार्यको इजाजत प्राप्त भएपछि नगरपालिकाद्वारा स्वीकृत मापदण्ड भित्र रही निर्माण कार्य ',
		form_content_3: ' यस दरखास्त फारममा लेखिएको व्यहोरा ठीक साँचो छ झुट्टा ठहरे कानून बमोजिम सहुँला बुझाउँला ।',
		form_tapasil: 'तपसिल',
		ma: {
			form_chu: 'छु ।',
			form_garnechu: 'गर्नेछु ।',
		},
		hami: {
			form_chu: 'छौं ।',
			form_garnechu: 'गर्नेछौं ।',
		},
		org: {
			form_chu: 'छौं ।',
			form_garnechu: 'गर्नेछौं ।',
		},
		org1: {
			form_chu: 'छौं ।',
			form_garnechu: 'गर्नेछौं ।',
		},
		form_step1: {
			heading: '१) जग्गाधनीको विवरण :',
			fieldName_1: '१.१ नाम, थर : ',
			fieldName_2: '१.२ ठेगाना : ',
			fieldName_3: '१.३ नागरिकता प्रमाणपत्र नं. : ',
			fieldName_4: '१.४ पिता/पति/पत्नीको नाम : ',
			spouseHead: '१.४ ',
			spouseTail: ' को नाम : ',
		},
		form_step1_birtnagar: {
			heading: '१) जग्गाधनीको विवरण :',
			fieldName_1: '१.१ नाम, थर : ',
			fieldName_2: '१.२ ठेगाना : ',
			naksawalaDetails: '२) नक्सावालाको विवरण :',
			naksawalaName: '२.१ नाम, थर : ',
			naksawalaAddress: '२.२ ठेगाना : ',
			fieldName_3: '२.३ नागरिकता प्रमाणपत्र नं. : ',
			citizenshipIssueDate: 'जारी मिति ',
			citizenshipIssueZone: 'जिल्ला',
			fieldName_4: '२.४ पिता/पति/पत्नीको नाम : ',
			spouseHead: '२.४ ',
			spouseTail: ' को नाम : ',
		},
		form_step2: {
			heading: '२) जग्गा रहेको ठाउँको विवरण : ',
			fieldName_1: '२.१ हाल ',
			fieldName_1_1: ' वडा नं. : ',
			fieldName_2: '२.२ साविक : ',
			fieldName_3: 'वडा : ',
			fieldName_3_1: '२.३ टोल: ',
			fieldName_4: '२.४ नजिकको मूल सडकको नाम तथा अन्य विवरण : ',
			fieldName_5: 'प्रस्तावित निर्माणसँग जोडिएको सडकको नाम तथा सडक कोड नं. : ',
			fieldName_6: '२.५ प्रस्तावित निर्माणसँग जोडिएको सडकको किसिम : ',
			checkBox_option: {
				option_1: 'पिच',
				option_2: 'ग्राभेल',
				option_3: 'मोटर जाने कच्ची',
				option_4: 'गोरेटो',
				option_5: 'बाटो नभएको',
				option_6: 'अन्य',
			},
		},
		form_step2_biratnagar: {
			heading: '३) जग्गा रहेको ठाउँको विवरण : ',
			fieldName_1: '३.१ हाल ',
			fieldName_1_1: ' वडा नं. : ',
			fieldName_2: '३.२ साविक : ',
			fieldName_3: 'वडा : ',
			fieldName_3_1: '३.३ टोल: ',
			fieldName_4: '३.४ नजिकको मूल सडकको नाम तथा अन्य विवरण : ',
			fieldName_5: 'प्रस्तावित निर्माणसँग जोडिएको सडकको नाम तथा सडक कोड नं. : ',
			fieldName_6: '३.५ प्रस्तावित निर्माणसँग जोडिएको सडकको किसिम : ',
			checkBox_option: {
				option_1: 'पिच',
				option_2: 'ग्राभेल',
				option_3: 'मोटर जाने कच्ची',
				option_4: 'गोरेटो',
				option_5: 'बाटो नभएको',
				option_6: 'अन्य',
			},
		},
		form_step3: {
			heading: '३) जग्गा रजिष्ट्रेसन पास : ',
			fieldName_1: 'मिति : ',
			fieldName_2: '३.१ प्रस्तावित जग्गाको कित्ता नं : ',
			landDetails: '३.१ प्रस्तावित जग्गाको: ',
			kittaNo: 'कित्ता नं :',
			fieldName_3: '३.२ जग्गाको स्वामित्व कसको नाममा छ ? ',
			fieldName_4: '३.३ जग्गाधनी प्रमाण पुर्जा अनुसारको क्षेत्रफल : ',
			fieldName_5: 'क्षेत्रफल : ',
			fieldName_6: 'पुर्जाको नं. : ',
			fieldName_7: 'जारी मिति : ',
		},
		form_step3_biratnagar: {
			heading: '४) जग्गा एवं चारकिल्लाको विवरण: ',
			fieldName_1: 'मिति : ',
			fieldName_2: '४.१ प्रस्तावित जग्गाको कित्ता नं : ',
			landDetails: '४.१ प्रस्तावित जग्गाको: ',
			kittaNo: 'कित्ता नं :',
			fieldName_3: '४.२ जग्गाको स्वामित्व कसको नाममा छ ? ',
			fieldName_4: '४.३ जग्गाधनी प्रमाण पुर्जा अनुसारको क्षेत्रफल : ',
			fieldName_5: 'क्षेत्रफल : ',
			fieldName_6: 'पुर्जाको नं. : ',
			fieldName_7: 'जारी मिति : ',
		},
		form_step4_birtanagar: {
			heading: '४.४ जग्गाको चारकिल्ला :',
			fieldName_East: '४.४.१ पूर्वमा लम्बाई : ',
			fieldName_West: '४.४.२ पश्चिममा लम्बाई : ',
			fieldName_North: '४.४.३ उत्तरमा लम्बाई : ',
			fieldName_South: '४.४.४ दक्षिणमा लम्बाई : ',
			directions: {
				fieldName_East: '४.४.१',
				fieldName_West: '४.४.२',
				fieldName_North: '४.४.३',
				fieldName_South: '४.४.४',
			},
			directionsLabel: {
				fieldName_East: '४.४.१ पूर्वमा लम्बाई : ',
				fieldName_West: '४.४.२ पश्चिममा लम्बाई : ',
				fieldName_North: '४.४.३ उत्तरमा लम्बाई : ',
				fieldName_South: '४.४.४ दक्षिणमा लम्बाई : ',
			},
			fieldName_poll_common1: 'आफ्नो जग्गा पछि कित्ता नं. : ',
			fieldName_poll_common2: 'सँधियारको नाम : ',
		},
		form_step4: {
			heading: '४) जग्गाको चारकिल्ला :',
			fieldName_East: '४.१ पूर्वमा लम्बाई : ',
			fieldName_West: '४.२ पश्चिममा लम्बाई : ',
			fieldName_North: '४.३ उत्तरमा लम्बाई : ',
			fieldName_South: '४.४ दक्षिणमा लम्बाई : ',
			directions: {
				fieldName_East: '४.१',
				fieldName_West: '४.२',
				fieldName_North: '४.३',
				fieldName_South: '४.४',
			},
			directionsLabel: {
				fieldName_East: '४.१ पूर्वमा लम्बाई : ',
				fieldName_West: '४.२ पश्चिममा लम्बाई : ',
				fieldName_North: '४.३ उत्तरमा लम्बाई : ',
				fieldName_South: '४.४ दक्षिणमा लम्बाई : ',
			},
			fieldName_poll_common1: 'आफ्नो जग्गा पछि कित्ता नं. : ',
			fieldName_poll_common2: 'सँधियारको नाम : ',
		},
		form_step5: {
			heading: '५) प्रस्तावित भवन निर्माणको विवरण : ',
			fieldName_1: '५.१ भवन/निर्माणको प्रयोजन : ',
			checkBox_option: {
				option_1: 'आवासीय',
				option_2: 'व्यापारिक',
				option_3: 'अन्य',
			},
			fieldName_2: '५.२ निर्माणको किसिम : ',
			isEarthquakeResistant: 'भूकम्प प्रतिरोधक',
			checkBox_option2: [
				{ value: '1', label: ConstructionType.NAYA_NIRMAN },
				{ value: '2', label: ConstructionType.PURANO_GHAR },
				{ value: '3', label: ConstructionType.TALLA_THAP },
			],
			fieldName_3: '५.३ पहिले नक्सा पास भएको भए सोको मिति : ',
			fieldName_4: '५.४ निर्माण हुने घरको मोहडा : ',
			checkBox_option3: {
				option_1: 'पुर्व',
				option_2: 'पश्चिम',
				option_3: 'उत्तर',
				option_4: 'दक्षिण',
			},
			fieldName_5: '५.५ भवनको बाहिरी भागको फिनिसिङ्ग : ',
			checkBox_option4: {
				option_1: 'सिमेन्ट प्लाष्टर',
				option_2: 'ईटा देखाउने',
				option_3: 'सिमेन्ट टिप्कार',
				option_4: 'बज्र टिप्कार',
				option_5: 'बज्र प्लाष्टर',
				option_6: 'अन्य',
			},
		},
		form_step6: {
			heading: '६) ढल निकासको व्यवस्था : ',
			fieldName_1: 'ढल भए कति टाढा पर्दछ (मिटर/फिट) ? ',
			checkBox_option: {
				option_1: 'सेप्टी टेङ्की बनाउने',
				option_2: 'सोकपिट बनाउने',
				option_3: 'ढलमा जोड्ने',
				option_4: 'ढलमा नजोड्ने',
			},
		},
		form_step7: {
			heading: '७) फोहोर फाल्ने व्यवस्था : ',
			checkBox_option: {
				option_1: 'न.पा.को कन्टेनरमा',
				option_2: 'आफ्नै जग्गामा',
				option_3: 'अन्य',
			},
		},
		form_step8: {
			heading: '८) खानेपानीको व्यवस्था : ',
			fieldName_1: '८.१ पाईप लाईन छ, छैन ? ',
			checkBox_option: {
				option_1: 'छ',
				option_2: 'छैन',
			},
			fieldName_2: '८.२ पाईप लाईन भए कति टाढा पर्दछ (मिटर/फिट) ? ',
			fieldName_3: '८.३ पाईप लाईनमा जडान गर्ने हो, होइन ? ',
			checkBox_option2: {
				option_1: 'हो',
				option_2: 'होइन',
			},
		},
		form_step9: {
			heading: '९) बिजुलीको व्यवस्था : ',
			fieldName_1: '९.१ भवन निर्माण गरिने जग्गा माथिबाट हाई टेन्सन लाईन गएको छ/छैन ? ',
			checkBox_option: {
				option_1: 'छ',
				option_2: 'छैन',
			},
			fieldName_2: '९.२ भवन निर्माण गरिने जग्गा माथिबाट लो टेन्सन लाईन गएको छ/छैन ? ',
			fieldName_3: '९.३ बिजुली जडान गरिने लठ्ठा कति टाढा पर्दछ (मिटर/फिट) ? ',
			floor_details: {
				table_heading: {
					heading1: 'घर/कम्पाउण्डबालाको',
					heading2: 'लम्बाई',
					heading3: 'चौडाई',
					heading4: 'उचाई',
					heading5: 'क्षेत्रफल',
				},
				table_headings: {
					heading1: 'घर/कम्पाउण्डबालाको',
					heading2: 'लम्बाई',
					heading3: 'चौडाई',
					heading4: 'उचाई',
					heading5: 'क्षेत्रफल',
					heading6: 'actions',
				},

				table_subheading: {
					subheading1: 'भूमिगत/अर्धभूमिगत तल्ला',
					subheading2: 'भुइँ तला',
					subheading3: 'पहिलो तल्ला',
					subheading4: 'दोस्रो तल्ला',
					subheading5: 'तेस्रो तल्ला',
					subheading6: 'चौथो तल्ला',
					subheading7: 'पाँचौं तल्ला',
					subheading8: 'छैठौं तल्ला',
					subheading9: 'सातौं तल्ला',
					subheading10: 'आठौं तल्ला',
					subheading11: 'नवौं तल्ला',
					subheading12: 'शौचालय',
					subheading13: 'कम्पाउण्डवाल',
				},
				nibedakPhoto: 'घरधनीको पासपोट साईज फोटो',
				commercialActions: 'तालिका थप्नुहोस्',
				addBlocks: 'ब्लक थप्नुहोस्',
				block: 'ब्लक',
				availableBlocks: 'उपलब्ध ब्लकहरू',
				pleaseAddBlock: 'कृपया ब्लक थप्नुहोस्',
				allFloorsAdded: 'सबै तलाहरु थपिसकेका छन् ।',
				floor: 'तला',
			},
		},
		form_tallaBibarab: 'तला विवरण',
		form_step10: {
			heading: '१०) नक्सा वाला को पारिवारिक सदस्य विवरण : ',
			member_details: {
				table_heading: {
					heading1: 'सि.न.',
					heading2: 'सदस्य को नाम',
					heading3: 'नाता',
				},
				table_subheading: {
					subheading1: '१',
					subheading2: '२',
					subheading3: '३',
					subheading4: '४',
					subheading5: '५',
					subheading6: '६',
				},
			},
		},
		petitioner_nibedak: 'निवेदक',
		petitioner_signature: 'सहि : ',
		petitioner_name: 'नाम : ',
		petitioner_age: 'वर्ष : ',
		petitioner_municipal: 'नगरपालिका : ',
		petitioner_vadaNo: 'वडा नं. : ',
		petitioner_road: 'सडक : ',
		petitioner_phone: 'टेलिफोन नं. : ',
		petitioner_date: 'मिति : ',
		petitioner_additional: 'निवेदकको तर्फबाट अन्य केही खुलाउनु पर्ने भए तल उल्लेख गर्ने',
	},
};

export const mapTechnical = {
	mapTechnicalDescription: {
		mapTech_subject: 'नक्सा बनाउने प्राविधिकले उल्लेख गर्नुपर्ने विवरण',
		// ---------------------------------------
		mapTech_1: '१. जग्गाधनीको नाम, थर : ',
		mapTech_2: '२. घरधनीको नाम, थर : ',
		mapTech_3: '३. भू–उपयोग क्षेत्र : ',
		mapTech_4: '४. निर्माणको प्रयोजन : ',
		mapTech_5: {
			mapTech_5_head: '५. प्रस्तावित निर्माणले विद्यमान उपभोगमा परिवर्तन गर्ने भए सो को विवरण : ',
			mapTech_5_1: '५.१ भू–उपयोग परिवर्तन (विवरण) : ',
			mapTech_5_2: '५.२ भवनको प्रयोजनमा परिवर्तन (विवरण) : ',
		},
		mapTech_6: {
			mapTech_6_head: '६. प्रस्तावित निर्माण वा उपभोगमा परिवर्तनको लागि मापदण्ड बमोजिम स्वीकृतिको किसिम : ',
			mapTech_6_Opt_1: 'स्वीकृति दिन सकिने',
			mapTech_6_Opt_2: 'स्वीकृति दिन नसकिने',
			mapTech_6_Opt_3: 'विशेष स्वीकृति दिन सकिने',
			checkbox_options: {
				option_1: 'स्वीकृति दिन सकिने',
				option_2: 'स्वीकृति दिन नसकिने',
				option_3: 'विशेष स्वीकृति दिन सकिने',
			},
		},
		mapTech_7: '७. निर्माणको लागि प्रस्तावित जग्गाको कित्ता नं : ',
		mapTech_8: '८. जग्गाधनी पूर्जा अनुसारको जग्गाको क्षेत्रफल : ',
		mapTech_8_1: 'लम्बाई : ',
		mapTech_8_2: 'चौडाई : ',
		mapTech_9: '९. फिल्ड नाप पूर्जा अनुसारको जग्गाको क्षेत्रफल : ',
		mapTech_9_1: 'लम्बाई : ',
		mapTech_9_2: 'चौडाई : ',
		mapTech_10: '१०. प्रस्तावित भवनको प्लिन्थको क्षेत्रफल : ',
		mapTech_10_1: 'लम्बाई : ',
		mapTech_10_2: 'चौडाई : ',
		mapTech_11: '११. प्रस्तावित भवनले ढाक्ने क्षेत्रफल प्रतिशतमा (Ground Coverage %) : ',
		mapTech_11_1: 'लम्बाई : ',
		mapTech_11_2: 'चौडाई : ',
		mapTech_12: '१२. प्रस्तावित तथा साविक भवन निर्माणको तला र क्षेत्रफल सम्बन्धी विवरण : ',
		mapTech_12_1: 'उचाई : ',
		mapTech_12_table: {
			mapTech_12_table_heading: {
				heading_1: 'तला',
				// heading_2: 'प्रस्तावित निर्माणको क्षेत्रफल (वर्ग फिट/वर्ग मिटर)',
				heading_2: 'प्रस्तावित निर्माणको क्षेत्रफल',
				// heading_3: 'साविक निर्माण भैसकेको क्षेत्रफल (वर्ग फिट/वर्ग मिटर)',
				heading_3: 'साविक निर्माण भैसकेको क्षेत्रफल',
				// heading_4: 'जम्मा क्षेत्रफल (वर्ग फिट/वर्ग मिटर)',
				heading_4: 'जम्मा क्षेत्रफल',
				heading_5: 'कैफियत',
			},
			mapTech_12_table_subheading: {
				subheading_1: 'भूमिगत तला',
				subheading_2: 'भुइँ तला',
				subheading_3: 'पहिलो तला',
				subheading_4: 'दोस्रो तला',
				subheading_5: 'तेस्रो तला',
				subheading_6: 'चैथो तला',
				subheading_7: 'शौचालय',
				subheading_8: 'कम्पाउण्डवाल',
			},
		},
		mapTech_13: '१३. प्रस्तावित अन्य निर्माण ( भवन बाहेक जस्तै : पर्खाल, टहरा आदि ) ले ढाक्ने क्षेत्रफल: ',
		mapTech_14: '१४. साविक अन्य निर्माण ( भवन बाहेक जस्तै : पर्खाल, टहरा आदि ) ले ढाकिसकेको क्षेत्रफल : ',
		mapTech_15: {
			mapTech_15_head: '१५. प्रस्तावित भवन निर्माण र साविक भवन निर्माणले गरी ढाक्ने जम्मा क्षेत्रफल (Ground Coverage) : ',
			mapTech_15_1: '१५.१ क्षेत्रफल : ',
			mapTech_15_2: '१५.२ प्रतिशत : ',
		},
		mapTech_16: '१६. जम्मा स्वीकृत गर्न सकिने (Ground Coverage) % : ',
		mapTech_17: {
			mapTech_17_head: '१७. भवनको तला : ',
			mapTech_17_Opt_1: 'से.मी. / वेसमेन्ट',
			mapTech_17_Opt_2: 'जमिन/भुई तला',
			mapTech_17_Opt_3: '१',
			mapTech_17_Opt_4: '२',
			mapTech_17_Opt_5: '३',
			mapTech_17_Opt_6: '४',
			checkbox_options: {
				option_1: 'से.मी. / वेसमेन्ट',
				option_2: 'जमिन/भुई तला',
				option_3: '१',
				option_4: '२',
				option_5: '३',
				option_6: '४',
				option_7: '५',
				option_8: '६',
				option_9: '७',
				option_10: '८',
				option_11: '९',
			},
		},
		mapTech_18: '१८. प्रत्येक तलाको सिलिङ्गको उचाई : ',
		mapTech_18_table: {
			mapTech_18_table_heading: {
				table_heading_1: 'तला विवरण',
				table_heading_2: 'उचाई',
			},
			mapTech_18_table_subheading: {
				table_subheading_1: 'भूमिगत तला',
				table_subheading_2: 'भुइँ तला',
				table_subheading_3: 'पहिलो तला',
				table_subheading_4: 'दोस्रो तला',
				table_subheading_5: 'तेश्रो तला',
				table_subheading_6: 'चैथो तला',
				table_subheading_7: 'शौचालय',
				table_subheading_8: 'कम्पाउण्डवाल',
			},
		},
		mapTech_19: '१९. भवनको कुल उचाई : ',
		mapTech_20: {
			mapTech_20_head: '२०. निर्माणको किसिम : ',
			mapTech_20_radioBox: {
				mapTech_20_Opt_1: 'पक्की',
				mapTech_20_Opt_2: 'लोड वियरिङ्ग',
				mapTech_20_Opt_3: 'फ्रेम स्ट्रक्चर',
				mapTech_20_Opt_4: 'कच्ची',
			},
		},
		mapTech_21: '२१. भवनको गाह्रोमा प्रयोग भएको मसलाको विवरण : ',
		mapTech_22: {
			mapTech_22_head: '२२. भवनको छानाको किसिम : ',
			mapTech_22_radioBox: {
				mapTech_22_Opt_1: 'आर.सि.सि',
				mapTech_22_Opt_2: 'आर.वि.सि',
				mapTech_22_Opt_3: 'टायल',
				mapTech_22_Opt_4: 'जस्ता',
				mapTech_22_Opt_5: 'खरको छाना',
				mapTech_22_Opt_6: 'अन्य',
			},
		},
		mapTech_23: {
			mapTech_23_head: '२३. मापदण्ड अनुसार प्रस्तावित निर्माणसंग जोडिएको सडकको सडक अधिकार क्षेत्र : ',
			mapTech_23_Opt_1: 'पुर्व तर्फ',
			mapTech_23_Opt_2: 'पश्चिम तर्फ',
			mapTech_23_Opt_3: 'उत्तर तर्फ',
			mapTech_23_Opt_4: 'दक्षिण तर्फ',
			direction_suffix: 'तर्फ',
			defaultRoadOption: 'बाटो छैन',
			road: 'बाटो',
			defaultNadiOption: 'अन्य',
			requiredDistance: 'छोड्नुपर्ने न्युनतम दुरी',
			adhikarChetra: 'छाडिएको दुरी',
			roadName: 'बाटोको नाम',
		},
		mapTech_24: '२४. सडकबाट प्रस्तावित भवनसम्मको सेट व्याक : ',
		mapTech_24_table: {
			mapTech_24_table_heading: {
				heading_1: 'जग्गाको',
				//  heading_2: 'सडकको केन्द्र रेखाबाट (मि/फिट) छाडिएको',
				// heading_3: 'सडकको सिमाना छेउबाट (मि/फिट) छाडिएको',
				// heading_4: 'सडकको अधिकार क्षेत्रको किनाराबाट (मि/फिट) छाडिएको',
				heading_2: 'सडकको केन्द्र रेखाबाट छाडिएको',
				heading_3: 'सडकको सिमाना छेउबाट छाडिएको',
				heading_4: 'सडकको अधिकार क्षेत्रको किनाराबाट छाडिएको',
			},
			mapTech_24_table_subheading: {
				// subheading_1: 'अगाडि तर्फ',
				// subheading_2: 'दाँया तर्फ',
				// subheading_3: 'बाँया तर्फ',
				// subheading_4: 'पछाडि तर्फ'
				subheading_1: 'पुर्व तर्फ',
				subheading_2: 'पश्चिम तर्फ',
				subheading_3: 'उत्तर तर्फ',
				subheading_4: 'दक्षिण तर्फ',
			},
		},
		mapTech_25: '२५. प्रस्तावित भवनको बाहिरी पर्खाल र सिमानासम्मको दुरीको विवरण : ',
		mapTech_25_table: {
			mapTech_25_table_heading: {
				heading_1: 'दिशा',
				heading_2: 'सडक',
				heading_3: 'झ्याल/ढोका',
				heading_4: 'न्युनतम छाड्नु पर्ने',
				heading_5: 'छाडिएको',
			},
			mapTech_25_table_subheading: {
				subheading_1: 'पुर्व',
				subheading_2: 'पश्चिम',
				subheading_3: 'उत्तर',
				subheading_4: 'दक्षिण',
			},
			options: {
				option1: 'छ',
				option2: 'छैन',
			},
		},
		mapTech_26: {
			mapTech_26_title: '२६. सार्वजनिक जग्गा,नदी, कुलो आदिको किनारामा निर्माण प्रस्ताव गरिएको भए सो को नाम तथा विवरण',
			mapTech_26_subtitle: '(नाम/विवरण)',
		},
		mapTech_26_table: {
			mapTech_26_table_subheading: {
				// subheading_1: 'छोड्नुपर्ने न्युनतम दुरी (मि/फिट)',
				// subheading_2: 'छाडिएको दूरी (मि/फिट)',
				subheading_1: 'छोड्नुपर्ने न्युनतम दुरी',
				subheading_2: 'छाडिएको दूरी',
			},
		},
		mapTech_27: '२७.भवनबाट कति टाढासम्म क्यान्टीलिभर (छज्जा) प्रस्ताव गरिएको छ ?',
		mapTech_27_table: {
			mapTech_27_table_heading: {
				heading_1: '',
				// heading_2: 'अगाडि तर्फ (मि/फिट)',
				// heading_3: 'पछाडि तर्फ (मि/फिट)',
				// heading_4: 'दाँया तर्फ (मि/फिट)',
				// heading_5: 'बाँया तर्फ (मि/फिट)',
				heading_2: 'अगाडि तर्फ',
				heading_3: 'पछाडि तर्फ',
				heading_4: 'दाँया तर्फ',
				heading_5: 'बाँया तर्फ',
				heading_6: 'कैफियत',
			},
			mapTech_27_table_subheading: {
				subheading_1: 'प्रस्तावित',
				subheading_2: 'स्वीकृत गर्न सकिने',
			},
		},
		mapTech_28: '२८. जग्गा माथिबाट हाई टेन्सन लाइन गएको भए सो को किनाराबाट प्रस्तावित भवन निर्माण सम्मको दूरी :',
		mapTech_28_table: {
			mapTech_28_table_subheading: {
				// subheading_1: 'छोड्नुपर्ने न्युनतम दुरी (मि/फिट)',
				// subheading_2: 'छाडिएको दूरी (मि/फिट)',
				subheading_1: 'छोड्नुपर्ने न्युनतम दुरी',
				subheading_2: 'छाडिएको दूरी',
			},
		},
		maptech_person_table: {
			maptech_person_table_header_1: 'नक्सा बनाउनेको तर्फबाट',
			maptech_person_table_header_2: 'नक्सा पास एवं निर्माण इजाजतका लागि निवेदन गर्नेको तर्फबाट',
			maptech_person_table_cell_1: {
				cell_1_data_1: 'मैले नक्सा बनाउने प्राविधिकले पालना गर्नुपर्ने कुराहरुको अध्ययन गरी निवेदन श्री ',
				cell_1_data_2: ' को नक्सा बनाएको हुँ । उक्त नक्सा तोकिएको मापदण्ड विपरीत बनाइएको ठहर नियमानुसार सहुँला बुझाउँला । ',
				cell_1_data_3: 'सही : ',
				cell_1_data_4: 'नाम : ',
				cell_1_data_5: 'योग्यता एंव पद : ',
				cell_1_data_6: 'कन्सल्टीङ्ग फर्म भए सो को नाम : ',
				cell_1_data_7: 'न.पा मा दर्ता भएको व्यवसाय प्रमाण पत्रको नं. : ',
				cell_1_data_8: 'फर्मको छाप : ',
				cell_1_data_9: 'मिति : ',
			},
			maptech_person_table_cell_2: {
				cell_2_data_1:
					'माथि उल्लेखित प्राविधिक विवरण एवं नगरपालिका मापदण्ड बमोजिम स्वीकृत नक्सा अनुसारको निर्माण कार्य गर्न म/हामी मञ्जुर छु/छौँ । मापदण्ड विपरित र ढाँचा विपरित साथै सार्वजनिक जग्गा अतिक्रमण हुने गरी बनाइएका ठहर कानुन बमोजिम सहुला बुझाउँला । ',
				cell_2_data_2: 'सही : ',
				cell_2_data_3: 'नाम : ',
				cell_2_data_4: 'ठेगाना : ',
				cell_2_data_5: 'मिति : ',
				cell_2_data_6: 'सम्पर्क फोन/मोबाइल : ',
			},
		},
	},
};

export const floorDetailsData = {
	floorDetailsDataTable: {
		floorDetailsDataTable_heading: {
			heading_1: 'घर/कम्पाउण्डवालको',
			heading_2: 'लम्बाई ',
			heading_3: 'चौडाई ',
			heading_4: 'उचाई ',
			heading_5: 'क्षेत्रफल ',
		},
		floorDetailsDataTable_subheading: {
			subheading_1: 'भुमिगत तला',
			subheading_2: 'भुइँ तला',
			subheading_3: 'पहिलो तला',
			subheading_4: 'दोस्रो तला',
			subheading_5: 'तेश्रो तला',
			subheading_6: 'चौथो तला',
			subheading_7: 'पाँचौ तला',
			subheading_8: 'छैटौँ तला',
			subheading_9: 'सातौँ तला',
			subheading_10: 'आठौं तला',
			subheading_11: 'नवौं तला',
			subheading_12: 'शौचालय',
			subheading_13: 'सेप्टिक',
			subheading_14: 'कम्पाउण्डवाल',
		},
	},
};

export const prabidhikpratibedhan = {
	prabidhikpratibedhan_data: {
		date: 'मिति : ',
		ppbv_heading: 'श्रीमान् प्रमुख प्रशासकिय अधिकृत ज्यु',
		// ---------------------------------------------
		ppbv_subject: 'बिषय : प्राबिधिक प्रतिबेदन पेश सम्बन्धमा ।',
		ppbv_data_1: ' प्रस्तुत बिषयमा ',
		ppbv_data_2: ' वडा न. ',
		ppbv_data_3: ' मा बस्ने श्री ',
		ppbv_data_4: ' ले श्री ',
		ppbv_data_5: ' का नाममा दर्ताकायम साबिक ',
		ppbv_data_6: ' हाल ',
		ppbv_data_7: ' वडा न. ',
		ppbv_data_8: ' अन्तर्गत ',
		ppbv_data_8_1: ' टोल ',
		ppbv_data_8_2: ' मार्गमा पर्ने ',
		ppbv_data_8_3: ' कि न. ',
		ppbv_data_9: ' ज.बि. ',
		ppbv_data_10: ' मा निर्माण कार्यको नक्सासहित दरखास्त पेश गर्नुभएकोमा स्थानिय सरकार संचालन ऐन २०७४ (२८) दफा ३१ को खण्ड (ख) बमोजिम मिती ',
		ppbv_data_11:
			' मा सो निर्माण कार्यको स्थलगत सर्जमीन मुचुल्का उठाई प्राबिधिक प्रतिबेदन पेश गर्नखटी गएकोले निर्माण कार्यका सम्बन्धमा तपसिल बमोजिमको प्रतिबेदन आबश्यक निर्णयार्थ श्रीमान् मा पेश गरेको छु । ',
		// ----------------------------------------------------
		ppbv_content_1: 'प्रतिबेदन पेश गर्नेको',
		ppbv_content_2: 'नाम, थर : ',
		ppbv_content_3: 'पद : ',
		ppbv_content_4: 'दस्तखत : ',
		// --------------------------------------------
		ppbv_footer_header: 'तपसिल',
		ppbv_footer_data_1: '१) निर्माण कार्यको विवरण : ',
		ppbv_footer_data_2: '२) घर बनिने जग्गाको क्षेत्रफल : ',
		ppbv_footer_data_3: '३) सडकको माप्दण्ड ',
		ppbv_footer_data_3_1: ' अधिकार क्षेत्र ',
		ppbv_footer_data_3_2: ' केन्द्र रेखाबाट छोड्नु पर्ने दुरी ',
		ppbv_footer_data_4: '४) छानाको किसिम ',
		ppbv_footer_data_4_1: ' दिवालको नाप ',
		ppbv_footer_data_5: '५) भुइतलाको ',
		ppbv_footer_data_5_0: ' लम्बाई ',
		ppbv_footer_data_5_1: ' चौडाइ ',
		ppbv_footer_data_5_2: ' उचाइ ',
		ppbv_footer_data_5_3: ' क्षेत्रफल ',
		ppbv_footer_data_6: '६) अन्य तलाको अधिकतम ',
		ppbv_footer_data_6_0: ' लम्बाइ ',
		ppbv_footer_data_6_1: ' चौडाई ',
		ppbv_footer_data_6_2: ' उचाई ',
		ppbv_footer_data_6_3: ' जम्मा क्षेत्रफल ',
		ppbv_footer_data_7: '७) बिधुत तारको दूरी ',
		ppbv_footer_data_7_1: ' भोल्ट ',
		ppbv_footer_data_8: '८) नदीको नाम ',
		// ppbv_footer_data_8_1: ' नदी दूरी ',
		ppbv_footer_data_8_1_1: ' छोड्नुपर्ने न्युनतम दुरी : ',
		ppbv_footer_data_8_1: ' छोडिएको दूरी ',
		ppbv_defaultNadiOptions: 'अन्य',
		ppbv_footer_data_9: '९) शौचलय/स्नानगृह/सेप्टिक ट्यांक/सोकपिट ब्यवस्था ',
		ppbv_footer_data_10: '१०) पेश नक्सासँग निर्माण स्थल मिल्ने/नमिल्ने ',
		ppbv_footer_data_11: '११) अन्य ब्यवहोरा',
		ppbv_footer_data_11_1: 'क) भवन निर्माण आस्थायी निर्माण स्विक्रिती दिन मिल्ने/नमिल्ने सम्बन्धमा उपशाखा/शाखा प्रमुखको आवश्यक राय',
		ppbv_footer_data_11_2:
			'ख) सबिक गा.वि.स.को न.पा.मा समाहित हुनु अगाडि निर्माण सम्पन्न भएको भवनको नक्सा नियमित गर्न मिल्ने नमिल्ने सम्बन्धमा शाखा र उपशाखा प्रमुखको राय ',
		// -- Sansodhan
		sansodhan_check: [' छैन', ' छ'],
		sansodhan_checklabel: 'संसोधन गर्नु पर्ने छ/छैन ?',
		sansodhan_legend: 'संसोधन',
	},
};

export const PlinthLvlOwnerRepresentation = {
	PlinthLvlOwnerRepresentation_data: {
		plor_heading1: '(प्राविधिक परामर्शदाताको प्रयोजनको लागि)',
		plor_heading2: 'प्लिन्थ लेभलसम्मको निर्माण कार्य सम्पन्नबारे घरधनीको तर्फबाट राखिएको सुपरिवेक्षकको प्रतिवेदन',
		radioBox: ['छ', 'छैन'],
		plor_else_placeholder: '(छैन भने विवरण खुलाउने)',
		// ------------------------------------------------------------------
		plor_data_1: 'श्री ',
		plor_data_2: ' को वडा नं ',
		plor_data_3: ' टोल/स्थान ',
		plor_data_4: ' मा प्लिन्थ लेभलसम्मको निर्माण कार्य सम्पन्न भएको हुनाले सो को प्रतिवेदन पेश गरेको छु ।',
		// ----------------------------------------------------------------------
		plor_data_5: {
			plor_data_5_1: '१. सडक अधिकार क्षेत्र सम्बन्धित मापदण्ड पालना भएको छ/छैन (छैन भने विवरण खुलाउने) :- ',
			plor_data_5_2: '२. साइट प्लानमा देखाइए बमोजिम सेट व्याक पालना भएको छ/छैन (छैन भने विवरण खुलाउने) :- ',
			plor_data_5_3: '३. ग्राउण्ड कभरेजमा फरक परेको छ/छैन (छैन भने विवरण खुलाउने) :- ',
			plor_data_5_4: '४. कुनै पनि फेरबदल भएको भए सो को विवरण खुलाउने ( आवश्यक भएमा अलग्गै कागजमा समेत खुलाउन सकिने ) ',
			plor_data_5_5: '५. नेपाल राष्ट्रिय भवन निर्माण संहिता २०६० अनुसार निर्माण भएको छ/छैन (छैन भने विवरण खुलाउने) :- ',
			plor_data_5_6: '६. कुनै पनि फेरबदल भएको भए सो को विवरण खुलाउने ( आवश्यक भएमा अलग्गै कागजमा समेत खुलाउन सकिने )',
		},
		// -------------------------------------------------------------------------
		plor_data_6:
			'उपरोक्त भवनको निर्माणको सुपरिवेक्षण म आफैंले गरेको हुनाले उक्त प्रतिवेदन पेश गरेको हुँ। अन्यथा भएमा नियमानुसार सहुँला बुझाउँला । ',
		plor_data_7: {
			plor_data_7_1: 'हस्ताक्षर : ',
			plor_data_7_2: 'प्राविधिकको नाम : ',
			plor_data_7_3: 'नेपाल ईन्जिनियरिङ परिषद दर्ता नं. ( ईन्जिनियर/आर्किटेक्टको लागि मात्र) : ',
			plor_data_7_4: 'कन्सल्टीङ्ग फर्म भए सोको नाम र छाप : ',
			plor_data_7_5: ' मा दर्ता भएको व्यवसाय प्रमाण पत्रको नं : ',
			plor_data_7_6: 'निर्माण कार्य नेपाल राष्ट्रिय भवन निर्माण संहिता २०६० अनुसार भएको अन्यथा भएमा नियमानुसार सहुँला बुझाउँला भनि सही गर्ने ।',
		},
		plor_data_footer_left: {
			plor_data_footer_left_1: 'निर्माण व्यवसाय',
			plor_data_footer_left_1_1: 'सम्बन्धित निर्माण व्यवसाय',
			plor_data_footer_left_2: 'नाम : ',
			plor_data_footer_left_3: 'ठेगाना : ',
			plor_data_footer_left_4: 'न.पा. दर्ता प्रमाण पत्र : ',
			plor_data_footer_left_5: 'द. ',
		},
		plor_data_footer_right: {
			plor_data_footer_right_1: 'मुख्य डकर्मी',
			plor_data_footer_right_1_1: 'सम्बन्धित डकर्मी',
			plor_data_footer_right_2: 'नाम : ',
			plor_data_footer_right_3: 'ठेगाना : ',
			plor_data_footer_right_4: 'न.पा. दर्ता प्रमाण पत्र : ',
		},
		footer_gharDhani: {
			gharDhani: 'घरधनी',
			name: 'नाम : ',
		},
		check: [' छैन', ' छ'],
		checklabel: 'संसोधन गर्नु पर्ने छ/छैन ?',
		sansodhantitle: 'संसोधन विवरण ',
		miti: 'मिति: ',
		nibedan: 'निवेदन ',
	},
};

export const superstructurenodeorderview = {
	superstructurenodeorderview_data: {
		date: 'मिति : ',
		title: 'विषय : सुपरस्ट्रक्चर निर्माणका निमित्त इजाजत प्रदान गर्ने टिप्पणी आदेश ।',
		title_1: 'टिप्पणी र आदेश',
		title_2: 'नक्सापास उपशाखा',
		gharNaksaUpasakha: 'घर नक्सापास उपशाखा',
		title_3: 'घर नक्सा शाखा',
		// ------------------------------
		pasa: 'प. सं.',
		chana: 'च. नं.',
		wada: ' वडा नं. ',
		data_1: ' बस्ने श्री ',
		data_2: ' ले मेरो/हाम्रो नाममा दर्ता रहेको साविक ',
		data_3: ' हाल वडा नं  ',
		data_4: ' को ',
		data_5: ' सडकमा पर्ने कि.नं ',
		data_6: ' को क्षेत्रफल ',
		data_7: ' मा प्लिन्थ लेवलसम्म निर्माणको लागि मिति : ',
		data_8:
			' मा स्वीकृत पत्र लिई प्लिन्थ लेवलसम्म निर्माण गरी निर्माण कार्यमा संलग्न प्राविधिक/कन्सल्टेन्टको प्रतिवेदन सहित सुपरस्ट्रक्चरको लागि निवेदन दिनु भएकोले सो को आधारमा यस न.पा.का प्राविधिक श्री ',
		data_9: ' ले निरीक्षण सुपरिवेक्षण गरी दिएको संलग्न प्रतिवेदन अनुसार निजलाई सुपरस्ट्रक्चर इजाजत',
		// data_9_1:'मिल्ने/मापदण्ड अनुसार देखिएकोले ',
		// data_9_1: 'भएकोले स्वीकृतका लागि निर्णय प्रेषित',
		dinaMilney: {
			first: 'भएकोले स्वीकृतिका लागि निर्णयार्थ प्रेषित ।',
		},
		parit: {
			first: 'नक्सा विपरित घर अदल बदल गरेकोले नक्सा सुधार गर्न',
			second: 'हुँदा जरिवाना गरी स्वीकृतिका लागि निर्णयार्थ प्रेषित ।',
		},
		data_10: ' विपरित भएकोले सुधार गरिआउन आदेश दिनुपर्ने देखिएकोले र ',
		data_10_1: 'सम्म भवन निर्माण गरेको पाईएको हुँदा जरिवाना गरी स्वीकृतिका लागि निर्णयार्थ प्रेषित ।',
		pesh: 'पेश गर्ने : ',
		ruji: 'रुजु गर्ने : ',
		swrikrit: ' स्व्रिकृत गर्ने : ',
		name: 'नाम : ',
		level: 'पद : ',
		sign: 'दस्तखत : ',
	},
	kamalamaiData: {
		bisaye: 'विषय : सुपरस्ट्रक्चर इजाजत सम्बन्धमा ।',
		address: ' यस ',
		esthan: " स्थान ",
		wardNo: ' वडा नं. ',
		kittaNo: ' मा अवस्थित कि.नं. ',
		area: ' क्षेत्रफल ',
		gharDhani: ' मा भवन निर्माण गर्ने घरधनी श्री ',
		bhawanDate: ' ले भबन निर्माण गर्न मिति ',
		prabidhikShree: ' मा प्लिन्थ ईजाजत लिनु भएको हुँदा सोही सिलसिलामा यस नगरपालिका कार्यालयका प्राविधिक श्री ',
		organization: ' ले स्थलगत निरिक्षण गरी पेश गर्नु भएको प्रतिवेदन अनुसार ',
		end:
			' को भवन निर्माण सापदण्ड र नेपाल राष्ट्रिय भवन निर्माण संहिताको पालना भएको प्रतिवेदन प्राप्त हुन आएकोले सुपरस्ट्रक्चर ईजाजत दिनको लागि निर्णयार्थ पेश गरेको छु।',
	},
	kageyshowriData: {
		nagarpalika : "कागेश्वरी मनोहरा नगरपालिका",
		location : "डाँछी, काठमाडौं, ३ नं. प्रदेश, नेपाल",
		subEng: "सब. ईन्जिनियर",
		eng: "ईन्जिनियर",
		strEng: "ईन्जिनियर",
		pramukh: "प्रमुख प्रशासकीय अधिकृत",
		bisaye: 'विषय : सुपरस्ट्रक्चर निर्माणको निमित्त इजाजत प्रदान गर्ने ।',
		sriman : "श्रीमान्",
		jilla: "जिल्ला",
		palika: "गा.पा./न.पा. वडा नं.",
		basne: "बस्ने ",
		shree: 'श्री',
		le : "ले",
		date : "मिति",
		content1 : "नाँउमा दर्ता रहेको कागेश्वरी मनोहरा नगरपालिका वडा नं.",
		kittaNo : "कि.नं.",
		cheetrafal : "षेत्रफल",
		bamiko : "व.मी. को",
		content2: "सडक/बाटोमा साँध सिमाना रहेको उक्त सडक/बाटोको बीच केन्द्रबाट",
		content3 : " छाडी चौतर्र्फी सँधियार",
		direction1: "पूर्व",
		direction2: "पश्चिम",
		direction3: "उत्तर",
		direction4: "दक्षिण",
		content4: "भएको जग्गामा",
		lambae : "लम्वाई",
		chaudae : "चौडाई",
		uchae : "उचाई",
		talle : "तले",
		bargafeet :"वर्गफिट",
		bhayeko :"भएको",
		content5 :"को निर्माणको लागि मिति",
		content6: "मा प्रथम चरण  (plinth level) सम्म निर्माण कार्य गर्नका लागि प्रथम चरणको ईजाजत–पत्र प्रदान गरिएको । स्वीकृत नक्शा बमोजिम प्रथम चरणको निर्माण कार्य सम्पन्न गरेको र सुपरस्ट्रक्चर निर्माणको निमित्त अनुमति पाउँ भनी मिति",
		content7: "मा निजले निवेदन दिनुभएकोमा यस नगरपालिकाको कार्यालयबाट प्राविधिक श्री",
		content8: "मा स्थलगत निरीक्षण गरी दिएको प्रतिवेदन अनुसार निज घरधनीले जमीन सतह  (plinth level) सम्म निर्माण कार्य स्वीकृत नक्सा र मापदण्ड बमोजिम नै सम्पन्न गरेको हुँदा सुपरस्ट्रक्चर निर्माण गर्नका लागि अनुमति दिन मिल्ने राय पेश गरेको हुँदा स्थानीय सरकार संचालन ऐन, २०७४ को दफा ३३ बमोजिम दोस्रो चरणको सुपरस्ट्रक्चर निर्माणको निमित्त अस्थायी ईजाजत पत्र उपलब्ध गराउन मनासिव देखि निर्णयार्थ यो टिप्पणी आदेश पेश गरेको छु ।",



	},
};
export const superstructureSundarHaraicha = {
	content: {
		superTitle: {
			title_1: '(भवन नक्सापास उपशाखा)',
			title_2: '(टिप्पणी र आदेश)',
			title_3: 'विषय : सुपरस्ट्रक्चर इजाजत सम्बन्धमा ।',
		},
		miti: 'मिति :',
		data_1: 'वडा नं. ',
		data_2: ' सडक ',
		data_3: ' बस्ने श्री ',
		data_4: 'ले घरनक्सा पासको लागि दिन भएको निवेदन अनुसार नीजक नाममा दर्ता कायम रहेको हाल ',
		data_5: ' साविक ',
		data_6: ' कि.नं ',
		data_7: 'को जम्मा क्षेत्रफल ',
		data_8: 'मा नयाँ घर निर्माण गर्न मिति',
		data_9: 'प्लिन्थ इजाजत लिनु भएको/इजाजत पूर्व साविक परानो घर निर्माण भएको हुँदा सोही सिलसिलामा यस न. पा. कार्यालयका  प्रावधिक श्री',
		data_10:
			'ले स्थलगत निरिक्षण गरी पेश गर्नु भएको प्रतिवेदन प्राप्त हुन आएकोले इजाजत पूर्व निर्माण गरेवापत जरिवाना लिई/इजाजत लिई गरेकोलवे जरीबाना नलि सुपरस्ट्रक्चर इजाजत दिनको लागि निर्णाथ प्रेषित ।',
		footer: 'इजाजत पूर्व बनिसकेको भए भवनको : ',
	},
};

export const superstructureconstructionview = {
	superstructureconstructionview_data: {
		title: 'दोस्रो चरणको (सुपरस्ट्रक्चर निर्माण कार्यको) इजाजत पत्र',
		// -----------------------------------------------
		header: {
			header_1: 'प.सं.:',
			header_2: 'च.नं.:',
			header_3: 'मिति:',
		},
		data_1: {
			data_1_1: 'श्री ',
			data_1_2: ' वडा नं. ',
			data_1_3: ' घर नं. ',
			data_1_4: ' सडकको नाम',
		},
		data_2: {
			data_2_1: ' उपर्युक्त सम्बन्धमा तपाईले यस कार्यालयबाट मिति ',
			data_2_2:
				' मा प्लिन्थ लेवलसम्म निर्माण कार्य गर्न स्वीकृति लिई / नलिई निर्माण गर्दै गर्नु भएकोले घरको नक्सा पास प्रक्रिया पूरा भएको हुँदा स्थानीय सरकार सञ्चालन ऎन २०७४ तथा ',
			//  स्वायत्त शासन ऐन तथा न.पा. को नियम अनुसार निर्माण कार्य गर्ने गरी सुपरस्ट्रक्चर निर्माण कार्यको इजाजत दिने निर्णय भएकोले नक्सा पास दस्तुर रु',
			// data_2_3: 'र जरिवाना रु',
			data_2_3:
				'  को योजना तथा भवन निर्माण विनियम २०७४ अनुसार निर्माण कार्य गर्ने गरी  सुपरस्ट्रक्चर निर्माण कार्यको इजाजत दिने निर्णय भएकोले निम्न विवरण अनुसार निर्माण गर्ने गरी सुपरस्ट्रक्चर निर्माण कार्यको इजाजत दिइएको छ ।',
			data_2_4: ' नियम अनुसार निर्माण कार्य गर्ने गरी  सुपरस्ट्रक्चर निर्माण कार्यको इजाजत दिने निर्णय भएकोले दस्तुर रु. ',
			data_2_5: 'जरिवाना रु. ',
			data_2_6: 'बुझि निम्न विवरण अनुसार निर्माण गर्ने गरी सुपरस्ट्रक्चर निर्माण कार्यको इजाजत दिइएको छ ।',
		},
		data_3: 'निर्माण कार्य समाप्त भएपछि निर्माण सम्पन्न प्रमाण–पत्र लिन अनिवार्य यो पत्र साथ संलग्न फारम भरी उपस्थित हुन अनुरोध छ ।',
		data_4: {
			data_4_1: 'साविक ',
			data_4_2: 'हाल ',
			data_4_3: ' वडा नं. ',
			data_4_4: 'घर नं. ',
			data_4_5: 'सडक ',
			data_4_6: 'कि.नं. ',
			data_4_7: 'क्षेत्रफल ',
		},
		maindata: {
			maindata_01: 'जग्गाको विवरण',
			maindata_02: 'निर्माण कार्यको विवरण ',
			maindata_1: '१. भवन निर्माणको किसिम : ',
			maindata_2: '२. भवनको स्ट्रक्चरल सिस्टम : ',
			maindata_3: '३. भवनको प्लिन्थ एरिया ',
			sabikPlinthArea: 'साविक भवनको प्लिन्थ एरिया ',
			maindata_3_sabik: 'साविक भवनको प्लिन्थ एरिया ',
			maindata_3_1: 'कभर एरिया ',
			maindata_3_2: 'स्वीकृत ग्राउण्ड कवरेज ',
			maindata_3_3: 'भवनको क्षेत्रफल ',
			maindata_3_4: 'कभरेज​ एरिया ',
			maindata_3_5: 'घरको लम्बाई ',
			maindata_3_6: ' चौडाई ',
			maindata_3_7: ' उचाई ',
			maindata_4: '४. भवनको तला संख्या : ',
			maindata_4_1: 'स्वीकृती पाउने तला संख्या : ',
			maindata_5: '५. भवनको कूल उचाई ',
			maindata_5_1: 'स्वीकृती पाउने कूल उचाई ',
			maindata_6: '६. भवनको प्रयोजन : ',
			maindata_7: '७. भवनको वर्गिकरण : ',
			maindata_8: '८. हालको सडककोे केन्द्र रेखाबाट सडकको अधिकार क्षेत्रको निमित्त छोड्नु पर्ने दुरी : ',
			maindata_9: '९. सडक अधिकार वाहेक सेटव्याक : ',
			maindata_10: '१०. मिति ',
			maindata_10_1:
				'मा प्रदान गरिएको No-Objection Certificate तथा भवन / घरनक्सा मापदण्ड  तथा कार्याविधि २०६९ बमोजिम स्वीकृत नक्सा अनुसार निर्माण कार्य गर्नु पर्नेछ । ',
			maindata_10_1_inaruwa:
				'मा प्रदान गरिएको No-Objection Certificate तथा नेपाल राष्ट्रिय भवन निर्माण संहिता २०६० बमोजिम स्वीकृत नक्सा अनुसार निर्माण कार्य गर्नु पर्नेछ । ',
			maindata_10_1_birtamod:
				'मा प्रदान गरिएको प्रथम चरणको अस्थायी निर्माण इजाजत पत्र तथा नेपाल राष्ट्रिय भवन संहिता २०६० बमोजिम स्वीकृत नक्सा अनुसार निर्माण कार्य गर्नु पर्नेछ । ',
			maindata_11_birtamod:
				'११. स्वीकृत नक्सा र भवन निर्माण संहिता विपरित निर्माण कार्य गरेमा न.पा. ऐन नियम अनुसार कार्वाहि भएमा मलाई मान्य हुनेछ भनी सही गर्दछु ।',
			maindata_11: '११. अन्य: ',
			// maindata_11_1: 'घरधनी वा वारेसको दस्तखत',
			maindata_11_2: 'मिति :',
			maindata_11_3: '४. ग्राउण्ड कवरेज ',
			maindata_11_4: 'अधिकतम ग्राउण्ड कवरेज ',
			maindata_11_5: '५. भवनको तला संख्या ',
			maindata_11_6: 'अधिकतम तला संख्या ',
			maindata_11_7: '६. भवनको कूल उचाई',
			maindata_11_8: 'अधिकतम् कूल उचाई',
			maindata_11_9: '७. भवनको प्रयोजन : ',
			maindata_12_1: '८. भू-उपयोग क्षेत्र : ',
			maindata_12_2: '९. भवनको वर्गिकरण : ',
			maindata_12_3: '१०. घरको ब्लक संख्या',
			maindata_12_4: '११. हालको सडककोे केन्द्र रेखाबाट सडकको अधिकार क्षेत्रको निमित्त छोड्नु पर्ने दुरी : ',
			eleven: '११. ',
			maindata_12_5: '१२. सडक अधिकार वाहेक सेटव्याक : ',
			maindata_12_6: '१३. नेपाल राष्ट्रिय भवन संहिता २०६० सम्बन्धित विवरण :',
			input_1: ['पालना भएको', 'पालना नभएको'],
		},
		newData: {
			data1: 'मा यस कार्यालय बाट प्लिन्थ लेवलसम्मको निर्माण कार्य गर्न स्वीकृति लिई/नलिई घर निर्माण गरी रहनु भएकोले मिति ',
			data2: ' मा यस कार्यालय बाट प्लिन्थ लेवल सम्म को निर्माण गर्न स्वीकृति नलिई गरेकोले/को पालना नगरेकोले जरिवाना रु. ',
			data3: 'लिई देहाय बमोजि निर्माण गर्ने गरी दोस्रो चरण (सुपरस्ट्रक्चर) निर्माण कार्यको इजाजत-पत्र दिइएको छ |',
			footerSignature: [
				['फारम भर्ने'],
				['भवन नक्सा पास उपशाखा', '(स. इन्जिनियर)'],
				['जाँच गर्ने', '(इन्जिनियर)'],
				['प्रमुख प्रशासकिय अधिकृत', 'प्रमाण-पत्र प्रदान गर्ने'],
			],
			data8:
				'१. भवन तथा योजना मापदण्ड तथा स्वीकृत नक्सा र नेपाल राष्ट्रिय भवन निर्माण संहिता २०६० विपरित पाका निर्माण काय गरेमा न. पा. ऐन, नियम अनुसार कार्याही भएमा मलाई मान्य हने छ भनी सही दर्गछु  ।',
			data9:
				'२. निर्माण सम्पन्न पश्चात निर्माण सम्पन्न प्रमाण-पत्र लिन आफले नक्सा वनाएको कन्सलटेन्सीको प्राविधिक प्रतिवेदन (निर्माणकर्मीको दस्तखत सहित )यस कार्यालयमा निवेदन पेश गरी निर्माण सम्पन्न प्रमाण-पत्र लिनुपर्नेछ |',
			data10:
				'३. घरको अगाडी बाटो तथा नाला साथै वरपर निर्माण सामाग्री हटाई सफा गर्न पर्नछ| साथै कम्तीमा २ वाट विरुवा रोपे पछि मात्र निर्माण सम्पन्न दिन सकिनेछ |',
		},
		footer: {
			footer_1: 'नक्सा पास उपशाखा',
			footer_2: 'इन्जिनियर',
			footer_3: 'प्रमुख प्रशासकिय अधिकृत​',
			nagarPramukh: 'नगर प्रमुख',
			footer_4:
				'स्वीकृत भवन / घरनक्सा तथा कार्याविधि २०७१ विपरित निर्माण कार्य गरेमा न.पा.नियमानुसार कारवाही भएमा मलाई मान्य हुनेछ भनी सही गर्दछु ।',
			footer_4_1: 'स्वीकृत नक्सा र भवन निर्माण संहिता विपरित निर्माण कार्य गरेमा ',
			footer_4_2: ' नियमानुसार कारवाही भएमा मलाई मान्य हुनेछ भनी सही गर्दछु ।',
			footer_5: 'घरधनी / वारेशको दस्तखत​',
			footer_6: 'फाँटवालाको दस्तखत​',
			sign_field: 'फ़िल्ड निरीक्षण गरि पेश गर्ने',
			sign_sipharis: 'सिफारिस गर्ने',
			sign_swikrit: 'स्वीकृत गर्ने',
		},
		footerArray: ['घरधनी / वारेशको दस्तखत​', 'फाँटवालाको दस्तखत​'],
	},
};
// yaDekhi
export const superstructureConstructionKageyshowri = {
	kageyshowriData: {
		kamalamaiTitle: 'सुपरस्ट्रक्चर निर्माणको ईजाजत-पत्र',
		date: 'मिति : ',
		shree: "श्री",
		shabik: "साविक",
		gabisha: "गा.वि.स./नगरपालिका वडा नं.",
		hal: "भई हाल",
		wadaNo: "नगरपालिका वडा नं.",
		alphabiram : '|',
		toll : "टोल",
		kittaNo: "कित्ता नं.",
		chhetrafal : "क्षेत्रफल",
		bamiBhayeko: "व.मि.",
		content1: "नगरपालिका वडा नं.",
		content2: "सडक/बाटोसँग साँध सिमान रहेको जग्गाको नाप नक्शा बमोजिम नयाँ घर/टहरा/पर्खाल निर्माण गर्नका लागि स्थानीय सरकार सञ्चालन ऐन २०७४ को दफा ३३ तथा राष्ट्रिय भवन निर्माण संहिता अनूरुपको भूकम्प सुरक्षात्मक प्रविधिलाई पूर्ण रुपमा पालना गर्ने शर्तमा यस कार्यालयको मिति ",
		content3: "को निर्णयानुसार तपाईलाई जमिन सतह (Plinth level) सम्मको निर्माण कार्य गर्नका लागि प्रथम चरणको अस्थायी ईजाजत पत्र प्रदान गरिएकोमा स्वीकृत नक्शा, डिजाईन अनुसार तपाईले उक्त कार्य सम्पन्न गर्नुभएको हुँदा दोस्रो चरणको सुपरस्ट्रक्चरको निर्माण कार्य गर्नका निमित्त तपाईलाई सुपरस्ट्रक्चर निर्माणको अस्थायी ईजाजत–पत्र प्रदान गरिएको छ ।",
		tableData: {
			headings : {
				heading1: "प्राविधिक प्रतिवेदन सारांश",
				heading2: "कैफियत",
				heading3: "क्र. स.",
				heading4: "विवरण",
				heading5: "मापदण्ड अनुसार",
				heading6: "नक्शा अनुसार",
				heading7: "साविक",
				heading8: "हाल",
			},
			notLoopable: {
				data:{
					checkbox: ["क", "ख", "ग", "घ"]
				},
				number: "१.",
				topic: 'भवनको वर्गीकरण'
			},
			loopableData:[
					{number: "२", topic: "भवनको लम्बाई"},
					{number: "३", topic: "भवनको चौडाई"},
					{number: "४", topic: "प्लिन्थ एरिया"},
					{number: "५", topic: "ग्राउण्ड कभरेज"},
					{number: "६", topic: "FAR"},
					{number: "८", topic: "ROW"},
					{number: "९", topic: "Setback"},
					{number: "१०", topic: "विद्युत लाईन ‘‘।। के।भी।"},
					{number: "११", topic: "खोलार खोल्सी रकुलो  किनार"},
					{number: "१२", topic: "अन्य"},
				]
		},
		signature: {
			nakshawala: 'नक्साबाला वा निजको बारेसको',
			field: 'सब. ईन्जिनियर',
			sifarish: 'ईन्जिनियर',
			suikrit: 'स्ट्रक्चरल ईन्जिनियर',
			footer_chief: 'प्रमुख प्रशासकीय अधिकृत',
		},
	},
};



export const superstructureConstructionKamalamai = {
	kamalamaiData: {
		title_1: 'दोस्रो चरणको ईजाजत पत्र',
		title_2: '(प्लिन्थ लेभल माथि सुपरस्ट्रक्चरको लागि)',
		date: 'मिति :',
		data_1: 'श्री ',
		data_2: 'उपरोक्त विषयमा तपाईले यस नगरपालिका वडा नं. ',
		data_3: ' अन्तर्गत ',
		data_4: ' टोलमा ',
		data_5: ' रहेको कि नं. ',
		data_6: ' क्षेत्रफल ',
		data_7: ' जग्गामा नक्सा बमोजिम मिति ',
		data_8:
			' मा प्रथम चरणको स्वीकृति प्रदान गरिसकेको हुदा यसै साथ संलग्न न.पा. को सम्पूर्ण नियम कानून पालना गर्ने शर्तहरु,वस्ती विकास/सहरी योजना तथा भवन निर्माण २०७२ र स्वीकृत नक्सा वमोजिम निर्माण कार्य गर्न स्थानीय स्वायत्त शासन ऐन,२०५५ दफा १५५ बमोजिम यो भवन निर्माणको दास्रो चरणको इजाजत पत्र प्रदान गरिएको छ । निर्माण कार्य सम्पन्न भएपछि ३५ (पैतिस) दिनभित्र "निर्माण सम्पन्न प्रमाण पत्र" अनिवार्य रुपमा लिनु पर्नेछ ।',
		body: {
			list1: '१. भवन निर्माणको किसिम : ',
			checkBox_option: {
				option_1: 'नयाँ निर्माण',
				option_2: 'तला थप',
				option_3: 'पुरानो घर',
			},
			list2: '२) बन्ने घरको लम्बाई ',
			chaudai: ' चौडाई ',
			tala: ' तला ',
			uchai: ' कुल उचाई ',
			area: ' कुल एरिया ',
			list3: '३) भवनको प्रयोजन ',
			checkBox_option2: {
				option_1: 'आवासीय',
				option_2: 'व्यापारिक',
				option_3: 'अन्य',
			},
			list4: '४) बन्ने घरको किसिम ',
			list8: '५) निर्माण को लागि इजाजत प्रदान गरिएको तला ',
			list9: '६) अन्य',
		},
		footer_1: 'फिल्ड निरीक्षण गरि पेश गर्ने',
		footer_2: 'सिफारिस गर्ने',
		footer_3: 'स्वीकृत गर्ने',
		footer_chief: 'प्रमुख प्रशासकीय अधिकृत',
		footer_4:
			'पास भएको नक्सा विपरीत कार्य गरेमा वा यसमा उल्लेखित शर्तहरुको बर्खिलाप अन्य कुनै कार्य गरेमा प्रचलित कानून बमोजिम कारवाही भएमा मलाई मान्य हुन्छ भनी सहीछाप गर्ने ।',
		footer_5: 'नक्सावालाका वा निजको वारेसको',
	},
};

export const structureDesignClassB = {
	structureDesignClassB_data: {
		heading: {
			heading_1: 'प्राबिधिक विवरण फारामहरू (ख) स्ट्रक्चरल डिजाईन सम्बन्धि',
			heading_2: '( सम्बन्धित प्राबिधिक वा परामर्शदाताबाट भराउनु पर्ने )',
			heading_3: 'Checklist for NBC 000:1994 to NBC 114:1994 Professionally Engineered Building (Class "B")',
			heading_4: 'Structural Design Description (In case of many units, fill up the form for main unit',
		},
		table: {
			heading: {
				heading_1: 'S.N.',
				heading_2: 'Description',
				heading_3: 'As per Submitted Design',
				heading_4: 'Remarks',
			},
			table1: {
				heading: '1. General',
				section: {
					section1: {
						description: 'Number of Storey',
					},
					section2: {
						description: 'Total height of structure',
					},
					section3: {
						description: 'Structure System',
						input: {
							checkbox: ['Frame', 'Load bearing', 'Other'],
						},
					},
					section4: {
						description: 'If Computer Aided Design (CAD) is used, please state the name of the package',
					},
					section5: {
						description_1: 'a) Provision for future extension',
						description_2: 'b) If Yes- How many floors will be extended?',
						description_3: 'c) Structural design consideration for future extension',
						radio: ['Yes', 'No'],
					},
					section6: {
						description: 'functional use of building',
					},
					section7: {
						description: 'plinth area of building',
					},
				},
				heading_2: '2. Requirements of NEPAL NATIONAL BUILDING CODE (NBC)',
				sub_section: {
					cell_1: {
						heading: '2.1 NBC-1000000-1994 Requirments for state of the Art Design: An Introduction',
						description: 'Level of design',
						checkbox: [
							{ code: 'A', value: 'International State-of-the-art' },
							{ code: 'B', value: 'Professionaly Engineered Structures' },
							{ code: 'C', value: 'Mandatory Rule of thumb' },
							{ code: 'D', value: 'Guidelines to rural buildings' },
						],
					},
					cell_2: {
						heading: '2.2 NBC 101:1994 Materials Specifications',
						description: 'Tick the listed materials that will be used in the construction',
						checkbox: [
							'Cement',
							'Fine Aggregates (Sand)',
							'Natural building stones',
							'Tiles',
							'Metal Frames',
							'Coarse Aggregates',
							'Building Lime',
							'Bricks',
							'Timber',
							'Structural Steel',
						],
						description_2: 'In what manner/way have you used NBC 101?',
					},
					cell_3: {
						heading: '2.3 NBC 102-1994 Unit weight of Materials',
						description: 'Where do you pal to apply NBC 102?',
						checkbox: ['Specifications', 'Design Calculation', 'Bill of Quantity'],
						description_1: 'Specify the design unit weight of materials',
						description_2: 'Steel',
						description_3: 'Brick',
						description_4: 'RCC',
						description_5: 'Brick Masonry',
					},
					note:
						'Note: * If any materials other than specified in NBC 102-1994 the designer should take responsibility that such materials are according to international standard',
				},
			},
			table2: {
				section1: {
					heading: '2.4 NBC 103-1994 Occupancy load (Imposed Load)',
					tableHead1: 'Proposed occuopancy type (Fill in only concerning occupancy type)',
					tableHead2: 'Occupancy Load',
					tableHead3: 'Uniformly Distributed (kN/m2)',
					tableHead4: 'Consentrated Load (kN)',
					radioBox: {
						radio1: 'For Residental Buildings',
						radio2: 'For Hotels, Hostels, Dormitories',
						radio3: 'For Educational Buildings',
						radio4: 'For Institutional Buildings',
						radio5: 'For Assembly Buildings',
						radio6: 'For Business and Office Buildings',
						radio7: 'Mercantile Buildings',
						radio8: 'Industrial Buildings',
						radio9: 'Storage Buildings',
					},
					tbody: {
						tRow_1: {
							title: 'For Residental Buildings',
							td_1: 'Rooms and Kitchen',
							td_2: 'Corridor, staircase store',
							td_3: 'Balcony',
						},
						tRow_2: {
							title: 'For Hotels, Hostels, Dormitories',
							td_1: 'Living, Bed and dormitories',
							td_2: 'Kitchen, Corridors, Staircase',
							td_3: 'Store rooms',
							td_4: 'Dining, restaurants',
							td_5: 'Office rooms',
						},
						tRow_3: {
							title: 'For Educational Buildings',
							td_1: 'Class rooms, Dinning roooms',
							td_2: 'Kitchen',
							td_3: 'Stores',
							td_4: 'Libraries and archives',
							td_5: 'Balconies',
						},
						tRow_4: {
							title: 'For Institutional Buildings',
							td_1: 'Bed rooms, ward, dressing rooms',
							td_2: 'Kitchens',
							td_3: 'X-ray rooms, operating rooms',
							td_4: 'Corridors and staircase',
							td_5: 'Balconies',
						},
						tRow_5: {
							title: 'For Assembly Buildings',
							td_1: 'Assembly areas',
							td_2: 'Projection rooms',
							td_3: 'Stages',
							td_4: 'Corridors, Passage and Staircase',
							td_5: 'Balconies',
						},
						tRow_6: {
							title: 'For Business and Office Buildings',
							td_1: 'Rooms with seperate storage',
							td_2: 'Rooms without seperate storage',
							td_3: 'Fill rooms and storage rooms',
							td_4: 'Stair and passage',
							td_5: 'Balconies',
						},
						tRow_7: {
							title: 'Mercantile Buildings',
							td_1: 'Retails shops',
							td_2: 'Wholesale shop',
							td_3: 'Office',
							td_4: 'Staircase and passage',
							td_5: 'Balconies',
						},
						tRow_8: {
							title: 'Industrial Buildings',
							td_1: 'Work area without mahinary',
							td_2: 'With machinary, Light duty',
							td_3: 'Medium Duty',
							td_4: 'Heavy Duty',
							td_5: 'Boiler',
							td_6: 'Staircase Passage',
						},
						tRow_9: {
							title: 'Storage Buildings',
							td_1: 'Storgae rooms',
							td_2: 'Cold storage',
							td_3: 'Corridor and passage',
							td_4: 'Boiler rooms',
						},
					},
				},
			},
			table3: {
				section1: {
					heading: '2.5 NBC 104-1994 Wind load',
					description_1: 'Wind zone',
					description_2: 'Basic wind velocity',
				},
				section2: {
					heading: '2.6 NBC 105-1994 Seismic Design of buildings in Nepal',
					description_1: 'Method of earthquake analysis',
					description_2: 'Subsoil category',
					description_3: 'Fundamental transaction period',
					description_4: 'Basic seismic coefficient',
					description_5: 'Seismic zoning factor',
					description_6: 'Importance factor',
					description_7: 'Structural performance factor',
					checkbox: ['Seismic coefficient method', 'Model Response Spectrum method', 'Other'],
				},
				section3: {
					heading: '2.7 NBC 106: 1994 Snow load',
					description_1: 'Snowfall area',
					description_2: 'Elevation',
					description_3: 'Design Depth',
					description_4: 'Design Density',
					checkbox: ['Personal', 'Occasional', 'No snowfall'],
				},
				section4: {
					heading: '2.8 NBC 107: 1994 Provisional Recommendation Fire Safety',
					description_1: 'Where do you plan to apply the fire safety requirments specified in NBC 107 and NBC 206-1994?',
					checkbox: ['Specifications', 'Design Calculation', 'Bill of Quantity'],
				},
				section5: {
					heading: '2.9 NBC 108: 1994 Site Consideration for Seismic Hazards',
					description_1: 'Distance from toe/beginning of downward',
					description_2: 'Distance from river bank',
					description_3: 'Soil type in footing',
					description_4: 'Adopted safe bearing capacity',
					description_5: 'Type of foundation',
					description_6: 'Depth of foundation',
					description_7: 'Soil test report available?',
					radiobox: ['Yes', 'No'],
				},
				note:
					'Note: Soil test is available for all professional engineered structures. In case, soil test is not carried out, the designer should take responsibility for assumed data concering site consideration.',
			},
			table4: {
				section: {
					thead1: 'Site Conditions of Neighboring Houses',
					thead2: 'Description',
					thead3: 'Remarks',
					tbody: {
						td_1: 'East',
						td_2: 'West',
						td_3: 'North',
						td_4: 'South',
						checkbox: {
							checkbox_1: 'Road',
							checkbox_2: 'Land',
							checkbox_3: {
								line_1: 'Building with',
								line_2: 'structure',
								line_3: 'floors',
							},
						},
					},
				},
			},
			table5: {
				section1: {
					heading: '2.10 NBC 109 : 1994 Masonry : Unreinforced',
					description_1: 'Concrete Grade',
					description_2: 'Brick crushing strength',
					description_3: 'Mortar ration for load bearing masonry',
					sub_section: {
						thead_1: 'Floor',
						thead_2: 'Wall height',
						thead_3: 'Wall thickness',
						thead_4: 'Maximum Length',
						td_cell_1: 'Ground floor',
						td_cell_2: 'First floor',
						td_cell_3: 'Second floor',
					},
					description_4: 'Last distance from inside corner Does the total length of opening in any wall exceed 50% of its length',
					description_5: 'Does the horizontal distance between any two opening less than 600 mm or 1/2 of height of sorter opening',
					description_6: 'Does the Verticle distance between two opening less than 600 mm or 1/2 of width of smaller opening',
					description_7: 'If any of above mentioned cases do not comply, do you have provision for strengthening around opening?',
					description_8: 'Bands provided',
					checkbox: {
						checkbox_1: ['Yes', 'No'],
						checkbox_2: ['Plinth level', 'Lintel level', 'Roof level', 'Gable band'],
					},
					sub_section2: {
						heading: 'Verticle steel reinforcement diameters at corner/tee joints',
						td_cell_1: 'Ground floor',
						td_cell_2: 'First floor',
						td_cell_3: 'Second floor',
					},
					description_9: 'C/C distance of corner/tee strengthening Horizontal dowel bars',
				},
				section2: {
					heading: '2.11 NBC 110:1994 Plain and Reinforced Concrete',
					description_1: 'Concrete grade',
					description_2: 'Reinforcement Steel Grade',
					description_3: 'Critical size of slab panel',
					description_4: 'Calculated short span to effective depth ratio (L/d) for corresponding slab',
					description_5: 'Permission L/d ratio',
					description_6: 'Effective depth',
					description_7: 'Basic value of L/d',
					description_8: 'Span correction factor',
					description_9: 'Tension reinforcement (Ast) Percent',
					description_10: 'Ast modification factor',
					description_11: 'Compression reinforcement modification factor',
					sub_section: {
						thead_1: 'Beam characteristics',
						thead_2: 'Conditions of beams',
						thead_3: 'Canti lever',
						thead_4: 'Simply supported',
						thead_5: 'One side continuous',
						thead_6: 'Both side continuous',
						td_cell_1: 'Maximum span/depth ration',
						td_cell_2: 'Span of corresponding team',
						td_cell_3: 'Depth of corresponding team',
						td_cell_4: 'Width of corresponding team',
					},
					descriptio_12: 'Maximum slenderness ration of column Lateral dimension of corresponding column',
					descriptio_13: 'Design Philosophy',
					radiobox: ['Limit state method', 'Working stress method', 'Unlimited strength method'],
					sub_section2: {
						heading: 'Loada combinations',
					},
					descriptio_14: 'Value of Horizontal Seismic Bse Shear (At least One Frame) Submit Calculation',
					descriptio_15: 'Submit Foundation Design (at least one type)',
				},
				section3: {
					heading: '2.12 NBC : 111-1994 Steel',
					description_1: 'Design assumption',
					checkbox: ['Simple connection', 'Semi-rigid connection', 'Fully rigid connection'],
					description_2: 'Yield Stress',
					description_3: 'Least wall thickness',
					sub_section: {
						thead_1: 'Expose condition',
						thead_2: 'Pipe',
						thead_3: 'Web of Standard size',
						thead_4: 'Composed section',
					},
					description_4: 'For Exposed Section',
					description_5: 'For not exposed section',
					description_6: 'Have you used Truss?',
					radiobox: ['Yes', 'No'],
					description_7: 'What is the critical span of purlin size',
					description_8: 'Have you used steel post?',
					description_9: 'Stenderness ratio of the critical post',
				},
				section4: {
					heading: '2.13 NBC : 112 Timber',
					description_1: 'Name of structural wood',
					description_2: 'Modules of Elasticity',
					description_3: 'Critical span of the beam element designed deflection',
					description_4: 'Slenderness ration of the critical post',
					description_5: 'Joint type',
				},
				section5: {
					heading: '2.14 NBC : 113 : 1994 Alumunium',
					description_1: 'Have you used alumunium as structure member?',
					description_2: 'If yes, please mention the name of design cope.',
					radiobox: ['Yes', 'No'],
				},
				section6: {
					heading: '2.15 NBC : 114 : 1994 Construction safety',
					description_1: 'Are you sure that all safety measure will be fulfilled in the construction site as per this code',
					description_2: 'Safety wares use',
					radiobox: ['Yes', 'No'],
					checkbox: ['Safety hard that', 'Safety goggles', 'Safety boots', 'Safety belt', 'First aid facility'],
				},
				note:
					'Note: Submission of detail design analysis and calculation will be highly appricated for residental building. For other type of important buildings like commercial complexes, educational & institutional buildings hotels, hostels, assembly & office building with mass public movement submission of bried dsign report (which should include base share calculation. Load combination, frame analysis and sample design of foundation, columns,beam etc.) is compulsion. Besides design report submission of brief report on planning permit will be highly appriciated.',
			},
		},
	},
};

export const dosrocharansupervisor = {
	dosrocharansupervisor_data: {
		heading_1: '(प्राबिधिक परामर्शदाताको प्रयोजनका लागि)',
		heading_2: 'सुपरस्ट्रक्चरको निर्माण कार्य सम्पन्न बारे घरधनीको तर्फबाट राखिएको सुपरीविक्षेक प्रतिवेदन ',

		sub_1: {
			sub_1_1: 'श्री ',
			sub_1_2: ' को ',
			sub_1_3: ' वडा न. ',
			sub_1_4: ' सडक ',
			sub_1_5: ' मा सुपरस्ट्रक्चरको निर्माणको कार्य सम्पन्न भएको हुनाले सो को प्रतिवेदन पेश गरेको छु | ',
		},
		data_1: {
			data_1_1: ' १. सडक अधिकार क्षेत्र सम्बन्धी मापदण्ड पालना भएको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			data_1_2: ' २. साइत प्लानमा देखाइए बमोजिम सेट व्याक पालना भएको छ कि छैन  ( छैन भने विवरण खुलाउने ):-',
			data_1_3: ' ३. ग्राउण्ड कभरेजमा फरक परेको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			data_1_4: '४. छज्जी (क्यानटीलिभर), बार्दली , बाहिरको सिंढी आदि निकालेको हकमा मापदण्डको पालना भएको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			data_1_5: '५. नेपाल राष्ट्रिय भवन निर्माण सहिंता २०६० अनुसार निर्माण भएको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			data_1_6: '६. कुनै पनि फेरबदल भएको भए सो को विवरण खुलाउने (आवश्यक भएमा अलग्गै कागजमा समेत खुलाउन सकिने ) :-',
		},
		subinfo: {
			subinfo_1:
				'उपर्युक्त्त भवनको निर्माणको सुपरीवेक्षण म आफैले गरेको हुनाले उक्त्त प्रतिवेदन पेश गरेको छु | अन्यथा भएमा नियमानुसार साहुला बुझाउँला',
		},

		info_1: {
			info_1_1: 'हस्ताक्षर :',
			info_1_2: 'प्राविधिक को नाम : ',
			info_1_3: 'नेपाल इन्जीनियरिङ्ग परिषद दर्ता न. (इन्जीनियर/आर्किटेक्टको लागि मात्र ) :',
			info_1_4: 'कन्सल्टीङ्ग  फर्म  भए सो नाम र छाप : ',
			info_1_5: 'मा दर्ता भएको व्यवसाय प्रमाण पत्रको न. : ',
			info_1_6: 'निर्माण कार्य नेपाल राष्ट्रिय भवन निर्माण सहिंता २०६० अनुसार भएको र अन्यथा नियमानुसार सहुँला बुझाउँला भनी सही गर्ने |',
			info_1_7: 'दस्तखत ',
		},
		thekdar: {
			thekdarinfo_1: 'निर्माण व्यवसाय',
			thekdarinfo_2: 'नाम :',
			thekdarinfo_3: 'ठेगाना :',
			thekdarinfo_4: 'न. पा.दर्ता प्रमाण पत्र न.',
			thekdarinfo_5: 'मिति ',
		},
		mukhya: {
			mukhya_1: 'दस्तखत.',
			mukhya_2: 'मुख्य डकर्मी',
			mukhya_3: 'नाम :',
			mukhya_4: 'ठेगाना :',
			mukhya_5: 'न. पा.दर्ता प्रमाण पत्र न.',
			mukhya_6: 'मिति ',
		},
		gharDhani: {
			gharDhani: 'घरधनी',
		},
		radioBox: ['छ ', 'छैन'],
		plor_else_placeholder: '(छैन भने विवरण खुलाउने)',
		sansodhan_check: [' छैन', 'संसोधन छ', 'तला थप छ'],
		sansodhan_checklabel: 'संसोधन/तला थप गर्नु पर्ने छ/छैन ?',
		sundarData: {
			heading: 'निर्माण कार्य सम्पन्न बारे घरधनीको तर्फबाट राखिएको सुपरीविक्षेक प्रतिवेदन',
		},
	},
};

export const NirmanKarya = {
	NirmanKarya_data: {
		heading: {
			heading_1: 'आवेदन  फारम',
			Subject: 'बिषय : निर्माण कार्य सम्पन्न प्रमाण पत्र बारे| ',
			SubjectSundar: 'निर्माण कार्य सम्पन्न तथा धरौटी फिता बारे |',
		},
		data_1: {
			data_1_1: 'महोदय,',
		},
		data_2: {
			data_2_1: {
				data_2_1_1: 'उपर्युक्त सम्बन्धमा ',
				data_2_1_2: 'को च.नं.',
			},
			data_2_2: ' मिति ',
			data_2_3: ' गते ',
			data_2_4: ' घरको ',
			data_2_5: ' सुपरस्ट्रक्चर',
			data_2_6: ' कम्पाउण्डवाल',
			data_2_7: ' निर्माण गर्ने ',
			data_2_8: ' मैले ',
			data_2_9: ' हामीले ',
			data_2_10: ' को  क्षेत्रफ़ल ',
			data_2_11: ' भित्र न.पा .बाट स्वीकृत नक्सा तथा मापदण्ड र नेपाल राष्ट्रिय भवन निर्माण सहित २०६० अनुसार निर्माण कार्य सम्पन्न गरेको',
			data_2_12: ' छु | ',
			data_2_13: ' छौं | ',
			data_2_14:
				'अतः निर्माण कार्यमा संलग्न प्राविधिक प्रतिवेदन संलग्न राखी न.पा .बाट जे बुज्नु पर्ने बुझी निर्माण कार्य सम्पन्न को प्रमाण पत्र पाउं भनि यो निवेदन पेश गरेको',
			data_2_ms: {
				maile: 'मैले',
				hamile: 'हामीले',
				chu: ' छु | ',
				chaun: ' छौं | ',
			},
		},
		footer_1: {
			footer_1_1: 'निवेदकको नाम:',
			footer_1_2: 'ठेगाना:',
			footer_1_3: 'दस्तखत:',
			wardNo: 'वडा नं.',
		},
		footer_2: {
			footer_2_1: 'वारेशको नाम:',
			footer_2_2: 'ठेगाना:',
			footer_2_3: 'दस्तखत:',
			footer_2_4: 'मिति:',
		},
		sundarData: {
			data1: ' निर्माण इजाजत अनुसार मैले साबिक ',
			data2: 'च.नं.',
			data3: ' को ',
			data4:
				'भित्र स्वीकृत नक्सा अनुसार परिवर्तन भएकोले संसोधित नक्सा अनुसार निर्माण कार्य सम्पन्न गरी प्राविधिक सुपरीवेक्षकको प्रतिवेदन सहित निर्माण सम्पन्न प्रमाण-पत्र पाउ भनी यो निवेदन पेश',
			data5: 'प.स. :-',
			data6: ' दर्ता नं.:',
			data7: ' दर्ता मिति :',
			data8: 'निवेदक',
		},
	},
	NirmanKarya_Kageyshowri_data: {
		
		heading: {
			heading_1: 'आवेदन  फारम',
			Subject: 'बिषय : निर्माण सम्पन्न प्रमाण–पत्र पाउँ| ',
			SubjectSundar: 'विषयः निर्माण सम्पन्न प्रमाण–पत्र पाउँ । ',
		},
		mainContent: {
			content1 : "यस",
			content1_2: "नगरपालिका वडा नं.",
			content2: "सडक/बाटोसँग सीमानासँग जोडिएको",
			content3: "नाउँमा रहेको उक्त जग्गामा घर/टहरा/पर्खाल निर्माण गर्नका लागि मिति ",
			content4: "मा यस कार्यालयबाट स्वीकृत लिएको हाल उक्त सुपरस्ट्रक्चरको निर्माण कार्य स्वीकृत नक्शा, डिजाईन र नगरपालिकाको मापदण्ड बमोजिम निर्माण सम्पन्न भएको हुँदा निर्माण सम्पन्न प्रमाण–पत्र पाउँ भनि यो निवेदन पेश गरेको छु ।",
			tol: "टोल",
			kittaNo: "कि.नं.",
			chhetrafal: "क्षेत्रफल",
			bimiBhayeko: "व.मी. भएको",
			nibedhaknam : "निवेदकको नामथरः",
			barisnam : "वारेशको नामथरः",
			thegana : "ठेगानाः",
			dartakhata : "दस्तखतः",
			mobile : "मोबाईल नं.:",
			email : "ईमेल:",

		},
		listData: {
			heading : "आवश्यक कागजात–पत्रहरुः",
			data : ["१) नागरिकताको फोटोकपि –१ प्रति", "२) जग्गाधनीको लालपुर्जा  फोटोकपि – १ प्रति", "३) जग्गाको ब्लु प्रिन्ट नक्शाको सट्टामा दिईएको कम्प्युटर प्रिन्ट नक्शा  – १ प्रति", "४) चालु आ.ब. सम्म तिरेको मालपोत रसिदको फोटोकपि  – १ प्रति", "५) स्वीकृत फोटोकपि  – १ प्रति", "६) स्वीकृत नक्शाको फोटोकपि  – १ प्रति", "७) घरको चार साईडबाट देखि रंगित फोटो"]
		},
		footer_1: {
			footer_1_1: 'निवेदकको नाम:',
			footer_1_2: 'ठेगाना:',
			footer_1_3: 'दस्तखत:',
			wardNo: 'वडा नं.',
		},
		data_1: {
			data_1_1: 'महोदय,',
		},
		data_2: {
			data_2_1: {
				data_2_1_1: 'उपर्युक्त सम्बन्धमा ',
				data_2_1_2: 'को च.नं.',
			},
			data_2_2: ' मिति ',
			data_2_3: ' गते ',
			data_2_4: ' घरको ',
			data_2_5: ' सुपरस्ट्रक्चर',
			data_2_6: ' कम्पाउण्डवाल',
			data_2_7: ' निर्माण गर्ने ',
			data_2_8: ' मैले ',
			data_2_9: ' हामीले ',
			data_2_10: ' को  क्षेत्रफ़ल ',
			data_2_11: ' भित्र न.पा .बाट स्वीकृत नक्सा तथा मापदण्ड र नेपाल राष्ट्रिय भवन निर्माण सहित २०६० अनुसार निर्माण कार्य सम्पन्न गरेको',
			data_2_12: ' छु | ',
			data_2_13: ' छौं | ',
			data_2_14:
				'अतः निर्माण कार्यमा संलग्न प्राविधिक प्रतिवेदन संलग्न राखी न.पा .बाट जे बुज्नु पर्ने बुझी निर्माण कार्य सम्पन्न को प्रमाण पत्र पाउं भनि यो निवेदन पेश गरेको',
			data_2_ms: {
				maile: 'मैले',
				hamile: 'हामीले',
				chu: ' छु | ',
				chaun: ' छौं | ',
			},
		},
		footer_2: {
			footer_2_1: 'वारेशको नाम:',
			footer_2_2: 'ठेगाना:',
			footer_2_3: 'दस्तखत:',
			footer_2_4: 'मिति:',
		},
		sundarData: {
			data1: ' निर्माण इजाजत अनुसार मैले साबिक ',
			data2: 'च.नं.',
			data3: ' को ',
			data4:
				'भित्र स्वीकृत नक्सा अनुसार परिवर्तन भएकोले संसोधित नक्सा अनुसार निर्माण कार्य सम्पन्न गरी प्राविधिक सुपरीवेक्षकको प्रतिवेदन सहित निर्माण सम्पन्न प्रमाण-पत्र पाउ भनी यो निवेदन पेश',
			data5: 'प.स. :-',
			data6: ' दर्ता नं.:',
			data7: ' दर्ता मिति :',
			data8: 'निवेदक',
		},
	},
};

export const olddosrocharanPrabidhikview = {
	dosrocharanPrabidhikview_data: {
		heading: {
			heading_1: '( कार्यालय प्रयोजनको लागि )',
			heading_2: 'सुपरस्ट्रक्चरको निर्माण कार्य सम्पन्न बारे स्थलगत निरीक्षण गर्ने न. पा. प्राबिधिकको राय  ',
		},
		radioOption: {
			opt_1: 'छ',
			opt_2: 'छैन',
		},
		subhead: {
			subhead_1: 'श्रीमान योजना प्रमुख ज्यु,',
		},
		content: {
			content_1: 'श्री ',
			content_2: ' ले वडा न. ',
			content_3: ' सडक ',
			content_4: ' मा सुपरस्ट्रक्चरको निर्माण कार्य सम्पन्न भएको हुनाले सो को प्रतिवेदन पेश गरेको छु | ',
		},
		subcontent: {
			subcontent_1: ' १. सडक अधिकार क्षेत्र सम्बन्धी मापदण्ड पालना भएको छ कि छैन (छैन भए विवरण खुलाउने ) :-',
			subcontent_2: ' २. साइट प्लानमा देखाइए बमोजिम सेट व्याक पालना भएको छ कि छैन (छैन भए विवरण खुलाउने ) :-',
			subcontent_3: ' ३. ग्राउण्ड कभरेजमा फरक परेको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			subcontent_4:
				' ४. छज्जी (क्यानटीलिभर), बार्दली , बाहिरको सिंडी आदि निकालेको हकमा मापदण्डको पालना भएको छ कि छैन ( छैन भने विवरण खुलाउने ):-',
			subcontent_5: ' ५. नेपाल राष्ट्रिय भवन निर्माण सहिंता २०६० अनुसार निर्माण भएको छ/छैन (छैन भए विवरण खुलाउने ) :-',
			subcontent_6: ' ६. कुनै पनि फेरबदल भएको भए सो को विवरण खुलाउने (आवश्यक भएमा अलग्गै कागजमा समेत खुलाउन सकिने ) :-',
		},

		condate: {
			condate_1: ' उपर्युक्त प्रतिवेदन मैले मिति : ',
			condate_2: ' मा स्थलगत निरीक्षण गरी पेश गरेको हुँ | ',
			condate_3: 'हस्तक्षर : ',
		},
		info1: {
			info_1: 'प्राविधिकको नाम :',
			info_2: ' शाखा : ',
		},
		info2: {
			info_1: ' पद : ',
			info_2: ' मिति : ',
		},
		dpv_else_placeholder: '(छैन भने विवरण खुलाउने)',
	},
	sundarData: {
		heading: 'निर्माण कार्य सम्पन्न बारे स्थलगत निरीक्षण गर्ने न. पा. प्राबिधिकको राय',
	},
	kageyshowriData: {
		heading: 'विषय: फिल्ड प्रतिवेदन पेश गरेको वारे ।',
		nepal: "नेपाल",
	}
};

export const GharNaksha = {
	GharNaksha_data: {
		firstheading: {
			heading_1: '"घर नक्साको  कबुलियतनामा"',
			date: 'मिति : ',
		},
		data_1: {
			data_1_1: 'लिखितम् श्री ',
			data_1_2: ' को ',
			data_1_3: ' श्री ',
			data_1_4: 'नाती/नातिनी/पत्नी',
			data_1_5: ' को ',
			data_1_6: 'छोरा/छोरी',
			data_1_7: ' जि.',
			data_1_8: ' मा जन्म भई हाल ',
			// data_1_9: 'उर्लाबारी',
			data_1_10: ' वार्ड नं ',
			data_1_11: ' बस्ने वर्ष ',
			data_1_12: ' का श्री ',
			data_1_13: ' आज्ञे तपशिल बमोजिमको पक्कि ',
			data_1_14:
				' तल्ले भवन/कम्पाउण्डवाल नक्शा पासको लागि मैले पेश गरेको नक्शा दर्खास्त अनुसार  न.पा.एन बमोजिमको कार्वाही भई सो नक्शा पास भई मिति ',
			data_1_14_1: '  ',
			data_1_15: ' मा ',
			// data_1_16: 'उर्लावारी',
			data_1_17: ' बाट भएको निर्माण बमोजिम मेरो चित्त बुझेकोले न.पा.ले दिएको ईजाजत पत्रमा उल्लेखित शर्त मुताविक पास भएको नक्शा बमोजिमको पक्कि ',
			data_1_18:
				' तल्ले भवन/कम्पाउण्डवाल  दुई(२) वर्ष भित्र बनाई तयार भए पछि जचाउँने  छु | सो  बमोजिमको नगरे वा कुनै कुरामा फरक पारि बनाएको समेत ठहरेमा स्थानीय स्वायत् शासन ऐन २०५५ र सो अन्तगतको नियम बमोजिम सहुला बुझाउँला |',
		},
		secondheading: {
			heading_1: 'तपसिल',
			heading_2: 'घर/कम्पाउण्डवाल बनिने जग्गाको चौहदिका सधियार:',
		},
		data_2: {
			data_2_1: 'पूर्व : ',
			data_2_2: 'पश्चिम : ',
			data_2_3: 'उत्तर : ',
			data_2_4: 'दक्षिण : ',
		},
		data_3: {
			data_3_1: 'घर/कम्पाउण्डवालको नाप : ',
			data_3_2: 'लम्बाई : ',
			data_3_3: 'चौडाई : ',
			data_3_4: 'उचाई : ',
			data_3_5: 'शौचालयको लम्बाई : ',
			data_3_6: 'चौडाई : ',
			data_3_7: 'उचाई : ',
			data_3_8: 'सेप्टिङ्ग लम्बाई : ',
			data_3_9: 'चौडाई : ',
			data_3_10: 'उचाई : ',
			data_3_11: 'कम्पाउण्डवाल लम्बाई : ',
			data_3_12: 'चौडाई : ',
			data_3_13: 'उचाई : ',
		},
		footer: {
			footer_1: 'इति सम्बत् ',
			footer_2: ' साल ',
			footer_3: ' गते ',
			footer_4: ' रोज ',
			footer_5: ' शुभम् |',
		},
	},
};

export const ElectricalDesign = {
	ElectricalDesign_Data: {
		heading: {
			heading_1: 'प्राविधिक विवरण फारमहरु ( ग ) इलेक्ट्रिकल डिजाईन सम्बन्धि',
			heading_2: '(सम्बन्धित प्राबिधिक वा परामर्शदाताबाट भराउनु पर्ने)',
			heading_3: 'Checklist for NBC 207 : 2003 - Electrical Design Requirements',
			heading_4:
				'(to be filled up only for selected buildings like Commercial Complexes, Educational/Institution Building, Industrial Buildings, Assemble Buildings, Hotels & other buildings having mass public movement for "Class A & B")',
		},
		footer: {
			footer_1: 'Note : ',
			footer_2:
				'1. When substantion and external electrical works are required, designer must comply NBC 207 : 2003 or/an relevent international electrical codes.',
			footer_3: '2. Designer is advised to consider lightning protection designated by international electrical codes.',
		},
	},
};

export const dosrocharanPrabidhikview = {
	dosrocharanPrabidhikview_data: {
		data0: ' मिति: ',
		data1: ' श्रीमान प्रमुख प्रसाशकिय अधिकृत ',
		data2: ' नगर कार्यपालिकाको कार्यालय ',
		data3: ' बिषय :- भवन निर्माण सम्पन्न प्रतिवेदन सम्बन्धमा ',
		data4: ' वडा न. ',
		data5: ' बस्ने श्री ',
		data6: ' ले श्री ',
		data7: ' को नाममा दर्ताकायम रहेको साविक ',
		data8: ' हाल ',
		data9: ' अन्तर्गत ',
		data10: ' टोल ',
		data11: ' मार्गमा पर्ने कि न. ',
		data12: ' ज. वि. ',
		data13: ' जग्गामा ',
		data14: ' निर्माण कार्य गर्न निबेदन पेश गर्नु भएकोमा स्थानीय सरकार स्ञ्चलन ऐन २०७४ को भवन निर्माण सम्बन्धि सम्पूर्ण प्रक्रिया पूरा भई मिति ',
		data15:
			' मा भवन निर्माण सम्पन्न प्रमाण-पत्र उपलव्ध गराईपाउन अनुरोध गरे बमोजिम कार्यलयबाट निर्माण कार्यको स्थलगत निरीक्षण गर्दा स्वीकृत नक्सा बमोजिम तपसिलमा उल्लेखित भए बमोजिमको निर्माण कार्यभएको व्योहराको प्रतिबेदन पेश गरेको छु | ',
		data16: ' प्रतिवेदन पेश गर्ने ',
		data17: ' नाम, थर : ',
		data18: ' पद : ',
		data19: ' दस्तखत : ',
		data20: ' निर्माण सम्पन्न विवरण ',
		data21: ' १. निर्माण सम्पन्न भवनको प्रकार ',
		data22: ' छानाको प्रकार ',
		data23: ' भवनको प्रयोजन ',
		data24: ' २. सडकको नाम ',
		data25: ' मापदण्ड ',
		data26: ' अधिकार क्षेत्र ',
		data27: ' केन्द्र रेखा छाडिएको दूरी ',
		data_three: '३. ',
		data28_0: '४.',
		data28: ' ४. निर्माण कार्यको जम्मा तला संख्या ',
		data28_1: ' निर्माण कार्यको जम्मा तला संख्या ',
		data29: ' जम्मा उचाई ',
		data30: ' जम्मा क्षेत्रफ़ल ',
		data31: ' वर्गफिट ',
		data32: ' ५. भवनको कूल कोठा संख्या ',
		data33: ' झ्याल संख्या ',
		data34: ' ढोका संख्या ',
		data35: ' सटर संख्या ',
		data36: ' ६. विधुत लाइनको दूरी ',
		data37: ' ७. नदी किनारको दूरी ',
		data38: ' ८. नेपाल राष्ट्रिय भवन निर्माण सहिंता २०६० अनुसार ',
		data39: ' कार्यसम्पन्न प्रमाण-पत्र उपलव्ध गराउन हुने/नहुने सम्बन्धमा उपशाखा/शाखाप्रमुखको राय ',
		data40: ['भएको', 'नभएको'],
		plor_else_placeholder: '(नभएको विवरण खुलाउने)',
	},
};
export const kankaiNoteView = {
	kankaiNoteView_data: {
		tip: ' टिप्पणी आदेश ',
		sub: ' विषय : घरको प्लिन्थ लेवलसम्म/कम्पाउण्ड वाल निर्माणका निमित  इजाजत प्रदान गर्ने ',
		subtopic: ' नक्सापास उपशाखा ',
		miti: ' मिति : ',
		data1: ' वडा न. ',
		data2: ' बस्ने श्री ',
		data3: ' ले ',
		data4: ' नाममा दर्ता रहेको साविक ',
		data5: ' हाल  ',
		data6: ' सडकमा पर्ने कित्ता न. ',
		data7: ' को क्षेत्रफ़ल ',
		data8: ' मा भवन निर्माण गर्ने स्वीकृति पाउ भनी स्थानीय स्वायत शासन ऐन २०५५को दफा १४८/१५०/१५१/ बमोजिम मिति ',
		data9:
			' मा नक्सा सहित आवश्यक प्रमाण राखी निवेदन दिनु भएकोमा सोही ऐनको दफा १५३ को खण्ड (क) बमोजिम १५ दिने सन्धी सपर्नको उजुरीबारे सूचना प्रकाशित गरिएकोमा ऐनको म्याद भित्र कसैको उजुरी नपरेको श्री ',
		data10: ' को मिति ',
		data11: ' मा उजुरी परी तत् सम्बन्धी छानविन भै सहमतीमा आएको हुँदा मिति ',
		data12:
			' मा निर्माण कार्य स्वीकृति दिंदा कसैको हानी नोक्सानी हुदैन भनी उल्लेख भई आएको/साथै प्राविधिकको  स्थलगत प्रतिवेदनमा समेत नक्सापास गरी ',
		data13: ' स्वीकृति दिन मिल्ने भन्ने मिति ',
		data14: ' मा भएको सजर्मिन मुचुल्कामा उल्लेख भै आएकोले दफा १५५ को १ बमोजिम इजाजत दिन दफा १५४ बमोजिम टिप्पणी लेखी निर्णयको लागि प्रेषित | ',
		data15: ' पेश गर्ने : ',
		data16: ' सही : ',
		data17: ' नाम : ',
		data18: ' पद : ',
		data19: ' संधियार ',
		data20: ' पूर्व ',
		data21: ' पश्चिम ',
		data22: ' उत्तर ',
		data23: ' दक्षिण  ',
		data24: ' सही गर्ने  ',
	},
};

export const LayoutGaripratibedan = {
	title: 'विषय : ले-आउट गरी प्रतिवेदन पेश गर्ने बारे ।',
	shree: ' श्री ',
	sakhaekai: 'शाखा/इकाई',
	miti: 'मिति: ',
	maindata: {
		data1: 'उपर्युक्त सम्बन्धमा ',
		data2: ' वडा नं ',
		data3: ' मा बस्ने श्री/श्रीमती/सुश्री ',
		data4: ' लाई साविक ',
		data5: ' हालको वडा नं ',
		data6: ' कि.न. ',
		data7: ' क्षेत्रफल ',
		data8: ' मा घर/कम्पाउण्डवाल निर्माणको लागि मिति ',
		data9: ' मा स्वीकृति दिईएकोले नियमअनुसार ले-आउट गरी फिल्ड प्रतिवेदन पेश गर्न हुन जानकारी गराईन्छ। ',
	},
};

export const nirmanKaryaGardaPalana = {
	heading: ' निर्माण कार्य गर्दा पालना गर्नु पर्ने शर्तहरु ',
	dataline1: ' १. सिमानाको पर्खाल गाह्रोको हक वा आ आफ्नो छुट्टा छुट्टै गर्नुपर्छ।	',
	dataline2: ' २. नक्शा लेखिएको भन्दा घटी बढी गरी बनाउनु हुदैन। ',
	dataline3: ' ३. कौशीको निकास तर्काउने डुड नराखी रेन वाटर पाईप राख्नु पर्दछ। ',
	dataline4:
		' ४. अकालाई दुर्गन्ध नआउने गरि आफ्नो हकको जग्गामा च्पी, तथा सेप्टिक टैक अनिवार्य रुपमा वनाउनु पर्दछ र सो स्थानमा ढलको व्यवस्था नभएको खण्डमा आवश्यकता अनुसार सोकपिट पनि बनाउनु पर्दछ। ',
	dataline5: ' ५. सार्वजनिक वा साझेदारी रुपमा छाडेको जग्गामा कायम वयेको बाटो र निकाश बन्द गर्नु हुदैन ।  ',
	dataline6:
		' ६. सडक, बाटो, ढल मंगल (Manhole), पाटीपैवा, देव देवालाय आदि सार्वजनिक स्थल र मनाही गरिएको ठाउको साथै आकांको जग्गा समेत मिच्नु हुदैन । ',
	dataline7:
		' ७. यस न. पा. वा अन्य सम्बन्धित निकायका कर्मचारीहरु जांच गर्न आउँदा जुनसुकै बखतमा पनि यो इजाजत पत्र तथा नक्सा र निर्माण भधैरहेको वा भैसकेको घर आदि अनिवार्य रुपमा देखाउनु पर्दछ। ',
	dataline8:
		' ८. निर्माण कार्य गर्दा बाटो, सडक, यातायात तथा मानिसहरु आवत जावत गर्नु असुविधा हुने गरि माटो इंटा, ढुङ्गा, बालुवा आदी निर्माण सामग्री थुपारी राख्नु हुँदैन। ',
	dataline9:
		' ९. नक्सा पास भएको मितिले दुई वर्ष भित्र निर्माण कार्य गरी सक्नु पर्दछ। सो अवधिभित्र निर्माण कार्य पूरा नभएमा नियमानुसार नविकरण दस्तुर तिरी अर्को दुईवर्षसम्मको म्याद थप गर्न सकिने छ सो म्याद भित्र पनि निर्माण कार्य पुरा नभएको अर्को नक्सा पास गरी मात्र बनाउनु पर्नेछ। ',
};
