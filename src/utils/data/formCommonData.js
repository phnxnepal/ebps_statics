export const patraChalani = {
	patraSankhya: 'प.सं.',
	chalaniSankyha: 'च.नं.',
	pasaFull: 'पत्र संख्या',
	canaFull: 'चलानी नं',
	gharNo: 'घर नं.',
	sanketNo: 'संकेत नं.'
};

export const date = {
	date: 'मिति: ',
};
