export const certificatePratibedanDetails = {
	space: ' ',
	chana: 'च. न.',
	pasa: 'प. स.',
	shri: 'श्री',
	date: 'मितिः',
	subject: 'बिषय: प्राबिधिक प्रतिबेदन पेश गर्ने सम्वन्धमा। ',
	uparokta_sambandhama_yash: 'उपरोक्त सम्बन्धमा यस ',
	wada_no: ' वडा नं. ',
	basne_shri: ' बस्ने श्री. ',
	le_niji : 'ले निजको नाममा दर्ता कायम रहेको साबिक ',
	haal: ' हाल ',
	kitta: ' मा पर्ने कि.न. ',
	chetrafal: ' जग्गा क्षेत्रफल ',
	jaggama_nirman: ' जग्गामा निर्माण भएको ',
	tale: ' तले पक्की घरको ',
	dataline:
		' सम्पन्न प्रमाणपत्र पाउँ भनि दिईएको माग निवेदन अनुसार उक्त घरको स्थलगत निरिक्षण गरि प्राबिधिक प्रतिबेदन पेश गर्नु हुन जानकारी गराईन्छ ।',
	engineer: ' इन्जिनियर  ',
};

export const superstructurePratibedanDetails = {
	space: ' ',
	chana: 'च. न.',
	pasa: 'प. स.',
	shri: 'श्री',
	date: 'मितिः',
	subject: 'बिषय: प्राबिधिक प्रतिबेदन पेश गर्ने सम्वन्धमा। ',
	uparokta_sambandhama_yash: 'उपरोक्त सम्बन्धमा यस ',
	wada_no: ' वडा नं. ',
	basne_shri: ' बस्ने श्री. ',
	le_niji : 'ले निजको नाममा दर्ता कायम रहेको साबिक ',
	haal: ' हाल ',
	kitta: ' मा पर्ने कि.न. ',
	chetrafal: ' जग्गा क्षेत्रफल ',
	jaggama_nirman: ' जग्गामा निर्माण भएको ',
	tale: ' तले पक्की घरको ',
	dataline:
		' superstrucure प्रमाणपत्र पाउँ भनि दिईएको माग निवेदन अनुसार उक्त घरको स्थलगत निरिक्षण गरि प्राबिधिक प्रतिबेदन पेश गर्नु हुन जानकारी गराईन्छ ।',
	engineer: ' इन्जिनियर  ',
};

