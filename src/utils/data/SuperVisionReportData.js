export const requiredDataRCCFrameStructure = {
	//here FS stands for Frame Structure

	//main header part
	superVisionReportFS: 'Supervision Report',
	onFS: 'on',
	rccFrameSructureBuildingFS: 'RCC Frame Structure Building',
	typeOfBuildingFS: '1. Type of the Building',
	levelOfCompletionFS: '2. Level of Completion of Building',

	//header 1
	wallFootingFS: 'A. Wall footing:',
	sizeOfWallFootingFS: 'a. Size of wall footing',
	mansoryTypeFS: 'b. masonry type:',

	//header 2
	plinthBeamFS: 'B. Plinth Beam:',
	sizeOfPlinthBeamFS: 'a. size of plinth beam',
	noAndSizeOfRebarUsedInThePlinthBeamFS: 'b. No and size of rebar used in the plinth beam',
	sizeAndSpacingOfStirrupsFS: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeFS: 'd. Concrete and rebar grade',

	//header 3
	starircaseFS: 'C. Staircase:',
	widthOfStairCaseFS: 'a. Width of staircase',
	widthOfLandingFS: 'b. With of landing',
	numberOfFlightFS: 'c. Number of flight',
	widthOfTreadAndHeightOfRiserFS: 'd. width of tread and height of Riser',
	sizeAndSpacingOfMainBarFS: 'e. Size and spacing of main bar',
	sizeAndSpacingOfDistributionBarFS: 'f. Size and spacing of distribution bar',
	sizeAndNumberOfRebarinZbeamFS: 'g. Size and number of rebar in Z-beam*',
	sizeAndNumberOfRebarinShortColumnFS: 'h. Size and number of rebar in Short column',

	//header 4
	wallInSuperStructureFS: 'D. Wall in Superstructure:',
	thicknessOfSillBandAndSizeOfmainBarFS: 'a. Thickness of sill band and size of main bar ',
	thicknessOfLintelbandAndSizeOfMainBarFS: 'b. Thickness of lintel band and size of main bar',
	sizeAndSpacingOfStirrupsInSillBandFS: 'c. size and spacing of stirrups in sill band',
	gradeOfConcreteFS: 'd. Grade of Concrete',

	//header 5
	floorBeamFS: 'E. Floor Beam:',
	sizeOfBeamFS: 'a. Size of beam',
	sizeAndNoOfRebarInBeamFS: 'b. size and no of rebar in beam',
	sizeAndSpacingOfStirrupsHeader5FS: 'c. size and spacing of stirrups',
	lapLengthAndLocationFS: 'd. Lap length and location',
	anchorageLengthInBeamColumnJunctionFS: 'e. Anchorage length in beam column junction',

	//header 6
	slabFS: 'F. Slab:',
	thicknessOfSlabFS: 'a Thickness of slab',
	sizeAndSpacingOfBottomRebarFS: 'b. Size and spacing of bottom rebar',
	sizeAndSpacingOfTopRebarFS: 'c. Size and spacing of top rebar',
	sizeAndSpacingOfChairFS: 'd. size and spacing of chair',

	//header 7

	photographsFS: 'G. Photographs',
	foundationTrenchFS: 'a. Foundation Trench and construction detail with formwork',
	reinforcementOfColumnAndFootingsFS: 'b. Reinforcement of column and footings',
	strapBeamFS: 'c. strap beam *',
	connectionDetailsOfFootingFS: 'd. connection details of footing and foundation beam',
	connectionDetailsOfColumnFS: 'e. connection detail of column and plinth beam',
	sillAndLintelBandsFS: 'f. sill and lintel bands in wall',
	beamColumnJunctionFS: 'g. beam column junction and cantilever',
	slabReinforcementFS: 'h. slab reinforcement and stab and beam junction',

	//header 8
	ifAnyFS: 'H. If any',
	clientFS: 'Client',
	nameFS: 'Name ',
	signatureFS: 'Signature',
	licenseHolderMasonFS: 'License holder Mason',
	supervisorEngineerFS: 'Supervisor Engineer',
};

export const requiredDataLoadBearingStructureBuilding = {
	//here LBS stands for Load Bearing Structure

	//header 1
	superVisionReportLBS: 'Supervision Report',
	onLBS: 'On',
	loadBearingStructureBuildingLBS: 'Load Bearing Structure Building',
	staircaseLBS: 'A. Staircase:',
	widthOfStairCaseLBS: 'a. Width of staircase',
	widthOfLandingLBS: 'b. Width of landing',
	numberOfFlightLBS: 'c. Number of flight',
	widthOfTreadAndHeightOfRiserLBS: 'd. width of tread and height of Riser',
	sizeAndSpacingOfMainBarLBS: 'e. Size and spacing of main bar',
	sizeAndSpacingOfDistributionBarLBS: 'f. Size and spacing of distribution bar',
	sizeAndNumberOfRebarInZbeamLBS: 'g. Size and number of rebar in Z- beam',
	sizeAndNumberOfRebarInShortColumnLBS: 'h. Size and number of rebar in Short column',

	//header 2
	wallInSuperStructureLBS: 'B. Wall in Superstructure:',
	thicknessAndTypesOfMasonryWallLBS: 'a. Thickness and types of masonry wall',
	thicknessOfSillBandAndSizeOfMainBarLBS: 'b. Thickness of sill band and size of main bar',
	thicknessOfLintelBandAndSizeOfMainBarLBS: 'c.Thickness of lintel band and size of main bar',
	sizeAndSpacingOfStirrupsInStillBandLBS: 'd. size and spacing of stirrups in sill band',
	gradeOfConcreteLBS: 'e grade of concrete',

	//header 3
	floorBandLBS: 'C. Floor Band:',
	sizeOfBandLBS: 'a. Size of band',
	sizeAndNoOfRebarInBandLBS: 'b. size and no of rebar in band ',
	sizeAndSpacingOfStirrupsLBS: 'c. size and spacing of stirrups',
	lapLengthAndLocationLBS: 'd. Lap length and location',
	anchorageLengthInBeamColumnJunctionLBS: 'e. Anchorage length in beam column junction',

	//header 4
	slabLBS: 'D. Slab:',
	thicknessOfSlabLBS: 'a. Thickness of slab',
	sizeAndSpacingOfBottomRebarLBS: 'b. Size and spacing of bottom rebar',
	sizeAndSpacingOfTopRebarLBS: 'c. Size and spacing of top rebar',
	sizeAndSpacingOfChairLBS: 'd. size and spacing of chair',

	//header 5
	roofLBS: 'E. Roof',
	typesOfRoofLBS: 'a. Types of roof',
	materialUsedInRoofLBS: 'b. Material used in roof',
	geometryOfRoofTrussLBS: 'c. Geometry of Roof truss',
	sizeAndGradeOfSteelUsedLBS: 'd. Size and grade of steel used in truss',
	ifAnyHeader5LBS: 'e. If any.',

	//header 6

	photographsLBS: 'F. Photographs',
	foundationTrenchLBS: 'a. Foundation Trench and construction detail with formwork',
	reinforcementOfFootingsLBS: 'b. Reinforcement of footings',
	strapBeamLBS: 'c. strap beam',
	connectionDetailsOfFootingLBS: 'd. connection details of footing and foundation beam',
	connectionDetailsOfFootingPlinthLBS: 'e. connection details of footing, plinth beam and superstructure wall',
	sillAndLintelBandsLBS: 'f. sill and lintel bands in wall',
	cornerAndTBandLBS: 'g. Corner and T band reinforcement and construction work',
	slabReinforcementLBS: 'h. Slab reinforcement and slab and band junction',

	//header 7
	ifAnyheader7: 'H. If any',
	clientLBS: 'Client',
	signatureLBS: 'Signature',
	NameLBS: 'Name:-',
	licenseHolderMasonLBS: 'License holder Mason',
	suprevisorEngineerLBS: 'Supervisor Engineer',
};
