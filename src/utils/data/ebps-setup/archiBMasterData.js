export const archiBMasterData = {
    table: {
        tableHeader: 'Architecture Design Class B Master',
        tableHeadings: {
            id: 'Id',
            buildingElements: 'Building Elements',
            groupId: 'Group Id',
            groupName: 'Group Name',
            unit: 'Unit',
            actions: 'Actions',
        }
    },
    modal: {
        titleAdd: 'Add Architecture Design Class B Master',
        titleEdit: 'Edit Architecture Design Class B Master',
        saveText: 'Save Architecture Design Class B Master',
        editText: 'Update Architecture Design Class B Master',
        deleteText: 'Delete Architecture Design Class B Master',
        delete: {
            title: 'Delete Architecture Design Class B Master',
            content: 'Are you sure you want to delete this row?'
        }
    }, 
    note: "बहु एकाइहरू थप्न, एकाइहरू ':' ले वर्गीकरण गर्नुहोस्। उदाहरण  'mm:feet'"
};

export const archiCMasterData = {
    archiCtable: {
        archiCHead: 'Architecture Design Class C Master',
        archiCtableHeadings: {
            id: 'Id',
            buildingElements: 'Building Elements',
            groupId: 'Group Id',
            groupName: 'Group Name',
            unit: 'Unit',
            actions: 'Actions',
        }
    },
    archiCmodal: {
        archiCAdd: 'Add Architecture Design Class C Master',
        archiCEdit: 'Edit Architecture Design Class C Master',
        archiCSave: 'Save Architecture Design Class C Master',
        archiCUpdate: 'Update Architecture Design Class C Master',
        archiCDelete: 'Delete Architecture Design Class C Master',
        archiCdeleteModal: {
            archiCDeletetitle: 'Delete Architecture Design Class C Master',
            archiCDeletecontent: 'Are you sure you want to delete this row?'
        }
    }
};

export const elecDesignData = {
    elecDesigntable: {
        elecDesignHead: 'Electrical Design Master',
        elecDesigntableHeadings: {
            id: 'Id',
            groupName: 'Group Name',
            elements: 'Electrical Elements',
            unit: 'Unit',
            actions: 'Actions',
        }
    },
    elecDesignmodal: {
        elecDesignAdd: 'Add Electrical Design Master',
        elecDesignEdit: 'Edit Electrical Design Master',
        elecDesignSave: 'Save Electrical Design Master',
        elecDesignUpdate: 'Update Electrical Design Master',
        elecDesignDelete: 'Delete Electrical Design Master',
        elecDesigndeleteModal: {
            elecDesignDeletetitle: 'Delete Electrical Design Master',
            elecDesignDeletecontent: 'Are you sure you want to delete this row?'
        }
    }
};

export const MapCheckData = {
    MapCheckTable: {
        header: 'Map Check Report Master',
        tableheader: {
            id: 'ID',
            sn: 'S.N',
            buildingDes: 'Building Description',
            strucType: 'Building Structural System',
            actions: 'Actions'
        }
    },
    MapCheckModal: {
        addMapCheck: 'Add Map Check Report Master',
        editMapCheck: 'Edit Map Check Report Master',
        saveMapCheck: 'Save Map Check Report Master',
        updateMapCheck: 'Update Map Check Report Master',
        deleteMapCheck: 'Delete Map Check Report Master',
        deleteMapCheckModal: {
            deleteMapCheckModaltitle: 'Delete Map Check Report Master',
            deleteMapCheckModalContent: 'Are you sure you want to delete this row?'
        }
    }
};

export const MasonListData = {
    MasonListTable: {
        header: 'Masons List',
        tableheader: {
            name: 'Name',
            address: 'Address',
            contact: 'Contact No.',
            regNo: 'Municipal Registration Number',
            status: 'Status',
            actions: 'Actions'
        }
    },
    MasonListModal: {
        addMasonList: 'Add Mason',
        editMasonList: 'Edit Mason',
        saveMasonList: 'Save Mason',
        updateMasonList: 'Update Mason',
        deleteMasonList: 'Delete Mason',
        deleteMasonListModal: {
            deleteMasonListModaltitle: 'Delete Mason',
            deleteMasonListModalContent: 'Are you sure you want to delete this row?'
        }
    }
};