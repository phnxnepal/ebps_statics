import { TableWidths } from '../../enums/ui';

export const namsariApplicationData = {
	notNamsaried: 'यो फाईलको नामसारी भएको छैन ।',
	namsariTitle: 'नामसारी विवरफ',
};

export const namsariApplicationTableData = [
	{
		text: 'शाखा दर्ता नं.',
		dataField: 'applicationNo',
	},
	{
		text: 'नासारि शाखा दर्ता नं.',
		dataField: 'applicantNo',
	},
	{
		text: 'जग्गाधनीको नाम',
		dataField: 'applicantName',
	},
	{
		text: 'जग्गाधनीको ठेगाना',
		dataField: 'applicantAddress',
	},
	{
		text: 'नामसारी भएको नाम',
		dataField: 'sasthaYaExtraName',
	},
	{
		text: 'नामसारी गर्ने',
		dataField: 'transaferEnterBy',
	},
	{
		text: 'नामसारी भएको मिति',
		dataField: 'transaferEnterDate',
	},
	{
		text: 'सम्पर्क फोन.',
		dataField: 'applicantMobileNo',
	},
];

export const namsariData = {
	modal: {
		title: 'नामसारीको लागी पठाउनुहोस्',
		content: 'के तपाईं यस फाईललाई नामसारीको लागि पठाउन चाहानुहुन्छ?',
		saveText: 'हुन्छ',
		confirmChange: 'नामसारीको लागी पठाउनुहोस्',
		historyNoData: 'नामसारीको लागि अझै कुनै फाईल पठाइएको छैन।',
		cancelText: 'रद्द गर्नुहोस्',
	},
	tab: {
		historyHeading: 'नामसारी History',
		setupHeading: 'नामसारी सेटप',
	},
	heading: 'नामसारी सेटप',
	namsariErrors: {
		namsariNotComplete: 'यस फाईलका लागि अघिल्लो नामसारी अझै पूर्ण भएको छैन ।',
	},
};

export const namsariColumns = [
	{
		text: 'शाखा दर्ता नं.',
		dataField: 'applicantNo',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.APPLICATION_NO,
		pinned: 'true',
	},
	{
		text: 'जग्गाधनीको नाम',
		dataField: 'applicantName',
		cellsalign: 'center',
		align: 'center',
		// width: '200',
		pinned: 'true',
	},

	{
		text: 'जग्गाधनीको ठेगाना',
		dataField: 'applicantAddress',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'निवेदकको नाम',
		dataField: 'nibedakName',
		cellsalign: 'center',
		align: 'center',
		// width: '200',
	},
	{
		text: 'सम्पर्क फोन.',
		dataField: 'applicantMobileNo',
		cellsalign: 'center',
		align: 'center',
	},
	{
		text: 'दर्ता मिति',
		dataField: 'applicantDate',
		cellsalign: 'center',
		align: 'center',
		width: '100',
	},
];

export const namsariDatafield = [
	{ name: 'applicantNo', type: 'string' },
	{ name: 'applicantName', type: 'string' },
	{ name: 'applicantAddress', type: 'string' },
	{ name: 'nibedakName', type: 'string' },
	{ name: 'yourStatus', type: 'string' },
	{ name: 'applicantDate', type: 'string' },
	{ name: 'applicantMobileNo', type: 'string' },
];

export const namsariHistory = [
	{
		text: 'शाखा दर्ता नं.',
		dataField: 'applicationNo',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.APPLICATION_NO,
		pinned: 'true',
	},
	{
		text: 'नासारि शाखा दर्ता नं.',
		dataField: 'applicantNo',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.NAMSARI_NO,
		pinned: 'true',
	},
	{
		text: 'जग्गाधनीको नाम',
		dataField: 'applicantName',
		cellsalign: 'center',
		align: 'center',
		pinned: 'true',
	},
	{
		text: 'जग्गाधनीको ठेगाना',
		dataField: 'applicantAddress',
		cellsalign: 'center',
		align: 'center',
	},
	{
		text: 'भवनको किसिम',
		dataField: 'constructionType',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.CONSTRUCTION_TYPE,
		pinned: 'true',
		filtertype: 'list',
	},
	{
		text: 'नामसारी गर्ने',
		dataField: 'transaferEnterBy',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'नामसारी भएको मिति',
		dataField: 'transaferEnterDate',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'सम्पर्क फोन.',
		dataField: 'applicantMobileNo',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.MOBILE_NO,
	},
];

export const historyDatafield = [
	{ name: 'applicationNo', type: 'string' },
	{ name: 'applicantNo', type: 'string' },
	{ name: 'applicantName', type: 'string' },
	{ name: 'applicantDate', type: 'string' },
	{ name: 'constructionType', type: 'string' },
	{ name: 'constructionType', type: 'string' },
	{ name: 'transaferEnterBy', type: 'string' },
	{ name: 'transaferEnterDate', type: 'string' },
	{ name: 'applicantAddress', type: 'string' },
	{ name: 'applicantMobileNo', type: 'string' },
];
