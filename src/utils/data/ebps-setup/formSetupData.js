export const classGroupData = {
	table: {
		tableHeader: 'Class Groups',
		tableHeadings: {
			formName: 'Form Name',
			id: 'Id',
			buildingClass: 'Building Class',
			constructionType: 'Construction Type',
			getValue: 'Keep form',
			actions: 'Actions',
		},
	},
	modal: {
		titleAdd: 'Add Class Group',
		titleEdit: 'Edit Class Group',
		saveText: 'Save Class Group',
		editText: 'Update Class Group',
		deleteText: 'Delete Class Group',
		delete: {
			title: 'Delete Class Group',
			content: 'Are you sure you want to delete this row?',
		},
	},
};

export const hasRevisedData = {
	table: {
		tableHeader: 'Has Revised Setup',
		tableHeadings: {
			id: 'Id',
			hasRevised17: 'प्राविधिक प्रतिवेदनमा संसोधन',
			hasRevised23: 'प्लिन्थ लेभलसम्मको प्रतिवेदनमा संसोधन',
			hasRevised29: 'सुपरस्ट्रक्चरको प्रतिवेदनमा संसोधन',
			hasRevised: 'संसोधन फारम',
			status: 'संसोधन स्थिति',
			statusTallaThap: 'संसोधन/तला थप स्थिति',
			ignoredForm: 'Form To Ignore',
			userType: 'User Type',
			actions: 'Actions',
		},
	},

	optionValues: {
		hasRevised17: 'प्राविधिक प्रतिवेदन',
		hasRevised23: 'प्लिन्थ लेभलसम्मको प्रतिवेदन',
		hasRevised29: 'सुपरस्ट्रक्चरको प्रतिवेदन',
	},

	modal: {
		titleAdd: 'Add Has Revised',
		titleEdit: 'Edit Has Revised',
		saveText: 'Save Has Revised',
		editText: 'Update Has Revised',
		deleteText: 'Delete Has Revised',
		delete: {
			title: 'Delete Has Revised',
			content: 'Are you sure you want to delete this row?',
		},
	},
};

export const menuSetupData = {
	table: {
		tableHeader: 'Menu Setup',
		tableHeadings: {
			id: 'Id',
			menu: 'Menu Name',
			menuType: 'Menu Type',
			url: 'Url',
			actions: 'Actions',
		},
	},

	modal: {
		titleAdd: 'Add Menu',
		titleEdit: 'Edit Menu',
		saveText: 'Save Menu',
		editText: 'Update Menu',
		deleteText: 'Delete Menu',
		delete: {
			title: 'Delete Menu',
			content: 'Are you sure you want to delete this row?',
		},
	},
};
export const menuAccessSetupData = {
	table: {
		tableHeader: 'Menu Access Setup',
		tableHeadings: {
			id: 'Id',
			menu: 'Menu Name',
			userType: 'User Type',
			menuType: 'Menu Type',
			status: 'Has Access',
			actions: 'Actions',
		},
	},

	modal: {
		titleAdd: 'Add Menu Access',
		titleEdit: 'Edit Menu Access',
		saveText: 'Save Menu Access',
		editText: 'Update Menu Access',
		deleteText: 'Delete Menu Access',
		delete: {
			title: 'Delete Menu Access',
			content: 'Are you sure you want to delete this row?',
		},
	},
};
export const userTypeSetupData = {
	table: {
		tableHeader: 'User Type Setup',
		tableHeadings: {
			id: 'Id',
			designation: 'Designation',
			designationNepali: 'Designation Nepali',
			hierarchy: 'Hierarchy',
			approveColumn: 'Approve Column',
			actions: 'Actions',
		},
	},

	modal: {
		titleAdd: 'Add User Type',
		titleEdit: 'Edit User Type',
		saveText: 'Save User Type',
		editText: 'Update User Type',
		deleteText: 'Delete User Type',
		delete: {
			title: 'Delete User Type',
			content: 'Are you sure you want to delete this row?',
		},
	},
};

export const rajasowSetupData = {
	dataOption: {
		floor: 'तला छान्नुहोस्',
		floorAll: 'सबै तला',
		floorType: 'तला	प्रकार छान्नुहोस्',
		floorTypeAll: 'सबै तला प्रकार',
		areaType: 'क्षेत्रफ़ल प्रकार छान्नुहोस्',
		areaTypeAll: 'सबै क्षेत्रफ़ल प्रकार',
		constructionType: 'भवनको प्रकार छान्नुहोस्',
		constructionTypeAll: 'सबै भवनको प्रकार',
		ward: 'वडा नं. छान्नुहोस्',
		wardAll: 'सबै वडा नं.',
	},
	table: {
		tableHeader: 'राजश्व सेटप',
		tableHeadings: {
			id: 'Id',
			floor: ' तला',
			floorType: 'तला प्रकार',
			wardNo: 'वडा नं.',
			area: 'क्षेत्रफ़ल प्रकार',
			dharautiRate: 'धरौटी  दर',
			dasturRate: 'दस्तुर दर',
			constructionType: 'भवनको प्रकार',
			actions: 'कार्य',
		},
	},

	modal: {
		titleAdd: 'राजश्व दर थप्नुहोस्',
		titleEdit: 'राजश्व दर अपडेट गर्नुहोस्',
		saveText: 'राजश्व दर थप्नुहोस्',
		editText: 'राजश्व दर अपडेट गर्नुहोस्',
		deleteText: 'राजश्व दर हटाउनुहोस्',
		cancelText: 'रद्द गर्नुहोस्',
		delete: {
			title: 'राजश्व दर हटाउनुहोस्',
			content: 'के तपाईं यो हटाउन चाहानुहुन्छ?',
		},
	},
};
export const wardSetupData = {
	enterWard: 'वडा नं.',
	table: {
		tableHeader: 'वडा सेटप',
		tableHeadings: {
			id: 'Id',
			ward: 'वडा नं.',
			actions: 'कार्य',
		},
	},
	modal: {
		titleAdd: 'वडा नं. थप्नुहोस्',
		titleEdit: 'वडा नं. अपडेट गर्नुहोस्',
		saveText: 'वडा नं. थप्नुहोस्',
		editText: 'वडा नं. अपडेट गर्नुहोस्',
		deleteText: 'वडा नं. नम्बर हटाउनुहोस्',
		cancelText: 'रद्द गर्नुहोस्',
		delete: {
			title: 'वडा नं. नम्बर हटाउनुहोस्',
			content: 'के तपाईं यो हटाउन चाहानुहुन्छ?',
		},
	},
};
