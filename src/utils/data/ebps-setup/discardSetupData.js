import { TableWidths } from '../../enums/ui';

export const discardData = {
    modal: {
        title: 'फाईल रद्द गर्नुहोस',
        content: 'के तपाईं यस फाईललाई रद्द गर्न चाहानुहुन्छ?',
        saveText: 'चाहन्छु',
        confirmChange: 'फाईल रद्द गर्नुहोस',
        historyNoData: 'रद्द गर्नको लागि अझै कुनै फाईल पठाइएको छैन।',
        cancelText: 'फाईललाई रद्द गर्न चाहन्न्',
    },
    tab: {
        historyHeading: ' रद्द गरिएको History',
        setupHeading: 'रद्द गर्नको सेटप',
    },
    heading: 'रद्द गर्नको सेटप',
    date: 'मिति: ',
    reason: 'रद्द गर्ने कारण: ',
    reasonFile: 'रद्द प्रक्रिया सम्बन्धित फाइल:',

    // discardErrors: {
    //     discardNotComplete: 'यस फाईलका लागि अघिल्लो नामसारी अझै पूर्ण भएको छैन ।',
    // },
};

export const discardHistory = [
    {
        text: 'शाखा दर्ता नं.',
        dataField: 'applicationNo',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_NO,
        pinned: 'true',
    },
    {
        text: 'जग्गाधनीको नाम',
        dataField: 'applicantName',
        cellsalign: 'center',
        align: 'center',
        pinned: 'true',
    },
    {
        text: 'जग्गाधनीको ठेगाना',
        dataField: 'applicantAddress',
        cellsalign: 'center',
        align: 'center',
    },
    {
        text: 'भवनको किसिम',
        dataField: 'constructionType',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.CONSTRUCTION_TYPE,
        pinned: 'true',
        filtertype: 'list',
    },
    {
        text: 'निवेदकको नाम ',
        dataField: 'nibedakName',
        cellsalign: 'center',
        align: 'center',
    },

    {
        text: 'रद्द गरिएको कारण',
        dataField: 'discardReason',
        cellsalign: 'center',
        align: 'center',
    },
    {
        text: 'रद्द गरिएको मिती',
        dataField: 'discardDate',
        cellsalign: 'center',
        align: 'center',
    },
    {
        text: 'दर्ता मिति',
        dataField: 'applicantDate',
        cellsalign: 'center',
        align: 'center',
        width: '100',
    },
];

export const historyDatafield = [
    { name: 'applicationNo', type: 'string' },
    { name: 'applicantName', type: 'string' },
    { name: 'applicantAddress', type: 'string' },
    { name: 'constructionType', type: 'string' },
    { name: 'nibedakName', type: 'string' },
    { name: 'discardReason', type: 'string' },
    { name: 'discardDate', type: 'string' },
    { name: 'applicantDate', type: 'string' }

];

export const discardYear = [
    { value: 2076, text: 2076 },
    { value: 2077, text: 2077 },
    { value: 2078, text: 2078 },
    { value: 2079, text: 2079 },
];
