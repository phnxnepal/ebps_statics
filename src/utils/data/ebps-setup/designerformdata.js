export const designer = {
    designer_data: {
        header: {
            header_1: 'डिजाइनर '
        },
        data_1: {
            data_1_1: 'नाम :',
            data_1_2: 'नाम:(अंग्रेजीमा)',
            data_1_3: 'ठेगाना',
            data_1_4: 'ठेगाना(अंग्रेजीमा)'
        },
        data_2: {
            data_2_1: 'कन्सल्टिङ फर्म',
            data_2_2: 'कन्सल्टिङ फर्म (अंग्रेजीमा)',
            data_2_3: 'इमेल',
            data_2_4: 'पास्वर्ड'
        },
        data_3: {
            data_3_1: 'पिता,पति,पत्नीको नाम',
            data_3_2: 'नागरिकता प्रमाण पत्र नं .',
            data_3_3: 'जारी जिल्ला',
            data_3_4: 'योग्यता'
        },
        data_4: {
            data_4_1: 'योग्यता(अंग्रेजीमा)',
            data_4_2: 'नेपाल इन्जिनियरिङ परिषद दर्ता नं .',
            data_4_3: 'फोन नम्बर ',
            data_4_4: 'मोबाईल नम्बर'
        },
        data_5: {
            data_5_1: 'न.पा. रजिस्ट्रेसन  नम्बर',
            data_5_2: 'न.पा. दर्ता मिती  :',
            data_5_3: 'म्याद सकिने मिती :',
        },
        second_header: {
            second_header_1: 'डिजाइनर सुची '
        },
        table: {
            table_1: 'क्र.स.',
            table_2: 'नाम ',
            table_3: 'नाम(अंग्रेजीमा)',
            table_4: 'योग्यता',
            table_5: 'नेपाल इन्जिनियरिङ परिषद दर्ता नं .',
            table_6: 'ठेगाना',
            table_7: 'ठेगाना(अंग्रेजीमा)',
            table_8: 'कन्सल्टिङ फर्म',
            table_9: 'कन्सल्टिङ फर्म (अंग्रेजीमा)',
            table_10: 'फोन',
            table_11: 'मोबाईल',
            table_12: 'न.पा. रजिस्ट्रेसन  नम्बर',
            table_13: 'मिती ',
            table_14: 'कार्य   '
        },
        renew: {
            previousRenewDate: 'पूर्व नवीकरण गरिएको मिति',
            previousRenewTillDate: 'यो मिति सम्म नवीकरण भएको छ',
            renewDate: 'नवीकरण गर्ने मिति',
            renewTillDate: 'नवीकरण गर्ने मिति (सम्म)',
            charge: 'दस्तुर भौचर',
            billNo: 'दस्तुर बिल नम्बर',
            fiscalYear: 'आर्थिक बर्ष',
        }

    }
}