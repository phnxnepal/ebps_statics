export const classGroupData = {
    table: {
        tableHeader: 'Class Groups',
        tableHeadings: {
            formName: 'Form Name',
            id: 'Id',
            buildingClass: 'Building Class',
            constructionType: 'Construction Type',
            getValue: 'Keep form',
            actions: 'Actions',
        }
    },
    modal: {
        titleAdd: 'Add Class Group',
        titleEdit: 'Edit Class Group',
        saveText: 'Save Class Group',
        editText: 'Update Class Group',
        deleteText: 'Delete Class Group',
        delete: {
            title: 'Delete Class Group',
            content: 'Are you sure you want to delete this row?'
        }
    }
};
