export const changeDesignerData = {
	applicationInfo: {
		applicationId: 'शाखा दर्ता नं.',
		applicantName: 'जग्गाधनीको नाम',
		applicantAddress: 'जग्गाधनीको ठेगाना',
		nibedakName: 'निवेदकको नाम',
		applicantMobileNo: 'सम्पर्क फोन',
		applicantDate: 'दर्ता मिती',
		constructionType: 'भवनको किसिम',
		applicationStatus: 'पूर्ण अवस्था',
		forwardTo: 'तह स्थिति',
		applicationActionBy: 'तह',
		yourStatus: 'अवस्था',
		wardNo: 'वडा',
		kittaNo: 'कित्ता नं',
		year: 'आर्थिक बर्ष',
		applicationNumb: 'दर्ता नं.',
		applicationInfo: 'फाईल जानकारी'
	},
	modal: {
		title: 'डिजाइनर परिवर्तन गर्नुहोस्',
		historyTitle: 'डिजाईनर परिवर्तन History',
		content: 'के तपाईं डिजाइनर परिवर्तन गर्न चाहानुहुन्छ?',
		saveText: 'हुन्छ',
		historyOkButton: 'Ok',
		confirmChange: 'डिजाइनर परिवर्तन गर्नुहोस्',
		historyNoData: 'यस फाईलको लागि डिजाइनर अझै परिवर्तन गरिएको छैन।',
		cancelText: 'रद्द गर्नुहोस्',
		select: 'डिजाइनर छान्नुहोस्',
		file: 'फाईल छान्नुहोस्',
	},
};
