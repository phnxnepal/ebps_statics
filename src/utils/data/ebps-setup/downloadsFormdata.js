export const downloads = {
    downloads_data: {
        heading: 'Add Downloads',
        data: {
            data_1: 'शीर्षक:',
            data_2: 'फाइल छान्नु होस् :',
            data_3: 'मिति',
        },
        existing: 'Existing File',
        downloads: 'Downloads Data',
        down_data: {
            data_1: 'शीर्षक:',
            data_2: 'फाइल',
            data_3: 'मिति',
            data_4: 'कार्य'
        }
    }
}
