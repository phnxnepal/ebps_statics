import { TableWidths } from "../../enums/ui";

export const tallaThapData = {
	applicationInfo: {
		applicationId: 'शाखा दर्ता नं.',
		applicantName: 'जग्गाधनीको नाम',
		applicantAddress: 'जग्गाधनीको ठेगाना',
		nibedakName: 'निवेदकको नाम',
		applicantMobileNo: 'सम्पर्क फोन',
		applicantDate: 'दर्ता मिती',
		constructionType: 'भवनको किसिम',
		applicationStatus: 'पूर्ण अवस्था',
		forwardTo: 'तह स्थिति',
		yourStatus: 'अवस्था',
		wardNo: 'वडा',
		kittaNo: 'कित्ता नं',
		year: 'आर्थिक बर्ष',
	},
	tallaThapInfo: {
		title: 'तला थप स्वीकृति',
		assignDesigner: 'डिजाइनर छान्नुहोस्',
		storeyHistory: 'तला थप History'
	},
	modal: {
		title: 'डिजाइनर छान्नुहोस्',
		content: 'के तपाईं यस फाईलको लागि डिजाइनर छान्न चाहानुहुन्छ?',
		saveText: 'हुन्छ',
		confirmChange: 'डिजाइनर छान्नुहोस्',
		historyNoData: 'कुनै पनि फाईल डिजाइनरलाई तोकिएको छैन',
		cancelText: 'रद्द गर्नुहोस्',
		selectDesigner: 'डिजाइनर छान्नुहोस् ',
		selectFile: 'फाईल छान्नुहोस्'
	},
};

export const tallaThapColumns = [
	{
		text: 'शाखा दर्ता नं.',
		dataField: 'applicantNo',
		cellsalign: 'center',
		align: 'center',
		width: TableWidths.APPLICATION_NO,
		pinned: 'true',
	},
	{
		text: 'जग्गाधनीको नाम',
		dataField: 'applicantName',
		cellsalign: 'center',
		align: 'center',
		// width: '200',
		pinned: 'true',
	},

	{
		text: 'जग्गाधनीको ठेगाना',
		dataField: 'applicantAddress',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'निवेदकको नाम',
		dataField: 'nibedakName',
		cellsalign: 'center',
		align: 'center',
		// width: '200',
	},
	{
		text: 'सम्पर्क फोन.',
		dataField: 'applicantMobileNo',
		cellsalign: 'center',
		align: 'center',
	},
	{
		text: 'दर्ता मिति',
		dataField: 'applicantDate',
		cellsalign: 'center',
		align: 'center',
		width: '100',
	},
];

export const tallaThapDatafield = [
	{ name: 'applicantNo', type: 'string' },
	{ name: 'applicantName', type: 'string' },
	{ name: 'applicantAddress', type: 'string' },
	{ name: 'nibedakName', type: 'string' },
	{ name: 'yourStatus', type: 'string' },
	{ name: 'applicantDate', type: 'string' },
	{ name: 'applicantMobileNo', type: 'string' },
];

export const tallaThapHistory = [
	{
		text: 'शाखा दर्ता नं.',
		dataField: 'applicantNo',
		cellsalign: 'center',
		align: 'center',
		width: '130',
		pinned: 'true',
	},
	{
		text: 'जग्गाधनीको नाम',
		dataField: 'applicantName',
		cellsalign: 'center',
		align: 'center',
		width: '200',
		pinned: 'true',
	},

	{
		text: 'Assigned By',
		dataField: 'talaThapAssignBy',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},

	{
		text: 'Assigned On',
		dataField: 'talaThapAssignDate',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'Assigned To',
		dataField: 'designer',
		cellsalign: 'center',
		align: 'center',
		width: '200',
	},
	{
		text: 'Status',
		dataField: 'talathapAssign',
		cellsalign: 'center',
		align: 'center',
	},
];

export const historyDatafield = [
	{ name: 'applicantNo', type: 'string' },
	{ name: 'applicantName', type: 'string' },
	{ name: 'talaThapAssignBy', type: 'string' },
	{ name: 'talaThapAssignDate', type: 'string' },
	{ name: 'designer', type: 'string' },
	{ name: 'talathapAssign', type: 'string' },
];
