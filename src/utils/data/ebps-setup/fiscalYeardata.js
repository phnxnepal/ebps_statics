export const fiscalYear = {
    fiscalYear_data: {
        heading: {
            heading_1: 'आर्थिक वर्षहरु',
            heading_2: 'आर्थिक वर्ष भर्नुहोस्',
            heading_3: 'आर्थिक वर्ष सेटप'
        },
        table_1: {
            table_1_1: 'कोड नं.',
            table_1_2: 'आर्थिक वर्ष',
            table_1_3: 'आर्थिक वर्ष सुरुवात',
            table_1_4: 'आर्थिक वर्ष अन्त',
            table_1_5: 'स्थिति '
        },
        modal: {
            message: 'के तपाईं यो हटाउन चाहानुहुन्छ?',
            acceptText: 'हुन्छ',
            cancelText: 'रद्द गर्नुहोस्',

        }
    }
}