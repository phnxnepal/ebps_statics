export const PahiloCharanBillVuktaniData = {
	heading: 'पहिलो चरण संशोधन बिल भुक्तानी',
	heading2: 'दोस्रो चरण संशोधन बिल भुक्तानी',
	content_1: 'बिल भुक्तानी रकम',
	content_2: 'रसिद न.',
	content_3: 'रकम बुझने',
	content_4: 'मिति​',
};

export const billVuktaniData = {
	pahiloCharanBill: 'पहिलो चरण संशोधन बिल भुक्तानी',
	sansodhanBill: 'संसोधन बिल भुक्तानी',
	namsariBill: 'नामसारी बिल भुक्तानी',
	dosroCharanBill: 'दोस्रो चरण संशोधन बिल भुक्तानी',
	vuktaniRakam: 'बिल भुक्तानी रकम',
	rashidNumber: 'रसिद न.',
	receiveSign: 'रकम बुझने',
	miti: 'मिति​',
};
