export const applicationTable = {
	applicationId: 'शाखा दर्ता नं.',
	applicantName: 'जग्गाधनीको नाम',
	applicantAddress: 'जग्गाधनीको ठेगाना',
	nibedakName: 'निवेदकको नाम',
	applicantMobileNo: 'सम्पर्क फोन',
	applicantDate: 'दर्ता मिती',
	constructionType: 'भवनको किसिम',
	applicationStatus: 'पूर्ण अवस्था',
	forwardTo: 'तह स्थिति',
	yourStatus: 'अवस्था',
	wardNo: 'वडा',
	kittaNo: 'कित्ता नं',
	year: 'आर्थिक बर्ष',
	applicationInfo: 'फाईल जानकारी'
};

export const activityTable = {
	api: 'API',
	method: 'API Method',
	dateFrom: 'Date From',
	dateTo: 'Date To',
	loginBy: 'Login By',
}

export const searchData = {
	search: 'खोज्नुहोस',
	searchApp: 'फाईल खोज्नुहोस',
	reset: 'खाली गर्नुहोस',
	searchResults: 'खोजीको परिणामहरू'
}
