export const dashboardLang = {
  dashboard_data: {
    datafetch: 'Fetching Permit Data...',
    infobarName: 'नक्सा पास तथा भवन निर्माण सुची',
    nayaDarta: 'नयाँ दर्ता',
    puranoDarta: 'पुरानो दर्ता',
    dakarmiSuchi: 'डकर्मीको सुचि',
    sakhaDarta: 'शाखा दर्ता नं.',
    jaggadhaniName: 'जग्गाधनीको नाम',
    jaggadhaniAdd: 'जग्गाधनीको ठेगाना',
    nibedak: 'निवेदकको नाम',
    contact: 'सम्पर्क फोन.',
    awastha: 'अवस्था',
    sthiti: 'स्थिति',
    oldSthiti: 'पुर्व अवस्था',
    sthitiForm: 'स्थिति फारम',
    dartaMiti: 'दर्ता मिती',
    buildingType: 'भवनको किसिम',
    tahaSthiti: 'तह स्थिति',
    applicationList: 'फाईल सुचि'
  },
  taskListData: {
    taskList: 'कार्य सुचि'
  }
};
