export const buildingFinishCertidicateData = {
	pasa: ' प. स. ',
	chana: ' च. न. ',
	miti: ' मिति: ',
	bisaye: ' नक्शा पास निर्माण कार्य सम्पन्न प्रमाण पत्र ',
	shir: ' श्री ',
	shrimati: ' श्रीमती ',
	shushri: ' सुश्री ',
	le: ' ले ', 
	wardNo: ' वडा न ',
	nimnaBamojim: ' मा निम्न बमोजिमको ',
	paritNaksha:
		' पारित नक्शा, भवन निर्माण सहिता २०६० र भवन तथा योजना मापदण्ड २०५६ बमोजिम निर्माण कार्य पुरा गरेकोले यो निर्माण कार्य सम्पन्न प्रमाण पत्र प्रदान गरिएको छ । ',

	oneJaggaBhani: ' १. जग्गा धनिको नाम, थर, वतन ',
	twoGharDhani: ' २. घर धनिको नाम, थर, वतन ',
	threeJaggaBibaran: ' ३. जग्गाको विवरण : ',
	sabik: ' साबिक ',
	halkoWadaNumber: ' हालको वडा न. ',
	gharNo: ' घर न. ',
	sadakKoNaam: ' सडकको नाम  ',
	kiNa: ' कि. न. ',
	chetrafal: ' क्षेत्रफ़ल ',
	bhuUpayogChetra: ' भू-उपयोग क्षेत्र ',

	fourJaggaKoCharKilla: ' ४. जग्गाको चारकिल्ला : ',
	purbaMa: ' पूर्वमा ',
	paschimMa: ' पश्चिममा ',
	dakshinMa: ' दक्षिणमा ',
	uttarMa: ' उत्तरमा ',

	fiveBhawanKoBargikaran: ' ५ भवनको वर्गीकरण ',
	ka: ' क ',
	kha: ' ख ',
	ga: ' ग ',
	gha: ' घ ',
	nirmanStructuralSystem: ' निर्माणको स्ट्रक्चरल सिस्टम ',

	sixNirmanKaryaIjajatPramanPatraNo: ' ६. क)  निर्माण कार्य इजाजत प्रमाण पत्र न. ',
	sixNakshaPassNagariBanayeko: ' ख) नक्शा पास नगरी बनाएको भए नियमित गरिएको मिति ',

	sevenTala: ' तला ',
	bhumiGatTala: ' क) भूमिगत वा अर्ध भूमिगत तला ',
	jaminTala: 'ख) जमिन तला ',
	pahiloTala: 'ग) पहिलो तला  ',
	dosroTala: 'घ) दोस्रो तला  ',
	tesroTala: 'ङ) तेस्रो तला  ',
	chauthoTala: 'च) चौथो तला  ',
	jammaChetrafal: 'जम्मा क्षेत्रफ़ल  ',

	swikritJaggaAnusar: ' स्वीकृत जग्गा अनुसार पाउने क्षेत्रफल ',
	nirmanBhayekoStiti: ' निर्माण भएको स्थिति  ',
	nirmanBhayekoMiti: ' निर्माण भएको मिति ',

	eightBhawanBanekoCoverArea: ' ८. भवन भनेको कभर एरिया :',
	groundCoverage: ' ग्राउन्ड कभरेग ',

	nineBanekoBhawanKoUchai: ' बनेको भवनको उचाई  ',
	nine: ' ९. ',
	tallaSankhya: ' तल्ला संख्या ',

	tenGharBanekoPlotSangaJodiyekoChodnuParneDuri: '१०. घर बनेको प्लटसंग जोडिएको वा प्रस्तावित सडकको लागी केन्द्ररेखबाट न्युनतम छोड्नुपर्ने दुरी ',
	sadakKoBichBataChodiyekoDuri: ' सडक बीचबाट छोडिएको दुरी ',

	elevenBijuliKoTarNajik: ' ११. बिजुलीको तार नजिक भएमा छोड्नुपर्ने दुरी ',
	BijuliChodekoDuri: ' छोडेको दुरी ',

	twelveNadiKuloKahareKoKinarBhayemaChodnuParneDuri: ' १२. नदि, कुलो, खहरेको किनार भएमा छोड्नुपर्ने दुरी ',
	nadiChodekoDuri: ' छोडेको दुरी ',

	thirteenDhalNikas: ' १३. ढल निकास सम्बन्धि ढल, सेप्टिक टैंक, सोकपिट भए सो को विवरण ',
	dhalNikasChodekoDuri: ' छोडेको दुरी ',
	fourteenAanyaKunai: ' १४. अन्य कुनै भए विवरण ',
	fifteenGharBanekoAawa: '१५. घरको बनेको आ. व.',
	sixteenSampannaIjajatNa: ' १६. सम्पन्न इजाजत न ',

	gharDhaniKoSahi: ' घरधनीको सहि ',
	phatWalaKoSahi: ' फाटवालाको सहि ',

	sthalgatNirikxak: ' स्थलगत निरिक्षक ',
	subEngineer: ' सव इन्जिनीयर ',
	jachGarneEngineer: ' जाच गर्ने इन्जिनियर ',
	gharNakshaPramukhSakha: ' घर नक्शा साखा प्रमुख ',
	pramanPatraPradanGarne: ' प्रमाण पत्र प्रदान गर्ने ',
	pramukhPrasaasaliyaAdhikrit: ' प्रमुख प्रशासकिया अधिकृत ',
};
