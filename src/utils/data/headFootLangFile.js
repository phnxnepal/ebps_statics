export const headfoot = {
    headfoot_data: {
        header: {
            nepaliDate: 'मिति : ',
            bs: 'बि. स.: ',
            ad: 'ई. स.: ',
            nepaliFY: 'आर्थिक बर्ष : ',
            engDate: 'Date : ',
            nayaDarta: 'नयाँ दर्ता',
            nayaPrakriya: 'नयाँ प्रक्रिया सुरु',
            search: 'खोजी सूची',
            adhyapadi: 'अद्यावधिक',
            puranoDarta: 'पुरानो दर्ता',
            dakarmiSuchi: 'डकर्मीको सुचि',
            tallaThap: 'तला थप',
            namsari: 'नामसारी',
            discard: 'रद्द प्रक्रिया',
            myad: 'म्याद थप्ने',
            myadSetup: 'म्याद थप्ने सेटप',
            myadThapApprove: 'म्याद थप्ने स्वीकृत',
            myadApprove: 'म्याद स्वीकृत'
        },
        footer: {
            footerNepali: 'नक्सा पास तथा भवन निर्माण व्यवस्थापन प्रणाली',
            footerEng: 'Electronic Building Permit System (eBPS)'
        },
        history: {
            allHistory: 'All History',
            allComments: 'All Comments',
            activityLog: 'Activity Log',
        },
        setup_lang: {
            addUser: 'प्रयोगकर्ता',
            buildingClass: 'Building Class',
            addDesigner: 'डिजाइनर',
            downloads: 'Downloads',
            masons: 'Mason List',
            addFAQ: 'Frequently Asked Questions',
            contacts: 'Contacts',
            floorRate: 'Floor Rate',
            addFY: 'आर्थिक वर्ष सेटप',
            menuSetup: 'Menu Setup',
            menuAccessSetup: 'Menu Access Setup',
            rajasowSetup: 'राजश्व सेटप',
            discardSetup: 'रद्द गर्नको सेटप',
            fileStorage: 'File Storage Category',
            formGrp: 'Form Group',
            forward: 'Forwarding Setup',
            formName: 'Form Name',
            organization: 'Organization',
            roadSetBack: 'Road SetBack',
            otherSetBack: 'Other SetBack',
            classGroup: 'Class Group Setup',
            hasRevised: 'Has Revision Setup',
            archiBMaster: 'Architecture B Master',
            archiCMaster: 'Architecture C Master',
            elecDesignMaster: 'Electrical Design Master',
            mapCheckMaster: 'Map Check Report Master',
            tallaThapSetup: 'तला थप स्वीकृति',
            namsariSetup: 'नामसारी सेटप',
            changeDesigner: 'डिजाइनर परिवर्तन गर्नुहोस्',
            wardSetup: 'वडा सेटप',
            userTypeSetup: 'User Type Setup'
        },
        files: {
            upload: 'Upload Files',
            view: 'View Files',
            print: 'Print All'
        },
        profile: {
            profile: 'My Profile',
            changePassword: 'Change Password',
            keyboardLayout: 'Keyboard Layout',
            logout: 'Logout',
            // profile: 'प्रोफाइल',
            // changePassword: 'पासवर्ड परिवर्तन',
            // keyboardLayout: 'किबोर्ड लेआउट',
            // logout: 'लगआउट'
        }
    }
};

export const profile = {
    profile_data: {
        u_name: 'Username : ',
        address: 'Address : ',
        org_name: 'Organization Name : ',
        office_name: 'Office Name : ',
        email: 'Email Address : ',
        educationQualification: 'Education Qualification : ',
        joinDate: 'Joined Date : ',
        licenseNo: 'license No : ',
        contact: 'Mobile No : ',
        municipalRegNo: 'Municipal RegNo : ',
        signature: 'Signature : ',
        stamp: 'Stamp : ',
        province: 'Province : '
    }
};

export const historyComment = {
    hc_data: {
        date: 'मिति : ',
        time: 'समय : ',
        status: 'स्थिति : ',
        user: 'प्रयोगकर्ताको प्रकार : ',
        comment: 'टिप्पणी : ',
        remarks: 'कैफियत : ',
        hist: 'History',
        cmts: 'Comments'
    }
};
