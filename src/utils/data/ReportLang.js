export const Reportlang = {
	report_data: {
		datafetch: 'Fetching Permit Data...',
		infobarName: 'घरनक्सा दरखास्त फारम रिपोर्टहरु',
		statistics: 'तथ्याङ्क',
		fiscalYear: 'आर्थिक वर्ष : ',
		wardNo: 'वडा नं.',
		constructionType: 'भवनको किसिम',
		allWards: 'सबै वडा',
		jammaDarta: 'नयाँ दर्ता',
		puranoDarta: 'पुरानो दर्ता',
		plinthPraman: 'प्लिन्थ लेभल सम्पन्न सम्पन्‍न प्रमाण पत्र',
		superStrucPraman: 'सुपरस्ट्रक्चरको निर्माण कार्य सम्पन्‍न प्रमाण पत्र',
		buildingFinish: 'निर्माण सम्पन्‍न प्रमाण पत्र',
		masariFinish: 'नामसारी सम्पन्‍न प्रमाण पत्र',
		NoReport: 'कुनै पनि रिपोर्ट फेला परेन ।',
	},
};
