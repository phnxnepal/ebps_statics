export const aminKoPratibedan = {
	StructureDesign: {
		heading: {
			head1: 'उर्लावारी ',
			head2: 'कार्यालय ',
			head3: 'घर नक्सा पास इकाई ',
		},
	},
	topic: 'अमिनको प्रतिवेदन',
	tippani: 'टिप्पणी / आदेश',
};

export const aminSthalgatPratibedan = {
	topic: '(अमिनको स्थलगत निरीक्षण प्रतिवेदन)',
	sitePlanLocation: 'साईट प्लान बनाउने स्थान',
	signature: {
		name: 'अमिनको नाम',
		designation: 'दर्जा',
		phone: 'फोन नं.',
		date: 'मिति',
		signature: 'हस्ताक्षर',
	},
};
