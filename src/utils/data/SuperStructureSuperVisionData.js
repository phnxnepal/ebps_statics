export const requiredDataFrametructure = {
	//header 1
	superVision: 'Supervision Report',
	on: 'on',
	frame: 'RCC Frame Structure Building',
	typeOfBuilding: 'Type Of Building',
	levelOfCompletion: 'Level of Completion of Building',
	constructionWorkDetails: 'Construction Work Details:',
	fundamentalWork: 'A. Foundation Work:-',
	isolatedFooting: 'a. Number of isolated footing',
	combinedFooting: 'b. Number of Combined footing',
	eccentricFooting: 'c. Number of eccentric Footing',
	numberOfColumn: 'd. Total Number of column',
	sizeIsolatedFooting: 'e. size and number of isolated footing',
	sizeCombineFooting: 'f. size and number of combine footing',
	sizeEccentricFooting: 'g. size and eccentric footing',
	typeOfSoling: 'h. Type of soling: stone or brick',
	thicknessAndGradeOfPCC: 'i. Thickness and grade of PCC',
	gradeOfConcrete: 'j. Grade of Concrete and rebar used',
	compactionMethod: 'k. Compaction method: ',
	formWorkUsedForFoundation: 'l. Formwork used for foundation:- (Timber/plywood/steel)',

	//header 2
	columnReinforcement: 'B. Column Reinforcement',
	sizeOfColumn: 'a. Size of Column',
	noAndSizeOfRebarUsedInTheColumn: 'b. No. and size of rebar used in the column',
	sizeAndSpacingOfStirrupsheader2: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeheader2: 'd. Concrete and rebar grade',

	//header3
	foundationBeam: 'C. Foundation Beam:-',
	sizeOfFoundationBeam: 'a. Size of foundation beam',
	noAndSizeOfRebarUsedInTheFoundationalBeam: 'b. No and size of rebar used in the foundation beam',
	sizeAndSpacingOfStirrupsheader3: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeheader3: 'd. Concrete and rebar grade',

	//header 4
	strapBeam: 'D. Strap Beam',
	sizeOfStrapBeam: 'a. Size of strap beam',
	sizeAndNumberOfRebar: 'b. Size and number of main rebar',
	sizeAndSpacingOfStirrupsheader4: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeheader4: 'd. Concrete and rebar grade',

	//header 5
	ifAny: 'E. If any',
	client: 'Client',
	name: 'Name',
	signature: 'Signature',
	licenseHolderMason: 'License holder Mason',
	superVisorEngineer: 'Supervisor Engineer',

	forOfficalUseOnly: 'For official Use only',
};

export const requiredDataLoadBearing = {
	//header 1
	superVisionReportLB: 'Supervision Report',
	onLB: 'On',
	loadBearingStructureBuilding: 'Load Bearing Structure Building',
	typeOfBuilding: 'Type of the building:',
	levelOfCompletion: 'Level of Completion of Building:-',
	constructionWorkDetails: 'Construction Work Details',
	foundationWork: 'A. Foundation Work:-',
	mansoryWork: 'a. Mansory Work:-',
	numberOfShortWallFooting: 'b. Number of short wall footing',
	numberOfLongWallFooting: 'c. Number of long wall footing',
	numberOfEccentricWallFooting: 'd. Number of eccentric wall footing',
	lengthaAndThicknessOfShortWallFooting: 'e. length and thickness of short wall footing',
	lengthaAndThicknessOfLongWallFooting: 'f. length and thickness of long wall footing',
	lengthAndThicknessOfEccentricFooting: 'g. length and thickness of eccentric footing',
	typeOfSolingForm2: 'h. Type of soling:',
	thicknessAndGradeOfPCCform2: 'i. Thickness and grade of PCC',
	gradeOfConcreteAndRebarForFoundationAndPlinthBand: 'j. Grade of Concrete and rebar used for foundation and plinth band',
	compactionMethodForm2: 'k. Compaction Method:',
	formworkUsedForFoundationForm2: 'l. Formwork used for foundation:-',

	//header2

	foundationBand: 'B. Foundation Band:-',
	sizeOfFoundationBand: 'a. Size of foundation band',
	noAndSizeOfRebarUsedInTheFoundationBand: 'b. No and size of rebar used in the foundation band',
	sizeAndSpacingOfStirrupsHeader2: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeheader2: 'd. Concrete and rebar grade',

	//header3
	strapBeam: 'C. Strap Beam',
	sizeOfStrapBeam: 'a. Size of strap beam',
	sizeAndNumberOfMainRebar: 'b. Size and number of main rebar',
	sizeAndSpacingOfStirrupsheader3Form2: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeheader3Form2: 'd. Grade of concrete and rebar',

	//header 4
	plinthBand: 'D. Plinth Band:-',
	sizeOfPlinthBand: 'a. Size of plinth band',
	noAndSizeOfRebarUSedInThePlinthBand: 'b. No and size of rebar used in the plinth band',
	sizeAndSpacingOfStirrupsHeader4Form2: 'c. Size and spacing of stirrups',
	concreteAndRebarGradeHeader4Form2: 'd. Concrete and rebar grade',

	//header 5
	ifAnyForm2: 'E. If Any',
	clientForm2: 'Client',
	nameForm2: 'Name:-',
	signature: 'Signature:-',
	licenseHolderMasonForm2: 'License holder Mason',
	superVisorEngineerForm2: 'Supervisor Engineer',
};
