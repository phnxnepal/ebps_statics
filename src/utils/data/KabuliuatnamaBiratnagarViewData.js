export const requiredDataForKabuliuatnamaBiratnagarView = {
	heading: 'कबुलियतामा',
	likhitimWadaNumber: ' लिखितम जिल्ला मोरंग वि. मा. न. पा. वडा न. ',
	basne: ' बस्ने ',
	ko: ' को ',
	nati: ' नाती ',
	natini: ' नातिनी ',
	chora: ' छोरा ',
	chori: ' छोरि ',
	pati: ' पति ',
	patni: ' पत्नी ',
	barsa: ' वर्ष ',
	aageMaile: ' आगे मैले बिराटनगर महानगरपालिका वडा न ',
	sitithKittaNumber: ' स्थित कि. न. ',
	purbaTarfa: ' को पूर्वतर्फ ',
	paschimTarfa: ' पश्चिम तर्फ ',
	uttarTarfa: ' उत्तर तर्फ ',
	dakshinTarfa: ' र दक्षिण तर्फ ',
	charKillaBhitrakoJagga: ' यति चार किल्ला भित्रको जग्गा विगाहा ',
	nirmanGarnakaLagiNakshaPass: ' मा निर्माण गर्नका लागि नक्शा पास गरिपाउ भनि नक्शा पेश गरेको उल्लेखित जग्गाको चौहादीमध्ये ',
	gravelKacchi: ' तर्फको पिच ग्राभेल कच्ची सडकको बीच सेन्टरबाट न पा मापदण्ड र प्रचलित नियमानुसार ',
	feetMeter: ' फीट मीटर ',
	chadiBanunuParne: ' छाडी बनाउनु पर्नेमा सो नगरी सडक अधिकारी क्षेत्रको जग्गा ',
	michiyekoDekhiyekale: ' फीट मीटर मिचिएको देखिएकोले मेरो ',
	nakshaPassHunaNasakeko: ' को नक्शा पास हुन नसकेको हुदा निम्नलिखित कबुलियत गरि हाललाइ मेरो ',
	nakshaPassGaridinuhuna: ' को नक्शा पास गरिदिनुहुन अनुरोध गर्दछु। मेरो कित्ता न ',
	jaggaBigaha: ' को जग्गा विगाहा ',
	ma: ' मा ',
	tarfaKoSaAaChe: ' तर्फ स. अ. क्षे. पर्ने सार्वजनिक जग्गा मिची बनाएको ',

	dataLine:
		'को सम्पूर्ण भाग नेपाल सरकार विराटनगर महानगरपालिकालाइ सडक विस्तार गर्दा लाग्ने खर्च आफैले व्यहोरी बिना मुआब्जा उल्लेखित भाग क्लियर गर्ने छु।  यस विषयमा कतै कुनै किसिमको उजुर बाजुर गर्ने छैन, गरेमा यसै कबुलियतनामाको आधारमा न पा ले उक्त मिचिएको भाग भत्काउन लगाई मेरा हाम्रा आचल सम्पतिबाट हर्जाना समेत आसुल उपर गरेमा मेरो मञ्जुर छ भनि न पा मा उपस्थित भै श्रीमान प्रमुखज्युको रोहवरमा यो कबुलियतमा गरि सहि छाप गरिदिए गरिदियौ व्यहोरा ठिक साचो हो, झुठ ठहरे कानुन बमोजिम सहुला बुझाउला। ',

	itiSambat: ' इति सम्बत ',
	saal: ' साल ',
	mahina: ' महिना ',
	gate: ' गते रोज सुभम । ',
};
