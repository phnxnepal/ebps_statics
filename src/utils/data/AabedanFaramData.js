export const aabedanFaramData = {
	aabedanFaram: 'आवेदन फारम',
	bisaye: 'विषय : निर्माण कार्य सम्पन्न प्रमाण पत्र तथा धरौटी फिर्ता पारे ।',
	mahodaye: 'महोदय,',
	chana: 'उपयुक्त सम्बन्धमा विराटनगर महानगरपालिकाको च. न. ',
	miti: 'मिति',
	gharKo: ' गत घरको ',
	superStructure: ' सुपरस्ट्रक्चर',
	compoundWall: ' कम्पाउन्डवाल ',
	nirman: ' निर्माण',
	maile: ' मैले',
	hamile: ' हामीले',
	chetraFalFeet: ' को क्षेत्रफल फिट',
	dharauti: 'भित्र वि.म.न.पा. बाट स्वीकृत नक्सा तथा मापदण्ड र नेपाल राष्ट्रिय भवन निर्माण मा २०६० अनुसार निर्माण कार्य सम्पन्न गरेको ',
	nirmanKarya:
		' । अतः निर्माण कार्यमा संग्लग्न प्राबिधिकको प्रतिबेदन संग्लग्न राखी न.पा. बाट जे बुझनु पर्ने बुझी निर्माण कार्य सम्पन्न प्रमाण पत्र सहित मैले म.न.पा. मा नक्सा पेश गर्दा राखेको धरौटी रु ',
	fitraPau: 'फिर्ता पाऊ भनि यो निवेदन पेश रहेको ',
	nibedak: 'निवेदक',
	nibedakKoNaam: 'निवेदक नाम:',
	waresKoNaam: 'वारेसको नाम:',
	thegana: 'ठेगाना:',
	dastaKhat: 'दस्तखत:',
	chu: 'छु',
	chau: 'छौ',
};
