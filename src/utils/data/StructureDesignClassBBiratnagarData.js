export const structureDesignClassBBiratnagarRequiredData = {
	//Section Header
	sectionHeader: {
		prabidhikBibarn: 'प्राविधिक विवरण फारामहरु',
		structuralDesignSambandhi: '(घ) स्ट्रक्चरल डिजाइन सम्बन्धि',
		checkList: 'Checklist for',
		professionallyEngineered: 'Professionally Engineered Buildings (Class "B") ',
		plinthAreaHeader: '(Plinth Area => 1000 Sq. Ft. & No. of Stories > 3)',
		nbc000: 'NBC 000: 1994 to NBC 114:1994',
		inCaseOfManyUnits: '(In case of many units, fill up the form for main unit only)',
	},

	//general table
	generalTable: {
		sn: 'S.N.',
		general: '1. General',
		description: 'Description',
		asPerSubmittedDesign: 'As per Submitted Design',
		remarks: 'Remarks ',
		category: 'Category of Building',
		functionalUseOfBuilding: 'Functional Use of Building',
		plinthAreaOfBuilding: 'Plinth Area of Building',
		numberOfStorey: 'Number of storey',
		totalHeightOfStructure: 'Total height of structure',
		structuralSystem: 'Structural System Building',
		typeOfSoil: 'Type of soil in foundation',
		adoptedSafeBearing: 'Adopted Safe bearing Capacity of Soil',
		nameOfSoftware: 'Name of software package used for structural design analysis',
		provisionForFutureExtension: 'a) Provision for future extension',
		IfYes: 'b) How many floors will be Extended?',
		structuralDesignConsideration: 'c) Structural design consideration for future extension',
		classOptions: ['A', 'B', 'C', 'D'],
		a: 'A',
		b: 'B',
		c: 'C',
		d: 'D',
		frame: 'Frame',
		loadBearing: 'Load Bearing',
		other: 'Other',
		yes: 'Yes',
		no: 'No',
		floors: 'Floors',
	},
	//requirement of nepal national building code (NBC)
	secondHeading: {
		requirement: '2. Requirement of Nepal National Building CODE (NBC)',
	},
	// NBC-000-1994 requirement for state of design
	requirementForStateOfDesign: {
		nbc0001994: '2.1 NBC-000-1994 Requirements for state of the Art Design: An Introduction',
		options: {
			internationalStateOfArt: 'International state of the Art',
			professionallyEngineeredStructures: 'Professionally Engineered structures',
			mandatoryRuleOfThumb: 'Mandatory rule of thumb',
			guideLinesOfRuralBuilding: 'Guidelines of rural building',
		},
		levelOfDesign: 'Level of design',
		internationalStateOfArt: 'International state of the Art',
		professionallyEngineeredStructures: 'Professionally Engineered structures',
		mandatoryRuleOfThumb: 'Mandatory rule of thumb',
		guideLinesOfRuralBuilding: 'Guidelines of rural building',
	},
	//// NBC-101-1994 materials specifications
	materialsSpecification: {
		nbc1011994: '2.2 NBC 101:1994 Materials specifications',
		tickTheListMaterials: 'Tick the listed materials that will be used in the construction',
		options: {
			cement: 'Cement',
			fineAggregates: 'Fine Aggregates',
			naturalBuildingStones: 'Natural building stones',
			tiles: 'Tiles',
			metalFrames: 'Metal frames',
			coarseAggregates: 'Coarse aggregates',
			bricks: 'Bricks',
			timber: 'Timber',
			structuralSteel: 'Structural Steel',
		},
		cement: 'Cement',
		fineAggregates: 'Fine Aggregates',
		naturalBuildingStones: 'Natural building stones',
		tiles: 'Tiles',
		metalFrames: 'Metal frames',
		coarseAggregates: 'Coarse aggregates',
		buildinglime: 'Building lime',
		bricks: 'Bricks',
		timber: 'Timber',
		structuralSteel: 'Structural Steel',
		inWhatManner: 'In what manner way have you used NBC 101?',
	},
	//NBC 102-1994 Unit weight of materials
	thirdHeading: {
		unitWeight: '2.3 102-1994 Unit Weight of Materials',
	},
	unitWeightOfMaterials: {
		whereDoYouPlanToApplyNBC102: 'Where do you plan to apply NBC 102?',
		specifyTheDesignUnitsOfMaterials: 'Specify the design unit weight of Materials',
		steel: 'Steel',
		brick: 'Brick',
		rcc: 'RCC',
		brickMasonry: 'Brick Masonry',
		specifications: 'Specifications',
		designCalculations: 'Design Calculation',
		billOfQuantity: 'Bill of Quantity',
		note:
			'Note: If any materials other than specified in NBC-102-1994, the designer should take responsibility that such materials are according to international standard.',
	},

	// 2.4 occupancy load (imposed load)
	fourthHeading: '2.4 NBC 103-1994 Occupancy load (Imposed Load)',

	occupancyLoad: {
		//columns
		porposedOccupancyType: 'Proposed Occupancy Type',
		fillInTheConcerning: '(Fill in only concerning occupancy type)',

		//for residential buildings
		forResidentialBuildings: 'For Residential Building',
		roomsAndKitchen: 'Rooms and Kitchen',
		corridorsStaircaseStore: 'Corridors, Staircase, Store',
		balcony: 'Balcony',

		//for hotels, hostels, dormitories
		forHotelsHostelsDorm: 'For Hotels, Hostels, Dormitories',
		livingBedAndDormitories: 'Living bed and dormitories',
		kitchenCorridorsStaricase: 'Kitchen, Corridors, Staircase',
		storeRooms: 'Store rooms',
		diningResturants: 'Dining, Resturants',
		officeRooms: 'Office Rooms',

		//for Educational Buildings
		forEducationalBuildings: 'For Educational Buildings',
		classRooms: 'Class rooms, Dining rooms',
		kitchen: 'Kitchen',
		stores: 'Stores',
		librariesAndArchives: 'Libraries and archives',
		blaconies: 'Balconies',

		//for institutional buildings
		forInstitutionalBuildings: 'For Institutional Building',
		bedRoomsWardsDressingRooms: 'Bed rooms, wards, dressing rooms',
		kitchens: 'Kitchens',
		xrayOperatingRooms: 'X-ray rooms, operating roomms',
		corridorsAndStaircase: 'Corridors And Staircase',
		//use balconies form educational buildings

		//for assembly building

		forAssemblyBuildings: 'For Assembly Buildings',
		assemblyAreas: 'Assembly Areas',
		projectionsRooms: 'Projection Rooms',
		stages: 'Stages',
		corridorsPassageStaricase: 'Corridors, Passage and staircase',
		//use balconies from educational buildings

		//for business and office building
		forBusinessAndOfficeBuildings: 'For Business and office Building',
		roomsWithSeperateStorage: 'Rooms with seperate Storage',
		roomsWithoutSeperateStorage: 'Rooms without seperate Storage',
		filesRoomsAndStorageRooms: 'Files rooms and storage rooms',
		stairAndPassage: 'Stair And Passage',
		//use balconies from educational buildings

		//for mercantile Buildings

		forMercantileBuildings: 'Mercantile Building',
		retailShops: 'Retail Shops',
		wholesaleShops: 'wholesale shops',
		office: 'Office',
		staricaseAndPassage: 'Staricase And Passage',
		//use balconies from educational buildings

		//industrial building
		industrialBuilding: 'Industrial Building',
		workAreaWithoutMachinery: 'Work Area without Machinery',
		withMachineryLightDuty: 'With Machinery:Light Duty',
		meduimDuty: 'Meduim Duty',
		heavyDuty: 'Heavy Duty',
		boiler: 'Boiler',
		staircasePassage: 'Staricase, Passage',

		//storage Buildings
		storageBuilding: 'Storage Building',
		storageRooms: 'Storage Rooms',
		coldStorag: 'Cold Storage',
		corridorAndPassage: 'Corridor and Passage',
		boilerRooms: 'Boiler rooms',

		//ROWS
		occupancyLoad: 'OccupancyLoad',
		uniformlyDistributedLoad: `Uniformly Distributed Load (Kn/meter square)`,
		concentratedLoad: '(KN)',
	},

	// 2.5 wind load
	windLoad: {
		windNBC: '2.5 NBC 104-1994 Wind Load',
		windZone: 'Wind Zone',
		basicWindVelocity: 'Basic wind velocity',
	},

	//2.6
	seismicDesign: {
		seismicNBC: '2.6 NBC 105-1994 Seismic Design of Building in Nepal',
		methodOfEarthquakeAnalysis: 'Method of earthquake analysis',
		seismicCoefficientMethos: 'Seismic coefficient method',
		modelResponseSpectrum: 'Model Response spectrum method',
		subsoilCatefory: 'Subsoil Category',
		fundamentaltransactionPeriod: 'Fundamental transaction period',
		basicSeismicCoefficient: 'Basic seismic coefficient',
		sesmicZoningFactor: 'Seismic zoning factor',
		importanceFactor: 'Importance factor',
		structuralPerformanceFactor: 'Structural performance factor',
	},

	//2.7

	snowLoad: {
		snowNBC: '2.7 106-1994 Snow Load',
		snowfallArea: 'Snowfall Area',
		elevation: 'Elevation',
		designDepth: 'Design Depth',
		designDensity: 'Design Density',
		perennial: 'Perennial',
		occasional: 'Occasional',
		noSnowfall: 'No Snowfall',
	},

	//2.8
	fireSafety: {
		fireSafetyNBC: '2.8 NBC 107-1994 Provisional Recommendation of fire safety',
		whereDoYouPlan: 'Where do you plan to apply the fire safety requirements specified in NBC 107 and NBC 206-1994?',
		specifications: 'Specifications',
		designCalculations: 'Design Calculations',
		billOfQuantity: 'Bill of quantity',
	},
	//2.9
	seismicHazards: {
		seismicHazardsNBC: '2.9 NBC 108:1994 Site Construction for Seismic Hazards',
		distanceFromToe: 'Distance from toe/beginning of downward slope',
		distanceFromRiverBank: 'Distance from river bank',
		soilTypeInFooting: 'Soil type in footing',
		adoptedSafeBearingCapacity: 'Adopted safe bearing capacity',
		depthOfFoundation: 'Depth of Foundation',
		soilTestReport: 'Soil test report available?',
		yes: 'Yes',
		no: 'No',
		note:
			'Note: Soil test is advisable for all professional engineered structures. In case, soil test is not carried out, the Designer should take responsibility for assumed data concerning site consideration',
	},
	//2.15
	constructionsafely: {
		construcationSafelyNBC: '2.15 NBC: 114:1994 Construction Safely',
		allSafetyMeasuresWillBeFulfilled: 'Are you sure that all safety measures will be fulfilled in the construction site as per this code?',
		yes: 'Yes',
		no: 'No',
		safetyWaresUse: 'Safety wares use',
		safetyHardHat: 'Safety hard hat',
		safetyGoogles: 'Safety Googles',
		safetyBoots: 'Safety Boots',
		safetyBelt: 'Safety belt',
		firstAidFacility: 'First aid facility',
	},
	//2.11
	//structural data for frame structure
	structuralDataForFrameStructure: {
		structuralData: 'STRUCTURAL DATA FOR FRAME STRUCTURE',
		structuralDataFrameStructureNBC: '2.11 NBC 110:1994 Plain and reinforced concrete',
		concreteGrade: 'Concrete Grade',
		reinforcementSteelGrade: 'Reinforcement steel grade',
		criticalSizeOfSlabPanel: 'Critical size of slab panel',
		slabThickness: 'Slab thickness',
		calculatedShortSpanToEffectiveDepthRatio: 'Calculated short span to effective depth ratio',
		LDforCorrespondingSlab: '(L/d) for corresponding slab',
		permissibleLDratio: 'permissible L/D ratio',
		effectiveDepth: 'Effective Depth',
		basicValue: 'Basic value of L/d',
		spanCorrectionFactor: 'Span correction factor',
		tensionReiforcement: 'Tension reinforcement (Ast) percent',
		modificationFactor: 'Ast modification factor',
		compressionReinforcementModificationFactor: 'Compression reinforcement modification factor',
		beamCharacteristics: 'Beam Characteristics',
		maximumSpanDepthRatio: 'Maximum Span/Depth ratio',
		spanOfCorrespondingbeam: 'span of corresponding beam',
		depthOfCorrespondingBeam: 'Depth of corresponding beam',
		widthOfCorrespondingbeam: 'Width of corresponding beam',

		maximumSlendernessRatioOfColumnLateral: 'Maximum slenderness ratio of column lateral dimension of corresponding column',
		designPhilosophy: 'Design Philosophy',
		loadCombination: 'Load Combination',
		workingStressMethod: 'Working stress method',
		limitStateMethod: 'Limit state method',

		valueOfHorizontalSeismicBaseShear: 'Value of Horizontal Seismic Base Shear (At Least One Frame) Submit Calculation:',
		submitDesignCalculation: 'Submit Design calculation  of foundation, Columns, beam and slab',
		typeOfFoundation: 'Type of Foundation:',
		depthOfFoundation: 'Depth of Foundation:',
		sizesOfFoundation: 'Sizes of foundation:',
		sizesOfColumns: 'Sizes of columns:',
		sizeAndNUmbersOfBArsProvidedInColumns: 'Size and Nos of bars provided in columns',
		typeOfBuilding: 'Type of Building',
		storeyOfBUilding: 'Storey of Building',
		load: 'Load',
		areaOfFoundation: 'Area of Foundation',
		sampleCalculationOfBase: 'Sample calculation of base area and depth',
		typeOfSoil: 'Type of Soil',
		factorOfSafetyProvided: 'Factor of safety provided:',
		isTheSoilLiquefiable: 'Is the soil liquefiable?',
		isItTestedByLab: 'Is it tested by lab/visualization? If not tested designer should verify',

		///-------
		conditionsOfBeams: 'Conditions of beams',
		cantilever: 'Cantilever',
		simplySupported: 'Simply Supported',
		oneSideContinuous: 'One side continuous',
		bothSideContinuous: 'Both side continuous',

		ultimateSteessMethod: 'Ultimate stress method',
		combined: 'Combined',
		combineIndependent: 'Combine Independent',
		matPile: 'Mat/Pile',
		strip: 'Strip',
		frame: 'Frame',
		loadBearing: 'Load Bearing',
		DLLLEQ: 'DL + LL + EQ + WL + if any',
		clay: 'Clay',
		siltyClay: 'Silty Clay',
		fineSand: 'Fine Sand',
		blackCotton: 'Black Cotton',
		specifyIfAny: 'Specify If any other',
		yes: 'Yes',
		no: 'No',

		note:
			'Note: on of detail design analysis and calculation is compulsory for all types of building under category A & B (which should include general data & load calculation, base shear calculations, load combinations, frame analysis and sample designs of foundations, columns, beams etc). Besides that for important buildings like commercial complexes, educational & etional buildings, hotels, assembly & office buildings with mass public movement, submission of soil test report is compulsory.',
	},
	//2.10
	//structural data for load bearing wall structure
	structuralDataForLoadBearing: {
		heading: 'II. Structural Data For Load Bearing Wall Structure',
		loadBearingNBC: '2.10 NBC 109:1994 Masonry: Unreinforced',
		depthOfFoundation: 'Depth of Foundation',
		widthOfFoundation: 'Width of Foundation',
		concreteGrade: 'Concrete Grade',
		brickCrushingStrength: 'Brick Crushing Strength',
		mortarRatioForLoadBearingMasonry: 'Mortar ratio for load bearing masonry',
		floor: 'Floor',
		groundFloor: 'Ground Floor',
		firstFloor: 'First Floor',
		secondFloor: 'SecondFloor',
		openingDetails: 'Opening Details',
		leastDistanceBetweenAnyTwoOpening:
			'Least distance from inside corner, Does the total length of opening in any wall exceed 50% of its length?',
		horizontalDistanceBetweenAnyTwoOpening:
			'Does the horizontal distance between any two opening less than 600 mm or 1/2 of height of shorter opening?',
		VerticalDistanceBetweenAnyTwoopening: 'Does the vertical distance between two opening less than 600 mm or 1/2 of wodth of smaller opening ?',
		anyOfTheAboveMentionedCases: 'If any of above mentioned cases do not comply, do you have provision for strengthening around opening?',
		bandsProvided: 'Bands provided',
		verticalSteelReinforcement: 'Vertical steel reinforcement diameters at corner/tee joints:',
		distanceOfCornerTee: 'C/C distance of corner/tee strengthening Horizontal dowel bars',

		wallHeight: 'Wall Height',
		wallThickness: 'Wall Thickness',
		maximumLength: 'Maximum Length',
		yes: 'Yes',
		no: 'No',
		plinthLevel: 'Plinth Level',
		lintelLevel: 'Lintel Level',
		roofLevel: 'Roof level',
		gableBand: 'Gabel band',
		note: 'Note',
		noteContent:
			'Submission of detail design analysis and calculation is compulsory for all types of building under category A & B (which should include general data & load calculations, load combinations, design analysis and sample designs of foundation, columns, beams, slab, etc). Besides that for important buildings like commercial complexes, educational and institutional buildings, hotels, hostel, assembly & office buildings with mass public movement, submission of soil test report is compulsory.',
	},

	//other types 2.12 2.13 2.14
	structuralDataForOtherTypesOfStructures: {
		nbc212: {
			heading: 'III. Structural Data For Other Types of Structures',
			otherTypesOfStructureNBC: '2.12 NBC: 111-1994 Steel',
			designAssumption: 'Design Assumptions',
			yeildStress: 'Yeild Stress',
			leastWallThickness: 'Least wall thickness',
			exposeCondition: 'Expose condition',
			forExposedSection: 'For Exposed Section',
			forNotExposedSection: 'For Not Exposed Section',
			haveYouUsedTruss: 'Have you used Truss?',
			whatIsTheCriticalSpan: 'What is the critical span of purlin?',
			purlinSize: 'Purlin Size',
			haveYouUsedSteelPost: 'Have You used steel post?',
			slendernessRatioOfTheCriticalPost: 'Slenderness ratio of the critical post',
			yes: 'Yes',
			no: 'No',
			options: {
				simpleConnection: 'Simple Connection',
				websOfStandardSize: 'Webs of standard size',
				composedSection: 'Composed Section',
			},
			simpleConnection: 'Simple Connection',
			semiRigidConnection: 'Semi-rigid connection',
			fullyRigidConnection: 'Fully rigid conneciton',
			pipe: 'Pipe',
			websOfStandardSize: 'Webs of standard size',
			composedSection: 'Composed Section',
		},
		nbc213: {
			woodNBC: '2.13 NBC: 112-1994 Timber',
			nameOfStructuralWood: 'Name of structural wood:',
			modulesOfElasticity: 'Modules of Elasticity',
			criticalSpanOfBeam: 'Critical Span of the beam element Designed deflection',
			slendernessRatioOfTheCriticalPost: 'Slenderness ratio of the critical post',
			jointType: 'Joint type:',
			nbc214: '2.14 NBC 113 : 1994 Aluminium',
			haveYouUsedAluminium: 'Have you used aluminium as structure member? If yes please mention the name of design code.',
			yes: 'Yes',
			no: 'No',
		},
	},
};
