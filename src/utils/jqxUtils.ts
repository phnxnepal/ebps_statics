import { TableWidths } from "./enums/ui";

// export const cellClass = (row: any, dataField: any, cellText: any, rowData: any) => {
//     const cellValue = rowData[dataField];
//     if (cellValue === 'अस्वीकृत गरिएको') {
//         return 'disapproved';
//     } else if (cellValue === 'स्वीकृत भएको') {
//         // return 'approved';
//         const columnValue = rowData['applicationActionBy'];
//         if (columnValue === 'डिजाइनर') {
//             return 'designer';
//         } else if (columnValue === 'सब-ईन्जिनियर') {
//             return 'subEngineer';
//         } else if (columnValue === 'ईन्जिनियर') {
//             return 'engineer';
//         } else if (columnValue === 'अधिकृत') {
//             return 'admin';
//         } else if (columnValue === 'राजस्व') {
//             return 'rajaswo';
//         } else {
//             return 'amin';
//         }
//     } else if (cellValue === 'कारबाही पूर्ण भएको ') {
//         return 'completedTask';
//     } else {
//         return 'average';
//     }
// };

const pinnedFields = ['applicantNo', 'applicantName'];

export const cellClass = (row: any, dataField: any, cellText: any, rowData: any) => {
    let className = '';
    const cellValue = rowData['yourStatus'];
    if (cellValue === 'अस्वीकृत गरिएको') {
        if (pinnedFields.includes(dataField)) {
            className += ' pinned-disapproved';
        } else {
            className = 'disapproved';
        }
    } else if (dataField === 'yourStatus') {
        if (cellValue === 'स्वीकृत भएको') {
            className = 'approved';
            const columnValue = rowData['applicationActionBy'];
            if (columnValue === 'डिजाइनर') {
                className += ' designer';
            } else if (columnValue === 'सब-ईन्जिनियर') {
                className += ' subEngineer';
            } else if (columnValue === 'ईन्जिनियर') {
                className += ' engineer';
            } else if (columnValue === 'अधिकृत') {
                className += ' admin';
            } else if (columnValue === 'राजस्व') {
                className += ' rajaswo';
            } else {
                className += ' amin';
            }
        } else if (cellValue === 'कारबाही पूर्ण भएको ') {
            className = 'completedTask';
        } else {
            className = 'average';
        }
    }

    return className;
};


export const applicationStatusCellClass = (row: any, dataField: any, cellText: any, rowData: any) => {
    const cellValue = rowData[dataField];
    if (cellValue === 'चलिरहेको') {
        return 'active_application';
    } else {
        return 'completed_application';
    }
};

export const appListDataFields = [
    { name: 'applicantNo', type: 'string' },
    { name: 'applicantName', type: 'string' },
    { name: 'applicantAddress', type: 'string' },
    { name: 'nibedakName', type: 'string' },
    { name: 'applicantMobileNo', type: 'string' },
    { name: 'applicationStatus', type: 'string' },
    { name: 'applicationActionBy', type: 'string' },
    { name: 'yourStatus', type: 'string' },
    { name: 'applicationFormName', type: 'string' },
    { name: 'applicantDate', type: 'string' },
    { name: 'constructionType', type: 'string' },
    { name: 'forwardTo', type: 'string' },
];

export const appListColumns = [
    {
        text: 'शाखा दर्ता नं.',
        dataField: 'applicantNo',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_NO,
        pinned: 'true',
    },
    {
        text: 'जग्गाधनीको नाम',
        dataField: 'applicantName',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        pinned: 'true',
    },

    {
        text: 'जग्गाधनीको ठेगाना',
        dataField: 'applicantAddress',
        cellsalign: 'center',
        align: 'center',
        width: '150',
    },
    {
        text: 'निवेदकको नाम',
        dataField: 'nibedakName',
        cellsalign: 'center',
        align: 'center',
        width: '150',
    },
    {
        text: 'सम्पर्क फोन.',
        dataField: 'applicantMobileNo',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.MOBILE_NO,
    },
    {
        text: 'तह',
        dataField: 'applicationActionBy',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_ACTION_BY,
        filtertype: 'list',
        // cellclassname: this.cellClassTaha
    },
    {
        text: 'तह स्थिति',
        dataField: 'forwardTo',
        cellsalign: 'center',
        align: 'center',
        width: '250',
        // filtertype: 'list',
    },
    {
        text: 'अवस्था',
        dataField: 'yourStatus',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        cellclassname: cellClass,
        filtertype: 'list',
    },
    {
        text: 'स्थिति फारम',
        dataField: 'applicationFormName',
        cellsalign: 'center',
        align: 'center',
        width: '350',
        // cellclassname: 'alignment',
        filtertype: 'list',
        // cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
    },
    {
        text: 'दर्ता मिति',
        dataField: 'applicantDate',
        cellsalign: 'center',
        align: 'center',
        width: '100',
    },
    {
        text: 'भवनको किसिम',
        dataField: 'constructionType',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.CONSTRUCTION_TYPE,
        pinned: 'true',
        filtertype: 'list',
    },
    {
        text: 'पूर्ण अवस्था',
        dataField: 'applicationStatus',
        cellsalign: 'center',
        cellclassname: applicationStatusCellClass,
        align: 'center',
        width: TableWidths.APPLICATION_STATUS,
        filtertype: 'list',
    },
];

export const namsariTaskListDataField = [
    { name: 'applicantNo', type: 'string' },
    { name: 'nameTransaferId', type: 'string' },
    { name: 'applicantName', type: 'string' },
    { name: 'applicantAddress', type: 'string' },
    { name: 'nibedakName', type: 'string' },
    { name: 'applicantMobileNo', type: 'string' },
    { name: 'applicationStatus', type: 'string' },
    { name: 'applicationActionBy', type: 'string' },
    { name: 'yourStatus', type: 'string' },
    { name: 'applicationFormName', type: 'string' },
    { name: 'applicantDate', type: 'string' },
    { name: 'constructionType', type: 'string' },
    { name: 'forwardTo', type: 'string' },
];

export const namsariTaskListColumns = [
    {
        text: 'शाखा दर्ता नं.',
        dataField: 'applicantNo',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_NO,
        cellclassname: cellClass,
        pinned: 'true',
    },
    {
        text: 'नामसारी दर्ता नं.',
        dataField: 'nameTransaferId',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_NO,
        cellclassname: cellClass,
        pinned: 'true',
    },
    {
        text: 'जग्गाधनीको नाम',
        dataField: 'applicantName',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        cellclassname: cellClass,
        pinned: 'true',
    },

    {
        text: 'जग्गाधनीको ठेगाना',
        dataField: 'applicantAddress',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        cellclassname: cellClass,
    },
    {
        text: 'निवेदकको नाम',
        dataField: 'nibedakName',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        cellclassname: cellClass,
    },
    {
        text: 'सम्पर्क फोन.',
        dataField: 'applicantMobileNo',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.MOBILE_NO,
        cellclassname: cellClass,
    },
    {
        text: 'तह',
        dataField: 'applicationActionBy',
        cellsalign: 'center',
        align: 'center',
        width: TableWidths.APPLICATION_ACTION_BY,
        filtertype: 'list',
        cellclassname: cellClass,
        // cellclassname: cellClassTaha
    },
    {
        text: 'तह स्थिति',
        dataField: 'forwardTo',
        cellsalign: 'center',
        align: 'center',
        width: '250',
        cellclassname: cellClass,
        // filtertype: 'list',
    },
    {
        text: 'अवस्था',
        dataField: 'yourStatus',
        cellsalign: 'center',
        align: 'center',
        width: '150',
        cellclassname: cellClass,
        filtertype: 'list',
    },
    {
        text: 'स्थिति फारम',
        dataField: 'applicationFormName',
        cellsalign: 'center',
        align: 'center',
        width: '350',
        cellclassname: cellClass,
        // cellclassname: 'alignment',
        filtertype: 'list',
        // cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
    },
    {
        text: 'दर्ता मिति',
        dataField: 'applicantDate',
        cellsalign: 'center',
        align: 'center',
        width: '100',
        cellclassname: cellClass,
    },
    // {
    // 	text: 'भवनको किसिम',
    // 	dataField: 'constructionType',
    // 	cellsalign: 'center',
    // 	align: 'center',
    // 	width: TableWidths.CONSTRUCTION_TYPE,
    // 	pinned: 'true',
    // 	filtertype: 'list',
    // },
    {
        text: 'पूर्ण अवस्था',
        dataField: 'applicationStatus',
        cellsalign: 'center',
        cellclassname: applicationStatusCellClass,
        align: 'center',
        width: TableWidths.APPLICATION_STATUS,
        filtertype: 'list',
    },
];