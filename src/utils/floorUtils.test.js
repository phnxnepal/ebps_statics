import { FloorBlockArray } from './floorUtils';

const floorArray = new FloorBlockArray([
	{ floorUnit: 'FEET', floor: 0, length: 12, width: 12, area: 144, height: 12, block: 'A' },
	{ floorUnit: 'FEET', floor: 12, length: 12, width: 12, area: 144, height: 12, block: 'A' },
	{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12, block: 'B' },
	{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12, block: 'B' },
	{ floorUnit: 'METRE', floor: 12, length: 12, width: 12, area: 144, height: 12, block: 'B' },
]);

const noBlocks = new FloorBlockArray([
	{ floorUnit: 'FEET', floor: 0, length: 12, width: 12, area: 144, height: 12, block: '' },
	{ floorUnit: 'FEET', floor: 12, length: 12, width: 12, area: 144, height: 12, block: '' },
	{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12, block: '' },
	{ floorUnit: 'METRE', floor: 2, length: 12, width: 12, area: 144, height: 12, block: '' },
	{ floorUnit: 'METRE', floor: 12, length: 12, width: 12, area: 144, height: 12, block: '' },
]);

test('should return correct sum of heights', () => {
	expect(floorArray.getSumOfHeights()).toEqual({ A: 12, B: 24 });
});

test('should return correct top of floor', () => {
	expect(floorArray.getTopFloor()).toEqual({
		A: {
			area: 144,
			block: 'A',
			englishCount: 'Underground',
			englishCountName: 'Underground',
			englishName: 'Underground floor',
			floor: 0,
			height: 12,
			length: 12,
			nepaliCount: 'से.मी. / वेसमेन्ट',
			nepaliCountName: 'से.मी. / वेसमेन्ट',
			nepaliName: 'भुमिगत तला',
			width: 12,
		},
		B: {
			area: 144,
			block: 'B',
			englishCount: '2',
			englishCountName: '2',
			englishName: 'First floor',
			floor: 2,
			height: 12,
			length: 12,
			nepaliCount: '२',
			nepaliCountName: '१',
			nepaliName: 'पहिलो तला',
			width: 12,
		},
	});
	expect(noBlocks.getTopFloor()).toEqual({
		area: 144,
		block: '',
		englishCount: '2',
		englishCountName: '2',
		englishName: 'First floor',
		floor: 2,
		height: 12,
		length: 12,
		nepaliCount: '२',
		nepaliCountName: '१',
		nepaliName: 'पहिलो तला',
		width: 12,
	});
});

test('should return correct bottom of floor', () => {
	expect(floorArray.getBottomFloor()).toEqual({
		A: {
			area: 144,
			block: 'A',
			englishCount: 'Underground',
			englishCountName: 'Underground',
			englishName: 'Underground floor',
			floor: 0,
			height: 12,
			length: 12,
			nepaliCount: 'से.मी. / वेसमेन्ट',
			nepaliCountName: 'से.मी. / वेसमेन्ट',
			nepaliName: 'भुमिगत तला',
			width: 12,
		},
		B: {
			area: 144,
			block: 'B',
			englishCount: '2',
			englishCountName: '2',
			englishName: 'First floor',
			floor: 2,
			height: 12,
			length: 12,
			nepaliCount: '२',
			nepaliCountName: '१',
			nepaliName: 'पहिलो तला',
			width: 12,
		},
	});
	expect(noBlocks.getBottomFloor()).toEqual({
		area: 144,
		block: '',
		englishCount: '2',
		englishCountName: '2',
		englishName: 'First floor',
		floor: 2,
		height: 12,
		length: 12,
		nepaliCount: '२',
		nepaliCountName: '१',
		nepaliName: 'पहिलो तला',
		width: 12,
	});
});
