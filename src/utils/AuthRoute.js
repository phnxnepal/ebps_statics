import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isAuthenticated } from './authUtils';

const AuthRoute = ({ component: Component, ...rest }) => {
  return (
  <Route
    {...rest}
    render={props =>
        isAuthenticated() ? (
        <Redirect exact to="/user/task-list" />
      ) : (
        <Component {...props} />
      )
    }
  />
);
  }

export default AuthRoute;
