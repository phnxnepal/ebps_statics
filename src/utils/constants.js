import { getClientSuffix } from './config';

export const TOKEN_NAME = 'PHOENIX-TOKEN' + getClientSuffix();
export const TOKEN_NAME_BACK = 'PHOENIX-TOKEN-BACK' + getClientSuffix();
export const CURRENT_BUILD_PERMIT = 'PHOENIX_EBPS_CURRENT_BUILD_PERMIT';
export const USER_INFO = 'PHOENIX_EBPS_USER_INFO' + getClientSuffix();
export const FISCAL_YEAR = 'PHOENIX_EBPS_FISCAL_YEAR';
export const FISCAL_YEAR_ALL = 'PHOENIX_EBPS_FISCAL_YEAR_ALL';
export const MENU_LIST = 'PHOENIX_EBPS_MENU_LIST';
export const FORM_GROUP_MASTER = 'PHOENIX_FORM_GROUP_MASTER';
export const FORM_NAME_MASTER = 'PHOENIX_FORM_NAME_MASTER';
export const FILE_STORAGE_CATEGORY = 'PHOENIX_FILE_STORAGE_CATEGORY';
export const APPLICATION_FORWARD_DATA = 'PHOENIX_APPLICATION_FORWARD_DATA';
export const WARD_MASTER = 'PHOENIX_WARD_MASTER';
export const KEYBOARD = 'PHOENIX_KEYBOARD_LAYOUT';
export const RENEW_DATA = 'PHOENIX_RENEW_DATA';

export const ARRAY_FIELD_DELIMITER = '~%^';

// Menu object names
export const ENTER_BY = 'enterBy';
export const APPROVE_BY = 'approveBy';

// Keyboard layout
export const UNICODE = 'unicodify';
export const PREETI = 'preetify';

// Form Related
export const BUILDING_CLASS = 'PHOENIX_EBPS_BUILDING_CLASS';
export const REJECTED_STATUS = 'PHOENIX_EBPS_REJECTED_STATUS';
export const PLOR_SANSODHAN = 'PHOENIX_EBPS_PLOR_SANSODHAN';
export const DOSRO_CHARAN_SANSODHAN = 'PHOENIX_EBPS_DOSRO_CHARAN_SANSODHAN';
export const PRABIDHIK_SANSODHAN = 'PHOENIX_EBPS_PRABIDHIK_SANSODHAN';
export const TASK_IDS = 'PHOENIX_EBPS_TASK_IDS';
export const ORG_IDS = 'PHOENIX_EBPS_ORG_IDS' + getClientSuffix();

// Date related
export const DAYS_TO_NOTIFY = 15;
export const DAYS_TO_URGENCY = 3;
export const BUILDING_CLASS_AREA_LIMIT_FEET = 999.99;
export const BUILDING_CLASS_AREA_LIMIT_METER = 92.9;

// File category
export const NIBEDAK_PHOTO = 'पासपोट साईज फोटो';
export const NIBEDAK_PHOTO_TITLE = 'घरधनीको पासपोट साईज फोटो';

// Default values
export const DEFAULT_UNIT_LENGTH = 'FEET';
export const DEFAULT_UNIT_AREA = 'SQUARE FEET';
// export const DEFAULT_UNIT_LENGTH = 'METRE';
// export const DEFAULT_UNIT_AREA = 'SQUARE METRE';