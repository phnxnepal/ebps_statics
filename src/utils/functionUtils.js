import React from 'react';

import { CURRENT_BUILD_PERMIT, USER_INFO, TOKEN_NAME, MENU_LIST, ENTER_BY, APPROVE_BY, BUILDING_CLASS, FORM_GROUP_MASTER } from './constants';
// import { toast } from 'react-semantic-toasts';
import { toast, Slide } from 'react-toastify';
import { checkError } from './dataUtils';
import { getLocalStorage, setLocalStorage } from './secureLS';
import { translateNumber, getInputType } from './langUtils';
import { PrintIdentifiers, genericRemoveOnPrint, getCheckboxLabel, PrintSelectors } from './printUtils';
import { userTypeOptionAll } from './optionUtils';
import { UserType } from './userTypeUtils';
import { LSKey } from './enums/localStorageKeys';
export const getUniqueFromArray = (array, uniqueKey) => {
	return [...new Set(array.map(item => item[uniqueKey]))];
};

export function isEmpty(obj) {
	for (var key in obj) {
		if (obj.hasOwnProperty(key)) return false;
	}
	return true;
}

export const replaceDot = string => {
	return string.replace('.', '_');
};

export const parseKey = key => {
	const keySplit = key.split('_');
	const groupId = keySplit[0];
	const classId = `${groupId}.${keySplit[keySplit.length - 2]}`;
	const field = `${keySplit[keySplit.length - 1]}`;

	return { groupId, classId, field };
};

export const flattenMessages = (nestedMessages, prefix = '') => {
	if (nestedMessages === null) {
		return {};
	}
	return Object.keys(nestedMessages).reduce((messages, key) => {
		const value = nestedMessages[key];
		const prefixedKey = prefix ? `${prefix}.${key}` : key;

		if (typeof value === 'string') {
			Object.assign(messages, { [prefixedKey]: value });
		} else {
			Object.assign(messages, flattenMessages(value, prefixedKey));
		}

		return messages;
	}, {});
};
//hardcode
export const printformat = (afterprint, options, formvalue, state) => {
	if (!state) {
		return afterprint;
	} else {
		if (options != null && formvalue != null) {
			let val = options.find(each => each.value === formvalue);
			val = val && val.text;

			return <span>{val}&nbsp;</span>;
		} else {
			return <span>{formvalue}&nbsp;</span>;
		}
	}
};

export const printformatt = (afterprint, options = null, formvalue, state, dname = null) => {
	let newspan = document.createElement('span');
	let btag = document.createElement('b');
	if (!state) {
		return afterprint;
	} else {
		let val = options ? options.find(each => each.text === formvalue) : formvalue;
		if (val) {
			val = val === 'अन्य (तल उल्लेख गर्नुहोस)' ? document.querySelector('input[name=dName]').value : options ? val.text : val;
		}

		btag.innerText = val;
		newspan.append(btag);

		return newspan;
	}
};

// export const makespecialprintformat = (doc, otherData) => {
//   const designerinfo = checkError(otherData);

//   const dUserName = designerinfo.enterBy ? designerinfo.enterBy.userName : '';

//   const supOptions = [
//     { key: 'super', value: dUserName, text: dUserName },
//     { key: 'others', value: 'other', text: 'अन्य (तल उल्लेख गर्नुहोस)' }
//   ];

//   Array.prototype.slice
//     .call(doc['getElementsByClassName']('anusuchighaSpecial'))
//     .forEach(each => {
//       const toreplace = printformatt(
//         null,
//         supOptions,
//         each.firstChild.firstChild['innerText'],
//         true,
//         otherData.dName
//       );
//       toreplace && each.firstChild.replaceWith(toreplace);
//     });
// };

const makeprintformathelper = (value, each, parent = null) => {
	let newspanhtml = document.createElement('span');
	let btag = document.createElement('b');
	//each[value] = translateNumber(each[value], getInputType());
	btag.innerText = ` ${each[value].trim()}`;

	newspanhtml.append(btag);
	if (parent === 'parent' && each.parentNode.tagName === 'DIV') {
		each.parentNode.replaceWith(newspanhtml);
	}
	// else if (parent == 'parent2') {
	// if (each.parentNode.parentNode.parentNode.parentNode.tagName == 'TD') {
	//   console.log(each);
	//   each.parentNode.parentNode.parentNode.replaceWith(newspanhtml);
	// } else {
	// each.parentNode.replaceWith(newspanhtml);
	// }
	// }
	else {
		each.replaceWith(newspanhtml);
	}
};

//(classname|element,selector,innertext|value)
export const makeprintformat = (doc, by, selector, value, selectOptions = null, parent = null) => {
	if (by === '15dayspecial') {
		special15days(doc);
	} else if (by === 'removeOnPrint') {
		try {
			const removeThis = doc.querySelectorAll('.remove-on-print');
			Array.from(removeThis).forEach(el => el.remove());
			const colSpanElement = doc.querySelector('.print-col-span');
			colSpanElement && colSpanElement.setAttribute('colspan', colSpanElement.dataset.colspan);
		} catch (err) {
			console.log('Error in print rajasow', err);
		}
	} else if (by === PrintIdentifiers.GENERIC_REMOVE_ON_PRINT) {
		genericRemoveOnPrint(doc);
	} else if (by === PrintIdentifiers.CHECKBOX_LABEL) {
		getCheckboxLabel(doc);
	} else {
		Array.prototype.slice.call(doc[by](selector)).forEach(each => {
			if (selector === 'input') {
				if (each.type === 'checkbox' && each.parentNode.className === 'ui checked checkbox') {
					makeprintformathelper(value, each, 'parent');
				} else if (each.type === 'radio' && each.checked) {
					makeprintformathelper(value, each, 'parent');
				} else if (each.type === 'radio' && !each.checked) {
					each.parentNode.remove();
				} else if (each.type === 'checkbox' && each.parentNode.className === 'ui checkbox') {
					/**
					 * @TODO refactor... wtf
					 */
					if (each.parentNode.parentNode.parentNode.parentNode && each.parentNode.parentNode.parentNode.tagName === 'TD') {
						each.parentNode.parentNode.parentNode.remove();
					} else {
						each.parentNode.remove();
					}
				} else {
					makeprintformathelper(value, each, parent);
				}
			} else if (selector === 'ui selection dropdown' || selector === 'ui dropdown') {
				const toreplace = printformatt(null, selectOptions, each.firstChild[value], true);
				toreplace && each.replaceWith(toreplace);
			} else if (selector === 'equal width fields inline-group') {
				let newspanhtml = document.createElement('span');
				let btag = document.createElement('b');
				btag.innerText = `${each[value].trim()}`;
				newspanhtml.append(btag);
				each.replaceWith(newspanhtml);
			} else if (selector === 'fields inline-group') {
				let newspanhtml = document.createElement('span');
				let btag = document.createElement('b');

				if (each.tagName === 'DIV') {
					btag.innerText = each.querySelector('input') ? each.querySelector('input').value : '';
					newspanhtml.innerText = each.querySelector('span') ? each.querySelector('span').innerText : '';
					newspanhtml.append(btag);
				}
				each.replaceWith(newspanhtml);
			} else if (selector === 'ui borderless menu') {
				Array.prototype.slice.call(each.children).forEach(each => {
					if (each.className === 'active item') {
						let newspanhtml = document.createElement('span');
						let btag = document.createElement('b');

						btag.innerText = `${each[value].trim()}`;
						newspanhtml.append(btag);
						each.parentNode.replaceWith(newspanhtml);
					} else {
						each.remove();
					}
				});
			} else if (selector === 'dashedForm-control') {
				let value = each.tagName === 'DIV' ? each.firstChild.value : each.value;
				if (!isEmpty(value)) {
					let newspanhtml = document.createElement('span');
					let btag = document.createElement('b');

					value = translateNumber(value, getInputType());
					//console.log(value);

					btag.innerText = `${value.trim()}`;
					btag.classList.add('data-padding')
					newspanhtml.append(btag);
					each.replaceWith(newspanhtml);
				} else {
					let newspanhtml = document.createElement('span');
					newspanhtml.className = each.className.includes('compact-form-input') ? 
						'dash-input-compact' : 
						each.className.includes('full-line') ? 
							'dash-full-line-input': 
							'dash-input';
					each.replaceWith(newspanhtml);
				}
			} else if (selector === PrintSelectors.DASHED_DROPDOWN) {
				let value = each.firstChild ? each.firstChild.innerText : '';
				if (!isEmpty(value)) {
					let newspanhtml = document.createElement('span');
					let btag = document.createElement('b');

					btag.innerText = `${value.trim()}`;
					newspanhtml.append(btag);
					each.replaceWith(newspanhtml);
				} else {
					let newspanhtml = document.createElement('span');
					newspanhtml.className = each.className.includes('compact-form-input') ? 'dash-input-compact' : 'dash-input';
					each.replaceWith(newspanhtml);
				}
			} else if (selector === 'ui checkbox') {
				if (each.firstChild.type === 'radio' && each.firstChild.checked) {
					let newspanhtml = document.createElement('span');
					let btag = document.createElement('b');
					if (value === 'label') {
						const label = doc.querySelector('label');
						if (label) {
							btag.innerText = label.innerText;
						}
					} else {
						btag.innerText = `${each.firstChild[value].trim()}`;
					}
					newspanhtml.append(btag);
					each.replaceWith(newspanhtml);
				} else if (each.firstChild.type === 'radio' && !each.firstChild.checked) {
					each.firstChild.parentNode.remove();
				}
			} else if (selector === 'textarea') {
				let newspanhtml = document.createElement('span');
				let btag = document.createElement('b');

				btag.innerText = `${each[value].trim()}`;
				newspanhtml.append(btag);
				each.replaceWith(newspanhtml);
			} else if (selector === 'ui input') {
				// each.remove();
			} else {
				let newspanhtml = document.createElement('span');
				newspanhtml.innerText = `${each[value].trim()}`;
				each.replaceWith(newspanhtml);
			}
		});
	}
};

export const getBuildPermit = () => {
	const buildPermitObj = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));
	return buildPermitObj.applicantNo;
};

export const getBuildPermitObj = () => {
	const buildPermitObj = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));
	return buildPermitObj;
};

export const getUserInfoObj = () => {
	return JSON.parse(getLocalStorage(USER_INFO));
};

// export const showToast = message => {
//   setTimeout(() => {
//     toast({
//       title: 'Data saved',
//       description: message
//     });
//   }, 1000);
// };

export const showToast = message => {
	toast.success(message, { transition: Slide });
};

export const showWarnToast = message => {
	toast.warn(message, { transition: Slide });
}

export const showErrorToast = message => {
	toast.error(message, { transition: Slide });
};

export const setBuildPermit = obj => {
	setLocalStorage(CURRENT_BUILD_PERMIT, JSON.stringify(obj));
};

export const getToken = () => {
	const token = getLocalStorage(TOKEN_NAME);

	if (token) {
		if (token.startsWith('Bearer')) {
			return token;
		} else {
			return `Bearer ${token}`;
		}
	}
};

export const getUserRole = () => {
	const userInfo = JSON.parse(getLocalStorage(USER_INFO));
	return userInfo ? userInfo.userType : '';
};

export const isDesigner = () => getUserRole() === 'D';
export const isSubEngineer = () => getUserRole() === 'B';
export const isSerOrEng = () => getUserRole() === 'A' || getUserRole() === 'B';

export const getUserTypeValue = code => {
	if (code === 'A') {
		return 'Engineer';
	} else if (code === 'B') {
		return 'Sub-Engineer';
	} else if (code === UserType.ADMIN) {
		return 'Chief';
	} else if (code === 'D') {
		return 'Designer';
	} else if (code === 'AD') {
		return 'Amin';
	} else if (code === 'R') {
		return 'Rajaswo';
	} else if (code === UserType.TECH_ADMIN) {
		return 'Tech Admin';
	} else if (code === UserType.ORGANIZATION_ADMIN) {
		return 'Admin';
	} else {
		return 'User';
	}
};

export const getUserTypeValueNepali = code => {
	try {
		const userTypeAllLocal = JSON.parse(getLocalStorage(LSKey.USER_TYPE_ALL));

		if (userTypeAllLocal) {
			return userTypeAllLocal.find(type => type.userType === code).designation;
		} else {
			return userTypeOptionAll.find(type => type.value === code).text;
		}
	} catch {
		return 'प्रयोगकर्ता';
	}
};

export const getUserNameNepali = name => {
	if (name === 'Engineer') {
		return 'ईन्जिनियर';
	} else if (name === 'Sub Engineer') {
		return 'सब-ईन्जिनियर';
	} else if (name === 'Chief') {
		return 'अधिकृत';
	} else if (name === 'Designer') {
		return 'डिजाइनर';
	} else if (name === 'Amin') {
		return 'अमिन';
	} else if (name === 'Rajasow') {
		return 'राजस्व';
	} else {
		return 'User';
	}
};

//     if (code.equalsIgnoreCase("A")) {
//         return "स्वीकृत भएको";
//     } else if (code.equalsIgnoreCase("R")) {
//         return "अस्वीकृत गरिएको";
//     } else if (code.equalsIgnoreCase("P")) {
//         return "कार्यवाहि चलिरहेको";
//     } else {
//         return "दर्ता गरिएको";
//     }

export const getUserStatusNepali = code => {
	if (code === 'A') {
		return 'स्वीकृत गरिएको छ';
	} else if (code === 'B') {
		return 'रद्ध गरिएको छ';
	} else {
		return 'दर्ता गरिएको छ';
	}
};

export const getActionObject = code => {
	if (code === 'A') {
		return 'enginieerAction';
	} else if (code === 'B') {
		return 'subEnginieerAction';
	} else if (code === 'R') {
		return 'rajaswarajasowAction';
	} else if (code === 'C') {
		return 'chiefAction';
	} else if (code === 'D') {
		return 'designerAction';
	} else {
		return 'applicationAction';
	}
};

export const getStartingGroupIdPostionFromMenuList = () => {
	const groupListId = JSON.parse(getLocalStorage(FORM_GROUP_MASTER)).map(group => group.id);
	let finalMenu = [];
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	groupListId.forEach((gId, index) => {
		finalMenu[index] = menuList && menuList.find(menu => String(gId) === String(menu.groupId));
	});
	return finalMenu.map(menu => menu && { groupId: menu.groupId, position: menu.position });
};

export const getNextUrl = (currentUrl, step = 1) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let url = '';
	menuList.forEach((row, index) => {
		if (row.viewURL && row.viewURL.trim() === currentUrl) {
			if (menuList.length > index + step) {
				url = menuList[index + step].viewURL;
				if (url === '/user/forms/forward-to-next') {
					url += `/${menuList[index + step].groupId}`;
				}
			}
		}
	});
	return url ? url.trim() : '/';
};

export const getNextGroupUrl = (currentUrl, groupId) => {
	const menuList = JSON.parse(getLocalStorage(MENU_LIST));
	let url = '';
	menuList.forEach((row, index) => {
		if (row.viewURL && row.viewURL.trim() === currentUrl && parseInt(row.groupId) === parseInt(groupId)) {
			if (menuList.length > index + 1) {
				url = menuList[index + 2].viewURL;
			}
		}
	});
	return url ? url.trim() : '/';
};

export const currentUrl = mainUrl => {
	const url = currentUrlObj(mainUrl);
	return url ? url.id : '';
};

export const currentUrlObj = mainUrl => {
	const currentUrl = JSON.parse(getLocalStorage(MENU_LIST));
	let url = currentUrl.find(row => row && row.viewURL && row.viewURL.trim() === mainUrl.trim());
	return url ? url : {};
};

export const getFileCatId = (fileCategories, currentUrl) => {
	try {
		const menuList = JSON.parse(getLocalStorage(MENU_LIST));
		// let formName = '';
		let formId = '';
		menuList.forEach((row, index) => {
			if (row.viewURL && row.viewURL.trim() === currentUrl) {
				// formName = row.formName;
				formId = row.id;
			}
		});

		const fileCat = fileCategories.find(row => row.formName.id === formId || row.formId === formId);
		//console.log('formname', formName, fileCat);

		return {
			fileCatId: fileCat.id,
			hasMultipleFiles: hasMultipleFiles(fileCat.isMultiple),
		};
	} catch {
		return { fileCatId: '', hasMultipleFiles: false };
	}
};

export const hasMultipleFiles = value => {
	return typeof value === 'string' && value.toUpperCase() === 'Y';
};

export const getUnitValue = unitData => {
	if (unitData === 'METRE' || unitData === 'METER') {
		return 'मिटर';
	} else if (unitData === 'SQUARE METRE' || unitData === 'SQUARE METER') {
		return 'वर्ग मिटर';
	} else if (unitData === 'SQUARE FEET') {
		return 'वर्ग फिट'
	} else return 'फिट'
};

export const getAreaUnitValue = unitData => {
	if (unitData === 'METRE' || unitData === 'METER') {
		return 'वर्ग मिटर';
	} else {
		return 'वर्ग फिट';
	}
};

export const getAreaUnitValueFromLength = unitData => {
	if (unitData === 'SQUARE METRE' || unitData === 'SQUARE METER') {
		return 'वर्ग मिटर';
	} else {
		return 'वर्ग फिट';
	}
};

export const getFormPermission = url => {
	try {
		const menuList = JSON.parse(getLocalStorage(MENU_LIST));
		const { approveBy: approveBy = 'N', enterBy: enterBy = 'N' } = menuList.find(menu => menu.viewURL && menu.viewURL.trim() === url);

		return { approveBy, enterBy };
	} catch (err) {
		return { approveBy: 'N', enterBy: 'N' };
	}
};

export const getSplitUrl = (urlName, splitType) => {
	return urlName ? urlName.split(splitType)[urlName.split(splitType).length - 1] : '';
};

//permission[type] ..... { approveBy: 'N', enterBy: 'N' }[approveBy]=N

/**
 * @deprecated Use getPermission() or getSavePermission or getApprovePermission instead.
 */
export const hasPermission = (url, type) => {
	const permission = getFormPermission(url);
	return permission[type] === 'Y';
};

export const getPermission = type => url => {
	const permission = getFormPermission(url);
	return permission[type] === 'Y';
};

export const getSavePermission = getPermission(ENTER_BY);
export const getApprovePermission = getPermission(APPROVE_BY);

export const getPreviousFormStatus = (data, prevUrlObj) => {
	if (!prevUrlObj) {
		return true;
	}

	if (isEmpty(checkError(data))) {
		// console.log('check error empty', data);
		return false;
	} else {
		const { erStatus, serStatus, rwStatus } = data;
		const userRole = getUserRole();

		// console.log('has prev now ', serStatus, userRole);
		return (erStatus === 'A' && userRole === 'A') || (userRole === 'B' && serStatus === 'A') || (userRole === 'R' && rwStatus === 'A');
	}
};

export const getBuildingClass = () => {
	const bClass = getLocalStorage(BUILDING_CLASS);
	return bClass ? bClass : '';
};
function special15days(doc) {
	Array.from(doc.getElementsByTagName('th')).forEach(function(each) {
		each.style.padding = '0 5px';
	});
	Array.from(doc.getElementsByTagName('td')).forEach(function(each) {
		each.style.padding = '0 5px';
	});
	Array.from(doc.getElementsByClassName('equal width fields')).forEach(function(each) {
		each.style.margin = '0';
	});
	Array.from(doc.getElementsByTagName('p')).forEach(function(each) {
		each.style.setProperty('margin', '0', 'important');
	});
	// doc
	//   .getElementsByClassName("heading")
	//   .style.setProperty("font-size", "25px", "important");
	Array.from(doc.getElementsByTagName('h2')).forEach(function(each) {
		each.style.setProperty('font-size', '18px', 'important');
	});
	Array.from(doc.getElementsByTagName('h2')).forEach(function(each) {
		each.style.setProperty('font-weight', 'bold', 'important');
	});
	Array.from(doc.getElementsByTagName('p')).forEach(function(each) {
		each.style.setProperty('text-align', 'justify', 'important');
	});
}

export function brack(doc) {
	// console.log('yeta', doc)
	Array.from(doc.getElementsByClassName('brackets')).forEach(function(each) {
		each.firstChild.remove();
		each.lastChild.remove();
	});
}
