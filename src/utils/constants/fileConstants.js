export const FILE_SIZE = 50 * 1024 * 1024;
export const SUPPORTED_FORMATS = [
	'image/jpg',
	'image/jpeg',
	'image/gif',
	'image/png',
	'application/pdf',
	//Autocad Formats
	'image/vnd.dwg',
	'image/vnd.dxf',
	'model/vnd.dwf',

];

export const SUPPORTED_FORMATS_IMAGE = [
	'image/jpg',
	'image/jpeg',
	'image/gif',
	'image/png',
];

export const SUPPORTED_FORMATS_FILE = [
	'dwg',
	'dxf',
	'dwf',
]

export const SUPPORTED_IMAGE_EXTENSTIONS = [
	'jpg',
	'jpeg',
	'gif',
	'png',
];

export const PDF = 'pdf';
export const IMAGE = 'image';
export const AUTOCAD = 'autocad';
