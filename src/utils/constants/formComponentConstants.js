export const DatePosition = {
	TOP: 'top',
	BOTTOM: 'bottom',
};

export const TextSize = {
	BIG: 'big',
	MEDIUM: 'medium',
	SMALL: 'small',
	TINY: 'tiny',
};
