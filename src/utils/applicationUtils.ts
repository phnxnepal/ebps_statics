import { NamsariStatus, TPermitGetResponse, TTaskListRow, TCertificateNoteData } from "./enums/apiTypes";
import { setLocalStorage } from "./secureLS";
import { CURRENT_BUILD_PERMIT, MENU_LIST, BUILDING_CLASS } from "./constants";
import { getGroupName } from "./dataUtils";
import { LSKey } from "./enums/localStorageKeys";
import { PermitObject, ApplicationListRawObject } from "../interfaces/DashboardInterfaces";


const getCertificateNote = (nameTransferId: number, data: TCertificateNoteData[]) => {
    let retData = ''
    try {
        if (data && Array.isArray(data)) {
            const reqRow = data.find(row => row.nameTransferId === nameTransferId)
            if (reqRow) {
                retData = JSON.parse(reqRow.jsonData).sasthaYaExtraName
            }
        }
    } catch (error) {
        console.log(error);
    }
    return retData;
}

export const handlePermitResponse = (response: TPermitGetResponse, currentPermit: TTaskListRow) => {
    setLocalStorage(CURRENT_BUILD_PERMIT, JSON.stringify(response.data.data));

    const hasDesignerChanged: boolean = currentPermit.designerChangeStatus ? currentPermit.designerChangeStatus === 'Y' : false;
    const newDesignerName: string = currentPermit.newDesignerName ? currentPermit.newDesignerName : '';
    const menu = response.data.menu;
    const buildingClass: string = response.data.buildingClass ? response.data.buildingClass : '';
    const namsariStatus: NamsariStatus = response.data.naamsariStatus ? response.data.naamsariStatus : '';
    const certificateNote: string = namsariStatus === 'C' && response.data.certificateNoteData ? getCertificateNote(currentPermit.nameTransaferId, response.data.certificateNoteData) : ''

    const groupName = getGroupName();

    menu.forEach((mn) => {
        if (mn.viewURL && mn.viewURL.trim() === '/user/forms/forward-to-next') {
            try {
                mn.formName = `${mn.formName} ${
                    //@ts-ignore
                    groupName.find((grp) => String(grp.id) === String(mn.groupId)).name
                    }`;
            } catch (err) {
                console.log('Unable to fetch group names master.', err);
            }
        }
    });

    if (['C', 'R'].includes(namsariStatus)) {
        menu.push({
            approveBy: null,
            enterBy: null,
            formName: 'नामसारी विवरण',
            groupId: null,
            id: null,
            position: null,
            viewURL: '/user/forms/namsari-history',
        });
    }

    setLocalStorage(MENU_LIST, JSON.stringify(menu));
    setLocalStorage(BUILDING_CLASS, buildingClass === 'NA' ? '' : buildingClass);
    setLocalStorage(LSKey.NAMSARI_STATUS, namsariStatus);
    setLocalStorage(LSKey.HAS_DESIGNER_CHANGED, hasDesignerChanged);
    setLocalStorage(LSKey.NEW_DESIGNER_NAME, newDesignerName);
    setLocalStorage(LSKey.CERTIFICATE_NOTE_DATA, certificateNote);
}

export const namsariFilter = (keepNamsari = false) => (permit: PermitObject | ApplicationListRawObject) => {
    if (keepNamsari) return permit.naamsariStatus === 'R';
    else return permit.naamsariStatus !== 'R';
} 