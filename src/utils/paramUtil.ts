interface ApiParamObject {
    api: string;
    objName: string;
    form?: boolean;
    utility?: boolean;
    concatToApi?: ConcatToApi | null;
}

interface ConcatToApi {
    prefix: string;
    fieldName: string;
    needsTranslation?: boolean;
}

/**
 * @see getFormContainerData()
 * Parameter api array obj for getFormContainerData Action
 */
export class ApiParam {

    private form?: boolean;
    private utility?: boolean;
    private concatToApi?: ConcatToApi;

    constructor(private api: string, private objName: string) {
    }

    setForm() {
        this.form = true;
        return this;
    }

    setUtility() {
        this.utility = true;
        return this;
    }

    setConcatToApi(v: ConcatToApi) {
        this.concatToApi = v
        return this;
    }

    getParams(): ApiParamObject {
        return { api: this.api, objName: this.objName, form: this.form || false, utility: this.utility || false, concatToApi: this.concatToApi || null }
    }

}