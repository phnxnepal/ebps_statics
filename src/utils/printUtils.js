// Print format identifier
export const PrintIdentifiers = {
	GENERIC_REMOVE_ON_PRINT: 'generic-remove-on-print',
	CHECKBOX_LABEL: 'checkbox-label',
};

export const PrintSelectors = {
	DASHED_DROPDOWN: 'ui dropdown dashedForm-control',
};

export const PrintParams = {
	TEXT_AREA: {
		p1: ['getElementsByTagName', 'textarea', 'value'],
	},
	INLINE_FIELD: {
		p2: ['getElementsByClassName', 'fields inline-group', 'innerText'],
	},
	REMOVE_ON_PRINT: {
		p3: ['removeOnPrint'],
	},
	DASHED_INPUT: {
		p4: ['getElementsByClassName', 'dashedForm-control', 'value'],
	},
};

export const genericRemoveOnPrint = (doc) => {
	try {
		const removeThis = doc.querySelectorAll('.remove-on-print');
		Array.from(removeThis).forEach((el) => el.remove());
	} catch (err) {
		console.log('Error in print generic remove element,', err);
	}
};

export const getCheckboxLabel = (doc) => {
	try {
		const checkboxes = doc.getElementsByClassName('ui radio checkbox');
		Array.from(checkboxes).forEach((checkbox) => {
			const input = checkbox.querySelector('input');
			if (input && input.checked) {
				const label = checkbox.querySelector('label');
				if (label) {
					const value = label.innerHTML;
					let newspanhtml = document.createElement('span');
					let btag = document.createElement('b');
					btag.textContent = value;
					btag.classList.add('data-padding')
					newspanhtml.append(btag);
					checkbox.replaceWith(newspanhtml);
				}
			} else {
				checkbox.remove();
			}
		});

		const checkboxesOnly = doc.getElementsByClassName('ui checkbox');
		Array.from(checkboxesOnly).forEach((checkbox) => {
			const input = checkbox.querySelector('input');
			if (input && input.checked) {
				const label = checkbox.querySelector('label');
				if (label) {
					const value = label.innerHTML;
					let newspanhtml = document.createElement('span');
					let btag = document.createElement('b');
					btag.innerText = value + ', ';
					btag.classList.add('data-padding')
					newspanhtml.append(btag);
					checkbox.replaceWith(newspanhtml);
				}
			} else {
				checkbox.remove();
			}
		});
	} catch (err) {
		console.log('Error in print generic remove element,', err);
	}
};
