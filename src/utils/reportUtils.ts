import { translateEngToNepWithZero } from "./langUtils";
import { getJsonDataV2 } from "./dataUtils";

interface ReportMap {
    label: string,
    position: number,
}

// interface A {
//     key: ('totaReg' | 'allowancePaper' | 'superStructure' | 'totalComplete' | 'buildingBuildCertificate' | 'rajaswaVoucher' | 'discard' |)
// }


// const ReportApiObject = [
//     'totalReg',
//     'allowancePaper',
//     'superStructure',
//     'totalComplete',
//     'buildingBuildCertificate',
//     'rajaswaVoucher',
//     'discard',
// ]

export const reportMappings = {
    totalReg: { label: 'जम्मा दर्ता', position: 1 },
    allowancePaper: { label: 'प्रथम चरणको अस्थायी निर्माण इजाजत पत्र संख्या', position: 2 },
    superStructure: { label: 'दोस्र‍ो चरणको ( सुपरस्ट्रक्चर निर्माण कार्यको ) इजाजत पत्र संख्या', position: 3 },
    totalComplete: { label: 'भवन निर्माण कार्य सम्‍पन्‍न प्रमाण पत्र संख्या', position: 4 },
    buildingBuildCertificate: { label: 'नामसारी', position: 5 },
    // rajaswaVoucher: { label: 'कुल राजस्व', position: 6 },
    discard: { label: 'रद्द गरिएको', position: 7 },
};

export const rajasowFields = ['rajaswaVoucher', 'PahilocharanBillVuktani', 'DosrocharanBillVuktani', 'NamsariBillVuktani', 'SansodhanBillVuktani']

export const getReportMapping = (key: string): ReportMap => {
    //@ts-ignore
    return reportMappings[key];
};

export const getReportKey = (position: number) => {
    try {
        return Object.entries(reportMappings).find(([_, value]) => value.position === position)![0]
    } catch  {
        return ''
    }
}

export const formatReport = (reportPermits: {}, isDesigner: boolean) => {
    const nonAmountReports = Object.keys(reportPermits)
        .filter(row => reportMappings.hasOwnProperty(row))
        .map((key) => {
            const { label, position } = getReportMapping(key);

            return {
                position,
                label,
                //@ts-ignore
                count: translateEngToNepWithZero(reportPermits[key].length),
            };
        });

    const grandTotal: number = Object.keys(reportPermits)
        .filter(row => rajasowFields.includes(row))
        .reduce((acc, billObj) => {
            //@ts-ignore
            const rajaswa: object[] = reportPermits[billObj];
            const totalArr = rajaswa.map(el => {
                //@ts-ignore
                const jsonData = getJsonDataV2(el, 'jsonData');
                if (billObj === 'rajaswaVoucher') {
                    return jsonData.totalAmt;
                } else return jsonData.vuktaniRakam;
            });

            const total = totalArr ? totalArr.reduce((total, curr) => (total += +curr), 0.0) : 0;
            return acc + total;
        }, 0.0);

    const formattedTotal = grandTotal ? Number(grandTotal).toLocaleString('en-IN', { maximumFractionDigits: 2 }) : 0;

    const allReports = isDesigner ? [...nonAmountReports] : [
        ...nonAmountReports,
        {
            position: 6,
            label: 'कुल राजस्व',
            count: `रु.${formattedTotal}`,
        },
    ];

    const reorderedReports = allReports.length > 0 ? allReports.sort((a, b) => a.position - b.position) : [];
    return reorderedReports;
}