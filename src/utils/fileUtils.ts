type FileObject = {
    fileCatId: number;
}

export const updateFileState = (prevStateFiles: FileObject[], fileObject: FileObject) => {
    const currentFileCat = fileObject.fileCatId
    return prevStateFiles.filter(obj => obj.fileCatId !== currentFileCat).concat(fileObject)
}