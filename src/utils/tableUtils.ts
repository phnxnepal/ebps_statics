export const setupTableProps = {
    size: "small",
    compact: "very",
    striped: true,
    celled: true,
    textAlign: "center",
    selectable: true
}