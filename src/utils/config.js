export const getBackUrl = () => {
  if (process.env.NODE_ENV === 'production') {
    // return process.env.REACT_APP_PROD_BACK_URL;
    // return 'http://stg.phoenixsolutions.com.np:8080/EBPS/';
    // { provide: 'LoginUrl', useValue:
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2)) + '/'
  //  },
  } else {
    return process.env.REACT_APP_DEV_BACK_URL;
  }
};

export const getTempUrls = () => {
  if (process.env.NODE_ENV === 'development') {
    return process.env.REACT_APP_TEMP_URLS;
  }
};

export const getDocUrl = () => {
  const backUrl = getBackUrl();
  return `${backUrl.substring(0, backUrl.length - 1)}Document`;
};

export const getClient = () => {
  if (process.env.NODE_ENV === 'production') {
    return process.env.REACT_APP_DEV_CLIENT;
  } else {
    return process.env.REACT_APP_PROD_CLIENT;
  }
};

export const getClientSuffix = () => {
  if (process.env.NODE_ENV === 'production') {
    const client = window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2))
    if (client) {
      return '-' + client.replace('/', '');
    } else {
      return ''
    }
  } else {
    return '';
  }
}

export const getRecaptchaSiteKey = () => {
  if (process.env.NODE_ENV === 'production') {
    return process.env.REACT_APP_PROD_RECAPTCHA_SITE_KEY
  } else {
    return process.env.REACT_APP_DEV_RECAPTCHA_SITE_KEY
  }
}


export const getRecaptchaSecretKey = () => {
  if (process.env.NODE_ENV === 'production') {
    return process.env.REACT_APP_PROD_RECAPTCHA_SECRET_KEY
  } else {
    return process.env.REACT_APP_DEV_RECAPTCHA_SECRET_KEY
  }
}