import * as Yup from 'yup';
import { commonMessages, numberMessages } from '../data/validationData';

export const validateRequiredNormalNumber = Yup.number()
	.typeError(numberMessages.number)
	.required(commonMessages.required);
