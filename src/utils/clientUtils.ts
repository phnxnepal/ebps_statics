import { getLocalStorage } from './secureLS';
import { USER_INFO } from './constants';

export enum Client {
	ILLAM = 'illam',
	KANKAI = 'kankai',
	MECHI = 'mechi',
	BIRTAMOD = 'birtamod',
	DAMAK = 'damak',
	BARDIBAS = 'bardibas',
	SUNDARHARAICHA = 'sundarharaicha',
	INARUWA = 'inaruwa',
	KAMALAMAI = 'kamalamai',
	BIRATNAGAR = 'biratnagar',
	KAGEYSHOWRI = 'manahara',
}

export const isClient = (clientCode: Client) => {
	try {
		const userObj: { organization: { organizationCode: string } } = JSON.parse(getLocalStorage(USER_INFO));
		const code = userObj.organization.organizationCode;
		return code === clientCode;
	} catch (err) {
		return false;
	}
};

export const checkClient = (orgCode: string, clientCode: Client) => {
	return orgCode === clientCode;
};

export const isIllam = isClient(Client.ILLAM);
export const isKankai = isClient(Client.KANKAI);
export const isDamak = isClient(Client.DAMAK);
export const isMechi = isClient(Client.MECHI);
export const isBirtamod = isClient(Client.BIRTAMOD);
export const isBardibas = isClient(Client.BARDIBAS);
export const isSundarHaraicha = isClient(Client.SUNDARHARAICHA);
export const isInaruwa = isClient(Client.INARUWA);
export const isKamalamai = isClient(Client.KAMALAMAI);
export const isBiratnagar = isClient(Client.BIRATNAGAR);
export const isKageyshowri = isClient(Client.KAGEYSHOWRI);

export const getClientCode = () => {
	try {
		const userObj: { organization: { organizationCode: string } } = JSON.parse(getLocalStorage(USER_INFO));
		const code = userObj.organization.organizationCode;
		return code ? code : 'default';
	} catch (err) {
		return 'default';
	}
};
