import { getLocalStorage } from './secureLS';
import { KEYBOARD, UNICODE, PREETI } from './constants';
import { isStringEmpty } from './stringUtils';
import { getMapper } from './nepali-mapping';

// saveCaretPosition = context => {
//   var selection = window.getSelection();
//   var range = selection.getRangeAt(0);
//   range.setStart(context, 0);
//   var len = range.toString().length;

//   return function restore() {
//     var pos = getTextNodeAtPosition(context, len);
//     selection.removeAllRanges();
//     var range = new Range();
//     range.setStart(pos.node, pos.position);
//     selection.addRange(range);
//   };
// };

// getTextNodeAtPosition = (root, index) => {
//   var lastNode = null;

//   var treeWalker = document.createTreeWalker(
//     root,
//     NodeFilter.SHOW_TEXT,
//     function next(elem) {
//       if (index >= elem.textContent.length) {
//         index -= elem.textContent.length;
//         lastNode = elem;
//         return NodeFilter.FILTER_REJECT;
//       }
//       return NodeFilter.FILTER_ACCEPT;
//     }
//   );
//   var c = treeWalker.nextNode();
//   return {
//     node: c ? c : root,
//     position: c ? index : 0
//   };
// };

export const translateWithSlashV2 = (e, funcname) => {
	//   var restore = saveCaretPosition();
	let retValue = '';
	let val = e.target ? e.target.value : e.toString();
	if (val) {
		// retValue = val.slice(0, -1);
		// let c = val.slice(-1);
		for (let c of val) {
			if (/\//.test(c)) {
				retValue += c;
			} else {
				let conv_char = getMapper(c.charCodeAt(0), funcname);
				retValue += conv_char || c;
			}
		}
	}
	return retValue;
};

export const translate = (e, funcname) => {
	//   var restore = saveCaretPosition();
	let retValue = '';
	let val = e.target ? e.target.value : e.toString();
	if (val) {
		// retValue = val.slice(0, -1);
		// let c = val.slice(-1);
		for (let c of val) {
			let conv_char = getMapper(c.charCodeAt(0), funcname);
			retValue += conv_char || c;
		}
	}
	return retValue;
};

export const translateNum = (e, funcname) => {
	let retValue = '';
	const value = e.target.value;
	if (funcname === PREETI) {
		if (value) {
			if (/[\u{0966}-\u{096f}0-9]/u.test(value)) {
				for (let c of value) {
					let conv_char = engToNep[c];
					retValue += conv_char || c;
				}
			}
		}
	} else {
		if (value) {
			if (/[\u{0966}-\u{096f}0-9]/u.test(value)) {
				for (let c of value) {
					let conv_char = engToNep[c];
					retValue += conv_char || c;
				}
			}
		}
	}
	return retValue;
};

export const translateWithSlash = (e, funcname) => {
	let retValue = '';
	const value = e.target.value;
	if (value) {
		retValue = value.slice(0, -1);
		let c = value.slice(-1);
		// for (let c of value) {
		if (/\//.test(c)) {
			retValue += c;
		} else {
			let conv_char = window[funcname](c.charCodeAt(0));
			retValue += conv_char || c;
		}
		// }
	}
	return retValue;
};

/**
 *
 * @param value Numeric value to translate.
 * @param funcname not required.
 * ONLY USE FOR PRINT. DONT USE FOR INPUT FIELDS.
 */
export const translateNumber = (value, funcname) => {
	let retValue = '';
	// const value = e.target.value ;
	if (value) {
		for (let c of value) {
			if (/\./.test(c)) {
				retValue += c;
			} else {
				let conv_char = window[UNICODE](c.charCodeAt(0));
				retValue += conv_char || c;
			}
		}
	}
	return retValue;
};

let nepToEng = {
	'\u0966': 0, // 0 -> ०
	'\u0967': 1, // 1 -> १
	'\u0968': 2, // 2 -> २
	'\u0969': 3, // 3 -> ३
	'\u096A': 4, // 4 -> ४
	'\u096B': 5, // 5 -> ५
	'\u096C': 6, // 6 -> ६
	'\u096D': 7, // 7 -> ७
	'\u096E': 8, // 8 -> ८
	'\u096F': 9, // 9 -> ९
};

let engToNep = {
	0: '\u0966', // 0 -> ०
	1: '\u0967', // 1 -> १
	2: '\u0968', // 2 -> २
	3: '\u0969', // 3 -> ३
	4: '\u096A', // 4 -> ४
	5: '\u096B', // 5 -> ५
	6: '\u096C', // 6 -> ६
	7: '\u096D', // 7 -> ७
	8: '\u096E', // 8 -> ८
	9: '\u096F', // 9 -> ९
};

// let engToNepPreeti = {
// 	'!': '\u0966', // 0 -> ०
// 	'@': '\u0967', // 1 -> १
// 	'#': '\u0968', // 2 -> २
// 	'$': '\u0969', // 3 -> ३
// 	'%': '\u096A', // 4 -> ४
// 	'^': '\u096B', // 5 -> ५
// 	'&': '\u096C', // 6 -> ६
// 	'*': '\u096D', // 7 -> ७
// 	'(': '\u096E', // 8 -> ८
// 	')': '\u096F' // 9 -> ९
// };

export const translateNepToEng = (value) => {
	let retValue = '';
	if (value) {
		for (let c of value.toString()) {
			if (/[\u{0966}-\u{096f}]/u.test(c)) {
				let conv_char = nepToEng[c];
				retValue += conv_char.toString() || c;
			} else retValue += c;
		}
	}
	return retValue;
};

export const translateEngToNep = (value) => {
	let retValue = '';
	if (value) {
		for (let c of value.toString()) {
			let conv_char = engToNep[c];
			retValue += conv_char || c;
		}
	}
	return retValue;
};

export const translateEngToNepWithZero = (value) => {
	let retValue = '';
	if (!isStringEmpty(value)) {
		for (let c of value.toString()) {
			let conv_char = engToNep[c];
			retValue += conv_char || c;
		}
	}
	return retValue;
};

export const translateDate = (value) => {
	// const funcname = getInputType();
	let retValue = '';
	if (value) {
		if (/[0-9]/.test(value)) {
			for (let c of value) {
				let conv_char = engToNep[c];
				retValue += conv_char || c;
			}
		}
	}
	return retValue;
};

export const getInputType = () => {
	let layout = UNICODE;
	try {
		layout = getLocalStorage(KEYBOARD);
		if (isStringEmpty(layout)) {
			layout = UNICODE;
		}
	} catch {
		layout = UNICODE;
	}
	return layout;
};
