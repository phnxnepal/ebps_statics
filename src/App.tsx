import React, { lazy, Suspense } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import './assets/scss/app.scss';
import 'jqwidgets-scripts/jqwidgets/styles/jqx.base.css';
import { connect } from 'react-redux';
import { loadReCaptcha } from 'react-recaptcha-v3';
import Login from './components/Login';
// import LoggedInRoutes from './routes/LoggedInRoutes';
import NotFound from './components/NotFound';
// import StartPage from './components/StartPage';

// Language
import messages_ne from './translations/ne.json';
import messages_en from './translations/en.json';
import { IntlProvider } from 'react-intl';
import { setLanguage } from './store/actions/general';

import { flattenMessages, getToken } from './utils/functionUtils';

// import "react-toastify/dist/ReactToastify.css";
import { toast } from 'react-toastify';
import AuthRoute from './utils/AuthRoute';
import Authorization from './utils/Authorization';
import Hotkey from './components/shared/Hotkey';
import { getLocalStorage } from './utils/secureLS';
import { Loading } from './components/shared/FallbackComponent';
import { getRecaptchaSiteKey } from './utils/config';

const LoggedInRoutes = lazy(() => import('./routes/LoggedInRoutes'));
const AdminRoutes = lazy(() => import('./routes/AdminRoutes'));
const PublicRoutes = lazy(() => import('./routes/PublicRoutes'));

const axios = require('axios');
require('dotenv').config();

toast.configure({
	autoClose: 2000,
	draggable: false,
});

const messages = {
	ne: messages_ne,
	en: messages_en,
};

// const Admin = Authorization(['C']);
const SetupUsers = Authorization(['C', 'A', 'ADM', 'TADM']);
const User = Authorization(['A', 'B', 'C', 'D', 'R', 'AD', 'ADM', 'TADM', 'E', 'F', 'G']);

class App extends React.PureComponent<{}> {
	constructor(props: {}) {
		super(props);
		axios.defaults.headers.common['Authorization'] = getToken();
		//@ts-ignore
		let language = getLocalStorage('language') || 'en';
		//@ts-ignore
		props.setLanguage(language);
	}

	componentDidMount() {
		try {
			loadReCaptcha(getRecaptchaSiteKey() || '');
		} catch (error) {
			console.log('Recaptcha Registration Failed');
		}
	}

	render() {
		return (
			<>
				<IntlProvider
					defaultLocale="ne"
					//@ts-ignore3
					locale={'ne'}
					//@ts-ignore
					messages={flattenMessages(messages['ne'])}
				>
					<Hotkey />
					<Router>
						<Suspense fallback={<Loading loading={true} />}>
							<Switch>
								<AuthRoute path="/login" component={Login} />
								<Route path="/user" component={User(LoggedInRoutes)} />
								<Route path="/admin" component={SetupUsers(AdminRoutes)} />
								<AuthRoute path="/" component={PublicRoutes} />
								<Route component={NotFound} />
							</Switch>
						</Suspense>
					</Router>
				</IntlProvider>
			</>
		);
	}
}
//@ts-ignore
const mapStateToProps = (state) => {
	return {
		getLanguage: state.root.general.lang,
		getTokenReload: state.root.general.token_reload,
	};
};

//@ts-ignore
const mapDispatchToProps = (dispatch) => {
	return {
		//@ts-ignore
		setLanguage: (lang) => dispatch(setLanguage(lang)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
