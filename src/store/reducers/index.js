import { combineReducers } from 'redux';
import { loginReducer } from './loginForm';
import uiReducer from './uiReducer';
import { formReducer } from './formReducer';
import { generalReducer } from './general';
import { dashboardReducer } from './dashboardReducer';
import { adminReducer } from './adminReducer';
import { publicReducer } from './publicReducer';

const rootReducer = combineReducers({
  login: loginReducer,
  ui: uiReducer,
  formData: formReducer,
  general: generalReducer,
  dashboard: dashboardReducer,
  admin: adminReducer,
  public: publicReducer,
});

export default rootReducer;
