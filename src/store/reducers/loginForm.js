import {
  LOADING_UI,
  LOADING_USER,
  SET_USER,
  SET_AUTHENTICATED,
  SET_UNAUTHENTICATED
} from '../types';

const initialState = {
  authenticated: false,
  loading: false
};

export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN_ERROR':
      return { ...state, loginError: action.data };

    case 'SUBMIT_BOTTOM':
      return { ...state, submitBtn: action.data };
    case LOADING_UI:
      return { ...state, loading: true };
    case LOADING_USER:
      return { ...state, loadingUser: true };
    case SET_AUTHENTICATED:
      return {
        ...state,
        authenticated: true
      };
    case SET_UNAUTHENTICATED:
      return initialState;
    case SET_USER:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
