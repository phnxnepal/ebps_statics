import { SET_ORGANIZATION_CODE, CLEAR_ORGANIZATION_CODE } from '../types';

const initialState = {
	organizationCode: '',
};

export const sessionReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_ORGANIZATION_CODE:
			return { ...state, organizationCode: action.payload };
		case CLEAR_ORGANIZATION_CODE:
			return { ...state, organizationCode: initialState.organizationCode };
		default:
			return state;
	}
};
