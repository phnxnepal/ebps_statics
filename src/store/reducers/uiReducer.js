import {
	SET_ERRORS,
	CLEAR_ERRORS,
	CLEAR_MODAL_ERRORS,
	LOADING_UI,
	STOP_LOADING,
	SET_UNSET_PERMIT_ERRORS,
	LOADING_MODAL,
	STOP_LOADING_MODAL,
	SET_MODAL_ERRORS,
	TOGGLE_NAVBAR,
	SET_FILE_LOADING,
	STOP_FILE_LOADING,
} from '../types';

const initialState = {
	loading: false,
	errors: null,
	isNavOpen: false,
	fileLoading: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case SET_ERRORS:
			return {
				...state,
				loading: false,
				errors: action.payload,
			};
		case SET_MODAL_ERRORS:
			return { ...state, loadingModal: false, modalErrors: action.payload };
		case SET_UNSET_PERMIT_ERRORS:
			return {
				...state,
				loading: false,
				errors: { message: 'Build permit not set.' },
			};
		case CLEAR_ERRORS:
			return {
				...state,
				// loading: false,
				errors: null,
			};
		case CLEAR_MODAL_ERRORS:
			return {
				...state,
				// loading: false,
				modalErrors: null,
			};
		case LOADING_UI:
			return {
				...state,
				loading: true,
				// errors: null,
			};
		case SET_FILE_LOADING:
			return {
				...state,
				fileLoading: {
					...state.fileLoading,
					[action.payload.id]: {
						progress: action.payload.progress,
						total: action.payload.total,
						percent: Math.round((100 * action.payload.progress) / action.payload.total),
					},
				},
				// errors: null,
			};
		case STOP_FILE_LOADING:
			return {
				...state,
				fileLoading: null,
				// errors: null,
			};
		case LOADING_MODAL:
			return {
				...state,
				loadingModal: true,
				// errors: null,
			};
		case STOP_LOADING_MODAL:
			return { ...state, loadingModal: false };
		case STOP_LOADING:
			return { ...state, loading: false };
		case TOGGLE_NAVBAR:
			return { ...state, isNavOpen: action.payload };
		default:
			return state;
	}
}
