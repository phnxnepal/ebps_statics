import { SET_FORM_DATA, CLEAR_FORM_DATA, SET_SUCCESS_FORM_SUBMIT, CLEAR_SUCCESS_STATE } from '../types';

const initialState = {};

export const formReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_FORM_DATA:
			return { ...state, ...action.payload };
		case CLEAR_FORM_DATA:
			return { ...initialState };
		// return { ...state, formData: {...state.formData, prevData: undefined} };
		case SET_SUCCESS_FORM_SUBMIT:
			return { ...state, success: action.payload, loading: false };
		case CLEAR_SUCCESS_STATE:
			return { ...state, success: null };
		default:
			return state;
	}
};
