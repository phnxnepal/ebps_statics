import { SET_ADMIN_DATA, CLEAR_ADMIN_DATA, SET_SUCCESS_ADMIN_SUBMIT, CLEAR_ADMIN_SUCCESS } from '../types';

const initialState = {};

export const adminReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_ADMIN_DATA:
			return { ...state, ...action.payload, loading: false };
		case CLEAR_ADMIN_DATA:
			return initialState;
		case SET_SUCCESS_ADMIN_SUBMIT:
			return { ...state, success: action.payload, loading: false };
		case CLEAR_ADMIN_SUCCESS:
			return { ...state, success: null };
		default:
			return state;
	}
};
