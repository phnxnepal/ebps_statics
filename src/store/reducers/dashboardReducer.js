import { SET_BUILD_PERMITS, SET_CURRENT_PERMIT, CLEAR_BUILD_PERMITS, SET_CURRENT_DATA, CLEAR_CURRENT_DATA, SET_USER_MENU, SET_ORGANIZATION } from '../types';

const initialState = {
	errors: null,
};

export const dashboardReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_BUILD_PERMITS:
			return { ...state, buildPermits: action.payload, loading: false };
		case CLEAR_BUILD_PERMITS:
			return { ...state, buildPermits: [], loading: false };
		case SET_USER_MENU:
			return { ...state, userMenu: action.payload };
		case SET_ORGANIZATION:
			return { ...state, ...action.payload };
		case SET_CURRENT_PERMIT:
			return { ...state, ...action.payload, loading: false };
		case SET_CURRENT_DATA:
			return { ...state, ...action.payload, loading: false };
		case CLEAR_CURRENT_DATA:
			return { ...initialState };
		default:
			return state;
	}
};