import { SET_LANGUAGE, SET_PRINT_CONTENT, SET_SHORTCUT } from '../types';

const initialState = {
  lang: 'en',
  printcontent: false,
  shortcut: false
};

export const generalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANGUAGE:
      return { ...state, lang: action.data };
    case SET_PRINT_CONTENT:
      return { ...state, printcontent: action.data };
    case SET_SHORTCUT:
      return { ...state, shortcut: !state.shortcut };

    default:
      return { ...state };
  }
};
