import {
	SET_PUBLICDESIGNER_DATA,
	CLEAR_PUBLICDESIGNER_DATA,
	SET_PUBLICMASON_DATA,
	CLEAR_PUBLICMASON_DATA,
	SET_SUCCESS_PUBLIC_SUBMIT,
	CLEAR_SUCCESS_PUBLIC_SUBMIT,
	SET_PUBLIC_DATA,
	CLEAR_PUBLIC_DATA,
} from '../types';

const initialState = {};

export const publicReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_PUBLICDESIGNER_DATA:
			return { ...state, ...action.payload };
		case CLEAR_PUBLICDESIGNER_DATA:
			return { ...initialState };
		// return { ...state, PUBLICDESIGNERData: {...state.PUBLICDESIGNERData, prevData: undefined} };
		case SET_PUBLICMASON_DATA:
			return { ...state, ...action.payload };
		case CLEAR_PUBLICMASON_DATA:
			return { ...initialState };
		case SET_SUCCESS_PUBLIC_SUBMIT:
			return { ...state, success: action.payload, loading: false };
		case CLEAR_SUCCESS_PUBLIC_SUBMIT:
			return { ...state, success: null };
		case SET_PUBLIC_DATA:
			return { ...state, ...action.payload };
		case CLEAR_PUBLIC_DATA:
			return { ...initialState };
		default:
			return state;
	}
};
