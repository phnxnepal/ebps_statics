import {
	LOADING_UI,
	CLEAR_ERRORS,
	SET_ERRORS,
	SET_USER,
	SET_UNAUTHENTICATED,
	STOP_LOADING,
	SET_SUCCESS_FORM_SUBMIT,
	CLEAR_FORM_DATA,
	SET_USER_MENU,
} from '../types';

import {
	USER_INFO,
	TOKEN_NAME,
	CURRENT_BUILD_PERMIT,
	FISCAL_YEAR,
	FORM_GROUP_MASTER,
	FILE_STORAGE_CATEGORY,
	APPLICATION_FORWARD_DATA,
	TOKEN_NAME_BACK,
	FORM_NAME_MASTER,
	RENEW_DATA,
	FISCAL_YEAR_ALL,
	WARD_MASTER,
	ORG_IDS,
} from '../../utils/constants';
import axios from 'axios';
import { getBackUrl } from '../../utils/config';
import { isEmpty, getUserInfoObj } from '../../utils/functionUtils';
import api from '../../utils/api';
import { getToken } from './../../utils/functionUtils';
import { setLocalStorage } from '../../utils/secureLS';
import { LSKey } from '../../utils/enums/localStorageKeys';
import { UserType } from '../../utils/userTypeUtils';
import decode from 'jwt-decode';

const loginError = err => {
	return { type: 'LOGIN_ERROR', data: err };
};

const setSubmitted = booll => {
	return { type: 'SUBMIT_BOTTOM', data: booll };
};

// const loginAction = (userData, history) => dispatch => {
// 	const { email, password } = userData;
// 	axios
// 		.get(`${getBackUrl()}/Login`, { params: { loginId: email, password } })
// 		.then(res => {
// 			if (!isEmpty(res.data.error)) {
// 				dispatch({ type: SET_ERRORS, payload: res.data.error });
// 				// history.push('/user');
// 			} else {
// 				const token = res.data.token;
// 				setAuthrizationHeader(token);
// 				axios
// 					.get(`${getBackUrl()}${api.fiscalYear}`)
// 					.then(fiscalRes => {
// 						const fiscalYear = fiscalRes.data.filter(year => year.status === 'Y');
// 						setLocalStorage(FISCAL_YEAR, JSON.stringify(fiscalYear[0]));
// 					})
// 					.catch(err => {
// 						console.log('error in fiscal year fetch', err);
// 					});
// 				axios
// 					.get(`${getBackUrl()}${api.formGroupMaster}`)
// 					.then(res => {
// 						const formGroupMaster = res.data;
// 						setLocalStorage(FORM_GROUP_MASTER, JSON.stringify(formGroupMaster));
// 					})
// 					.catch(err => {
// 						console.log('error in form group master fetch', err);
// 					});
// 				axios
// 					.get(`${getBackUrl()}${api.formNameMaster}`)
// 					.then(resFNM => {
// 						const formNameMaster = resFNM.data;
// 						setLocalStorage(FORM_NAME_MASTER, JSON.stringify(formNameMaster));
// 					})
// 					.catch(err => {
// 						console.log('error in form name master fetch', err);
// 					});
// 				let userInfoObj = res.data;
// 				axios
// 					.get(`${getBackUrl()}${api.organizationUserInfo}`)
// 					.then(infoRes => {
// 						if (isEmpty(infoRes.data.error)) {
// 							userInfoObj = { ...userInfoObj, ...{ info: infoRes.data[0] } };
// 						}
// 						setLocalStorage(USER_INFO, JSON.stringify(userInfoObj));
// 						dispatch({ type: SET_USER, payload: res.data });
// 						dispatch({ type: CLEAR_ERRORS });
// 						dispatch({ type: STOP_LOADING });
// 						history.push('/user/task-list');
// 					})
// 					.catch(err => {
// 						console.log('user info fetch error', err);
// 						setLocalStorage(USER_INFO, JSON.stringify(userInfoObj));
// 					});
// 				// Page is redirecting before token is being set
// 				// setTimeout(() => {
// 				// history.push('/user/task-list');
// 				// }, 500);
// 			}
// 		})
// 		.catch(err => {
// 			dispatch({ type: SET_ERRORS, payload: err });
// 			// --- temp --
// 			// setAuthrizationHeader('token');
// 			// setLocalStorage('user', 'asdf');
// 			// dispatch({ type: SET_USER, payload: getDemoData() });
// 			// dispatch({ type: CLEAR_ERRORS });
// 			// history.push('/user');
// 			// --- end temp ---
// 			//   dispatch({ type: SET_ERRORS, payload: err });
// 		});
// };

const fetchGroups = (userType) => async dispatch => {
	const groupResponse = await axios.get(`${getBackUrl()}${api.formGroup}/1-${userType}`)
	if (!isEmpty(groupResponse.data.error)) dispatch({ type:SET_ERRORS, payload: groupResponse.data.error })
	else {	
		const formGroup = groupResponse.data? groupResponse.data: [];
		setLocalStorage(LSKey.FORM_GROUP, JSON.stringify(formGroup));
	}
}

export const startLogin = (userData, history) => dispatch => {
	const { email, password } = userData;
	axios
		.get(`${getBackUrl()}/Login`, { params: { loginId: email, password } })
		.then(res => {
			if (!isEmpty(res.data.error)) {
				dispatch({ type: SET_ERRORS, payload: res.data.error });
			} else {
				let userInfoObj = {};

				userInfoObj.organization = res.data.organization && res.data.organization;
				userInfoObj.designer = res.data.designer && res.data.organization;
				userInfoObj.token = res.data.token && res.data.token;

				userInfoObj = {
					...userInfoObj,
					userType: res.data.userType ? res.data.userType : '',
					userName: res.data.userName ? res.data.userName : '',
					userCode: res.data.userCode ? res.data.userCode : '',
					status: res.data.status ? res.data.status : '',
				};

				const token = res.data.token;

				try {
					const { userTypeAll } = decode(token);
					setLocalStorage(LSKey.USER_TYPE_ALL, userTypeAll);
				} catch {
					setLocalStorage(LSKey.USER_TYPE_ALL, JSON.stringify([]));
				}

				setAuthrizationHeader(token);

				fetchGroups(userInfoObj.userType)(dispatch)

				const fiscalYear = res.data.fiscalYear ? res.data.fiscalYear.filter(year => year.status === 'Y') : [];
				setLocalStorage(FISCAL_YEAR, JSON.stringify(fiscalYear[0]));
				setLocalStorage(FISCAL_YEAR_ALL, JSON.stringify(res.data.fiscalYear));

				setLocalStorage(ORG_IDS, JSON.stringify({ organization: res.data.organization }));

				const formGroupMaster = res.data.formGroupMaster ? res.data.formGroupMaster : [];
				setLocalStorage(FORM_GROUP_MASTER, JSON.stringify(formGroupMaster));

				const formNameMaster = res.data.formNameMaster ? res.data.formNameMaster : [];
				setLocalStorage(FORM_NAME_MASTER, JSON.stringify(formNameMaster));

				const fileStorageCategory = res.data.fileStorageCategory ? res.data.fileStorageCategory : [];
				setLocalStorage(FILE_STORAGE_CATEGORY, JSON.stringify(fileStorageCategory));

				const applicationForwardingSetup = res.data.applicationForwardingSetup ? res.data.applicationForwardingSetup : [];
				setLocalStorage(APPLICATION_FORWARD_DATA, JSON.stringify(applicationForwardingSetup));

				const wardMaster = res.data.wardMaster ? res.data.wardMaster : [];
				setLocalStorage(WARD_MASTER, JSON.stringify(wardMaster));

				const userDetails = res.data.userDetails ? res.data.userDetails : [];
				setLocalStorage(LSKey.USER_DETAILS, JSON.stringify(userDetails));

				let userMenu = [];
				if (userInfoObj.userType === UserType.TECH_ADMIN) {
					userMenu = res.data.menuList
						? res.data.menuList.find(row => row.url === '/admin/menu-setup' || row.url === '/admin/menu-access-setup')
							? res.data.menuList
							: res.data.menuList.concat({ url: '/admin/menu-setup' }, { url: '/admin/menu-access-setup' })
						: [{ url: '/admin/menu-setup' }, { url: '/admin/menu-access-setup' }];
				} else {
					userMenu = res.data.menuList ? res.data.menuList : [];
				}

				setLocalStorage(LSKey.USER_MENU, JSON.stringify(userMenu));

				userInfoObj = res.data.userInfo && { ...userInfoObj, ...{ info: res.data.userInfo } };
				setLocalStorage(USER_INFO, JSON.stringify(userInfoObj));
				dispatch({ type: SET_USER, payload: res.data });
				dispatch({ type: SET_USER_MENU, payload: userMenu });
				dispatch({ type: CLEAR_ERRORS });
				dispatch({ type: STOP_LOADING });
				history.push('/user/task-list');
			}
		})
		.catch(err => {
			dispatch({ type: SET_ERRORS, payload: err });
		});
};

export const loginUser = (userData, history) => dispatch => {
	dispatch({ type: LOADING_UI });

	axios
		.get(`${getBackUrl()}/api/HibernateUtil`)
		.then(res => {
			if (res.data && res.data.msg === 'Success') {
				startLogin(userData, history)(dispatch);
			} else {
				axios
					.post(`${getBackUrl()}/api/HibernateUtil`)
					.then(res => {
						if (res.data.msg && res.data.msg === 'Success') {
							startLogin(userData, history)(dispatch);
						} else if (res.data.db) {
							dispatch({ type: SET_ERRORS, payload: { message: res.data.db } });
							dispatch({ type: STOP_LOADING });
						} else {
							dispatch({ type: SET_ERRORS, payload: { message: 'Error in connection' } });
							dispatch({ type: STOP_LOADING });
						}
					})
					.catch(err => {
						console.log('error', err);
						dispatch({ type: SET_ERRORS, payload: err });
						dispatch({ type: STOP_LOADING });
					});
			}
		})
		.catch(err => {
			console.log('error', err);
			dispatch({ type: SET_ERRORS, payload: err });
			dispatch({ type: STOP_LOADING });
		});
};

export const logoutUser = () => dispatch => {
	// console.log('Logout action');
	localStorage.removeItem(TOKEN_NAME);
	localStorage.removeItem(TOKEN_NAME_BACK);
	localStorage.removeItem('user');
	localStorage.removeItem(USER_INFO);
	localStorage.removeItem(CURRENT_BUILD_PERMIT);
	localStorage.removeItem(FISCAL_YEAR);
	localStorage.removeItem(APPLICATION_FORWARD_DATA);
	localStorage.removeItem(FILE_STORAGE_CATEGORY);
	localStorage.removeItem(FORM_GROUP_MASTER);
	localStorage.removeItem(RENEW_DATA);
	// localStorage.removeItem(KEYBOARD);
	delete axios.defaults.headers.common['Authorization'];
	dispatch({ type: SET_UNAUTHENTICATED });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: STOP_LOADING });
};

export const getUserData = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	try {
		const userInfo = getUserInfoObj();
		const response = await axios.get(`${getBackUrl()}${api.organizationUserInfo}`);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			dispatch({ type: STOP_LOADING });
		} else {
			dispatch({
				type: SET_USER,
				payload: { userInfo: userInfo, userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export { loginError, setSubmitted };

const setAuthrizationHeader = token => {
	const AuthToken = token;
	setLocalStorage(TOKEN_NAME, AuthToken);
	localStorage.setItem(TOKEN_NAME_BACK, AuthToken);
	axios.defaults.headers.common['Authorization'] = `Bearer ${AuthToken}`;
};

export const postPassword = data => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_FORM_DATA });
	const postUrl = `${getBackUrl()}api/ChangePassword`;

	try {
		axios.defaults.headers.common['Authorization'] = getToken();
		const response = await axios.post(postUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_FORM_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
		dispatch({ type: LOADING_UI });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: LOADING_UI });
	}
};

export const clearErrors = () => async dispatch => {
	dispatch({ type: STOP_LOADING });
	dispatch({ type: CLEAR_ERRORS });
};

export const postForgotPassword = data => async dispatch => {
	dispatch({ type: LOADING_UI });
	const postUrl = `${getBackUrl()}/ForgetPassword`;
	try {
		const response = await axios.post(postUrl, data);

		if (response.data === 'Sent') {
			dispatch({
				type: SET_SUCCESS_FORM_SUBMIT,
				payload: { success: true, data: response.data },
			});
			dispatch({ type: STOP_LOADING });
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};