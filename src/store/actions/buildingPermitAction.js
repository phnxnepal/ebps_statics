import { LOADING_UI, CLEAR_ERRORS, SET_ERRORS, SET_UNSET_PERMIT_ERRORS, SET_FORM_DATA, STOP_LOADING, SET_SUCCESS_FORM_SUBMIT } from '../types';
import axios from 'axios';
import { getBackUrl } from '../../utils/config';
import { isEmpty, getToken } from '../../utils/functionUtils';
import { CURRENT_BUILD_PERMIT } from '../../utils/constants';
import api from '../../utils/api';
import { getLocalStorage } from '../../utils/secureLS';

export const postFormData = data => dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	const url = `${getBackUrl()}api/Application/BuildingPermit/`;
	const AuthToken = getToken();

	if (Array.isArray(data.buildingJoinRoadType)) data.buildingJoinRoadType = data.buildingJoinRoadType.join(',');
	// if (Array.isArray(data.buildingJoinRoadTypeOther))
	//   data.buildingJoinRoadTypeOther = data.buildingJoinRoadTypeOther.join(',');
	// if (Array.isArray(data.purposeOfConstruction))
	//   data.purposeOfConstruction = data.purposeOfConstruction.join(',');
	// if (Array.isArray(data.constructionType))
	//   data.constructionType = data.constructionType.join(',');
	// if (Array.isArray(data.mohada)) data.mohada = data.mohada.join(',');
	// if (Array.isArray(data.constructionFinishing))
	//   data.constructionFinishing = data.constructionFinishing.join(',');
	// if (Array.isArray(data.dhalNikasArrangement))
	//   data.dhalNikasArrangement = data.dhalNikasArrangement.join(',');
	// if (Array.isArray(data.dhalNikasArrangementOther))
	//   data.dhalNikasArrangementOther = data.dhalNikasArrangementOther.join(',');
	// if (Array.isArray(data.foharArrangement))
	//   data.foharArrangement = data.foharArrangement.join(',');

	// if (data && data.floor)
	//   data.floor.map(floor => {
	//     if (floor) {
	//     floor.floor = data.floor.indexOf(floor)
	//   }
	// });
	if (Array.isArray(data.mohada)) data.mohada = data.mohada.join(',');

	if (data.citizenshipNo) data.nationalIdNo = data.citizenshipNo;
	// data.newMunicipal = '';
	data.certificateArea = '';
	data.landCertificateNo = '';
	// data.oldMapDate = '';
	// data.purposeOfConstructionOther = '';
	// data.constructionTypeOther = '';
	// data.constructionFinishingOther = '';
	// data.foharArrangementOther = '';

	// let data1 = data
	// const dataToSend1 =
	// data.floor.filter(el => el !== undefined)
	// console.log('data to send ', dataToSend1);
	// data1.floor = dataToSend1
	// console.log('data to send filtered ', data1);

	// Object.assign(data, {
	//   surrounding: [
	//     {
	//       feet: 111,
	//       district: '111',
	//       municipal: '111',
	//       wardNo: '111',
	//       side: 111,
	//       sandhiyar: '1111'
	//     },
	//     {
	//       feet: 1212,
	//       district: '12',
	//       municipal: '1212',
	//       wardNo: '1',
	//       side: 212,
	//       sandhiyar: '1121'
	//     }
	//   ]
	// });

	// let original_data = { ...data };
	data.aminAction && delete data.aminAction;
	data.applicationAction && delete data.applicationAction;
	data.chiefAction && delete data.chiefAction;
	data.designerAction && delete data.designerAction;
	data.enginieerAction && delete data.enginieerAction;
	data.rajasowAction && delete data.rajasowAction;
	data.subEnginieerAction && delete data.subEnginieerAction;

	axios.defaults.headers.common['Authorization'] = AuthToken;
	dispatch({ type: LOADING_UI });
	return axios
		.post(url, data, {
			headers: {
				Authorization: AuthToken,
			},
		})
		.then(res => {
			if (!isEmpty(res.data.error)) {
				dispatch({ type: SET_ERRORS, payload: res.data.error });
				dispatch({ type: STOP_LOADING });
			}
			dispatch({ type: STOP_LOADING });
			return res;
		})
		.catch(err => {
			console.log('err', err);
			dispatch({ type: SET_ERRORS, payload: err });
			dispatch({ type: STOP_LOADING });
			return { error: err };
		});
	// console.log('Posting data ', data, url);
};

export const getFormData = () => dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	const data = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));

	if (isEmpty(data)) {
		dispatch({
			type: SET_UNSET_PERMIT_ERRORS,
		});
	} else {
		dispatch({
			type: SET_FORM_DATA,
			payload: {
				//   groups: groups,
				formData: data, //demoData
				//   prevData: prevData
			},
		});
	}
};

export const getFormDataWithComment = () => async dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: LOADING_UI });
	const data = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));

	// if (isEmpty(data)) {
	//   dispatch({
	//     type: SET_UNSET_PERMIT_ERRORS
	//   });
	// } else {
	//   dispatch({
	//     type: SET_FORM_DATA,
	//     payload: {
	//       //   groups: groups,
	//       formData: data //demoData
	//       //   prevData: prevData
	//     }
	//   });

	try {
		axios.defaults.headers.common['Authorization'] = getToken();
		const response = await axios.get(`${getBackUrl()}${api.buildPermit}${data.applicantNo}`);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					formData: response.data.data,
					comment: response.data.comment,
					history: response.data.history,
				},
			});
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	// }
};


export const getNibedakPhoto = () => async dispatch => {
	axios.defaults.headers.common['Authorization'] = getToken();

	const permit = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));
	try {
		const response = await axios.get(`${getBackUrl()}${api.buildPermit}${permit.applicantNo}`);

		// console.log('response struct data submit', response);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_FORM_DATA,
				payload: { permitDataNew: response.data.data },
			});
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const postNibedakPhoto = (url, data) => async dispatch => {
	axios.defaults.headers.common['Authorization'] = getToken();

	try {
		const response = await axios.post(`${getBackUrl()}${url}`, data);

		// console.log('response struct data submit', response);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_SUCCESS_FORM_SUBMIT,
				payload: { success: true, data: response.data },
			});
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
