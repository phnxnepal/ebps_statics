import {
	LOADING_UI,
	SET_ERRORS,
	SET_BUILD_PERMITS,
	CLEAR_ERRORS,
	STOP_LOADING,
	SET_CURRENT_PERMIT,
	SET_UNSET_PERMIT_ERRORS,
	CLEAR_FORM_DATA,
	CLEAR_BUILD_PERMITS,
	SET_CURRENT_DATA,
	SET_FORM_DATA,
	CLEAR_ADMIN_DATA,
	SET_ADMIN_DATA,
	SET_ORGANIZATION,
	CLEAR_REJECTED_BY,
} from '../types';
import { getBackUrl } from '../../utils/config';
import { CURRENT_BUILD_PERMIT, MENU_LIST, BUILDING_CLASS, TASK_IDS, PLOR_SANSODHAN, RENEW_DATA, ORG_IDS, REJECTED_STATUS } from '../../utils/constants';
import axios from 'axios';
import { getToken, isEmpty, getUserRole, getUserInfoObj } from '../../utils/functionUtils';
import api from '../../utils/api';
import { setLocalStorage, getLocalStorage } from '../../utils/secureLS';
import { isStringEmpty } from '../../utils/stringUtils';
import { UserType } from '../../utils/userTypeUtils';
import { getGroupName } from '../../utils/dataUtils';
import { LSKey } from '../../utils/enums/localStorageKeys';
import { handlePermitResponse } from '../../utils/applicationUtils';

export const getBuildPermits = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: CLEAR_BUILD_PERMITS });
	dispatch({ type: CLEAR_ERRORS });
	const url = `${getBackUrl()}api/Application/BuildingPermit`;

	const token = getToken();

	/**
	 * @todo confirms if this should be removed?
	 * */

	localStorage.removeItem(CURRENT_BUILD_PERMIT);
	localStorage.removeItem(MENU_LIST);
	localStorage.removeItem(BUILDING_CLASS);
	localStorage.removeItem(LSKey.NAMSARI_STATUS);
	localStorage.removeItem(LSKey.HAS_DESIGNER_CHANGED);
	localStorage.removeItem(LSKey.NEW_DESIGNER_NAME);
	localStorage.removeItem(REJECTED_STATUS);
	localStorage.removeItem(PLOR_SANSODHAN);

	try {
		const response =
			// demoBuildPermitData
			await axios.get(url, {
				headers: {
					Authorization: token,
				},
			});
		dispatch({ type: CLEAR_ERRORS });
		dispatch({ type: SET_BUILD_PERMITS, payload: response.data });
		dispatch({ type: STOP_LOADING });
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

export const clearBuildPermits = () => dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: CLEAR_BUILD_PERMITS });
	dispatch({ type: CLEAR_REJECTED_BY });
	localStorage.removeItem(CURRENT_BUILD_PERMIT);
	localStorage.removeItem(REJECTED_STATUS);
	localStorage.removeItem(MENU_LIST);
	localStorage.removeItem(BUILDING_CLASS);
	localStorage.removeItem(LSKey.NAMSARI_STATUS);
	localStorage.removeItem(LSKey.HAS_DESIGNER_CHANGED);
	localStorage.removeItem(LSKey.NEW_DESIGNER_NAME);
	localStorage.removeItem(PLOR_SANSODHAN);
};

export const getTaskList = a => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_BUILD_PERMITS });
	dispatch({ type: CLEAR_REJECTED_BY });
	if (!a) {
		dispatch({ type: CLEAR_ERRORS });
	}
	const url = `${getBackUrl()}${api.tasks}`;

	const token = getToken();
	/**
	 * @todo confirms if this should be removed?
	 * */

	localStorage.removeItem(CURRENT_BUILD_PERMIT);
	localStorage.removeItem(MENU_LIST);
	localStorage.removeItem(REJECTED_STATUS);
	localStorage.removeItem(BUILDING_CLASS);
	localStorage.removeItem(LSKey.NAMSARI_STATUS);
	localStorage.removeItem(LSKey.HAS_DESIGNER_CHANGED);
	localStorage.removeItem(LSKey.NEW_DESIGNER_NAME);
	localStorage.removeItem(PLOR_SANSODHAN);

	try {
		const response =
			// demoBuildPermitData
			await axios.get(url, {
				headers: {
					Authorization: token,
				},
			});

		if (response.data.error) {
			if (!a) {
				dispatch({ type: SET_ERRORS, payload: response.data.errror });
			}
		} else {
			setLocalStorage(TASK_IDS, JSON.stringify(response.data.map(each => each.applicantNo)));

			if (getUserRole() === UserType.DESIGNER) {
				const userObj = getUserInfoObj();
				// const renewalResponse = await axios.get(`${getBackUrl()}${api.designerRenewStatus}`);
				// if (renewalResponse.data.error) {
				// console.log('Error in renewal status fetch');
				// } else {
				// const renewalData = renewalResponse.data.filter(designer => designer.designerId === userObj.info.id).reduce((prev, current) => prev.renewDate > current.renewDate ? prev : current, {});
				const renewalData = userObj.info.validDate;
				// console.log('renewal', renewalData, { tilldate: userObj.info.validDate});
				if (renewalData) {
					setLocalStorage(RENEW_DATA, JSON.stringify({ tilldate: renewalData }));
					dispatch({
						type: SET_CURRENT_DATA,
						payload: { renewalData: { tilldate: renewalData } },
					});
				}
				// }
			}

			if (!a) {
				dispatch({ type: CLEAR_ERRORS });

				dispatch({ type: SET_BUILD_PERMITS, payload: response.data });
				dispatch({ type: STOP_LOADING });
			}
		}
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

export const getDirtyTaskList = a => async dispatch => {
	dispatch({ type: LOADING_UI });

	const url = `${getBackUrl()}${api.tasks}`;

	const token = getToken();

	/**
	 * @todo confirms if this should be removed?
	 * */

	try {
		const response =
			// demoBuildPermitData
			await axios.get(url, {
				headers: {
					Authorization: token,
				},
			});

		setLocalStorage(TASK_IDS, JSON.stringify(response.data.map(each => each.applicantNo)));
		dispatch({ type: STOP_LOADING });
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

//getting build permit from application ID
export const setBuildPermit = permit => async dispatch => {
	dispatch({ type: LOADING_UI });
	const token = getToken();
	try {
		if (isEmpty(permit)) {
			dispatch({ type: SET_UNSET_PERMIT_ERRORS });
			throw new Error('ViewURL not set for this user and build permit');
		} else if (isStringEmpty(permit.yourAction.viewURL)) {
			throw new Error('ViewURL not set for this user and build permit');
		} else {
			const id = permit.applicantNo;
			try {
				const response = await axios.get(`${getBackUrl()}${api.buildPermit}${id}`, {
					headers: {
						Authorization: token,
					},
				});

				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
					dispatch({ type: STOP_LOADING });
				} else {
					dispatch({
						type: SET_CURRENT_PERMIT,
						payload: {
							menu: response.data.menu,
							permitData: response.data.data,
						},
					});
					dispatch({ type: STOP_LOADING });
				}

				return response;

				// if (permit.yourAction) {
				//   dispatch({ type: SET_CURRENT_PERMIT, payload: response.data });
				//   setLocalStorage(
				//     CURRENT_BUILD_PERMIT,
				//     JSON.stringify(response.data.data)
				//   );

				//   const menu = response.data.menu

				//   menu.forEach(mn => {
				//     if (mn.viewURL.trim() === "/user/forms/forward-to-next") {
				//       console.log('gotint', mn);
				//       try {
				//       mn.formName = `${mn.formName} ${groupName.find(grp => grp.id == mn.groupId).name}`
				//       } catch (err) {
				//         console.log('err unexpected', err);
				//       }
				//       console.log('goutout', mn.formName);
				//     }
				//   })

				//   console.log('manu', menu);
				//   setLocalStorage(MENU_LIST, JSON.stringify(menu));
				//   dispatch({ type: STOP_LOADING });
				//   return new Promise((resolve, reject) => {
				//     setTimeout(() => {
				//       resolve(menu)
				//     }, 1000);
				//   })
				// } else {
				//   throw new Error('View URL not set for this user and build permit.');
				// }
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
				dispatch({ type: STOP_LOADING });
			}
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const setBuildPermitAdd = permit => async dispatch => {
	dispatch({ type: LOADING_UI });
	const token = getToken();
	try {
		if (isEmpty(permit)) {
			dispatch({ type: SET_UNSET_PERMIT_ERRORS });
			throw new Error('ViewURL not set for this user and build permit');
		} else {
			const id = permit.applicantNo;
			try {
				const response = await axios.get(`${getBackUrl()}${api.buildPermit}${id}`, {
					headers: {
						Authorization: token,
					},
				});

				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
					dispatch({ type: STOP_LOADING });
				} else {
					dispatch({
						type: SET_CURRENT_PERMIT,
						payload: {
							menu: response.data.menu,
							permitData: response.data.data,
						},
					});
					dispatch({ type: STOP_LOADING });
				}

				return response;
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
				dispatch({ type: STOP_LOADING });
			}
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getBuildingClass = permit => async dispatch => {
	dispatch({ type: LOADING_UI });
	const token = getToken();
	if (isEmpty(permit)) {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	} else {
		const id = permit.applicantNo;
		try {
			const response = await axios.get(`${getBackUrl()}${api.designApproval}${id}`, {
				headers: {
					Authorization: token,
				},
			});
			dispatch({ type: SET_CURRENT_PERMIT, payload: { buildingClass: response.data.data.buildingClass } });
			return response;
		} catch (err) {
			// dispatch({ type: SET_ERRORS, payload: err });
			dispatch({ type: STOP_LOADING });
		}
	}
};

export const getCompletedList = a => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: CLEAR_BUILD_PERMITS });
	const url = `${getBackUrl()}${api.tallaThapList}?status=Y`;

	const token = getToken();

	/**
	 * @todo confirms if this should be removed?
	 * */

	localStorage.removeItem(CURRENT_BUILD_PERMIT);
	localStorage.removeItem(REJECTED_STATUS);
	localStorage.removeItem(MENU_LIST);
	localStorage.removeItem(BUILDING_CLASS);
	localStorage.removeItem(LSKey.NAMSARI_STATUS);
	localStorage.removeItem(LSKey.HAS_DESIGNER_CHANGED);
	localStorage.removeItem(LSKey.NEW_DESIGNER_NAME);
	localStorage.removeItem(PLOR_SANSODHAN);

	try {
		const response =
			// demoBuildPermitData
			await axios.get(url, {
				headers: {
					Authorization: token,
				},
			});

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			setLocalStorage(TASK_IDS, JSON.stringify(response.data.map(each => each.applicantNo)));
			if (!a) {
				dispatch({ type: CLEAR_ERRORS });

				dispatch({ type: SET_BUILD_PERMITS, payload: response.data });
				dispatch({ type: STOP_LOADING });
			}
		}
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

export const getBuildPermitNew = id => async dispatch => {
	dispatch({ type: LOADING_UI });
	const token = getToken();
	try {
		const response = await axios.get(`${getBackUrl()}${api.buildPermit}${id}`, {
			headers: {
				Authorization: token,
			},
		});

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			dispatch({ type: STOP_LOADING });
		} else {
			// if (response.data.data.chiefStatus === 'C') {
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					permitData: response.data.data,
				},
			});
			dispatch({ type: STOP_LOADING });
			// } else {
			// dispatch({ type: SET_ERRORS, payload: { message: 'Invalid Application No.' } });
			// dispatch({ type: STOP_LOADING });
			// }
		}

		// return response;
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const getDataByUrl = (urlArray, params) => async dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_CURRENT_DATA});
	dispatch({ type: LOADING_UI });

	Promise.all(
		urlArray.map(async url => {
			try {
				const getUrl = `${getBackUrl()}${url.api}`;
				const response = await axios.get(getUrl, { params: { ...params } });

				// console.log('response re', response);
				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
				} else {
					dispatch({
						type: SET_CURRENT_DATA,
						payload: {
							[url.objName]: response.data,
						},
					});
				}
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		dispatch({ type: STOP_LOADING });
	});
};

/**
 * Get local api from local storage
 * @param {LocalAPIArray} localApis
 */
export const getLocalDataByKey = (localApis, needsLoading = true) => async dispatch => {
	needsLoading && dispatch({ type: LOADING_UI });
	Promise.all(
		localApis.getLocalApis().map(async api => {
			try {
				const data = await getLocalStorage(api.getKey());
				dispatch({
					type: SET_CURRENT_DATA,
					payload: {
						[api.getObjName()]: JSON.parse(data),
					},
				});
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		needsLoading && dispatch({ type: STOP_LOADING });
	});
};

//getting build permit from application ID
export const setBuildPermitSetup = permit => async dispatch => {
	dispatch({ type: LOADING_UI });
	const token = getToken();
	try {
		if (isEmpty(permit)) {
			dispatch({ type: SET_UNSET_PERMIT_ERRORS });
			throw new Error('ViewURL not set for this user and build permit');
		} else if (isStringEmpty(permit.yourAction.viewURL)) {
			throw new Error('ViewURL not set for this user and build permit');
		} else {
			const id = permit.applicantNo;
			try {
				const response = await axios.get(`${getBackUrl()}${api.buildPermit}${id}`, {
					headers: {
						Authorization: token,
					},
				});

				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
					dispatch({ type: STOP_LOADING });
				} else {
					handlePermitResponse(response, permit);
					dispatch({
						type: SET_CURRENT_PERMIT,
						payload: {
							menu: response.data.menu,
							permitData: response.data.data,
						},
					});
					dispatch({ type: STOP_LOADING });
				}
				// return response;
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
				dispatch({ type: STOP_LOADING });
			}
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getOrg = () => async dispatch => {
	dispatch({ type: LOADING_UI });

	const getUrl = `${getBackUrl()}Organization`;
	try {
		const response = await axios.get(getUrl);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			setLocalStorage(ORG_IDS, JSON.stringify(response.data));
			dispatch({
				type: SET_ORGANIZATION,
				payload: response.data,
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in organization data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};
