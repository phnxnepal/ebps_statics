import {
  LOADING_UI,
  STOP_LOADING,
  SET_UNSET_PERMIT_ERRORS,
  SET_FORM_DATA,
  SET_ERRORS
} from "./../types";
import {
  getBuildPermitObj,
  isEmpty,
  getUserInfoObj
} from "../../utils/functionUtils";

 

export const getPermitAndUserData = () => dispatch => {
  dispatch({type: LOADING_UI})
  dispatch({ type: SET_FORM_DATA, payload: { permitData: {} } });
  const bpObj = getBuildPermitObj();
  const userObj = getUserInfoObj();

  if (isEmpty(bpObj) && isEmpty(userObj)) {
    dispatch({
      type: SET_UNSET_PERMIT_ERRORS
    });
  } else {
    try {
      dispatch({
        type: LOADING_UI
      });
      dispatch({
        type: SET_FORM_DATA,
        payload: {
          permitData: bpObj,
          UserData: userObj
        }
      });
      dispatch({
        type: STOP_LOADING
      });
    } catch (err) {
      dispatch({
        type: STOP_LOADING
      });
      dispatch({type: SET_ERRORS, payload: err});
    }
  }
};
