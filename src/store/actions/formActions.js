import {
	LOADING_UI,
	CLEAR_ERRORS,
	SET_ERRORS,
	SET_FORM_DATA,
	SET_UNSET_PERMIT_ERRORS,
	SET_SUCCESS_FORM_SUBMIT,
	STOP_LOADING,
	LOADING_MODAL,
	STOP_LOADING_MODAL,
	CLEAR_MODAL_ERRORS,
	CLEAR_FORM_DATA,
	// SET_BUILD_PERMITS,
	SET_CURRENT_PERMIT,
} from '../types';
import axios from 'axios';
import { getBackUrl } from '../../utils/config';
import {
	// getUniqueFromArray,
	// replaceDot,
	isEmpty,
	getBuildPermitObj,
	getToken,
	getUserInfoObj,
} from '../../utils/functionUtils';
import { CURRENT_BUILD_PERMIT, MENU_LIST, FILE_STORAGE_CATEGORY } from '../../utils/constants';
// import { prepareArchiData } from '../../utils/dataUtils';
import { getLocalStorage, setLocalStorage } from '../../utils/secureLS';
import api from '../../utils/api';
import { translateNepToEng } from '../../utils/langUtils';
import { LSKey } from '../../utils/enums/localStorageKeys';
// import { LocalAPIArray } from '../../utils/urlUtils';

export const getFormData = (backUrl, objName) => dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_FORM_DATA });
	const buildPermitObj = getLocalStorage(CURRENT_BUILD_PERMIT);

	if (isEmpty(buildPermitObj)) {
		dispatch({
			type: SET_UNSET_PERMIT_ERRORS,
		});
	} else {
		const appId = JSON.parse(buildPermitObj).applicantNo;
		const url = `${getBackUrl()}${backUrl}${appId}`;

		axios
			.get(url)
			.then(res => {
				if (isEmpty(res.data.error)) {
					// let prevData = {};
					// const listData = res.data.listData;
					// // console.log('New Data -- ', listData);

					// const groups = {};

					// // Get unique groups
					// getUniqueFromArray(listData, 'groupId').forEach(groupId => {
					// 	const groupName = listData.filter(
					// 		row => row.groupId === groupId
					// 	)[0].groupName;
					// 	Object.assign(groups, { [groupId]: groupName });
					// });

					// // Retrieve previously populated data
					// listData
					// 	.filter(row => row.qty !== null || row.remarks !== null)
					// 	.forEach(filtered => {
					// 		const keyQty = `${replaceDot(
					// 			filtered.classId
					// 		)}_qty`;
					// 		const keyRemark = `${replaceDot(
					// 			filtered.classId
					// 		)}_remarks`;
					// 		Object.assign(prevData, {
					// 			[keyQty]: filtered.qty,
					// 			[keyRemark]: filtered.remarks
					// 		});
					// 	});

					dispatch({
						type: SET_FORM_DATA,
						payload: {
							// groups: groups,
							// archiFormData: listData,
							[objName]: res.data,
							// formStatus: res.data
						},
					});
					dispatch({ type: STOP_LOADING });
				} else {
					dispatch({
						type: SET_ERRORS,
						payload: res.data.error,
					});
					dispatch({ type: STOP_LOADING });
				}
			})
			.catch(err => {
				console.log(err);
				dispatch({
					type: SET_ERRORS,
					payload: err,
				});
				dispatch({ type: STOP_LOADING });
			});
	}
	// Object.keys(getDemoFormDataFromId).map(obj => {
	//   console.log('archi obj--', obj);
	//   if (Array.isArray(getDemoFormDataFromId[obj])) {
	//     const currentArr = getDemoFormDataFromId[obj];
	//     console.log('getDemo archi -- ', currentArr);
	//     currentArr.map(ob => (ob.groupcode = obj));
	//     serverData.push(currentArr);
	//     // reqData.push(currentArr)
	//   }
	// });
	// let prevData = {};

	// console.log('New Data -- ', newDemo.listData);

	// const groups = {};

	// // Get unique groups
	// getUniqueFromArray(newDemo.listData, 'group_id').map(groupId => {
	//   const groupName = newDemo.listData.filter(
	//     row => row.group_id === groupId
	//   )[0].groupname;
	//   Object.assign(groups, { [groupId]: groupName });
	// });

	// // Retrieve previously populated data
	// newDemo.listData
	//   .filter(row => row.qty !== null || row.remarks !== null)
	//   .map(filtered => {
	//     const keyQty = `${replaceDot(filtered.classid)}_qty`;
	//     const keyRemark = `${replaceDot(filtered.classid)}_remarks`;
	//     Object.assign(prevData, {
	//       [keyQty]: filtered.qty,
	//       [keyRemark]: filtered.remarks
	//     });
	//   });

	// console.log('groups', groups, prevData);

	// Object.keys(getDemoFormDataFromId)
	//   .filter(obj => Array.isArray(getDemoFormDataFromId[obj]))
	//   .map(filtered => {
	//     const data = getDemoFormDataFromId[filtered];
	//     const formattedData = [];
	//     console.log('Filtered', data.flat());
	//     getUniqueFromArray(data.flat(), 'group_id').map(groupId =>
	//       formattedData.push(
	//         data.flat().filter(data => data.group_id === groupId)
	//       )
	//     );

	//     console.log('formated daata ---', formattedData);
	//     formattedData.map(section => {
	//       console.log('Section -- ', section);
	//       section
	//         .filter(row => row.qty !== null || row.remarks !== null)
	//         .map(filtered => {
	//           const keyQty = `${replaceDot(filtered.classid)}_qty`;
	//           const keyRemark = `${replaceDot(filtered.classid)}_remarks`;
	//           Object.assign(prevData, {
	//             [keyQty]: filtered.qty,
	//             [keyRemark]: filtered.remarks
	//           });
	//         });
	//     });

	//     data.map(ob => (ob.groupcode = filtered));
	//     return reqData.push(data);
	//   });
	// console.log('archi action arr', serverData.flat());
	// console.log(
	//   'formData -- ',
	//   reqData.sort((a, b) => (a.classid > b.classid ? 1 : -1))
	// );
	// console.log('prevData --- ', prevData);

	// const serverData = this.props.formData.formData;
	// getUniqueFromArray(serverData, 'group_id').map(groupId =>
	//   formattedData.push(serverData.filter(data => data.group_id === groupId))
	// );

	// formattedData.map(section => {
	//   console.log('Section -- ', section);
	//   section
	//     .filter(row => row.qty !== null || row.remarks !== null)
	//     .map(filtered => {
	//       const keyQty = `${replaceDot(filtered.classid)}_qty`;
	//       const keyRemark = `${replaceDot(filtered.classid)}_remarks`;
	//       Object.assign(prevData, {
	//         [keyQty]: filtered.qty,
	//         [keyRemark]: filtered.remarks
	//       });
	//     });
	// });

	// const sorted = serverData
	//   .flat()
	//   .sort((a, b) => (a.classid > b.classid ? 1 : -1));
	// -- temp --
	// dispatch({
	//   type: SET_FORM_DATA,
	//   payload: {
	//     groups: groups,
	//     formData: newDemo.listData,
	//     prevData: prevData
	//   }
	// });
	// -- end temp --
};

export const getBuildPermitData = appId => async dispatch => {
	dispatch({ type: LOADING_UI });
	try {
		const response = await axios.get(`${getBackUrl()}${api.buildPermit}${appId}`);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_CURRENT_PERMIT,
				payload: { menu: response.data.menu, permitData: response.data.data },
			});
			setLocalStorage(MENU_LIST, JSON.stringify(response.data.menu));
			setLocalStorage(CURRENT_BUILD_PERMIT, JSON.stringify(response.data.data));
		}
	} catch (err) {
		// console.log('Error', err);
		dispatch({ type: SET_ERRORS, payload: err });
	}

	dispatch({ type: STOP_LOADING });
};

export const postFormData = (baseUrl, data) => async dispatch => {
	const buildPermitObj = getLocalStorage(CURRENT_BUILD_PERMIT);

	if (isEmpty(buildPermitObj)) {
		dispatch({
			type: SET_UNSET_PERMIT_ERRORS,
		});
	} else {
		const appId = JSON.parse(buildPermitObj).applicantNo;
		const url = `${getBackUrl()}${baseUrl}${appId}`;
		const token = getToken();
		axios.defaults.headers.common['Authorization'] = token;

		// return axios
		// 	.post(url, data)
		// 	.then(res => {
		// 		// console.log('response actios', res);
		// 		return res;
		// 	})
		// 	.catch(err => {
		// 		// console.log('err', err.message);
		// 		dispatch({ type: SET_ERRORS });
		// 		return { error: err };
		// 	});
		// console.log('Posting data ', data, url);
		try {
			const response = await axios.post(url, data);

			// console.log('response struct data submit', response);

			if (response.data.message === 'Success') {
				dispatch({
					type: SET_SUCCESS_FORM_SUBMIT,
					payload: { success: true, data: response.data },
				});
			} else {
				dispatch({ type: SET_ERRORS, payload: response.data.error });
			}
		} catch (err) {
			dispatch({ type: SET_ERRORS, payload: err });
		}
	}
};

export const postFormDataByUrl = (url, data, withAppId = false, concatAppId = true, fetchMenu = false, bClass = null) => async dispatch => {
	const buildPermitObj = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));

	const postUrl = withAppId ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}` : `${getBackUrl()}${url}`;
	// const postUrl = `${getBackUrl()}${url}${buildPermitObj.applicantNo}`;

	if (concatAppId) {
		data = { ...data, applicationNo: buildPermitObj.applicantNo };
	}

	data.uploadFile && delete data.uploadFile;
	data.imagePreviewUrl && delete data.imagePreviewUrl;

	axios.defaults.headers.common['Authorization'] = getToken();

	try {
		const response = await axios.post(postUrl, data);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			if (fetchMenu) {
				await getBuildPermitData(buildPermitObj.applicantNo)(dispatch);
			}

			if (bClass) {
				dispatch({ type: SET_CURRENT_PERMIT, payload: { buildingClass: bClass } });
			}

			dispatch({
				type: SET_SUCCESS_FORM_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getFormDataWithPermit = (url, preparePrevData) => async dispatch => {
	const bpObj = getBuildPermitObj();
	// console.log('permit data', bpObj);
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: LOADING_UI });

	if (isEmpty(bpObj)) {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	} else {
		const getUrl = `${getBackUrl()}${url}${bpObj.applicantNo}`;

		dispatch({ type: SET_FORM_DATA, payload: { permitData: bpObj } });
		try {
			const response = await axios.get(getUrl);
			// console.log('response strcut b', response);
			if (response.data.error && response.data.error.message === 'invalid application no') {
				// console.log('Invalid Application Id');
			} else {
				// console.log('gotin');
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						prevData: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
						comment: response.data.comment,
						history: response.data.history,
						// permitData: bpObj
					},
				});
				dispatch({ type: STOP_LOADING });
			}
		} catch (err) {
			dispatch({ type: STOP_LOADING });
			dispatch({ type: SET_ERRORS, payload: err });
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					permitData: bpObj,
				},
			});
			console.log('Error in stuct form data fetch', err);
		}
	}
};

export const getFormDataWithPermitAndUser = (url, preparePrevData) => async dispatch => {
	const bpObj = getBuildPermitObj();
	const userObj = getUserInfoObj();
	// console.log('permit data', bpObj);
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_FORM_DATA });
	dispatch({ type: LOADING_UI });

	if (isEmpty(bpObj) || isEmpty(userObj)) {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	} else {
		const getUrl = `${getBackUrl()}${url}${bpObj.applicantNo}`;

		dispatch({
			type: SET_FORM_DATA,
			payload: { permitData: bpObj, userData: userObj },
		});
		try {
			const response = await axios.get(getUrl);
			if (response.data.error && response.data.error.message === 'invalid application no') {
				// console.log('Invalid Application Id');
			} else {
				// console.log('gotin');
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						prevData: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
						comment: response.data.comment,
						history: response.data.history,
						// permitData: bpObj
					},
				});
				dispatch({ type: STOP_LOADING });
			}
		} catch (err) {
			dispatch({ type: STOP_LOADING });
			dispatch({ type: SET_ERRORS, payload: err });
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					permitData: bpObj,
				},
			});
			console.log('Error in data fetch', err);
		}
	}
};

const setPreviousFormFilesUploaded = (fileCategories, files, formUrl) => dispatch => {
	
	try {
		const formGroup = JSON.parse(getLocalStorage(LSKey.FORM_GROUP)) || [];
		const formGroupRow = formGroup.find(obj => obj.formId.viewUrl.trim() === String(formUrl))
		const previousForm = formGroupRow.previousForm
		const previousFormRequiredFiles = fileCategories.filter(categories => categories.formId === previousForm && categories.isRequired === 'Y')
		// const hasFiles = previousFormRequiredFiles.every(cat => files.find(file => file.fileType.id === cat.formId))
		const hasFiles = previousFormRequiredFiles.every(cat => files.find(file => file.fileType.formName.id === cat.formId))
		dispatch({ type: SET_FORM_DATA, payload: { hasPreviousFiles: hasFiles }})
	} catch(err) {
		console.error('Error in previous file check', err)
	}

}

export const getFormContainerData = (urlArray, preparePrevData, formUtility, localDataArray, checkPrevious = false) => async dispatch => {
	// console.log('multiple === ', urlArray);
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_FORM_DATA });
	let permitObj = getBuildPermitObj();
	const userObj = getUserInfoObj();
	const fileCategories = JSON.parse(getLocalStorage(FILE_STORAGE_CATEGORY)) || [];
	const needsFile = fileCategories && fileCategories.some(category => category.viewUrl && category.viewUrl.trim() === formUtility.getUrl());

	if (localDataArray && Array.isArray(localDataArray) && localDataArray.length > 0) {
		try {
			for (const key of localDataArray) {
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						[key.getObjName()]: JSON.parse(getLocalStorage(key.getKey())),
					},
				});
			}
		} catch (err) {
			console.error(err, "Couldn't load local storage data");
		}
	}

	if (!isEmpty(permitObj) && !isEmpty(userObj)) {
		const appId = permitObj.applicantNo;

		if (needsFile || checkPrevious) {
			const getFileUrl = `${getBackUrl()}${api.fileUpload}${permitObj.applicantNo}`;

			try {
				const responseFile = await axios.get(getFileUrl);

				if (responseFile.data.error) {
					dispatch({ type: SET_ERRORS, payload: responseFile.data.error });
				} else {
					if (checkPrevious) setPreviousFormFilesUploaded(fileCategories, responseFile.data, formUtility.getUrl())(dispatch)
					dispatch({
						type: SET_FORM_DATA,
						payload: {
							files: responseFile.data,
							fileCategories: fileCategories,
						},
					});
				}
			} catch (error) {
				dispatch({ type: SET_ERRORS, payload: error });
				dispatch({ type: STOP_LOADING });
			}
		}
		makeRequestFile(0, urlArray, appId, permitObj, userObj, preparePrevData)(dispatch);

		// dispatch({ type: STOP_LOADING });
	} else {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
		dispatch({ type: STOP_LOADING });
	}
};

const makeRequestFile = (retry = 0, urlArray, appId, permitObj, userObj, preparePrevData) => async dispatch => {
	try {
		const responses = await Promise.all(
			urlArray.map(async url => {
				const getUrl = url.utility ? `${getBackUrl()}${url.api}` : `${getBackUrl()}${url.api}${appId}`;

				if (url.concatToApi) {
					const fieldValue = permitObj[url.concatToApi.fieldName];
					if (url.concatToApi.needsTranslation) {
						return axios.get(`${getUrl}${url.concatToApi.prefix}${translateNepToEng(fieldValue)}`);
					}
					return axios.get(`${getUrl}${url.concatToApi.prefix}${fieldValue}`);
				}
				return axios.get(getUrl);
			})
		);

		responses.forEach(response => {
			const url = urlArray.find(obj => response.config.url.includes(obj.api));
			if (url.form) {
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						[url.objName]: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
						comment: response.data.comment,
						history: response.data.history,
						permitData: permitObj,
						userData: userObj,
					},
				});
			} else {
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						[url.objName]: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
					},
				});
			}
		});
		dispatch({ type: STOP_LOADING });
	} catch (error) {
		if (retry < 3) {
			makeRequestFile(retry + 1, urlArray, appId, permitObj, userObj, preparePrevData)(dispatch);
		} else {
			console.log('Error in form data fetch', error, retry);
			dispatch({ type: SET_ERRORS, payload: error });
			dispatch({ type: STOP_LOADING });
			// throw error;
		}
	}
};

export const getMultipleFormDataByUrl = (urlArray, preparePrevData) => async dispatch => {
	// console.log('multiple === ', urlArray);
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_FORM_DATA });
	let obj = getBuildPermitObj();
	const userObj = getUserInfoObj();
	if (!isEmpty(obj) && !isEmpty(userObj)) {
		const appId = obj.applicantNo;
		Promise.all(
			urlArray.map(async url => {
				// try {
				//   const getUrl = url.utility
				//     ? `${getBackUrl()}${url.api}`
				//     : `${getBackUrl()}${url.api}${appId}`;
				//   const response = await axios.get(getUrl);

				//   // dispatch({ type: LOADING_UI });
				//   if (url.form) {
				//     dispatch({
				//       type: SET_FORM_DATA,
				//       payload: {
				//         [url.objName]: response.data.data
				//           ? preparePrevData(response.data.data)
				//           : preparePrevData(response.data),
				//         comment: response.data.comment,
				//         history: response.data.history,
				//         permitData: obj,
				//         userData: userObj
				//       }
				//     });
				//   } else {
				//     dispatch({
				//       type: SET_FORM_DATA,
				//       payload: {
				//         [url.objName]: response.data.data
				//           ? preparePrevData(response.data.data)
				//           : preparePrevData(response.data)
				//       }
				//     });
				//   }

				//   // dispatch({ type: STOP_LOADING });
				// } catch (err) {
				//   console.log('Error in form data fetch', err);
				//   dispatch({ type: SET_ERRORS, payload: err });
				//   dispatch({ type: STOP_LOADING });
				// }
				makeRequest(0, url, appId, preparePrevData, obj, userObj)(dispatch);
			})
		).then(() => {
			// dispatch({ type: STOP_LOADING });
		});
	} else {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
		dispatch({ type: STOP_LOADING });
	}
};

const makeRequest = (retry = 0, url, appId, preparePrevData, obj, userObj) => async dispatch => {
	// console.log('retry --- ', retry);
	try {
		const getUrl = url.utility ? `${getBackUrl()}${url.api}` : `${getBackUrl()}${url.api}${appId}`;
		const response = await axios.get(getUrl);

		// dispatch({ type: LOADING_UI });
		if (url.form) {
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					[url.objName]: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
					comment: response.data.comment,
					history: response.data.history,
					permitData: obj,
					userData: userObj,
				},
			});
		} else {
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					[url.objName]: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
				},
			});
		}
	} catch (error) {
		if (retry < 3) {
			await makeRequest(retry + 1, url, appId, preparePrevData, obj, userObj)(dispatch);
		} else {
			// console.log('Error in form data fetch', error);
			dispatch({ type: SET_ERRORS, payload: error });
			// dispatch({ type: STOP_LOADING });
		}
	}
};

export const postFormDataWithFileByUrl = (url, data, fileUrl, fileData, isFileSelected = false) => async dispatch => {
	let buildPermitObj = getBuildPermitObj();

	if (!isEmpty(buildPermitObj)) {
		const postUrl = `${getBackUrl()}${url}${buildPermitObj.applicantNo}`;
		const postFileUrl = `${getBackUrl()}${fileUrl}${buildPermitObj.applicantNo}`;

		data = { ...data, applicationNo: buildPermitObj.applicantNo };

		axios.defaults.headers.common['Authorization'] = getToken();

		try {
			const res = await Promise.all([await axios.post(postUrl, data), isFileSelected ? await axios.post(postFileUrl, fileData) : false]);

			if (res[0].data && res[0].data.error) {
				const errStr = `${res[0].data.error.message || ''}; ${res[1] ? res[1].data.error : ''}`;
				dispatch({ type: SET_ERRORS, payload: { message: errStr } });
			} else {
				const successMsg = `${res[0].data.message || ''}; ${res[1].data ? res[1].data.message : ''}`;
				dispatch({
					type: SET_SUCCESS_FORM_SUBMIT,
					payload: {
						success: true,
						data: { message: successMsg },
					},
				});
			}
		} catch (err) {
			console.log('g errs?', err);
			dispatch({ type: SET_ERRORS, payload: err });
		}
	} else {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
		dispatch({ type: STOP_LOADING });
	}
};

export const getPermitData = () => dispatch => {
	const bpObj = getBuildPermitObj();

	if (isEmpty(bpObj)) {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	}

	try {
		dispatch({ type: LOADING_UI });
		dispatch({
			type: SET_FORM_DATA,
			payload: {
				permitData: bpObj,
			},
		});
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: STOP_LOADING });
	}
};

export const getPermitAndUserData = () => dispatch => {
	dispatch({ type: SET_FORM_DATA, payload: { permitData: {} } });
	const bpObj = getBuildPermitObj();
	const userObj = getUserInfoObj();

	if (isEmpty(bpObj) || isEmpty(userObj)) {
		dispatch({
			type: SET_UNSET_PERMIT_ERRORS,
		});
	} else {
		try {
			dispatch({
				type: LOADING_UI,
			});
			dispatch({
				type: SET_FORM_DATA,
				payload: {
					permitData: bpObj,
					userData: userObj,
				},
			});
			dispatch({
				type: STOP_LOADING,
			});
		} catch (err) {
			dispatch({
				type: STOP_LOADING,
			});
			dispatch({ type: SET_ERRORS, payload: err });
		}
	}
};

export const putApprovalByUrl = (url, data) => async dispatch => {
	dispatch({ type: STOP_LOADING_MODAL });
	dispatch({ type: CLEAR_ERRORS });
	// console.log('put approval by url ', url, data);
	try {
		const token = getToken();
		axios.defaults.headers.common['Authorization'] = token;

		dispatch({ type: LOADING_MODAL });

		// setTimeout(async () => {
		const response = await axios.put(url, data);
		// console.log('Approval sent before', response);

		if (response.data && response.data.message) {
			dispatch({ type: STOP_LOADING_MODAL });

			// console.log('Approval sent', response);
			return response;
		} else {
			// const error =
			//   response.data && response.data.error
			//     ? response.data.error.message
			//     : 'Invalid user type. Only Sub-engineer or Engineer can approve.';
			// dispatch({ type: SET_MODAL_ERRORS, payload: error });
			dispatch({ type: STOP_LOADING_MODAL });
			return response;
		}

		// return response;
		// }, 3000);
	} catch (err) {
		dispatch({ type: STOP_LOADING_MODAL });
		// return new Promise(() => {
		throw new Error(err);
		// });
		// dispatch({ type: SET_MODAL_ERRORS, payload: err });
	}
};

export const clearErrors = () => dispatch => {
	dispatch({ type: CLEAR_MODAL_ERRORS });
};

/**
 * Get local api from local storage
 * @param {LocalAPIArray} localApis
 */
export const getLocalDataByKey = localApis => async dispatch => {
	dispatch({ type: LOADING_UI });
	Promise.all(
		localApis.getLocalApis().map(async api => {
			try {
				const data = await getLocalStorage(api.getKey());
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						[api.getObjName()]: JSON.parse(data),
					},
				});
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		dispatch({ type: STOP_LOADING });
	});
};
