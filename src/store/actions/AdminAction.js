import { LOADING_UI, CLEAR_ERRORS, SET_ERRORS, SET_ADMIN_DATA, CLEAR_ADMIN_DATA, STOP_LOADING, SET_SUCCESS_ADMIN_SUBMIT, CLEAR_ADMIN_SUCCESS, CLEAR_SUCCESS_PUBLIC_SUBMIT, CLEAR_SUCCESS_STATE } from '../types';
import { getToken } from '../../utils/functionUtils';
import axios from 'axios';
import { getBackUrl } from '../../utils/config';
import api from '../../utils/api';
import { getLocalStorage } from '../../utils/secureLS';
import { areUrlsEqual } from '../../utils/urlUtils';

export const getUsers = url => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getUserInfoObj();

	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/OrganizationUser/`;
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.get(getUrl);
		// console.log('response', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	// } else {
	//     dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	// }
};

export const postUsers = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const postUrl = `${getBackUrl()}api/Utility/OrganizationUser/`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const putUsers = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/OrganizationUser/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const deleteUsers = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/OrganizationUser/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getFiscalYears = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/FiscalYear/`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};
export const postFiscalYears = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const postUrl = `${getBackUrl()}api/Utility/FiscalYear/`;
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(postUrl, data);
		// console.log('response struct data submit', response);
		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const putFiscalYear = (yearCode, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/FiscalYear/${yearCode}`;
	// console.log("putedit", yearCode)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const deleteFiscalYear = yearCode => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/FiscalYear/${yearCode}`;
	// console.log("delete", yearCode)
	axios.defaults.headers.common['Authorization'] = getToken();

	try {
		const response = await axios.delete(deleteUrl);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getFileStorage = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/FileStorageCategory`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};
export const postFileStorage = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const postUrl = `${getBackUrl()}api/Utility/FileStorageCategory`;

	// console.log("data,", data)

	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(postUrl, data);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const putFileStorage = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/FileStorageCategory/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const deleteFileStorage = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/FileStorageCategory/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getFormGroupData = userType => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/FormGroup/${userType}`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { formList: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};

export const getFormNameMasterData = (update = false) => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/FormNameMaster`;
	const userTypeMasterUrl = `${getBackUrl()}${api.userType}`;
	// console.log('FormName');

	try {
		const response = await axios.get(getUrl);
		if (!update) {
			const responseUserType = await axios.get(userTypeMasterUrl);
			if (responseUserType.error) {
				dispatch({ type: SET_ERRORS, payload: response.error})
			} else {
				dispatch({
					type: SET_ADMIN_DATA,
					payload: { userTypeMaster: responseUserType.data },
				});
				dispatch({ type: STOP_LOADING });
			}
		}
		if (response.error) {
			dispatch({ type: SET_ERRORS, payload: response.error})
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { formNames: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};

export const getFormGroupMasterData = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/FormGroupMaster`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { group: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};

export const postFormDatas = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const postUrl = `${getBackUrl()}api/Utility/FormGroup`;

	axios.defaults.headers.common['Authorization'] = getToken();

	Object.keys(data).forEach(key => (data[key] = data[key].toString()));

	try {
		const response = await axios.post(postUrl, data);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const putFormDatas = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/FormGroup/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	Object.keys(data).forEach(key => {
		if (data[key]) {
			data[key] = data[key].toString();
		}
	});
	try {
		const response = await axios.put(putUrl, data);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const deleteFormDatas = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/FormGroup/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();

	try {
		const response = await axios.delete(deleteUrl);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const getDesigner = () => async dispatch => {
	// console.log("adminaction");

	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getUserInfoObj();

	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}${api.organizationUser}`;
	const renewUrl = `${getBackUrl()}${api.designerRenewStatus}`;
	const fiscalYearUrl = `${getBackUrl()}${api.fiscalYear}`;

	try {
		const response = await axios.get(getUrl);
		const renewResponse = await axios.get(renewUrl);
		const fiscalYearResponse = await axios.get(fiscalYearUrl);
		// console.log('renew response', renewResponse);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response', response);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else if (renewResponse.data.error) {
			dispatch({ type: SET_ERRORS, payload: renewResponse.data.error });
		} else if (fiscalYearResponse.data.error) {
			dispatch({ type: SET_ERRORS, payload: fiscalYearResponse.data.error });
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: {
					userData: response.data,
					renewalData: renewResponse.data,
					fiscalYear: fiscalYearResponse.data,
				},
			});
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	// } else {
	//     dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	// }
};

export const putDesigner = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/OrganizationUser/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const deleteDesigner = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/OrganizationUser/${id}`;
	// console.log("delete", id)
	// axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl, {
			headers: {
				Authorization: getToken(),
			},
			data: {
				source: 'khj',
			},
		});
		// axios.delete(deleteUrl, {
		//     headers: {
		//         "Authorization": getToken()
		//     },
		//     data: {}
		// });
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const postDesigner = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const postUrl = `${getBackUrl()}api/Utility/OrganizationUser/`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const renewDesigner = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/OrganizationUser/Renew`;
	// console.log("putedit", data)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const clearSuccessState = () => dispatch =>{
	dispatch({ type: CLEAR_SUCCESS_STATE });
}

export const getForwardData = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	dispatch({ type: CLEAR_SUCCESS_PUBLIC_SUBMIT });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/ApplicationForwardingSetup/`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in struct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};

export const getGroupwiseForwardData = groupId => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });

	const getUrl = `${getBackUrl()}${api.forwardMaster}?formGroup=${groupId}`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { forwardMaster: response.data },
			});
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const postForwardData = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	// console.log("post here")
	const postUrl = `${getBackUrl()}api/Utility/ApplicationForwardingSetup/`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const putForwardData = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/ApplicationForwardingSetup/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const deleteForwardData = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/ApplicationForwardingSetup/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const postFormName = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	// console.log("post here")
	const postUrl = `${getBackUrl()}api/Utility/FormNameMaster`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const putFormName = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/FormNameMaster/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const deleteFormName = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/FormNameMaster/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const getOrganizationData = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/OrganizationMaster`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in struct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};
export const postOrganization = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	// console.log("post here")
	const postUrl = `${getBackUrl()}api/Utility/OrganizationMaster`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const putOrganization = data => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ADMIN_SUCCESS });
	const postUrl = `${getBackUrl()}${api.organizationMaster}`;

	try {
		const response = await axios.put(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const getOtherSetBack = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/OtherSetBackMaster`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in struct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};
export const postOtherSetBack = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	// console.log("post here")
	const postUrl = `${getBackUrl()}api/Utility/OtherSetBackMaster`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const putOtherSetBack = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/OtherSetBackMaster/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const deleteOtherSetBack = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/OtherSetBackMaster/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const getRoadSetBack = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;

	const getUrl = `${getBackUrl()}api/Utility/RoadSetBackMaster`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'invalid application no') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in struct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
	//     } else {
	//         dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	//     }
};
export const postRoadSetBack = data => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	// console.log("post here")
	const postUrl = `${getBackUrl()}api/Utility/RoadSetBackMaster`;

	try {
		const response = await axios.post(postUrl, data);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const putRoadSetBack = (id, data) => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const putUrl = `${getBackUrl()}api/Utility/RoadSetBackMaster/${id}`;
	// console.log("putedit", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.put(putUrl, data);
		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
export const deleteRoadSetBack = id => async dispatch => {
	// const buildPermitObj = JSON.parse(
	//     getLocalStorage(CURRENT_BUILD_PERMIT)
	// );

	// const postUrl = withAppId
	//     ? `${getBackUrl()}${url}${buildPermitObj.applicantNo}`
	//     : `${getBackUrl()}${url}`;
	const deleteUrl = `${getBackUrl()}api/Utility/RoadSetBackMaster/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);
		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getDownloads = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });

	const getUrl = `${getBackUrl()}api/Application/PublicDownloads`;

	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		if (response.error && response.error.message === 'invalid application no') {
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in struct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const postDownloads = data => async dispatch => {
	const postUrl = `${getBackUrl()}api/Application/PublicDownloads`;

	try {
		axios.defaults.headers.common['Authorization'] = getToken();
		const response = await axios.post(postUrl, data);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const putDownloads = (id, data) => async dispatch => {
	const putUrl = `${getBackUrl()}api/Application/PublicDownloads/${id}`;
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.post(putUrl, data);

		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getFAQ = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });

	const getUrl = `${getBackUrl()}api/application/FrequentlyAskedQuestion`;
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.get(getUrl);
		// console.log('response', response);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { faqData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const postFAQ = data => async dispatch => {
	dispatch({ type: LOADING_UI });
	const postUrl = `${getBackUrl()}api/application/FrequentlyAskedQuestion`;

	try {
		const response = await axios.post(postUrl, data);
		console.log('data', response.data);
		axios.defaults.headers.common['Authorization'] = getToken();

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, faqdata: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const getContact = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });

	const getUrl = `${getBackUrl()}api/application/Contacts`;
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.get(getUrl);
		// console.log('response', response);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_ADMIN_DATA,
				payload: { conData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const postContact = data => async dispatch => {
	dispatch({ type: LOADING_UI });
	const postUrl = `${getBackUrl()}api/application/Contacts`;

	try {
		const response = await axios.post(postUrl, data);
		console.log('data', response.data);
		axios.defaults.headers.common['Authorization'] = getToken();

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, condata: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
		dispatch({ type: STOP_LOADING });
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const deleteDownloads = id => async dispatch => {
	const deleteUrl = `${getBackUrl()}api/Application/PublicDownloads/${id}`;
	// console.log("delete", id)
	axios.defaults.headers.common['Authorization'] = getToken();
	try {
		const response = await axios.delete(deleteUrl);
		// console.log('response struct data submit', response);
		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_ADMIN_SUBMIT,
				payload: { success: true, data: response.data },
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};

export const getAdminDataByUrl = urlArray => async dispatch => {
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: CLEAR_ADMIN_DATA });
	dispatch({ type: LOADING_UI });

	Promise.all(
		urlArray.map(async url => {
			try {
				const getUrl = `${getBackUrl()}${url.api}`;
				const response = await axios.get(getUrl);

				// console.log('response re', response);
				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
				} else {
					dispatch({
						type: SET_ADMIN_DATA,
						payload: {
							[url.objName]: response.data,
						},
					});
				}
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		dispatch({ type: STOP_LOADING });
	});
};

export const getAdminLocalDataByUrl = localApi => async dispatch => {
	dispatch({ type: LOADING_UI });
	Promise.all(
		localApi.map(async api => {
			try {
				const data = await getLocalStorage(api.key);
				dispatch({
					type: SET_ADMIN_DATA,
					payload: {
						[api.objName]: JSON.parse(data),
					},
				});
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		dispatch({ type: STOP_LOADING });
	});
};

export const getAfterUpdateAdminData = urlArray => async dispatch => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_ADMIN_DATA });

	try {
		const responses = await Promise.all(
			urlArray.map(async url => {
				const getUrl = `${getBackUrl()}${url.api}`;
				return axios.get(getUrl);
			})
		);
		responses.forEach(response => {
			const url = urlArray.find(obj => areUrlsEqual(response.config.url, obj.api));
			dispatch({
				type: SET_ADMIN_DATA,
				payload: {
					[url.objName]: response.data,
				},
			});
		});

		dispatch({ type: STOP_LOADING });
	} catch (error) {
		dispatch({ type: STOP_LOADING });
	}
};

export const postAdminDataByUrl = (url, data) => async dispatch => {
	dispatch({ type: CLEAR_ADMIN_SUCCESS });
	dispatch({ type: LOADING_UI });
	const postUrl = `${getBackUrl()}${url}`;

	try {
		const response = await axios.post(postUrl, data);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({ type: SET_SUCCESS_ADMIN_SUBMIT, payload: { success: true, data: response.data } });
		}
		dispatch({ type: STOP_LOADING });
	} catch (error) {
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

export const patchAdminDataByUrl = (url, data) => async dispatch => {
	dispatch({ type: CLEAR_ADMIN_SUCCESS });
	dispatch({ type: LOADING_UI });
	const patchUrl = `${getBackUrl()}${url}`;

	try {
		const response = await axios.patch(patchUrl, data);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({ type: SET_SUCCESS_ADMIN_SUBMIT, payload: { success: true, data: response.data } });
		}
	} catch (error) {
		dispatch({ type: SET_ERRORS, payload: error });
	}
	dispatch({ type: STOP_LOADING });
};

export const putAdminDataByUrl = (url, id, data) => async dispatch => {
	dispatch({ type: CLEAR_ADMIN_SUCCESS });
	dispatch({ type: LOADING_UI });
	const putUrl = `${getBackUrl()}${url}/${id}`;

	try {
		const response = await axios.put(putUrl, data);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({ type: SET_SUCCESS_ADMIN_SUBMIT, payload: { success: true, data: response.data } });
		}
	} catch (error) {
		dispatch({ type: SET_ERRORS, payload: error });
	}
	dispatch({ type: STOP_LOADING });
};

export const deleteAdminDataByUrl = (url, id) => async dispatch => {
	dispatch({ type: CLEAR_ADMIN_SUCCESS });
	dispatch({ type: LOADING_UI });
	const deleteUrl = `${getBackUrl()}${url}/${id}`;

	try {
		const response = await axios.delete(deleteUrl);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			dispatch({ type: SET_SUCCESS_ADMIN_SUBMIT, payload: { success: true, data: response.data } });
		}
	} catch (error) {
		dispatch({ type: SET_ERRORS, payload: error });
	}
	dispatch({ type: STOP_LOADING });
};
