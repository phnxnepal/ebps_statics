import axios from 'axios';
import {
    SET_ERRORS,
    SET_SUCCESS_FORM_SUBMIT,
    LOADING_UI,
    CLEAR_ERRORS,
    STOP_LOADING,
    SET_FORM_DATA,
    SET_FILE_LOADING,
    STOP_FILE_LOADING
} from '../types';
import { getToken, showToast } from '../../utils/functionUtils';
import { CURRENT_BUILD_PERMIT, FILE_STORAGE_CATEGORY, FORM_NAME_MASTER } from '../../utils/constants';
import { getBackUrl } from '../../utils/config';
import api from '../../utils/api';
import { getLocalStorage } from '../../utils/secureLS';

export const postFile = (url, file) => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    try {
        const token = getToken();
        axios.defaults.headers.common['Authorization'] = token;
        const response = await axios.post(url, file);
        if (response.data.error) {
            dispatch({ type: SET_ERRORS, payload: response.data.error });
        } else {
            dispatch({
                type: SET_SUCCESS_FORM_SUBMIT,
                payload: { success: true, data: response.data }
            });
        }
        dispatch({ type: STOP_LOADING });
        // console.log('response ', response);
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
    }
};

export const postFileValidation = (url, file) => async dispatch => {
	dispatch({ type: LOADING_UI });
	// dispatch({ type: CLEAR_ERRORS });
	try {
		const token = getToken();
		axios.defaults.headers.common['Authorization'] = token;
		const response = await axios.post(url, file);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		} else {
			showToast(response.data.message || 'File uploaded');
		}
		dispatch({ type: STOP_LOADING });
		// console.log('response ', response);
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const postFileWithProgress = (url, file, fileId) => async dispatch => {
    // dispatch({ type: SET_FILE_LOADING });
    
    const onUploadProgress = (e) => {
        dispatch({ type: SET_FILE_LOADING, payload: { id: fileId, progress: e.loaded, total: e.total}})
    }
	// dispatch({ type: CLEAR_ERRORS });
	try {
		const token = getToken();
		axios.defaults.headers.common['Authorization'] = token;
		const response = await axios.post(url, file, {
            headers: {
            "Content-Type": "multipart/form-data",
            },
            onUploadProgress,
        });
		if (response.data.error) {
            dispatch({ type: STOP_FILE_LOADING })
		} else {
			showToast(response.data.message || 'File uploaded');
        }
        /**
         * @TODO fix this
         */
        dispatch({ type: STOP_FILE_LOADING })
		dispatch({ type: STOP_LOADING });
		// console.log('response ', response);
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_FILE_LOADING })
		dispatch({ type: STOP_LOADING });
	}
};

export const getUploadedFiles = url => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    try {
        const token = getToken();
        const buildPermitObj = JSON.parse(
            getLocalStorage(CURRENT_BUILD_PERMIT)
        );

        const getUrl = `${getBackUrl()}${api.fileUpload}${
            buildPermitObj.applicantNo
        }`;

        axios.defaults.headers.common['Authorization'] = token;
        const response = await axios.get(getUrl);
        const fileCategories = JSON.parse(getLocalStorage(FILE_STORAGE_CATEGORY)) || []

        if (response.data.error) {
            dispatch({ type: SET_ERRORS, payload: response.data.error });
        } else {
            dispatch({
                type: SET_FORM_DATA,
                payload: {
                    files: response.data,
                    fileCategories: fileCategories,
                    success: false
                }
            });
        }

        dispatch({ type: STOP_LOADING });
        // console.log('response ', response);
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });

        dispatch({ type: STOP_LOADING });
    }
};

export const getUploadedFilesValidation = (applicationNo) => async dispatch => {
    dispatch({ type: LOADING_UI });
    try {
        const token = getToken();
        const getUrl = `${getBackUrl()}${api.fileUpload}${applicationNo}`;
        axios.defaults.headers.common['Authorization'] = token;
        const response = await axios.get(getUrl);
        const fileCategories = JSON.parse(getLocalStorage(FILE_STORAGE_CATEGORY)) || []

        if (response.data.error) {
            dispatch({ type: SET_ERRORS, payload: response.data.error });
        } else {
            dispatch({
                type: SET_FORM_DATA,
                payload: {
                    files: response.data,
                    fileCategories: fileCategories,
                    success: false
                }
            });
        }
        dispatch({ type: STOP_LOADING });
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
    }
};

export const getUploadedFilesMaster = (hasFiles) => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    try {
        const fileCategories = JSON.parse(getLocalStorage(FILE_STORAGE_CATEGORY)) || []
        const formMaster = JSON.parse(getLocalStorage(FORM_NAME_MASTER)) || []
        if (hasFiles) {
            dispatch({
                type: SET_FORM_DATA,
                payload: {
                    fileCategories: fileCategories,
                    formMaster: formMaster,
                    success: false
                }
            });
        } else {
            const token = getToken();
            const buildPermitObj = JSON.parse(
                getLocalStorage(CURRENT_BUILD_PERMIT)
            );
    
            const getUrl = `${getBackUrl()}${api.fileUpload}${
                buildPermitObj.applicantNo
            }`;
    
            axios.defaults.headers.common['Authorization'] = token;
            const response = await axios.get(getUrl);
    
            if (response.data.error) {
                dispatch({ type: SET_ERRORS, payload: response.data.error });
            } else {
                dispatch({
                    type: SET_FORM_DATA,
                    payload: {
                        files: response.data,
                        fileCategories: fileCategories,
                        formMaster: formMaster,
                        success: false
                    }
                });
            }
        }
        dispatch({ type: STOP_LOADING });
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
    }
};

export const getFileCategories = () => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    try {
        const token = getToken();
        // const getFileCatUrl = `${getBackUrl()}${api.fileCategories}`;

        axios.defaults.headers.common['Authorization'] = token;

        const fileCategories = JSON.parse(getLocalStorage(FILE_STORAGE_CATEGORY)) || [];

        // if (responseFileCategory.data.error) {
        //     dispatch({
        //         type: SET_ERRORS,
        //         payload: responseFileCategory.data.error
        //     });
        // } else {
            // console.log('responseFileCategor');
            // console.log(responseFileCategory);
            dispatch({
                type: SET_FORM_DATA,
                payload: {
                    fileCategories: fileCategories,
                    success: false
                }
            });
        // }

        dispatch({ type: STOP_LOADING });
        // console.log('response ', response);
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });

        dispatch({ type: STOP_LOADING });
    }
};

export const deleteFile = id => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    try {
        const token = getToken();

        const getFileCatUrl = `${getBackUrl()}${api.fileUpload}${id}`;

        axios.defaults.headers.common['Authorization'] = token;

        const response = await axios.delete(getFileCatUrl);
        if (response.data.error) {
            dispatch({ type: SET_ERRORS, payload: response.data.error });
            return response;
        } else {
            dispatch({
                type: SET_SUCCESS_FORM_SUBMIT,
                payload: { success: true, data: response.data }
            });
            return response;
        }
        // dispatch({ type: STOP_LOADING });
        // console.log('response ', response);
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
        console.log(err);
    }
};
