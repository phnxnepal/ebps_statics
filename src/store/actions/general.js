import { SET_LANGUAGE, SET_PRINT_CONTENT, SET_SHORTCUT } from '../types';
import { setLocalStorage } from '../../utils/secureLS';

export const setLanguage = lang => {
  setLocalStorage('language', lang);
  return { type: SET_LANGUAGE, data: lang };
};

export const setPrint = res => {
  return { type: SET_PRINT_CONTENT, data: res };
};

export const setShortcut = () => {
  return { type: SET_SHORTCUT };
};
