export const userObj = {
  dbPassword: null,
  dbpassword: 'EBPSADMIN',
  organization: {
    address: 'लहान, सिरहा, प्रदेश २, नेपाल',
    name: 'लहान नगरपालिका ',
    officename: 'नगर कार्यपालिकाको कार्यालय'
  },
  status: 'Y',
  token:
    'eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJBRE1JTiIsImF1ZCI6IkFETUlOIiwiaXNzIjoiQyIsInJvbGVzIjoidXNlciIsImlhdCI6MTU2NTY4MDY4MSwiZXhwIjoxNTY1NzY3MDgxfQ.VIwvg5NQftDv7JxdBi6FWmH_IKTAg4oTLe6QzCINqkA',
  userPassword: null,
  usercode: 'ADMIN',
  username: 'ADMIN',
  usertype: 'C'
};

export const demoData = {
  erStatus: 'N',
  enterDate: '2019-08-25',
  serDate: null,
  enterBy: 'designer',
  serName: null,
  details: [
    {
      descriptionId: 1,
      applicationNo: 7677000009,
      description: 'जग्गाको लम्बाई',
      remark: '1',
      sn: '1',
      type: '',
      designData: '1'
    },
    {
      descriptionId: 10,
      applicationNo: 7677000009,
      description: 'माटोको प्रकार',
      remark: '',
      sn: '10',
      type: '',
      designData: ''
    },
    {
      descriptionId: 11,
      applicationNo: 7677000009,
      description: 'माटोको भार वहन क्षमता (सोयल वियरिङ क्यापासिटि)',
      remark: '',
      sn: '11',
      type: '',
      designData: ''
    },
    {
      descriptionId: 2,
      applicationNo: 7677000009,
      description: 'जग्गाको चौडाई',
      remark: '1',
      sn: '2',
      type: '',
      designData: '1'
    },
    {
      descriptionId: 3,
      applicationNo: 7677000009,
      description: 'भवनको वर्गिकरण',
      remark: '1',
      sn: '3',
      type: '',
      designData: '1'
    },
    {
      descriptionId: 4,
      applicationNo: 7677000009,
      description: 'भवनको प्रयोग',
      remark: '9',
      sn: '4',
      type: '',
      designData: ''
    },
    {
      descriptionId: 5,
      applicationNo: 7677000009,
      description: 'प्लिन्थ एरिया',
      remark: '',
      sn: '5',
      type: '',
      designData: ''
    },
    {
      descriptionId: 6,
      applicationNo: 7677000009,
      description: 'तला संख्या',
      remark: '',
      sn: '6',
      type: '',
      designData: ''
    },
    {
      descriptionId: 7,
      applicationNo: 7677000009,
      description: 'घरको कुल उचाई',
      remark: '',
      sn: '7',
      type: '',
      designData: ''
    },
    {
      descriptionId: 8,
      applicationNo: 7677000009,
      description: 'भवन निर्माणको किसिम',
      remark: '',
      sn: '8',
      type: '',
      designData: ''
    },
    {
      descriptionId: 9,
      applicationNo: 7677000009,
      description: 'भवनको स्ट्रक्चरल सिस्टम',
      remark: '',
      sn: '9',
      type: '',
      designData: ''
    },
    {
      descriptionId: 12,
      applicationNo: 7677000009,
      description: 'जगको प्रकार',
      remark: 'asdf',
      sn: '9.1.1',
      type: 'P',
      designData: 'asf'
    },
    {
      descriptionId: 21,
      applicationNo: 7677000009,
      description: 'कंक्रिट ब्याण्डहरु राखिएको छ/छैन',
      remark: '',
      sn: '9.1.10',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 13,
      applicationNo: 7677000009,
      description: 'जगको गहिराइ',
      remark: '',
      sn: '9.1.2',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 14,
      applicationNo: 7677000009,
      description: 'जगको साइजहरू',
      remark: '',
      sn: '9.1.3',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 15,
      applicationNo: 7677000009,
      description: 'पिलरका साइजहरू',
      remark: '',
      sn: '9.1.4',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 16,
      applicationNo: 7677000009,
      description: 'पिलरमा प्रयोग गर्ने डण्डीका साइज र संख्या',
      remark: '',
      sn: '9.1.5',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 17,
      applicationNo: 7677000009,
      description: 'विमको स्थान',
      remark: '',
      sn: '9.1.6',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 18,
      applicationNo: 7677000009,
      description: 'विमको साइजहरु',
      remark: '',
      sn: '9.1.7',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 19,
      applicationNo: 7677000009,
      description: 'स्ल्याबको मोटाई/staircase',
      remark: '',
      sn: '9.1.8',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 20,
      applicationNo: 7677000009,
      description: 'कंक्रिटको ग्रेड (सिमेन्टःबालुवाःरोडा)',
      remark: '',
      sn: '9.1.9',
      type: 'P',
      designData: ''
    },
    {
      descriptionId: 22,
      applicationNo: 7677000009,
      description: 'जगको गहिराइ',
      remark: '',
      sn: '9.2.1',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 31,
      applicationNo: 7677000009,
      description: 'भर्टिकल डण्डीको साईज (कुना र कर्नर ज्वाईन्टहरुमा)',
      remark: '',
      sn: '9.2.10',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 32,
      applicationNo: 7677000009,
      description: 'कर्नर स्टिचिङ कंक्रिट व्याण्डहरु राखिएको छ/छैन',
      remark: '',
      sn: '9.2.11',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 33,
      applicationNo: 7677000009,
      description: 'अन्य',
      remark: '',
      sn: '9.2.12',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 23,
      applicationNo: 7677000009,
      description: 'जगको चौडाई',
      remark: 'adsf',
      sn: '9.2.2',
      type: 'G',
      designData: 'asdf'
    },
    {
      descriptionId: 24,
      applicationNo: 7677000009,
      description: 'इट्टाको क्रसिङ्ग स्ट्रेन्थ',
      remark: '',
      sn: '9.2.3',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 25,
      applicationNo: 7677000009,
      description: 'कंक्रिटको ग्रेड (सिमेन्टःबालुवाःरोडा)',
      remark: '',
      sn: '9.2.4',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 26,
      applicationNo: 7677000009,
      description: 'स्ल्याबको मोटाई',
      remark: '',
      sn: '9.2.5',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 27,
      applicationNo: 7677000009,
      description: 'फ्लोर विमको साइजहरु',
      remark: '',
      sn: '9.2.6',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 28,
      applicationNo: 7677000009,
      description: 'गारोमा सिमेन्ट, बालुवाको भाग',
      remark: '',
      sn: '9.2.7',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 29,
      applicationNo: 7677000009,
      description: 'गारोको विवरण',
      remark: '',
      sn: '9.2.8',
      type: 'G',
      designData: ''
    },
    {
      descriptionId: 30,
      applicationNo: 7677000009,
      description: 'कंक्रिट ब्याण्डरु राखिएको छ/छैन',
      remark: '',
      sn: '9.2.9',
      type: 'G',
      designData: ''
    }
  ],
  erDate: null,
  erName: null,
  serStatus: 'N'
};

export const rajaswaData = {
  applicationNo: 7677000004,
  enterBy: 'AAAA',
  enterDate: '2019-08-25',
  rEntery: '',
  rDate: '2019-08-25',
  rStatus: 'A',
  remark: '',
  sakhaDartaNo: '',
  sakhaDartaDate: '',
  amtOfDharauti: '',
  amtReceivedDate: '',
  rashidNumber: '',
  receiveSign: '',
  documnentDetail: '',
  aminOpinion: '',
  sansodhanDetail: '',
  layoutAmt: 0.0,
  designAmt: 0.0,
  formAmt: 0.0,
  applicationAmt: 0.0,
  totalAmt: 0.0,
  details: [
    {
      dArea: 1.0,
      jArea: 1.0,
      drRate: 1.0,
      jrRate: 1.0,
      drAmount: 1.0,
      jrAmount: 1.0,
      depositRate: 1.0,
      depositAmount: 1.0,
      totalAmount: 1.0,
      floor: 1.0
    },
    {
      dArea: 2.0,
      jArea: 2.0,
      drRate: 2.0,
      jrRate: 2.0,
      drAmount: 2.0,
      jrAmount: 2.0,
      depositRate: 2.0,
      depositAmount: 2.0,
      totalAmount: 2.0,
      floor: 2.0
    }
  ],
  amountInWords: ''
};

const getDemoData = () => ({
  userPassword: null,
  dbpassword: 'ADMIN',
  organization: {
    name: 'नमुना नगरपालिका ',
    officename: 'नगर कार्यपलिकको कार्यालय  ',
    address: 'काठमाडौं , नेपाल '
  },
  usertype: 'CEO',
  usercode: 'ADMIN',
  username: 'ADMIN',
  status: 'Y'
});

const getDemoDataUser = () => ({
  loginId: 'ADMIN',
  id: 1,
  userName: 'ADMIN',
  address: 'ADMIN',
  email: 'ADMIN',
  mobile: ' ',
  status: 'Y',
  joinDate: '2019-08-06',
  signature: ' ',
  photo: ' ',
  educationQualification: 'ADMIN',
  licenseNo: ' ',
  municipalRegNo: ' ',
  stamp: ' ',
  userType: 'CEO'
});

const dataToSend = {
  applicantYear: 7677,
  applicantMs: '1',
  applicantName: '1',
  applicantAddress: '1',
  applicantMobileNo: '1',
  citizenshipNo: '1',
  nationalIdNo: '1',
  fathersSpouseName: '1',
  oldMunicipal: '1',
  oldWardNo: '1',
  newMunicipal: '1',
  newWardNo: '1',
  kittaNo: '1',
  landArea: '1',
  nearestLocation: '1',
  buildingJoinRoad: '1',
  buildingJoinRoadType: '1',
  buildingJoinRoadTypeOther: '1',
  landPassDate: '1',
  landRegNo: '1',
  ownershipName: '1',
  certificateArea: '1',
  landCertificateNo: '1',
  landCertificateIssueDate: '1',
  purposeOfConstruction: '1',
  purposeOfConstructionOther: '1',
  constructionType: '1',
  constructionTypeOther: '1',
  oldMapDate: '1',
  mohada: '1',
  constructionFinishing: '1',
  constructionFinishingOther: '1',
  dhalNikasArrangement: '1',
  dhalNikasArrangementOther: '1',
  foharArrangement: '1',
  foharArrangementOther: '1',
  pipeline: '1',
  pipelineDistance: '1',
  doPipelineConnection: '1',
  isHighTensionLine: '1',
  isHighTensionLineDistance: '1',
  member: [
    {
      relation: '1',
      memberName: '1'
    },
    {
      relation: '176770001',
      memberName: '11176770001'
    },
    {
      relation: '76770001',
      memberName: '76770001'
    },
    {
      relation: '7677000111',
      memberName: '7677000111'
    },
    {
      relation: '7677000133',
      memberName: '7677000133'
    }
  ],
  surrounding: [
    {
      feet: 111,
      district: '111',
      municipal: '111',
      wardNo: '111',
      side: 111,
      sandhiyar: '1111'
    },
    {
      feet: 1212,
      district: '12',
      municipal: '1212',
      wardNo: '1',
      side: 212,
      sandhiyar: '1121'
    },
    {
      feet: 121,
      district: '121',
      municipal: '2121',
      wardNo: '212',
      side: 12,
      sandhiyar: '12'
    },
    {
      feet: 21121220,
      district: '12121212',
      municipal: '121212',
      wardNo: '212',
      side: 121212,
      sandhiyar: '245'
    }
  ],
  floor: [
    {
      length: 1,
      width: 1,
      height: 1,
      area: 1,
      feetArea: 1,
      floor: 1
    },
    {
      length: 0,
      width: 0,
      height: 0,
      area: 0,
      feetArea: 0,
      floor: 2
    },
    {
      length: 1,
      width: 1,
      height: 1,
      area: 1,
      feetArea: 1,
      floor: 0
    },
    {
      length: 3,
      width: 3,
      height: 3,
      area: 3,
      feetArea: 3,
      floor: 3
    }
  ]
};