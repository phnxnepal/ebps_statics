import {
  LOADING_UI,
  CLEAR_ERRORS,
  SET_ERRORS,
  SET_UNSET_PERMIT_ERRORS,
  SET_FORM_DATA
} from '../types';
import axios from 'axios';
import { getBackUrl } from '../../utils/config';
import { isEmpty, getToken } from '../../utils/functionUtils';
import { CURRENT_BUILD_PERMIT, USER_INFO } from '../../utils/constants';
import { getLocalStorage } from '../../utils/secureLS';

export const getFormData = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
  const data = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));
  const UserInfoData = JSON.parse(getLocalStorage(USER_INFO));
  if (isEmpty(data)) {
    dispatch({
      type: SET_UNSET_PERMIT_ERRORS
    });
  } else {
    dispatch({
      type: SET_FORM_DATA,
      payload: {
        //   groups: groups,
        permitData: data, //demoData
        userData: UserInfoData //demoData
        //   prevData: prevData
      }
    });
  }
};

export const getUserInfo = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
  const UserInfoData = JSON.parse(getLocalStorage(USER_INFO));

  dispatch({
    type: SET_FORM_DATA,
    payload: {
      UserData: UserInfoData
    }
  });
};

export const getOptionData = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
  const buildPermitObj = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));

  if (isEmpty(buildPermitObj)) {
    dispatch({
      type: SET_UNSET_PERMIT_ERRORS
    });
  } else {
    const url = `${getBackUrl()}api/Processing/Phase1/AnusuchiKa/${
      buildPermitObj.applicantNo
      }`;
    dispatch({ type: LOADING_UI });

    return axios
      .get(url)
      .then(res => {
        // console.log('Get option data--', res);

        if (isEmpty(res.data.error)) {
          dispatch({
            type: SET_FORM_DATA,
            payload: {
              //   groups: groups,
              optionData: res.data.data //demoData
              //   prevData: prevData
            }
          });
        } else {
          dispatch({
            type: SET_ERRORS,
            payload: res.data.error
          });
        }
      })
      .catch(err => {
        console.log(err);
        dispatch({
          type: SET_ERRORS,
          payload: err
        });
      });
  }
};

export const getDesignerData = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
  const buildPermitObj = JSON.parse(getLocalStorage(CURRENT_BUILD_PERMIT));

  if (isEmpty(buildPermitObj)) {
    dispatch({
      type: SET_UNSET_PERMIT_ERRORS
    });
  } else {
    const url = `${getBackUrl()}api/Processing/Phase1/AnusuchiGa/${
      buildPermitObj.applicantNo
      }`;

    dispatch({ type: LOADING_UI });
    return axios
      .get(url)
      .then(res => {
        // console.log('Get Designer data--', res);

        // if (isEmpty(res.data.error)) {
        // const designerData = res.data.filter(
        //   row => row.applicationNo === buildPermitObj.applicantNo
        // );
        dispatch({
          type: SET_FORM_DATA,
          payload: {
            designerData: res.data.data,
            comment: res.data.comment,
            history: res.data.history
          }
        });
        // } else {
        //   dispatch({
        //     type: SET_ERRORS,
        //     payload: res.data.error
        //   });
        // }
      })
      .catch(err => {
        console.log(err);
        dispatch({
          type: SET_ERRORS,
          payload: err
        });
      });
  }
};

export const postFormData = (data, appId) => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
  const url = `${getBackUrl()}api/Processing/Phase1/AnusuchiGa/${appId}`;
  const token = getToken();

  data.applicationNo = appId;

  axios.defaults.headers.common['Authorization'] = token;
  return axios
    .post(url, data, {
      headers: {
        Authorization: token
      }
    })
    .then(res => {
      // console.log('response -- after submit', res);
      if (res.data.error) {
        dispatch({ type: SET_ERRORS, payload: res.data.error.message });
      }
      return res;
    })
    .catch(err => {
      console.log('err', err);
      dispatch({ type: SET_ERRORS, payload: err });
      return { error: err };
    });
};
