import axios from 'axios';
import { SET_SUCCESS_FORM_SUBMIT, SET_ERRORS } from "../types";
import { getToken } from "../../utils/functionUtils";

export const putFormDataByUrl = (
	url,
	data,
) => async dispatch => {

	axios.defaults.headers.common['Authorization'] = getToken();

	try {
		const response = await axios.put(url, data);

		// console.log('response struct data submit', response);

		if (response.data.message === 'Success') {
			dispatch({
				type: SET_SUCCESS_FORM_SUBMIT,
				payload: { success: true, data: response.data }
			});
		} else {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
		}
	} catch (err) {
		dispatch({ type: SET_ERRORS, payload: err });
	}
};
