import { getBackUrl } from '../../utils/config';
import axios from 'axios';
import { getToken, isEmpty } from '../../utils/functionUtils';
import { SET_ERRORS, LOADING_UI, STOP_LOADING, SET_SUCCESS_FORM_SUBMIT, CLEAR_ERRORS, SET_UNSET_PERMIT_ERRORS, SET_FORM_DATA } from '../types';

import { getUserRole, getBuildPermitObj } from '../../utils/functionUtils';
import api from '../../utils/api';
import { isStringEmpty } from '../../utils/stringUtils';
import { genericErrors } from '../../utils/data/validationData';

export const postForwardToNext = (groupId, sendTo, forcefull = false, autoSaveForm = null) => async (dispatch) => {
	dispatch({ type: LOADING_UI });
	const userType = getUserRole();
	const permit = getBuildPermitObj();

	if (isStringEmpty(userType) || isStringEmpty(permit)) {
		dispatch({
			type: SET_ERRORS,
			payload: { message: 'User type or appId not found.' },
		});
		dispatch({ type: STOP_LOADING });
	} else {
		const postUrl = isStringEmpty(sendTo)
			? `${getBackUrl()}${api.forwardToNext}${userType}/${permit.applicantNo}/${groupId}`
			: `${getBackUrl()}${api.forwardToNext}${userType}/${permit.applicantNo}/${groupId}?forwaTo=${sendTo}`;
		console.log(postUrl)
		const forcefullUrl = isStringEmpty(sendTo)
			? `${getBackUrl()}${api.forwardToNext}${userType}/${permit.applicantNo}/${groupId}?forcefully=Y`
			: `${getBackUrl()}${api.forwardToNext}${userType}/${permit.applicantNo}/${groupId}?forwaTo=${sendTo}&forcefully=Y`;

		axios.defaults.headers.common['Authorization'] = getToken();

		if (autoSaveForm) {
			const autoSaveUrl = `${getBackUrl()}api/Processing/Skip/${permit.applicantNo}/${autoSaveForm}`;
			try {
				const autoSaveReponse = await axios.post(autoSaveUrl);

				if (autoSaveReponse.data.data[0].message === 'Success') {
					const response = await axios.post(forcefull ? forcefullUrl : postUrl);

					if (response.data.error) {
						dispatch({ type: SET_ERRORS, payload: response.data.error });
					} else {
						dispatch({
							type: SET_SUCCESS_FORM_SUBMIT,
							payload: { success: true, data: response.data },
						});
					}
					dispatch({ type: STOP_LOADING });
				} else {
					const hasError = autoSaveReponse.data.data.some((row) => row.body && row.body.error && row.body.error.message);
					hasError && dispatch({ type: SET_ERRORS, payload: { message: genericErrors.technicalError } });
				}
			} catch (error) {
				dispatch({ type: SET_ERRORS, payload: error });
				dispatch({ type: STOP_LOADING });
				return;
			}
		} else {
			try {
				const response = await axios.post(forcefull ? forcefullUrl : postUrl);

				// console.log('response forward to next', response);
				if (response.data.error) {
					dispatch({ type: SET_ERRORS, payload: response.data.error });
				} else {
					dispatch({
						type: SET_SUCCESS_FORM_SUBMIT,
						payload: { success: true, data: response.data },
					});
				}
				dispatch({ type: STOP_LOADING });
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
				dispatch({ type: STOP_LOADING });
			}
		}
	}
};

export const getFormDataWithPermit = (url, preparePrevData) => async dispatch => {
	const bpObj = getBuildPermitObj();
	dispatch({ type: LOADING_UI });

	if (isEmpty(bpObj)) {
		dispatch({ type: SET_UNSET_PERMIT_ERRORS });
	} else {
		const getUrl = `${getBackUrl()}${url}${bpObj.applicantNo}`;

		dispatch({ type: SET_FORM_DATA, payload: { permitData: bpObj } });
		try {
			const response = await axios.get(getUrl);
			if (response.data.error && response.data.error.message === 'invalid application no') {
				
			} else {
				dispatch({
					type: SET_FORM_DATA,
					payload: {
						amountData: response.data.data ? preparePrevData(response.data.data) : preparePrevData(response.data),
					},
				});
				dispatch({ type: STOP_LOADING });
			}
		} catch (err) {
			dispatch({ type: STOP_LOADING });
			dispatch({ type: SET_ERRORS, payload: err });
			console.log('Error in stuct form data fetch', err);
		}
	}
};