import { LOADING_UI, STOP_LOADING, SET_ERRORS, SET_CURRENT_DATA, SET_FORM_DATA } from './../types';
import { getBackUrl } from './../../utils/config';
import { getToken, getUserInfoObj } from '../../utils/functionUtils';
import axios from 'axios';
import { isArray } from 'util';
import { getLocalDataByKey } from './dashboardActions';

export const fetchLocalDataAndReports = localApis => async (dispatch, getState) => {
	await dispatch(getLocalDataByKey(localApis, false));
	const dashboardState = getState().root.dashboard;
	const fiscalYear = dashboardState.fiscalYears ? dashboardState.fiscalYears[0].yearCode : 7677;
	const wardNo = '', constructionType = '';

	dispatch(getReportPermits(fiscalYear, wardNo, constructionType));
};

export const getReportPermits = (id, wardNo, constructionType) => async dispatch => {
	dispatch({ type: LOADING_UI });
	// dispatch({ type: CLEAR_FORM_DATA });
	// dispatch({ type: CLEAR_BUILD_PERMITS });
	const url = `${getBackUrl()}api/Utility/FormReport/${id}?wardNo=${wardNo}&constructionType=${constructionType}`;

	const token = getToken();

	/**
	 * @todo confirms if this should be removed?
	 * */

	// localStorage.removeItem(CURRENT_BUILD_PERMIT);
	// localStorage.removeItem(MENU_LIST);
	// localStorage.removeItem(BUILDING_CLASS);
	// localStorage.removeItem(PLOR_SANSODHAN);

	try {
		const response = await axios.get(url, {
			headers: {
				Authorization: token,
			},
		});
		// console.log('response', response.data);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			dispatch({ type: STOP_LOADING });
		} else {
			// dispatch({ type: CLEAR_ERRORS });
			dispatch({
				type: SET_CURRENT_DATA,
				payload: { reportPermits: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: STOP_LOADING });
	}
};

export const getReportData = id => async dispatch => {
	dispatch({ type: SET_CURRENT_DATA, payload: { report: { loading: true } } });
	// dispatch({ type: CLEAR_FORM_DATA });
	// dispatch({ type: CLEAR_BUILD_PERMITS });
	const url = `${getBackUrl()}api/Utility/FormReport/${id}?wardNo=&constructionType=`;

	const token = getToken();

	/**
	 * @todo confirms if this should be removed?
	 * */

	// localStorage.removeItem(CURRENT_BUILD_PERMIT);
	// localStorage.removeItem(MENU_LIST);
	// localStorage.removeItem(BUILDING_CLASS);
	// localStorage.removeItem(PLOR_SANSODHAN);

	try {
		const response = await axios.get(url, {
			headers: {
				Authorization: token,
			},
		});
		// console.log('response', response.data);
		if (response.data.error) {
			dispatch({ type: SET_ERRORS, payload: response.data.error });
			dispatch({ type: SET_CURRENT_DATA, payload: { report: { loading: false } } });
		} else {
			dispatch({ type: SET_CURRENT_DATA, payload: { report: { data: response.data, loading: false } } });
		}
	} catch (error) {
		console.log('err', error);
		dispatch({ type: SET_ERRORS, payload: error });
		dispatch({ type: SET_CURRENT_DATA, payload: { report: { loading: false } } });
	}
};

export const getFiscalYears = () => async dispatch => {
	dispatch({ type: LOADING_UI });
	// dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_ADMIN_DATA });
	// let obj = getBuildPermit();
	// if (!isEmpty(obj)) {
	// const appId = obj.applicantNo;
	const userObj = getUserInfoObj();
	const getUrl = `${getBackUrl()}api/Utility/FiscalYear/`;
	makeFiscalRequest(getUrl)(dispatch);

	dispatch({ type: SET_FORM_DATA, payload: { userData: userObj } });
	dispatch({ type: STOP_LOADING });
};

const makeFiscalRequest = (getUrl, retry = 0) => async dispatch => {
	try {
		const response = await axios.get(getUrl);
		axios.defaults.headers.common['Authorization'] = getToken();
		// console.log('reponse', response);
		if (response.error && response.error.message === 'Invalid Application No') {
			// console.log('Invalid Application Id');
		} else {
			if (isArray(response.data) && response.data.length < 1 && retry < 3) {
				makeFiscalRequest(getUrl, retry + 1)(dispatch);
			}
			dispatch({
				type: SET_CURRENT_DATA,
				payload: { fiscalYear: response.data },
			});
		}
	} catch (err) {
		console.log('Error in stuct form data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};
