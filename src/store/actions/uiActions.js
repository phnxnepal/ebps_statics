import { TOGGLE_NAVBAR } from '../types';

export const toggleNavbar = (value) => {
	return {
		type: TOGGLE_NAVBAR,
		payload: value
	};
};
