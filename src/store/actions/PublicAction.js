import {
    LOADING_UI,
    CLEAR_ERRORS,
    SET_ERRORS,
    STOP_LOADING,
    SET_PUBLICDESIGNER_DATA,
    // CLEAR_PUBLICDESIGNER_DATA,
    // CLEAR_PUBLICMASON_DATA,
    SET_PUBLICMASON_DATA,
    SET_SUCCESS_PUBLIC_SUBMIT,
    CLEAR_SUCCESS_PUBLIC_SUBMIT,
    SET_PUBLIC_DATA,
	// CLEAR_PUBLIC_DATA,
} from '../types';
import { getBackUrl, getRecaptchaSecretKey } from '../../utils/config';
import axios from 'axios';

export const getPublicDesigner = () => async (dispatch) => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_PUBLICDESIGNER_DATA });
	const getUrl = `${getBackUrl()}api/DesignerList`;
	// console.log("get", getUrl);

	try {
		const response = await axios.get(getUrl);
		// console.log("res", response);

		if (response.error && response.error.message === 'No Designers Found') {
			// console.log('Invalid Application Id');
		} else {
			dispatch({
				type: SET_PUBLICDESIGNER_DATA,
				payload: { userData: response.data },
			});
			dispatch({ type: STOP_LOADING });
		}
	} catch (err) {
		console.log('Error in designer data fetch', err);
		dispatch({ type: SET_ERRORS, payload: err });
		dispatch({ type: STOP_LOADING });
	}
};

export const getPublicMason = () => async (dispatch) => {
	dispatch({ type: LOADING_UI });
	dispatch({ type: CLEAR_ERRORS });
	// dispatch({ type: CLEAR_PUBLICMASON_DATA });
	const getUrl = `${getBackUrl()}api/Utility/MasonList`;
	// console.log("get", getUrl);

	try {
		const response = await axios.get(getUrl);
		// console.log("res", response);

        if (
            response.error &&
            response.error.message === 'No Masons Found'
        ) {
            // console.log('Invalid Application Id');
        } else {
            dispatch({
                type: SET_PUBLICMASON_DATA,
                payload: { userData: response.data }
            });
            dispatch({ type: STOP_LOADING });
        }
    } catch (err) {
        console.log('Error in mason data fetch', err);
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
    }
};

export const postByUrl = (url, data) => async dispatch => {
    dispatch({ type: LOADING_UI });
    dispatch({ type: CLEAR_ERRORS });
    dispatch({ type: CLEAR_SUCCESS_PUBLIC_SUBMIT });
    const postUrl = `${getBackUrl()}${url}`;

    try {
        const response = await axios.post(postUrl, data);


        if (response.data.message === 'Success') {

            dispatch({
                type: SET_SUCCESS_PUBLIC_SUBMIT,
                payload: { success: true, data: response.data },
            });
            dispatch({ type: STOP_LOADING });
        } else {
            dispatch({ type: SET_ERRORS, payload: response.data.error });
            dispatch({ type: STOP_LOADING });
        }
    } catch (err) {
        dispatch({ type: SET_ERRORS, payload: err });
        dispatch({ type: STOP_LOADING });
    }
};

export const getPublicDataByUrl = (urlArray) => async (dispatch, getState) => {
	dispatch({ type: CLEAR_ERRORS });
	dispatch({ type: LOADING_UI });

	const publicState = getState().root.public;

	Promise.all(
		urlArray.map(async (urlObject) => {
            const url = urlObject.getParams();
			try {
				if (!publicState[url.objName]) {
					const getUrl = `${getBackUrl()}${url.api}`;
					const response = await axios.get(getUrl);
					if (response.data.error) {
						dispatch({ type: SET_ERRORS, payload: response.data.error });
					} else {
						dispatch({
							type: SET_PUBLIC_DATA,
							payload: {
								[url.objName]: response.data,
							},
						});
					}
				}
			} catch (err) {
				dispatch({ type: SET_ERRORS, payload: err });
			}
		})
	).then(() => {
		dispatch({ type: STOP_LOADING });
	});
};


export const postCatchaVerify = (token) => async dispatch => {
    const postUrl = 'https://www.google.com/recaptcha/api/siteverify';

    try {
        const response = await axios.post(postUrl, {
			secret: getRecaptchaSecretKey(), response: token
		});

		console.log('response', response);

        // if (response.data.message === 'Success') {

        //     dispatch({
        //         type: SET_SUCCESS_PUBLIC_SUBMIT,
        //         payload: { success: true, data: response.data },
        //     });
        //     dispatch({ type: STOP_LOADING });
        // } else {
        //     dispatch({ type: SET_ERRORS, payload: response.data.error });
        //     dispatch({ type: STOP_LOADING });
        // }
    } catch (err) {
        console.log('err', err);
        // dispatch({ type: SET_ERRORS, payload: err });
        // dispatch({ type: STOP_LOADING });
    }
};