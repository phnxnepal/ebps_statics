import { getLocalStorage } from "../../utils/secureLS"
import { ORG_IDS } from "../../utils/constants"

export const selectOrganizationName = (state: { organization: { name: string } }): string => {

    if (state.organization && state.organization.name)
        return state.organization.name
    else {
        const organizationLocalStorage = getLocalStorage(ORG_IDS)

        if (organizationLocalStorage) {
            const organization: { organization: { name: string } } = JSON.parse(organizationLocalStorage)
            if (organization && organization.organization && organization.organization.name) {
                return organization.organization.name
            }
        }
        return ''
    }
}

export const selectNeedsPublicSignup = (state: { organization: { designerLoginType: string } }): boolean => {

    if (state.organization && state.organization.designerLoginType)
        return state.organization.designerLoginType === 'public'
    else {
        const organizationLocalStorage = getLocalStorage(ORG_IDS)

        if (organizationLocalStorage) {
            const organization: { organization: { designerLoginType: string } } = JSON.parse(organizationLocalStorage)
            if (organization && organization.organization && organization.organization.designerLoginType) {
                return organization.organization.designerLoginType === 'public'
            }
        }
        return false
    }
}