import React, { useState } from 'react';
import GenericSetupTable from './forms/GenericSetupTable';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { namsariHistory, historyDatafield } from '../../utils/data/ebps-setup/namsariSetupData';

export const NamsariHistory = ({ historyList }) => {
	const [historyChanged, setHistoryChanged] = useState(false);

	useJqxNepaliFilter(historyChanged, [2, 4], []);

	const generateViewButtonRenderer = row => {
		setHistoryChanged(!historyChanged);
		return '<p></p>';
	};

	return (
		<div>
			<GenericSetupTable
				data={historyList}
				columns={namsariHistory}
				datafield={historyDatafield}
				generateButtonRenderer={generateViewButtonRenderer}
				actionWidth={0}
				needsAction={false}
			/>
		</div>
	);
};
