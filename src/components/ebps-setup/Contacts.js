import React, { Component } from 'react';
import CKEditor from 'react-ckeditor-component';
import { Segment, Form, Button } from 'semantic-ui-react';
import { showToast } from './../../utils/functionUtils';

//redux
import { connect } from 'react-redux';
import { getContact, postContact } from './../../store/actions/AdminAction';
import Helmet from 'react-helmet';

class FAQ extends Component {
	constructor(props) {
		super(props);
		this.state = {
			content: ' ',
			isSubmitting: false,
		};
		this.onChange = this.onChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		this.props.getContact();

		this.setState({ content: this.props.conData });
	}

	setSubmitting = d => {
		this.setState({ isSubmitting: d });
	};

	onChange(evt) {
		var newContent = evt.editor.getData();
		this.setState({
			content: newContent,
		});
	}

	async handleSubmit() {
		this.setSubmitting(true);
		try {
			if (this.state.content) {
				await this.props.postContact({ jsonData: this.state.content });
			}
			this.setSubmitting(false);
			showToast('Your data has been successfully');
		} catch (err) {
			console.log('err', err);
			this.setSubmitting(false);
			window.scrollTo(0, 0);
			showToast('Something went wrong !!');
		}
	}

	componentDidUpdate(prev) {
		if (this.props.conData !== prev.conData) {
			let MainData = this.props.conData && this.props.conData.map(m => JSON.parse(m.jsonData).jsonData);
			let data = MainData && MainData[0];
			this.setState({ content: data });
		}
	}

	render() {
		return (
			<Form loading={this.state.isSubmitting}>
				<Helmet>
					<title>
						Add Contacts
					</title>
				</Helmet>
				<Segment>
					<Segment inverted className="tableSectionHeader">
						Contacts
					</Segment>
					<CKEditor
						activeClass="p11"
						content={this.state.content}
						events={{
							blur: this.onBlur,
							afterPaste: this.afterPaste,
							change: this.onChange,
						}}
					/>
					<br />
					<Button color="violet" content="Submit" onClick={this.handleSubmit} />
				</Segment>
			</Form>
		);
	}
}

const mapDispatchToProps = {
	getContact,
	postContact,
};

const mapStateToProps = state => ({
	conData: state.root.admin.conData,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(FAQ);
