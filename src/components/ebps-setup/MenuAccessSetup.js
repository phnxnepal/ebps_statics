import React, { useState, useEffect, useCallback } from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Label, Select, Grid, Dropdown } from 'semantic-ui-react';
import { menuSetupData, menuAccessSetupData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast, getUserTypeValueNepali } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { EditButton } from './forms/Buttons';
import { validateString } from '../../utils/validationUtils';
import { SelectInput } from '../shared/Select';
import { hasAccessOption } from '../../utils/optionUtils';
import { FlexSingleRight } from '../uiComponents/FlexDivs';
import Helmet from 'react-helmet';
import { getOptionText } from '../../utils/dataUtils';
import { menuTypeOptions } from '../../utils/optionUtils';
import { getAdminFilteredOptions } from '../../utils/userTypeUtils';

const tableHeadings = menuAccessSetupData.table.tableHeadings;
const modalData = menuAccessSetupData.modal;

const initialState = {
	menu: '',
	userId: 'C',
	status: '',
};

const schema = Yup.object().shape({
	menu: validateString,
	userType: validateString,
	status: validateString,
});

const MenuAccessSetupComponent = ({
	adminData,
	success,
	getAfterUpdateAdminData,
	postAdminDataByUrl,
	// deleteAdminDataByUrl,
	// putAdminDataByUrl,
	errors,
	loading,
}) => {
	const {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		// openDeleteModal,
		// handleDelete,
		handleAdd,
		handleEdit,
		setOpenAddModal,
		// setOpenDeleteModal,
	} = useAdminSetup(initialState, success);

	const [menuMasterOptions, setMenuMasterOptions] = useState([]);
	const [userTypeOptions, setUserTypeOptions] = useState([]);
	const [menuAccess, setMenuAccess] = useState([]);
	const [userType, setUserType] = useState('C');
	const [menuType, setMenuType] = useState('');

	const getUpdateData = useCallback(getAfterUpdateAdminData, []);

	useEffect(() => {
		if (success && success.success) {
			getUpdateData([{ api: `${api.menuAccess}${userType}`, objName: 'menuAccess' }]);
		}
	}, [success, userType, getUpdateData]);

	useEffect(() => {
		const menuAcc = adminData.menuAccess.filter((menu) => menu.menuType === menuType);
		setMenuAccess(menuAcc);
	}, [adminData.menuAccess, menuType]);

	useEffect(() => {
		if (adminData.menuMaster && Array.isArray(adminData.menuMaster)) {
			const options = adminData.menuMaster.map((row) => ({ key: row.id, value: row.id, text: row.menu }));
			setMenuMasterOptions(options);
		}
	}, [adminData.menuMaster]);

	useEffect(() => {
		if (adminData.userTypeMaster && Array.isArray(adminData.userTypeMaster)) {
			// const userOptions = getAdminFilteredOptions(adminData.userTypeMaster);
			const userOptions = adminData.userTypeMaster.map((user, index) => ({
				key: index,
				value: user.id,
				text: user.designationNepali || user.designation,
			}));
			setUserTypeOptions(userOptions);
		}
	}, [adminData.userTypeMaster]);

	useEffect(() => {
		if (adminData.menuAccess) {
			setMenuAccess(adminData.menuAccess);
		}
	}, [adminData.menuAccess]);

	const userTypeChangeHandler = (userType) => {
		setUserType(userType);
		getAfterUpdateAdminData([{ api: `${api.menuAccess}${userType}`, objName: 'menuAccess' }]);
	};

	return (
		<div className="english-div">
			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				validationSchema={schema}
				onSubmit={async (values, actions) => {
					let requiredValues = [];

					if (!isDeleting && !isEditing && values.hasOwnProperty('menu')) {
						const menus = values['menu'];
						menus.forEach((mn) => {
							if (values.hasOwnProperty('userType')) {
								const userTypes = values['userType'];
								userTypes.forEach((userType) => requiredValues.push({ menu: mn, userType, status: values.status }));
							}
						});
					}

					actions.setSubmitting(true);
					try {
						if (isEditing) {
							postAdminDataByUrl(api.menuAccess, [values]);
						} else if (isDeleting) {
							// deleteAdminDataByUrl(api.menuAccess, `${values.menu}-${values.userType}`);
						} else {
							postAdminDataByUrl(api.menuAccess, requiredValues);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in menu master post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, values, isSubmitting, errors: formErrors, setFieldValue }) => (
					<>
						<Helmet title="Menu Access Setup" />
						<AdminModalContainer
							open={openAddModal}
							scroll={false}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenAddModal(false)}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
						>
							<Form loading={isSubmitting || loading}>
								<Grid>
									<Grid.Row columns="1">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.menu} name="menu" options={menuMasterOptions} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.menu}>
														{<span>{tableHeadings.menu}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.menu}
															name="menu"
															onChange={(e, { value }) => setFieldValue('menu', value)}
															options={menuMasterOptions}
															value={values.menu}
														/>
														{formErrors.menu && (
															<Label pointing prompt size="large">
																{formErrors.menu}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.userType} name="userType" options={userTypeOptions} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.userType}>
														{<span>{tableHeadings.userType}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.userType}
															name="userType"
															onChange={(e, { value }) => setFieldValue('userType', value)}
															options={userTypeOptions}
															value={values.userType}
														/>
														{formErrors.userType && (
															<Label pointing prompt size="large">
																{formErrors.userType}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
										<Grid.Column>
											<SelectInput label={tableHeadings.status} name="status" options={hasAccessOption} />
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</Form>
						</AdminModalContainer>
						{/* <AdminModalContainer
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenDeleteModal(false)}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer> */}
					</>
				)}
			</Formik>
			<Segment className="tableSectionHeader">
				{menuSetupData.table.tableHeader}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon.Group>
							<Icon name="file outline" />
							<Icon inverted corner name="add" />
						</Icon.Group>
						{`  ${modalData.titleAdd}`}
					</ButtonContent>
				</Button>
			</Segment>
			<FlexSingleRight>
				Select User Type:{' '}
				<Select
					name="userType"
					options={userTypeOptions}
					onChange={(e, { value }) => {
						userTypeChangeHandler(value);
					}}
					value={userType}
				/>{' '}
				Select Menu Type:{' '}
				<Select
					name="menuType"
					options={menuTypeOptions}
					onChange={(e, { value }) => {
						setMenuType(value);
					}}
					value={menuType}
				/>
			</FlexSingleRight>
			<Table compact="very" size="small" sortable striped celled>
				<Table.Header>
					<Table.Row>
						{/* <Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell> */}
						<Table.HeaderCell>{tableHeadings.menu}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.userType}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.menuType}</Table.HeaderCell>
						<Table.HeaderCell width="2">{tableHeadings.status}</Table.HeaderCell>
						<Table.HeaderCell width="3">{tableHeadings.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{menuAccess.length > 0 ? (
						menuAccess.map((row) => (
							<Table.Row key={row.menu}>
								{/* <Table.Cell>{row.id}</Table.Cell> */}
								<Table.Cell>{row.menuName}</Table.Cell>
								<Table.Cell>{getUserTypeValueNepali(row.userType)}</Table.Cell>
								<Table.Cell>{getOptionText(row.menuType, menuTypeOptions)}</Table.Cell>
								<Table.Cell textAlign="center">
									<Icon name={row.status === 'Y' ? 'check' : 'close'} color={row.status === 'Y' ? 'green' : 'red'} />
								</Table.Cell>
								<Table.Cell>
									<EditButton row={{ ...row, id: parseInt(row.id) }} handleEdit={handleEdit} />
									{/* <DeleteButton handleDelete={handleDelete} row={row} /> */}
								</Table.Cell>
							</Table.Row>
						))
					) : (
						<Table.Row>
							<Table.Cell colSpan="7">
								<Segment textAlign="center">No Data Found.</Segment>
							</Table.Cell>
						</Table.Row>
					)}
				</Table.Body>
			</Table>
		</div>
	);
};

export const MenuAccessSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: `${api.menuAccess}C`, objName: 'menuAccess' },
			{ api: api.menuMaster, objName: 'menuMaster' },
			{ api: api.userType, objName: 'userTypeMaster' },
		]}
		updateApi={[]}
		render={(props) => <MenuAccessSetupComponent {...props} parentProps={parentProps} />}
	/>
);
