import React, { Component } from 'react';
import CKEditor from 'react-ckeditor-component';
import { Segment, Form, Button } from 'semantic-ui-react';
import { showToast } from './../../utils/functionUtils';

//redux
import { connect } from 'react-redux';
import { getFAQ, postFAQ } from './../../store/actions/AdminAction';
import Helmet from 'react-helmet';

class FAQ extends Component {
	constructor(props) {
		super(props);
		this.state = {
			content: ' ',
			isSubmitting: false,
		};
		this.onChange = this.onChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	componentDidMount() {
		this.props.getFAQ();
	}

	setSubmitting = d => {
		this.setState({ isSubmitting: d });
	};

	onChange(evt) {
		var newContent = evt.editor.getData();
		this.setState({
			content: newContent,
		});
	}

	async handleSubmit() {
		this.setSubmitting(true);
		try {
			if (this.state.content) {
				await this.props.postFAQ({ jsonData: this.state.content });
			}
			this.setSubmitting(false);
			showToast('Your data has been successfully');
		} catch (err) {
			this.setSubmitting(false);
			window.scrollTo(0, 0);
			showToast('Something went wrong !!');
		}
	}

	componentDidUpdate(prev) {
		if (this.props.faqdata !== prev.faqdata) {
			let MainData = this.props.faqdata && this.props.faqdata.map(m => JSON.parse(m.jsonData).jsonData);
			let data = MainData && MainData[0];
			this.setState({ content: data });
		}
	}

	render() {
		return (
			<Form loading={this.state.isSubmitting}>
				<Helmet>
					<title>
						Add FAQ
					</title>
				</Helmet>
				<Segment>
					<Segment inverted className="tableSectionHeader">
						Frequently Asked Questions
					</Segment>
					<CKEditor
						activeClass="p10"
						content={this.state.content}
						// content="dfdf"
						events={{
							blur: this.onBlur,
							afterPaste: this.afterPaste,
							change: this.onChange,
						}}
					/>
					<br />
					<Button color="violet" content="Submit" onClick={this.handleSubmit} />
				</Segment>
			</Form>
		);
	}
}

const mapDispatchToProps = {
	getFAQ,
	postFAQ,
};
//@ts-ignore
const mapStateToProps = state => ({
	faqdata: state.root.admin.faqData,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(FAQ);
