import React from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Form, Label, Grid } from 'semantic-ui-react';
import { userTypeSetupData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik, Field } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { EditButton } from './forms/Buttons';
import { validateString } from '../../utils/validationUtils';
import Helmet from 'react-helmet';
import { EbpsFormIm } from './../shared/EbpsForm';

const tableHeadings = userTypeSetupData.table.tableHeadings;
const modalData = userTypeSetupData.modal;
const initialState = {
    id: '',
    designation: '',
    designationNepali: '',
    hierarchy: '',
    approveColumn: '',
};

const schema = Yup.object().shape({
    designationNepali: validateString,
});

const UserTypeSetupComponent = ({
    adminData,
    success,
    putAdminDataByUrl,
    // deleteAdminDataByUrl,
    // putAdminDataByUrl,
    errors,
    loading,
}) => {
    const {
        isEditing,
        isDeleting,
        formState,
        openAddModal,
        // openDeleteModal,
        // handleDelete,
        // handleAdd,
        handleEdit,
        setOpenAddModal,
        // setOpenDeleteModal,
    } = useAdminSetup(initialState, success);
    return (
        <div className="english-div">
            <Formik
                enableReinitialize
                initialValues={{ ...formState }}
                validationSchema={schema}
                onSubmit={async (values, actions) => {
                    // let requiredValues = [];
                    actions.setSubmitting(true);
                    try {
                        if (isEditing) {
                            putAdminDataByUrl(api.userType, '', { designation: values.designation, designationNepali: values.designationNepali, id: formState.id })
                        } else if (isDeleting) {
                            // deleteAdminDataByUrl(api.menuAccess, `${values.menu}-${values.userType}`);
                        } else {
                            // postAdminDataByUrl(api.menuAccess, requiredValues);
                        }
                        actions.setSubmitting(false);
                    } catch (error) {
                        showErrorToast('Something went wrong.');
                        actions.setSubmitting(false);
                    }
                }}
            >
                {({ handleSubmit, setFieldValue, values, isSubmitting, errors: formErrors }) => (
                    <>
                        <Helmet title="User Type Setup" />
                        <AdminModalContainer
                            open={openAddModal}
                            scroll={false}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={() => setOpenAddModal(false)}
                            title={isEditing ? modalData.titleEdit : modalData.titleAdd}
                            saveText={isEditing ? modalData.editText : modalData.saveText}
                        >
                            <Form loading={isSubmitting || loading}>
                                <Grid>
                                    <Grid.Row columns="3">
                                        <Grid.Column>
                                            <Form.Group >
                                                <Form.Field error={!!formErrors.id}>
                                                    {<span>{tableHeadings.id}</span>}
                                                    <Field name="id" readOnly={true} />
                                                    {formErrors.id && (
                                                        <Label pointing prompt size="large">
                                                            {formErrors.id}
                                                        </Label>
                                                    )}
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Form.Group >
                                                <Form.Field error={!!formErrors.hierarchy}>
                                                    {<span>{tableHeadings.hierarchy}</span>}
                                                    <Field name="hierarchy" readOnly={true} />
                                                    {formErrors.hierarchy && (
                                                        <Label pointing prompt size="large">
                                                            {formErrors.hierarchy}
                                                        </Label>
                                                    )}
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Form.Group >
                                                <Form.Field error={!!formErrors.approveColumn}>
                                                    {<span>{tableHeadings.approveColumn}</span>}
                                                    <Field name="approveColumn" readOnly={true} />
                                                    {formErrors.approveColumn && (
                                                        <Label pointing prompt size="large">
                                                            {formErrors.approveColumn}
                                                        </Label>
                                                    )}
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <Form.Group widths='equal' >
                                                <Form.Field error={!!formErrors.designation}>
                                                    {<span>{tableHeadings.designation}</span>}
                                                    <Field name="designation" autoFocus />
                                                    {formErrors.designation && (
                                                        <Label pointing prompt size="large">
                                                            {formErrors.designation}
                                                        </Label>
                                                    )}
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Form.Group widths='equal'>
                                                <Form.Field >
                                                    <EbpsFormIm
                                                        label={tableHeadings.designationNepali}
                                                        name="designationNepali"
                                                        error={formErrors.designationNepali}
                                                        value={values.designationNepali}
                                                        setFieldValue={setFieldValue}
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                        </Grid.Column>

                                    </Grid.Row>
                                </Grid>
                            </Form>
                        </AdminModalContainer>
                    </>
                )}
            </Formik>
            <Segment className="tableSectionHeader">
                {userTypeSetupData.table.tableHeader}
                {/* <Button
                    basic
                    style={{ right: 6, top: 8, position: 'absolute' }}
                    inverted
                    type="reset"
                    onClick={() => {
                        handleAdd();
                    }}
                >
                    <ButtonContent>
                        <Icon.Group>
                            <Icon name="file outline" />
                            <Icon inverted corner name="add" />
                        </Icon.Group>
                        {`  ${modalData.titleAdd}`}
                    </ButtonContent>
                </Button> */}
            </Segment>
            <Table compact="very" size="small" sortable striped celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
                        <Table.HeaderCell>{tableHeadings.designation}</Table.HeaderCell>
                        <Table.HeaderCell>{tableHeadings.designationNepali}</Table.HeaderCell>
                        <Table.HeaderCell>{tableHeadings.hierarchy}</Table.HeaderCell>
                        <Table.HeaderCell >{tableHeadings.approveColumn}</Table.HeaderCell>
                        <Table.HeaderCell >{tableHeadings.actions}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {adminData.userType.length > 0 ? (
                        adminData.userType.map(row => (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.id}</Table.Cell>
                                <Table.Cell>{row.designation}</Table.Cell>
                                <Table.Cell>{row.designationNepali}</Table.Cell>
                                <Table.Cell>{row.hierarchy}</Table.Cell>
                                <Table.Cell>{row.approveColumn} </Table.Cell>
                                <Table.Cell>
                                    <EditButton row={{ ...row, id: (row.id) }} handleEdit={handleEdit} />
                                </Table.Cell>
                            </Table.Row>
                        ))
                    ) : (
                            <Table.Row>
                                <Table.Cell colSpan="7">
                                    <Segment textAlign="center">No Data Found.</Segment>
                                </Table.Cell>
                            </Table.Row>
                        )}
                </Table.Body>
            </Table>
        </div>
    );
};

export const UserTypeSetup = parentProps => (
    <AdminContainer
        api={[{ api: api.userType, objName: 'userType' }]}
        updateApi={[{ api: api.userType, objName: 'userType' }]}
        render={props => <UserTypeSetupComponent {...props} parentProps={parentProps} />}
    />
);
