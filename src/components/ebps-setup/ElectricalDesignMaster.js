import React from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { elecDesignData } from '../../utils/data/ebps-setup/archiBMasterData';
import { Table, Segment, Form, Grid } from 'semantic-ui-react';
import { useSetup } from '../../hooks/useAdminSetup';
import { Formik } from 'formik';
import { showErrorToast } from '../../utils/functionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import { EbpsNormalFormIm } from '../shared/EbpsForm';
import { EditButton, DeleteButton } from './forms/Buttons';
import Helmet from 'react-helmet';


const initialState = {
    id: '',
    groupName: '',
    data: '',
    unit: '',
};

const tableData = elecDesignData.elecDesigntable.elecDesigntableHeadings;
const modalData = elecDesignData.elecDesignmodal;

export const ElectricalDesignMasterComponent = ({
    adminData,
    success,
    putAdminDataByUrl,
    postAdminDataByUrl,
    deleteAdminDataByUrl,
    errors,
    loading,
}) => {
    const { isEditing, isDeleting, formState, openAddModal, openDeleteModal, handleEdit, handleDelete, handleClose } = useSetup(initialState, success);

    return (
        <div className="setup-main">
            <Helmet title="Electrical Design Master" />
            <Segment inverted className="tableSectionHeader">
                {elecDesignData.elecDesigntable.elecDesignHead}
            </Segment>
            <Table striped celled compact="very" size="small">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{tableData.id}</Table.HeaderCell>
                        <Table.HeaderCell>{tableData.groupName}</Table.HeaderCell>
                        <Table.HeaderCell>{tableData.elements}</Table.HeaderCell>
                        <Table.HeaderCell>{tableData.unit}</Table.HeaderCell>
                        <Table.HeaderCell>{tableData.actions}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {adminData && adminData.elecDesignMaster && adminData.elecDesignMaster.length > 0 ? (
                        adminData.elecDesignMaster.map(row => (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.id}</Table.Cell>
                                <Table.Cell>{row.groupName}</Table.Cell>
                                <Table.Cell>{row.data}</Table.Cell>
                                <Table.Cell>{row.unit}</Table.Cell>
                                <Table.Cell>
                                    <EditButton handleEdit={handleEdit} row={row} />
                                    <DeleteButton handleDelete={handleDelete} row={row} />
                                </Table.Cell>
                            </Table.Row>
                        ))
                    ) : (
                            <Table.Row>
                                <Table.Cell colSpan="6">
                                    <Segment textAlign="center">No Data Found.</Segment>
                                </Table.Cell>
                            </Table.Row>
                        )}
                </Table.Body>
            </Table>

            <Formik
                enableReinitialize
                initialValues={{ ...formState }}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        if (isEditing) {
                            putAdminDataByUrl(api.elecDesignMaster, '', values);
                        } else if (isDeleting) {
                            deleteAdminDataByUrl(api.elecDesignMaster, formState.id);
                            // setOpenDeleteModal(false);
                        } else {
                            postAdminDataByUrl(api.elecDesignMaster, values);
                        }
                        actions.setSubmitting(false);
                    } catch (error) {
                        console.log('Error in Map Check Master post', error);
                        showErrorToast('Something went wrong.');
                        actions.setSubmitting(false);
                    }
                }}
            >
                {({ handleSubmit, isSubmitting }) => (
                    <>
                        <AdminModalContainer
                            key={'elec-design-master-edit'}
                            open={openAddModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={handleClose}
                            title={isEditing ? modalData.elecDesignEdit : modalData.elecDesignAdd}
                            saveText={isEditing ? modalData.elecDesignUpdate : modalData.elecDesignSave}
                        >
                            <Form loading={isSubmitting || loading}>
                                <Grid>
                                    {isEditing && (
                                        <Grid.Row>
                                            <Grid.Column>Id: {formState.id}</Grid.Column>
                                        </Grid.Row>
                                    )}
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsNormalFormIm label={tableData.groupName} name="groupName" />
                                        </Grid.Column>
                                        <Grid.Column>
                                            <EbpsNormalFormIm label={tableData.elements} name="data" />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsNormalFormIm label={tableData.unit} name="unit" />
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Form>
                        </AdminModalContainer>
                        <AdminModalContainer
                            key={'elec-design-master-delete'}
                            open={openDeleteModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={handleClose}
                            title={modalData.elecDesigndeleteModal.elecDesignDeletetitle}
                            saveText={modalData.elecDesignDelete}
                        >
                            <Form loading={isSubmitting || loading}>{modalData.elecDesigndeleteModal.elecDesignDeletecontent}</Form>
                        </AdminModalContainer>
                    </>
                )}
            </Formik>
        </div>
    );
};

export const ElectricalDesignMaster = parentProps => (
    <AdminContainer
        api={[{ api: api.elecDesignMaster, objName: 'elecDesignMaster' }]}
        updateApi={[{ api: api.elecDesignMaster, objName: 'elecDesignMaster' }]}
        render={props => <ElectricalDesignMasterComponent {...props} parentProps={parentProps} />}
    />
);
