import React, { Component } from 'react';
import { Formik } from 'formik';
import { designer } from '../../utils/data/ebps-setup/designerformdata';
import { Form, Button, Table, Select, Segment, Label, Divider, Grid, Icon, ButtonContent } from 'semantic-ui-react';
import { userdata } from '../../utils/data/ebps-setup/userformdata';
import { connect } from 'react-redux';
import FallbackComponent from '../shared/FallbackComponent';
import { showToast, showErrorToast } from '../../utils/functionUtils';
// import FormWrapper from '../shared/FormWrapper';
import { EbpsNormalForm } from '../shared/EbpsForm';
import { getDesigner, postDesigner, deleteDesigner, putDesigner, renewDesigner } from '../../store/actions/AdminAction';
import ErrorDisplay from '../shared/ErrorDisplay';
import { FileInputWithPreview, FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import { EbpsFormDateField, LangDateField } from '../shared/DateField';
import * as Yup from 'yup';
import EbpsForm from './../shared/EbpsForm';
import { validateFile, validateString, validateNullableNumber, validateNormalNepaliDateWithRange } from '../../utils/validationUtils';
import ImagePreview from '../shared/ImagePreview';
import { emailMessage, commonMessages } from '../../utils/data/validationData';
import { SelectInput } from '../shared/Select';
import { FileView, FileViewThumbnail } from '../shared/file/FileView';
import Helmet from 'react-helmet';

const getRenewDate = (id, renewalData) => {
	const requiredRows = renewalData.filter(row => row.designerId === parseInt(id));
	let tillDate = '';
	if (requiredRows) {
		const maxRow = requiredRows.reduce(function (acc, current) {
			if (acc.tilldate > current.tilldate) {
				return acc;
			} else return current;
		}, {});
		tillDate = maxRow.tilldate;
	}
	return tillDate;
};

const opt = [
	// { key: 'a', value: 'R', text: 'राजस्व' },
	// { key: 'b', value: 'B', text: 'सव इन्न्जिनियर' },
	// { key: 'c', value: 'A', text: 'इन्न्जिनियर' },
	// { key: 'd', value: 'C', text: 'प्रमुख प्रशसकीय अधिकृत' },
	{ key: 'e', value: 'D', text: 'डिजाइनर' },
	// { key: 'f', value: 'AD', text: 'अमिन' },
];
const opt2 = [
	{ key: 'a', value: 'Y', text: 'Yes' },
	{ key: 'b', value: 'N', text: 'No' },
];
const us = userdata.us_data;
const des = designer.designer_data;
const initialUser = { id: 0, token: '', userType: 'D', status: 'N' };
const initialState = {
	currentUser: initialUser,
	renewUser: {},
	isEditing: false,
	isModalOpen: false,
	idToDelete: '',
	isRenewing: false,
};

const numData = ['joinDate'];
// const fileData = ['userStamp', 'userSignature', 'userImage'];
const strData = ['userName', 'userType', 'address', 'loginId', 'educationQualification', 'status'];
const phoneData = ['mobile', 'municipalRegNo'];

const numSchema = numData.map(row => {
	return {
		[row]: validateString,
	};
});
// const normalNumSchema = normalNumData.map(row => {
//     return {
//         [row]: validateNormalNumber
//     };
// });
// const fileSchema = fileData.map(row => {
//     return {
//         [row]: validateFile
//     };
// });
const strSchema = strData.map(rows => {
	return {
		[rows]: validateString,
	};
});

const phoneDataSchema = phoneData.map(row => {
	return {
		[row]: validateNullableNumber,
	};
});
const renewSchema = Yup.object().shape({ tillDate: validateNormalNepaliDateWithRange, fiscalYear: validateString, charge: validateFile });
const UsersSchema = Yup.object().shape(
	Object.assign(
		{
			email: Yup.string()
				.email(emailMessage.incorrect)
				.required(commonMessages.required),
		},
		// ...numSchema,
		...strSchema,
		...phoneDataSchema
	)
);

class Designer extends Component {
	constructor(props) {
		super(props);
		this.state = { ...initialState, fiscalYearOptions: [], fiscalYear: '' };
	}
	modalOpen = id => {
		this.setState({ isModalOpen: true, idToDelete: id });
	};
	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};

	componentDidMount() {
		this.props.getDesigner();
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			// console.log('pp', prevProps.success, this.props.success);
			setTimeout(() => {
				this.props.getDesigner();
			}, 0);
		}
		if (prevProps.fiscalYear !== this.props.fiscalYear) {
			const validFiscalYears = this.props.fiscalYear.filter(fy => fy.status === 'Y');
			if (validFiscalYears) {
				this.setState({
					fiscalYearOptions: validFiscalYears.map(fy => {
						return { value: fy.yearName, text: fy.yearName };
					}),
					fiscalYear: validFiscalYears[0].yearName,
				});
			}
		}
	}

	handleEdit = User => {
		window.scrollTo(0, 0);
		this.setState({
			currentUser: {
				userName: User.userName,
				address: User.address,
				mobile: User.mobile,
				phone: User.phone,
				email: User.email,
				dbPassword: User.dbPassword,
				photo: User.photo,
				educationQualification: User.educationQualification,
				licenseNo: User.licenseNo,
				userStamp: User.stamp,
				loginId: User.loginId,
				joinDate: User.joinDate,
				userType: User.userType.id,
				userSignature: User.signature,
				municipalRegNo: User.municipalRegNo,
				status: User.status,
				id: User.id,
			},
			isEditing: true,
			isRenewing: false,
		});
	};

	handleRenew = obj => {
		window.scrollTo(0, 0);
		const { renewalData } = this.props;
		const renewData = renewalData.filter(row => row.designerId === parseInt(obj.id)) || [];
		const latestRenew =
			renewData && renewData.length > 0 && renewData.reduce((prev, current) => (prev.renewDate > current.renewDate ? prev : current));
		this.setState({
			renewUser: {
				...obj,
			},
			latestRenew,
			fiscalYear: this.state.fiscalYearOptions[0].value,
			renewData,
			isEditing: false,
			isRenewing: true,
		});
	};

	handleDelete = async id => {
		try {
			await this.props.deleteDesigner(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};

	render() {
		const { currentUser, isRenewing } = this.state;
		const { userData, renewalData } = this.props;

		const existingImages = {
			userImage: currentUser.photo,
			userStamp: currentUser.userStamp,
			userSignature: currentUser.userSignature,
		};

		return (
			<>
				<Helmet>
					<title>Add Designer</title>
				</Helmet>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message}></ErrorDisplay>}
				<Formik
					enableReinitialize
					initialValues={isRenewing ? { fiscalYear: this.state.fiscalYear } : currentUser}
					validationSchema={this.state.isRenewing ? renewSchema : UsersSchema}
					// validateOnBlur
					validateOnChange
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						values.id && delete values.id;
						values['imagePreviewUrl_userStamp'] && delete values['imagePreviewUrl_userStamp'];
						values['imagePreviewUrl_userSignature'] && delete values['imagePreviewUrl_userSignatrure'];
						values['imagePreviewUrl_userImage'] && delete values['imagePreviewUrl_userImage'];

						const data = new FormData();
						const selectedSignFile = values.userSignature;
						const selectedStampFile = values.userStamp;
						const selectedFile = values.userImage;
						// let isFileSelected = false;
						if (selectedSignFile) {
							for (var x = 0; x < selectedSignFile.length; x++) {
								data.append('userSignature', selectedSignFile[x]);
							}
							// isFileSelected = true;
						} else {
							data.append('userSignature', null);
						}
						if (selectedStampFile) {
							for (var x = 0; x < selectedStampFile.length; x++) {
								data.append('userStamp', selectedStampFile[x]);
							}
							// isFileSelected = true;
						} else {
							data.append('userStamp', null);
						}
						if (selectedFile) {
							for (var x = 0; x < selectedFile.length; x++) {
								data.append('userImage', selectedFile[x]);
							}
							// isFileSelected = true;
						} else {
							data.append('userImage', null);
						}

						Object.keys(values).forEach(key => {
							data.append(key, values[key]);
						});

						try {
							if (this.state.isEditing) {
								await this.props.putDesigner(this.state.currentUser.id, data);
							} else if (this.state.isRenewing) {
								const renewData = new FormData();
								const voucher = values.charge;

								if (voucher) {
									voucher.forEach(fl => renewData.append('file', fl));
								}

								renewData.append('tillDate', values.tillDate);
								renewData.append('fiscalYear', values.fiscalYear);
								// renewData.append('jsonData', JSON.stringify({ id: this.state.renewUser.id, tillDate: values.tillDate }));
								renewData.append('id', this.state.renewUser.id);

								await this.props.renewDesigner(renewData);
							} else {
								await this.props.postDesigner(data);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								if (this.state.isEditing) {
									showToast('Your data has been successfully saved.');
								} else if (this.state.isRenewing) {
									showToast('The designer has been renewed.');
								} else {
									showToast('Please check your email for password reset instructions.');
								}
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showErrorToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, handleChange, errors, values, setFieldValue, isSubmitting, handleReset }) => (
						<Form loading={isSubmitting}>
							<Segment attached>
								<Segment inverted className="fileStorageSegment">
									{' '}
									{des.header.header_1}
									<Button
										basic
										style={{ right: 6, top: 8, position: 'absolute' }}
										inverted
										type="reset"
										onClick={() => {
											this.setState({
												...initialState,
												currentUser: initialUser,
											});
											handleReset();
										}}
									>
										<ButtonContent>
											<Icon name="add user" />
										</ButtonContent>
									</Button>
								</Segment>
								{isRenewing ? (
									<RenewForm state={this.state} />
								) : (
										<fieldset className="borderlessFieldset" disabled={isRenewing}>
											<Form.Group widths="equal">
												<Form.Field>
													<EbpsForm
														label={<strong>{us.formdata.formdata1}</strong>}
														name="userName"
														onChange={handleChange}
														error={errors.userName}
														value={values.userName}
														setFieldValue={setFieldValue}
													/>
												</Form.Field>
												<Form.Field>
													<EbpsForm
														label={<strong>{us.formdata.formdata2}</strong>}
														name="address"
														onChange={handleChange}
														error={errors.address}
														value={values.address}
														setFieldValue={setFieldValue}
													/>
												</Form.Field>
												<Form.Field>
													<EbpsForm
														label={<strong>{us.formdata.formdata3}</strong>}
														name="mobile"
														// disabled={true}
														onChange={handleChange}
														value={values.mobile}
														error={errors.mobile}
														setFieldValue={setFieldValue}
													/>
												</Form.Field>
											</Form.Group>

											<Form.Group widths="equal">
												<Form.Field>
													<EbpsNormalForm
														label={<strong>{us.formdata.formdata5}</strong>}
														name="email"
														onChange={handleChange}
														error={errors.email}
														value={values.email}
													/>
												</Form.Field>
												{/* <Form.Field>
                                                <EbpsNormalForm label={<strong>{us.formdata.formdata6}</strong>}
                                                    name="dbPassword"
                                                    onChange={handleChange}
                                                    error={errors.dbPassword}
                                                    value={values.dbPassword} />
                                            </Form.Field> */}
												<Form.Field>
													<EbpsNormalForm
														label={us.formdata.formdata13}
														name="educationQualification"
														onChange={handleChange}
														error={errors.educationQualification}
														value={values.educationQualification}
													/>
													{/* <EbpsForm
														label={<strong>{us.formdata.formdata13}</strong>}
														name="educationQualification"
														onChange={handleChange}
														error={errors.educationQualification}
														value={values.educationQualification}
														setFieldValue={setFieldValue}
													/> */}
												</Form.Field>
												<Form.Field>
													<EbpsNormalForm
														label={<strong>{us.formdata.formdata14}</strong>}
														name="licenseNo"
														onChange={handleChange}
														error={errors.licenseNo}
														value={values.licenseNo}
													/>
												</Form.Field>
											</Form.Group>

											<Form.Group widths="equal">
												<Form.Field>
													<EbpsNormalForm
														label={<strong>{us.formdata.formdata19}</strong>}
														name="loginId"
														onChange={handleChange}
														error={errors.loginId}
														value={values.loginId}
													/>
												</Form.Field>
												<Form.Field>
													<EbpsForm
														label={<strong>{us.formdata.formdata16}</strong>}
														name="municipalRegNo"
														onChange={handleChange}
														error={errors.municipalRegNo}
														value={values.municipalRegNo}
														setFieldValue={setFieldValue}
													/>
												</Form.Field>

												<Form.Field error={!!errors.userType}>
													<strong>{us.formdata.formdata10}</strong>
													<br />
													<Select
														// error={!!error}
														value={values.userType}
														disabled={this.state.isRenewing}
														name="userType"
														options={opt}
														onChange={(e, { value }) => {
															setFieldValue('userType', value);
														}}
													// error={errors.userType}
													/>
													{errors.userType && (
														<Label pointing prompt size="large">
															{errors.userType}
														</Label>
													)}
												</Form.Field>

												<Form.Field error={!!errors.status}>
													<strong>{us.formdata.formdata18}</strong>
													<Select
														fluid
														name="status"
														value={values.status}
														disabled={this.state.isRenewing}
														options={opt2}
														onChange={(e, { value }) => {
															setFieldValue('status', value);
														}}
													></Select>
													{errors.status && (
														<Label pointing prompt size="large">
															{errors.status}
														</Label>
													)}
												</Form.Field>
											</Form.Group>
										</fieldset>
									)}
								{/* <Form.Group widths='equal'>
                                    <fieldset className="borderlessFieldset" disabled={this.state.isRenewing}>
                                            <Form.Field>

                                                <EbpsFormDateField

                                                    label={<strong>{us.formdata.formdata11}</strong>}
                                                    name='joinDate'
                                                    handleChange={handleChange}
                                                    value={values.joinDate}
                                                    error={errors.joinDate}
                                                />
                                            </Form.Field>
                                        </fieldset>
                                        <Form.Field>
                                            <EbpsNormalForm label={<strong>{des.data_5.data_5_3}</strong>}
                                                name="tillDate"
                                                disabled={this.state.isEditing}
                                                onChange={handleChange}
                                                error={errors.tillDate}
                                                value={values.tillDate} />
                                        </Form.Field>
                                    </Form.Group> */}
								<Form.Group>
									{this.state.isEditing && (
										<fieldset style={{ marginLeft: 7 }} className="borderlessFieldset" disabled={this.state.isEditing}>
											<Form.Field>
												<LangDateField
													label={<strong>{us.formdata.formdata11}</strong>}
													name="joinDate"
													// handleChange={handleChange}
													// error={errors.joinDate}
													value={values.joinDate}
												// setFieldValue={setFieldValue}
												/>
											</Form.Field>
										</fieldset>
									)}

									{this.state.isRenewing && this.state.latestRenew && (
										<>
											<Form.Field>
												<EbpsForm label={<strong>{des.renew.previousRenewDate}</strong>} value={this.state.latestRenew.renewDateBS} />
											</Form.Field>
											<Form.Field>
												<EbpsForm label={<strong>{des.renew.previousRenewTillDate}</strong>} value={this.state.latestRenew.tillDateBS} />
											</Form.Field>
										</>
									)}
								</Form.Group>
								{this.state.isRenewing && (
									<Form.Group>
										<Form.Field width={4}>
											<EbpsFormDateField
												label={<strong>{des.renew.renewTillDate}</strong>}
												name="tillDate"
												handleChange={handleChange}
												error={errors.tillDate}
												value={values.tillDate}
											/>
										</Form.Field>
										<Form.Field width={4}>
											<SelectInput label={<strong>{des.renew.fiscalYear}</strong>} name="fiscalYear" options={this.state.fiscalYearOptions} />
										</Form.Field>
										<Form.Field width={5}>
											<strong>{des.renew.charge}</strong>
											<FileInputWithPreview name="charge" hasMultipleFiles={false} error={errors.charge} fileCatId={'renewBill'} />
										</Form.Field>
									</Form.Group>
								)}
								{!isRenewing && (
									<fieldset className="borderlessFieldset" disabled={this.state.isRenewing}>
										<Form.Group widths="equal">
											<FileInput existingImage={existingImages.userImage} name="userImage" label={us.formdata.formdata12} />
											<FileInput existingImage={existingImages.userStamp} name="userStamp" label={us.formdata.formdata17} />
											<FileInput existingImage={existingImages.userSignature} name="userSignature" label={us.formdata.formdata15} />
										</Form.Group>
									</fieldset>
								)}
								<Button type="button" className="fileStorageSegment" onClick={handleSubmit}>
									{this.state.isEditing ? 'Update' : isRenewing ? 'Renew Designer' : 'Save'}
								</Button>
							</Segment>
							<Divider />
							{userData && renewalData ? (
								<div>
									<Segment inverted className="fileStorageSegment">
										{' '}
										{des.second_header.second_header_1}
									</Segment>
									{/* <div> */}
									<div style={{ overflowX: 'scroll' }}>
										<Table striped celled compact="very" size="small" style={{ textAlign: 'center' }}>
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={2}>
														<span>{us.formdata.name}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.address}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.phone}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.email}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.issueDate}</span>
													</Table.HeaderCell>
													{/* <Table.HeaderCell width={1}>
														<span>{us.formdata.eduQualification}</span>
													</Table.HeaderCell> */}
													<Table.HeaderCell width={1}>
														<span>{us.formdata.formdata14}</span>
													</Table.HeaderCell>
													{/* <Table.HeaderCell width={1}>
														<span>{us.formdata.formdata16}</span>
													</Table.HeaderCell> */}
													<Table.HeaderCell width={1}>
														<span>{us.formdata.formdata18}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.formdata19}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={1}>
														<span>{us.formdata.expiryDate}</span>
													</Table.HeaderCell>
													<Table.HeaderCell width={3}> Action</Table.HeaderCell>
												</Table.Row>
											</Table.Header>

											<Table.Body>
												{this.props.userData
													.filter(user => user.userType && user.userType.id === 'D')
													.map(designer => {
														return (
															<Table.Row key={designer.id}>
																<Table.Cell>{designer.userName}</Table.Cell>
																<Table.Cell>{designer.address}</Table.Cell>
																{/* <Table.Cell >{designer.phone}</Table.Cell> */}
																<Table.Cell>{designer.mobile}</Table.Cell>
																<Table.Cell>{designer.email}</Table.Cell>
																{/* <Table.Cell>{designer.userType.id}</Table.Cell> */}
																<Table.Cell>{designer.joinDate}</Table.Cell>
																{/* <Table.Cell >{designer.photo}</Table.Cell> */}
																{/* <Table.Cell>{designer.educationQualification}</Table.Cell> */}
																<Table.Cell>{designer.licenseNo}</Table.Cell>
																{/* <Table.Cell >{designer.signature}</Table.Cell> */}
																{/* <Table.Cell>{designer.municipalRegNo}</Table.Cell> */}
																{/* <Table.Cell >{designer.stamp}</Table.Cell> */}
																<Table.Cell>{designer.status}</Table.Cell>
																<Table.Cell>{designer.loginId}</Table.Cell>
																{/* <Table.Cell >{designer.napadartano}</Table.Cell> */}
																<Table.Cell>{getRenewDate(designer.id, renewalData)}</Table.Cell>
																<Table.Cell>
																	<Button
																		// circular
																		inverted
																		title="Edit"
																		type="reset"
																		color="green"
																		onClick={() => {
																			handleReset();
																			this.setState({ currentUser: initialUser, renewUser: initialUser });
																			this.handleEdit(designer);
																		}}
																		icon="edit"
																		size="tiny"
																		style={{ marginBottom: '2px' }}
																	></Button>
																	{/* <Button circular title="Delete" color='red' icon='remove circle' onClick={() => this.modalOpen(designer.id)}></Button> */}
																	<Button
																		// circular
																		inverted
																		title="Renew"
																		type="reset"
																		color="yellow"
																		icon="calendar plus outline"
																		onClick={() => {
																			handleReset();
																			this.setState({ currentUser: initialUser, renewUser: initialUser });
																			this.handleRenew(designer);
																		}}
																		size="tiny"
																	></Button>
																	{/* <Modal close={this.modalClose} open={this.state.isModalOpen}>
                                                                            <Modal.Content>
                                                                                <p>Do you want to delete this section?</p>
                                                                            </Modal.Content>
                                                                            <Modal.Actions>
                                                                                <Button color='green' title="Confirm Delete!" onClick={() => this.handleDelete(this.state.idToDelete)} positive
                                                                                    labelPosition='left'
                                                                                    icon='checkmark'
                                                                                    content='Yes'>

                                                                                </Button>
                                                                                <Button color='red' title="Decline Delete!" onClick={this.modalClose} negative
                                                                                    labelPosition='left'
                                                                                    icon='remove'
                                                                                    content='No'>

                                                                                </Button>
                                                                            </Modal.Actions>
                                                                        </Modal> */}
																</Table.Cell>
															</Table.Row>
														);
													})}
											</Table.Body>
										</Table>
									</div>
								</div>
							) : (
									<div style={{ alignContent: 'center' }}>
										<FallbackComponent errors={this.props.errors} loading={true} />
									</div>
								)}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const mapDispatchToProps = {
	getDesigner,
	postDesigner,
	putDesigner,
	deleteDesigner,
	renewDesigner,
};

const mapStateToProps = state => ({
	userData: state.root.admin.userData,
	fiscalYear: state.root.admin.fiscalYear,
	renewalData: state.root.admin.renewalData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});

const FileInput = ({ existingImage, label, name }) => (
	<Form.Field>
		<label>
			<strong>{label}</strong>
		</label>
		<FileInputWithMultiplePreview name={name} fileCatId={name} hasMultipleFiles={false} />
		{existingImage && <ImagePreview fileUrl={existingImage} />}
	</Form.Field>
);

const RenewForm = ({ state }) => {
	const { renewUser: currentUser, renewData } = state;
	return (
		<Form>
			<Grid padded>
				<Grid.Row>
					<Grid.Column width={6}>
						<h3>Designer Details</h3>
						<p>
							{us.formdata.loginId} : {currentUser.userName}
						</p>
						<p>
							{us.formdata.address}: {currentUser.address}
						</p>
						<p>Consultancy Name: {currentUser.consultancyName}</p>
						<p>
							{us.formdata.email}: {currentUser.email}
						</p>
						<p>
							{us.formdata.issueDate}: {currentUser.joinDate}
						</p>
						<p>
							{us.formdata.photo}: <ImagePreview fileUrl={currentUser.photo} />
						</p>
					</Grid.Column>
					<Grid.Column width={10}>
						<h3>Renewal History</h3>
						<Table celled compact="very" striped>
							<Table.Header>
								<Table.HeaderCell>Renewed By</Table.HeaderCell>
								<Table.HeaderCell>Renewed On</Table.HeaderCell>
								<Table.HeaderCell>Till Date</Table.HeaderCell>
								<Table.HeaderCell>Fiscal Year</Table.HeaderCell>
								<Table.HeaderCell>Renew Voucher</Table.HeaderCell>
							</Table.Header>
							<Table.Body>
								{renewData.length <= 0 ? (
									<Table.Row>This designer has not been renewed yet.</Table.Row>
								) : (
										renewData.map(
											row =>
												row && (
													<Table.Row>
														<Table.Cell>{row.renewBy}</Table.Cell>
														<Table.Cell>{row.renewDateBS}</Table.Cell>
														<Table.Cell>{row.tillDateBS}</Table.Cell>
														<Table.Cell>{row.fiscalYear}</Table.Cell>
														<Table.Cell>
															<FileViewThumbnail el={{ fileUrl: row.fileUrl }} deletable={false} />
														</Table.Cell>
													</Table.Row>
												)
										)
									)}
							</Table.Body>
						</Table>
					</Grid.Column>
				</Grid.Row>
			</Grid>
		</Form>
	);
};

export default connect(mapStateToProps, mapDispatchToProps)(Designer);
