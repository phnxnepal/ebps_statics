import React from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { MapCheckData } from '../../utils/data/ebps-setup/archiBMasterData';
import { Table, Segment, Form, Grid } from 'semantic-ui-react';
import { useSetup } from '../../hooks/useAdminSetup';
import { Formik } from 'formik';
import { showErrorToast } from '../../utils/functionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import { EbpsNormalFormIm } from '../shared/EbpsForm';
import { EditButton, DeleteButton } from './forms/Buttons';
import { buildingStructureOption } from '../../utils/optionUtils';
import { SelectInput } from '../shared/Select';
import { getOptionText } from '../../utils/dataUtils';
import Helmet from 'react-helmet';

const initialState = {
    id: '',
    sn: '',
    buildingDescription: '',
    type: '',
    // unit: '',
};

const tableMapCheck = MapCheckData.MapCheckTable.tableheader;
const modalMapCheck = MapCheckData.MapCheckModal;

export const MapCheckReportMasterComponent = ({
    adminData,
    success,
    putAdminDataByUrl,
    postAdminDataByUrl,
    deleteAdminDataByUrl,
    errors,
    loading,
}) => {
    const { isEditing, isDeleting, formState, openAddModal, openDeleteModal, handleEdit, handleDelete, handleClose } = useSetup(initialState, success);

    return (
        <div className="setup-main">
            <Helmet title="Map Check Report Master" />
            <Segment inverted className="tableSectionHeader">
                {MapCheckData.MapCheckTable.header}
            </Segment>
            <Table striped celled compact="very" size="small">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{tableMapCheck.id}</Table.HeaderCell>
                        <Table.HeaderCell>{tableMapCheck.sn}</Table.HeaderCell>
                        <Table.HeaderCell>{tableMapCheck.buildingDes}</Table.HeaderCell>
                        <Table.HeaderCell>{tableMapCheck.strucType}</Table.HeaderCell>
                        {/* <Table.HeaderCell>{tableHeadings.unit}</Table.HeaderCell> */}
                        <Table.HeaderCell>{tableMapCheck.actions}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {adminData && adminData.mapCheckMaster && adminData.mapCheckMaster.length > 0 ? (
                        adminData.mapCheckMaster.map(row => (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.id}</Table.Cell>
                                <Table.Cell>{row.sn}</Table.Cell>
                                <Table.Cell>{row.buildingDescription}</Table.Cell>
                                <Table.Cell>{getOptionText(row.type, buildingStructureOption)}</Table.Cell>
                                {/* <Table.Cell>{row.unit}</Table.Cell> */}
                                <Table.Cell>
                                    <EditButton handleEdit={handleEdit} row={row} />
                                    <DeleteButton handleDelete={handleDelete} row={row} />
                                </Table.Cell>
                            </Table.Row>
                        ))
                    ) : (
                            <Table.Row>
                                <Table.Cell colSpan="6">
                                    <Segment textAlign="center">No Data Found.</Segment>
                                </Table.Cell>
                            </Table.Row>
                        )}
                </Table.Body>
            </Table>

            <Formik
                enableReinitialize
                initialValues={{ ...formState }}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        if (isEditing) {
                            putAdminDataByUrl(api.mapCheckMaster, formState.id, values);
                        } else if (isDeleting) {
                            deleteAdminDataByUrl(api.mapCheckMaster, formState.id);
                            // setOpenDeleteModal(false);
                        } else {
                            postAdminDataByUrl(api.mapCheckMaster, values);
                        }
                        actions.setSubmitting(false);
                    } catch (error) {
                        console.log('Error in Map Check Master post', error);
                        showErrorToast('Something went wrong.');
                        actions.setSubmitting(false);
                    }
                }}
            >
                {({ handleSubmit, isSubmitting }) => (
                    <>
                        <AdminModalContainer
                            key={'map-check-master-edit'}
                            open={openAddModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={handleClose}
                            title={isEditing ? modalMapCheck.editMapCheck : modalMapCheck.addMapCheck}
                            saveText={isEditing ? modalMapCheck.updateMapCheck : modalMapCheck.saveMapCheck}
                        >
                            <Form loading={isSubmitting || loading}>
                                <Grid>
                                    {isEditing && (
                                        <Grid.Row>
                                            <Grid.Column>Id: {formState.id}</Grid.Column>
                                        </Grid.Row>
                                    )}
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsNormalFormIm label={tableMapCheck.buildingDes} name="buildingDescription" />
                                        </Grid.Column>
                                        <Grid.Column>
                                            <SelectInput label={tableMapCheck.strucType} name="type" options={buildingStructureOption} />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsNormalFormIm label={tableMapCheck.sn} name="sn" />
                                        </Grid.Column>
                                        {/* <Grid.Column>
                                            <EbpsNormalFormIm label={tableHeadings.unit} name="unit" />
                                        </Grid.Column> */}
                                    </Grid.Row>
                                </Grid>
                            </Form>
                        </AdminModalContainer>
                        <AdminModalContainer
                            key={'map-check-master-delete'}
                            open={openDeleteModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={handleClose}
                            title={modalMapCheck.deleteMapCheckModal.deleteMapCheckModaltitle}
                            saveText={modalMapCheck.deleteMapCheck}
                        >
                            <Form loading={isSubmitting || loading}>{modalMapCheck.deleteMapCheckModal.deleteMapCheckModalcontent}</Form>
                        </AdminModalContainer>
                    </>
                )}
            </Formik>
        </div>
    );
};

export const MapCheckReportMaster = parentProps => (
    <AdminContainer
        api={[{ api: api.mapCheckMaster, objName: 'mapCheckMaster' }]}
        updateApi={[{ api: api.mapCheckMaster, objName: 'mapCheckMaster' }]}
        render={props => <MapCheckReportMasterComponent {...props} parentProps={parentProps} />}
    />
);
