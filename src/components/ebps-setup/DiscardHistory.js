import React, { useState, useEffect } from 'react';
import Viewer from 'react-viewer';
import { getDocUrl } from '../../utils/config';
import GenericSetupTable from './forms/GenericSetupTable';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { discardHistory, historyDatafield } from '../../utils/data/ebps-setup/discardSetupData';

export const DiscardHistory = ({ historyList }) => {
    const [fileView, setFileView] = useState('');
    const [fileViewer, setFileViewer] = useState(false);
    const [historyChanged, setHistoryChanged] = useState(false);
    const [year, setYear] = useState('');
    useJqxNepaliFilter(historyChanged, [2, 4], []);

    useEffect(() => {
        const fileViewButton = document.querySelectorAll('.view-file');
        if (fileViewButton.length > 0) {
            Array.from(fileViewButton).forEach(button => {
                const row = button.dataset.row;
                const application = historyList[row];
                button.addEventListener('click', () => handleFileViewer(application));
            });
        }
        return () => {
            const fileViewButton = document.querySelectorAll('.view-file');
            if (fileViewButton.length > 0) {
                Array.from(fileViewButton).forEach(button => {
                    const row = button.dataset.row;
                    const application = historyList[row];
                    button.removeEventListener('click', () => handleFileViewer(application));
                });
            }
        };
    }, [historyList, historyChanged]);

    const generateViewButtonRenderer = row => {
        setHistoryChanged(!historyChanged);
        return (
            '<button title="View File" class="ui primary-table-single-btn tiny icon button view-file" data-row=' +
            row +
            '><i aria-hidden="true" class="eye icon"></i></button>'
        );
    };

    const handleFileViewer = row => {
        setFileView(row.talathapDocument);
        setFileViewer(true);
    };

    return (
        <div>

            <Viewer
                visible={fileViewer}
                onClose={() => setFileViewer(false)}
                images={[{ src: `${getDocUrl()}${fileView}` }]}
                activeIndex={0}
                zIndex={10000}
            />
            <GenericSetupTable
                data={historyList}
                columns={discardHistory}
                datafield={historyDatafield}
                generateButtonRenderer={generateViewButtonRenderer}
                actionWidth={100}
            />
        </div>
    );
};
