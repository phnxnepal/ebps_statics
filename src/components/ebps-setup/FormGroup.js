import React, { Component } from 'react';
import { Form, Button, Table, Input, Modal, Segment } from 'semantic-ui-react';
// import FormWrapper from "../shared/FormWrapper";
import { Formik } from 'formik';
import { connect } from 'react-redux';
import {
	getFormGroupData,
	getFormNameMasterData,
	getFormGroupMasterData,
	postFormDatas,
	putFormDatas,
	deleteFormDatas,
} from '../../store/actions/AdminAction';
import { showToast } from '../../utils/functionUtils';
// import api from "../../utils/api"
import FallbackComponent from '../shared/FallbackComponent';
import EbpsSelect from '../shared/Select';
import ErrorDisplay from '../shared/ErrorDisplay';
import Helmet from 'react-helmet';
import { getAdminFilteredOptions } from '../../utils/userTypeUtils';
import { getOptionText } from '../../utils/dataUtils';
import { SearchableDropdown } from '../shared/formComponents/SearchableDropdown';
import { setupTableProps } from '../../utils/tableUtils';
import { isKamalamai } from '../../utils/clientUtils';

// const opt2 = [
// 	{ key: 'a', value: 'R', text: 'राजस्व' },
// 	{ key: 'b', value: 'B', text: 'सव इन्न्जिनियर' },
// 	{ key: 'c', value: 'A', text: 'इन्न्जिनियर' },
// 	{ key: 'd', value: 'C', text: 'प्रमुख प्रशसकीय अधिकृत' },
// 	{ key: 'e', value: 'D', text: 'डिजाइनर' },
// 	{ key: 'f', value: 'AD', text: 'अमिन' },
// ];
const groupTypeOpt = [
	{ key: 'a', value: '1', text: 'नयाँ' },
	{ key: 'b', value: '2', text: 'पुरानो' },
	{ key: 'c', value: '3', text: 'तला थप' },
	{ key: 'N', value: 'N', text: 'नामसारी' },
];

const groupTypeKamalamaiOpt = [
	{ key: 'a', value: '1', text: 'नयाँ' },
	{ key: 'b', value: '2', text: 'पुरानो' },
	{ key: 'c', value: '3', text: 'तला थप' },
	{ key: 'N', value: 'N', text: 'नामसारी' },
	{ key: '1E', value: '1E', text: 'नयाँ घर भूकम्प प्रतिरोधक' },
	{ key: '2E', value: '2E', text: 'पुरानो घर भूकम्प प्रतिरोधक' },
	{ key: '3E', value: '3E', text: 'तला थप भूकम्प प्रतिरोधक' },

];

class FormGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			formData: {
				formId: 1,
				groupId: 1,
				groupPosition: '1',
				id: '',
				groupType: '1',
				userType: 'R',
				previousForm: 1,
			},
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
			formList: [],
			enterByOptions: [],
			groupOptions: [],
			formNameOptions: [],
			previousFormOptions: [],
		};
	}

	modalOpen = (id, groupId, userType) => {
		this.setState((prevState) => ({ isModalOpen: true, idToDelete: id, formData: { ...prevState.formData, groupId, userType } }));
	};

	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	componentDidMount() {
		this.props.getFormGroupData('1-R');
		this.props.getFormNameMasterData();
		this.props.getFormGroupMasterData();
	}
	componentDidUpdate(prevProps) {
		if (prevProps.userTypeMaster !== this.props.userTypeMaster && this.props.userTypeMaster) {
			const userTypeMaster = this.props.userTypeMaster;
			const enterByOptions = getAdminFilteredOptions(userTypeMaster);
			this.setState({
				enterByOptions,
			});
		}

		if (this.props.formNames !== prevProps.formNames && this.props.formNames) {
			const formNameOptions = this.props.formNames.map((row) => ({ value: row.id, text: row.name }));
			this.setState({
				formNameOptions: formNameOptions,
				previousFormOptions: [{ value: '', text: 'No Previous Form' }].concat(formNameOptions),
			});
		}

		if (this.props.group !== prevProps.group && this.props.group) {
			this.setState({ groupOptions: this.props.group.map((row) => ({ value: row.id, text: row.name })) });
		}

		// if (prevProps.success !== this.props.success) {
		// 	// console.log('updateing');
		// 	// setTimeout(() => {
		// 	//  this.props.getFormGroupData(
		// 	//      `${this.state.formData.groupType}-${this.state.formData.userType}`
		// 	//  );
		// 	// this.props.getFormNameMasterData()
		// 	// this.props.getFormGroupMasterData()
		// 	//}, 0);
		// 	this.props.getFormGroupData(`${this.state.formData.groupId}-${this.state.formData.userType}`);
		// 	this.setState({ isEditing: false });
		// }
	}
	handleEdit = (User) => {
		// console.log("click", User)
		window.scrollTo(0, 0);
		this.setState({
			formData: {
				userType: User.userType.id,
				formId: User.formId.id,
				groupId: User.groupId.id,
				groupType: User.groupType,
				groupPosition: User.groupPosition,
				previousForm: User.previousForm,
				id: User.id,
			},

			isEditing: true,
		});
	};

	handleDelete = async (id) => {
		try {
			await this.props.deleteFormDatas(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
				this.props.getFormGroupData(`${this.state.formData.groupType}-${this.state.formData.userType}`);
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};

	handleUserTypeSelect = (userType) => {
		this.props.getFormGroupData(userType);
	};

	state = {};
	render() {
		// console.log('this.prop', this.props);
		// console.log('formData', this.state.formData)
		const { enterByOptions, groupOptions, formNameOptions, previousFormOptions } = this.state;

		if (this.props.group && this.props.formNames && this.props.formList) {
			return (
				// <div className="ui container">
				<Formik
					enableReinitialize
					initialValues={this.state.formData}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						try {
							if (this.state.isEditing) {
								// console.log('formData', this.state.formData)
								await this.props.putFormDatas(`${this.state.formData.id}`, values);
							} else {
								await this.props.postFormDatas(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully added');
								this.props.getFormGroupData(`${values.groupType}-${values.userType}`);
								this.setState({ isEditing: false });
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, setFieldValue, values, isSubmitting }) => (
						// <FormWrapper>
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Helmet title="Form Group" />
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									Form Group
								</Segment>
								{/* <Grid> */}
								<Form.Group widths="equal">
									<Form.Field>
										<strong>Group ID</strong>
										<EbpsSelect
											name="groupId"
											// label='Group ID'
											options={groupOptions}
											onChange={(e, { value }) => {
												setFieldValue('groupId', value);
											}}
											value={values.groupId}
										/>
									</Form.Field>
									<Form.Field>
										<strong>User Type</strong>
										<EbpsSelect
											name="userType"
											// label='User Type'
											options={enterByOptions}
											onChange={(e, { value }) => {
												setFieldValue('userType', value);
												this.handleUserTypeSelect(`${values.groupType}-${value}`);
											}}
											value={values.userType}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Group Type</strong>
										<EbpsSelect
											name="groupType"
											// label='User Type'
											options={isKamalamai ? groupTypeKamalamaiOpt : groupTypeOpt}
											onChange={(e, { value }) => {
												setFieldValue('groupType', value);
												this.handleUserTypeSelect(`${value}-${values.userType}`);
											}}
											value={values.groupType}
										/>
									</Form.Field>
								</Form.Group>
								<Form.Group widths="equal">
									<Form.Field>
										Form ID
										<SearchableDropdown
											options={formNameOptions}
											name="formId"
											value={values.formId}
											handleChange={(value) => setFieldValue('formId', value)}
										/>
										{/* <EbpsSelect
											name="formId"
											// label='Form ID'
											options={formNameOptions}
											onChange={(e, { value }) => {
												setFieldValue('formId', value);
											}}
											value={values.formId}
										/> */}
									</Form.Field>
									<Form.Field>
										<strong>Position</strong>
										<Input
											name="groupPosition"
											type="number"
											onChange={(e, { value }) => {
												setFieldValue('groupPosition', value);
											}}
											value={values.groupPosition}
										/>
									</Form.Field>
									<Form.Field>
										Previous Form
										<SearchableDropdown
											options={previousFormOptions}
											name="previousForm"
											value={values.previousForm}
											handleChange={(value) => setFieldValue('previousForm', value)}
										/>
										{/* <EbpsSelect
											name="previousForm"
											options={previousFormOptions}
											// type="number"
											onChange={(e, { value }) => {
												setFieldValue('previousForm', value);
											}}
											value={values.previousForm}
										/> */}
									</Form.Field>
									{/* </Grid.Row> */}
									{/* </Grid> */}
								</Form.Group>
								<br />
								<Button onClick={handleSubmit} className="fileStorageSegment">
									{this.state.isEditing ? 'Update' : 'Save'}
								</Button>
							</Segment>
							<br />
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									Form Group Data
								</Segment>
								<Table {...setupTableProps}>
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell>User</Table.HeaderCell>
											<Table.HeaderCell>Group</Table.HeaderCell>
											<Table.HeaderCell>Group Type</Table.HeaderCell>
											<Table.HeaderCell>Form</Table.HeaderCell>
											<Table.HeaderCell>Position</Table.HeaderCell>
											<Table.HeaderCell>Previous Form</Table.HeaderCell>
											<Table.HeaderCell>Action</Table.HeaderCell>
										</Table.Row>
									</Table.Header>

									<Table.Body>
										{this.props.formList.map((f, index) => {
											const previousForm = getOptionText(f.previousForm, previousFormOptions);
											return (
												<Table.Row key={index}>
													<Table.Cell>{getOptionText(f.userType.id, enterByOptions)}</Table.Cell>
													<Table.Cell>{f.groupId.name}</Table.Cell>
													<Table.Cell>{getOptionText(f.groupType, groupTypeKamalamaiOpt)}</Table.Cell>
													<Table.Cell>{f.formId.name}</Table.Cell>
													<Table.Cell>{f.groupPosition}</Table.Cell>
													<Table.Cell>{previousForm}</Table.Cell>
													<Table.Cell>
														<Button
															inverted
															title="Edit"
															color="green"
															onClick={() => this.handleEdit(f)}
															icon="edit"
															size="tiny"
															style={{ marginBottom: '2px' }}
														></Button>

														<Button
															inverted
															color="red"
															title="Delete"
															icon="remove circle"
															onClick={() => this.modalOpen(f.id, f.groupId.id, f.userType.id)}
															size="tiny"
														></Button>

														<Modal close={this.modalClose} open={this.state.isModalOpen}>
															<Modal.Content>
																<p>Do you want to delete this section?</p>
															</Modal.Content>
															<Modal.Actions>
																<Button
																	color="green"
																	onClick={() => this.handleDelete(this.state.idToDelete)}
																	positive
																	labelPosition="left"
																	icon="checkmark"
																	content="Yes"
																	title="Confirm Delete!"
																></Button>
																<Button
																	color="red"
																	onClick={this.modalClose}
																	negative
																	labelPosition="left"
																	icon="remove"
																	content="No"
																	title="Cancel"
																></Button>
															</Modal.Actions>
														</Modal>
													</Table.Cell>
												</Table.Row>
											);
										})}
									</Table.Body>
								</Table>
							</Segment>
						</Form>
						// </FormWrapper>
					)}
				</Formik>
				// </div>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = {
	getFormGroupData,
	getFormNameMasterData,
	getFormGroupMasterData,
	postFormDatas,
	putFormDatas,
	deleteFormDatas,
};
const mapStateToProps = (state) => ({
	formNames: state.root.admin.formNames,
	userTypeMaster: state.root.admin.userTypeMaster,
	group: state.root.admin.group,
	loading: state.root.ui.loading,
	formList: state.root.admin.formList,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(FormGroup);
