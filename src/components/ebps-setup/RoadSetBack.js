import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Table, Button, Segment, Modal, Label, Select, ButtonContent, Icon } from 'semantic-ui-react';
import { EbpsFormIm } from '../shared/EbpsForm';
import EbpsNormalForm from '../shared/EbpsForm';
import { postRoadSetBack, getRoadSetBack, putRoadSetBack, deleteRoadSetBack } from '../../store/actions/AdminAction';
import { connect } from 'react-redux';
import FallbackComponent from '../shared/FallbackComponent';
import * as Yup from 'yup';
import { lengthconvertor } from '../../utils/mathUtils';
import { validateString, validateNumber } from '../../utils/validationUtils';
import { showToast } from '../../utils/functionUtils';
import ErrorDisplay from '../shared/ErrorDisplay';
import { statusOption } from '../../utils/optionUtils';

const strData = ['roadName', 'status'];
const unitData = ['setBackFoot', 'setBackMeter'];
const formDataSchema = strData.map(row => {
	return {
		[row]: validateString,
	};
});
const unitDataSchema = unitData.map(row => {
	return {
		[row]: validateNumber,
	};
});
const roadSetBackSchema = Yup.object().shape(Object.assign({}, ...formDataSchema, ...unitDataSchema));

const initialValues = {
	id: '',
	roadName: '',
	setBackFoot: 0,
	setBackMeter: 0,
	status: 'N',
};
class RoadSetBack extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentUser: initialValues,
			isEditing: false,
			isModelOpen: false,
			idToDelete: '',
		};
	}

	modelClose = () => {
		this.setState({
			isModelOpen: false,
		});
	};

	modelOpen = id => {
		this.setState({ isModelOpen: true, idToDelete: id });
	};

	handleAdd = () => {
		window.scrollTo(0, 0);
		this.setState({
			currentUser: initialValues,
			isEditing: false,
		});
	};

	handleEdit = obj => {
		// console.log('User:', obj);
		window.scrollTo(0, 0);
		this.setState({
			currentUser: {
				roadName: obj.roadName,
				setBackFoot: obj.setBackFoot,
				setBackMeter: obj.setBackMeter,
				status: obj.status,
				id: obj.id,
			},
			isEditing: true,
		});
	};
	handleDelete = async id => {
		try {
			await this.props.deleteRoadSetBack(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
				this.props.getRoadSetBack();
			}
			this.modelClose();
		} catch (err) {
			console.log('Error', err);
			this.modelClose();
		}
	};
	componentDidMount() {
		this.props.getRoadSetBack();
	}

	// componentDidUpdate(prevProps) {
	// 	if (prevProps.success !== this.props.success) {
	//         this.handleAdd();
	// 		// setTimeout(() => {
	// 			this.props.getRoadSetBack();
	// 		// }, 0);
	// 	}
	// }
	render() {
		const { postRoadSetBack, userData, putRoadSetBack } = this.props;
		if (userData) {
			return (
				<Formik
					enableReinitialize
					validationSchema={roadSetBackSchema}
					initialValues={this.state.currentUser}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						// console.log(values);
						try {
							if (this.state.isEditing) {
								// console.log('isediting', this.state.currentUser.id);
								await putRoadSetBack(this.state.currentUser.id, values);
							} else {
								await postRoadSetBack(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully posted.');
								this.props.getRoadSetBack();
								this.handleAdd();
								actions.resetForm();
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ values, setFieldValue, handleSubmit, errors }) => (
						<Form>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									Road SetBack
									<Button
										basic
										style={{ right: 6, top: 8, position: 'absolute' }}
										inverted
										type="reset"
										onClick={() => {
											this.handleAdd();
										}}
									>
										<ButtonContent>
											<Icon name="add" />
											Add Road Set Back
										</ButtonContent>
									</Button>
								</Segment>
								<Form.Group widths="equal">
									<Form.Field>
										<strong>Road Name</strong>
										<EbpsFormIm name="roadName" setFieldValue={setFieldValue} error={errors.roadName} value={values.roadName} />
									</Form.Field>
									<Form.Field>
										<strong>SetBack Foot</strong>
										<EbpsNormalForm
											name="setBackFoot"
											error={errors.setBackFoot}
											value={values.setBackFoot}
											onChange={e => {
												if (e.target.value !== undefined) {
													setFieldValue('setBackFoot', e.target.value);
													setFieldValue('setBackMeter', lengthconvertor(e.target.value, 'METRE', 'FEET'));
												}
											}}
										/>
									</Form.Field>
									<Form.Field>
										<strong>SetBack Meter</strong>
										<EbpsNormalForm
											name="setBackMeter"
											error={errors.setBackMeter}
											value={values.setBackMeter}
											onChange={e => {
												if (e.target.value !== undefined) {
													setFieldValue('setBackMeter', e.target.value);
													setFieldValue('setBackFoot', lengthconvertor(e.target.value, 'FEET', 'METRE'));
												}
											}}
										/>
									</Form.Field>
									<Form.Field error={!!errors.status}>
										<strong>Status</strong>
										<br />
										<Select
											placeholder="Yes"
											value={values.status}
											name="status"
											options={statusOption}
											onChange={(e, { value }) => {
												setFieldValue('status', value);
											}}
										/>
										{errors.status && (
											<Label pointing prompt size="large">
												{errors.status}
											</Label>
										)}
									</Form.Field>
								</Form.Group>
								<br />
								<Button onClick={handleSubmit} className="fileStorageSegment">
									{this.state.isEditing ? 'Update' : 'Save'}
								</Button>
							</Segment>
							<br />
							<Segment raised>
								<Segment className="tableSectionHeader">Road SetBack Data</Segment>
								<Table striped celled selectable style={{ textAlign: 'center' }}>
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell>Road Name</Table.HeaderCell>
											<Table.HeaderCell>SetBack Foot</Table.HeaderCell>
											<Table.HeaderCell>SetBack Meter</Table.HeaderCell>
											<Table.HeaderCell>Status</Table.HeaderCell>
											<Table.HeaderCell>Action</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									<Table.Body>
										{this.props.userData.map((roadSet, index) => {
											return (
												<Table.Row key={index}>
													<Table.Cell>{roadSet.roadName}</Table.Cell>
													<Table.Cell>{roadSet.setBackFoot}</Table.Cell>
													<Table.Cell>{roadSet.setBackMeter}</Table.Cell>
													<Table.Cell>{roadSet.status}</Table.Cell>
													<Table.Cell>
														<Button
															// circular
															inverted
															color="green"
															title="Edit"
															size="tiny"
															onClick={() => this.handleEdit(roadSet)}
															icon="edit"
														></Button>
														<Button
															// circular
															inverted
															color="red"
															title="Delete"
															icon="remove circle"
															size="tiny"
															onClick={() => this.modelOpen(roadSet.id)}
														></Button>
														<Modal close={this.modelClose} open={this.state.isModelOpen}>
															<Modal.Content>Are you sure you want to delete this section?</Modal.Content>
															<Modal.Actions>
																<Button
																	color="green"
																	onClick={() => this.handleDelete(this.state.idToDelete)}
																	positive
																	labelPosition="left"
																	icon="checkmark"
																	content="Yes"
																	title="Confirm Delete!"
																></Button>
																<Button
																	color="red"
																	onClick={this.modelClose}
																	negative
																	labelPosition="left"
																	icon="remove"
																	content="No"
																	title="Decline Delete!"
																></Button>
															</Modal.Actions>
														</Modal>
													</Table.Cell>
												</Table.Row>
											);
										})}
									</Table.Body>
								</Table>
							</Segment>
						</Form>
					)}
				</Formik>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = { postRoadSetBack, getRoadSetBack, putRoadSetBack, deleteRoadSetBack };
const mapStateToProps = state => ({
	userData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(RoadSetBack);
