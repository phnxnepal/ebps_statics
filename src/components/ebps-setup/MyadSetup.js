import React, { useState, useEffect } from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { showToast, isEmpty } from '../../utils/functionUtils';
import { Divider, Header, Form } from 'semantic-ui-react';
import { Formik } from 'formik';
// import { applicationTable } from '../../utils/data/genericSetupData';
import { namsariColumns, namsariDatafield } from '../../utils/data/ebps-setup/namsariSetupData';
import { discardData } from '../../utils/data/ebps-setup/discardSetupData';
import { myadData } from '../../utils/data/ebps-setup/myadThapData';
import * as Yup from 'yup';
// import EbpsForm, { EbpsNormalForm } from '../shared/EbpsForm';
// import { SelectInput } from '../shared/Select';
// import { consTypeWithDefaultSelectOptions } from '../../utils/optionUtils';
// import GenericSetupTable from './forms/GenericSetupTable';
import { AdminModalContainer } from './AdminModalContainer';
import ErrorDisplay from '../shared/ErrorDisplay';
// import { getDataConstructionType } from '../../utils/enums/constructionType';
import { validateNepaliDate, validateFileOptional } from '../../utils/validationUtils';
import { getCurrentDate } from './../../utils/dateUtils';
import Helmet from 'react-helmet';
import ApplicationDetails from './shared/ApplicationDetails';
import GenericTableOneButton from './forms/GenericTableOneButton';

const changeDesignerSchema = Yup.object().shape({
	file: validateFileOptional,
	date: validateNepaliDate,
});

// // const searchSchema = Yup.object().shape({
// // 	applicationNo: validateNullableNormalNumber,
// // });

// const applicationData = applicationTable;
const modalData = discardData.modal;

// const initialValues = {
// 	nibedakName: '',
// 	constructionType: '',
// 	year: '',
// 	kittaNo: '',
// 	wardNo: '',
// 	applicationNo: '',
// };
const intiVal1 = {
	date: getCurrentDate(),
};

const MyadSetupComponent = ({ postAdminDataByUrl, errors, success, loading, adminData }) => {
	const [appInfo, setAppInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	// const [historyList, sethistoryList] = useState([]);
	const [isChanging, setIsChanging] = useState(false);
	const [dataChanged] = useState(false);

	// const { fy, fyOption } = useFiscalYearAll(adminData.fiscalYear, 'yearCode', true);
	useJqxNepaliFilter(dataChanged, [2, 3, 4]);

	useEffect(() => {
		if (success && success.success) {
			showToast('Data saved successfully');
			setOpenModal(false);
			setIsChanging(false);
			setAppInfo({});
		}
	}, [success]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	// useEffect(() => {
	//     adminData.historyList &&
	//         sethistoryList(
	//             adminData.historyList.map(el => {
	//                 return { ...el, constructionType: getDataConstructionType(el.constructionType) };
	//             })
	//         );
	// }, [adminData.historyList]);

	// useEffect(() => {
	// 	const changeButtons = document.querySelectorAll('.myad-app');
	// 	if (changeButtons.length > 0) {
	// 		Array.from(changeButtons).forEach((button) => {
	// 			const row = button.dataset.row;
	// 			const application = appList[row];
	// 			button.addEventListener('click', () => handleConfirmation(application));
	// 		});
	// 	}

	// 	return () => {
	// 		const changeButtons = document.querySelectorAll('.myad-app');
	// 		if (changeButtons.length > 0) {
	// 			Array.from(changeButtons).forEach((button) => {
	// 				const row = button.dataset.row;
	// 				const application = appList[row];
	// 				button.removeEventListener('click', () => handleConfirmation(application));
	// 			});
	// 		}
	// 	};
	// }, [appList, dataChanged]);

	// const generateButtonRenderer = (row) => {
	// 	setDataChanged(!dataChanged);
	// 	return (
	// 		'<button title="म्याद थपको लागि पठाउनुहोस्" class="ui primary-table-single-btn  icon button myad-app" data-row=' +
	// 		row +
	// 		'><i aria-hidden="true" class="list alternate icon"></i></button>'
	// 	);
	// };

	const handleConfirmation = (row) => {
		setAppInfo(row);
		setOpenModal(true);
	};

	const handleChangeDesigner = () => {
		setOpenModal(false);
		setIsChanging(true);
	};

	const handleClose = () => {
		setAppInfo({});
		setIsChanging(false);
		setOpenModal(false);
	};
	// const discardYearHandleChange = year => {
	// 	getAfterUpdateAdminData([{ api: `${api.discardHistory}?year=${year}`, objName: 'historyList' }]);
	// };
	// const panes = [
	//     {
	//         menuItem: myadData.tab.setupHeading,
	//         render: () => (

	//         ),
	//     },
	// ];
	return (
		<div className="setup-main">
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{myadData.heading}</Header>
			<>
				<Helmet title={myadData.heading}></Helmet>
				{/* <Formik
                    key="get-app-info"
                    initialValues={{ ...initialValues, year: fy }}
                    validationSchema={searchSchema}
                    onSubmit={(values, actions) => {
                        actions.setSubmitting(true);
                        try {
                            setAppList([]);
                            getAfterUpdateAdminData([
                                {
                                    api: api.myadSetup,
                                    objName: 'applicationList',
                                },
                            ]);
                            actions.setSubmitting(false);
                        } catch (err) {
                            actions.setSubmitting(false);
                            console.log('err', err);
                        }
                    }}
                >
                    {({ handleSubmit, errors, isSubmitting, values, setFieldValue, handleChange }) => (
                        <Form loading={isSubmitting || loading}>
                            <Form.Group widths="4">
                                <Form.Field>
                                    <EbpsNormalForm
                                        name="applicationNo"
                                        label={applicationData.applicationId}
                                        onChange={handleChange}
                                        error={errors.applicationNo}
                                        value={values.applicationNo}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <EbpsForm
                                        name="nibedakName"
                                        label={applicationData.nibedakName}
                                        setFieldValue={setFieldValue}
                                        errors={errors.nibedakName}
                                        value={values.nibedakName}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <SelectInput
                                        needsZIndex={true}
                                        name="constructionType"
                                        label={applicationData.constructionType}
                                        options={consTypeWithDefaultSelectOptions}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <SelectInput needsZIndex={true} name="year" label={applicationData.year} options={fyOption} />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group widths="4">
                                <Form.Field>
                                    <EbpsForm
                                        name="kittaNo"
                                        label={applicationData.kittaNo}
                                        setFieldValue={setFieldValue}
                                        errors={errors.kittaNo}
                                        value={values.kittaNo}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <EbpsForm
                                        name="wardNo"
                                        label={applicationData.wardNo}
                                        setFieldValue={setFieldValue}
                                        errors={errors.wardNo}
                                        value={values.wardNo}
                                    />
                                </Form.Field>
                            </Form.Group>
                            <Form.Group widths="equal">
                                <Form.Field>
                                    <Button
                                        type="button"
                                        className="primary-btn"
                                        icon="search"
                                        content={searchData.searchApp}
                                        onClick={handleSubmit}
                                    />
                                </Form.Field>
                            </Form.Group>
                        </Form>
                    )}
                </Formik> */}
				{/* <h4>{searchData.searchResults}</h4> */}
				<GenericTableOneButton
					data={appList}
					columns={namsariColumns}
					datafield={namsariDatafield}
					handleButtonClick={(row) => handleConfirmation(appList[row])}
					buttonProps={{ class: 'primary-table-single-btn icon', title: 'म्याद थपको लागि पठाउनुहोस्', icon: 'list alternative icon' }}
					actionWidth={80}
				/>
				<br />
				{!isEmpty(appInfo) && (
					<div>
						<Divider />
						<Formik
							initialValues={intiVal1}
							key="assign-designer"
							validationSchema={changeDesignerSchema}
							onSubmit={(values, actions) => {
								actions.setSubmitting(true);
								try {
									postAdminDataByUrl(`${api.myadSetup}/${appInfo.applicantNo}`);
									actions.setSubmitting(false);
								} catch (err) {
									actions.setSubmitting(false);
								}
							}}
						>
							{({ handleSubmit, isSubmitting, errors, validateForm }) => {
								return (
									<Form>
										<AdminModalContainer
											key={'open-confirmation'}
											open={openModal}
											// errors={errors}
											isSubmitting={isSubmitting || loading}
											handleSubmit={handleChangeDesigner}
											handleClose={handleClose}
											title={myadData.modal.title}
											saveText={modalData.saveText}
											cancelText={modalData.cancelText}
										>
											{myadData.modal.content}
										</AdminModalContainer>

										<AdminModalContainer
											open={isChanging}
											// errors={errors}
											isSubmitting={isSubmitting || loading}
											handleSubmit={() => {
												validateForm().then((errors) => {
													if (isEmpty(errors)) {
														handleSubmit();
													}
												});
											}}
											handleClose={handleClose}
											title={myadData.modal.title}
											saveText={myadData.modal.title}
											cancelText={modalData.cancelText}
										>
											<Form loading={isSubmitting || loading}>
												<ApplicationDetails appInfo={appInfo} />
											</Form>
										</AdminModalContainer>
									</Form>
								);
							}}
						</Formik>
					</div>
				)}
			</>
		</div>
	);
};
export const MyadSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{ api: api.myadSetup, objName: 'applicationList' },
		]}
		updateApi={[{ api: api.myadSetup, objName: 'applicationList' }]}
		render={(props) => <MyadSetupComponent {...props} parentProps={parentProps} />}
	/>
);

export { MyadSetup as default };
