import React, { useState, useEffect } from 'react';
import api from '../../utils/api';
import AdminContainer from '../../containers/base/AdminContainer';
import { Form, Button, Segment, Header, List, Grid, Divider, Table } from 'semantic-ui-react';
import { Formik } from 'formik';
import ErrorDisplay from '../shared/ErrorDisplay';
import * as Yup from 'yup';
import { validateString, validateFile } from '../../utils/validationUtils';
import { SelectInput } from '../shared/Select';
import { UserType } from '../../utils/userTypeUtils';
import { isEmpty, showToast, getUserTypeValueNepali } from '../../utils/functionUtils';
import { changeDesignerData } from '../../utils/data/ebps-setup/changeDesignerData';
import { AdminModalContainer, AdminInfoModalContainer } from './AdminModalContainer';
import { translateEngToNepWithZero } from '../../utils/langUtils';
import { FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import ApplicationListTable from './forms/ApplicationListTable';
import EbpsForm from '../shared/EbpsForm';
import { constructionTypeSelectOptions } from '../../utils/optionUtils';
import { useFiscalYear } from '../../hooks/useFiscalYear';
import { FileViewText } from '../shared/file/FileView';
import Helmet from 'react-helmet';

// const searchSchema = Yup.object().shape({
// 	applicationNo: validateNormalNumber,
// });

const changeDesignerSchema = Yup.object().shape({
	designer: validateString,
	file: validateFile,
});

const applicationData = changeDesignerData.applicationInfo;
const modalData = changeDesignerData.modal;

const initialValues = {
	constructionType: '',
	year: '',
	nibedakName: '',
	kittaNo: '',
	wardNo: '',
	applicationStatus: '',
	applicationNo: '',
};

export const ChangeDesignerComponent = ({ getAfterUpdateAdminData, postAdminDataByUrl, errors, success, loading, adminData }) => {
	const [designerOption, setDesignerOption] = useState();
	const [appInfo, setAppInfo] = useState({});
	const [historyInfo, setHistoryInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [openHistory, setOpenHistory] = useState(false);
	const [isShowingHistory, setIsShowingHistory] = useState(false);
	const [isChanging, setIsChanging] = useState(false);
	const [dataChanged, setDataChanged] = useState(false);

	const { fyOption } = useFiscalYear(adminData.fiscalYear);

	useEffect(() => {
		if (success && success.success) {
			showToast('Data saved successfully');
			setOpenModal(false);
			setIsChanging(false);
			setAppInfo({});
		}
	}, [success]);

	useEffect(() => {
		if (adminData.history && isShowingHistory) {
			setHistoryInfo(adminData.history);
		}
	}, [adminData.history, isShowingHistory]);

	useEffect(() => {
		if (isShowingHistory) {
			setOpenHistory(true);
		}
	}, [isShowingHistory, historyInfo]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	// useEffect(() => {
	// 	adminData.applicationInfo && setAppInfo(adminData.applicationInfo);
	// }, [adminData.applicationInfo]);

	useEffect(() => {
		adminData.designers &&
			setDesignerOption(
				adminData.designers
					.filter(user => user.userType && user.userType.id === UserType.DESIGNER)
					.map(row => {
						return { value: row.loginId, text: `${row.userName} - ${row.email}` };
					})
			);
	}, [adminData.designers]);

	useEffect(() => {
		const changeButtons = document.querySelectorAll('.change-designer');
		if (changeButtons.length > 0) {
			Array.from(changeButtons).forEach(button => {
				const row = button.dataset.row;
				const application = appList[row];
				button.addEventListener('click', () => handleConfirmation(application));
			});
		}

		const historyButtons = document.querySelectorAll('.show-change-history');
		if (historyButtons.length > 0) {
			Array.from(historyButtons).forEach(button => {
				const row = button.dataset.row;
				const application = appList[row];
				button.addEventListener('click', () => handleShowHistory(application));
			});
		}

		return () => {
			const changeButtons = document.querySelectorAll('.change-designer');
			if (changeButtons.length > 0) {
				Array.from(changeButtons).forEach(button => {
					const row = button.dataset.row;
					const application = appList[row];
					button.removeEventListener('click', () => handleConfirmation(application));
				});
			}

			const historyButtons = document.querySelectorAll('.show-change-history');
			if (historyButtons.length > 0) {
				Array.from(historyButtons).forEach(button => {
					const row = button.dataset.row;
					const application = appList[row];
					button.removeEventListener('click', () => handleShowHistory(application));
				});
			}
		};
	}, [appList, dataChanged]);

	const generateButtonRenderer = row => {
		setDataChanged(!dataChanged);
		return (
			'<button title="Change Designer" class="ui primary-table-three-btn tiny icon button change-designer" data-row=' +
			row +
			'><i aria-hidden="true" class="exchange icon"></i></button><button title="Show History" class="ui primary-table-three-btn tiny icon button show-change-history" data-row=' +
			row +
			'><i aria-hidden="true" class="history icon"></i></button>'
		);
	};

	const handleConfirmation = row => {
		setAppInfo(row);
		setOpenModal(true);
	};

	const handleChangeDesigner = () => {
		setIsChanging(true);
	};

	const handleClose = () => {
		setAppInfo({});
		setIsChanging(false);
		setOpenModal(false);
	};

	const handleHistoryClose = () => {
		setHistoryInfo([]);
		setIsShowingHistory(false);
		setOpenHistory(false);
	};

	const handleShowHistory = row => {
		setHistoryInfo([]);
		setAppInfo(row);
		getAfterUpdateAdminData([{ api: `${api.changeDesigner}/${parseInt(row.applicantNo)}`, objName: 'history' }]);
		setIsShowingHistory(true);
	};

	return (
		<div className="setup-main">
			<Helmet title={modalData.title} />
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{modalData.title}</Header>
			<Formik
				key="get-app-info"
				initialValues={initialValues}
				// validationSchema={searchSchema}
				onSubmit={(values, actions) => {
					actions.setSubmitting(true);
					try {
						// setApplicationNo(values.applicationNo);
						setAppList([]);
						// getAfterUpdateAdminData([{ api: `${api.buildPermit}`, objName: 'applicationInfo' }]);
						// getAfterUpdateAdminData([{ api: `${api.changeDesigner}/${parseInt(values.applicationNo)}`, objName: 'applicationInfo' }]);
						getAfterUpdateAdminData([
							{
								api: `${api.applicationFilter}?constructionType=${values.constructionType}&year=${values.year}&nibedakName=${values.nibedakName}&kittaNo=${values.kittaNo}&wardNo=${values.wardNo}&applicationStatus=${values.applicationStatus}`,
								objName: 'applicationList',
							},
						]);
						actions.setSubmitting(false);
					} catch (err) {
						actions.setSubmitting(false);
						console.log('err', err);
					}
				}}
			>
				{({ handleSubmit, errors, isSubmitting, values, setFieldValue }) => (
					<Form loading={isSubmitting || loading}>
						<Form.Group widths="equal">
							{/* <Form.Field error={!!errors.applicationNo}>
								<Field type="text" name="applicationNo" placeholder="Enter Application Number" />
								{errors.applicationNo && (
									<Label pointing prompt size="large">
										{errors.applicationNo}
									</Label>
								)}
							</Form.Field> */}
							<Form.Field>
								<EbpsForm
									name="nibedakName"
									label={applicationData.nibedakName}
									setFieldValue={setFieldValue}
									errors={errors.nibedakName}
									value={values.nibedakName}
								/>
							</Form.Field>
							<Form.Field>
								<SelectInput
									needsZIndex={true}
									name="constructionType"
									label={applicationData.constructionType}
									options={constructionTypeSelectOptions}
								/>
							</Form.Field>
							<Form.Field>
								<EbpsForm
									name="kittaNo"
									label={applicationData.kittaNo}
									setFieldValue={setFieldValue}
									errors={errors.kittaNo}
									value={values.kittaNo}
								/>
							</Form.Field>
							<Form.Field>
								<EbpsForm name="wardNo" label={applicationData.wardNo} setFieldValue={setFieldValue} errors={errors.wardNo} value={values.wardNo} />
							</Form.Field>

							<Form.Field>
								<SelectInput needsZIndex={true} name="year" label={applicationData.year} options={fyOption} />
							</Form.Field>
						</Form.Group>
						<Form.Group widths="equal">
							<Form.Field>
								<Button className="primary-btn" icon="search" content="Search Application" onClick={handleSubmit} />
							</Form.Field>
						</Form.Group>
					</Form>
				)}
			</Formik>
			<h4>Search Results</h4>

			<ApplicationListTable
				handleAction={handleConfirmation}
				handleShowHistory={handleShowHistory}
				applicationList={appList}
				generateButtonRenderer={generateButtonRenderer}
			/>
			{!isEmpty(appInfo) && (
				<div>
					<Divider />
					<Formik
						key="change-designer"
						validationSchema={changeDesignerSchema}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);
							// console.log('values', values);
							const data = new FormData();
							const selectedFile = values.file;

							if (selectedFile) {
								selectedFile.forEach(file => data.append('file', file));
							}

							// data.append('designer', values.designer)

							try {
								postAdminDataByUrl(`${api.changeDesigner}/${appInfo.applicantNo}?designer=${values.designer}`, data);
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('err', err);
							}
						}}
					>
						{({ handleSubmit, isSubmitting, errors: formErrors, validateForm }) => (
							<Form>
								<AdminModalContainer
									key={'open-confirmation'}
									open={openModal}
									errors={errors}
									isSubmitting={isSubmitting || loading}
									handleSubmit={handleChangeDesigner}
									handleClose={handleClose}
									title={modalData.title}
									saveText={modalData.saveText}
									cancelText={modalData.cancelText}

								>
									{modalData.content}
								</AdminModalContainer>

								{/* Show History Modal */}
								<AdminInfoModalContainer
									key={'show-change-history'}
									open={openHistory}
									isSubmitting={loading}
									handleClose={handleHistoryClose}
									title={modalData.historyTitle}
									saveText={modalData.historyOkButton}
								>
									<Form loading={loading}>{historyInfo && <ChangeHistory historyData={historyInfo} appId={appInfo.applicantNo} />}</Form>
								</AdminInfoModalContainer>

								<AdminModalContainer
									open={isChanging}
									errors={errors}
									isSubmitting={isSubmitting || loading}
									handleSubmit={() => {
										validateForm().then(errors => {
											if (isEmpty(errors)) {
												handleSubmit();
											}
										});
									}}
									handleClose={handleClose}
									title={modalData.title}
									saveText={modalData.confirmChange}
									cancelText={modalData.cancelText}


								>
									<Form loading={isSubmitting || loading}>
										<Form.Group widths="equal">
											<Form.Field error={!!formErrors.designer}>
												<label>{modalData.select}</label>
												<SelectInput name="designer" options={designerOption} />
											</Form.Field>
											<Form.Field error={!!formErrors.file}>
												<label>{modalData.file}</label>
												<FileInputWithMultiplePreview name="file" fileCatId={'change-designer'} hasMultipleFiles={false} />
											</Form.Field>
										</Form.Group>
										{/* <Form.Group> */}
										{/* </Form.Group> */}
										<Segment>
											<Header>{applicationData.applicationInfo}</Header>
											<List className="app-info">
												<Grid>
													<Grid.Row columns="4">
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.applicantNo}</List.Header>
																<List.Description>{applicationData.applicationId}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.applicantName}</List.Header>
																<List.Description>{applicationData.applicantName}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.nibedakName}</List.Header>
																<List.Description>{applicationData.nibedakName}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.applicationStatus}</List.Header>
																<List.Description>{applicationData.applicationStatus}</List.Description>
															</List.Item>
														</Grid.Column>
													</Grid.Row>
													<Grid.Row columns="4">
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.applicantMobileNo}</List.Header>
																<List.Description>{applicationData.applicantMobileNo}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{translateEngToNepWithZero(appInfo.applicantDate)}</List.Header>
																<List.Description>{applicationData.applicantDate}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.constructionType}</List.Header>
																<List.Description>{applicationData.constructionType}</List.Description>
															</List.Item>
														</Grid.Column>
														<Grid.Column>
															<List.Item>
																<List.Header>{getUserTypeValueNepali(appInfo.applicationActionBy)}</List.Header>
																<List.Description>{applicationData.applicationActionBy}</List.Description>
															</List.Item>
														</Grid.Column>
													</Grid.Row>
													{/* <Grid.Row>
														<Grid.Column>
															<List.Item>
																<List.Header>{appInfo.forwardTo}</List.Header>
																<List.Description>{applicationData.forwardTo}</List.Description>
															</List.Item>
														</Grid.Column>
													</Grid.Row> */}
												</Grid>
											</List>
										</Segment>
									</Form>
								</AdminModalContainer>

								{/* <Button
									className="primary-btn"
									onClick={() => {
										validateForm().then(errors => {
											if (isEmpty(errors)) {
												setOpenModal(true);
											}
										});
									}}
								>
									Change Designer
								</Button> */}
							</Form>
						)}
					</Formik>
				</div>
			)}
		</div>
	);
};

export const ChangeDesigner = parentProps => (
	<AdminContainer
		api={[
			{ api: api.organizationUser, objName: 'designers' },
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{
				api: `${api.applicationFilter}?constructionType=${initialValues.constructionType}&year=${initialValues.year}&nibedakName=${initialValues.nibedakName}&kittaNo=${initialValues.kittaNo}&wardNo=${initialValues.wardNo}&applicationStatus=${initialValues.applicationStatus}`,
				objName: 'applicationList',
			},
		]}
		updateApi={[]}
		// updateApi={[{ api: api.changeDesigner, objName: 'changeDesigner' }]}
		render={props => <ChangeDesignerComponent {...props} parentProps={parentProps} />}
	/>
);

export const ChangeHistory = ({ historyData, appId }) => (
	<>
		<h3>{applicationData.applicationNumb} {appId}</h3>
		{historyData.length > 0 ? (
			<div className="scrollable-table-body">
				<Table celled compact="very" striped>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>Changed By</Table.HeaderCell>
							<Table.HeaderCell>Changed On</Table.HeaderCell>
							<Table.HeaderCell>Changed To</Table.HeaderCell>
							<Table.HeaderCell>File</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{historyData.map(
							row =>
								row && (
									<Table.Row key={row.id}>
										<Table.Cell>{row.changeBy}</Table.Cell>
										<Table.Cell>{row.changeDate}</Table.Cell>
										<Table.Cell>{row.changeTo}</Table.Cell>
										<Table.Cell>
											<FileViewText el={{ fileUrl: row.fileUrl }} viewText="View Voucher" />
										</Table.Cell>
									</Table.Row>
								)
						)}
					</Table.Body>
				</Table>
			</div>
		) : (
				<Segment>{modalData.historyNoData}</Segment>
			)}
	</>
);
