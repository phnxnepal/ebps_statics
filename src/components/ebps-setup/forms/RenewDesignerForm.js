import React from 'react';
import { Form, Grid, Table, Divider } from 'semantic-ui-react';
import { FileViewText, FileViewThumbnail } from '../../shared/file/FileView';
import { EbpsFormDateField } from '../../shared/DateField';
import { SelectInput } from '../../shared/Select';
import { FileInputWithPreview } from '../../shared/FileUploadInput';
import { userdata } from '../../../utils/data/ebps-setup/userformdata';
import { designer } from '../../../utils/data/ebps-setup/designerformdata';
import EbpsForm from '../../shared/EbpsForm';
import { getCurrentDate } from '../../../utils/dateUtils';

const us = userdata.us_data;
const des = designer.designer_data;

export const RenewDesignerForm = ({
	currentUser,
	loading,
	renewData,
	values,
	errors,
	handleChange,
	setFieldValue,
	fiscalYearOptions,
	latestRenew,
}) => {
	return (
		<Form loading={loading} className="setup-modal">
			{latestRenew && (
				<Form.Group widths="three">
					<Form.Field>
						<b>{des.renew.previousRenewDate}</b>
						<p>{latestRenew.renewDateBS}</p>
					</Form.Field>
					<Form.Field>
						<b>{des.renew.previousRenewTillDate}</b>
						<p>{latestRenew.tillDateBS} </p>
					</Form.Field>
				</Form.Group>
			)}
			<Form.Group widths="three">
				<Form.Field>
					<b>{des.renew.renewDate}</b>
					<p>{getCurrentDate()} </p>
				</Form.Field>
				<Form.Field>
					<EbpsFormDateField
						label={des.renew.renewTillDate}
						name="tillDate"
						handleChange={handleChange}
						error={errors.tillDate}
						value={values.tillDate}
					/>
				</Form.Field>
				<Form.Field>
					<SelectInput label={des.renew.fiscalYear} name="fiscalYear" options={fiscalYearOptions} />
				</Form.Field>
			</Form.Group>
			<Form.Group widths="three">
				<Form.Field>
					{des.renew.charge}
					<FileInputWithPreview name="charge" hasMultipleFiles={false} error={errors.charge} fileCatId={'renewBill'} />
				</Form.Field>
				<Form.Field>
					<EbpsForm label={des.renew.billNo} name="billNo" setFieldValue={setFieldValue} error={errors.billNo} value={values.billNo} />
				</Form.Field>
			</Form.Group>
			<Divider />
			<h3>{us.modal.titleView}</h3>
			<Grid padded className="setup-modal">
				<Grid.Row>
					<Grid.Column width={8}>
						<p>
							{us.formdata.loginId} : {currentUser.userName}
						</p>
						<p>
							{us.formdata.address}: {currentUser.address}
						</p>
						<p>Consultancy Name: {currentUser.consultancyName}</p>
						<p>
							{us.formdata.email}: {currentUser.email}
						</p>
						<p>
							{us.formdata.issueDate}: {currentUser.joinDateBS}
						</p>
					</Grid.Column>
					<Grid.Column width={8}>
						{us.formdata.photo}: <FileViewThumbnail deletable={false} el={{ fileUrl: currentUser.photo }} />
					</Grid.Column>
				</Grid.Row>
			</Grid>
			<Divider />
			<RenewHistory renewData={renewData} />
		</Form>
	);
};

export const RenewHistory = ({ renewData }) => (
	<>
		<h3>Renewal History</h3>
		<div className="scrollable-table-body">
			<Table celled compact="very" striped>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>Renewed By</Table.HeaderCell>
						<Table.HeaderCell>Renewed On</Table.HeaderCell>
						<Table.HeaderCell>Till Date</Table.HeaderCell>
						<Table.HeaderCell>Fiscal Year</Table.HeaderCell>
						<Table.HeaderCell>Bill Number</Table.HeaderCell>
						<Table.HeaderCell>Voucher</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{renewData.length <= 0 ? (
						<Table.Row>This designer has not been renewed yet.</Table.Row>
					) : (
							renewData.map(
								row =>
									row && (
										<Table.Row key={row.id}>
											<Table.Cell>{row.renewBy}</Table.Cell>
											<Table.Cell>{row.renewDateBS}</Table.Cell>
											<Table.Cell>{row.tillDateBS}</Table.Cell>
											<Table.Cell>{row.fiscalYear}</Table.Cell>
											<Table.Cell>{row.billNo}</Table.Cell>
											<Table.Cell>
												<FileViewText el={{ fileUrl: row.fileUrl }} viewText="View Voucher" />
											</Table.Cell>
										</Table.Row>
									)
							)
						)}
				</Table.Body>
			</Table>
		</div>
	</>
);
