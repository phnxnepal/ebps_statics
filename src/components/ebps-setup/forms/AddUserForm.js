import React from 'react';
import { Form, List, Grid } from 'semantic-ui-react';
import EbpsForm, { EbpsNormalForm } from '../../shared/EbpsForm';
import { SelectInput } from '../../shared/Select';
import { statusOption, userTypeDesignerOption } from '../../../utils/optionUtils';
import { LangDateField } from '../../shared/DateField';
import { FileInputWithMultiplePreview } from '../../shared/FileUploadInput';
import { FileViewThumbnail } from '../../shared/file/FileView';
import { getOptionText } from '../../../utils/dataUtils';

export const AddUserForm = ({
	handleChange,
	labelData: us,
	loading,
	errors,
	values,
	existingImages,
	setFieldValue,
	userTypeOption,
	isEditing,
	setLoginId = undefined,
}) => {
	return (
		<Form loading={loading}>
			<Form.Group widths="equal">
				<Form.Field>
					<EbpsForm
						label={us.formdata.formdata1}
						name="userName"
						onChange={handleChange}
						error={errors.userName}
						value={values.userName}
						setFieldValue={setFieldValue}
					/>
				</Form.Field>
				<Form.Field>
					<EbpsForm
						label={us.formdata.formdata2}
						name="address"
						onChange={handleChange}
						error={errors.address}
						value={values.address}
						setFieldValue={setFieldValue}
					/>
				</Form.Field>
				<Form.Field>
					<EbpsForm
						label={us.formdata.formdata3}
						name="mobile"
						// disabled={true}
						onChange={handleChange}
						value={values.mobile}
						error={errors.mobile}
						setFieldValue={setFieldValue}
					/>
				</Form.Field>
			</Form.Group>

			<Form.Group widths="equal">
				<Form.Field>
					<EbpsNormalForm label={us.formdata.formdata5} name="email" onChange={handleChange} error={errors.email} value={values.email} />
				</Form.Field>
				<Form.Field>
					<EbpsNormalForm
						label={us.formdata.formdata13}
						name="educationQualification"
						onChange={handleChange}
						error={errors.educationQualification}
						value={values.educationQualification}
					/>
					{/* <EbpsForm
						label={us.formdata.formdata13}
						name="educationQualification"
						onChange={handleChange}
						error={errors.educationQualification}
						value={values.educationQualification}
						setFieldValue={setFieldValue}
					/> */}
				</Form.Field>
				<Form.Field>
					<EbpsNormalForm
						label={us.formdata.formdata14}
						name="licenseNo"
						onChange={handleChange}
						error={errors.licenseNo}
						value={values.licenseNo || ''}
					/>
				</Form.Field>
			</Form.Group>

			<Form.Group widths="equal">
				<Form.Field>
					<EbpsNormalForm
						label={us.formdata.formdata19}
						name="loginId"
						readOnly={isEditing}
						onChange={e => {
							if (isEditing) {
								return undefined;
							} else {
								if (setLoginId) {
									setLoginId(e.target.value)
								}
								handleChange(e);
							}
						}}
						error={errors.loginId}
						value={values.loginId}
					/>
				</Form.Field>
				<Form.Field>
					<EbpsForm
						label={us.formdata.formdata16}
						name="municipalRegNo"
						onChange={handleChange}
						error={errors.municipalRegNo}
						value={values.municipalRegNo}
						setFieldValue={setFieldValue}
					/>
				</Form.Field>

				<Form.Field error={!!errors.userType}>
					{us.formdata.formdata10}
					<SelectInput name="userType" options={userTypeOption} />
				</Form.Field>
			</Form.Group>
			<Form.Group widths="three">
				<Form.Field error={!!errors.status}>
					{us.formdata.formdata18}
					<SelectInput name="status" options={statusOption} />
				</Form.Field>
				<Form.Field>
					<EbpsNormalForm
						label={us.formdata.consultancyName}
						name="consultancyName"
						onChange={handleChange}
						error={errors.consultancyName}
						value={values.consultancyName || ''}
					/>
				</Form.Field>
				{isEditing && (
					<fieldset style={{ marginLeft: 7 }} className="borderlessFieldset" disabled={true}>
						<Form.Field>
							<LangDateField label={us.formdata.formdata11} name="joinDate" value={values.joinDateBS} />
						</Form.Field>
					</fieldset>
				)}
			</Form.Group>
			<Form.Group widths="equal">
				<FileInput existingImage={existingImages.userImage} name="userImage" label={us.formdata.formdata12} />
				<FileInput existingImage={existingImages.userStamp} name="userStamp" label={us.formdata.formdata17} />
			</Form.Group>
			<Form.Group>
				<FileInput existingImage={existingImages.userSignature} name="userSignature" label={us.formdata.formdata15} />
			</Form.Group>
		</Form>
	);
};

const FileInput = ({ existingImage, label, name }) => (
	<Form.Field>
		<label>{label}</label>
		<FileInputWithMultiplePreview name={name} fileCatId={name} hasMultipleFiles={false} />
		{existingImage && <FileViewThumbnail deletable={false} el={{ fileUrl: existingImage }} />}
	</Form.Field>
);

export const ViewUser = ({ labelData: us, values, existingImages }) => {
	return (
		<List className="app-info">
			<Grid>
				<Grid.Row columns="4">
					<Grid.Column>
						<List.Item>
							<List.Header>{values.userName}</List.Header>
							<List.Description>{us.formdata.formdata1}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.address}</List.Header>
							<List.Description>{us.formdata.address}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.consultancyName}</List.Header>
							<List.Description>{us.formdata.consultancyName}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.educationQualification}</List.Header>
							<List.Description>{us.formdata.eduQualification}</List.Description>
						</List.Item>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row columns="4">
					<Grid.Column>
						<List.Item>
							<List.Header>{values.email}</List.Header>
							<List.Description>{us.formdata.email}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.fiscalYear}</List.Header>
							<List.Description>{us.formdata.fiscalYear}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.joinDateBS}</List.Header>
							<List.Description>{us.formdata.issueDate}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.licenseNo}</List.Header>
							<List.Description>{us.formdata.license}</List.Description>
						</List.Item>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row columns="4">
					<Grid.Column>
						<List.Item>
							<List.Header>{values.loginId}</List.Header>
							<List.Description>{us.formdata.loginId}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.mobile}</List.Header>
							<List.Description>{us.formdata.phone}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.municipalRegNo}</List.Header>
							<List.Description>{us.formdata.regNo}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{getOptionText(values.status, statusOption)}</List.Header>
							<List.Description>{us.formdata.status}</List.Description>
						</List.Item>
					</Grid.Column>
				</Grid.Row>
				<Grid.Row columns="2">
					<Grid.Column>
						<List.Item>
							<List.Header>{getOptionText(values.userType, userTypeDesignerOption)}</List.Header>
							<List.Description>{us.formdata.userType}</List.Description>
						</List.Item>
					</Grid.Column>
					<Grid.Column>
						<List.Item>
							<List.Header>{values.validDateBS}</List.Header>
							<List.Description>{us.formdata.expiryDate}</List.Description>
						</List.Item>
					</Grid.Column>
				</Grid.Row>

				{existingImages && (
					<Grid.Row columns="3">
						<Grid.Column>
							<List.Item>
								<List.Header>{us.formdata.photo}</List.Header>
								<List.Description>
									<FileViewThumbnail deletable={false} el={{ fileUrl: existingImages.userImage }} />
								</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{us.formdata.stamp}</List.Header>
								<List.Description>
									<FileViewThumbnail deletable={false} el={{ fileUrl: existingImages.userStamp }} />
								</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{us.formdata.signature}</List.Header>
								<List.Description>
									<FileViewThumbnail deletable={false} el={{ fileUrl: existingImages.userSignature }} />
								</List.Description>
							</List.Item>
						</Grid.Column>
					</Grid.Row>
				)}
			</Grid>
		</List>
		// <Form loading={loading}>
		// 	<Form.Group widths="equal">
		// 		<Form.Field>
		// 			<EbpsForm
		// 				label={us.formdata.formdata1}
		// 				name="userName"
		// 				onChange={handleChange}
		// 				error={errors.userName}
		// 				value={values.userName}
		// 				setFieldValue={setFieldValue}
		// 			/>
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsForm
		// 				label={us.formdata.formdata2}
		// 				name="address"
		// 				onChange={handleChange}
		// 				error={errors.address}
		// 				value={values.address}
		// 				setFieldValue={setFieldValue}
		// 			/>
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsForm
		// 				label={us.formdata.formdata3}
		// 				name="mobile"
		// 				// disabled={true}
		// 				onChange={handleChange}
		// 				value={values.mobile}
		// 				error={errors.mobile}
		// 				setFieldValue={setFieldValue}
		// 			/>
		// 		</Form.Field>
		// 	</Form.Group>

		// 	<Form.Group widths="equal">
		// 		<Form.Field>
		// 			<EbpsNormalForm
		// 				label={us.formdata.formdata5}
		// 				name="email"
		// 				onChange={handleChange}
		// 				error={errors.email}
		// 				value={values.email}
		// 			/>
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsForm
		// 				label={us.formdata.formdata13}
		// 				name="educationQualification"
		// 				onChange={handleChange}
		// 				error={errors.educationQualification}
		// 				value={values.educationQualification}
		// 				setFieldValue={setFieldValue}
		// 			/>
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsNormalForm
		// 				label={us.formdata.formdata14}
		// 				name="licenseNo"
		// 				onChange={handleChange}
		// 				error={errors.licenseNo}
		// 				value={values.licenseNo || ''}
		// 			/>
		// 		</Form.Field>
		// 	</Form.Group>

		// 	<Form.Group widths="equal">
		// 		<Form.Field>
		// 			<EbpsNormalForm
		// 				label={us.formdata.formdata19}
		// 				name="loginId"
		// 				onChange={handleChange}
		// 				error={errors.loginId}
		// 				value={values.loginId}
		// 			/>
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsForm
		// 				label={us.formdata.formdata16}
		// 				name="municipalRegNo"
		// 				onChange={handleChange}
		// 				error={errors.municipalRegNo}
		// 				value={values.municipalRegNo}
		// 				setFieldValue={setFieldValue}
		// 			/>
		// 		</Form.Field>

		// 		<Form.Field error={!!errors.userType}>
		// 			{us.formdata.formdata10}
		// 			<SelectInput name="userType" options={userTypeOption} />
		// 		</Form.Field>
		// 	</Form.Group>
		// 	<Form.Group widths="three">
		// 		<Form.Field error={!!errors.status}>
		// 			{us.formdata.formdata18}
		// 			<SelectInput name="status" options={statusOption} />
		// 		</Form.Field>
		// 		<Form.Field>
		// 			<EbpsNormalForm
		// 				label={us.formdata.consultancyName}
		// 				name="consultancyName"
		// 				onChange={handleChange}
		// 				error={errors.consultancyName}
		// 				value={values.consultancyName || ''}
		// 			/>
		// 		</Form.Field>
		// 		{isEditing && (
		// 			<fieldset style={{ marginLeft: 7 }} className="borderlessFieldset" disabled={true}>
		// 				<Form.Field>
		// 					<LangDateField label={us.formdata.formdata11} name="joinDate" value={values.joinDate} />
		// 				</Form.Field>
		// 			</fieldset>
		// 		)}
		// 	</Form.Group>
		// 	<Form.Group widths="equal">
		// 		<FileInput existingImage={existingImages.userImage} name="userImage" label={us.formdata.formdata12} />
		// 		<FileInput existingImage={existingImages.userStamp} name="userStamp" label={us.formdata.formdata17} />
		// 	</Form.Group>
		// 	<Form.Group>
		// 		<FileInput existingImage={existingImages.userSignature} name="userSignature" label={us.formdata.formdata15} />
		// 	</Form.Group>
		// </Form>
	);
};
