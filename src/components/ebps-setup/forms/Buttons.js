import React from 'react';
import { Button } from 'semantic-ui-react';

export const EditButton = ({ handleEdit, row }) => {
	return (
		<Button
			inverted
			title="Edit"
			type="reset"
			color="green"
			onClick={() => {
				handleEdit({ ...row });
			}}
			icon="edit"
			size="tiny"
			style={{ marginBottom: '2px' }}
		></Button>
	);
};

export const DeleteButton = ({ handleDelete, row }) => {
	return (
		<Button
			inverted
			title="Delete"
			type="reset"
			color="red"
			icon="delete"
			onClick={() => {
				handleDelete(row);
			}}
			size="tiny"
		></Button>
	);
};
