import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
import JqxGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid/react_jqxgrid';
import RootHistoryGrid from '../../loggedInComponents/Grid/RootHistoryGrid';
import FallbackComponent from '../../shared/FallbackComponent';
import { UIState } from '../../../interfaces/ReduxInterfaces';
import { Button } from 'semantic-ui-react';

interface IState {
	id: string;
	pageable: boolean;
	groupable: boolean;
	showfilterrow: boolean;
}

interface IComponentProps {
	data: [];
	columns: [];
	datafield: [];
	buttonProps: {
		class?: string;
		title?: string;
		icon?: string;
	};
	handleButtonClick: (row: any) => string;
	actionWidth: number;
	needsAction?: boolean;
}

class GenericTableOneButton extends React.PureComponent<IDataTableProps & UIState & IComponentProps, IState> {
	private tableRef = React.createRef<JqxGrid>();

	constructor(props: IDataTableProps & UIState & IComponentProps) {
		super(props);

		this.state = {
			id: '',
			pageable: true,
			groupable: true,
			showfilterrow: true,
		};
	}

	renderButton = (row: any, htmlElement: HTMLElement) => {
		ReactDOM.render(
			<Button
				title={this.props.buttonProps.title}
				onClick={() => this.props.handleButtonClick(row)}
				size="tiny"
				className={this.props.buttonProps.class}
			>
				<i aria-hidden="true" className={this.props.buttonProps.icon}></i>
			</Button>,
			htmlElement
		);
	};

	actionColumns = [
		{
			text: 'Actions',
			cellsalign: 'center',
			align: 'center',
			createwidget: (row: any, column: any, value: string, htmlElement: HTMLElement) => {
				this.renderButton(row.boundindex, htmlElement);
			},
			initwidget: (row: any, column: any, value: string, htmlElement: HTMLElement) => {
				this.renderButton(row, htmlElement);
			},
			width: this.props.actionWidth,
			pinned: 'true',
		},
	].concat(this.props.columns);

	render() {
		const { data, datafield, needsAction = true as boolean } = this.props;

		if (data) {
			return (
				<React.Fragment>
					<div className="mapRegistration-dataTableList">
						{/* 
              				// @ts-ignore */}
						<RootHistoryGrid
							// Grid={this.tableRef}
							//@ts-ignore
							localdata={data}
							datafield={datafield}
							columns={needsAction ? this.actionColumns : this.props.columns}
							groups={false}
							// groupable={this.state.groupable}
							rowsheight={50}
							pageable={this.state.pageable}
							showfilterrow={this.state.showfilterrow}
							onRowselect={false}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

export default GenericTableOneButton;
