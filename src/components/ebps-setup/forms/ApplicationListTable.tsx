import * as React from 'react';
import { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
import JqxGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid/react_jqxgrid';
import RootHistoryGrid from '../../loggedInComponents/Grid/RootHistoryGrid';
import FallbackComponent from '../../shared/FallbackComponent';
import { UIState } from '../../../interfaces/ReduxInterfaces';
import { getUserTypeValueNepali } from '../../../utils/functionUtils';

interface IState {
	id: string;
	pageable: boolean;
	groupable: boolean;
	showfilterrow: boolean;
}

interface IComponentProps {
	applicationList: object;
	generateButtonRenderer: (row: any) => string;
}

class ApplicationListTable extends React.PureComponent<IDataTableProps & UIState & IComponentProps, IState> {
	private tableRef = React.createRef<JqxGrid>();

	constructor(props: IDataTableProps & UIState & IComponentProps) {
		super(props);

		this.state = {
			id: '',
			pageable: true,
			groupable: true,
			showfilterrow: true,
		};

		this.buttonrenderer = this.buttonrenderer.bind(this);
	}

	buttonrenderer = (row: any, columnfield: string, value: any, defaulthtml: any, columnproperties: any, rowdata: any) => {
		//@ts-ignore
		// console.log(this);

		//@ts-ignore
		return this.props.generateButtonRenderer(row);

		// '<button title="History" class="ui button tiny primary-btn show-history" data-row=' +
		// row +
		// '"><i aria-hidden="true" class="history icon"></i></button>'
		// );
	};

	// //@ts-ignore
	// componentDidUpdate(prevProps) {
	// 	if (prevProps.applicationList !== this.props.applicationList) {
	// 		const actionButtons = document.querySelectorAll('.change-designer');
	// 		if (actionButtons.length > 0) {
	// 			Array.from(actionButtons).forEach(actionButton => {
	// 				//@ts-ignore
	// 				const row: number = actionButton.dataset.row;
	// 				//@ts-ignore
	// 				actionButton.addEventListener('click', () => this.props.handleAction(this.props.applicationList[row]));
	// 			});
	// 		}
	// 	}
	// }

	cellClass = (row: any, dataField: any, cellText: any, rowData: any) => {
		const cellValue = rowData[dataField];
		if (cellValue === 'अस्वीकृत गरिएको') {
			return 'disapproved';
		} else if (cellValue === 'स्वीकृत भएको') {
			return 'approved';
		} else {
			return 'avg';
		}
	};

	datafield = [
		{ name: 'applicantNo', type: 'string' },
		{ name: 'applicantName', type: 'string' },
		{ name: 'applicantAddress', type: 'string' },
		{ name: 'nibedakName', type: 'string' },
		{ name: 'applicantMobileNo', type: 'string' },
		{ name: 'applicationStatus', type: 'string' },
		{ name: 'applicationActionBy', type: 'string' },
		{ name: 'yourStatus', type: 'string' },
		{ name: 'applicationAction', type: 'string' },
		{ name: 'applicantDate', type: 'string' },
		{ name: 'constructionType', type: 'string' },
		{ name: 'forwardTo', type: 'string' },
	];

	columns = [
		// {
		// 	buttonclick: (row: number): void => {
		// 		// get the data and append in to the inputs
		// 		const dataRecord = this.tableRef.current!.getrowdata(row);
		// 		console.log(dataRecord);

		// 		//@ts-ignore
		// 		this.props.handleAction(dataRecord);
		// 	},
		// 	cellsrenderer: (): string => {
		// 		return 'Change Designer';
		// 	},
		// 	columntype: 'button',
		// 	text: 'Action',
		// 	width: '140',
		// 	cellclassname: 'jqx-table-btn',
		// 	pinned: 'true',
		// },
		{ text: 'Action', cellsalign: 'center', align: 'center', cellsrenderer: this.buttonrenderer, width: 90, pinned: 'true' },
		{
			text: 'शाखा दर्ता नं.',
			dataField: 'applicantNo',
			cellsalign: 'center',
			align: 'center',
			width: '130',
			pinned: 'true',
		},
		{
			text: 'जग्गाधनीको नाम',
			dataField: 'applicantName',
			cellsalign: 'center',
			align: 'center',
			width: '200',
			pinned: 'true',
		},

		{
			text: 'जग्गाधनीको ठेगाना',
			dataField: 'applicantAddress',
			cellsalign: 'center',
			align: 'center',
			width: '200',
		},
		{
			text: 'निवेदकको नाम',
			dataField: 'nibedakName',
			cellsalign: 'center',
			align: 'center',
			width: '200',
		},
		{
			text: 'सम्पर्क फोन.',
			dataField: 'applicantMobileNo',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'तह',
			dataField: 'applicationActionBy',
			cellsalign: 'center',
			align: 'center',
			width: '120',
			filterType: 'list',
			cellclassname: 'alignment',
			cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any =>
				getUserTypeValueNepali(value),
		},
		{
			text: 'स्थिति फारम',
			dataField: 'applicationAction',
			cellsalign: 'center',
			align: 'center',
			width: '300',
			cellclassname: 'alignment',
			cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
		},
		{
			text: 'अवस्था',
			dataField: 'applicationStatus',
			cellsalign: 'center',
			align: 'center',
			width: '120',
			cellclassname: this.cellClass,
		},
		{
			text: 'दर्ता मिति',
			dataField: 'applicantDate',
			cellsalign: 'center',
			align: 'center',
			width: '100',
		},
		{
			text: 'भवनको किसिम',
			dataField: 'constructionType',
			cellsalign: 'center',
			align: 'center',
			width: '100',
			pinned: 'true',
			filterType: 'list',
		},
		// {
		// 	text: 'तह स्थिति',
		// 	dataField: 'forwardTo',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: '250',
		// },
		// {
		// 	text: 'पूर्ण अवस्था',
		// 	dataField: 'yourStatus',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: '120',
		// },
	];

	render() {
		//@ts-ignore
		const { applicationList } = this.props;

		if (applicationList) {
			return (
				<React.Fragment>
					<div className="mapRegistration-dataTableList">
						{/* 
              				// @ts-ignore */}
						<RootHistoryGrid
							//@ts-ignore
							Grid={this.tableRef}
							//@ts-ignore
							localdata={applicationList}
							datafield={this.datafield}
							columns={this.columns}
							groups={false}
							// groupable={this.state.groupable}
							rowsheight={40}
							pageable={this.state.pageable}
							// showfilterrow={this.state.showfilterrow}
							onRowselect={false}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

export default ApplicationListTable;