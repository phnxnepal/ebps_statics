import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
import RootHistoryGrid from '../../loggedInComponents/Grid/RootHistoryGrid';
import FallbackComponent from '../../shared/FallbackComponent';
import { UIState } from '../../../interfaces/ReduxInterfaces';
import { Button } from 'semantic-ui-react';

interface IState {
	id: string;
	pageable: boolean;
	groupable: boolean;
	showfilterrow: boolean;
}

interface IComponentProps {
	data: [];
	columns: [];
	datafield: [];
	buttonProps: [
		{
			class?: string;
			title?: string;
			icon?: string;
			visible?: boolean;
			handleButtonClick: (row: any) => string;
		}
	];
	actionWidth: number;
	needsAction?: boolean;
}

class GenericTableMultiButton extends React.PureComponent<IDataTableProps & UIState & IComponentProps, IState> {
	constructor(props: IDataTableProps & UIState & IComponentProps) {
		super(props);

		this.state = {
			id: '',
			pageable: true,
			groupable: true,
			showfilterrow: true,
		};
	}

	renderButton = (row: any, htmlElement: HTMLElement) => {
		const { buttonProps } = this.props;
		ReactDOM.render(
			<>
				{buttonProps.map((button) => {
					{
						return button.visible ? (
							<Button title={button.title} onClick={() => button.handleButtonClick(row)} size="tiny" className={button.class}>
								<i aria-hidden="true" className={button.icon}></i>
							</Button>
						) : null;
					}
				})}
			</>,
			htmlElement
		);
	};

	actionColumns = [
		{
			text: 'Actions',
			cellsalign: 'center',
			align: 'center',
			createwidget: (row: any, column: any, value: string, htmlElement: HTMLElement) => {
				this.renderButton(row.boundindex, htmlElement);
			},
			initwidget: (row: any, column: any, value: string, htmlElement: HTMLElement) => {
				this.renderButton(row, htmlElement);
			},
			width: this.props.actionWidth,
			pinned: 'true',
		},
	].concat(this.props.columns);

	render() {
		const { data, datafield, needsAction = true as boolean } = this.props;

		if (data) {
			return (
				<React.Fragment>
					<div className="mapRegistration-dataTableList">
						{/* 
              				// @ts-ignore */}
						<RootHistoryGrid
							// Grid={this.tableRef}
							//@ts-ignore
							localdata={data}
							datafield={datafield}
							columns={needsAction ? this.actionColumns : this.props.columns}
							groups={false}
							// groupable={this.state.groupable}
							rowsheight={50}
							pageable={this.state.pageable}
							showfilterrow={this.state.showfilterrow}
							onRowselect={false}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

export default GenericTableMultiButton;
