import * as React from 'react';
import { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
import JqxGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid/react_jqxgrid';
import RootHistoryGrid from '../../loggedInComponents/Grid/RootHistoryGrid';
import FallbackComponent from '../../shared/FallbackComponent';
import { UIState } from '../../../interfaces/ReduxInterfaces';

interface IState {
	id: string;
	pageable: boolean;
	groupable: boolean;
	showfilterrow: boolean;
}

interface IComponentProps {
	data: [];
	columns: [];
	datafield: [];
	generateButtonRenderer: (row: any) => string;
	actionWidth: number;
	needsAction?: boolean;
}

class GenericSetupTable extends React.PureComponent<IDataTableProps & UIState & IComponentProps, IState> {
	private tableRef = React.createRef<JqxGrid>();

	constructor(props: IDataTableProps & UIState & IComponentProps) {
		super(props);

		this.state = {
			id: '',
			pageable: true,
			groupable: true,
			showfilterrow: true,
		};

		this.buttonrenderer = this.buttonrenderer.bind(this);
	}

	buttonrenderer = (row: any, columnfield: string, value: any, defaulthtml: any, columnproperties: any, rowdata: any) => {
		//@ts-ignore
		return this.props.generateButtonRenderer(row);
	};

	actionColumns = [
		{ text: 'Actions', cellsalign: 'center', align: 'center', cellsrenderer: this.buttonrenderer, width: this.props.actionWidth, pinned: 'true' },
	].concat(this.props.columns);

	render() {
		
		const { data, datafield, needsAction = true as boolean } = this.props;

		if (data) {
			return (
				<React.Fragment>
					<div className="mapRegistration-dataTableList">
						{/* 
              				// @ts-ignore */}
						<RootHistoryGrid
							// Grid={this.tableRef}
							//@ts-ignore
							localdata={data}
							datafield={datafield}
							columns={needsAction ? this.actionColumns : this.props.columns}
							groups={false}
							// groupable={this.state.groupable}
							rowsheight={50}
							pageable={this.state.pageable}
							showfilterrow={this.state.showfilterrow}
							onRowselect={false}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

export default GenericSetupTable;
