import React, { useState, useEffect } from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { useFiscalYearAll } from '../../hooks/useFiscalYear';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { showToast, isEmpty } from '../../utils/functionUtils';
import { Tab, Button, Divider, Header, Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import { applicationTable, searchData } from '../../utils/data/genericSetupData';
import { namsariData, namsariColumns, namsariDatafield } from '../../utils/data/ebps-setup/namsariSetupData';
import * as Yup from 'yup';
import EbpsForm, { EbpsNormalForm } from '../shared/EbpsForm';
import { SelectInput } from '../shared/Select';
import { consTypeWithDefaultSelectOptions } from '../../utils/optionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import ErrorDisplay from '../shared/ErrorDisplay';
import ApplicationDetails from './shared/ApplicationDetails';
import { NamsariHistory } from './NamsariHistory';
import { getDataConstructionType } from '../../utils/enums/constructionType';
import Helmet from 'react-helmet';
import GenericTableOneButton from './forms/GenericTableOneButton';

// const changeDesignerSchema = Yup.object().shape({
// 	// designer: validateString,
// 	// file: validateFile,
// });

const applicationData = applicationTable;
const modalData = namsariData.modal;

const initialValues = {
	nibedakName: '',
	constructionType: '',
	year: '',
	kittaNo: '',
	wardNo: '',
	applicationNo: '',
};

const NamsariSetupComponent = ({ getAfterUpdateAdminData, postAdminDataByUrl, errors, success, loading, adminData }) => {
	const [appInfo, setAppInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [historyList, sethistoryList] = useState([]);
	const [isChanging, setIsChanging] = useState(false);
	const [dataChanged] = useState(false);

	const { fy, fyOption } = useFiscalYearAll(adminData.fiscalYear, 'yearCode', true);
	useJqxNepaliFilter(dataChanged, [2, 3, 4, 5]);

	useEffect(() => {
		if (success && success.success) {
			showToast('Data saved successfully');
			setOpenModal(false);
			setIsChanging(false);
			setAppInfo({});
		}
	}, [success]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	useEffect(() => {
		adminData.historyList &&
			sethistoryList(
				adminData.historyList.nameTransferHistory.map((el) => {
					return { ...el, constructionType: getDataConstructionType(el.constructionType) };
				})
			);
	}, [adminData.historyList]);

	const handleButtonClick = (id) => {
		const row = appList[id];
		handleConfirmation(row);
	};

	const handleConfirmation = (row) => {
		setAppInfo(row);
		setOpenModal(true);
	};

	const handleChangeDesigner = () => {
		setOpenModal(false);
		setIsChanging(true);
	};

	const handleClose = () => {
		setAppInfo({});
		setIsChanging(false);
		setOpenModal(false);
	};

	const panes = [
		{
			menuItem: namsariData.tab.setupHeading,
			render: () => (
				<Tab.Pane className="simple-tab-pane">
					<Helmet>
						<title>{namsariData.heading}</title>
					</Helmet>
					<Formik
						key="get-app-info"
						initialValues={{ ...initialValues, year: fy }}
						// validationSchema={searchSchema}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);
							try {
								setAppList([]);
								getAfterUpdateAdminData([
									{
										api: `${api.namsariSetup}?applocationNo=${values.applicationNo}&nibedakName=${values.nibedakName}&constructionType=${values.constructionType}&year=${values.year}&kittaNo=${values.kittaNo}&wardNo=${values.wardNo}`,
										objName: 'applicationList',
									},
								]);
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('err', err);
							}
						}}
					>
						{({ handleSubmit, errors, isSubmitting, values, setFieldValue, handleChange }) => (
							<Form loading={isSubmitting || loading}>
								<Form.Group widths="4">
									<Form.Field>
										<EbpsNormalForm
											name="applicationNo"
											label={applicationData.applicationId}
											onChange={handleChange}
											errors={errors.applicationNo}
											value={values.applicationNo}
										/>
									</Form.Field>
									<Form.Field>
										<EbpsForm
											name="nibedakName"
											label={applicationData.nibedakName}
											setFieldValue={setFieldValue}
											errors={errors.nibedakName}
											value={values.nibedakName}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput
											needsZIndex={true}
											name="constructionType"
											label={applicationData.constructionType}
											options={consTypeWithDefaultSelectOptions}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput needsZIndex={true} name="year" label={applicationData.year} options={fyOption} />
									</Form.Field>
								</Form.Group>
								<Form.Group widths="4">
									<Form.Field>
										<EbpsForm
											name="kittaNo"
											label={applicationData.kittaNo}
											setFieldValue={setFieldValue}
											errors={errors.kittaNo}
											value={values.kittaNo}
										/>
									</Form.Field>
									<Form.Field>
										<EbpsForm
											name="wardNo"
											label={applicationData.wardNo}
											setFieldValue={setFieldValue}
											errors={errors.wardNo}
											value={values.wardNo}
										/>
									</Form.Field>
								</Form.Group>
								<Form.Group widths="equal">
									<Form.Field>
										<Button
											type="button"
											className="primary-btn"
											icon="search"
											content={searchData.searchApp}
											onClick={handleSubmit}
										/>
									</Form.Field>
								</Form.Group>
							</Form>
						)}
					</Formik>
					<h4>{searchData.searchResults}</h4>
					<GenericTableOneButton
						data={appList}
						columns={namsariColumns}
						datafield={namsariDatafield}
						buttonProps={{
							class: 'primary-table-single-btn icon',
							icon: 'exchange icon',
							title: 'Send for Namsari',
						}}
						handleButtonClick={handleButtonClick}
						actionWidth={80}
					/>
					<br />
					{!isEmpty(appInfo) && (
						<div>
							<Divider />
							<Formik
								key="namsari"
								// validationSchema={changeDesignerSchema}
								onSubmit={(values, actions) => {
									actions.setSubmitting(true);

									try {
										postAdminDataByUrl(`${api.namsariSetup}/${appInfo.applicantNo}/${appInfo.time}`);
										actions.setSubmitting(false);
									} catch (err) {
										actions.setSubmitting(false);
										console.log('err', err);
									}
								}}
							>
								{({ handleSubmit, isSubmitting, errors: formErrors, validateForm }) => {
									const globalError =
										errors && errors.message === 'PNC' ? { message: namsariData.namsariErrors.namsariNotComplete } : errors;
									return (
										<Form>
											<AdminModalContainer
												key={'open-confirmation'}
												open={openModal}
												errors={globalError}
												isSubmitting={isSubmitting || loading}
												handleSubmit={handleChangeDesigner}
												handleClose={handleClose}
												title={modalData.title}
												saveText={modalData.saveText}
												cancelText={modalData.cancelText}
											>
												{modalData.content}
											</AdminModalContainer>

											<AdminModalContainer
												open={isChanging}
												errors={globalError}
												isSubmitting={isSubmitting || loading}
												handleSubmit={() => {
													validateForm().then((errors) => {
														if (isEmpty(errors)) {
															handleSubmit();
														}
													});
												}}
												handleClose={handleClose}
												title={modalData.title}
												saveText={modalData.confirmChange}
												cancelText={modalData.cancelText}
											>
												<Form loading={isSubmitting || loading}>
													<ApplicationDetails appInfo={appInfo} />
												</Form>
											</AdminModalContainer>
										</Form>
									);
								}}
							</Formik>
						</div>
					)}
				</Tab.Pane>
			),
		},
		{
			menuItem: namsariData.tab.historyHeading,
			render: () => (
				<Tab.Pane className="simple-tab-pane">
					<Helmet>
						<title>{namsariData.tab.historyHeading}</title>
					</Helmet>
					<NamsariHistory historyList={historyList} />
				</Tab.Pane>
			),
		},
	];

	return (
		<div className="setup-main">
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{namsariData.heading}</Header>
			<Tab menu={{ secondary: true, pointing: true }} panes={panes} />
		</div>
	);
};
export const NamsariSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{ api: api.namsariHistory, objName: 'historyList' },
		]}
		updateApi={[{ api: api.namsariHistory, objName: 'historyList' }]}
		render={(props) => <NamsariSetupComponent {...props} parentProps={parentProps} />}
	/>
);

export { NamsariSetup as default };
