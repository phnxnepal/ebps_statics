import React, { useState, useEffect } from 'react';
import { Dropdown, Grid, Icon } from 'semantic-ui-react';
import { setupTags, adminDropdownData as adminMenu } from '../../routes/AdminRoutes';

export const SetupDropdown = ({ handleSetupMenu, userMenu: userMenus }) => {
	const [formMenu, setFormMenu] = useState();
	const [userMenu, setUserMenu] = useState();
	const [publicMenu, setPublicMenu] = useState();
	const [otherMenu, setOtherMenu] = useState();
	const [needsSecondColumn, setNeedsSecondColumn] = useState(false);

	useEffect(() => {
		const userMenuUrls = userMenus.map(menu => menu.url);
		const requiredMenu = adminMenu.filter(menu => userMenuUrls.includes(`${menu.layout}${menu.path}`));
		let menuTagCount = 0;

		for (const tag of setupTags) {
			if (tag.tag === 'form' && requiredMenu.find(row => row.tag === 'form')) {
				setFormMenu(requiredMenu.filter(row => row.tag === 'form'));
				menuTagCount++;
			} else if (tag.tag === 'user' && requiredMenu.find(row => row.tag === 'user')) {
				setUserMenu(requiredMenu.filter(row => row.tag === 'user'));
				menuTagCount++;
			} else if (tag.tag === 'public' && requiredMenu.find(row => row.tag === 'public')) {
				menuTagCount++;
				setPublicMenu(requiredMenu.filter(row => row.tag === 'public'));
			} else if (tag.tag === 'other' && requiredMenu.find(row => row.tag === 'other')) {
				menuTagCount++;
				setOtherMenu(requiredMenu.filter(row => row.tag === 'other'));
			}
		}
		if (menuTagCount > 0) setNeedsSecondColumn(true);
	}, [userMenus]);

	return (
		<>
			{formMenu || needsSecondColumn ? (
				<Dropdown text="Setup" item direction="left">
					<Dropdown.Menu className={needsSecondColumn ? 'setup-dropdown' : 'setup-dropdown-one-column'}>
						<Grid padded>
							<Grid.Row columns={needsSecondColumn ? '2' : '1'}>
								{formMenu && (
									<Grid.Column>
										<Dropdown.Menu className="noScroll noTopBorder">
											<Dropdown.Header icon="file" content={setupTags[2].label} />
											{formMenu.map(option => (
												<Dropdown.Item key={option.id} id={option.id} onClick={handleSetupMenu}>
													{option.icon && (
														//@ts-ignore
														<Icon name={option.icon} color="black" />
													)}
													{option.name}
												</Dropdown.Item>
											))}
										</Dropdown.Menu>
									</Grid.Column>
								)}

								{needsSecondColumn && (
									<Grid.Column>
										<Grid.Row>
											{userMenu && (
												<Grid.Column>
													<Dropdown.Menu scrolling className="noTopBorder">
														<Dropdown.Header icon="user" content={setupTags[0].label} />
														{userMenu.map(option => (
															<Dropdown.Item key={option.id} id={option.id} onClick={handleSetupMenu}>
																{option.icon && (
																	//@ts-ignore
																	<Icon name={option.icon} color="black" />
																)}
																{option.name}
															</Dropdown.Item>
														))}
													</Dropdown.Menu>
												</Grid.Column>
											)}
										</Grid.Row>
										<Grid.Row>
											{publicMenu && (
												<Grid.Column>
													<Dropdown.Menu scrolling className="noTopBorder">
														<Dropdown.Header icon="tags" content={setupTags[1].label} />
														{publicMenu.map(option => (
															<Dropdown.Item key={option.id} id={option.id} onClick={handleSetupMenu}>
																{option.icon && (
																	//@ts-ignore
																	<Icon name={option.icon} color="black" />
																)}
																{option.name}
															</Dropdown.Item>
														))}
													</Dropdown.Menu>
												</Grid.Column>
											)}
										</Grid.Row>
										<Grid.Row>
											{otherMenu && (
												<Grid.Column>
													<Dropdown.Menu scrolling className="noTopBorder">
														<Dropdown.Header icon="tags" content={setupTags[3].label} />
														{otherMenu.map(option => (
															<Dropdown.Item key={option.id} id={option.id} onClick={handleSetupMenu}>
																{option.icon && (
																	//@ts-ignore
																	<Icon name={option.icon} color="black" />
																)}
																{option.name}
															</Dropdown.Item>
														))}
													</Dropdown.Menu>
												</Grid.Column>
											)}
										</Grid.Row>
									</Grid.Column>
								)}
							</Grid.Row>
							{/* <Grid.Row columns="1" className="noTopPadding">
						<Grid.Column>
							<Dropdown.Menu scrolling className="noTopBorder">
								<Dropdown.Header icon="tags" content={setupTags[3].label} />
								{adminDropdownData
									.filter(row => row.tag === 'other')
									.map(option => (
										<Dropdown.Item key={option.id} id={option.id} onClick={handleSetupMenu}>
											{option.icon && (
												//@ts-ignore
												<Icon name={option.icon} color="black" />
											)}
											{option.name}
										</Dropdown.Item>
									))}
							</Dropdown.Menu>
						</Grid.Column>
					</Grid.Row> */}
						</Grid>
					</Dropdown.Menu>
				</Dropdown>
			) : null}
		</>
	);
};
