import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Button, ButtonContent, Icon, Select, Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast } from '../../utils/functionUtils';
import { useUserSetup } from '../../hooks/useAdminSetup';
import { UserType } from '../../utils/userTypeUtils';
import { userdata, tableColumns, tableDatafield } from '../../utils/data/ebps-setup/userformdata';
import { AddUserForm, ViewUser } from './forms/AddUserForm';
import { userTypeDesignerOption, pendingOption } from '../../utils/optionUtils';
import { validateString, validateNullableNumber, validateNormalNepaliDateWithRange, validateFile } from '../../utils/validationUtils';
import { emailMessage, commonMessages } from '../../utils/data/validationData';
import { RenewDesignerForm } from './forms/RenewDesignerForm';
import { displayNepaliDate } from '../../utils/dateUtils';
import { useFiscalYear } from '../../hooks/useFiscalYear';
import Helmet from 'react-helmet';
import { FlexSingleRight } from '../uiComponents/FlexDivs';
import { isStringEmpty } from '../../utils/stringUtils';
import GenericTableMultiButton from './forms/GenericTableMultiButton';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';

const modalData = userdata.us_data.modal;
const us = userdata.us_data;

/**
 * Schema
 */

// const numData = ['joinDate'];
// const fileData = ['userStamp', 'userSignature', 'userImage'];
const strData = ['userName', 'userType', 'address', 'loginId', 'educationQualification', 'status'];
const phoneData = ['mobile', 'municipalRegNo'];

// const numSchema = numData.map(row => {
// 	return {
// 		[row]: validateString,
// 	};
// });

const strSchema = strData.map((rows) => {
	return {
		[rows]: validateString,
	};
});

const phoneDataSchema = phoneData.map((row) => {
	return {
		[row]: validateNullableNumber,
	};
});

const renewSchema = Yup.object().shape({
	tillDate: validateNormalNepaliDateWithRange,
	fiscalYear: validateString,
	charge: validateFile,
	billNo: validateString,
});

const UsersSchema = Yup.object().shape(
	Object.assign(
		{
			email: Yup.string().email(emailMessage.incorrect).required(commonMessages.required),
		},
		// ...numSchema,
		...strSchema,
		...phoneDataSchema
	)
);

const initialUser = { id: 0, userType: 'D', status: 'N' };

const DesignerSetupComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
	const {
		isEditing,
		isDeleting,
		isViewing,
		formState,
		openAddModal,
		handleAdd,
		handleEdit,
		handleView,
		handleClose,
		isRenewing,
		handleRenew,
		openRenewModal,
		renewState,
		latestRenew,
	} = useUserSetup(initialUser, {}, success);

	const [userData, setUserData] = useState([]);
	const [renewalData, setRenewalData] = useState([]);
	const [existingImages, setExistingImages] = useState({});
	const [needsRenew, setNeedsRenew] = useState(true);
	const [dataChanged] = useState(false);
	const [loginIdBeingAdded, setLoginIdBeingAdded] = useState(false);
	const [status, setStatus] = useState('A');
	const [approveModalOpen, setApproveModalOpen] = useState(false);
	const [approvingId, setApprovingId] = useState(false);

	const { fy, fyOption } = useFiscalYear(adminData.fiscalYear);

	useEffect(() => {
		if (success && success.success) {
			const addedDesigner = adminData.userData.find((row) => String(row.loginId).toLowerCase() === String(loginIdBeingAdded).toLowerCase());
			if (addedDesigner) {
				handleRenew(addedDesigner, adminData.renewalData);
				setLoginIdBeingAdded(undefined);
			}
		}
	}, [success, loginIdBeingAdded, adminData.userData, adminData.renewalData, handleRenew]);

	useEffect(() => {
		setNeedsRenew(adminData.organization[0] && adminData.organization[0].designerLoginType === 'private');
	}, [adminData.organization]);

	useEffect(() => {
		if (success && success.success && approveModalOpen) {
			setApproveModalOpen(false);
		}
	}, [success, approveModalOpen]);

	// useEffect(() => {
	// 	const designers = adminData.userData.filter((user) => user.userType.id === UserType.DESIGNER);
	// 	if (designers) {
	// 		setUserData(
	// 			designers.map((row) => {
	// 				return { ...row, joinDateBS: displayNepaliDate(row.joinDateBS), validDateBS: displayNepaliDate(row.validDateBS) };
	// 			})
	// 		);
	// 	}
	// }, [adminData.userData]);

	useEffect(() => {
		setRenewalData(adminData.renewalData);
	}, [adminData.renewalData]);

	useEffect(() => {
		if (status === 'P') {
			setUserData(adminData.pendingDesigners);
		} else {
			const designers = adminData.userData.filter((user) => user.userType.id === UserType.DESIGNER);
			if (designers) {
				setUserData(
					designers.map((row) => {
						return { ...row, joinDateBS: displayNepaliDate(row.joinDateBS), validDateBS: displayNepaliDate(row.validDateBS) };
					})
				);
			}
		}
	}, [status, adminData.userData, adminData.pendingDesigners]);

	useEffect(() => {
		setExistingImages({
			userImage: formState.photo,
			userStamp: formState.stamp,
			userSignature: formState.signature,
		});
	}, [formState]);

	useJqxNepaliFilter(dataChanged, [1, 2, 4, 8]);

	// useEffect(() => {
	// 	const filterFields = document.querySelectorAll('.jqx-filter-input');
	// 	if (filterFields.length > 0) {
	// 		Array.from(filterFields).forEach((field, index) => {
	// 			if (index === 0) {
	// 				field.style.display = 'none';
	// 			} else if ([1, 2, 4, 8].includes(index)) {
	// 				field.addEventListener('input', (e) => {
	// 					e.target.value = translate(e, getInputType());
	// 				});
	// 			}
	// 		});
	// 	}

	// 	const viewButtons = document.querySelectorAll('.view-button');
	// 	if (viewButtons.length > 0) {
	// 		Array.from(viewButtons).forEach((button) => {
	// 			const row = button.dataset.row;
	// 			const designer = userData[row];
	// 			button.addEventListener('click', () => handleView({ ...designer, userType: UserType.DESIGNER }));
	// 		});
	// 	}

	// 	const editButtons = document.querySelectorAll('.edit-button');
	// 	if (editButtons.length > 0) {
	// 		Array.from(editButtons).forEach((button) => {
	// 			const row = button.dataset.row;
	// 			const designer = userData[row];
	// 			button.addEventListener('click', () => handleEdit({ ...designer, userType: UserType.DESIGNER }));
	// 		});
	// 	}

	// 	const renewButtons = document.querySelectorAll('.renew-button');
	// 	if (renewButtons.length > 0) {
	// 		Array.from(renewButtons).forEach((button) => {
	// 			const row = button.dataset.row;
	// 			const designer = userData[row];
	// 			button.addEventListener('click', () => handleRenew({ ...designer }, renewalData));
	// 		});
	// 	}

	// 	const approveButtons = document.querySelectorAll('.approve-button');
	// 	if (approveButtons.length > 0) {
	// 		Array.from(approveButtons).forEach((button) => {
	// 			const row = button.dataset.row;
	// 			const designer = userData[row];
	// 			button.addEventListener('click', () => handleApprove(designer.id));
	// 		});
	// 	}

	// 	return () => {
	// 		const viewButtons = document.querySelectorAll('.view-button');
	// 		if (viewButtons.length > 0) {
	// 			Array.from(viewButtons).forEach((button) => {
	// 				const row = button.dataset.row;
	// 				const designer = userData[row];
	// 				button.removeEventListener('click', () => handleView({ ...designer, userType: UserType.DESIGNER }));
	// 			});
	// 		}

	// 		const editButtons = document.querySelectorAll('.edit-button');
	// 		if (editButtons.length > 0) {
	// 			Array.from(editButtons).forEach((button) => {
	// 				const row = button.dataset.row;
	// 				const designer = userData[row];
	// 				button.removeEventListener('click', () => handleEdit({ ...designer, userType: UserType.DESIGNER }));
	// 			});
	// 		}

	// 		const renewButtons = document.querySelectorAll('.renew-button');
	// 		if (renewButtons.length > 0) {
	// 			Array.from(renewButtons).forEach((button) => {
	// 				const row = button.dataset.row;
	// 				const designer = userData[row];
	// 				button.removeEventListener('click', () => handleRenew({ ...designer }, renewalData));
	// 			});
	// 		}

	// 		const approveButtons = document.querySelectorAll('.approve-button');
	// 		if (approveButtons.length > 0) {
	// 			Array.from(approveButtons).forEach((button) => {
	// 				const row = button.dataset.row;
	// 				const designer = userData[row];
	// 				button.removeEventListener('click', () => handleApprove(designer.id));
	// 			});
	// 		}
	// 	};
	// }, [userData, handleEdit, handleRenew, handleView, renewalData, dataChanged, status]);

	const handleApprove = (id) => {
		setApprovingId(id);
		setApproveModalOpen(true);
	};

	// const generateButtonRenderer = (row) => {
	// 	setDataChanged(!dataChanged);
	// 	if (status === 'P') {
	// 		return (
	// 			'<button title="View" class="ui primary-table-three-btn tiny icon button view-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="eye icon"></i></button><button title="Edit" class="ui primary-table-three-btn tiny icon button edit-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="edit icon"></i></button><button title="Approve" class="ui primary-table-three-btn tiny icon button approve-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="check icon"></i></button>'
	// 		);
	// 	}

	// 	if (needsRenew) {
	// 		return (
	// 			'<button title="View" class="ui primary-table-three-btn tiny icon button view-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="eye icon"></i></button><button title="Edit" class="ui primary-table-three-btn tiny icon button edit-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="edit icon"></i></button><button title="Renew" class="ui primary-table-three-btn tiny icon button renew-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="calendar plus outline icon"></i></button>'
	// 		);
	// 	} else {
	// 		return (
	// 			'<button title="View" class="ui primary-table-btn tiny icon button view-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="eye icon"></i></button><button title="Edit" class="ui primary-table-btn tiny icon button edit-button" data-row=' +
	// 			row +
	// 			'><i aria-hidden="true" class="edit icon"></i></button>'
	// 		);
	// 	}
	// };

	return (
		<div>
			<Helmet>
				<title>{modalData.titleAdd}</title>
			</Helmet>
			<Formik
				enableReinitialize
				validationSchema={isRenewing ? renewSchema : approveModalOpen ? null : UsersSchema}
				initialValues={{ ...formState, fiscalYear: fy, billNo: '' }}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);

					const data = new FormData();
					const selectedSignFile = values.userSignature;
					const selectedStampFile = values.userStamp;
					const selectedFile = values.userImage;

					delete values.imagePreviewUrl_userImage;
					delete values.imagePreviewUrl_userSignature;
					delete values.imagePreviewUrl_userStamp;

					if (selectedFile) {
						selectedFile.forEach((file) => data.append('userImage', file));
					} else {
						data.append('userImage', null);
					}
					if (selectedSignFile) {
						selectedSignFile.forEach((file) => data.append('userSignature', file));
					} else {
						data.append('userSignature', null);
					}
					if (selectedStampFile) {
						selectedStampFile.forEach((file) => data.append('userStamp', file));
					} else {
						data.append('userStamp', null);
					}

					Object.keys(values).forEach((key) => {
						if (!['approveBy', 'approveDate', 'joinDate'].includes(key)) {
							data.append(key, values[key]);
						}
					});

					try {
						if (status === 'P') {
							if (isEditing) {
								postAdminDataByUrl(`${api.userRequest}/${formState.id}`, data);
								actions.setSubmitting(false);
							} else if (approveModalOpen && !isStringEmpty(approvingId)) {
								putAdminDataByUrl(api.userRequest, approvingId);
							}
						} else {
							if (isEditing) {
								postAdminDataByUrl(`${api.organizationUser}${formState.id}`, data);
								actions.setSubmitting(false);
							} else if (isDeleting) {
								deleteAdminDataByUrl(api.classGroup, formState.id);
								// setOpenDeleteModal(false);
								actions.setSubmitting(false);
							} else if (isRenewing) {
								const renewData = new FormData();
								const voucher = values.charge;

								if (voucher) {
									voucher.forEach((fl) => renewData.append('file', fl));
								}

								renewData.append('tillDate', values.tillDate);
								renewData.append('fiscalYear', values.fiscalYear);
								renewData.append('billNo', values.billNo);
								renewData.append('id', formState.id);
								postAdminDataByUrl(`${api.renewDesigner}`, renewData);
								actions.setSubmitting(false);
							} else {
								postAdminDataByUrl(api.organizationUser, data);
								// setRenewAfterAdd(values);
								// // /**
								// //  * @TODO Make better for cases when there is an error in add
								// //  */
								// setTimeout(() => {
								// 	// if (errors && errors.message) {
								actions.setSubmitting(false);
								// }
								// }, 3000);
							}
						}
					} catch (error) {
						console.log('Error in Designer post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting, errors: formErrors, handleChange, setFieldValue, values }) => (
					<>
						<AdminModalContainer
							// key={'user-edit'}
							open={openAddModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={isViewing ? handleClose : handleSubmit}
							handleClose={handleClose}
							title={isEditing ? modalData.titleEdit : isViewing ? modalData.titleView : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : isViewing ? modalData.viewText : modalData.saveText}
							cancelText={modalData.cancelText}
						>
							{isViewing ? (
								<ViewUser labelData={us} values={values} userTypeOption={userTypeDesignerOption} existingImages={existingImages} />
							) : (
								<AddUserForm
									loading={isSubmitting || loading}
									labelData={us}
									errors={formErrors}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									values={values}
									userTypeOption={userTypeDesignerOption}
									existingImages={existingImages}
									isEditing={isEditing}
									setLoginId={setLoginIdBeingAdded}
								/>
							)}
						</AdminModalContainer>
						<AdminModalContainer
							// key={'designer-renew'}
							open={openRenewModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={handleClose}
							title={modalData.titleRenew}
							saveText={modalData.renewText}
							cancelText={modalData.cancelText}
						>
							<RenewDesignerForm
								loading={isSubmitting || loading}
								errors={formErrors}
								handleChange={handleChange}
								setFieldValue={setFieldValue}
								values={values}
								existingImages={existingImages}
								fiscalYearOptions={fyOption}
								renewData={renewState}
								currentUser={formState}
								latestRenew={latestRenew}
							/>
						</AdminModalContainer>
						<AdminModalContainer
							// key={'user-approve'}
							open={approveModalOpen}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setApproveModalOpen(false)}
							title={modalData.titleApprove}
							saveText={modalData.approveText}
							cancelText={modalData.cancelText}
						>
							<Form loading={isSubmitting || loading}>{modalData.approveBody}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
			<Segment inverted className="tableSectionHeader">
				{us.tableTitle}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon name="add user" />
						{us.modal.titleAdd}
					</ButtonContent>
				</Button>
			</Segment>
			{!needsRenew && (
				<FlexSingleRight>
					<div className="zIndexSelect compact">
						<span>Designer Status: </span>
						<Select options={pendingOption} value={status} name="status" onChange={(e, { value }) => setStatus(value)} />
					</div>
				</FlexSingleRight>
			)}
			<br />
			<GenericTableMultiButton
				data={userData}
				columns={tableColumns}
				datafield={tableDatafield}
				buttonProps={[
					{
						visible: true,
						title: 'View',
						class: `${needsRenew || status === 'P' ? 'primary-table-three-btn' : 'primary-table-two-btn'} icon`,
						icon: 'eye icon',
						handleButtonClick: (row) => {
							const designer = userData[row];
							handleView({ ...designer, userType: UserType.DESIGNER });
						},
					},
					{
						visible: true,
						title: 'Edit',
						class: `${needsRenew || status === 'P' ? 'primary-table-three-btn' : 'primary-table-two-btn'} icon`,
						icon: 'edit icon',
						handleButtonClick: (row) => {
							const designer = userData[row];
							handleEdit({ ...designer, userType: UserType.DESIGNER });
						},
					},
					{
						visible: needsRenew,
						title: 'Renew',
						class: 'primary-table-three-btn icon',
						icon: 'calendar plus outline icon',
						handleButtonClick: (row) => {
							const designer = userData[row];
							handleRenew({ ...designer }, renewalData);
						},
					},
					{
						visible: status === 'P',
						title: 'Approve',
						class: 'success-table-three-btn icon',
						icon: 'check icon',
						handleButtonClick: (row) => {
							const designer = userData[row];
							handleApprove(designer.id);
						},
					},
				]}
				actionWidth={130}
				needsAction={true}
			/>
		</div>
	);
};

export const DesignerSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.organizationUser, objName: 'userData' },
			{ api: api.userRequest, objName: 'pendingDesigners' },
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{ api: api.designerRenewStatus, objName: 'renewalData' },
			{ api: api.organizationMaster, objName: 'organization' },
		]}
		updateApi={[
			{ api: api.organizationUser, objName: 'userData' },
			{ api: api.designerRenewStatus, objName: 'renewalData' },
			{ api: api.userRequest, objName: 'pendingDesigners' },
		]}
		render={(props) => <DesignerSetupComponent {...props} parentProps={parentProps} />}
	/>
);
