import React, { useState, useEffect } from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { showToast, isEmpty } from '../../utils/functionUtils';
import { Divider, Header, Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import { namsariColumns, namsariDatafield } from '../../utils/data/ebps-setup/namsariSetupData';
import { discardData } from '../../utils/data/ebps-setup/discardSetupData';
import { myadData } from '../../utils/data/ebps-setup/myadThapData';
import { AdminModalContainer } from './AdminModalContainer';
import ErrorDisplay from '../shared/ErrorDisplay';
import Helmet from 'react-helmet';
import GenericTableOneButton from './forms/GenericTableOneButton';

const modalData = discardData.modal;
const MyadApproveSetupComponent = ({ postAdminDataByUrl, errors, success, loading, adminData }) => {
	const [appInfo, setAppInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [dataChanged] = useState(false);

	useJqxNepaliFilter(dataChanged, [2, 3, 4]);

	useEffect(() => {
		if (success && success.success) {
			showToast('Data saved successfully');
			setOpenModal(false);
			setAppInfo({});
		}
	}, [success]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	const handleConfirmation = (row) => {
		setAppInfo(row);
		setOpenModal(true);
	};
	const handleClose = () => {
		setAppInfo({});
		setOpenModal(false);
	};
	return (
		<div className="setup-main">
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{myadData.myadApproveData.heading}</Header>
			<>
				<Helmet title={myadData.myadApproveData.heading}></Helmet>
				<GenericTableOneButton
					data={appList}
					columns={namsariColumns}
					datafield={namsariDatafield}
					handleButtonClick={(row) => handleConfirmation(appList[row])}
					actionWidth={80}
					buttonProps={{ class: 'success-table-single-btn icon', title: 'म्याद थप्न स्वीकृत लागि पठाउनुहोस्', icon: 'check icon' }}
				/>
				<br />
				{!isEmpty(appInfo) && (
					<div>
						<Divider />
						<Formik
							key="assign-designer"
							onSubmit={(values, actions) => {
								actions.setSubmitting(true);

								try {
									postAdminDataByUrl(`${api.myadApproveSetup}/${appInfo.applicantNo}`);
									actions.setSubmitting(false);
								} catch (err) {
									actions.setSubmitting(false);
								}
							}}
						>
							{({ handleSubmit, isSubmitting, validateForm }) => {
								return (
									<Form>
										<AdminModalContainer
											key={'open-confirmation'}
											open={openModal}
											// errors={errors}
											isSubmitting={isSubmitting || loading}
											handleSubmit={() => {
												validateForm().then((errors) => {
													if (isEmpty(errors)) {
														handleSubmit();
													}
												});
											}}
											handleClose={handleClose}
											title={myadData.myadApproveData.title}
											saveText={modalData.saveText}
											cancelText={modalData.cancelText}
										>
											{myadData.myadApproveData.content}
										</AdminModalContainer>
									</Form>
								);
							}}
						</Formik>
					</div>
				)}
			</>
		</div>
	);
};
export const MyadApproveSetup = (parentProps) => (
	<AdminContainer
		api={[{ api: api.myadApproveSetup, objName: 'applicationList' }]}
		updateApi={[{ api: api.myadApproveSetup, objName: 'applicationList' }]}
		render={(props) => <MyadApproveSetupComponent {...props} parentProps={parentProps} />}
	/>
);

export { MyadApproveSetup as default };
