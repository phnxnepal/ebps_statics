import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import ErrorDisplay from '../shared/ErrorDisplay';

export const AdminModalContainer = ({ handleClose, open, title, saveText, cancelText = 'Cancel', handleSubmit, errors, isSubmitting, children, key, scroll = true }) => {
	return (
		<div>
			<Modal key={key} closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{title}</Modal.Header>
				<Modal.Content scrolling={scroll ? true : undefined}>
					<>
						{errors && <ErrorDisplay message={errors.message} />}
						{children}
					</>
				</Modal.Content>
				<Modal.Actions>
					<Button disabled={isSubmitting} negative onClick={handleClose}>
						{cancelText}
					</Button>
					<Button
						disabled={isSubmitting}
						positive
						icon="checkmark"
						labelPosition="right"
						type="button"
						content={saveText || 'Save'}
						onClick={e => {
							handleSubmit();
						}}
					/>
				</Modal.Actions>
			</Modal>
		</div>
	);
};

export const AdminInfoModalContainer = ({ handleClose, open, title, saveText, isSubmitting, children, key }) => {
	return (
		<div>
			<Modal key={key} closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{title}</Modal.Header>
				<Modal.Content scrolling>
					<>
						{children}
					</>
				</Modal.Content>
				<Modal.Actions>
					<Button
						disabled={isSubmitting}
						positive
						icon="checkmark"
						labelPosition="right"
						content={saveText || 'Ok'}
						onClick={handleClose}
					/>
				</Modal.Actions>
			</Modal>
		</div>
	);
};