import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Button, Divider, Segment, Table, Select, Label } from 'semantic-ui-react';
import { userdata } from '../../utils/data/ebps-setup/userformdata';
import { getUsers, postUsers, putUsers, deleteUsers } from '../../store/actions/AdminAction';
import { connect } from 'react-redux';
import FallbackComponent from '../shared/FallbackComponent';
import { showToast } from '../../utils/functionUtils';
import * as Yup from 'yup';
import { validateString, validateNullableNumber } from '../../utils/validationUtils';
import { EbpsNormalForm } from '../shared/EbpsForm';
import { FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import ErrorDisplay from '../shared/ErrorDisplay';
import EbpsForm from './../shared/EbpsForm';
import { FileViewThumbnail } from '../shared/file/FileView';
import Helmet from 'react-helmet';
import api from '../../utils/api';
import { getAdminFilteredOptions, UserType } from '../../utils/userTypeUtils';
import { getAdminDataByUrl } from './../../store/actions/AdminAction';

const us = userdata.us_data;
// const opt = [
// 	{ key: 'a', value: 'R', text: 'राजस्व' },
// 	{ key: 'b', value: 'B', text: 'सव इन्न्जिनियर' },
// 	{ key: 'c', value: 'A', text: 'इन्न्जिनियर' },
// 	{ key: 'd', value: 'C', text: 'प्रमुख प्रशसकीय अधिकृत' },
// 	// { key: 'e', value: 'D', text: 'डिजाइनर' },
// 	{ key: 'f', value: 'AD', text: 'अमिन' },
// ];

const opt2 = [
	{ key: 'a', value: 'Y', text: 'Yes' },
	{ key: 'b', value: 'N', text: 'No' },
];
// const handleClose = () => {
//     onClose();
//     setError('');
// };

const filterFields = ['userStamp', 'userSignature', 'userImage'];
class AddUser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentUser: {
				id: 0,
				token: '',
				userName: '',
				address: '',
				mobile: '',
				phone: '',
				email: '',
				dbPassword: '',
				photo: '',
				educationQualification: '',
				licenseNo: '',
				userStamp: '',
				loginId: '',
				joinDate: '',
				userType: '',
				userSignature: '',
				municipalRegNo: '',
				status: '',
			},
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
			userTypeOptions: [],
		};
	}
	modalOpen = (id) => {
		this.setState({ isModalOpen: true, idToDelete: id });
	};

	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	componentDidMount() {
		this.props.getUsers();
		this.props.getAdminDataByUrl([
			{ api: `${api.userType}`, objName: 'userTypeMaster' },
		]);
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			// this.props.getUsers();
			setTimeout(() => {
				this.props.getUsers();
			}, 0);
			// console.log('updates', prevProps, this.props.success);
		}
		if (prevProps.userTypeMaster !== this.props.userTypeMaster && this.props.userTypeMaster) {
			this.setState({
				userTypeOptions: getAdminFilteredOptions(this.props.userTypeMaster).filter(user => user.value !== UserType.DESIGNER)
			});
		}
	}
	handleEdit = (User) => {
		window.scrollTo(0, 0);
		this.setState({
			currentUser: {
				userName: User.userName,
				address: User.address,
				mobile: User.mobile,
				phone: User.phone,
				email: User.email,
				dbPassword: User.dbPassword,
				photo: User.photo,
				educationQualification: User.educationQualification,
				licenseNo: User.licenseNo,
				userStamp: User.stamp,
				loginId: User.loginId,
				joinDate: User.joinDate,
				userType: User.userType.id,
				userSignature: User.signature,
				municipalRegNo: User.municipalRegNo,
				status: User.status,
				id: User.id,
			},
			isEditing: true,
		});
	};
	handleDelete = async (id) => {
		try {
			await this.props.deleteUsers(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};
	render() {
		// const numData = ['joinDate'];
		const strData = ['userName', 'userType', 'address', 'loginId', 'educationQualification', 'status'];
		// const fileData = ['userStamp', 'userSignature', 'userImage'];
		// const fileRequiredData = ['userImage'];
		// const normalNumData = ['municipalRegNo'];
		const phoneData = ['mobile', 'municipalRegNo'];
		// const numSchema = numData.map(row => {
		// 	return {
		// 		[row]: validateString,
		// 	};
		// });
		const strSchema = strData.map((rows) => {
			return {
				[rows]: validateString,
			};
		});
		// const fileSchema = fileData.map(row => {
		//     return {
		//         // [row]: validateFile
		//         [row]: validateFileOptional
		//     };
		// });
		// const fileRequiredSchema = fileRequiredData.map(row => {
		// 	return {
		// 		[row]: validateFile,
		// 	};
		// });
		// const normalNumSchema = normalNumData.map(row => {
		//     return {
		//         [row]: validateNullableOfficialReqNumbers
		//     };
		// });
		const phoneDataSchema = phoneData.map((row) => {
			return {
				[row]: validateNullableNumber,
			};
		});
		const UsersSchema = Yup.object().shape(
			Object.assign(
				{
					email: Yup.string()
						.email('इमेल गलत भयो')
						.required('यो भर्नु आवश्यक छ'),
					// }, ...numSchema, ...strSchema, ...phoneDataSchema, ...fileRequiredSchema));
				},
				// ...numSchema,
				...strSchema,
				...phoneDataSchema
			)
		);

		const { userData, postUsers, putUsers } = this.props;
		const { isEditing, currentUser, userTypeOptions } = this.state;
		// console.log('otherdata', otherData);

		// const { fileCatId, hasMultipleFiles } = getFileCatId(
		//     // this.props.otherData.fileCategories,
		//     this.props.parentProps.location.pathname
		// );
		if (userData) {
			return (
				<>
					<Helmet>
						<title>{us.heading.heading1}</title>
					</Helmet>
					{this.props.errors && <ErrorDisplay message={this.props.errors.message}></ErrorDisplay>}
					<Formik
						enableReinitialize
						initialValues={{ ...this.state.currentUser }}
						validationSchema={UsersSchema}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);
							values.error && delete values.error;
							values.id && delete values.id;
							const data = new FormData();
							const selectedSignFile = values.userSignature;
							const selectedStampFile = values.userStamp;
							const selectedFile = values.userImage;
							// let isFileSelected = false;
							if (selectedSignFile) {
								for (let x = 0; x < selectedSignFile.length; x++) {
									data.append('userSignature', selectedSignFile[x]);
								}
								// isFileSelected = true;
							} else {
								data.append('userSignature', null);
							}
							if (selectedStampFile) {
								for (let x = 0; x < selectedStampFile.length; x++) {
									data.append('userStamp', selectedStampFile[x]);
								}
								// isFileSelected = true;
							} else {
								data.append('userStamp', null);
							}
							if (selectedFile) {
								for (let x = 0; x < selectedFile.length; x++) {
									data.append('userImage', selectedFile[x]);
								}
								// isFileSelected = true;
							} else {
								data.append('userImage', null);
							}
							Object.keys(values).forEach((key) => {
								if (!filterFields.includes(key)) data.append(key, values[key]);
							});
							// console.log(values);
							try {
								if (this.state.isEditing) {
									await putUsers(this.state.currentUser.id, data);
								} else {
									// console.log('data', data);
									await postUsers(data);
								}
								actions.setSubmitting(false);
								window.scrollTo(0, 0);
								if (this.props.success && this.props.success.success) {
									// showToast('Your data has been successfully');
									if (isEditing) {
										showToast('The user details has been updated.');
									} else {
										showToast('Please Check Your email for password');
									}
								}
							} catch (err) {
								actions.setSubmitting(false);
								window.scrollTo(0, 0);
								showToast('Something went wrong !!');
							}
						}}
					>
						{({ handleSubmit, isSubmitting, setFieldValue, values, handleChange, errors, touched }) => {
							return (
								<Form loading={this.props.loading || isSubmitting}>
									<Segment>
										<Segment inverted className="tableSectionHeader">
											{us.heading.heading1}
										</Segment>
										<Form.Group widths="equal">
											<Form.Field>
												<EbpsForm
													label={us.formdata.formdata1}
													name="userName"
													onChange={handleChange}
													error={errors.userName}
													value={values.userName}
													setFieldValue={setFieldValue}
												/>
											</Form.Field>
											<Form.Field>
												<EbpsForm
													label={us.formdata.formdata2}
													name="address"
													onChange={handleChange}
													error={errors.address}
													value={values.address}
													setFieldValue={setFieldValue}
												/>
											</Form.Field>
											<Form.Field>
												<EbpsForm
													label={us.formdata.formdata3}
													name="mobile"
													onChange={handleChange}
													error={errors.mobile}
													value={values.mobile}
													setFieldValue={setFieldValue}
												/>
											</Form.Field>
											<Form.Field error={!!errors.email} className="english-div">
												<EbpsNormalForm
													label={us.formdata.formdata5}
													name="email"
													onChange={handleChange}
													error={errors.email}
													value={values.email}
												/>
											</Form.Field>
										</Form.Group>
										<Form.Group widths="equal">
											<Form.Field className="english-div">
												<EbpsNormalForm
													label={us.formdata.formdata19}
													name="loginId"
													readOnly={isEditing}
													onChange={isEditing ? undefined : handleChange}
													error={errors.loginId}
													value={values.loginId}
												/>
											</Form.Field>
											{/* <Form.Field>
												<EbpsNormalForm
													label={us.formdata.formdata6}
													name="dbPassword"
													onChange={handleChange}
													error={errors.dbPassword}
													value={values.dbPassword}
												/>
											</Form.Field> */}
											<Form.Field>
												<EbpsNormalForm
													label={us.formdata.eduQualification}
													name="educationQualification"
													onChange={handleChange}
													error={errors.educationQualification}
													value={values.educationQualification}
												/>
												{/* <EbpsForm
													label={us.formdata.formdata13}
													name="educationQualification"
													onChange={handleChange}
													error={errors.educationQualification}
													value={values.educationQualification}
													setFieldValue={setFieldValue}
												/> */}
											</Form.Field>
											<Form.Field>
												<EbpsNormalForm
													label={us.formdata.formdata14}
													name="licenseNo"
													onChange={handleChange}
													error={errors.licenseNo}
													value={values.licenseNo}
												/>
											</Form.Field>
											<Form.Field error={!!errors.userType}>
												{us.formdata.formdata10}
												<br />
												<Select
													value={values.userType}
													name="userType"
													options={userTypeOptions}
													onChange={(e, { value }) => {
														setFieldValue('userType', value);
													}}
												/>
												{errors.userType && (
													<Label pointing prompt size="large">
														{errors.userType}
													</Label>
												)}
											</Form.Field>
										</Form.Group>
										<Form.Group widths="four">
											{/* <Form.Field>
												<LangDateField
													label={<strong>{us.formdata.formdata11}</strong>}
													name="joinDate"
													handleChange={handleChange}
													error={errors.joinDate}
													value={values.joinDate}
													setFieldValue={setFieldValue}
												/>
											</Form.Field> */}
											<Form.Field>
												<EbpsForm
													label={us.formdata.formdata16}
													name="municipalRegNo"
													onChange={handleChange}
													error={errors.municipalRegNo}
													value={values.municipalRegNo}
													setFieldValue={setFieldValue}
												/>
											</Form.Field>
											<Form.Field error={!!errors.status}>
												{us.formdata.formdata18}
												<br />
												<Select
													fluid
													name="status"
													value={values.status}
													options={opt2}
													onChange={(e, { value }) => {
														setFieldValue('status', value);
													}}
												></Select>
												{errors.status && (
													<Label pointing prompt size="large">
														{errors.status}
													</Label>
												)}
											</Form.Field>
										</Form.Group>
										<Form.Group widths="three">
											<Form.Field>
												<label>{us.formdata.formdata15}</label>
												<FileInputWithMultiplePreview
													name="userSignature"
													fileCatId={'userSignature'}
													hasMultipleFiles={false}
												/>
												{currentUser.userSignature && (
													<div>
														<div>Existing File</div>
														<FileViewThumbnail deletable={false} el={{ fileUrl: currentUser.userSignature }} />
													</div>
												)}
											</Form.Field>
											<Form.Field>
												<label>{us.formdata.formdata17}</label>
												<FileInputWithMultiplePreview name="userStamp" fileCatId={'userStamp'} hasMultipleFiles={false} />
												{currentUser.userStamp && (
													<div>
														<div>Existing File</div>
														<FileViewThumbnail deletable={false} el={{ fileUrl: currentUser.userStamp }} />
													</div>
												)}
											</Form.Field>
											<Form.Field>
												<label>{us.formdata.formdata12}</label>
												<FileInputWithMultiplePreview name="userImage" fileCatId={'userImage'} hasMultipleFiles={false} />
												{currentUser.photo && (
													<div>
														<div>Existing File</div>
														<FileViewThumbnail deletable={false} el={{ fileUrl: currentUser.photo }} />
													</div>
												)}
											</Form.Field>
										</Form.Group>
										<br />
										<Button type="submit" className="fileStorageSegment" onClick={handleSubmit}>
											{this.state.isEditing ? 'Update' : 'Save'}
										</Button>
									</Segment>
									<Divider />
									<Segment>
										<Segment inverted className="tableSectionHeader">
											{us.user}
										</Segment>
										<div style={{ overflowX: 'scroll' }}>
											<Table striped celled size="small" compact="very" selectable style={{ textAlign: 'center' }}>
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell>{us.tableheads.head3}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head4}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head1}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head2}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head5}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head7}</Table.HeaderCell>
														<Table.HeaderCell>{us.tableheads.head10}</Table.HeaderCell>
														<Table.HeaderCell>Action</Table.HeaderCell>
													</Table.Row>
												</Table.Header>
												<Table.Body>
													{userData
														.filter((user) => user.userType && user.userType.id !== 'D')
														.map((userif, index) => {
															return (
																<Table.Row key={index}>
																	<Table.Cell>{userif.userName}</Table.Cell>
																	<Table.Cell>{userif.address}</Table.Cell>
																	<Table.Cell>{userif.mobile}</Table.Cell>
																	<Table.Cell className="english-div">{userif.email}</Table.Cell>
																	<Table.Cell>{userif.municipalRegNo}</Table.Cell>
																	<Table.Cell>{userif.joinDate && String(userif.joinDate).substring(0, 10)}</Table.Cell>
																	<Table.Cell>{userif.userType.designationNepali || userif.userType.designation}</Table.Cell>

																	<Table.Cell width="2">
																		<Button
																			// circular
																			inverted
																			color="green"
																			onClick={() => this.handleEdit(userif)}
																			icon="edit"
																			title="Edit"
																			size="tiny"
																			style={{ marginBottom: '2px' }}
																		></Button>

																		{/* <Button circular color='red' title="Delete" icon='remove circle' onClick={() => this.modalOpen(userif.id)}></Button> */}

																		{/* <Modal close={this.modalClose} open={this.state.isModalOpen}>
                                                                                <Modal.Content>
                                                                                    <p>Do you want to delete this section?</p>
                                                                                </Modal.Content>
                                                                                <Modal.Actions>
                                                                                    <Button color='green' title="Confirm Delete" onClick={() => this.handleDelete(this.state.idToDelete)} positive
                                                                                        labelPosition='left'
                                                                                        icon='checkmark'
                                                                                        content='Yes'>

                                                                                    </Button>
                                                                                    <Button color='red' title="Decline Delete" onClick={this.modalClose} negative
                                                                                        labelPosition='left'
                                                                                        icon='remove'
                                                                                        content='No'>

                                                                                    </Button>
                                                                                </Modal.Actions>
                                                                            </Modal> */}
																	</Table.Cell>
																</Table.Row>
															);
														})}
												</Table.Body>
											</Table>
										</div>
									</Segment>
									{/* </div> */}
								</Form>
							);
						}}
					</Formik>
				</>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = { getUsers, postUsers, putUsers, deleteUsers, getAdminDataByUrl };
const mapStateToProps = state => ({
	userTypeMaster: state.root.admin.userTypeMaster,
	userData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(AddUser);
