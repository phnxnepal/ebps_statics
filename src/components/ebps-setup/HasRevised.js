import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Grid, Dropdown, Label, Select } from 'semantic-ui-react';
import { hasRevisedData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { SelectInput } from '../shared/Select';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { getOptionText } from '../../utils/dataUtils';
import { useFormNameMaster } from '../../hooks/useFormNameMaster';
import { statusNepaliOption, statusNepaliTallaThapOption } from '../../utils/optionUtils';
import { EditButton, DeleteButton } from './forms/Buttons';
import { FORM_NAME_MASTER } from '../../utils/constants';
import { validateString } from '../../utils/validationUtils';
import Helmet from 'react-helmet';
import { getAdminFilteredOptions } from '../../utils/userTypeUtils';
import { SearchableDropdown } from '../shared/formComponents/SearchableDropdown';

const tableHeadings = hasRevisedData.table.tableHeadings;
const modalData = hasRevisedData.modal;
const revisionOptions = hasRevisedData.optionValues;

export const revisionForOption = [
	{ value: 17, text: revisionOptions.hasRevised17 },
	{ value: 23, text: revisionOptions.hasRevised23 },
	{ value: 29, text: revisionOptions.hasRevised29 },
];

const initialState = {
	id: '',
	hasRevised: 17,
	status: 'N',
	ignoredForm: [],
	userType: [],
};

const schema = Yup.object().shape({
	ignoredForm: validateString,
	userType: validateString,
});
const HasRevisedComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
	const {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		setOpenAddModal,
		setOpenDeleteModal,
	} = useAdminSetup(initialState, success);

	const [formNameMasterOption] = useFormNameMaster(adminData.formNameMaster);
	const [hasRevised, setHasRevised] = useState([]);
	const [userTypeOption, setUserTypeOption] = useState([]);
	const [revision, setRevision] = useState();
	const [userType, setUserType] = useState();

	useEffect(() => {
		if (adminData.hasRevised && Array.isArray(adminData.hasRevised)) {
			setHasRevised(
				adminData.hasRevised.filter(
					(data) =>
						(revision !== undefined ? data.hasRevised === revision : true) && (userType !== undefined ? data.userType === userType : true)
				)
			);
		}
	}, [adminData.hasRevised, revision, userType]);

	useEffect(() => {
		if (adminData.hasRevised) {
			const revised = adminData.hasRevised.sort((a, b) => a.id - b.id);
			setHasRevised(revised);
		}
	}, [adminData.hasRevised]);

	useEffect(() => {
		if (adminData.userTypeMaster && Array.isArray(adminData.userTypeMaster)) {
			const userOptions = getAdminFilteredOptions(adminData.userTypeMaster);
			setUserTypeOption(userOptions);
		}
	}, [adminData.userTypeMaster]);

	return (
		<div>
			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				validationSchema={schema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					// values.userType = values.userType.join(',')
					// values.ignoredForm= values.ignoredForm.join(',')
					try {
						if (isEditing) {
							putAdminDataByUrl(api.hasRevised, formState.id, values);
						} else if (isDeleting) {
							deleteAdminDataByUrl(api.hasRevised, formState.id);
							// setOpenDeleteModal(false);
						} else {
							postAdminDataByUrl(api.hasRevised, values);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in has revised post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting, setFieldValue, errors: formErrors, values, handleChange }) => (
					<>
						<Helmet title="Has Revised Setup" />
						<AdminModalContainer
							key={'has-revised-edit'}
							open={openAddModal}
							scroll={false}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenAddModal(false)}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
						>
							<Form loading={isSubmitting || loading}>
								<Grid>
									<Grid.Row columns="1">
										<Grid.Column>
											{isEditing ? (
												// <SelectInput label={tableHeadings.ignoredForm} name="ignoredForm" options={formNameMasterOption} />

												<SearchableDropdown
													label={tableHeadings.ignoredForm}
													options={formNameMasterOption}
													name="ignoredForm"
													value={values.ignoredForm}
													handleChange={(value) => setFieldValue('ignoredForm', value)}
													bold
												/>
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.ignoredForm}>
														{<span>{tableHeadings.ignoredForm}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.ignoredForm}
															name="ignoreForm"
															onChange={(e, { value }) => setFieldValue('ignoredForm', value)}
															options={formNameMasterOption}
															value={values.ignoredForm}
														/>
														{formErrors.ignoredForm && (
															<Label pointing prompt size="large">
																{formErrors.ignoredForm}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="1">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.userType} name="userType" options={userTypeOption} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.userType}>
														{<span>{tableHeadings.userType}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.userType}
															name="userType"
															onChange={(e, { value }) => setFieldValue('userType', value)}
															options={userTypeOption}
															value={values.userType}
														/>
														{formErrors.userType && (
															<Label pointing prompt size="large">
																{formErrors.userType}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											<SelectInput label={tableHeadings.hasRevised} name="hasRevised" options={revisionForOption} />
										</Grid.Column>
										<Grid.Column>
											<SelectInput
												label={values.hasRevised === 29 ? tableHeadings.statusTallaThap : tableHeadings.status}
												name="status"
												options={values.hasRevised === 29 ? statusNepaliTallaThapOption : statusNepaliOption}
											/>
										</Grid.Column>
									</Grid.Row>
									{/* <Grid.Row columns="2">
										<Grid.Column style={{ marginBottom: '40px' }}>
											<SelectInput needsZIndex={true} label={tableHeadings.hasRevised29} name="hasRevised29" options={statusNepaliOption} />
										</Grid.Column>
									</Grid.Row> */}
								</Grid>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							key={'has-revised-delete'}
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenDeleteModal(false)}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
			<Segment inverted className="tableSectionHeader">
				{hasRevisedData.table.tableHeader}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon.Group>
							<Icon name="file outline" />
							<Icon inverted corner name="add" />
						</Icon.Group>
						{`  ${modalData.titleAdd}`}
					</ButtonContent>
				</Button>
			</Segment>
			<Form className="compact-select">
				<Form.Group>
					<Form.Field width="5">
						{tableHeadings.hasRevised}
						<Select
							name="revision"
							options={revisionForOption}
							onChange={(e, { value }) => {
								setRevision(value);
							}}
							value={revision}
						/>
					</Form.Field>
					<Form.Field width="4">
						{tableHeadings.userType}
						<Select
							name="userType"
							options={userTypeOption}
							onChange={(e, { value }) => {
								setUserType(value);
							}}
							value={userType}
						/>
					</Form.Field>
				</Form.Group>
			</Form>
			<Table striped celled compact="very" size="small">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.ignoredForm}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.hasRevised}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.status}</Table.HeaderCell>
						{/* <Table.HeaderCell>{tableHeadings.hasRevised29}</Table.HeaderCell> */}
						<Table.HeaderCell>{tableHeadings.userType}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{hasRevised.length > 0 ? (
						hasRevised.map((row) => (
							<Table.Row key={row.id}>
								<Table.Cell>{row.id}</Table.Cell>
								<Table.Cell>{getOptionText(parseInt(row.ignoredForm), formNameMasterOption)}</Table.Cell>
								<Table.Cell>{getOptionText(row.hasRevised, revisionForOption)}</Table.Cell>
								<Table.Cell>{getOptionText(row.status, statusNepaliTallaThapOption)}</Table.Cell>
								{/* <Table.Cell>{getOptionText(row.hasRevised29, statusNepaliOption)}</Table.Cell> */}
								<Table.Cell>{getOptionText(row.userType, userTypeOption)}</Table.Cell>
								<Table.Cell>
									<EditButton row={{ ...row, id: parseInt(row.id) }} handleEdit={handleEdit} />
									<DeleteButton handleDelete={handleDelete} row={row} />
								</Table.Cell>
							</Table.Row>
						))
					) : (
						<Table.Row>
							<Table.Cell colSpan="7">
								<Segment textAlign="center">No Data Found.</Segment>
							</Table.Cell>
						</Table.Row>
					)}
				</Table.Body>
			</Table>
		</div>
	);
};

export const HasRevised = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.hasRevised, objName: 'hasRevised' },
			{ api: api.userType, objName: 'userTypeMaster' },
		]}
		localApi={[{ key: FORM_NAME_MASTER, objName: 'formNameMaster' }]}
		updateApi={[{ api: api.hasRevised, objName: 'hasRevised' }]}
		render={(props) => <HasRevisedComponent {...props} parentProps={parentProps} />}
	/>
);
