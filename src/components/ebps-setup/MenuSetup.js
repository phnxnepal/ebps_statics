import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Label, Select } from 'semantic-ui-react';
import { menuSetupData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik, Field } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { EditButton, DeleteButton } from './forms/Buttons';
import { validateString } from '../../utils/validationUtils';
import { SelectInput } from '../shared/Select';
import { menuTypeOptions } from '../../utils/optionUtils';
import { getOptionText } from '../../utils/dataUtils';
import { FlexSingleRight } from '../uiComponents/FlexDivs';
import Helmet from 'react-helmet';

const tableHeadings = menuSetupData.table.tableHeadings;
const modalData = menuSetupData.modal;

const initialState = {
	id: '',
	menu: '',
	menuType: '',
	url: '',
};

const schema = Yup.object().shape({
	menu: validateString,
	menuType: validateString,
	url: validateString,
});

const MenuSetupComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
	const {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		setOpenAddModal,
		setOpenDeleteModal,
	} = useAdminSetup(initialState, success);

	const [menuMaster, setMenuMaster] = useState([]);
	const [menuType, setMenuType] = useState('P');

	useEffect(() => {
		if (adminData.menuMaster && Array.isArray(adminData.menuMaster)) {
			setMenuMaster(adminData.menuMaster.filter(data => (menuType !== undefined ? data.menuType === menuType : true)));
		}
	}, [adminData.menuMaster, menuType]);

	return (
		<div className="english-div">
			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				validationSchema={schema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						if (isEditing) {
							putAdminDataByUrl(api.menuMaster, formState.id, values);
						} else if (isDeleting) {
							deleteAdminDataByUrl(api.menuMaster, formState.id);
						} else {
							postAdminDataByUrl(api.menuMaster, values);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in menu master post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting, errors: formErrors }) => (
					<>
						<Helmet title="Menu Setup" />
						<AdminModalContainer
							open={openAddModal}
							scroll={false}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenAddModal(false)}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
						>
							<Form loading={isSubmitting || loading}>
								<Form.Group widths="equal">
									<Form.Field error={!!formErrors.menu}>
										{<span>{tableHeadings.menu}</span>}
										<Field name="menu" />
										{formErrors.menu && (
											<Label pointing prompt size="large">
												{formErrors.menu}
											</Label>
										)}
									</Form.Field>
									<Form.Field error={!!formErrors.url}>
										{<span>{tableHeadings.url}</span>}
										<Field name="url" />
										{formErrors.url && (
											<Label pointing prompt size="large">
												{formErrors.url}
											</Label>
										)}
									</Form.Field>
								</Form.Group>
								<Form.Group>
									<Form.Field>
										<SelectInput label={tableHeadings.menuType} name="menuType" options={menuTypeOptions} />
									</Form.Field>
								</Form.Group>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenDeleteModal(false)}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
			<Segment inverted className="tableSectionHeader">
				{menuSetupData.table.tableHeader}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon.Group>
							<Icon name="file outline" />
							<Icon inverted corner name="add" />
						</Icon.Group>
						{`  ${modalData.titleAdd}`}
					</ButtonContent>
				</Button>
			</Segment>
			<FlexSingleRight>
				Select Menu Type:{' '}
				<Select
					name="menuType"
					options={menuTypeOptions}
					onChange={(e, { value }) => {
						setMenuType(value);
					}}
					value={menuType}
				/>
			</FlexSingleRight>
			<Table sortable striped celled compact="very" size="small">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.menu}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.menuType}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.url}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{menuMaster.length > 0 ? (
						menuMaster.map(row => (
							<Table.Row key={row.id}>
								<Table.Cell>{row.id}</Table.Cell>
								<Table.Cell>{row.menu}</Table.Cell>
								<Table.Cell>{getOptionText(row.menuType, menuTypeOptions)}</Table.Cell>
								<Table.Cell>{row.url}</Table.Cell>
								<Table.Cell>
									<EditButton row={{ ...row, id: parseInt(row.id) }} handleEdit={handleEdit} />
									<DeleteButton handleDelete={handleDelete} row={row} />
								</Table.Cell>
							</Table.Row>
						))
					) : (
							<Table.Row>
								<Table.Cell colSpan="7">
									<Segment textAlign="center">No Data Found.</Segment>
								</Table.Cell>
							</Table.Row>
						)}
				</Table.Body>
			</Table>
		</div>
	);
};

export const MenuSetup = parentProps => (
	<AdminContainer
		api={[{ api: api.menuMaster, objName: 'menuMaster' }]}
		updateApi={[{ api: api.menuMaster, objName: 'menuMaster' }]}
		render={props => <MenuSetupComponent {...props} parentProps={parentProps} />}
	/>
);
