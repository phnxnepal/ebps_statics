import React, { useState, useEffect, useCallback } from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Label, Select, Grid, Dropdown } from 'semantic-ui-react';
import { rajasowSetupData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik, Field } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { EditButton, DeleteButton } from './forms/Buttons';
import { validateString } from '../../utils/validationUtils';
import { SelectInput } from '../shared/Select';
import { floorOptions as flrOpt, areaTypeOptions as areaTypeOpt, floorTypeOptions as floorTypeOpt, wardNoOptions } from '../../utils/optionUtils';
import { getOptionText } from '../../utils/dataUtils';
import Helmet from 'react-helmet';
import { ConstructionType } from '../../utils/enums/constructionType';
import { validateRequiredNormalNumber } from '../../utils/validation/numberValidations';

const tableHeadings = rajasowSetupData.table.tableHeadings;
const modalData = rajasowSetupData.modal;
const constructionTypeOption = [
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
];
const constructionTypeOptionAll = [
	{ value: 'all', text: rajasowSetupData.dataOption.constructionTypeAll },
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
];

const initialState = {
	floor: [1],
	wardNo: ['01'],
	floorType: ['F'],
	area: ['U'],
	constructionType: ['1'],
	dharoutiRate: 0,
	dasturRate: 0,
};

const schema = Yup.object().shape({
	floor: validateString,
	dasturRate: validateRequiredNormalNumber,
	dharoutiRate: validateRequiredNormalNumber,
	area: validateString,
	floorType: validateString,
	wardNo: validateString,
});

const RajasowSetupComponent = ({
	adminData,
	success,
	getAfterUpdateAdminData,
	postAdminDataByUrl,
	deleteAdminDataByUrl,
	// putAdminDataByUrl,
	errors,
	loading,
	// otherData,
	// prevData
}) => {
	const {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleDelete,
		handleAdd,
		handleEdit,
		setOpenAddModal,
		setOpenDeleteModal,
	} = useAdminSetup(initialState, success);

	const [rajasowMaster, setRajasowMaster] = useState(initialState);
	const [wardNo, setWardNo] = useState('01');
	const [constructionType, setConstructionType] = useState('1');
	const [floor, setFloor] = useState();
	const [floorType, setFloorType] = useState();
	const [areaType, setAreaType] = useState();
	const [wardOption, setWardOption] = useState([]);
	const [wardOptionAll, setWardOptionAll] = useState([]);

	const { floorOptions, areaTypeOptions, floorTypeOptions } = {
		floorOptions: [{ value: 'all', text: rajasowSetupData.dataOption.floorAll }].concat(flrOpt),
		areaTypeOptions: [{ value: 'all', text: rajasowSetupData.dataOption.areaTypeAll }].concat(areaTypeOpt),
		floorTypeOptions: [{ value: 'all', text: rajasowSetupData.dataOption.floorTypeAll }].concat(floorTypeOpt),
	};

	const getUpdateData = useCallback(getAfterUpdateAdminData, []);

	useEffect(() => {
		if (adminData.wardSetup && adminData.wardSetup.length > 0) {
			setWardOption(
				adminData.wardSetup.map((ward) => {
					return { key: ward.id, value: ward.name, text: ward.name };
				})
			);
			setWardOptionAll(
				[{ value: 'all', text: rajasowSetupData.dataOption.wardAll }].concat(
					adminData.wardSetup.map((ward) => {
						return { key: ward.id, value: ward.name, text: ward.name };
					})
				)
			);
		}
	}, [adminData.wardSetup]);

	useEffect(() => {
		if (success && success.success) {
			getUpdateData([{ api: `${api.rajasowSetup}?wardNo=${wardNo}`, objName: 'rajasowMaster' }]);
		}
	}, [success, wardNo, getUpdateData]);

	useEffect(() => {
		if (adminData.rajasowMaster && Array.isArray(adminData.rajasowMaster)) {
			setRajasowMaster(
				adminData.rajasowMaster.filter(
					(data) =>
						(floor !== undefined && floor !== 'all' ? data.floor === floor : true) &&
						(floorType !== undefined && floorType !== 'all' ? data.floorType === floorType : true) &&
						(areaType !== undefined && areaType !== 'all' ? data.area === areaType : true) &&
						(constructionType !== undefined && constructionType !== 'all' ? data.constructionType === constructionType : true)
				)
			);
		}
	}, [adminData.rajasowMaster, floor, floorType, areaType, constructionType]);

	const wardNoChangeHandler = (wardNo) => {
		setWardNo(wardNo);
		getAfterUpdateAdminData([{ api: `${api.rajasowSetup}?wardNo=${wardNo}`, objName: 'rajasowMaster' }]);
	};

	return (
		<div>
			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				validationSchema={schema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					let requiredValues = [];
					let formattedValues = {};
					if (!isDeleting && !isEditing) {
						formattedValues = {
							...values,
							area: values.area.includes('all')
								? areaTypeOptions.filter((row) => row.value !== 'all').map((row) => row.value)
								: values.area,
							floor: values.floor.includes('all')
								? floorOptions.filter((row) => row.value !== 'all').map((row) => row.value)
								: values.floor,
							floorType: values.floorType.includes('all')
								? floorTypeOptions.filter((row) => row.value !== 'all').map((row) => row.value)
								: values.floorType,
							wardNo: values.wardNo.includes('all')
								? wardNoOptions.filter((row) => row.value !== 'all').map((row) => row.value)
								: values.wardNo,
							constructionType: values.constructionType.includes('all')
								? constructionTypeOption.filter((row) => row.value !== 'all').map((row) => row.value)
								: values.constructionType,
						};

						if (!isDeleting && !isEditing && formattedValues.hasOwnProperty('wardNo')) {
							const wardNos = formattedValues['wardNo'];
							wardNos.forEach((wrd) => {
								if (formattedValues.hasOwnProperty('floor')) {
									const floors = formattedValues['floor'];
									floors.forEach((fl) => {
										if (formattedValues.hasOwnProperty('floorType')) {
											const floorTypes = formattedValues['floorType'];
											floorTypes.forEach((flTyp) => {
												if (formattedValues.hasOwnProperty('area')) {
													const areas = formattedValues['area'];
													areas.forEach((ar) => {
														if (formattedValues.hasOwnProperty('constructionType')) {
															const constructionTypes = formattedValues['constructionType'];
															constructionTypes.forEach((cType) =>
																requiredValues.push({
																	wardNo: wrd,
																	floor: fl,
																	floorType: flTyp,
																	area: ar,
																	dasturRate: formattedValues.dasturRate,
																	dharoutiRate: formattedValues.dharoutiRate,
																	constructionType: cType,
																})
															);
														}
													});
												}
											});
										}
									});
								}
							});
						}
					}

					try {
						if (isEditing) {
							await postAdminDataByUrl(api.rajasowSetup, [values]);
						} else if (isDeleting) {
							await deleteAdminDataByUrl(api.rajasowSetup, formState.id);
						} else {
							await postAdminDataByUrl(api.rajasowSetup, requiredValues);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in menu master post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, values, isSubmitting, errors: formErrors, setFieldValue }) => (
					<>
						<Helmet title={rajasowSetupData.table.tableHeader}></Helmet>
						<AdminModalContainer
							open={openAddModal}
							scroll={false}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenAddModal(false)}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
							cancelText={modalData.cancelText}
						>
							<Form loading={isSubmitting || loading}>
								<Grid>
									<Grid.Row columns="2">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.wardNo} name="wardNo" options={wardOption} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.wardNo}>
														{<span>{tableHeadings.wardNo}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.wardNo}
															name="floor"
															onChange={(e, { value }) => setFieldValue('wardNo', value)}
															options={wardOptionAll}
															value={values.wardNo}
														/>
														{formErrors.wardNo && (
															<Label pointing prompt size="large">
																{formErrors.wardNo}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.floor} name="floor" options={flrOpt} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.floor}>
														{<span>{tableHeadings.floor}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.floor}
															name="floor"
															onChange={(e, { value }) => setFieldValue('floor', value)}
															options={floorOptions}
															value={values.floor}
														/>
														{formErrors.floor && (
															<Label pointing prompt size="large">
																{formErrors.floor}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.area} name="area" options={areaTypeOpt} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.area}>
														{<span>{tableHeadings.area}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.area}
															name="area"
															onChange={(e, { value }) => setFieldValue('area', value)}
															options={areaTypeOptions}
															value={values.area}
														/>
														{formErrors.area && (
															<Label pointing prompt size="large">
																{formErrors.area}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
										<Grid.Column>
											{isEditing ? (
												<SelectInput label={tableHeadings.floorType} name="floorType" options={floorTypeOpt} />
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.floorType}>
														{<span>{tableHeadings.floorType}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.floorType}
															name="floorType"
															onChange={(e, { value }) => setFieldValue('floorType', value)}
															options={floorTypeOptions}
															value={values.floorType}
														/>
														{formErrors.floorType && (
															<Label pointing prompt size="large">
																{formErrors.floorType}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="3">
										<Grid.Column>
											{isEditing ? (
												<SelectInput
													label={tableHeadings.constructionType}
													name="constructionType"
													options={constructionTypeOption}
												/>
											) : (
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.constructionType}>
														{<span>{tableHeadings.constructionType}</span>}
														<Dropdown
															selection
															multiple
															label={tableHeadings.constructionType}
															name="constructionType"
															onChange={(e, { value }) => setFieldValue('constructionType', value)}
															options={constructionTypeOptionAll}
															value={values.constructionType}
														/>
														{formErrors.constructionType && (
															<Label pointing prompt size="large">
																{formErrors.constructionType}
															</Label>
														)}
													</Form.Field>
												</Form.Group>
											)}
										</Grid.Column>
										<Grid.Column>
											<Form.Field error={!!formErrors.dharoutiRate}>
												{tableHeadings.dharautiRate}
												<Field name="dharoutiRate" />
												{formErrors.dharoutiRate && (
													<Label pointing prompt size="large">
														{formErrors.dharoutiRate}
													</Label>
												)}
											</Form.Field>
										</Grid.Column>
										<Grid.Column>
											<Form.Field error={!!formErrors.dasturRate}>
												{tableHeadings.dasturRate}
												<Field name="dasturRate" />
												{formErrors.dasturRate && (
													<Label pointing prompt size="large">
														{formErrors.dasturRate}
													</Label>
												)}
											</Form.Field>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenDeleteModal(false)}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
							cancelText={modalData.cancelText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
			<Segment className="tableSectionHeader">
				{rajasowSetupData.table.tableHeader}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon.Group>
							<Icon name="file outline" />
							<Icon inverted corner name="add" />
						</Icon.Group>
						{`  ${modalData.titleAdd}`}
					</ButtonContent>
				</Button>
			</Segment>
			<Form className="compact-select">
				<Form.Group widths="16">
					<Form.Field width="3">
						{rajasowSetupData.dataOption.floor}
						<Select
							name="floor"
							options={floorOptions}
							onChange={(e, { value }) => {
								setFloor(value);
							}}
							value={floor}
						/>
					</Form.Field>{' '}
					<Form.Field width="3">
						{rajasowSetupData.dataOption.floorType}
						<Select
							name="floorType"
							options={floorTypeOptions}
							onChange={(e, { value }) => {
								setFloorType(value);
							}}
							value={floorType}
						/>
					</Form.Field>{' '}
					<Form.Field width="3">
						{rajasowSetupData.dataOption.areaType}
						<Select
							name="areaType"
							options={areaTypeOptions}
							onChange={(e, { value }) => {
								setAreaType(value);
							}}
							value={areaType}
						/>
					</Form.Field>{' '}
					<Form.Field width="3">
						{rajasowSetupData.dataOption.constructionType}
						<Select
							name="constructionType"
							options={constructionTypeOptionAll}
							onChange={(e, { value }) => {
								setConstructionType(value);
							}}
							value={constructionType}
						/>
					</Form.Field>{' '}
					<Form.Field width="2">
						{rajasowSetupData.dataOption.ward}
						<Select
							// wrapSelection={true}
							name="wardOption"
							options={wardOptionAll}
							onChange={(e, { value }) => {
								wardNoChangeHandler(value);
							}}
							value={wardNo}
						/>
					</Form.Field>
				</Form.Group>
			</Form>
			<Table compact="very" size="small" sortable striped celled>
				<Table.Header>
					<Table.Row>
						{/* <Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell> */}
						<Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.wardNo}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.floor}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.floorType}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.area}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.constructionType}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.dharautiRate}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.dasturRate}</Table.HeaderCell>
						<Table.HeaderCell width="3">{tableHeadings.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{rajasowMaster.length > 0 ? (
						rajasowMaster.map((row, idx) => (
							<Table.Row key={`${row.id}-${idx}`}>
								<Table.Cell>{row.id}</Table.Cell>
								<Table.Cell>{row.wardNo}</Table.Cell>
								<Table.Cell>{getOptionText(row.floor, floorOptions)}</Table.Cell>
								<Table.Cell>{getOptionText(row.floorType, floorTypeOptions)}</Table.Cell>
								<Table.Cell>{getOptionText(row.area, areaTypeOptions)}</Table.Cell>
								<Table.Cell>{getOptionText(row.constructionType, constructionTypeOption)}</Table.Cell>
								<Table.Cell>{row.dharoutiRate}</Table.Cell>
								<Table.Cell>{row.dasturRate}</Table.Cell>
								<Table.Cell>
									<EditButton row={{ ...row, id: parseInt(row.id) }} handleEdit={handleEdit} />
									<DeleteButton handleDelete={handleDelete} row={row} />
								</Table.Cell>
							</Table.Row>
						))
					) : (
						<Table.Row>
							<Table.Cell colSpan="9">
								<Segment textAlign="center">No Data Found.</Segment>
							</Table.Cell>
						</Table.Row>
					)}
				</Table.Body>
			</Table>
		</div>
	);
};

export const RajasowSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: `${api.rajasowSetup}?wardNo=01`, objName: 'rajasowMaster' },
			{
				api: api.wardSetup,
				objName: 'wardSetup',
				form: false,
			},
		]}
		updateApi={[]}
		render={(props) => <RajasowSetupComponent {...props} parentProps={parentProps} />}
	/>
);
