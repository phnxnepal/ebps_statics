import React, { useState, useEffect } from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { useFiscalYearAll } from '../../hooks/useFiscalYear';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import { showToast, isEmpty } from '../../utils/functionUtils';
import { Tab, Button, Divider, Header, Form, Grid } from 'semantic-ui-react';
import { Formik } from 'formik';
import { applicationTable, searchData } from '../../utils/data/genericSetupData';
import { namsariColumns, namsariDatafield } from '../../utils/data/ebps-setup/namsariSetupData';
import { discardData } from '../../utils/data/ebps-setup/discardSetupData';
import * as Yup from 'yup';
import EbpsForm, { EbpsNormalForm } from '../shared/EbpsForm';
import { SelectInput } from '../shared/Select';
import { consTypeWithDefaultSelectOptions } from '../../utils/optionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import ErrorDisplay from '../shared/ErrorDisplay';
import { getDataConstructionType } from '../../utils/enums/constructionType';
import { FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import { DashedLangDateField } from './../shared/DateField';
import EbpsTextareaField from './../shared/MyTextArea';
import { validateNepaliDate, validateFileOptional, validateNullableNormalNumber } from '../../utils/validationUtils';
import { getCurrentDate } from './../../utils/dateUtils';
// import { DiscardHistory } from './DiscardHistory';
// import { FlexSingleRight } from './../uiComponents/FlexDivs';
import Helmet from 'react-helmet';
import GenericTableOneButton from './forms/GenericTableOneButton';

const changeDesignerSchema = Yup.object().shape({
	file: validateFileOptional,
	date: validateNepaliDate,
});

const searchSchema = Yup.object().shape({
	applicationNo: validateNullableNormalNumber,
});

const applicationData = applicationTable;
const modalData = discardData.modal;

const initialValues = {
	nibedakName: '',
	constructionType: '',
	year: '',
	kittaNo: '',
	wardNo: '',
	applicationNo: '',
};
const intiVal1 = {
	date: getCurrentDate(),
};

const DiscardSetupComponent = ({ getAfterUpdateAdminData, patchAdminDataByUrl, errors, success, loading, adminData }) => {
	const [appInfo, setAppInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [, sethistoryList] = useState([]);
	const [isChanging, setIsChanging] = useState(false);
	const [dataChanged] = useState(false);

	const { fy, fyOption } = useFiscalYearAll(adminData.fiscalYear, 'yearCode', true);
	useJqxNepaliFilter(dataChanged, [2, 3, 4]);

	useEffect(() => {
		if (success && success.success) {
			showToast('Discarded successfully');
			setOpenModal(false);
			setIsChanging(false);
			setAppInfo({});
		}
	}, [success]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	useEffect(() => {
		adminData.historyList &&
			sethistoryList(
				adminData.historyList.map((el) => {
					return { ...el, constructionType: getDataConstructionType(el.constructionType) };
				})
			);
	}, [adminData.historyList]);

	const handleButtonClick = (id) => {
		const row = appList[id];
		handleConfirmation(row);
	};

	const handleConfirmation = (row) => {
		setAppInfo(row);
		setOpenModal(true);
	};

	const handleChangeDesigner = () => {
		setOpenModal(false);
		setIsChanging(true);
	};

	const handleClose = () => {
		setAppInfo({});
		setIsChanging(false);
		setOpenModal(false);
	};
	// const discardYearHandleChange = year => {
	// 	getAfterUpdateAdminData([{ api: `${api.discardHistory}?year=${year}`, objName: 'historyList' }]);
	// };
	const panes = [
		{
			menuItem: discardData.tab.setupHeading,
			render: () => (
				<Tab.Pane className="simple-tab-pane">
					<Helmet title={discardData.heading}></Helmet>
					<Formik
						key="get-app-info"
						initialValues={{ ...initialValues, year: fy }}
						validationSchema={searchSchema}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);
							try {
								setAppList([]);
								getAfterUpdateAdminData([
									{
										api: `${api.applicationFilter}?applicationNo=${values.applicationNo}&nibedakName=${values.nibedakName}&constructionType=${values.constructionType}&year=${values.year}&kittaNo=${values.kittaNo}&wardNo=${values.wardNo}`,
										objName: 'applicationList',
									},
								]);
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('err', err);
							}
						}}
					>
						{({ handleSubmit, errors, isSubmitting, values, setFieldValue, handleChange }) => (
							<Form loading={isSubmitting || loading}>
								<Form.Group widths="4">
									<Form.Field>
										<EbpsNormalForm
											name="applicationNo"
											label={applicationData.applicationId}
											onChange={handleChange}
											error={errors.applicationNo}
											value={values.applicationNo}
										/>
									</Form.Field>
									<Form.Field>
										<EbpsForm
											name="nibedakName"
											label={applicationData.nibedakName}
											setFieldValue={setFieldValue}
											errors={errors.nibedakName}
											value={values.nibedakName}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput
											needsZIndex={true}
											name="constructionType"
											label={applicationData.constructionType}
											options={consTypeWithDefaultSelectOptions}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput needsZIndex={true} name="year" label={applicationData.year} options={fyOption} />
									</Form.Field>
								</Form.Group>
								<Form.Group widths="4">
									<Form.Field>
										<EbpsForm
											name="kittaNo"
											label={applicationData.kittaNo}
											setFieldValue={setFieldValue}
											errors={errors.kittaNo}
											value={values.kittaNo}
										/>
									</Form.Field>
									<Form.Field>
										<EbpsForm
											name="wardNo"
											label={applicationData.wardNo}
											setFieldValue={setFieldValue}
											errors={errors.wardNo}
											value={values.wardNo}
										/>
									</Form.Field>
								</Form.Group>
								<Form.Group widths="equal">
									<Form.Field>
										<Button
											type="button"
											className="primary-btn"
											icon="search"
											content={searchData.searchApp}
											onClick={handleSubmit}
										/>
									</Form.Field>
								</Form.Group>
							</Form>
						)}
					</Formik>
					<h4>{searchData.searchResults}</h4>
					<GenericTableOneButton
						data={appList}
						columns={namsariColumns}
						datafield={namsariDatafield}
						buttonProps={{
							class: 'danger-table-single-btn icon',
							icon: 'times rectangle icon',
							title: modalData.title,
						}}
						handleButtonClick={handleButtonClick}
						actionWidth={100}
					/>
					<br />
					{!isEmpty(appInfo) && (
						<div>
							<Divider />
							<Formik
								initialValues={intiVal1}
								key="assign-designer"
								validationSchema={changeDesignerSchema}
								onSubmit={(values, actions) => {
									actions.setSubmitting(true);

									try {
										const renewData = new FormData();
										const files = values.file;

										if (files) {
											files.forEach((fl) => renewData.append('file', fl));
										}

										renewData.append('reason', values.reason);
										renewData.append('date', values.date);
										patchAdminDataByUrl(`${api.buildPermit}/${appInfo.applicantNo}`, renewData);
										actions.setSubmitting(false);
									} catch (err) {
										actions.setSubmitting(false);
										console.log('err', err);
									}
								}}
							>
								{({ handleSubmit, isSubmitting, errors: formErrors, validateForm, handleChange, values, setFieldValue }) => {
									return (
										<Form>
											<AdminModalContainer
												key={'open-confirmation'}
												open={openModal}
												errors={errors}
												isSubmitting={isSubmitting || loading}
												handleSubmit={handleChangeDesigner}
												handleClose={handleClose}
												title={modalData.title}
												saveText={modalData.saveText}
												cancelText={modalData.cancelText}
											>
												{modalData.content}
											</AdminModalContainer>

											<AdminModalContainer
												open={isChanging}
												errors={errors}
												isSubmitting={isSubmitting || loading}
												handleSubmit={() => {
													validateForm().then((errors) => {
														if (isEmpty(errors)) {
															handleSubmit();
														}
													});
												}}
												handleClose={handleClose}
												title={modalData.title}
												saveText={modalData.confirmChange}
												cancelText={modalData.cancelText}
											>
												<Form loading={isSubmitting || loading}>
													<Grid>
														<Grid.Row columns="2">
															<Grid.Column>
																{discardData.reasonFile}
																<FileInputWithMultiplePreview
																	name="file"
																	fileCatId="fileUpload"
																	hasMultipleFiles={true}
																/>
															</Grid.Column>
															<DashedLangDateField
																label={discardData.date}
																name="date"
																handleChange={handleChange}
																value={values.date}
																error={formErrors.date}
																inline={true}
															/>
														</Grid.Row>
														<Grid.Row>
															<Grid.Column>
																<EbpsTextareaField
																	labelName={discardData.reason}
																	placeholder="....."
																	name="reason"
																	setFieldValue={setFieldValue}
																	value={values.reason}
																	error={formErrors.reason}
																/>
															</Grid.Column>
														</Grid.Row>
													</Grid>
												</Form>
											</AdminModalContainer>
										</Form>
									);
								}}
							</Formik>
						</div>
					)}
				</Tab.Pane>
			),
		},
		// {
		// 	menuItem: discardData.tab.historyHeading,
		// 	render: () => (
		// 		<Tab.Pane className="simple-tab-pane">
		// 			<FlexSingleRight>
		// 				<div className="zIndexSelect">
		// 					Discard Year:
		// 					<Select
		// 						placeholder="२०७६"
		// 						name="discardYear"
		// 						options={discardYear}
		// 						onChange={(e, { value }) => {
		// 							discardYearHandleChange(value);
		// 						}}
		// 					/>
		// 				</div>
		// 			</FlexSingleRight>
		// 			<br />
		// 			<DiscardHistory historyList={historyList} />
		// 		</Tab.Pane>
		// 	),
		// },
	];

	return (
		<div className="setup-main">
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{discardData.heading}</Header>
			<Tab menu={{ secondary: true, pointing: true }} panes={panes} />
		</div>
	);
};
export const DiscardSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{ api: `${api.discardHistory}?year=2076`, objName: 'historyList' },
		]}
		updateApi={[{ api: `${api.discardHistory}?year=2076`, objName: 'historyList' }]}
		render={(props) => <DiscardSetupComponent {...props} parentProps={parentProps} />}
	/>
);

export { DiscardSetup as default };
