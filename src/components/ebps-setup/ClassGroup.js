import React, { useState, useEffect } from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Button, ButtonContent, Icon, Form, Grid, Dropdown } from 'semantic-ui-react';
import { classGroupData } from '../../utils/data/ebps-setup/classGroupData';
import { Formik } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { SelectInput } from '../shared/Select';
import { ConstructionType } from '../../utils/enums/constructionType';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { getOptionText } from '../../utils/dataUtils';
import Helmet from 'react-helmet';
import { SetupTable } from './shared/SetupTable';
import { SelectColumnFilter } from '../shared/tableFilters/SelectColumnFilter';
import { useFormNameMaster } from '../../hooks/useFormNameMaster';

const tableHeadings = classGroupData.table.tableHeadings;
const modalData = classGroupData.modal;

const buildingClassOptions = [
	{ value: 'A', text: 'Class A' },
	{ value: 'B', text: 'Class B' },
	{ value: 'C', text: 'Class C' },
	{ value: 'D', text: 'Class D' },
];

const constructionTypeOptions = [
	{ value: '1', text: ConstructionType.NAYA_NIRMAN },
	{ value: '2', text: ConstructionType.PURANO_GHAR },
	{ value: '3', text: ConstructionType.TALLA_THAP },
];

// const getValueOptions = [
// 	{ value: 'Y', text: 'Yes' },
// 	{ value: 'N', text: 'No' },
// ];

const initialState = {
	id: '',
	ignoredForm: 1,
	buildingClass: 'A',
	constructionType: '1',
	// getValue: 'N',
};

const ClassGroupComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
	const {
		isEditing,
		isDeleting,
		formState,
		openAddModal,
		openDeleteModal,
		handleAdd,
		handleEdit,
		handleDelete,
		setOpenAddModal,
		setOpenDeleteModal,
	} = useAdminSetup(initialState, success);

	const [formNameMasterOptions] = useFormNameMaster(adminData.formNameMaster);
	const [classGroupMaster, setClassGroupMaster] = useState([]);

	useEffect(() => {
		if (adminData && adminData.classGroup) {
			const sorted = adminData.classGroup.sort((a, b) => a.id - b.id);
			setClassGroupMaster(sorted);
		}
	}, [adminData.classGroup, adminData]);

	const columns = React.useMemo(
		() => [
			{
				Header: tableHeadings.id,
				accessor: 'id',
			},
			{
				Header: tableHeadings.formName,
				id: 'ignoredForm',
				accessor: (originalRow) => {
					return getOptionText(parseInt(originalRow.ignoredForm), formNameMasterOptions);
				},
				Filter: SelectColumnFilter,
				filter: 'includes',
				defaultOption: 'All',
			},
			{
				Header: tableHeadings.buildingClass,
				Filter: SelectColumnFilter,
				filter: 'includes',
				id: 'buildingClass',
				accessor: (originalRow) => {
					return getOptionText(originalRow.buildingClass, buildingClassOptions);
				},
				disableFilters: false,
				defaultOption: 'All',
			},
			{
				Header: tableHeadings.constructionType,
				id: 'constructionType',
				accessor: (originalRow) => {
					return getOptionText(originalRow.constructionType, constructionTypeOptions);
				},
				Filter: SelectColumnFilter,
				filter: 'includes',
				defaultOption: 'All',
			},
			{
				Header: 'Actions',
				// accessor: 'actions',
				Cell: ({ row }) => {
					return (
						<>
							<Button
								inverted
								title="Edit"
								type="reset"
								color="green"
								onClick={() => {
									handleEdit({ ...row.original, ignoredForm: parseInt(row.original.ignoredForm) });
								}}
								icon="edit"
								size="tiny"
								style={{ marginBottom: '2px' }}
							></Button>
							<Button
								inverted
								title="Delete"
								type="reset"
								color="red"
								icon="delete"
								onClick={() => {
									handleDelete(row.original);
								}}
								size="tiny"
							></Button>
						</>
					);
				},
			},
		],
		[formNameMasterOptions, handleDelete, handleEdit]
	);

	return (
		<div>
			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						if (isEditing) {
							putAdminDataByUrl(api.classGroup, formState.id, values);
						} else if (isDeleting) {
							deleteAdminDataByUrl(api.classGroup, formState.id);
							// setOpenDeleteModal(false);
						} else {
							postAdminDataByUrl(api.classGroup, values);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in Class Group post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting, errors: formErrors, setFieldValue }) => (
					<>
						<Helmet title="Class Groups Setup" />
						<AdminModalContainer
							// key={'class-group-edit'}
							open={openAddModal}
							scroll={false}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenAddModal(false)}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
						>
							<Form loading={isSubmitting || loading}>
								<Grid>
									<Grid.Row columns="2">
										<Grid.Column>
											{isEditing ? (
												<SelectInput label="Form to ignore" name="ignoredForm" options={formNameMasterOptions} />
											) : (
													<Form.Group widths="equal">
														<Form.Field error={!!formErrors.ignoredForm}>
															{<span>Form to ignore</span>}
															<Dropdown
																selection
																multiple
																label={tableHeadings.ignoredForm}
																name="ignoredForm"
																onChange={(e, { value }) => setFieldValue('ignoredForm', value)}
																options={formNameMasterOptions}
															/>
														</Form.Field>
													</Form.Group>
												)}
										</Grid.Column>
										<Grid.Column>
											<SelectInput label="Building Class" name="buildingClass" options={buildingClassOptions} />
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											<SelectInput label="Construction Type" name="constructionType" options={constructionTypeOptions} />
										</Grid.Column>
										{/* <Grid.Column>
											<SelectInput label="Keep Form" name="getValue" options={getValueOptions} />
										</Grid.Column> */}
									</Grid.Row>
								</Grid>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							// key={'class-group-delete'}
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={() => setOpenDeleteModal(false)}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
			<Segment inverted className="tableSectionHeader">
				{classGroupData.table.tableHeader}
				<Button
					basic
					style={{ right: 6, top: 8, position: 'absolute' }}
					inverted
					type="reset"
					onClick={() => {
						handleAdd();
					}}
				>
					<ButtonContent>
						<Icon.Group>
							<Icon name="file outline" />
							<Icon inverted corner name="add" />
						</Icon.Group>{' '}
						Add Class Group
					</ButtonContent>
				</Button>
			</Segment>
			<SetupTable columns={columns} data={classGroupMaster} />
		</div>
	);
};

export const ClassGroup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.classGroup, objName: 'classGroup' },
			{ api: api.formNameMaster, objName: 'formNameMaster' },
		]}
		updateApi={[{ api: api.classGroup, objName: 'classGroup' }]}
		render={(props) => <ClassGroupComponent {...props} parentProps={parentProps} />}
	/>
);
