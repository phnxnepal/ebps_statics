import React, { Component } from 'react'
import { Formik } from 'formik'
import { fiscalYear } from '../../utils/data/ebps-setup/fiscalYeardata'
import { Button, Form, Table, Select, Segment, Modal, Label } from 'semantic-ui-react';
import { connect } from "react-redux";
import { getFiscalYears, postFiscalYears, putFiscalYear, deleteFiscalYear } from "../../store/actions/AdminAction";
import FallbackComponent from '../shared/FallbackComponent';
import { showToast } from '../../utils/functionUtils';
// import api from '../../utils/api';
// import FormWrapper from '../shared/FormWrapper';
// import { async } from 'q';
import ErrorDisplay from '../shared/ErrorDisplay';
import { EbpsNormalForm } from '../shared/EbpsForm'
import { EbpsFormDateField, FiscalYearField } from '../shared/DateField';
import * as Yup from 'yup';
import {
    validateString, validateNepaliDate
} from '../../utils/validationUtils';
import Helmet from 'react-helmet';
const fyear = fiscalYear.fiscalYear_data
const formOption = [
    { key: 'a1', value: 'Y', text: 'Y' },
    { key: 'a2', value: 'N', text: 'N' }
]
class FiscalYear extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formData: {
                yearCode: '',
            },

            isEditing: false,
            isModalOpen: false,
            idToDelete: ''
        }

    }
    modalOpen = (yearCode) => {
        this.setState({
            isModalOpen: true,
            idToDelete: yearCode
        })
    }
    modalClose = () => {
        this.setState({
            isModalOpen: false
        })
    }
    handleDelete = async (yearCode) => {
        try {
            await this.props.deleteFiscalYear(yearCode)
            if (this.props.success && this.props.success.success) {
                showToast('Your data has been successfully deleted.');
            }
            this.modalClose();
            // this.setState({
            //     formData: {}
            // })
        } catch (err) {
            console.log("Error", err)
            this.modalClose();

        }
    }
    handleEdit = (User) => {
        console.log("User:", User)
        window.scrollTo(0, 0);
        this.setState({
            formData: {
                endDate: User.endDate,
                startDate: User.startDate,
                status: User.status,
                yearCode: User.yearCode,
                yearName: User.yearName,
            },
            isEditing: true

        })
    }
    componentDidMount() {
        this.props.getFiscalYears();
    };
    componentDidUpdate(prevProps) {
        if (prevProps.success !== this.props.success) {
            setTimeout(() => {
                this.props.getFiscalYears()
            },
                0
            )

        }

    }

    render() {
        const strData = [
            'yearCode',
            'yearName',
            'status'
        ];
        const dateData = ['startDate', 'endDate'];
        const formDataSchema = strData.map(row => {
            return {
                [row]: validateString
            };
        });
        const dateDataSchema = dateData.map(row => {
            return {
                [row]: validateNepaliDate
            };
        })
        const FiscalYearSchema = Yup.object().shape(
            Object.assign({},
                ...formDataSchema,
                ...dateDataSchema
            )
        );

        const { userData, postFiscalYears } = this.props;
        if (userData) {
            return (
                <Formik
                    enableReinitialize
                    initialValues={this.state.formData}
                    validationSchema={FiscalYearSchema}
                    onSubmit={async (values, actions) => {
                        actions.setSubmitting(true);
                        values.error && delete values.error;
                        try {
                            if (this.state.isEditing) {
                                await this.props.putFiscalYear(this.state.formData.yearCode, values)
                            }
                            else {
                                await postFiscalYears(values);
                            }
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            if (this.props.success && this.props.success.success) {
                                showToast('Your data has been successfully added.');
                            }
                        }
                        catch (err) {
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            showToast('Something went wrong !!');
                        }
                    }}
                >
                    {({ handleSubmit, setFieldValue, values, errors, handleChange }) => (

                        <Form>
                            {this.props.errors && (
                                <ErrorDisplay
                                    message={this.props.errors.message}
                                />
                            )}
                            <Helmet title={fyear.heading.heading_3} />
                            <div> <Segment raised>
                                <Segment inverted className="tableSectionHeader">{fyear.heading.heading_2}</Segment>
                                {/* <Grid> */}
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <strong>{fyear.table_1.table_1_1}</strong>
                                        {/* <Field type="text" name="yearCode" /> */}
                                        {/* <EbpsForm
                                                    name="yearCode"
                                                    // onChange={this.props.handleChange}
                                                    setFieldValue={setFieldValue}
                                                    error={errors.yearCode}
                                                    value={values.yearCode}
                                                /> */}
                                        <EbpsNormalForm
                                            name="yearCode"
                                            onChange={handleChange}
                                            error={errors.yearCode}
                                            value={values.yearCode} />
                                    </Form.Field>
                                    <Form.Field>
                                        <strong>{fyear.table_1.table_1_2}</strong>
                                        <FiscalYearField
                                            name="yearName"
                                            value={values.yearName}
                                            onChange={(e, { value }) => { setFieldValue("yearName", value); }}
                                            handleChange={handleChange}
                                            error={errors.yearName}
                                        />

                                    </Form.Field>
                                    <Form.Field>
                                        <strong>{fyear.table_1.table_1_3}</strong>
                                        <EbpsFormDateField
                                            name="startDate"
                                            onChange={(e, { value }) => { setFieldValue("startDate", value); }}
                                            handleChange={handleChange}
                                            value={values.startDate}
                                            error={errors.startDate}
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <strong>{fyear.table_1.table_1_4}</strong>
                                        <EbpsFormDateField
                                            name="endDate"
                                            value={values.endDate}
                                            onChange={(e, { value }) => { setFieldValue("endDate", value); }}
                                            handleChange={handleChange}
                                            error={errors.endDate}
                                        />
                                    </Form.Field>
                                    {/* <Grid.Column> */}
                                    {/* <strong>{fyear.table_1.table_1_5}</strong> */}
                                    {/* <Select
                                                    placeholder='Y'
                                                    options={formOption}
                                                    name="status"
                                                    style={{ width: '100%' }}
                                                    value={values.status}
                                                    onChange={(e, { value }) => { setFieldValue("status", value); }} /> */}
                                    <Form.Field error={!!errors.status}>
                                        <strong>{fyear.table_1.table_1_5}</strong><br />
                                        <Select
                                            placeholder='Y'
                                            value={values.status}
                                            name="status"
                                            options={formOption}
                                            onChange={(e, { value }) => {
                                                setFieldValue("status", value);
                                            }
                                            }
                                        // error={errors.userType}
                                        />
                                        {errors.status && (
                                            <Label pointing prompt size="large">
                                                {errors.status}
                                            </Label>
                                        )}
                                    </Form.Field>
                                </Form.Group>
                                {/* </Grid.Column> */}
                                {/* </Grid.Row> */}
                                {/* </Grid> */}
                                <br />
                                <Button type="submit" onClick={handleSubmit} className="fileStorageSegment">
                                    {this.state.isEditing ? 'अपडेट' : 'Save'}
                                </Button>
                            </Segment>

                                <br />
                                <Segment raised>
                                    <Segment inverted className="tableSectionHeader">
                                        {fyear.heading.heading_1}
                                    </Segment>
                                    <Table striped celled selectable style={{ textAlign: 'center' }}>
                                        <Table.Header>
                                            <Table.Row>
                                                <Table.HeaderCell>{fyear.table_1.table_1_1}</Table.HeaderCell>
                                                <Table.HeaderCell>{fyear.table_1.table_1_2}</Table.HeaderCell>
                                                <Table.HeaderCell>{fyear.table_1.table_1_3}</Table.HeaderCell>
                                                <Table.HeaderCell>{fyear.table_1.table_1_4}</Table.HeaderCell>
                                                <Table.HeaderCell>{fyear.table_1.table_1_5}</Table.HeaderCell>
                                                <Table.HeaderCell>Action</Table.HeaderCell>
                                            </Table.Row>
                                        </Table.Header>

                                        <Table.Body>
                                            {this.props.userData.map((fiscal, index) => {
                                                return (
                                                    <Table.Row key={index}>
                                                        <Table.Cell >{fiscal.yearCode}</Table.Cell>
                                                        <Table.Cell >{fiscal.yearName}</Table.Cell>
                                                        <Table.Cell >{fiscal.startDate}</Table.Cell>
                                                        <Table.Cell >{fiscal.endDate}</Table.Cell>
                                                        <Table.Cell >{fiscal.status}</Table.Cell>
                                                        <Table.Cell >
                                                            <Button
                                                                // circular
                                                                inverted
                                                                title='Edit'
                                                                color='green'
                                                                icon='edit'
                                                                size='tiny'
                                                                style={{ marginBottom: '2px' }}
                                                                onClick={() => this.handleEdit(fiscal)}>
                                                            </Button>
                                                            <Button
                                                                // circular
                                                                inverted
                                                                color='red'
                                                                title="Delete"
                                                                icon='remove circle'
                                                                size='tiny'
                                                                style={{ marginBottom: '2px' }}
                                                                onClick={() => this.modalOpen(fiscal.yearCode)}>
                                                            </Button>
                                                            <Modal close={this.modalClose} open={this.state.isModalOpen}>
                                                                <Modal.Content>
                                                                    <p>{fyear.modal.message}</p>
                                                                </Modal.Content>
                                                                <Modal.Actions>
                                                                    <Button
                                                                        color='green'
                                                                        onClick={() => this.handleDelete(this.state.idToDelete)}
                                                                        title="Confirm Delete!" positive
                                                                        labelPosition='left'
                                                                        icon='checkmark'
                                                                        content={fyear.modal.acceptText}>
                                                                    </Button>
                                                                    <Button color='red' onClick={this.modalClose} title="Decline Delete!" negative
                                                                        labelPosition='left'
                                                                        icon='remove'
                                                                        content={fyear.modal.cancelText}>
                                                                    </Button>
                                                                </Modal.Actions>
                                                            </Modal>
                                                        </Table.Cell>
                                                    </Table.Row>)
                                            })
                                            }

                                        </Table.Body>
                                    </Table>
                                </Segment>


                            </div>
                        </Form>

                    )
                    }
                </Formik>
            );
        } else {
            return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />
        }
    }
}
const mapDispatchToProps = { getFiscalYears, postFiscalYears, putFiscalYear, deleteFiscalYear }
const mapStateToProps = state => ({
    userData: state.root.admin.userData,
    loading: state.root.ui.loading,
    errors: state.root.ui.errors,
    success: state.root.admin.success
});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FiscalYear);