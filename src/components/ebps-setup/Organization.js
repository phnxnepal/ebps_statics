import React, { Component } from 'react';
import { Form, Button, Table, Segment, Icon, Label } from 'semantic-ui-react';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import EbpsForm, { EbpsNormalFormIm } from '../shared/EbpsForm';
import { validateString, validateNumberWithDash } from '../../utils/validationUtils';
import ErrorDisplay from '../shared/ErrorDisplay';
import * as Yup from 'yup';
import { pick } from 'lodash';
import {
	getOrganizationData,
	postOrganization,
	putOrganization,
	// deleteOrganization
} from '../../store/actions/AdminAction';
import FallbackComponent from '../shared/FallbackComponent';
import { showToast } from '../../utils/functionUtils';
import Helmet from 'react-helmet';
import { SelectInput } from '../shared/Select';
import { designerSignupTypeOptions, hasAccessOption } from '../../utils/optionUtils';
import { setLocalStorage } from '../../utils/secureLS';
import { ORG_IDS } from '../../utils/constants';
import { yesNoData } from '../../utils/data/genericFormData';

const initialFormData = {
	id: '',
	address: '',
	province: '',
	name: '',
	email: '',
	mayorName: '',
	officeContactNo: '',
	officeName: '',
	secretaryName: '',
	subMayorName: '',
	fileLocation: '',
	url: '',
	designerLoginType: '',
	mailServer: '',
	mailServerPort: '',
	mailSender: '',
	mailPassword: '',
	showSignatureImage: 'N',
};

const strData = [
	'address',
	'province',
	'mayorName',
	'name',
	'officeContactNo',
	'officeName',
	'fileLocation',
	'designerLoginType',
	'mailServer',
	'mailServerPort',
	// 'mailSender',
	'mailPassword',
	// 'secretaryName',
	// 'subMayorName',
	// 'url',
];
const phoneData = ['officeContactNo'];
const formDataSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});
const phoneDataSchema = phoneData.map((row) => {
	return {
		[row]: validateNumberWithDash,
	};
});
const organizationSchema = Yup.object().shape(
	Object.assign(
		{
			email: Yup.string().email('इमेल गलत भयो').required('यो भर्नु आवश्यक छ'),
			mailSender: Yup.string().email('इमेल गलत भयो').required('यो भर्नु आवश्यक छ'),
		},
		...formDataSchema,
		...phoneDataSchema
	)
);

class Organization extends Component {
	constructor(props) {
		super(props);
		this.state = {
			formData: initialFormData,
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
			hidden: true,
		};
	}
	modalOpen = (id) => {
		this.setState({ isModalOpen: true, idToDelete: id });
	};
	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	componentDidMount() {
		this.props.getOrganizationData();
	}
	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success && this.state.isEditing) {
			setTimeout(() => {
				this.props.getOrganizationData();
			}, 0);
			this.setState({ isEditing: false });
		}
		if (this.props.success && this.props.success.success && this.props.organizationData !== prevProps.organizationData) {
			const newOrgData = pick(this.props.organizationData[0], [
				'name',
				'officeName',
				'address',
				'province',
				'organizationCode',
				'designerLoginType',
			]);

			setLocalStorage(ORG_IDS, JSON.stringify({ organization: newOrgData }));
		}
	}
	handleEdit = (org) => {
		window.scrollTo(0, 0);
		this.setState({
			formData: {
				address: org.address,
				province: org.province,
				name: org.name,
				email: org.email,
				mayorName: org.mayorName,
				officeContactNo: org.officeContactNo,
				officeName: org.officeName,
				secretaryName: org.secretaryName,
				subMayorName: org.subMayorName,
				fileLocation: org.fileLocation,
				url: org.url,
				id: org.id,
				designerLoginType: org.designerLoginType,
				mailServer: org.mailServer,
				mailServerPort: org.mailServerPort,
				mailSender: org.mailSender,
				mailPassword: org.mailPassword,
				showSignatureImage: org.showSignatureImage,
			},
			isEditing: true,
		});
	};
	// handleDelete = async id => {
	//     try {
	//         await this.props.deleteOrganization(id);
	//         if (this.props.success && this.props.success.success) {
	//             showToast('Your data has been successfully deleted.');
	//         }
	//         this.modalClose();
	//     } catch (err) {
	//         console.log('Error', err);
	//         this.modalClose();
	//     }
	// };
	render() {
		const { organizationData } = this.props;
		const { hidden } = this.state;
		if (organizationData) {
			return (
				<Formik
					enableReinitialize
					validationSchema={organizationSchema}
					initialValues={this.state.formData}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						try {
							if (this.state.isEditing) {
								await this.props.putOrganization(values);
							} else {
								await this.props.postOrganization(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								actions.resetForm();
								this.setState({ formData: initialFormData });
								showToast('Your data has been successfully posted.');
							}
						} catch (err) {
							actions.setSubmitting(false);
							console.log('error ', err);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, setFieldValue, handleBlur, values, isSubmitting, errors, handleChange, handleReset }) => (
						<Form loading={isSubmitting}>
							<Helmet title="Organization" />
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Segment>
								<Segment inverted className="tableSectionHeader">
									Organization
								</Segment>
								<Form.Group widths="4">
									<Form.Field>
										<strong>Address</strong>
										<EbpsForm
											name="address"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.address}
											value={values.address}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Province</strong>
										<EbpsForm
											name="province"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.province}
											value={values.province}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Email</strong>
										<EbpsNormalFormIm name="email" />
									</Form.Field>
									<Form.Field>
										<strong>Mayor Name</strong>
										<EbpsForm
											name="mayorName"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.mayorName}
											value={values.mayorName}
										/>
									</Form.Field>
								</Form.Group>
								<Form.Group widths="4">
									<Form.Field>
										<strong>Name</strong>
										<EbpsForm
											name="name"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.name}
											value={values.name}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Office Contact No</strong>
										<EbpsForm
											name="officeContactNo"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.officeContactNo}
											value={values.officeContactNo}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Office Name</strong>
										<EbpsForm
											name="officeName"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.officeName}
											value={values.officeName}
										/>
									</Form.Field>
									<Form.Field>
										<strong>Secretary Name</strong>
										<EbpsForm
											name="secretaryName"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.secretaryName}
											value={values.secretaryName}
										/>
									</Form.Field>
								</Form.Group>
								<Form.Group widths="3">
									<Form.Field>
										<strong>Sub Mayor Name</strong>
										<EbpsForm
											name="subMayorName"
											setFieldValue={setFieldValue}
											onChange={handleChange}
											error={errors.subMayorName}
											value={values.subMayorName}
										/>
									</Form.Field>
									<Form.Field>
										<strong>URL</strong>
										<EbpsNormalFormIm name="url" />
									</Form.Field>
									<Form.Field>
										<strong>File Location</strong>
										<EbpsNormalFormIm name="fileLocation" />
									</Form.Field>
								</Form.Group>
								<Form.Group widths="4">
									<Form.Field>
										<strong>Mail Server</strong>
										<EbpsNormalFormIm name="mailServer" />
									</Form.Field>
									<Form.Field>
										<strong>Mail Server Port</strong>
										<EbpsNormalFormIm name="mailServerPort" />
									</Form.Field>
									<Form.Field>
										<strong>Mail Sender</strong>
										<EbpsNormalFormIm name="mailSender" />
									</Form.Field>
									<Form.Field error={errors.mailPassword}>
										<strong>Mail Password</strong>
										<Form.Input
											icon={
												<Icon
													name={hidden ? 'eye slash' : 'eye'}
													link
													onClick={() =>
														this.setState((prevState) => ({
															hidden: !prevState.hidden,
														}))
													}
												/>
											}
											type={hidden ? 'password' : 'text'}
											name="mailPassword"
											autoComplete={false}
											onChange={handleChange}
											onBlur={handleBlur}
											value={values.mailPassword}
											error={errors.mailPassword}
											placeholder="Mail Password"
										/>
										{errors.mailPassword && (
											<Label pointing prompt size="large">
												{errors.mailPassword}
											</Label>
										)}
									</Form.Field>
								</Form.Group>
								<Form.Group widths="1">
									<Form.Field>
										<SelectInput label="Designer Signup Type" name="designerLoginType" options={designerSignupTypeOptions} />
									</Form.Field>
									<Form.Field>
										<SelectInput label="Show Signature Image" name="showSignatureImage" options={hasAccessOption} />
									</Form.Field>
								</Form.Group>
								<br />
								<Button type="submit" disabled={!this.state.isEditing} onClick={handleSubmit} className="fileStorageSegment">
									Update
								</Button>
							</Segment>
							<br />
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									Organization Data
								</Segment>
								<Table striped celled selectable style={{ textAlign: 'center' }}>
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell>Address</Table.HeaderCell>
											<Table.HeaderCell>Province</Table.HeaderCell>
											<Table.HeaderCell>Name</Table.HeaderCell>
											<Table.HeaderCell>Office Name</Table.HeaderCell>
											<Table.HeaderCell>Action</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									<Table.Body>
										{organizationData.map((org, index) => {
											return (
												<Table.Row key={1}>
													<Table.Cell>{org.address}</Table.Cell>
													<Table.Cell>{org.province}</Table.Cell>
													<Table.Cell>{org.name}</Table.Cell>
													<Table.Cell>{org.officeName}</Table.Cell>
													<Table.Cell>
														<Button
															inverted
															title="Edit"
															color="green"
															onClick={() => {
																handleReset();
																this.setState({ formData: initialFormData });
																this.handleEdit(org);
															}}
															icon="edit"
															size="tiny"
														></Button>

														{/* <Button
                                                            circular
                                                            color="red"
                                                            title="Delete"
                                                            icon="remove circle"
                                                            onClick={() => this.modalOpen(org.id)}
                                                            size="tiny"
                                                        ></Button> */}

														{/* <Modal
                                                            close={this.modalClose}
                                                            open={this.state.isModalOpen}
                                                        >
                                                            <Modal.Content>
                                                                <p>Do you want to delete this section?</p>
                                                            </Modal.Content>
                                                            <Modal.Actions>
                                                                <Button
                                                                    color="green"
                                                                    onClick={() =>
                                                                        this.handleDelete(this.state.idToDelete)
                                                                    }
                                                                    positive
                                                                    labelPosition="left"
                                                                    icon="checkmark"
                                                                    content="Yes"
                                                                    title="Confirm Delete!"
                                                                ></Button>
                                                                <Button
                                                                    color="red"
                                                                    onClick={this.modalClose}
                                                                    negative
                                                                    labelPosition="left"
                                                                    icon="remove"
                                                                    content="No"
                                                                    title="Decline Delete!"
                                                                ></Button>
                                                            </Modal.Actions>
                                                        </Modal> */}
													</Table.Cell>
												</Table.Row>
											);
										})}
									</Table.Body>
								</Table>
							</Segment>
						</Form>
					)}
				</Formik>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = {
	getOrganizationData,
	postOrganization,
	putOrganization,
	// deleteOrganization
};
const mapStateToProps = (state) => ({
	organizationData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(Organization);
