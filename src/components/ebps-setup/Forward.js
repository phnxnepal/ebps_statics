import React, { Component } from 'react';
import { Form, Segment, Button, Table, Modal, Label } from 'semantic-ui-react';
// import FormWrapper from '../shared/FormWrapper';
// import EbpsForm from '../shared/EbpsForm';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { getAdminDataByUrl, getGroupwiseForwardData, postForwardData, putForwardData, deleteForwardData } from '../../store/actions/AdminAction';
import { getForwardData } from '../../store/actions/AdminAction';
import FallbackComponent from '../shared/FallbackComponent';
import EbpsSelect from '../shared/Select';
import { getUserRole, showToast } from '../../utils/functionUtils';
import { validateString } from '../../utils/validationUtils';
import { forwardMaster as forwardData } from '../../utils/data/ebps-setup/forwardMaster';
import * as Yup from 'yup';
import Helmet from 'react-helmet';
import api from '../../utils/api';
import { getAdminFilteredOptions } from '../../utils/userTypeUtils';
import { getOptionText } from '../../utils/dataUtils';

class Forward extends Component {
	constructor(props) {
		super(props);
		const userType = getUserRole();
		this.state = {
			// currentUser: { id: 0, token: "" },
			formData: {
				id: '',
				formGroup: 1,
				forwardBy: '',
				forwardTo: '',
				skipParameter: '',
			},
			currentGroup: 1,
			userType: userType,
			groupMasterOptions: [],
			userTypeOptions: [],
			formNameOptions: [],
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
		};
	}

	modalOpen = (id) => {
		this.setState({ isModalOpen: true, idToDelete: id });
	};

	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	componentDidMount() {
		this.props.getAdminDataByUrl([
			{ api: `${api.userType}`, objName: 'userTypeMaster' },
			{ api: `${api.formGroupMaster}`, objName: 'group' },
			{ api: `${api.forwardMaster}?formGroup=1`, objName: 'forwardMaster' },
			{ api: `${api.formNameMaster}`, objName: 'formNameMaster' },
		]);
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			// this.props.getUsers();
			setTimeout(() => {
				this.props.getGroupwiseForwardData(this.state.currentGroup);
			}, 0);
			// console.log('updates', prevProps, this.props.success);
		}

		if (prevProps.userTypeMaster !== this.props.userTypeMaster && this.props.userTypeMaster) {
			this.setState({ userTypeOptions: getAdminFilteredOptions(this.props.userTypeMaster) });
		}

		if (this.props.formNameMaster !== prevProps.formNameMaster && this.props.formNameMaster) {
			const formNameOptions = this.props.formNameMaster.map((row) => ({ value: String(row.id), text: row.name }));
			this.setState({
				formNameOptions: [{ value: '', text: 'No Form' }].concat(formNameOptions),
			});
		}

		if (prevProps.group !== this.props.group && this.props.group) {
			this.setState({
				groupMasterOptions: this.props.group.map((grp) => {
					return { value: grp.id, text: grp.name };
				}),
				formData: {
					formGroup: 1,
					forwardBy: 'D',
					forwardTo: 'B',
					skipParameter: null,
				},
			});
		}
	}

	handleGroupSelect = (groupId) => {
		this.setState({ currentGroup: groupId });
		this.props.getGroupwiseForwardData(groupId);
	};

	handleEdit = (User) => {
		// console.log('click', User);
		window.scrollTo(0, 0);
		this.setState({
			formData: {
				id: User.id,
				formGroup: User.formGroup.id,
				forwardBy: User.forwardBy,
				forwardTo: User.forwardTo,
				skipParameter: User.skipParameter,
			},

			isEditing: true,
		});
	};

	handleDelete = async (id) => {
		try {
			await this.props.deleteForwardData(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};

	render() {
		const { forwardMaster, group } = this.props;
		const { formNameOptions, groupMasterOptions, formData, userTypeOptions } = this.state;
		const strData = ['forwardTo', 'forwardBy'];
		const strSchema = strData.map((row) => {
			return {
				[row]: validateString,
			};
		});
		const forwardSchema = Yup.object().shape(Object.assign({}, ...strSchema));

		// console.log('ths.prop', this.props);
		if (forwardMaster && group) {
			return (
				<>
					<Formik
						initialValues={formData}
						enableReinitialize
						validationSchema={forwardSchema}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);
							values.error && delete values.error;
							// console.log('user', values);
							try {
								if (this.state.isEditing) {
									// console.log('for edit');
									await this.props.putForwardData(this.state.formData.id, values);
								} else {
									// console.log('add');
									await this.props.postForwardData(values);
								}
								actions.setSubmitting(false);
								window.scrollTo(0, 0);
								if (this.props.success && this.props.success.success) {
									showToast('Your data has been successfully added.');
								}
							} catch (err) {
								actions.setSubmitting(false);
								window.scrollTo(0, 0);
								showToast('Something went wrong !!');
							}
						}}
					>
						{({ values, setFieldValue, isSubmitting, handleSubmit, errors }) => (
							<Form loading={isSubmitting}>
								<Helmet>
									<title>Forward To</title>
								</Helmet>
								<Segment raised>
									<Segment inverted className="tableSectionHeader">
										Application Forwarding Setup
									</Segment>
									<Form.Group widths="equal">
										{/* <Grid.Row> */}
										{/* <Grid.Column  > */}
										<Form.Field error={!!errors.formGroup}>
											<strong>Form Group</strong>
											<EbpsSelect
												name="formGroup"
												// label='User Type'
												options={groupMasterOptions}
												onChange={(e, { value }) => {
													setFieldValue('formGroup', value);
													this.handleGroupSelect(value);
												}}
												value={values.formGroup}
											/>
											{errors.formGroup && (
												<Label pointing prompt size="large">
													{errors.formGroup}
												</Label>
											)}
										</Form.Field>
										<Form.Field error={!!errors.forwardBy}>
											{/* <Grid.Column>
                                                <strong>
                                                    Id
                                            </strong>
                                                <EbpsForm
                                                    name="id"
                                                    // setFieldValue={setFieldValue}
                                                    error={errors.id}
                                                    value={values.id}
                                                    onChange={handleChange}/>
                                            </Grid.Column> */}

											<strong>Forward By</strong>
											<EbpsSelect
												name="forwardBy"
												// label='User Type'
												options={userTypeOptions}
												onChange={(e, { value }) => {
													setFieldValue('forwardBy', value);
													// this.handleUserTypeSelect(value);
												}}
												value={values.forwardBy}
											/>
											{errors.forwardBy && (
												<Label pointing prompt size="large">
													{errors.forwardBy}
												</Label>
											)}
										</Form.Field>
										{/* </Grid.Column> */}
										{/* <Grid.Column > */}
										<Form.Field error={!!errors.forwardTo}>
											{/* <Grid.Column>
                                                <strong>
                                                    Id
                                            </strong>
                                                <EbpsForm
                                                    name="id"
                                                    // setFieldValue={setFieldValue}
                                                    error={errors.id}
                                                    value={values.id}
                                                    onChange={handleChange}/>
                                            </Grid.Column> */}

											<strong>Forward To</strong>
											<EbpsSelect
												name="forwardTo"
												// label='User Type'
												options={userTypeOptions}
												onChange={(e, { value }) => {
													setFieldValue('forwardTo', value);
													// this.handleUserTypeSelect(value);
												}}
												value={values.forwardTo}
											/>
											{errors.forwardTo && (
												<Label pointing prompt size="large">
													{errors.forwardTo}
												</Label>
											)}
										</Form.Field>{' '}
										<Form.Field>
											<strong>छोड्न सकिने फारम</strong>
											<EbpsSelect
												name="skipParameter"
												// label='User Type'
												options={formNameOptions}
												onChange={(e, { value }) => {
													setFieldValue('skipParameter', value);
												}}
												value={values.skipParameter}
											/>
											{errors.skipParameter && (
												<Label pointing prompt size="large">
													{errors.skipParameter}
												</Label>
											)}
										</Form.Field>
										{/* </Grid.Column>   */}
										{/* </Grid.Row> */}
										{/* </Grid> */}
									</Form.Group>
									<Button type="submit" onClick={handleSubmit} className="fileStorageSegment">
										{this.state.isEditing ? 'Update' : 'Save'}
									</Button>
								</Segment>

								<Segment raised>
									<Segment className="tableSectionHeader">Application Forwarding Setup Data</Segment>
									<Table celled selectable striped style={{ textAlign: 'center' }}>
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell> Id</Table.HeaderCell>
												<Table.HeaderCell>Form Group</Table.HeaderCell>
												<Table.HeaderCell>Forward By</Table.HeaderCell>
												<Table.HeaderCell>Forward To</Table.HeaderCell>
												<Table.HeaderCell>Skipable Form</Table.HeaderCell>
												<Table.HeaderCell>Action</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{forwardMaster.map((fstore, index) => {
												return (
													<Table.Row key={index}>
														<Table.Cell>{fstore.id}</Table.Cell>
														<Table.Cell>{fstore.formGroup.name}</Table.Cell>
														<Table.Cell>{getOptionText(fstore.forwardBy, userTypeOptions)}</Table.Cell>
														<Table.Cell>{getOptionText(fstore.forwardTo, userTypeOptions)}</Table.Cell>
														<Table.Cell>{getOptionText(fstore.skipParameter, formNameOptions)}</Table.Cell>
														<Table.Cell>
															<Button
																// circular
																inverted
																color="green"
																title="Edit"
																size="tiny"
																onClick={() => this.handleEdit(fstore)}
																icon="edit"
															></Button>

															<Button
																// circular
																inverted
																color="red"
																title="Delete"
																icon="remove circle"
																size="tiny"
																onClick={() => this.modalOpen(fstore.id)}
															></Button>
															<Modal onClose={this.modalClose} open={this.state.isModalOpen}>
																<Modal.Header>{forwardData.modal.heading}</Modal.Header>
																<Modal.Content>
																	<p>{forwardData.modal.content}</p>
																</Modal.Content>
																<Modal.Actions>
																	<Button
																		color="green"
																		onClick={() => this.handleDelete(this.state.idToDelete)}
																		positive
																		labelPosition="left"
																		icon="checkmark"
																		content="Yes"
																		title="Confirm Delete!"
																	></Button>
																	<Button
																		color="red"
																		onClick={this.modalClose}
																		negative
																		labelPosition="left"
																		icon="remove"
																		content="No"
																		title="Decline Delete!"
																	></Button>
																</Modal.Actions>
															</Modal>
														</Table.Cell>
													</Table.Row>
												);
											})}
											{/* <Table.Row>
                                                    <Table.Cell>1</Table.Cell>
                                                </Table.Row> */}
										</Table.Body>
									</Table>
								</Segment>
							</Form>
						)}
					</Formik>
				</>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}

const mapDispatchToProps = {
	getAdminDataByUrl,
	getForwardData,
	getGroupwiseForwardData,
	postForwardData,
	putForwardData,
	deleteForwardData,
};
const mapStateToProps = (state) => ({
	forwardMaster: state.root.admin.forwardMaster,
	formNameMaster: state.root.admin.formNameMaster,
	userTypeMaster: state.root.admin.userTypeMaster,
	group: state.root.admin.group,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(Forward);
