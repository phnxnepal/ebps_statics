import React from 'react';
import * as Yup from 'yup';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Label } from 'semantic-ui-react';
import { wardSetupData } from '../../utils/data/ebps-setup/formSetupData';
import { Formik, Field } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { EditButton, DeleteButton } from './forms/Buttons';
import { validateWardNo } from './../../utils/validationUtils';
import Helmet from 'react-helmet';

const modalData = wardSetupData.modal;
const tableHeadings = wardSetupData.table.tableHeadings;
const initialState = {
    id: '',
    name: '',
};
const schema = Yup.object().shape({
    name: validateWardNo,
});

const WardSetupComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
    const {
        isEditing,
        isDeleting,
        formState,
        openAddModal,
        openDeleteModal,
        handleAdd,
        handleEdit,
        handleDelete,
        setOpenAddModal,
        setOpenDeleteModal,
    } = useAdminSetup(initialState, success);
    return (
        <div>
            <Formik
                enableReinitialize
                initialValues={{ ...formState }}
                validationSchema={schema}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    try {
                        if (isEditing) {
                            putAdminDataByUrl(api.wardSetup, formState.id, values);
                        } else if (isDeleting) {
                            deleteAdminDataByUrl(api.wardSetup, formState.id);
                        } else {
                            postAdminDataByUrl(api.wardSetup, values);
                        }
                        actions.setSubmitting(false);
                    } catch (error) {
                        showErrorToast('Something went wrong.');
                        actions.setSubmitting(false);
                    }
                }}
            >
                {({ handleSubmit, isSubmitting, errors: formErrors }) => (
                    <>
                        <Helmet title={wardSetupData.table.tableHeader} />
                        <AdminModalContainer
                            open={openAddModal}
                            scroll={false}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={() => setOpenAddModal(false)}
                            title={isEditing ? modalData.titleEdit : modalData.titleAdd}
                            saveText={isEditing ? modalData.editText : modalData.saveText}
                            cancelText={modalData.cancelText}
                        >
                            <Form onSubmit={handleSubmit} loading={isSubmitting || loading}>
                                <Form.Group >
                                    <Form.Field error={!!formErrors.name}>
                                        {<span>{wardSetupData.enterWard}</span>}
                                        <Field name="name" autoFocus />
                                        {formErrors.name && (
                                            <Label pointing prompt size="large">
                                                {formErrors.name}
                                            </Label>
                                        )}
                                    </Form.Field>
                                </Form.Group>
                            </Form>
                        </AdminModalContainer>
                        <AdminModalContainer
                            open={openDeleteModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={() => setOpenDeleteModal(false)}
                            title={modalData.delete.title}
                            saveText={modalData.deleteText}
                            cancelText={modalData.cancelText}

                        >
                            <Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
                        </AdminModalContainer>
                    </>
                )}
            </Formik>
            <Segment inverted className="tableSectionHeader">
                {wardSetupData.table.tableHeader}
                <Button
                    basic
                    style={{ right: 6, top: 8, position: 'absolute' }}
                    inverted
                    type="reset"
                    onClick={() => {
                        handleAdd();
                    }}
                >
                    <ButtonContent>
                        <Icon.Group>
                            <Icon name="file outline" />
                            <Icon inverted corner name="add" />
                        </Icon.Group>
                        {`  ${modalData.titleAdd}`}
                    </ButtonContent>
                </Button>
            </Segment>
            <Table sortable striped celled compact="very" size="small">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
                        <Table.HeaderCell>{tableHeadings.ward}</Table.HeaderCell>
                        <Table.HeaderCell>{tableHeadings.actions}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {adminData.wardSetup.length > 0 ? (
                        adminData.wardSetup.map(row => (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.id}</Table.Cell>
                                <Table.Cell>{row.name}</Table.Cell>
                                <Table.Cell>
                                    <EditButton row={{ ...row, id: parseInt(row.id) }} handleEdit={handleEdit} />
                                    <DeleteButton handleDelete={handleDelete} row={row} />
                                </Table.Cell>
                            </Table.Row>
                        ))
                    ) : (
                            <Table.Row>
                                <Table.Cell colSpan="7">
                                    <Segment textAlign="center">No Data Found.</Segment>
                                </Table.Cell>
                            </Table.Row>
                        )}
                </Table.Body>
            </Table>
        </div>
    );
};

export const WardSetup = parentProps => (
    <AdminContainer
        api={[{ api: api.wardSetup, objName: 'wardSetup' }]}
        updateApi={[{ api: api.wardSetup, objName: 'wardSetup' }]}
        render={props => <WardSetupComponent {...props} parentProps={parentProps} />}
    />
);
