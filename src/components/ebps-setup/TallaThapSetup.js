import React, { useState, useEffect } from 'react';
import api from '../../utils/api';
import AdminContainer from '../../containers/base/AdminContainer';
import { Form, Button, Header, Divider, Tab } from 'semantic-ui-react';
import { Formik } from 'formik';
import ErrorDisplay from '../shared/ErrorDisplay';
import * as Yup from 'yup';
import { validateString, validateFile } from '../../utils/validationUtils';
import { SelectInput, SearchableSelectInput } from '../shared/Select';
import { UserType } from '../../utils/userTypeUtils';
import { isEmpty, showToast } from '../../utils/functionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import { FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import { constructionTypeSelectOptions } from '../../utils/optionUtils';
import { useFiscalYearAll } from '../../hooks/useFiscalYear';
import { tallaThapData, tallaThapColumns, tallaThapDatafield } from '../../utils/data/ebps-setup/tallaThapData';
import EbpsForm, { EbpsNormalForm } from '../shared/EbpsForm';
import { TallaThapHistory } from './TallaThapHistory';
import { useJqxNepaliFilter } from '../../hooks/useJqGridButton';
import ApplicationDetails from './shared/ApplicationDetails';
import Helmet from 'react-helmet';
import GenericTableOneButton from './forms/GenericTableOneButton';

// const searchSchema = Yup.object().shape({
// 	applicationNo: validateNormalNumber,
// });

const changeDesignerSchema = Yup.object().shape({
	designer: validateString,
	file: validateFile,
});

const applicationData = tallaThapData.applicationInfo;
const modalData = tallaThapData.modal;

const initialValues = {
	nibedakName: '',
	constructionType: '',
	year: '',
	applicationNo: '',
};

export const TallaThapSetupComponent = ({ getAfterUpdateAdminData, postAdminDataByUrl, errors, success, loading, adminData }) => {
	const [designerOption, setDesignerOption] = useState();
	const [appInfo, setAppInfo] = useState({});
	const [appList, setAppList] = useState([]);
	const [openModal, setOpenModal] = useState(false);
	const [historyList, sethistoryList] = useState([]);
	const [isChanging, setIsChanging] = useState(false);
	const [dataChanged] = useState(false);

	const { fy, fyOption } = useFiscalYearAll(adminData.fiscalYear, 'yearCode', true);
	useJqxNepaliFilter(dataChanged);

	useEffect(() => {
		if (success && success.success) {
			showToast('Data saved successfully');
			setOpenModal(false);
			setIsChanging(false);
			setAppInfo({});
			setAppList([]);
			getAfterUpdateAdminData([
				{
					api: `${api.completedPermitList}?applicationNo=&nibedakName=&constructionType=&year=`,
					objName: 'applicationList',
				},
			]);
		}
	}, [success, getAfterUpdateAdminData]);

	useEffect(() => {
		adminData.applicationList && setAppList(adminData.applicationList);
	}, [adminData.applicationList]);

	useEffect(() => {
		adminData.historyList && sethistoryList(adminData.historyList);
	}, [adminData.historyList]);

	useEffect(() => {
		adminData.designers &&
			setDesignerOption(
				adminData.designers
					.filter((user) => user.userType && user.userType.id === UserType.DESIGNER)
					.map((row) => {
						return { value: row.loginId, text: `${row.userName} - ${row.email}` };
					})
			);
	}, [adminData.designers]);

	const handleButtonClick = (id) => {
		const row = appList[id];
		handleConfirmation(row);
	};

	const handleConfirmation = (row) => {
		setAppInfo(row);
		setOpenModal(true);
	};

	const handleChangeDesigner = () => {
		setIsChanging(true);
	};

	const handleClose = () => {
		setAppInfo({});
		setIsChanging(false);
		setOpenModal(false);
	};

	const panes = [
		{
			menuItem: 'डिजाइनर छान्नुहोस्',
			render: () => (
				<Tab.Pane className="simple-tab-pane">
					<Helmet title={tallaThapData.tallaThapInfo.title} />
					<Formik
						key="get-app-info"
						initialValues={{ ...initialValues, year: fy }}
						// validationSchema={searchSchema}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);
							try {
								setAppList([]);
								getAfterUpdateAdminData([
									{
										api: `${api.completedPermitList}?applicationNo=${values.applicationNo}&nibedakName=${values.nibedakName}&constructionType=${values.constructionType}&year=${values.year}`,
										objName: 'applicationList',
									},
								]);
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('err', err);
							}
						}}
					>
						{({ handleSubmit, errors, isSubmitting, values, setFieldValue, handleChange }) => (
							<Form loading={isSubmitting || loading}>
								<Form.Group widths="4">
									{/* <Form.Field error={!!errors.applicationNo}>
								<Field type="text" name="applicationNo" placeholder="Enter Application Number" />
								{errors.applicationNo && (
									<Label pointing prompt size="large">
										{errors.applicationNo}
									</Label>
								)}
							</Form.Field> */}
									<Form.Field>
										<EbpsNormalForm
											name="applicationNo"
											label={applicationData.applicationId}
											onChange={handleChange}
											errors={errors.applicationNo}
											value={values.applicationNo}
										/>
									</Form.Field>
									<Form.Field>
										<EbpsForm
											name="nibedakName"
											label={applicationData.nibedakName}
											setFieldValue={setFieldValue}
											errors={errors.nibedakName}
											value={values.nibedakName}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput
											needsZIndex={true}
											name="constructionType"
											label={applicationData.constructionType}
											options={constructionTypeSelectOptions}
										/>
									</Form.Field>
									<Form.Field>
										<SelectInput needsZIndex={true} name="year" label={applicationData.year} options={fyOption} />
									</Form.Field>
								</Form.Group>
								<Form.Group widths="equal">
									<Form.Field>
										<Button className="primary-btn" icon="search" content="Search Application" onClick={handleSubmit} />
									</Form.Field>
								</Form.Group>
							</Form>
						)}
					</Formik>
					<h4>Search Results</h4>
					<GenericTableOneButton
						data={appList}
						columns={tallaThapColumns}
						datafield={tallaThapDatafield}
						buttonProps={{
							class: 'primary-table-single-btn icon',
							icon: 'add user icon',
							title: 'Assign Designer',
						}}
						handleButtonClick={handleButtonClick}
						actionWidth={100}
					/>
					<br />
					{!isEmpty(appInfo) && (
						<div>
							<Divider />
							<Formik
								key="assign-designer"
								validationSchema={changeDesignerSchema}
								onSubmit={(values, actions) => {
									actions.setSubmitting(true);
									const data = new FormData();
									const selectedFile = values.file;

									if (selectedFile) {
										selectedFile.forEach((file) => data.append('file', file));
									}

									data.append('applicationNo', appInfo.applicantNo);
									data.append('designer', values.designer);

									try {
										postAdminDataByUrl(`${api.completedPermitList}`, data);
										actions.setSubmitting(false);
									} catch (err) {
										actions.setSubmitting(false);
										console.log('err', err);
									}
								}}
							>
								{({ handleSubmit, isSubmitting, errors: formErrors, validateForm }) => (
									<>
										{/* // <Form> */}
										<AdminModalContainer
											key={'open-confirmation'}
											open={openModal}
											errors={errors}
											isSubmitting={isSubmitting || loading}
											handleSubmit={handleChangeDesigner}
											handleClose={handleClose}
											title={modalData.title}
											saveText={modalData.saveText}
											cancelText={modalData.cancelText}
										>
											{modalData.content}
										</AdminModalContainer>

										<AdminModalContainer
											open={isChanging}
											errors={errors}
											isSubmitting={isSubmitting || loading}
											handleSubmit={() => {
												validateForm().then((errors) => {
													if (isEmpty(errors)) {
														handleSubmit();
													}
												});
											}}
											handleClose={handleClose}
											title={modalData.title}
											saveText={modalData.confirmChange}
											cancelText={modalData.cancelText}
										>
											<Form loading={isSubmitting || loading}>
												<Form.Group widths="equal">
													<Form.Field error={!!formErrors.designer}>
														<label>{modalData.selectDesigner}</label>
														<SearchableSelectInput name="designer" options={designerOption} />
													</Form.Field>
													<Form.Field error={!!formErrors.file}>
														<label>{modalData.selectFile}</label>
														<FileInputWithMultiplePreview
															name="file"
															fileCatId={'assign-designer'}
															hasMultipleFiles={false}
														/>
													</Form.Field>
												</Form.Group>
												<ApplicationDetails appInfo={appInfo} />
											</Form>
										</AdminModalContainer>
										{/* // </Form> */}
									</>
								)}
							</Formik>
						</div>
					)}
				</Tab.Pane>
			),
		},
		{
			menuItem: 'तला थप History',
			render: () => (
				<Tab.Pane className="simple-tab-pane">
					<Helmet title={tallaThapData.tallaThapInfo.storeyHistory} />
					<TallaThapHistory historyList={historyList} />
				</Tab.Pane>
			),
		},
	];

	return (
		<div className="setup-main">
			{errors && <ErrorDisplay message={errors.message} />}
			<Header>{tallaThapData.tallaThapInfo.title}</Header>
			<Tab menu={{ secondary: true, pointing: true }} panes={panes} />
		</div>
	);
};

export const TallaThapSetup = (parentProps) => (
	<AdminContainer
		api={[
			{ api: api.organizationUser, objName: 'designers' },
			{ api: api.fiscalYear, objName: 'fiscalYear' },
			{ api: `${api.tallaThapList}?status=`, objName: 'historyList' },
		]}
		updateApi={[{ api: `${api.tallaThapList}?status=`, objName: 'historyList' }]}
		render={(props) => <TallaThapSetupComponent {...props} parentProps={parentProps} />}
	/>
);

export { TallaThapSetup as default };
