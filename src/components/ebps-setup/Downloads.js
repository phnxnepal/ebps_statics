import React, { Component } from 'react'
import { Formik } from "formik";
import { downloads } from '../../utils/data/ebps-setup/downloadsFormdata';
import { Button, Modal, Form, Segment, Table} from 'semantic-ui-react';
import EbpsForm from '../shared/EbpsForm'
import { FileInputWithMultiplePreview } from '../shared/FileUploadInput';
import { getDocUrl } from '../../utils/config';
import { validateString, validateFile } from '../../utils/validationUtils';
import { getDownloads, postDownloads, putDownloads, deleteDownloads } from '../../store/actions/AdminAction';
import { connect } from 'react-redux';
import FallbackComponent from '../shared/FallbackComponent';
import ErrorDisplay from '../shared/ErrorDisplay';
import {
    showToast
} from "../../utils/functionUtils";

import * as Yup from 'yup'
import Helmet from 'react-helmet';
const down = downloads.downloads_data;
// const filterFields = ['file'];

const strData = ['fileType',];
const fileData = ['file'];

const strSchema = strData.map(row => {
    return {
        [row]: validateString
    };
});
const fileSchema = fileData.map(row => {
    return {
        [row]: validateFile
    };
});
const downloadSchema = Yup.object().shape(
    Object.assign({}, ...strSchema, ...fileSchema)
);

class Downloads extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentUser: {
                id: ''
            },
            isEditing: false,
            isModelOpen: false,
            idToDelete: '',

        }
    }
    modelClose = () => {
        this.setState({
            isModelOpen: false
        })
    }
    modelOpen = (id) => {
        this.setState({ isModelOpen: true, idToDelete: id })
    }

    handleEdit = (obj) => {
        console.log("User:", obj)
        window.scrollTo(0, 0);
        this.setState({
            currentUser: {
                fileType: obj.fileType,
                file: obj.file,
                id: obj.id
            },
            isEditing: true
        })
    }
    handleDelete = async (id) => {
        try {
            await this.props.deleteDownloads(id)
            if (this.props.success && this.props.success.success) {
                showToast('Your data has been successfully deleted.');
            }
            this.modelClose();

        } catch (err) {
            console.log("Error", err)
            this.modelClose();

        }

    }
    componentDidMount() {
        this.props.getDownloads();
    };
    componentDidUpdate(prevProps) {
        if (prevProps.success !== this.props.success) {
            setTimeout(() => {
                this.props.getDownloads()
            }, 0)
        }
    }
    render() {
        const { postDownloads, userData, putDownloads } = this.props;

        if (userData) {
            return (
                <Formik
                    validationSchema={downloadSchema}
                    enableReinitialize
                    initialValues={this.state.currentUser}
                    // onSubmit={(values, { setSubmitting }) => {
                    //     console.log(values);
                    //     setSubmitting(false);
                    // }}
                    onSubmit={async (values, actions) => {
                        // console.log('hellooooooo');

                        actions.setSubmitting(true);
                        values.error && delete values.error;
                        const data = new FormData();
                        const selectedFile = values.file;
                        if (selectedFile) {
                            for (var x = 0; x < selectedFile.length; x++) {
                                data.set('file', selectedFile[x]);
                            }
                            // console.log("selectedFile", selectedFile);
                            // isFileSelected = true;
                        }
                        data.set('fileType', values.fileType)
                        // console.log(values);

                        try {
                            if (this.state.isEditing) {
                                console.log('isediting', this.state.currentUser.id)
                                await putDownloads(this.state.currentUser.id, data)
                            }
                            else {
                                // await postDownloads(data);
                                await postDownloads(data)
                            }
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            if (this.props.success && this.props.success.success) {
                                showToast('Your data has been successfully posted.');
                            }
                        }
                        catch (err) {
                            console.log('err rroo', err);
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            showToast('Something went wrong !!');
                        }
                        // console.log("props", this.props);

                    }
                    }
                >
                    {({ handleChange, values, handleSubmit, errors, setFieldValue, isSubmitting }) => (
                        <Form loading={isSubmitting}>
                            {this.props.errors && (
                                <ErrorDisplay
                                    message={this.props.errors.message}
                                />
                            )}
                            <Helmet>
                                <title>
                                    Add Downloads
                            </title>
                            </Helmet>
                            <Segment>
                                <Segment inverted className="tableSectionHeader">{down.heading}</Segment>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <label><strong>{down.data.data_1}</strong></label>
                                        <EbpsForm
                                            name="fileType"
                                            setFieldValue={setFieldValue}
                                            onChange={handleChange}
                                            error={errors.fileType}
                                            value={values.fileType} />
                                    </Form.Field>
                                    <Form.Field>
                                        <label><strong>{down.data.data_2}</strong></label>
                                        <FileInputWithMultiplePreview
                                            name="file"
                                            fileCatId={'downloads'}
                                            hasMultipleFiles={false}
                                            setFieldValue={setFieldValue}
                                        // error={errors.file}
                                        />
                                        {/* {values.file && (
                                            <List relaxed>
                                                <List.Header>{down.existing}</List.Header>
                                                <List.Item
                                                    key={values.file}
                                                    content={values.file}
                                                    icon="file alternate outline"
                                                    target="_blank"
                                                    href={`${getDocUrl()}${values.file}`}
                                                ></List.Item>
                                            </List>)} */}
                                    </Form.Field>
                                    {/* <Form.Field>
                                        <label><strong>{down.data.data_3}</strong></label>
                                        <EbpsFormDateField
                                            name="fileUploadDate"
                                            handleChange={handleChange}
                                            error={errors.fileUploadDate}
                                            value={values.fileUploadDate} />
                                    </Form.Field> */}
                                </Form.Group>
                                <br />
                                <br />
                                <Button type="submit" className="fileStorageSegment" onClick={handleSubmit}>
                                    {this.state.isEditing ? 'Update' : 'Save'}
                                </Button>
                            </Segment>
                            <Segment raised>
                                <Segment className="tableSectionHeader">{down.downloads}</Segment>
                                <Table striped celled selectable style={{ textAlign: 'center' }}>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell>{down.down_data.data_1}</Table.HeaderCell>
                                            <Table.HeaderCell>{down.down_data.data_2}</Table.HeaderCell>
                                            <Table.HeaderCell>{down.down_data.data_3}</Table.HeaderCell>
                                            <Table.HeaderCell>{down.down_data.data_4}</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    <Table.Body>
                                        {this.props.userData.map((down, index) => {
                                            return (
                                                <Table.Row key={index}>
                                                    <Table.Cell>{down.fileType}</Table.Cell>
                                                    <Table.Cell>{down.fileUrl}</Table.Cell>
                                                    <Table.Cell>{down.uploadDate}</Table.Cell>
                                                    <Table.Cell>
                                                        <Button
                                                            // circular
                                                            inverted
                                                            color='green'
                                                            title='Edit'
                                                            size='tiny'
                                                            onClick={() => this.handleEdit(down)}
                                                            icon='edit'>
                                                        </Button>
                                                        <Button
                                                            // circular
                                                            inverted
                                                            color='red'
                                                            title='Delete'
                                                            icon='remove circle'
                                                            size='tiny'
                                                            onClick={() => this.modelOpen(down.id)}
                                                        >
                                                        </Button>
                                                        <Modal close={this.modelClose} open={this.state.isModelOpen}>
                                                            <Modal.Content>
                                                                <p>Do you want to delete this section?</p>
                                                            </Modal.Content>
                                                            <Modal.Actions>
                                                                <Button color='green' onClick={() => this.handleDelete(this.state.idToDelete)} positive
                                                                    labelPosition='left'
                                                                    icon='checkmark'
                                                                    content='Yes'
                                                                    title="Confirm Delete!">
                                                                </Button>
                                                                <Button color='red' onClick={this.modelClose} negative
                                                                    labelPosition='left'
                                                                    icon='remove'
                                                                    content='No'
                                                                    title="Decline Delete!">
                                                                </Button>
                                                            </Modal.Actions>
                                                        </Modal>
                                                        <Button
                                                            // circular
                                                            inverted
                                                            color='blue'
                                                            title='Download'
                                                            size='tiny'
                                                            icon='download'
                                                            as='a'
                                                            href={`${getDocUrl()}${
                                                                down.fileUrl
                                                                }`}
                                                            target="_blank"
                                                            rel="noopener noreferrer" />
                                                    </Table.Cell>
                                                </Table.Row>
                                            )
                                        })
                                        }
                                    </Table.Body>
                                </Table>
                            </Segment>
                        </Form>
                    )
                    }
                </Formik>
            );
        } else {
            return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />
        }
    }
}
const mapDispatchToProps = { getDownloads, postDownloads, putDownloads, deleteDownloads }
const mapStateToProps = state => ({
    userData: state.root.admin.userData,
    loading: state.root.ui.loading,
    errors: state.root.ui.errors,
    success: state.root.admin.success
});
export default connect(
    mapStateToProps,
    mapDispatchToProps)(Downloads);

