import React, { Component } from 'react';
import { Formik } from 'formik';
import { connect } from 'react-redux';
import { Form, Button, Table, Segment, Grid, Divider, Select } from 'semantic-ui-react';
import { showToast } from '../../utils/functionUtils';
import FallbackComponent from '../shared/FallbackComponent';
import EbpsSelect from '../shared/Select';
import { getFormNameMasterData, postFormName, putFormName, deleteFormName } from '../../store/actions/AdminAction';
import { UserType, approveColumnMapping, getAdminFilteredOptions } from '../../utils/userTypeUtils';
import ErrorDisplay from '../shared/ErrorDisplay';
import * as Yup from 'yup';
import { validateString } from '../../utils/validationUtils';
import Helmet from 'react-helmet';
import { isStringEmpty } from '../../utils/stringUtils';
import { FlexSingleRight } from '../uiComponents/FlexDivs';
import { getOptionText } from '../../utils/dataUtils';
// const enterOpt = [
// 	{ key: 'a', value: UserType.ENGINEER, text: 'Engineer' },
// 	{ key: 'b', value: UserType.SUB_ENGINEER, text: 'Sub Engineer' },
// 	{ key: 'c', value: UserType.ADMIN, text: 'Chief' },
// 	{ key: 'd', value: UserType.DESIGNER, text: 'Designer' },
// 	{ key: 'e', value: UserType.RAJASWO, text: 'Rajaswo' },
// 	{ key: 'f', value: UserType.AMIN, text: 'Amin' },
// ];
const approvalOpt = [
	{ key: 'a', value: 'Y', text: 'Yes' },
	{ key: 'b', value: 'N', text: 'No' },
];

class FormName extends Component {
	constructor(props) {
		super(props);
		this.state = {
			enterByOptions: [],
			approvalUsers: [],
			formData: {
				enterBy: UserType.ENGINEER,
				approval: 'Y',
				subEng: 'Y',
				engineer: 'Y',
				amin: 'Y',
				rajaswo: 'Y',
				chief: 'Y',
				id: '',
			},
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
			enterByFilter: '',
			formList: [],
		};
	}
	modalOpen = id => {
		this.setState({ isModalOpen: true, idToDelete: id });
	};
	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	componentDidMount() {
		// console.log('this.state', this.state);
		this.props.getFormNameMasterData();
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.userTypeMaster !== this.props.userTypeMaster && this.props.userTypeMaster) {
			const userTypeMaster = this.props.userTypeMaster;
			const enterByOptions = getAdminFilteredOptions(userTypeMaster);
			const approvalUsers = userTypeMaster
				.filter(user => !isStringEmpty(user.approveColumn))
				.map(user => ({ designation: user.designationNepali || user.designation, id: user.id, field: approveColumnMapping[user.approveColumn] }));
			this.setState({
				enterByOptions,
				approvalUsers,
			});
		}

		if (this.props.formNames !== prevProps.formNames && this.props.formNames) {
			this.setState({ formList: this.props.formNames });
		}

		if (this.state.enterByFilter !== prevState.enterByFilter && this.state.enterByFilter) {
			this.setState({ formList: this.props.formNames.filter(row => row.enterBy === this.state.enterByFilter) });
		}

		if (prevProps.success !== this.props.success) {
			setTimeout(() => {
				this.props.getFormNameMasterData(true);
			}, 0);
			this.setState({ isEditing: false });
		}
	}

	handleEdit = user => {
		// console.log("click", User)
		window.scrollTo(0, 0);
		this.setState({
			formData: {
				name: user.name,
				pageCode: user.pageCode,
				viewUrl: user.viewUrl,
				enterBy: user.enterBy,
				tableName: user.tableName,
				designerApproval: user.designerApproval,
				subEngrApproval: user.subEngrApproval,
				engrApproval: user.engrApproval,
				aminApproval: user.aminApproval,
				rajasowApproval: user.rajasowApproval,
				chiefApproval: user.chiefApproval,
				posteApproval: user.posteApproval,
				postfApproval: user.postfApproval,
				postgApproval: user.postgApproval,
				id: user.id,
			},
			isEditing: true,
		});
	};
	handleDelete = async id => {
		try {
			await this.props.deleteFormName(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};
	// handleEnterTypeSelect = enterType => {
	//     this.props.getFormGroupData(enterType);
	// };
	render() {
		const { enterByOptions, approvalUsers, enterByFilter, formList } = this.state;
		const strData = ['name'];
		const formDataSchema = strData.map(row => {
			return {
				[row]: validateString,
			};
		});
		const FormNameSchema = Yup.object().shape(Object.assign({}, ...formDataSchema));
		if (this.props.formNames) {
			return (
				<Formik
					enableReinitialize
					validationSchema={FormNameSchema}
					initialValues={this.state.formData}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						try {
							if (this.state.isEditing) {
								// console.log('formData', this.state.formData)
								await this.props.putFormName(`${this.state.formData.id}`, values);
							} else {
								await this.props.postFormName(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully added');
								actions.resetForm();
								this.setState({ formData: { enterBy: UserType.ENGINEER } });
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, setFieldValue, values, isSubmitting, handleReset }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Helmet title="Form Name" />
							<Segment>
								<Segment inverted className="tableSectionHeader">
									Form Name
								</Segment>
								<Form.Group widths="equal">
									<Form.Field>
										<b>Name</b>
										<input value={values.name} />
									</Form.Field>
									<Form.Field>
										<b>View URL</b>
										<input value={values.viewUrl} />
									</Form.Field>
								</Form.Group>
								<Form.Group widths="4">
									<Form.Field>
										<b>Code</b>
										<input value={values.pageCode} />
									</Form.Field>
									<Form.Field>
										<b>Table Name</b>
										<input
											// name="tableName"
											// onChange={handleChange}
											// onChange={(e, { value }) => {
											//     setFieldValue('tableName', value);
											// }}
											// error={errors.tableName}
											value={values.tableName}
										/>
									</Form.Field>
									<Form.Field>
										<b>Enter By</b>
										<EbpsSelect
											name="enterBy"
											options={enterByOptions}
											onChange={(e, { value }) => {
												setFieldValue('enterBy', value);
											}}
											value={values.enterBy}
										/>
									</Form.Field>
								</Form.Group>
								<Grid>
									<Grid.Row className="div-indent-one">
										<Divider horizontal>Approval Permissions</Divider>
									</Grid.Row>
									<Grid.Row>
										{approvalUsers.map(user => (
											<Grid.Column key={user.id} computer={4} tablet={8} mobile={16}>
												<Form.Group>
													<Form.Field>
														<b>{user.designation} Approval</b>
														<EbpsSelect
															name={user.field}
															options={approvalOpt}
															onChange={(e, { value }) => {
																setFieldValue(user.field, value);
															}}
															value={values[user.field]}
														/>
													</Form.Field>
												</Form.Group>
											</Grid.Column>
										))}
									</Grid.Row>
								</Grid>
								<br />
								<Button type="submit" disabled={!this.state.isEditing} onClick={handleSubmit} className="fileStorageSegment">
									Update
								</Button>
							</Segment>
							{/* <br /> */}
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									Form Name Data
								</Segment>
								<FlexSingleRight>
									Enter By:{' '}
									<Select
										name="enterByFilter"
										options={enterByOptions}
										onChange={(e, { value }) => {
											this.setState({ enterByFilter: value });
										}}
										value={enterByFilter}
									/>
								</FlexSingleRight>
								<Table striped celled selectable style={{ textAlign: 'center' }}>
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell>Name</Table.HeaderCell>
											<Table.HeaderCell>Page Code</Table.HeaderCell>
											<Table.HeaderCell>View URL</Table.HeaderCell>
											<Table.HeaderCell>Enter By</Table.HeaderCell>
											<Table.HeaderCell>Table Name</Table.HeaderCell>
											<Table.HeaderCell>Action</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									<Table.Body>
										{formList.map((f, index) => {
											return (
												<Table.Row key={index}>
													<Table.Cell>{f.name}</Table.Cell>
													<Table.Cell>{f.pageCode}</Table.Cell>
													<Table.Cell>{f.viewUrl}</Table.Cell>
													<Table.Cell>{getOptionText(f.enterBy, enterByOptions)}</Table.Cell>
													<Table.Cell>{f.tableName}</Table.Cell>
													<Table.Cell>
														<Button
															circular
															title="Edit"
															color="green"
															onClick={() => {
																handleReset();
																this.setState({ formData: { enterBy: UserType.ENGINEER } });
																this.handleEdit(f);
															}}
															icon="edit"
															size="tiny"
															// style={{ marginBottom: '2px' }}
														></Button>

														{/* <Button 
                                                            circular
                                                            color="red"
                                                            title="Delete"
                                                            icon="remove circle"
                                                            onClick={() => this.modalOpen(f.id)}
                                                            size="tiny"
                                                        ></Button>

                                                        <Modal
                                                            close={this.modalClose}
                                                            open={this.state.isModalOpen}
                                                        >
                                                            <Modal.Content>
                                                                <p>Do you want to delete this section?</p>
                                                            </Modal.Content>
                                                            <Modal.Actions>
                                                                <Button
                                                                    color="green"
                                                                    onClick={() =>
                                                                        this.handleDelete(this.state.idToDelete)
                                                                    }
                                                                    positive
                                                                    labelPosition="left"
                                                                    icon="checkmark"
                                                                    content="Yes"
                                                                    title="Confirm Delete!"
                                                                ></Button>
                                                                <Button
                                                                    color="red"
                                                                    onClick={this.modalClose}
                                                                    negative
                                                                    labelPosition="left"
                                                                    icon="remove"
                                                                    content="No"
                                                                    title="Decline Delete!"
                                                                ></Button>
                                                            </Modal.Actions>
                                                                </Modal> */}
													</Table.Cell>
												</Table.Row>
											);
										})}
									</Table.Body>
								</Table>
							</Segment>
						</Form>
					)}
				</Formik>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = {
	getFormNameMasterData,
	postFormName,
	putFormName,
	deleteFormName,
};
const mapStateToProps = state => ({
	formNames: state.root.admin.formNames,
	userTypeMaster: state.root.admin.userTypeMaster,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(FormName);
