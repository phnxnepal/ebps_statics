import React, { Component } from 'react';
import { fileStorage } from '../../utils/data/ebps-setup/fileStorageData';
import { Formik } from 'formik';
import { Form, Table, Button, Segment, Modal, Label, Select, ButtonContent, Icon } from 'semantic-ui-react';
import { EbpsFormIm } from '../shared/EbpsForm';
import { getFileStorage, postFileStorage, getFormNameMasterData, putFileStorage, deleteFileStorage } from '../../store/actions/AdminAction';
import { connect } from 'react-redux';
import FallbackComponent from '../shared/FallbackComponent';
import * as Yup from 'yup';
import { validateString } from '../../utils/validationUtils';
//import api from "../../utils/api";
import { showToast } from '../../utils/functionUtils';
// import FormWrapper from "../shared/FormWrapper";
import ErrorDisplay from '../shared/ErrorDisplay';
import { setTimeout } from 'timers';
import Helmet from 'react-helmet';
import { SearchableDropdown } from '../shared/formComponents/SearchableDropdown';
const fileStore = fileStorage.fileStorage_data;
const Multiple = [
	{ key: 'm1', value: 'Y', text: 'Yes' },
	{ key: 'm2', value: 'N', text: 'No' },
];

class FileStorage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentUser: {
				id: '',
			},
			isEditing: false,
			isModelOpen: false,
			idToDelete: '',
		};
	}
	modelClose = () => {
		this.setState({
			isModelOpen: false,
		});
	};
	modelOpen = (id) => {
		this.setState({ isModelOpen: true, idToDelete: id });
	};
	handleEdit = (obj) => {
		console.log('User:', obj);
		window.scrollTo(0, 0);
		this.setState({
			currentUser: {
				name: obj.name,
				isMultiple: obj.isMultiple,
				formName: obj.formName.id,
				isRequired: obj.isRequired,
				id: obj.id,
			},
			isEditing: true,
		});
	};
	handleDelete = async (id) => {
		try {
			await this.props.deleteFileStorage(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
			}
			this.modelClose();
		} catch (err) {
			console.log('Error', err);
			this.modelClose();
		}
	};
	componentDidMount() {
		this.props.getFileStorage();
		this.props.getFormNameMasterData();
	}
	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			setTimeout(() => {
				this.props.getFileStorage();
				this.props.getFormNameMasterData();
			}, 0);
		}
	}

	// componentDidUpdate(prevProps, prevState) {
	//     console.log('prev --', prevProps, prevState)
	//     console.log('prev --', this.props, this.state)
	// }
	render() {
		const strData = ['name', 'isMultiple', 'formName', 'isRequired'];
		const formDataSchema = strData.map((row) => {
			return {
				[row]: validateString,
			};
		});
		const FileStorageSchema = Yup.object().shape(Object.assign({}, ...formDataSchema));
		const { postFileStorage, formNames, userData, putFileStorage } = this.props;
		if (formNames && userData) {
			let formOption = [];
			const listdata = formNames;
			listdata.map((row) =>
				formOption.push({
					value: row.id,
					text: row.name,
				})
			);
			return (
				<Formik
					enableReinitialize
					validationSchema={FileStorageSchema}
					initialValues={this.state.currentUser}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						values.formName = values.formName.toString();
						console.log(values);
						try {
							if (this.state.isEditing) {
								await putFileStorage(this.state.currentUser.id, values);
							} else {
								await postFileStorage(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully posted.');
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ values, setFieldValue, handleSubmit, handleChange, errors, handleReset }) => (
						<Form>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Helmet>
								<title>File Storage Category</title>
							</Helmet>
							<Segment raised>
								<Segment inverted className="tableSectionHeader">
									File Storage Category
									<Button
										basic
										style={{ right: 6, top: 6, position: 'absolute' }}
										inverted
										type="reset"
										onClick={() => {
											handleReset();
											this.setState({ currentUser: {}, isEditing: false });
										}}
									>
										<ButtonContent>
											<Icon.Group>
												<Icon name="file outline" />
												<Icon inverted corner name="add" />
											</Icon.Group>{' '}
											Add File Category
										</ButtonContent>
									</Button>
								</Segment>
								{/* <Grid> */}
								<Form.Group widths="equal">
									<Form.Field>
										{/* {fileStore.header.header_1}
                                                <Field type="text" name="name"></Field> */}
										<strong>{fileStore.header.header_1}</strong>
										<EbpsFormIm
											// label={fileStore.header.header_1}
											name="name"
											setFieldValue={setFieldValue}
											error={errors.name}
											value={values.name}
										/>
									</Form.Field>
									{/* <Grid.Column> */}
									{/* <strong>{fileStore.header.header_2}</strong>
                                                <Select placeholder='Yes' options={Multiple} name="isMultiple" value={values.isMultiple} style={{ width: '100%' }} onChange={(e, { value }) => {
                                                    setFieldValue("isMultiple", value);
                                                }} /> */}
									<Form.Field error={!!errors.isMultiple}>
										<strong>{fileStore.header.header_2}</strong>
										<br />
										<Select
											placeholder="Yes"
											value={values.isMultiple}
											name="isMultiple"
											options={Multiple}
											onChange={(e, { value }) => {
												setFieldValue('isMultiple', value);
											}}
										/>
										{errors.isMultiple && (
											<Label pointing prompt size="large">
												{errors.isMultiple}
											</Label>
										)}
									</Form.Field>
									{/* </Grid.Column> */}
									{/* <Grid.Column> */}
									{/* <strong> {fileStore.header.header_3}</strong> */}
									{/* <Select placeholder='निवेदन' options={formOption} name="formName" value={values.formName} style={{ width: '100%' }} onChange={(e, { value }) => {
                                                    setFieldValue("formName", value);
                                                }} /> */}
									<Form.Field error={!!errors.formName}>
										<strong>{fileStore.header.header_3}</strong>
										<br />
										<SearchableDropdown
											placeholder="संसोधन बिल भुक्तानी"
											value={values.formName}
											name="formName"
											options={formOption}
											handleChange={(value) => {
												setFieldValue('formName', value);
											}}
										/>
										{errors.formName && (
											<Label pointing prompt size="large">
												{errors.formName}
											</Label>
										)}
									</Form.Field>

									{/* </Grid.Column> */}
									<Form.Field error={!!errors.isRequired}>
										<strong>{fileStore.header.header_5}</strong>
										<br />
										<Select
											placeholder="Yes"
											value={values.isRequired}
											name="isRequired"
											options={Multiple}
											onChange={(e, { value }) => {
												setFieldValue('isRequired', value);
											}}
										/>
										{errors.isRequired && (
											<Label pointing prompt size="large">
												{errors.isRequired}
											</Label>
										)}
									</Form.Field>
								</Form.Group>

								{/* </Grid> */}
								<br />
								<Button onClick={handleSubmit} className="fileStorageSegment">
									{this.state.isEditing ? 'Update' : 'Save'}
								</Button>
							</Segment>
							<br />
							<Segment raised>
								<Segment className="tableSectionHeader">File Storage Category Data</Segment>
								<Table striped celled selectable style={{ textAlign: 'center' }}>
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell> {fileStore.header.header_1}</Table.HeaderCell>
											<Table.HeaderCell> {fileStore.header.header_2}</Table.HeaderCell>
											<Table.HeaderCell> {fileStore.header.header_3}</Table.HeaderCell>
											<Table.HeaderCell> {fileStore.header.header_5}</Table.HeaderCell>
											<Table.HeaderCell> {fileStore.header.header_4}</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									<Table.Body>
										{this.props.userData.map((fstore, index) => {
											return (
												<Table.Row key={index}>
													<Table.Cell>{fstore.name}</Table.Cell>
													<Table.Cell>{fstore.isMultiple}</Table.Cell>
													<Table.Cell>{fstore.formName && fstore.formName.name}</Table.Cell>
													<Table.Cell>{fstore.isRequired}</Table.Cell>
													<Table.Cell>
														<Button
															// circular
															inverted
															color="green"
															title="Edit"
															size="tiny"
															onClick={() => this.handleEdit(fstore)}
															icon="edit"
														></Button>
														{!fstore.staticCategoryIdentifier ? (
															<Button
																// circular
																inverted
																color="red"
																title="Delete"
																icon="remove circle"
																size="tiny"
																onClick={() => this.modelOpen(fstore.id)}
															></Button>
														) : null}
														<Modal close={this.modelClose} open={this.state.isModelOpen}>
															<Modal.Content>
																<p>Do you want to delete this section?</p>
															</Modal.Content>
															<Modal.Actions>
																<Button
																	color="green"
																	onClick={() => this.handleDelete(this.state.idToDelete)}
																	positive
																	labelPosition="left"
																	icon="checkmark"
																	content="Yes"
																	title="Confirm Delete!"
																></Button>
																<Button
																	color="red"
																	onClick={this.modelClose}
																	negative
																	labelPosition="left"
																	icon="remove"
																	content="No"
																	title="Decline Delete!"
																></Button>
															</Modal.Actions>
														</Modal>
													</Table.Cell>
												</Table.Row>
											);
										})}
									</Table.Body>
								</Table>
							</Segment>
						</Form>
					)}
				</Formik>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = { getFileStorage, postFileStorage, getFormNameMasterData, putFileStorage, deleteFileStorage };
const mapStateToProps = (state) => ({
	formNames: state.root.admin.formNames,
	userData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(FileStorage);
