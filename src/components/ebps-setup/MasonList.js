import React from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { Segment, Table, Button, ButtonContent, Icon, Form, Grid } from 'semantic-ui-react';
import { MasonListData } from "../../utils/data/ebps-setup/archiBMasterData";
import { Formik } from 'formik';
import { AdminModalContainer } from './AdminModalContainer';
import { SelectInput } from '../shared/Select';
import { showErrorToast } from '../../utils/functionUtils';
import { useAdminSetup } from '../../hooks/useAdminSetup';
import { getOptionText } from '../../utils/dataUtils';
import EbpsForm, { EbpsSlashForm } from '../shared/EbpsForm';
import { validateNullableNumber, validateString, validateNullableOfficialNumbers } from '../../utils/validationUtils';
import * as Yup from 'yup';
import Helmet from 'react-helmet';
import { FileInputWithMultiplePreview } from './../shared/FileUploadInput';
import ImagePreview from './../shared/ImagePreview';

const masonTableData = MasonListData.MasonListTable.tableheader;
const masonModalData = MasonListData.MasonListModal;

const statusOption = [
    { value: 'Y', text: 'Active' },
    { value: 'N', text: 'Not Active' }
]

const initialState = {
    id: '',
    name: '',
    address: '',
    contactNo: '',
    municipalRegNo: '',
    status: 'Y',
    photo: ''
};

const strData = ['name', 'address'];
const phoneData = ['contactNo'];
const numSlashData = ['municipalRegNo']

const strSchema = strData.map(rows => {
    return {
        [rows]: validateString,
    };
});

const phoneDataSchema = phoneData.map(row => {
    return {
        [row]: validateNullableNumber,
    };
});

const validSlashSchema = numSlashData.map(row => {
    return {
        [row]: validateNullableOfficialNumbers,
    };
});

const MasonSchema = Yup.object().shape(
    Object.assign(
        // ...numSchema,
        ...strSchema,
        ...phoneDataSchema,
        ...validSlashSchema
    )
);
const MasonListComponent = ({ adminData, success, postAdminDataByUrl, deleteAdminDataByUrl, putAdminDataByUrl, errors, loading }) => {
    const {
        isEditing,
        isDeleting,
        formState,
        openAddModal,
        openDeleteModal,
        handleAdd,
        handleEdit,
        handleDelete,
        setOpenAddModal,
        setOpenDeleteModal,
    } = useAdminSetup(initialState, success);
    return (
        <div>
            <Formik
                enableReinitialize
                initialValues={{ ...formState }}
                validationSchema={MasonSchema}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    const data = new FormData();
                    const selectedFile = values.masonPhoto;

                    if (selectedFile) {
                        selectedFile.forEach(masonPhoto => data.append('photoMason', masonPhoto));
                    }
                    data.append('name', values.name);
                    data.append('address', values.address);
                    data.append('contactNo', values.contactNo);
                    data.append('status', values.status);
                    data.append('municipalRegNo', values.municipalRegNo);
                    try {
                        if (isEditing) {
                            putAdminDataByUrl(api.masonList, formState.id, data);
                        } else if (isDeleting) {
                            deleteAdminDataByUrl(api.masonList, formState.id);
                            // setOpenDeleteModal(false);
                        } else {
                            postAdminDataByUrl(api.masonList, data);
                        }
                        actions.setSubmitting(false);
                    } catch (error) {
                        console.log('Error in Mason post', error);
                        showErrorToast('Something went wrong.');
                        actions.setSubmitting(false);
                    }
                }}
            >
                {({ handleSubmit, isSubmitting, setFieldValue, values, handleChange, errors: formErrors }) => (
                    <>
                        <Helmet>
                            <title>Add Masons List</title>
                        </Helmet>
                        <AdminModalContainer
                            key={'mason-edit'}
                            open={openAddModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={() => setOpenAddModal(false)}
                            title={isEditing ? masonModalData.editMasonList : masonModalData.addMasonList}
                            saveText={isEditing ? masonModalData.updateMasonList : masonModalData.saveMasonList}
                        >
                            <Form loading={isSubmitting || loading}>
                                <Grid>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsForm
                                                label={masonTableData.name}
                                                name="name"
                                                onChange={handleChange}
                                                error={formErrors.name}
                                                value={values.name}
                                                setFieldValue={setFieldValue}
                                            />
                                            {/* <EbpsNormalFormIm label={masonTableData.name} name="name" /> */}
                                        </Grid.Column>
                                        <Grid.Column>
                                            <EbpsForm
                                                label={masonTableData.address}
                                                name="address"
                                                onChange={handleChange}
                                                error={formErrors.address}
                                                value={values.address}
                                                setFieldValue={setFieldValue}
                                            />
                                            {/* <EbpsNormalFormIm label={masonTableData.address} name="address" /> */}
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsForm
                                                label={masonTableData.contact}
                                                name="contactNo"
                                                onChange={handleChange}
                                                error={formErrors.contactNo}
                                                value={values.contactNo}
                                                setFieldValue={setFieldValue}
                                            />
                                            {/* <EbpsNormalFormIm label={masonTableData.contact} name="contactNo" /> */}
                                        </Grid.Column>
                                        <Grid.Column>
                                            <SelectInput label={masonTableData.status} name="status" options={statusOption} />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns="2">
                                        <Grid.Column>
                                            <EbpsSlashForm
                                                label={masonTableData.regNo}
                                                name="municipalRegNo"
                                                setFieldValue={setFieldValue}
                                                error={formErrors.municipalRegNo}
                                                value={values.municipalRegNo}
                                            />
                                            {/* <EbpsForm
                                                label={masonTableData.regNo}
                                                name="municipalRegNo"
                                                onChange={handleChange}
                                                error={formErrors.municipalRegNo}
                                                value={values.municipalRegNo}
                                                setFieldValue={setFieldValue}
                                            /> */}
                                            {/* <EbpsNormalFormIm label={masonTableData.regNo} name="municipalRegNo" /> */}
                                        </Grid.Column>
                                        <Grid.Column>
                                            <label>Select a Photo</label>
                                            <FileInputWithMultiplePreview name="masonPhoto" fileCatId="masonPhoto" hasMultipleFiles={false} />
                                            {values.photo && <ImagePreview fileUrl={values.photo} />}
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Form>
                        </AdminModalContainer>
                        <AdminModalContainer
                            key={'mason-delete'}
                            open={openDeleteModal}
                            errors={errors}
                            isSubmitting={isSubmitting || loading}
                            handleSubmit={handleSubmit}
                            handleClose={() => setOpenDeleteModal(false)}
                            title={masonModalData.deleteMasonListModal.deleteMasonListModaltitle}
                            saveText={masonModalData.deleteMasonList}
                        >
                            <Form loading={isSubmitting || loading}>{masonModalData.deleteMasonListModal.deleteMasonListModalContent}</Form>
                        </AdminModalContainer>
                    </>
                )}
            </Formik>
            <Segment inverted className="tableSectionHeader">
                {MasonListData.MasonListTable.header}
                <Button
                    basic
                    style={{ right: 6, top: 8, position: 'absolute' }}
                    inverted
                    type="reset"
                    onClick={() => {
                        handleAdd();
                    }}
                >
                    <ButtonContent>
                        <Icon.Group>
                            <Icon name="list alternate outline" />
                            <Icon inverted corner name="add" />
                        </Icon.Group>{' '}
                        Add Mason
					</ButtonContent>
                </Button>
            </Segment>
            <Table striped celled compact="very" size="small">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>{masonTableData.name}</Table.HeaderCell>
                        <Table.HeaderCell>{masonTableData.address}</Table.HeaderCell>
                        <Table.HeaderCell>{masonTableData.contact}</Table.HeaderCell>
                        <Table.HeaderCell>{masonTableData.regNo}</Table.HeaderCell>
                        <Table.HeaderCell>{masonTableData.status}</Table.HeaderCell>
                        <Table.HeaderCell>{masonTableData.actions}</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {adminData && adminData.masonList && adminData.masonList.length > 0 ? (
                        adminData.masonList.map(row => (
                            <Table.Row key={row.id}>
                                <Table.Cell>{row.name}</Table.Cell>
                                <Table.Cell>{row.address}</Table.Cell>
                                <Table.Cell>{row.contactNo}</Table.Cell>
                                <Table.Cell>{row.municipalRegNo}</Table.Cell>
                                <Table.Cell>{getOptionText(row.status, statusOption)}</Table.Cell>
                                <Table.Cell>
                                    <Button
                                        inverted
                                        title="Edit"
                                        type="reset"
                                        color="green"
                                        onClick={() => {
                                            handleEdit({ ...row, formId: parseInt(row.formId) });
                                        }}
                                        icon="edit"
                                        size="tiny"
                                        style={{ marginBottom: '2px' }}
                                    ></Button>
                                    <Button
                                        inverted
                                        title="Delete"
                                        type="reset"
                                        color="red"
                                        icon="delete"
                                        onClick={() => {
                                            handleDelete(row);
                                        }}
                                        size="tiny"
                                    ></Button>
                                </Table.Cell>
                            </Table.Row>
                        ))
                    ) : (
                            <Table.Row>
                                <Table.Cell colSpan="6">
                                    <Segment textAlign="center">No Data Found.</Segment>
                                </Table.Cell>
                            </Table.Row>
                        )}
                </Table.Body>
            </Table>
        </div>
    );
};

export const MasonList = parentProps => (
    <AdminContainer
        api={[{ api: api.masonList, objName: 'masonList' }]}
        updateApi={[{ api: api.masonList, objName: 'masonList' }]}
        render={props => <MasonListComponent {...props} parentProps={parentProps} />}
    />
);
