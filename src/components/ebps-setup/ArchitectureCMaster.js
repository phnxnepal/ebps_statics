import React from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { archiCMasterData, archiBMasterData } from '../../utils/data/ebps-setup/archiBMasterData';
import { Table, Segment, Form, Grid, Message, Icon } from 'semantic-ui-react';
import { useSetup } from '../../hooks/useAdminSetup';
import { Formik } from 'formik';
import { showErrorToast } from '../../utils/functionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import { EbpsNormalFormIm } from '../shared/EbpsForm';
import { EditButton, DeleteButton } from './forms/Buttons';
import Helmet from 'react-helmet';

const initialState = {
	id: '',
	buildingElements: '',
	groupId: '',
	groupName: '',
	unit: '',
};

const archiCtabledata = archiCMasterData.archiCtable.archiCtableHeadings;
const archiCModaldata = archiCMasterData.archiCmodal;

export const ArchitectureCMasterComponent = ({
	adminData,
	success,
	putAdminDataByUrl,
	postAdminDataByUrl,
	deleteAdminDataByUrl,
	errors,
	loading,
}) => {
	const { isEditing, isDeleting, formState, openAddModal, openDeleteModal, handleEdit, handleDelete, handleClose } = useSetup(
		initialState,
		success
	);

	return (
		<div className="setup-main">
			<Helmet title="Architecture Design Class C Master" />
			<Segment inverted className="tableSectionHeader">
				{archiCMasterData.archiCtable.archiCHead}
			</Segment>
			<Table striped celled compact="very" size="small">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{archiCtabledata.id}</Table.HeaderCell>
						<Table.HeaderCell>{archiCtabledata.buildingElements}</Table.HeaderCell>
						<Table.HeaderCell>{archiCtabledata.groupId}</Table.HeaderCell>
						<Table.HeaderCell>{archiCtabledata.groupName}</Table.HeaderCell>
						<Table.HeaderCell>{archiCtabledata.unit}</Table.HeaderCell>
						<Table.HeaderCell>{archiCtabledata.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{adminData && adminData.archiCMaster && adminData.archiCMaster.length > 0 ? (
						adminData.archiCMaster.map((row) => (
							<Table.Row key={row.id}>
								<Table.Cell>{row.id}</Table.Cell>
								<Table.Cell>{row.buildingElements}</Table.Cell>
								<Table.Cell>{row.groupId}</Table.Cell>
								<Table.Cell>{row.groupName}</Table.Cell>
								<Table.Cell>{row.unit}</Table.Cell>
								<Table.Cell>
									<EditButton handleEdit={handleEdit} row={row} />
									<DeleteButton handleDelete={handleDelete} row={row} />
								</Table.Cell>
							</Table.Row>
						))
					) : (
						<Table.Row>
							<Table.Cell colSpan="6">
								<Segment textAlign="center">No Data Found.</Segment>
							</Table.Cell>
						</Table.Row>
					)}
				</Table.Body>
			</Table>

			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						if (isEditing) {
							putAdminDataByUrl(api.archiCMaster, formState.groupId, values);
						} else if (isDeleting) {
							deleteAdminDataByUrl(api.archiCMaster, formState.id);
							// setOpenDeleteModal(false);
						} else {
							postAdminDataByUrl(api.archiCMaster, values);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in Archi C Master post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting }) => (
					<>
						<AdminModalContainer
							key={'archi-c-master-edit'}
							open={openAddModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={handleClose}
							title={isEditing ? archiCModaldata.archiCEdit : archiCModaldata.archiCAdd}
							saveText={isEditing ? archiCModaldata.archiCUpdate : archiCModaldata.archiCSave}
						>
							<Form loading={isSubmitting || loading}>
								<Message info>
									<Message.Header>
										<Icon name="help circle" />
										{archiBMasterData.note}
									</Message.Header>
								</Message>
								<Grid>
									{isEditing && (
										<Grid.Row>
											<Grid.Column>Id: {formState.id}</Grid.Column>
										</Grid.Row>
									)}
									<Grid.Row columns="2">
										<Grid.Column>
											<EbpsNormalFormIm label={archiCtabledata.buildingElements} name="buildingElements" />
										</Grid.Column>
										<Grid.Column>
											<EbpsNormalFormIm label={archiCtabledata.groupId} name="groupId" />
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											<EbpsNormalFormIm label={archiCtabledata.groupName} name="groupName" />
										</Grid.Column>
										<Grid.Column>
											<EbpsNormalFormIm label={archiCtabledata.unit} name="unit" />
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							key={'archi-c-master-delete'}
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={handleClose}
							title={archiCModaldata.archiCdeleteModal.archiCDeletetitle}
							saveText={archiCModaldata.archiCDelete}
						>
							<Form loading={isSubmitting || loading}>{archiCModaldata.archiCdeleteModal.archiCDeletecontent}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
		</div>
	);
};

export const ArchitectureCMaster = (parentProps) => (
	<AdminContainer
		api={[{ api: api.archiCMaster, objName: 'archiCMaster' }]}
		updateApi={[{ api: api.archiCMaster, objName: 'archiCMaster' }]}
		render={(props) => <ArchitectureCMasterComponent {...props} parentProps={parentProps} />}
	/>
);
