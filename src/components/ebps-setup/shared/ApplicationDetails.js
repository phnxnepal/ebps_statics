import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Header, List, Grid } from 'semantic-ui-react';
import { applicationTable } from '../../../utils/data/genericSetupData';
import { translateEngToNepWithZero } from '../../../utils/langUtils';
import { getOptionText } from '../../../utils/dataUtils';
import { constructionTypeSelectOptions } from '../../../utils/optionUtils';

const applicationData = applicationTable;

function ApplicationDetails({ appInfo }) {
	return (
		<Segment>
			<Header>{applicationData.applicationInfo}</Header>
			<List className="app-info">
				<Grid>
					<Grid.Row columns="3">
						<Grid.Column>
							<List.Item>
								<List.Header>{appInfo.applicantNo}</List.Header>
								<List.Description>{applicationData.applicationId}</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{appInfo.applicantName}</List.Header>
								<List.Description>{applicationData.applicantName}</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{appInfo.nibedakName}</List.Header>
								<List.Description>{applicationData.nibedakName}</List.Description>
							</List.Item>
						</Grid.Column>
					</Grid.Row>
					<Grid.Row columns="3">
						<Grid.Column>
							<List.Item>
								<List.Header>{appInfo.applicantMobileNo}</List.Header>
								<List.Description>{applicationData.applicantMobileNo}</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{translateEngToNepWithZero(appInfo.applicantDate)}</List.Header>
								<List.Description>{applicationData.applicantDate}</List.Description>
							</List.Item>
						</Grid.Column>
						<Grid.Column>
							<List.Item>
								<List.Header>{getOptionText(appInfo.constructionType, constructionTypeSelectOptions)}</List.Header>
								<List.Description>{applicationData.constructionType}</List.Description>
							</List.Item>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</List>
		</Segment>
	);
}

ApplicationDetails.propTypes = {
	appInfo: PropTypes.object.isRequired,
};

export default ApplicationDetails;
