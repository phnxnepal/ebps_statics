import React from 'react';
import { useTable, useFilters, usePagination } from 'react-table';
import { Table, Menu, Icon } from 'semantic-ui-react';

export const SetupTable = ({ columns, data }) => {

	const defaultColumn = React.useMemo(
		() => ({
			// Let's set up our default Filter UI
			Filter: <></>,
			// disableFilters: true,
		}),
		[]
	);

	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		page,
		prepareRow,
		canPreviousPage,
		canNextPage,
		pageOptions,
		// pageCount,
		gotoPage,
		nextPage,
		previousPage,
		setPageSize,
		state: { pageIndex, pageSize },
	} = useTable(
		{
			columns,
			data,
			defaultColumn,
			initialState: { pageIndex: 0 },
		},
		useFilters,
		usePagination
	);

	return (
		<div>
			<Table striped celled compact="very" size="small" {...getTableProps()}>
				<Table.Header>
					{headerGroups.map((headerGroup) => (
						<Table.Row {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column) => (
								<Table.HeaderCell {...column.getHeaderProps()}>
									{column.render('Header')}
									<div>{column.canFilter ? column.render('Filter') : null}</div>
								</Table.HeaderCell>
							))}
						</Table.Row>
					))}
				</Table.Header>
				<Table.Body {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row);
						return (
							<Table.Row {...row.getRowProps()}>
								{row.cells.map((cell) => {
									return <Table.Cell {...cell.getCellProps()}>{cell.render('Cell')}</Table.Cell>;
								})}
							</Table.Row>
						);
					})}
				</Table.Body>
				<Table.Footer>
					<Table.Row>
						<Table.HeaderCell colSpan="5">
							<Menu floated="right" pagination>
								<Menu.Item className="pagination-select">
									<select
										className="ui selection dropdown"
										value={pageSize}
										onChange={(e) => {
											setPageSize(Number(e.target.value));
										}}
									>
										{[5, 10, 20, 30, 40, 50].map((pageSize) => (
											<option key={pageSize} value={pageSize}>
												Show {pageSize}
											</option>
										))}
									</select>
								</Menu.Item>
								<Menu.Item as="a" onClick={() => previousPage()} disabled={!canPreviousPage} icon>
									<Icon name="chevron left" />
								</Menu.Item>
								{pageOptions.map((pageOption) => (
									<Menu.Item
										key={pageOption}
										onClick={() => {
											gotoPage(pageOption);
										}}
										active={pageIndex === pageOption}
										as="a"
									>
										{pageOption + 1}
									</Menu.Item>
								))}
								<Menu.Item as="a" onClick={() => nextPage()} disabled={!canNextPage} icon>
									<Icon name="chevron right" />
								</Menu.Item>
							</Menu>
						</Table.HeaderCell>
					</Table.Row>
				</Table.Footer>
			</Table>
		</div>
	);
};
