import React, { Component } from 'react'
import { buildclass } from '../../utils/data/ebps-setup/userformdata';
import { Form, Segment, Grid, Button, Table, Menu, Icon } from 'semantic-ui-react';
import { Formik, Field } from 'formik';

const buildClass = buildclass.builded;
const colors = ['red']

class BuildingClass extends Component {
    state = {}
    render() {
        return (<>
            <Formik
                onSubmit={(values, { setSubmitting }) => {
                    console.log(values);
                    setSubmitting(false);
                }}
                render={({ handleChange, values, handleSubmit }) => (
                    <Form onSubmit={handleSubmit} className="NJ-right">
                        <Segment><Grid padded>
                            {colors.map((color) => (
                                <Grid.Row color={color} key={color}>
                                    <Grid.Column><label style={{ color: 'White', fontSize: '20px' }}><center>{buildClass.header.head1}</center></label></Grid.Column>
                                </Grid.Row>
                            ))}
                        </Grid>
                            <Segment>
                                <Grid >
                                    <Grid.Row>
                                        <Grid.Column width="5">
                                            {buildClass.content.content1}
                                            <Field type="text" name="classkonaam" />
                                        </Grid.Column>
                                        <Grid.Column width="5">
                                            {buildClass.content.content2}
                                            <Field type="text" name="classkobibraneng" />
                                        </Grid.Column>
                                        <Grid.Column width="5">
                                            {buildClass.content.content3}
                                            <Field type="text" name="classkobibarannep" />
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid><br />
                                <Button type="submit" color="red">Save</Button>
                            </Segment>
                            <Grid padded >
                                {colors.map((color) => (
                                    <Grid.Row color={color} key={color}>
                                        <Grid.Column><label style={{ color: 'White', fontSize: '20px' }}><center>{buildClass.header.head2}</center></label></Grid.Column>
                                    </Grid.Row>
                                ))}
                            </Grid>
                            <Table striped selectable padded size="small">
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell>{buildClass.content.content1}</Table.HeaderCell>
                                        <Table.HeaderCell>{buildClass.content.content2}</Table.HeaderCell>
                                        <Table.HeaderCell>{buildClass.content.content3}</Table.HeaderCell>
                                        <Table.HeaderCell>{buildClass.content.content4}</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell><a > <Icon disabled name='edit' /></a> | <a ><Icon disabled name='remove circle' /></a></Table.Cell>

                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell><a > <Icon disabled name='edit' /></a> | <a ><Icon disabled name='remove circle' /></a></Table.Cell>

                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell>Cell</Table.Cell>
                                        <Table.Cell><a ><Icon disabled name='edit' /></a> | <a ><Icon disabled name='remove circle' /></a></Table.Cell>
                                    </Table.Row>
                                </Table.Body>
                            </Table>
                        </Segment>
                    </Form>
                )}
            />
        </>);
    }
}

export default BuildingClass;
