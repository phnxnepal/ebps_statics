import React from 'react';
import AdminContainer from '../../containers/base/AdminContainer';
import api from '../../utils/api';
import { archiBMasterData } from '../../utils/data/ebps-setup/archiBMasterData';
import { Table, Segment, Form, Grid, Message, Icon } from 'semantic-ui-react';
import { useSetupV2 } from '../../hooks/useAdminSetup';
import { Formik } from 'formik';
import { showErrorToast } from '../../utils/functionUtils';
import { AdminModalContainer } from './AdminModalContainer';
import { EbpsNormalFormIm } from '../shared/EbpsForm';
import { EditButton, DeleteButton } from './forms/Buttons';
import Helmet from 'react-helmet';

const initialState = {
	id: '',
	buildingElements: '',
	groupId: '',
	groupName: '',
	unit: '',
};

const tableHeadings = archiBMasterData.table.tableHeadings;
const modalData = archiBMasterData.modal;

export const ArchitectureBMasterComponent = ({
	adminData,
	success,
	putAdminDataByUrl,
	postAdminDataByUrl,
	deleteAdminDataByUrl,
	errors,
	loading,
}) => {
	const { isEditing, isDeleting, formState, openAddModal, openDeleteModal, handleEdit, handleDelete, handleClose } = useSetupV2(
		initialState,
		success
	);

	return (
		<div className="setup-main">
			<Helmet title="Architecture Design Class B Master" />
			<Segment inverted className="tableSectionHeader">
				{archiBMasterData.table.tableHeader}
			</Segment>
			<Table striped celled compact="very" size="small">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{tableHeadings.id}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.buildingElements}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.groupId}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.groupName}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.unit}</Table.HeaderCell>
						<Table.HeaderCell>{tableHeadings.actions}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{adminData && adminData.archiBMaster && adminData.archiBMaster.length > 0 ? (
						adminData.archiBMaster.map((row) => (
							<Table.Row key={row.id}>
								<Table.Cell>{row.id}</Table.Cell>
								<Table.Cell>{row.buildingElements}</Table.Cell>
								<Table.Cell>{row.groupId}</Table.Cell>
								<Table.Cell>{row.groupName}</Table.Cell>
								<Table.Cell>{row.unit}</Table.Cell>
								<Table.Cell>
									<EditButton handleEdit={handleEdit} row={row} />
									<DeleteButton handleDelete={handleDelete} row={row} />
								</Table.Cell>
							</Table.Row>
						))
					) : (
						<Table.Row>
							<Table.Cell colSpan="6">
								<Segment textAlign="center">No Data Found.</Segment>
							</Table.Cell>
						</Table.Row>
					)}
				</Table.Body>
			</Table>

			<Formik
				enableReinitialize
				initialValues={{ ...formState }}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						if (isEditing) {
							putAdminDataByUrl(api.archiBMaster, formState.groupId, values);
						} else if (isDeleting) {
							deleteAdminDataByUrl(api.archiBMaster, formState.id);
							// setOpenDeleteModal(false);
						} else {
							postAdminDataByUrl(api.archiBMaster, values);
						}
						actions.setSubmitting(false);
					} catch (error) {
						console.log('Error in Archi B Master post', error);
						showErrorToast('Something went wrong.');
						actions.setSubmitting(false);
					}
				}}
			>
				{({ handleSubmit, isSubmitting }) => (
					<>
						<AdminModalContainer
							key={'archi-b-master-edit'}
							open={openAddModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={handleClose}
							title={isEditing ? modalData.titleEdit : modalData.titleAdd}
							saveText={isEditing ? modalData.editText : modalData.saveText}
						>
							<Form loading={isSubmitting || loading}>
								<Message info>
									<Message.Header>
										<Icon name="help circle" />
										{archiBMasterData.note}
									</Message.Header>
								</Message>
								<Grid>
									{isEditing && (
										<Grid.Row columns="2">
											<Grid.Column>Id: {formState.id}</Grid.Column>
										</Grid.Row>
									)}
									<Grid.Row columns="2">
										<Grid.Column>
											<EbpsNormalFormIm label={tableHeadings.buildingElements} name="buildingElements" />
										</Grid.Column>
										<Grid.Column>
											<EbpsNormalFormIm label={tableHeadings.groupId} name="groupId" />
										</Grid.Column>
									</Grid.Row>
									<Grid.Row columns="2">
										<Grid.Column>
											<EbpsNormalFormIm label={tableHeadings.groupName} name="groupName" />
										</Grid.Column>
										<Grid.Column>
											<EbpsNormalFormIm label={tableHeadings.unit} name="unit" />
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</Form>
						</AdminModalContainer>
						<AdminModalContainer
							key={'archi-b-master-delete'}
							open={openDeleteModal}
							errors={errors}
							isSubmitting={isSubmitting || loading}
							handleSubmit={handleSubmit}
							handleClose={handleClose}
							title={modalData.delete.title}
							saveText={modalData.deleteText}
						>
							<Form loading={isSubmitting || loading}>{modalData.delete.content}</Form>
						</AdminModalContainer>
					</>
				)}
			</Formik>
		</div>
	);
};

export const ArchitectureBMaster = (parentProps) => (
	<AdminContainer
		api={[{ api: api.archiBMaster, objName: 'archiBMaster' }]}
		updateApi={[{ api: api.archiBMaster, objName: 'archiBMaster' }]}
		render={(props) => <ArchitectureBMasterComponent {...props} parentProps={parentProps} />}
	/>
);
