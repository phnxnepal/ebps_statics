import React, { Component } from 'react';
import { Formik } from 'formik';
import { Button, Form, Table, Segment, Modal, ButtonContent, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { postOtherSetBack, getOtherSetBack, deleteOtherSetBack, putOtherSetBack } from '../../store/actions/AdminAction';
import FallbackComponent from '../shared/FallbackComponent';
import { showToast } from '../../utils/functionUtils';
import ErrorDisplay from '../shared/ErrorDisplay';
import { EbpsFormIm } from '../shared/EbpsForm';

import * as Yup from 'yup';
import { validateString, validateNumber } from '../../utils/validationUtils';
import { lengthconvertor, getFloatInput } from '../../utils/mathUtils';
import { statusOption } from '../../utils/optionUtils';
import { SelectInput } from '../shared/Select';
import { NumberInput } from '../shared/fields/NumberInput';
import { CustomFormik } from '../shared/formComponents/CustomFormik';

const initialValues = {
	id: '',
	placeName: '',
	setBackFoot: 0,
	setBackMeter: 0,
	status: 'N',
};

const strData = ['placeName', 'status'];

const numData = ['setBackMeter', 'setBackFoot'];

const formDataSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});

const numDataSchema = numData.map((row) => {
	return {
		[row]: validateNumber,
	};
});
const otherSetBackSchema = Yup.object().shape(Object.assign({}, ...formDataSchema, ...numDataSchema));

class OtherSetBack extends Component {
	constructor(props) {
		super(props);
		this.state = {
			formData: initialValues,
			isEditing: false,
			isModalOpen: false,
			idToDelete: '',
		};
	}
	modalOpen = (id) => {
		this.setState({
			isModalOpen: true,
			idToDelete: id,
		});
	};
	modalClose = () => {
		this.setState({
			isModalOpen: false,
		});
	};
	handleDelete = async (id) => {
		try {
			await this.props.deleteOtherSetBack(id);
			if (this.props.success && this.props.success.success) {
				showToast('Your data has been successfully deleted.');
				this.props.getOtherSetBack();
			}
			this.modalClose();
		} catch (err) {
			console.log('Error', err);
			this.modalClose();
		}
	};

	handleAdd = () => {
		window.scrollTo(0, 0);
		this.setState({
			formData: initialValues,
			isEditing: false,
		});
	};

	handleEdit = (User) => {
		// console.log('User:', User);
		window.scrollTo(0, 0);
		this.setState({
			formData: {
				placeName: User.placeName,
				setBackMeter: User.setBackMeter,
				status: User.status,
				id: User.id,
				setBackFoot: User.setBackFoot,
			},
			isEditing: true,
		});
	};
	componentDidMount() {
		this.props.getOtherSetBack();
	}
	// componentDidUpdate(prevProps) {
	//     if (prevProps.success !== this.props.success) {
	//         setTimeout(() => {
	//             this.props.getOtherSetBack()
	//         },
	//             0
	//         )

	//     }

	// }

	render() {
		const { userData, postOtherSetBack } = this.props;
		if (userData) {
			return (
				<CustomFormik
					enableReinitialize
					initialValues={this.state.formData}
					validationSchema={otherSetBackSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.error && delete values.error;
						try {
							if (this.state.isEditing) {
								await this.props.putOtherSetBack(this.state.formData.id, values);
							} else {
								await postOtherSetBack(values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully added.');
								this.props.getOtherSetBack();
								this.handleAdd();
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, setFieldValue, errors, getSetFieldProps, getProps }) => {
						return (
							<Form>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div>
									<Segment raised>
										<Segment inverted className="tableSectionHeader">
											Other SetBack
											<Button
												basic
												style={{ right: 4, top: 5, position: 'absolute' }}
												inverted
												type="reset"
												onClick={() => {
													this.handleAdd();
												}}
											>
												<ButtonContent>
													<Icon name="add" />
													Add Road Set Back
												</ButtonContent>
											</Button>
										</Segment>
										{/* <Grid> */}
										<Form.Group widths="equal">
											<Form.Field>
												{'Place Name'}
												<EbpsFormIm name="placeName" {...getSetFieldProps('placeName')} />
											</Form.Field>
											<NumberInput
												label={'SetBack (Feet)'}
												name="setBackFoot"
												onChange={(e) => {
													if (e.target.value !== undefined) {
														setFieldValue('setBackFoot', getFloatInput(e.target.value));
														setFieldValue('setBackMeter', lengthconvertor(e.target.value, 'METRE', 'FEET'));
													}
												}}
												{...getProps('setBackFoot')}
											/>
											<NumberInput
												label={'SetBack (Meter)'}
												name="setBackMeter"
												onChange={(e) => {
													if (e.target.value !== undefined) {
														setFieldValue('setBackMeter', getFloatInput(e.target.value));
														setFieldValue('setBackFoot', lengthconvertor(e.target.value, 'FEET', 'METRE'));
													}
												}}
												{...getProps('setBackMeter')}
											/>

											<Form.Field error={!!errors.status}>
												{'Status'}
												<br />
												<SelectInput name="status" options={statusOption} />
											</Form.Field>
										</Form.Group>
										{/* </Grid.Column> */}
										{/* </Grid.Row> */}
										{/* </Grid> */}
										<br />
										<Button type="submit" onClick={handleSubmit} className="fileStorageSegment">
											{this.state.isEditing ? 'Update' : 'Save'}
										</Button>
									</Segment>
									<br />
									<Segment raised>
										<Segment inverted className="tableSectionHeader">
											Other Setback Details
										</Segment>
										<Table striped celled selectable style={{ textAlign: 'center' }}>
											<Table.Header>
												<Table.Row>
													{/* <Table.HeaderCell>{"Id"}</Table.HeaderCell> */}
													<Table.HeaderCell>{'Place Name'}</Table.HeaderCell>
													<Table.HeaderCell>{'SetBack Foot'}</Table.HeaderCell>
													<Table.HeaderCell>{'SetBack Metre'}</Table.HeaderCell>
													<Table.HeaderCell>{'Status'}</Table.HeaderCell>
													<Table.HeaderCell>{'Action'}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>

											<Table.Body>
												{this.props.userData.map((other, index) => {
													return (
														<Table.Row key={index}>
															{/* <Table.Cell >{other.id}</Table.Cell> */}
															<Table.Cell>{other.placeName}</Table.Cell>
															<Table.Cell>{other.setBackFoot}</Table.Cell>
															<Table.Cell>{other.setBackMeter}</Table.Cell>
															<Table.Cell>{other.status}</Table.Cell>
															<Table.Cell>
																<Button
																	// circular
																	inverted
																	title="Edit"
																	color="green"
																	icon="edit"
																	size="tiny"
																	onClick={() => this.handleEdit(other)}
																></Button>
																<Button
																	// circular
																	inverted
																	color="red"
																	title="Delete"
																	icon="remove circle"
																	size="tiny"
																	onClick={() => this.modalOpen(other.id)}
																></Button>
																<Modal onClose={this.modalClose} open={this.state.isModalOpen}>
																	<Modal.Content>Are you sure you want to delete this section?</Modal.Content>
																	<Modal.Actions>
																		<Button
																			color="green"
																			onClick={() => this.handleDelete(this.state.idToDelete)}
																			title="Confirm Delete!"
																			positive
																			labelPosition="left"
																			icon="checkmark"
																			content="Yes"
																		></Button>
																		<Button
																			color="red"
																			onClick={this.modalClose}
																			title="Decline Delete!"
																			negative
																			labelPosition="left"
																			icon="remove"
																			content="No"
																		></Button>
																	</Modal.Actions>
																</Modal>
															</Table.Cell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</Segment>
								</div>
							</Form>
						);
					}}
				</CustomFormik>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}
const mapDispatchToProps = { postOtherSetBack, getOtherSetBack, deleteOtherSetBack, putOtherSetBack };
const mapStateToProps = (state) => ({
	userData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.admin.success,
});
export default connect(mapStateToProps, mapDispatchToProps)(OtherSetBack);
