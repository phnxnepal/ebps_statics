import * as React from 'react';
import { Header, Icon } from 'semantic-ui-react';
import JqxDataTable, { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
// Redux
import { connect } from 'react-redux';
import { getCompletedList, setBuildPermit, getBuildingClass } from '../store/actions/dashboardActions';
import FallbackComponent from './shared/FallbackComponent';
import { Loader, Dimmer } from 'semantic-ui-react';
import { isEmpty, getUserTypeValueNepali } from '../utils/functionUtils';
import ErrorDisplay from './shared/ErrorDisplay';
import { dashboardLang } from '../utils/data/dashboardLangFile';
import { getMessage } from './shared/getMessage';
// import { getOptionText } from '../utils/dataUtils';
import { KEYBOARD } from '../utils/constants';
import { getLocalStorage } from '../utils/secureLS';
import RootGrid from './loggedInComponents/Grid/RootGrid';
import { KeyboardSelectionModal } from './shared/modals/KeyboardSelectionModal';
import { TableWidths } from '../utils/enums/ui';
// import { constructionTypeSelectOptions } from '../utils/data/genericData';
import { getConstructionTypeText } from '../utils/enums/constructionType';

const dash_lang = dashboardLang.dashboard_data;
const messageId = 'dashboardLang.dashboard_data';

class CompletedList extends React.PureComponent<{}, IDataTableProps> {
	private tableRef = React.createRef<JqxDataTable>();

	constructor(props: {}) {
		super(props);

		this.state = {
			//@ts-ignore
			langModalOpen: false,
		};

		this.onSelectionInfo = this.onSelectionInfo.bind(this);
		this.setLangModalOpen = this.setLangModalOpen.bind(this);
	}

	componentDidMount() {
		document.title = `${document.title} - Completed List`;

		// console.log(this.state, getLocalStorage(KEYBOARD));
		if (isEmpty(getLocalStorage(KEYBOARD))) {
			//@ts-ignore
			this.setState({ langModalOpen: true });
		}
		//@ts-ignore
		this.props.getCompletedList();
	}

	componentWillUnmount() {
		document.title = 'EBPS';
	}

	setLangModalOpen(value: boolean) {
		// console.log("values -- ", value);

		//@ts-ignore
		this.setState({ langModalOpen: value });
	}

	// cellClassTaha = (row: any, dataField: any, cellText: any, rowData: any) => {
	//   const cellValue = rowData[dataField];
	//   if (cellValue === 'डिजाइनर') {
	//     return 'designer';
	//   } else if (cellValue === 'सब-ईन्जिनियर') {
	//     return 'subEngineer';
	//   } else if (cellValue === 'ईन्जिनियर') {
	//     return 'engineer';
	//   } else if (cellValue === 'अधिकृत') {
	//     return 'admin';
	//   } else {
	//     return 'others';
	//   }
	// };

	cellClass = (row: any, dataField: any, cellText: any, rowData: any) => {
		const cellValue = rowData[dataField];
		if (cellValue === 'अस्वीकृत गरिएको') {
			return 'disapproved';
		} else if (cellValue === 'स्वीकृत भएको') {
			// return 'approved';
			const columnValue = rowData['applicationActionBy'];
			if (columnValue === 'डिजाइनर') {
				return 'designer';
			} else if (columnValue === 'सब-ईन्जिनियर') {
				return 'subEngineer';
			} else if (columnValue === 'ईन्जिनियर') {
				return 'engineer';
			} else if (columnValue === 'अधिकृत') {
				return 'admin';
			} else if (columnValue === 'राजस्व') {
				return 'rajaswo';
			} else {
				return 'amin';
			}
		} else {
			return 'average';
		}
	};

	datafield = [
		{ name: 'applicantNo', type: 'string' },
		{ name: 'talathapApplicationNo', type: 'string' },
		{ name: 'applicantName', type: 'string' },
		{ name: 'applicantAddress', type: 'string' },
		{ name: 'nibedakName', type: 'string' },
		{ name: 'applicantMobileNo', type: 'string' },
		{ name: 'applicationStatus', type: 'string' },
		{ name: 'applicationActionBy', type: 'string' },
		{ name: 'yourStatus', type: 'string' },
		{ name: 'applicationAction', type: 'string' },
		{ name: 'applicantDate', type: 'string' },
		{ name: 'constructionType', type: 'string' },
		{ name: 'forwardTo', type: 'string' },
	];

	columns = [
		{
			text: 'शाखा दर्ता नं.',
			dataField: 'applicantNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_NO,
			pinned: 'true',
		},
		{
			text: 'तला थप दर्ता नं.',
			dataField: 'talathapApplicationNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_NO,
			pinned: 'true',
		},
		{
			text: 'जग्गाधनीको नाम',
			dataField: 'applicantName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			pinned: 'true',
		},

		{
			text: 'जग्गाधनीको ठेगाना',
			dataField: 'applicantAddress',
			cellsalign: 'center',
			align: 'center',
			width: '150',
		},
		{
			text: 'निवेदकको नाम',
			dataField: 'nibedakName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
		},
		{
			text: 'सम्पर्क फोन.',
			dataField: 'applicantMobileNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.MOBILE_NO,
		},
		{
			text: 'तह',
			dataField: 'applicationActionBy',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_ACTION_BY,
			filtertype: 'list',
			// cellclassname: this.cellClassTaha
		},
		{
			text: 'तह स्थिति',
			dataField: 'forwardTo',
			cellsalign: 'center',
			align: 'center',
			width: '250',
			filtertype: 'list',
		},
		{
			text: 'स्थिति फारम',
			dataField: 'applicationAction',
			cellsalign: 'center',
			align: 'center',
			// width: '350',
			filtertype: 'list',
			// cellclassname: 'alignment',
			// cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
		},
		{
			text: 'दर्ता मिती.',
			dataField: 'applicantDate',
			cellsalign: 'center',
			align: 'center',
			width: '100',
		},
		{
			text: 'भवनको किसिम',
			dataField: 'constructionType',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.CONSTRUCTION_TYPE,
			pinned: 'true',
			filtertype: 'list',
		},

		// {
		// 	text: 'पूर्ण अवस्था',
		// 	dataField: 'applicationStatus',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: TableWidths.APPLICATION_STATUS,
		// 	filtertype: 'list',
		// },
	];

	render() {
		//@ts-ignore
		const { buildPermits } = this.props;

		let formattedData = [];
		//@ts-ignore
		// const userType = getUserInfoObj.userType;
		// const actionObject = getActionObject(userType)
		// console.log("render props", this.props, buildPermits);

		if (!isEmpty(buildPermits) || Array.isArray(buildPermits)) {
			formattedData = JSON.parse(JSON.stringify(buildPermits));
			formattedData.forEach((permit: {}) => {
				//@ts-ignore
				permit.applicationActionBy = getUserTypeValueNepali(
					// @ts-ignore
					permit.applicationActionBy
				);
				//@ts-ignore
				permit.constructionType = getConstructionTypeText(permit.constructionType);
				//@ts-ignore
				permit.applicationAction = permit.applicationAction ? permit.applicationAction.formName : '';
			});
			return (
				<React.Fragment>
					<KeyboardSelectionModal
						langModalOpen={
							// @ts-ignore
							this.state.langModalOpen
						}
						setLangModalOpen={this.setLangModalOpen}
					/>
					{
						// for props.errors
						// @ts-ignore
						this.props.errors && (
							//@ts-ignore
							<ErrorDisplay message={this.props.errors.message} />
						)
					}
					<Dimmer
						active={
							//@ts-ignore
							this.props.loadingnestedNumbers
						}
						inverted
						page
					>
						<Loader
							active={
								//@ts-ignore
								this.props.loading
							}
							inline="centered"
						>
							{getMessage(`${messageId}.datafetch`, dash_lang.datafetch)}
						</Loader>
					</Dimmer>
					<div className="mapRegistration-dataTableList">
						<Header>
							<Icon name="list alternate" />
							<Header.Content>Add Storey List</Header.Content>
						</Header>
						<div className="pointer">
							{/* 
              // @ts-ignore */}
							<RootGrid
								//@ts-ignore
								localdata={formattedData}
								datafield={this.datafield}
								columns={this.columns}
								groupable={false}
								rowsheight={60}
								showfilterrow={true}
								// autorowheight={true}
								// @ts-ignore
								onRowClick={
									// e => {
									// console.log(e);
									this.onSelectionInfo
									// (e);
									// console.log(e);
									// }
								}
							/>
						</div>
					</div>
				</React.Fragment>
			);
		} else {
			return (
				<FallbackComponent
					//@ts-ignore
					errors={this.props.errors}
					//@ts-ignore
					loading={this.props.loading}
				/>
			);
		}
	}

	//@ts-ignore
	private onSelectionInfo(e) {
		// gets selected row indexes. The method returns an Array of indexes.
		const index = e.args.row.boundindex;

		window.scroll(0, 0);
		//@ts-ignore
		const currentPermit = this.props.buildPermits[index];
		setTimeout(() => {
			//@ts-ignore
			this.props.history.push(`/user/building-permit-tallathap/${currentPermit.id}`);
		}, 1000);
	}
}

const mapDispatchToProps = { getCompletedList, getBuildingClass, setBuildPermit };
//@ts-ignore
const mapStateToProps = (state) => ({
	buildPermits: state.root.dashboard.buildPermits,
	currentPermit: state.root.dashboard.currentPermit,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});
export default connect(mapStateToProps, mapDispatchToProps)(CompletedList);
