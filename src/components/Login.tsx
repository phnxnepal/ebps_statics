import React, { useState } from 'react';
// import { withRouter } from 'react-router';

// Formik
import { FormikValues } from 'formik';
import { withFormik, FormikProps } from 'formik';
import * as Yup from 'yup';
import EBPS from '../assets/images/EBPS.jpg';
import NepalSarkarLogo from '../assets/images/Nepal_sarkar_logo.webp';
// Semantic UI
import { Grid, Segment, Header, Form, Button, Loader, Item } from 'semantic-ui-react';

// Redux s
import { connect } from 'react-redux';
import { loginUser } from '../store/actions/loginForm';

import { store } from '..';
import ErrorDisplay from './shared/ErrorDisplay';
import { Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { useCleanup } from '../hooks/useCleanup';
import Helmet from 'react-helmet';
import { selectNeedsPublicSignup } from '../store/selectors/organizationSelectors';

interface LoginFormValues {
	email: string;
	password: string;
}

interface OtherProps {
	title?: string;
	needsPublicSignup: boolean;
}

interface LoginFormProps {
	initialEmail?: string;
	initialPassword?: string;
	ui: {};
}

const LoginForm = (props: OtherProps & FormikProps<FormikValues>) => {
	useCleanup();

	const [hidden, setHidden] = useState(true);

	const {
		values,
		errors,
		touched,
		needsPublicSignup,
		handleChange,
		handleBlur,
		handleSubmit,
		isSubmitting,
		//@ts-ignore
		ui,
	} = props;

	return (
		// <div className='background_image'>
		<>
			<Helmet title="EBPS - Login" />
			<div
				style={{
					backgroundImage: `url(${EBPS})`,
					backgroundSize: 'cover',
					transform: 'translateY(14px)',
				}}
			>
				<Link to="/" className="login_home">
					<Icon name="home" />
				</Link>
				<Grid textAlign="center" style={{ height: '100vh' }} verticalAlign="middle">
					<Grid.Column style={{ maxWidth: 450 }} className="login-background">
						<div className="nepal-sarkar-logo">
							<img
								src={NepalSarkarLogo}
								alt="nepal-sarkar-logo"
								style={{
									height: '25%',
									width: '25%',
									paddingTop: '10px',
									paddingBottom: '10px',
								}}
							/>
						</div>
						<Form onSubmit={handleSubmit} loading={isSubmitting || ui.loading} size="large" className="adminLogin-form">
							<Segment stacked>
								<Header as="h4" textAlign="center">
									नक्सा पास तथा भवन निर्माण व्यवस्थापन प्रणाली
									<br />
									<span className="english-div">Electronic Building Permit System (EBPS)</span>
								</Header>
								{ui.errors && <ErrorDisplay message={ui.errors.message} />}
								<Form.Field className="english-div">
									<Form.Input
										icon="user"
										type="text"
										name="email"
										autoComplete="username"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.email}
										error={errors.email}
										placeholder="Username"
									/>
								</Form.Field>
								<Form.Field>
									<Form.Input
										icon={<Icon name={hidden ? 'eye slash' : 'eye'} link onClick={() => setHidden(!hidden)} />}
										type={hidden ? 'password' : 'text'}
										name="password"
										autoComplete="current-password"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.password}
										error={errors.password}
										placeholder="Password"
									/>
								</Form.Field>
								<Button
									fluid
									size="large"
									type="submit"
									disabled={
										isSubmitting || ui.loading || !!(errors.email && touched.email) || !!(errors.password && touched.password)
									}
								>
									LOGIN
									<Loader active={ui.loading} size="small" inline />
								</Button>
								<br />
								<Grid>
									{needsPublicSignup && (
										<Grid.Column floated="left" width={4}>
											<Link to="/createAccount">
												<Item name="signup">Sign Up</Item>
											</Link>
										</Grid.Column>
									)}
									<Grid.Column floated="right" width={needsPublicSignup ? 6 : 16}>
										<Link to="/forgotPassword">
											<Item name="forget">Forgot Password?</Item>
										</Link>
									</Grid.Column>
								</Grid>
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		</>
	);
};

const Login = withFormik<LoginFormProps & OtherProps, LoginFormValues>({
	mapPropsToValues: props => ({
		email: props.initialEmail || '',
		password: props.initialPassword || '',
	}),

	validationSchema: Yup.object().shape({
		email: Yup.string()
			// .email('Email not valid')
			.required('Email is required'),
		password: Yup.string().required('Password is required'),
	}),

	handleSubmit({ email, password }: LoginFormValues, { props, setSubmitting, setErrors }) {
		setSubmitting(true);
		//@ts-ignore
		store.dispatch(loginUser({ email, password }, props.history));
		setSubmitting(false);
		// setSubmitted()
	},
})(LoginForm);

//@ts-ignore
const mapDispatchToProps = { loginUser };
//@ts-ignore
const mapStateToProps = state => {
	return {
		getError: state.root.login.loginError,
		getSubmit: state.root.login.submitBtn,
		needsPublicSignup: selectNeedsPublicSignup(state.root.dashboard),
		ui: state.root.ui,
		errors: state.root.ui.errors,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
