import React from 'react';

export const SectionHeader = ({ children }) => {
	return <div className="section-header">{children}</div>;
};
