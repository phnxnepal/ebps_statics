import React from 'react';

export const FlexSingleLeft = ({ className, children }) => {
	return (
		<div className={`flex-item-single-left ${className}`}>
			<div>{children}</div>
		</div>
	);
};

export const FlexSingleRight = ({ className = '', children }) => {
	return (
		<div className={`flex-item-single-right ${className}`}>
			<div>{children}</div>
		</div>
	);
};

export const FlexSpaceBetween = ({ className = '', children }) => (
	<div className="flex-container">
		<div className={`flex-item-space-between ${className}`}>{children}</div>
	</div>
);
