import * as React from 'react';
import { Header, Icon, Divider } from 'semantic-ui-react';
import JqxDataTable, { IDataTableProps, jqx } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
// Redux
import { connect } from 'react-redux';
import { setBuildPermitSetup, getTaskList, getDataByUrl, getLocalDataByKey } from '../store/actions/dashboardActions';
import FallbackComponent from './shared/FallbackComponent';
import { Loader, Dimmer } from 'semantic-ui-react';
import { isEmpty, getUserTypeValueNepali } from '../utils/functionUtils';
import ErrorDisplay from './shared/ErrorDisplay';
import { dashboardLang } from './../utils/data/dashboardLangFile';
import { getMessage } from './shared/getMessage';
import { FISCAL_YEAR_ALL, WARD_MASTER } from '../utils/constants';
import RootGrid from './loggedInComponents/Grid/RootGrid';
import { getFormMasterUrl, LocalAPIArray, LocalAPI, getFormMasterById } from '../utils/urlUtils';
import SearchForm from './loggedInComponents/shared/SearchForm';
import { UIState } from '../interfaces/ReduxInterfaces';
import { appListDataFields, appListColumns } from '../utils/jqxUtils';
import api from '../utils/api';
import Helmet from 'react-helmet';
import { getConstructionTypeText } from '../utils/enums/constructionType';
import { ApplicationListObject } from '../interfaces/DashboardInterfaces';

const dash_lang = dashboardLang.dashboard_data;
const messageId = 'dashboardLang.dashboard_data';

interface ApplistProps {
	getDataByUrl: Function;
	setBuildPermitSetup: Function;
	getTaskList: Function;
	getLocalDataByKey: Function;
	fiscalYears: object[];
	wardMaster: object[];
	applicationList: ApplicationListObject[];
}

interface ApplistState {
	appList: object[];
}

class Dashboard extends React.PureComponent<UIState & ApplistProps, IDataTableProps & ApplistState> {
	private tableRef = React.createRef<JqxDataTable>();

	constructor(props: UIState & ApplistProps) {
		super(props);

		this.state = {
			appList: [],
		};

		this.onSelectionInfo = this.onSelectionInfo.bind(this);
	}

	componentDidMount() {
		this.props.getTaskList(true);
		this.props.getLocalDataByKey(new LocalAPIArray([new LocalAPI(FISCAL_YEAR_ALL, 'fiscalYears'), new LocalAPI(WARD_MASTER, 'wardMaster')]));
	}

	componentDidUpdate(prevProps: UIState & ApplistProps) {
		if (prevProps.applicationList !== this.props.applicationList && this.props.applicationList) {
			this.setState({
				appList: this.props.applicationList.map((permit) => {
					return {
						...permit,
						applicationActionBy: getUserTypeValueNepali(permit.applicationActionBy),
						applicationFormName: getFormMasterById(permit.yourForm).name,
						constructionType: getConstructionTypeText(permit.constructionType),
					};
				}),
			});
		}
	}

	render() {
		const { appList } = this.state;

		return (
			<>
				<SearchForm
					getDataByUrl={this.props.getDataByUrl}
					loading={this.props.loading}
					fiscalYears={this.props.fiscalYears}
					wardMaster={this.props.wardMaster}
					api={api.applicationFilter}
				/>
				{
					// for props.errors
					this.props.errors && (
						//@ts-ignore
						<ErrorDisplay message={this.props.errors.message} />
					)
				}
				{this.props.errors ? (
					<FallbackComponent errors={this.props.errors} loading={this.props.loading} />
				) : (
					<React.Fragment>
						<Dimmer active={this.props.loading} inverted page>
							<Loader active={this.props.loading} inline="centered">
								{getMessage(`${messageId}.datafetch`, dash_lang.datafetch)}
							</Loader>
						</Dimmer>
						<Helmet title="EBPS - Application" />
						<div className="app-table">
							<Divider />
							<Header className="app-table-header">
								<Icon name="list alternate outline" />
								<Header.Content>{dash_lang.applicationList}</Header.Content>
							</Header>
							<div className="pointer">
								{/* 
								//@ts-ignore */}
								<RootGrid
									//@ts-ignore
									localdata={appList}
									datafield={appListDataFields}
									columns={appListColumns}
									groupable={false}
									rowsheight={60}
									showfilterrow={true}
									// autorowheight={true}
									// @ts-ignore
									onRowClick={this.onSelectionInfo}
								/>
							</div>
						</div>
					</React.Fragment>
				)}
			</>
		);
	}

	//@ts-ignore
	private onSelectionInfo(e) {
		// gets selected row indexes. The method returns an Array of indexes.
		const index = e.args.row.boundindex;

		window.scroll(0, 0);
		//@ts-ignore
		const currentPermit = this.state.appList[index];
		this.props
			.setBuildPermitSetup(currentPermit)
			//@ts-ignore
			.then(() => {
				//@ts-ignore
				if (!currentPermit.yourAction) {
					throw new Error('View URL not set for this user and build permit.');
				}
				//@ts-ignore
				if (!isEmpty(currentPermit) && !isEmpty(currentPermit.yourForm)) {
					const formUrl: string =
						//@ts-ignore
						currentPermit.isRejected === 'Y' ? currentPermit.applicationAction.viewURL : getFormMasterUrl(currentPermit.yourForm);

					// setTimeout(() => {
					//@ts-ignore
					this.props.history.push(formUrl.trim());
					// }, 1000);
				}
			})
			.catch((err: any) => {
				console.log(err);
			});
	}
}

const mapDispatchToProps = {
	setBuildPermitSetup,
	getTaskList,
	getDataByUrl,
	getLocalDataByKey,
};
//@ts-ignore
const mapStateToProps = (state) => ({
	//initial(in table)
	applicationList: state.root.dashboard.applicationList,
	fiscalYears: state.root.dashboard.fiscalYears,
	wardMaster: state.root.dashboard.wardMaster,
	//details of one id
	currentPermit: state.root.dashboard.currentPermit,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
