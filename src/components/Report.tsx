import * as React from 'react';
import { Header, Button, Grid, Select, Form, Label } from 'semantic-ui-react';
import Printthis from './shared/Printthis';
import { getReportPermits, fetchLocalDataAndReports } from './../store/actions/ReportActions';
import { getLocalDataByKey } from './../store/actions/dashboardActions';
import JqxGrid, { IGridProps, jqx } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import { connect } from 'react-redux';
import FallbackComponent from './shared/FallbackComponent';
import { FISCAL_YEAR_ALL, USER_INFO, WARD_MASTER } from '../utils/constants';
import { Reportlang } from '../utils/data/ReportLang';
// import { getFiscalYears } from "./../store/actions/AdminAction";
import { Formik } from 'formik';
import ErrorDisplay from './shared/ErrorDisplay';
import { withRouter } from 'react-router';
import { makeprintformat, brack, isDesigner } from './../utils/functionUtils';
import { setPrint } from '../store/actions/general';
import { FiscalYear, ReportObject, WardMaster } from '../interfaces/DashboardInterfaces';
import { UIState } from '../interfaces/ReduxInterfaces';
import { LocalAPIArray, LocalAPI } from '../utils/urlUtils';
import { FiscalYearOption, OptionType } from '../interfaces/OptionInterfaces';
import Helmet from 'react-helmet';
import { formatReport } from '../utils/reportUtils';
import { isStringEmpty } from '../utils/stringUtils';
import { consTypeWithDefaultSelectOptions } from '../utils/optionUtils';
const report_lang = Reportlang.report_data;

interface ReportListProps {
	reportPermits: object;
	fiscalYears: FiscalYear[];
	wardMaster: WardMaster[];
	userData: {
		organization: {
			name: string;
		};
	};
	getLocalDataByKey: (localApiArray: LocalAPIArray, needsLoading?: boolean) => {};
	getReportPermits: Function;
	fetchLocalDataAndReports: Function;
}

interface ReportListState {
	fiscalYearOptions: FiscalYearOption[];
	wardOptions: OptionType[];
	fiscalCode: number;
	wardNo: string;
	constructionType: string;
	organization: string;
	reportPermits: object[];
	isDesigner: boolean;
}

class Report extends React.PureComponent<UIState & ReportListProps, IGridProps & ReportListState> {
	private myGrid = React.createRef<JqxGrid>();

	constructor(props: UIState & ReportListProps) {
		super(props);
		this.state = {
			organization: '',
			fiscalYearOptions: [],
			reportPermits: [],
			wardOptions: [],
			fiscalCode: 0,
			wardNo: '',
			constructionType: '',
			isDesigner: isDesigner(),
		};
	}

	componentDidMount() {
		this.props.fetchLocalDataAndReports(
			new LocalAPIArray([
				new LocalAPI(FISCAL_YEAR_ALL, 'fiscalYears'),
				new LocalAPI(USER_INFO, 'userData'),
				new LocalAPI(WARD_MASTER, 'wardMaster'),
			])
		);
	}

	componentDidUpdate(prevProps: UIState & ReportListProps) {
		if (prevProps.fiscalYears !== this.props.fiscalYears) {
			if (this.props.fiscalYears && Array.isArray(this.props.fiscalYears)) {
				const fiscalYearOptions: FiscalYearOption[] = this.props.fiscalYears.map((year: FiscalYear) => {
					return {
						id: Number(year.yearCode),
						text: String(year.yearName),
						value: Number(year.yearCode),
					};
				});
				this.setState({ fiscalYearOptions, fiscalCode: fiscalYearOptions[0].value });
			}
		}

		if (prevProps.wardMaster !== this.props.wardMaster) {
			if (this.props.wardMaster && Array.isArray(this.props.wardMaster)) {
				const wardOptions: OptionType[] = this.props.wardMaster
					.map((ward: WardMaster) => {
						return {
							id: ward.id,
							text: String(ward.name),
							value: String(ward.name),
						};
					})
					.concat({ id: '', text: report_lang.allWards, value: '' });
				this.setState({ wardOptions, wardNo: '' });
			}
		}

		if (prevProps.userData !== this.props.userData && this.props.userData && this.props.userData.organization) {
			this.setState({ organization: this.props.userData.organization.name });
		}

		if (prevProps.reportPermits !== this.props.reportPermits && this.props.reportPermits) {
			const reportPermits: ReportObject[] = formatReport(this.props.reportPermits, this.state.isDesigner);
			this.setState({ reportPermits });
		}
	}

	updatePermits = (fiscalCode: number, wardNo: string, constructionType: string) => {
		this.props.getReportPermits(fiscalCode, wardNo, constructionType);
	};

	setPrintContent = (value: any) => {
		setPrint(value);
	};

	datafield = [
		{ name: 'id', type: 'string' },
		{ name: 'label', type: 'string' },
		{ name: 'count', type: 'string' },
	];

	column = [
		{
			text: 'क्र.स',
			dataField: 'id',
			cellsalign: 'center',
			align: 'center',
			width: '50px',
		},
		{
			text: 'निर्माणका चरण',
			dataField: 'label',
			cellsalign: 'center',
			align: 'center',
			width: '400px',
		},
		{
			text: 'संख्या',
			dataField: 'count',
			cellsalign: 'center',
			align: 'center',
			width: '200px',
		},
	];

	render() {
		const { loading, errors } = this.props;
		const { fiscalYearOptions, fiscalCode, reportPermits, organization, wardNo, constructionType, wardOptions } = this.state;

		return (
			<React.Fragment>
				<Helmet title="EBPS - Report" />
				<Formik
					initialValues={{
						fiscalyear: fiscalCode,
						wardNo,
						constructionType,
					}}
					onSubmit={() => {}}
					enableReinitialize={true}
					render={(props) => {
						return (
							<>
								{
									// for props.errors
									errors && <ErrorDisplay message={errors.message} />
								}

								<Grid centered>
									<Grid.Row>
										<Grid.Column computer={8} mobile={16} tablet={12} textAlign="left">
											{/* 
                    								//@ts-ignore */}
											<div ref={(r) => (this.RPrint = r)}>
												<Header style={{ textAlign: 'center' }}>
													<Header.Content>
														{organization} {report_lang.infobarName}
													</Header.Content>
												</Header>
												<br />
												<div
													className="flex-container-flex-start margin-bottom"
													// style={{
													// 	display: 'flex',
													// 	paddingBottom: '5px',
													// }}
												>
													<Form.Field className="item zIndexSelect compact">
														{report_lang.fiscalYear}
														<Select
															placeholder="Select Fiscal Year"
															options={fiscalYearOptions}
															name="fiscalyear"
															onChange={(e, { value }) => {
																props.setFieldValue('fiscalyear', value);
																this.setState({ fiscalCode: Number(value) });
																this.updatePermits(Number(value), wardNo, constructionType);
															}}
															value={props.values.fiscalyear}
														/>
													</Form.Field>
													<Form.Field className="item zIndexSelect compact">
														{report_lang.wardNo}:
														<Select
															options={wardOptions}
															name="wardNo"
															onChange={(e, { value }) => {
																props.setFieldValue('wardNo', value);
																this.setState({ wardNo: String(value) });
																this.updatePermits(fiscalCode, String(value), constructionType);
															}}
															value={props.values.wardNo}
														/>
													</Form.Field>
													<Form.Field className="item zIndexSelect compact">
														{report_lang.constructionType}:
														<Select
															options={consTypeWithDefaultSelectOptions}
															name="constructionType"
															onChange={(e, { value }) => {
																props.setFieldValue('constructionType', value);
																this.setState({ constructionType: String(value) });
																this.updatePermits(fiscalCode, wardNo, String(value));
															}}
															value={props.values.constructionType}
														/>
													</Form.Field>
												</div>
												{errors || loading ? (
													<FallbackComponent errors={errors} loading={loading} />
												) : (
													<div className="jqx-tables">
														<JqxGrid
															//@ts-ignore
															ref={this.myGrid}
															// @ts-ignore
															width={'650px'}
															//@ts-ignore
															groupable={this.groupable}
															source={
																new jqx.dataAdapter({
																	//@ts-ignore
																	datafields: this.datafield,
																	datatype: 'array',
																	id: 'id',
																	//@ts-ignore
																	localdata: reportPermits,
																})
															}
															//@ts-ignore
															columns={this.column}
															autoheight={true}
															sortable={true}
															columnsresize={true}
															rowsheight={35}
															filterable={true}
															//@ts-ignore
															onRowselect={
																//@ts-ignore
																this.onRowClick
															}
														/>
													</div>
												)}
											</div>
											<br />
											<Printthis
												ui={
													<div>
														<Button color="violet">Print</Button>
													</div>
												}
												//@ts-ignore
												content={() => this.RPrint}
												onBeforeGetContent={() => {
													this.setPrintContent(true);
													//@ts-ignore
													this.RPrint = this.RPrint.cloneNode(true);
													//@ts-ignore
													brack(this.RPrint);

													makeprintformat(
														//@ts-ignore
														this.RPrint,
														'getElementsByClassName',
														'ui dropdown',
														'innerText'
													);
												}}
												onBeforePrint={() => {
													this.setPrintContent(false);
												}}
											/>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</>
						);
					}}
				/>
			</React.Fragment>
		);
	}
	//@ts-ignore
	private onRowClick = (e) => {
		// gets selected row indexes. The method returns an Array of indexes.
		// const index = e.args.index;
		const id = e.args.row.boundindex;

		//@ts-ignore
		if (e.args.row.label !== 'कुल राजस्व')
			//@ts-ignore
			this.props.history.push({
				//@ts-ignore
				pathname: `/user/report-list/${this.state.fiscalCode}/${isStringEmpty(this.state.wardNo) ? 'allWards' : this.state.wardNo}/${
					isStringEmpty(this.state.constructionType) ? 'all' : this.state.constructionType
				}/${id + 1}`,
				// state: {
				// 	id: id,
				// 	//@ts-ignore
				// 	fiscalCode: this.state.fiscalCode,
				// },
			});
	};
}

const mapDispatchToProps = {
	getReportPermits,
	getLocalDataByKey,
	fetchLocalDataAndReports,
	setPrint: (res: any) => setPrint(res),
};
//@ts-ignore
const mapStateToProps = (state) => ({
	reportPermits: state.root.dashboard.reportPermits,
	fiscalYears: state.root.dashboard.fiscalYears,
	userData: state.root.dashboard.userData,
	wardMaster: state.root.dashboard.wardMaster,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

//@ts-ignore
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Report));
