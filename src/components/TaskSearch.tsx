import * as React from 'react';
import { Header, Icon, Divider } from 'semantic-ui-react';
import JqxDataTable, { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
// Redux
import { connect } from 'react-redux';
//@ts-ignore
import { setBuildPermitSetup, clearBuildPermits, getDataByUrl, getLocalDataByKey } from '../store/actions/dashboardActions';
import FallbackComponent from './shared/FallbackComponent';
import { Loader, Dimmer } from 'semantic-ui-react';
import { isEmpty, getUserNameNepali } from '../utils/functionUtils';
import ErrorDisplay from './shared/ErrorDisplay';
import { dashboardLang } from './../utils/data/dashboardLangFile';
import { getMessage } from './shared/getMessage';
//@ts-ignore
import { FISCAL_YEAR_ALL, WARD_MASTER } from '../utils/constants';
import RootGrid from './loggedInComponents/Grid/RootGrid';
import SearchForm from './loggedInComponents/shared/SearchForm';
import { UIState } from '../interfaces/ReduxInterfaces';
import { appListDataFields, appListColumns, namsariTaskListDataField, namsariTaskListColumns } from '../utils/jqxUtils';
import { LocalAPIArray, LocalAPI, getFormMasterUrl, getFormMasterById } from '../utils/urlUtils';
import api from '../utils/api';
import { withRouter } from 'react-router';
import { isStringEmpty } from '../utils/stringUtils';
import { ApplicationListObject, ApplicationListRawObject } from '../interfaces/DashboardInterfaces';
import Helmet from 'react-helmet';
import { getLocalStorage } from '../utils/secureLS';
import { getUserTypeValueNepali } from './../utils/functionUtils';
import { getConstructionTypeText } from '../utils/enums/constructionType';
import { namsariFilter } from '../utils/applicationUtils';

const dash_lang = dashboardLang.dashboard_data;
const taskData = dashboardLang.taskListData;
const messageId = 'dashboardLang.dashboard_data';

interface ApplistProps {
	getDataByUrl: Function;
	setBuildPermitSetup: Function;
	clearBuildPermits: Function;
	getLocalDataByKey: Function;
	fiscalYears: object[];
	wardMaster: object[];
	applicationList: ApplicationListRawObject[];
}

interface ApplistState {
	isNamsari: boolean;
	appList: ApplicationListObject[];
	searchParams: {
		nibedakName: string;
		constructionType: string;
		year: string;
		kittaNo: string;
		wardNo: string;
		applicationNo: string;
		applicationStatus: string;
	};
}

class Dashboard extends React.PureComponent<UIState & ApplistProps, IDataTableProps & ApplistState> {
	private tableRef = React.createRef<JqxDataTable>();

	constructor(props: UIState & ApplistProps) {
		super(props);

		this.state = {
			appList: [],
			isNamsari: false,
			searchParams: {
				nibedakName: '',
				constructionType: '',
				year: '',
				kittaNo: '',
				wardNo: '',
				applicationNo: '',
				applicationStatus: '',
			},
		};

		this.onSelectionInfo = this.onSelectionInfo.bind(this);
	}

	componentDidMount() {
		this.props.clearBuildPermits();
		this.props.getLocalDataByKey(new LocalAPIArray([new LocalAPI(FISCAL_YEAR_ALL, 'fiscalYears'), new LocalAPI(WARD_MASTER, 'wardMaster')]));
		//@ts-ignore
		const { state } = this.props.location;

		if (state) {
			this.setState(prevState => ({ searchParams: { ...prevState.searchParams, ...state.searchParams } }));
			this.props.getDataByUrl([
				{
					api: `${api.taskFilter}?nibedakName=&constructionType=${
						state.searchParams.constructionType
					}&year=${''}&kittaNo=${''}&wardNo=${''}&applicationStatus=`,
					objName: 'applicationList',
				},
			]);
		} else {
			this.props.getDataByUrl([
				{
					api: `${api.taskFilter}?nibedakName=&constructionType=${''}&year=${''}&kittaNo=${''}&wardNo=${''}&applicationStatus=`,
					objName: 'applicationList',
				},
			]);
		}
	}

	componentDidUpdate(prevProps: UIState & ApplistProps, prevState: ApplistState) {
		if (prevProps.applicationList !== this.props.applicationList && this.props.applicationList) {
			//@ts-ignore
			const pathname = this.props.location.pathname;

			if (pathname && pathname.endsWith('namsari')) {
				this.setState({
					isNamsari: true,
					appList: this.props.applicationList
						.filter(namsariFilter(true))
						.map(permit => {
							return {
								...permit,
								applicationActionBy: getUserTypeValueNepali(permit.applicationActionBy),
								applicationFormName: getFormMasterById(permit.yourForm).name,
								constructionType: getConstructionTypeText(permit.constructionType),
							};
						}),
				});
			} else {
				this.setState({
					appList: this.props.applicationList
						.filter(namsariFilter())
						.map(permit => {
							return {
								...permit,
								applicationActionBy: getUserTypeValueNepali(permit.applicationActionBy),
								applicationFormName: getFormMasterById(permit.yourForm).name,
								constructionType: getConstructionTypeText(permit.constructionType),
							};
						}),
				});
			}
		}

		//@ts-ignore
		if (this.props.location !== prevProps.location) {
			//@ts-ignore
			const pathname = this.props.location.pathname;

			if (pathname && pathname.endsWith('namsari')) {
				this.setState({
					isNamsari: true,
				});
			} else {
				this.setState({ isNamsari: false });
			}
			//@ts-ignore
			const { state } = this.props.location;

			if (state) {
				this.setState(prevState => ({ searchParams: { ...prevState.searchParams, ...state.searchParams } }));
				this.props.getDataByUrl([
					{
						api: `${api.taskFilter}?nibedakName=&constructionType=${
							state.searchParams.constructionType
						}&year=${''}&kittaNo=${''}&wardNo=${''}&applicationStatus=`,
						objName: 'applicationList',
					},
				]);
			} else {
				this.props.getDataByUrl([
					{
						api: `${api.taskFilter}?nibedakName=&constructionType=${''}&year=${''}&kittaNo=${''}&wardNo=${''}&applicationStatus=`,
						objName: 'applicationList',
					},
				]);
			}
		}
	}

	render() {
		const { appList, searchParams, isNamsari } = this.state;

		return (
			<>
				{this.props.errors ? (
					<FallbackComponent errors={this.props.errors} loading={this.props.loading} />
				) : (
					<React.Fragment>
						<Helmet title="EBPS - Task Search" />
						<SearchForm
							searchParams={searchParams}
							getDataByUrl={this.props.getDataByUrl}
							loading={this.props.loading}
							fiscalYears={this.props.fiscalYears}
							wardMaster={this.props.wardMaster}
							api={api.taskFilter}
						/>
						{// for props.errors
						this.props.errors && (
							//@ts-ignore
							<ErrorDisplay message={this.props.errors.message} />
						)}
						<Dimmer active={this.props.loading} inverted page>
							<Loader active={this.props.loading} inline="centered">
								{getMessage(`${messageId}.datafetch`, dash_lang.datafetch)}
							</Loader>
						</Dimmer>
						<div className="app-table">
							<Divider />
							<Header className="app-table-header">
								<Icon name="list alternate outline" />
								<Header.Content>{taskData.taskList}</Header.Content>
							</Header>
							<div className="pointer">
								{/* 
								//@ts-ignore */}
								<RootGrid
									//@ts-ignore
									localdata={appList}
									datafield={isNamsari ? namsariTaskListDataField : appListDataFields}
									columns={isNamsari ? namsariTaskListColumns : appListColumns}
									groupable={false}
									rowsheight={60}
									showfilterrow={true}
									// autorowheight={true}
									// @ts-ignore
									onRowClick={this.onSelectionInfo}
								/>
							</div>
						</div>
					</React.Fragment>
				)}
			</>
		);
	}

	//@ts-ignore
	private onSelectionInfo(e) {
		// gets selected row indexes. The method returns an Array of indexes.
		const index = e.args.row.boundindex;

		window.scroll(0, 0);
		//@ts-ignore
		const currentPermit = this.state.appList[index];
		this.props
			.setBuildPermitSetup(currentPermit)
			//@ts-ignore
			.then(() => {
				//@ts-ignore
				if (!currentPermit.yourAction) {
					throw new Error('View URL not set for this user and build permit.');
				}
				//@ts-ignore
				if (!isEmpty(currentPermit) && !isEmpty(currentPermit.yourForm)) {
					const formUrl: string =
						//@ts-ignore
						currentPermit.isRejected === 'Y' ? currentPermit.applicationAction.viewURL : getFormMasterUrl(currentPermit.yourForm);

					// setTimeout(() => {
					//@ts-ignore
					this.props.history.push(formUrl.trim());
					// }, 1000);
				}
			})
			.catch((err: any) => {
				console.log(err);
			});
	}
}

const mapDispatchToProps = {
	setBuildPermitSetup,
	clearBuildPermits,
	getDataByUrl,
	getLocalDataByKey,
};
//@ts-ignore
const mapStateToProps = state => ({
	//initial(in table)
	applicationList: state.root.dashboard.applicationList,
	fiscalYears: state.root.dashboard.fiscalYears,
	wardMaster: state.root.dashboard.wardMaster,
	//details of one id
	currentPermit: state.root.dashboard.currentPermit,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});
//@ts-ignore
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Dashboard));
