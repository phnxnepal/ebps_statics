import React, { useEffect, useState } from 'react';
import { Segment, Icon, Grid, Card, Header, Divider } from 'semantic-ui-react';
import groupBy from 'lodash/groupBy';
import { Link } from 'react-router-dom';
import { LSKey } from '../utils/enums/localStorageKeys';
import { adminDropdownData } from '../routes/AdminRoutes';
import { getLocalStorage } from '../utils/secureLS';
import { SectionHeader } from './uiComponents/Headers';

const AdminDashboard = () => {
	const [menu, setMenu] = useState([]);
	useEffect(() => {
		const userMenus = JSON.parse(getLocalStorage(LSKey.USER_MENU));
		const userMenuUrls = userMenus.map((menu) => menu.url);
		const requiredMenu = adminDropdownData.filter((menu) => userMenuUrls.includes(`${menu.layout}${menu.path}`));
		setMenu(requiredMenu);
	}, []);
	return (
		<div className="setup-cards">
			<Divider />
			{Object.entries(groupBy(menu, 'tag')).map(([tag, menus]) => (
				<React.Fragment key={tag}>
					<SectionHeader>
						<h4 className="left-align">{tag.toUpperCase()} SETUP</h4>
					</SectionHeader>
					<Card.Group>
						{menus.map((m) => (
							<Card
								key={m.id}
								header={
									<b>
										<Icon name={m.icon} /> {m.name}
									</b>
								}
								link
								as={Link}
								to={`${m.layout}${m.path}`}
							/>
						))}
					</Card.Group>
					<br />
				</React.Fragment>
			))}
		</div>
	);
};

export default AdminDashboard;
