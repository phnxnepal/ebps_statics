import React from 'react';
import Footer from '../loggedInComponents/Footer';
import { Segment, Table } from 'semantic-ui-react';
import { getPublicMason } from '../../store/actions/PublicAction';
import { connect } from 'react-redux';
import { checkError, getOptionText } from '../../utils/dataUtils';
import FallbackComponent from '../shared/FallbackComponent';
import { ImageDisplayWithViewer } from '../shared/file/FileView';
import Helmet from 'react-helmet';

const statusOption = [
	{ value: 'Y', text: 'Active' },
	{ value: 'N', text: 'Not Active' },
];

export class Reg_mason extends React.PureComponent {
	//@ts-ignore
	constructor(props) {
		super(props);
		this.state = {};
	}

	componentDidMount() {
		// @ts-ignore
		this.props.getPublicMason();
	}

	render() {
		// @ts-ignore
		const masonList = checkError(this.props.userData);
		//@ts-ignore
		const { loading, errors } = this.props;

		return (
			<>
				<div className="public-div">
					<Helmet>
						<title>EBPS - Masons List</title>
					</Helmet>
					{loading || errors ? (
						<FallbackComponent errors={errors} loading={loading} />
					) : (
						<>
							<Segment attached className="tableSectionHeader">
								Mason Details
							</Segment>
							<Table celled striped attached selectable>
								<Table.Header>
									<Table.Row>
										<Table.HeaderCell>Photo</Table.HeaderCell>
										<Table.HeaderCell>Name</Table.HeaderCell>
										<Table.HeaderCell>Address</Table.HeaderCell>
										<Table.HeaderCell>Contact No.</Table.HeaderCell>
										<Table.HeaderCell>Municipal Registration No.</Table.HeaderCell>
										<Table.HeaderCell>Status</Table.HeaderCell>
									</Table.Row>
								</Table.Header>

								<Table.Body>
									{// @ts-ignore
									// @ts-ignore
									Array.isArray(masonList) &&
										// @ts-ignore
										masonList.map((mason, index) => {
											return (
												<Table.Row key={index}>
													<Table.Cell>
														<ImageDisplayWithViewer type={null} alt={'mason'} width={60} src={mason.photo} />
													</Table.Cell>
													<Table.Cell>{mason.name}</Table.Cell>
													<Table.Cell>{mason.address}</Table.Cell>
													<Table.Cell>{mason.contactNo}</Table.Cell>
													<Table.Cell>{mason.municipalRegNo}</Table.Cell>
													{/* 
												// @ts-ignore */}
													<Table.Cell>{getOptionText(mason.status, statusOption)}</Table.Cell>
												</Table.Row>
											);
										})}
								</Table.Body>
							</Table>
						</>
					)}
					{/* </Segment> */}
				</div>
				<br />
				<Footer />
			</>
		);
	}
}

const mapDispatchToProps = { getPublicMason };
//@ts-ignore
const mapStateToProps = state => ({
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	userData: state.root.public.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Reg_mason);

