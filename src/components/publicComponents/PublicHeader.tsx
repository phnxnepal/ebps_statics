import React, { Component } from 'react';
import { Menu, Icon, Header, Button, Sidebar, Segment } from 'semantic-ui-react';
import { Link, NavLink, withRouter } from 'react-router-dom';
import NepalSarkarLogo from './../../assets/images/Nepal_sarkar_logo.webp';
import { toggleNavbar } from '../../store/actions/uiActions';
import { connect } from 'react-redux';

interface HeaderProps {
	isNavOpen: boolean;
	toggleNavbar: (value: boolean) => {};
}
const menuItems = [
	{
		link: '/',
		id: 'home',
		name: 'Home',
	},
	{
		link: '/downloads',
		id: 'downloads',
		name: 'Downloads',
	},
	{
		link: '/faq',
		id: 'faq',
		name: 'FAQ',
	},
	{
		link: '/designer',
		id: 'designers',
		name: 'Registered Designers',
	},
	{
		link: '/mason',
		id: 'masons',
		name: 'Masons List',
	},
	{
		link: '/contact',
		id: 'contact',
		name: 'Contact',
	},
];
//@ts-ignore
class PublicHeader extends Component<HeaderProps> {
	//@ts-ignore
	constructor(props) {
		super(props);
		this.state = {};
	}
	render() {
		return (
			<Menu inverted className="ebpsMain-header fixed">
				<Menu.Item>
					<Header style={{ color: 'white' }}>
						<Link to="/">
							<div className="logo-div">
								<span className="comp-logo">
									<img src={NepalSarkarLogo} alt="phoenix-logo" />
								</span>
								<span>EBPS</span>
							</div>
						</Link>
					</Header>
				</Menu.Item>
				<Menu.Item>
					<Header style={{ color: 'white' }}>
						{
							//@ts-ignore
							this.props.orgName
						}
					</Header>
				</Menu.Item>
				<Menu.Menu position="right" className="navbar-toggle">
					<Menu.Item>
						<Link to="/login">
							<Button primary to="/login">
								<Icon name="sign-in" />
								Log In
							</Button>
						</Link>
					</Menu.Item>
					<Menu.Item>
						<Icon name="bars" size="large" onClick={() => this.props.toggleNavbar(!this.props.isNavOpen)} />
					</Menu.Item>
				</Menu.Menu>
				<Menu.Menu position="right" className="navbar-items">
					{menuItems.map((menu, idx) => (
						<Menu.Item key={idx} exact as={NavLink} to={menu.link} name={menu.name} id={menu.id} />
					))}
					<Menu.Item>
						<Link to="/login">
							<Button primary>
								<Icon name="sign-in" />
								Log In
							</Button>
						</Link>
					</Menu.Item>
				</Menu.Menu>

				<aside className="drawer-menu">
					<nav className="sidenav" style={{ width: this.props.isNavOpen ? '200px' : '0px' }}>
						<Icon name="window close outline" size="big" className="closebtn" onClick={() => this.props.toggleNavbar(false)} />
						<Sidebar.Pushable>
							<Sidebar
								// icon="labeled"
								direction="left"
								vertical
								inverted
								visible={this.props.isNavOpen}
								width="wide"
								as={Menu}
								onHide={() => this.props.toggleNavbar(false)}
							>
								{/* {menuItems.map((menu, idx) => (
									<Menu.Item key={idx} exact as={NavLink} to={menu.link} name={menu.name} id={menu.id} />
								))} */}
								<Menu.Item exact as={NavLink} to="/">
									<span>
										<Icon name="home" />
										Home
									</span>
								</Menu.Item>
								<Menu.Item exact as={NavLink} to="/downloads">
									<span>
										<Icon name="download" />
										Downloads
									</span>
								</Menu.Item>
								<Menu.Item exact as={NavLink} to="/faq">
									<span>
										<Icon name="question circle" />
										FAQ
									</span>
								</Menu.Item>
								<Menu.Item exact as={NavLink} to="/designer">
									<span>
										<Icon name="user" />
										Registered Designers
									</span>
								</Menu.Item>
								<Menu.Item exact as={NavLink} to="/mason">
									<span>
										<Icon name="list ul" />
										Masons List
									</span>
								</Menu.Item>
								<Menu.Item exact as={NavLink} to="/contact">
									<span>
										<Icon name="phone volume" />
										Contact
									</span>
								</Menu.Item>
							</Sidebar>
						</Sidebar.Pushable>
					</nav>
				</aside>
			</Menu>
		);
	}
}
//@ts-ignore
const mapStateToProps = state => {
	return {
		isNavOpen: state.root.ui.isNavOpen,
	};
};

//@ts-ignore
const mapDispatchToProps = { toggleNavbar };

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(props => <PublicHeader {...props} />));
