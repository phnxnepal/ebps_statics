import React, { Component } from 'react';
import { Grid, Image, Container } from 'semantic-ui-react';
import img from '../../assets/images/MNEBPSflowDiagram.webp';
import Helmet from 'react-helmet';

export class Home extends Component {
	render() {
		return (
			<div className="public-div">
				<Helmet><title>EBPS - Home</title></Helmet>
				<h1 className="main-heading">{/* Mechinagar Municipality */}</h1>
				<Grid>
					<Grid.Row>
						<Grid.Column width={10}>
							<Image src={img} />
						</Grid.Column>
						<Grid.Column width={5}>
							<Container>
								<h2 className="sub-heading">What is EBPS?</h2>
								<p>
									E-BPS is an online web application software system developed as an alternative to existing building permit process. Through E-BPS
									building permit applications for new buildings are processed as well as the existing building records are maintained.
								</p>
								<p>
									E-BPS is a distributed system that will be implemented at both the municipal offices as well as their respective ward offices. This
									system allows general public to submit their applications over Internet. It supports data verification and rejection by different
									municipal staff in accordance to their authority and hierarchy.
								</p>
							</Container>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

export default Home;
