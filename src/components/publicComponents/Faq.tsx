import React from 'react';
import { Segment, Container } from 'semantic-ui-react';
import { getFAQ } from './../../store/actions/AdminAction';
import FallbackComponent from '../shared/FallbackComponent';
import { connect } from 'react-redux';
import { UIState } from '../../interfaces/ReduxInterfaces';
import { isEmpty } from '../../utils/functionUtils';
import Helmet from 'react-helmet';
import { SectionHeader } from '../uiComponents/Headers';

interface FAQProps {
	faqData: Array<FAQResponse>;
}

interface FAQState {
	data: string;
}

interface FAQResponse {
	jsonData: string;
}

export class Faq extends React.PureComponent<UIState & FAQProps, FAQState> {
	constructor(props: UIState & FAQProps) {
		super(props);
		this.state = {
			data: '',
		};
	}
	componentDidMount() {
		//@ts-ignore
		this.props.getFAQ();
	}

	componentDidUpdate(prevProps: UIState & FAQProps) {
		if (prevProps.faqData !== this.props.faqData) {
			const { faqData } = this.props;
			try {
				const data = faqData && Array.isArray(faqData) && JSON.parse(faqData[0].jsonData).jsonData;
				this.setState({ data });
			} catch {
				this.setState({ data: '' });
			}
		}
	}

	render() {
		const { loading, errors } = this.props;
		const { data } = this.state;
		return (
			<div className="public-div">
				<Helmet>
					<title>EBPS - FAQ</title>
				</Helmet>
				{loading || errors ? (
					<FallbackComponent errors={errors} loading={loading} />
				) : (
					<>
						<br />
						<Container>
							<SectionHeader>
								<h3 className="left-align english-div">Frequently Asked Questions</h3>
							</SectionHeader>
							{isEmpty(data) ? (
								<Segment textAlign="center">No data found.</Segment>
							) : (
								<div className="content">
									<div dangerouslySetInnerHTML={{ __html: data }} />
								</div>
							)}
						</Container>
					</>
				)}
			</div>
		);
	}
}

const mapDispatchToProps = {
	getFAQ,
};

//@ts-ignore
const mapStateToProps = state => ({
	faqData: state.root.admin.faqData,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Faq);
