import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';
import { Segment, Table, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { getDownloads } from '../../store/actions/AdminAction';
import { getDocUrl } from '../../utils/config';
import { checkError } from '../../utils/dataUtils';
import { isEmpty } from '../../utils/functionUtils';
import FallbackComponent from '../shared/FallbackComponent';
import Helmet from 'react-helmet';
class Downloads extends Component {
	componentDidMount() {
		//@ts-ignore
		this.props.getDownloads();
	}
	render() {
		// @ts-ignore
		const downloadList = checkError(this.props.userData);
		//@ts-ignore
		const { loading, errors } = this.props;

		// console.log(downloadList);

		return (
			// <React.Fragment>
			<div className="public-div">
				<Helmet><title>EBPS - Downloads</title></Helmet>

				{loading || errors ? (
					<FallbackComponent errors={errors} loading={loading} />
				) : (
						<>
							<Segment attached className="tableSectionHeader">
								Downloads Data
						</Segment>
							{/*
                    // @ts-ignore */}
							{isEmpty(downloadList) || downloadList.length < 1 ? (
								<>
									<div className="download-content">
										<Message icon="download" header="Required Document Will Be Uploaded Soon" />
									</div>
								</>
							) : (
									<>
										<Table striped celled attached selectable compact="very" className="small-font">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>शीर्षक</Table.HeaderCell>
													<Table.HeaderCell>फाइल</Table.HeaderCell>
													<Table.HeaderCell>मिति</Table.HeaderCell>
													<Table.HeaderCell>कार्य</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{// @ts-ignore
													Array.isArray(downloadList) &&
													// @ts-ignore
													downloadList.map((down, index) => {
														return (
															<Table.Row key={index}>
																<Table.Cell>{down.fileType}</Table.Cell>
																<Table.Cell className="english-div">{down.fileUrl}</Table.Cell>
																<Table.Cell>{down.uploadDate}</Table.Cell>
																<Table.Cell>
																	<a href={`${getDocUrl()}${down.fileUrl}`} target="_blank" rel="noopener noreferrer">
																		<Icon title="Download" circular color="blue" name="download" />
																	</a>
																</Table.Cell>
															</Table.Row>
														);
													})}
											</Table.Body>
										</Table>
									</>
								)}
						</>

					)}
			</div>
			// </React.Fragment>
		);
	}
}

const mapDispatchToProps = { getDownloads };
//@ts-ignore
const mapStateToProps = state => ({
	userData: state.root.admin.userData,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});
export default connect(mapStateToProps, mapDispatchToProps)(Downloads);
