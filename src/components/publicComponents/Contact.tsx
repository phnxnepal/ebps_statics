import React from 'react';
import { getPublicDataByUrl } from './../../store/actions/PublicAction';
import FallbackComponent from '../shared/FallbackComponent';
import { connect } from 'react-redux';
import { Segment, Container } from 'semantic-ui-react';
import { UIState } from '../../interfaces/ReduxInterfaces';
import { isEmpty } from '../../utils/functionUtils';
import Helmet from 'react-helmet';
import { SectionHeader } from '../uiComponents/Headers';
import api from '../../utils/api';
import { ApiParam } from '../../utils/paramUtil';

interface ContactProps {
	conData: Array<ContanctResponse>;
	getPublicDataByUrl: Function;
}

interface ContactState {
	data: string;
}

interface ContanctResponse {
	jsonData: string;
}

export class Contact extends React.PureComponent<UIState & ContactProps, ContactState> {
	constructor(props: UIState & ContactProps) {
		super(props);
		this.state = {
			data: this.getDataFromProps(),
		};
	}

	getDataFromProps() {
		const { conData } = this.props;
		try {
			const data = conData && Array.isArray(conData) && JSON.parse(conData[0].jsonData).jsonData;
			return data;
		} catch {
			return '';
		}
	}

	componentDidMount() {
		this.props.getPublicDataByUrl([new ApiParam(api.contacts, 'conData')]);
	}

	componentDidUpdate(prevProps: UIState & ContactProps) {
		if (prevProps.conData !== this.props.conData) {
			this.setState({ data: this.getDataFromProps() });
		}
	}

	render() {
		const { loading, errors } = this.props;
		const { data } = this.state;
		return (
			<div className="public-div">
				<Helmet>
					<title>EBPS - Contact</title>
				</Helmet>
				{loading || errors ? (
					<FallbackComponent errors={errors} loading={loading} />
				) : (
					<Container>
						<br />
						<SectionHeader>
							<h3 className="left-align end-section">सम्पर्क जानकारी</h3>
						</SectionHeader>
						{isEmpty(data) ? (
							<Segment textAlign="center">No data found.</Segment>
						) : (
							<div>
								<div dangerouslySetInnerHTML={{ __html: data }} />
							</div>
						)}
					</Container>
				)}
			</div>
		);
	}
}

const mapDispatchToProps = {
	getPublicDataByUrl,
};

//@ts-ignore
const mapStateToProps = (state) => ({
	conData: state.root.public.conData,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
