import React from 'react';
import { Segment, Table } from 'semantic-ui-react';
import { getPublicDesigner } from '../../store/actions/PublicAction';
import { connect } from 'react-redux';
import { UIState } from '../../interfaces/ReduxInterfaces';
import { ImageDisplayWithViewer } from '../shared/file/FileView';
import FallbackComponent from '../shared/FallbackComponent';
import Helmet from 'react-helmet';

interface DesignerProps {
	userData: DesignerObject[];
}
interface DesignerState {
	data: DesignerObject[];
}

interface DesignerObject {
	userName: string;
	address: string;
	email: string;
	mobile: string;
	consultancyName: string;
	photo: string;
}
export class Reg_designer extends React.PureComponent<UIState & DesignerProps, DesignerState> {
	//@ts-ignore
	constructor(props) {
		super(props);
		this.state = {
			data: [],
		};
	}

	componentDidMount() {
		//@ts-ignore
		this.props.getPublicDesigner();
	}

	componentDidUpdate(prevProps: UIState & DesignerProps) {
		if (prevProps.userData !== this.props.userData) {
			const { userData } = this.props;
			try {
				const data = userData && Array.isArray(userData) ? userData : [];
				this.setState({ data });
			} catch {
				this.setState({ data: [] });
			}
		}
	}

	render() {
		const { data } = this.state;
		const { loading, errors } = this.props;
		return (
			<div className="public-div">
				<Helmet>
					<title>EBPS - Registered Designers</title>
				</Helmet>
				<Segment size="tiny" attached className="tableSectionHeader">
					डिजाईनर सुचि
				</Segment>
				{loading || errors ? (
					<div>
						<br />
						<FallbackComponent errors={errors} loading={loading} />
					</div>
				) : (
					<>
						<Table celled striped attached selectable compact="very" size="small">
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell>Photo</Table.HeaderCell>
									<Table.HeaderCell>Name</Table.HeaderCell>
									<Table.HeaderCell>Address</Table.HeaderCell>
									<Table.HeaderCell>E-mail</Table.HeaderCell>
									<Table.HeaderCell>Phone No.</Table.HeaderCell>
									<Table.HeaderCell>Consultancy Name</Table.HeaderCell>
								</Table.Row>
							</Table.Header>

							<Table.Body>
								{data.map((userif: DesignerObject, index) => (
									<Table.Row key={index}>
										<Table.Cell>
											<ImageDisplayWithViewer type={null} alt={'designer'} width={60} src={userif.photo} />
										</Table.Cell>
										<Table.Cell>{userif.userName}</Table.Cell>
										<Table.Cell>{userif.address}</Table.Cell>
										<Table.Cell className="english-div">{userif.email}</Table.Cell>
										<Table.Cell>{userif.mobile}</Table.Cell>
										<Table.Cell>{userif.consultancyName}</Table.Cell>
									</Table.Row>
								))}
							</Table.Body>
						</Table>
					</>
				)}
			</div>
		);
	}
}

const mapDispatchToProps = { getPublicDesigner };
//@ts-ignore
const mapStateToProps = state => ({
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	userData: state.root.public.userData,
});

export default connect(mapStateToProps, mapDispatchToProps)(Reg_designer);
