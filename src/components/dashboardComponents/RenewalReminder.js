import React from 'react';
import { Grid, Message } from 'semantic-ui-react';
import { taskList } from '../../utils/data/genericData';

export const RenewalReminder = ({ daysRemaining }) => {
	return (
		<Grid>
			<Grid.Row>
				<Grid.Column floated="right" width={6}>
					<Message
						floating
						negative
						className="renew"
						icon="warning circle"
						header={taskList.renewalReminder.heading}
						content={`${taskList.renewalReminder.contentPrefix} ${daysRemaining} ${taskList.renewalReminder.contentSuffix}`}
					/>
				</Grid.Column>
			</Grid.Row>
		</Grid>
	);
};
