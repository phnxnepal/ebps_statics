import React from 'react';
// import RootGrid from '../loggedInComponents/Grid/RootGrid';
import { Form, Select, Header, Icon, Statistic } from 'semantic-ui-react';
import { Reportlang } from '../../utils/data/ReportLang';
import { useFiscalYearAll } from '../../hooks/useFiscalYear';
import { CompactInfoMessage } from '../shared/formComponents/CompactInfoMessage';
import { taskList } from '../../utils/data/genericData';
import { Loading } from '../shared/FallbackComponent';
// import { translateEngToNep } from '../../utils/langUtils';
import { Formik } from 'formik';
// import { useHistory } from 'react-router';

// const datafield = [
// 	{ name: 'id', type: 'string' },
// 	{ name: 'NirmanCharan', type: 'string' },
// 	{ name: 'NirmanCharanNumber', type: 'string' },
// ];

// const columns = [
// 	{
// 		text: 'क्र.स',
// 		dataField: 'id',
// 		cellsalign: 'center',
// 		align: 'center',
// 		width: '50',
// 	},
// 	{
// 		text: 'निर्माणका चरण',
// 		dataField: 'NirmanCharan',
// 		cellsalign: 'center',
// 		align: 'center',
// 		// width: '400px',
// 	},
// 	{
// 		text: 'संख्या',
// 		dataField: 'NirmanCharanNumber',
// 		cellsalign: 'center',
// 		align: 'center',
// 		width: '100',
// 	},
// ];

export const ReportStats = ({ data, fiscalYears, updateFiscalYear, loading }) => {
	const { fy, fyOption } = useFiscalYearAll(fiscalYears, 'yearCode');
	// const history = useHistory();
	// const table = useRef();

	// const onRowClick = (e, fiscalYear) => {
	// 	const id = e.args.row.boundindex;
	// 	history.push(`/user/report-list/${fiscalYear}/${id}`);
	// };

	// const handlePrint = fiscalYear => {
	// 	const gridContent = table.current.exportdata('html');
	// 	const newWindow = window.open('', '', 'width=800, height=500');
	// 	const document = newWindow.document.open();
	// 	const pageContent =
	// 		'<!DOCTYPE html>\n' +
	// 		'<html>\n' +
	// 		'<head>\n' +
	// 		'<meta charset="utf-8" />\n' +
	// 		'<title>jQWidgets Grid</title>\n' +
	// 		'</head>\n' +
	// 		'<body>\n' +
	// 		'<h3>' +
	// 		Reportlang.report_data.infobarName +
	// 		' रिपोर्ट</h3>' +
	// 		'<p>' +
	// 		Reportlang.report_data.fiscalYear +
	// 		' ' +
	// 		fiscalYear +
	// 		'</p>' +
	// 		gridContent +
	// 		'\n</body>\n</html>';
	// 	document.write(pageContent);
	// 	document.close();
	// 	newWindow.print();
	// };

	return (
		<>
			<Loading loading={loading} />
			<div className="app-table-task">
				{data ? (
					<div>
						<Formik
							initialValues={{ fiscalYear: fy }}
							enableReinitialize
							render={({ values, setFieldValue }) => (
								<>
									<div className="flex-item-space-between">
										<Header className="app-table-header">
											<Icon name="chart line" />
											<Header.Content>{Reportlang.report_data.statistics}</Header.Content>
										</Header>
										<Form.Field inline>
											{Reportlang.report_data.fiscalYear}
											<Select
												style={{ zIndex: '1000' }}
												placeholder="Select Fiscal Year"
												options={fyOption}
												name="fiscalYear"
												onChange={(e, { value }) => {
													setFieldValue('fiscalYear', value);
													updateFiscalYear(value);
												}}
												value={values.fiscalYear}
											/>
										</Form.Field>
										{/* <Button
											type="button"
											className="primary-btn"
											icon="print"
											content="Print"
											onClick={() =>
												handlePrint(
													translateEngToNep(fyOption.find(el => (values.fiscalYear ? el.value === values.fiscalYear : el.value === fy)).text)
												)
											}
										/> */}
									</div>

									<br />
									{/* <RootGrid
										Grid={table}
										onRowClick={(e) => onRowClick(e, values.fiscalYear)}
										pageable={false}
										localdata={data}
										datafield={datafield}
										columns={columns}
										groupable={false}
									/> */}
									<div className="flex-space-between-center">
										{data.map((el, index) => (
											<Statistic key={index} size="mini">
												<Statistic.Label>{el.label}</Statistic.Label>
												<Statistic.Value>{el.count}</Statistic.Value>
											</Statistic>
										))}
									</div> 
									{/* <Card.Group>
										{data.map((el, index) => (
											<Card color="blue">
												<CardContent>
													<Card.Header>{el.label}</Card.Header>
													<CardDescription><Icon name="check circle" />{el.count}</CardDescription>
												</CardContent>
											</Card>
										))}
									</Card.Group> */}
								</>
							)}
						></Formik>
						{/* <Divider /> */}
					</div>
				) : (
					<span>
						<CompactInfoMessage content={taskList.headings.noData} />
					</span>
				)}
			</div>
		</>
	);
};
