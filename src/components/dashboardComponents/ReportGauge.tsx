import * as React from 'react';
import JqxBarGauge, { IBarGaugeProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxbargauge';
import { translateEngToNep } from '../../utils/langUtils';

interface PropInterface {
	data: object[];
}

class ReportGauge extends React.PureComponent<PropInterface, IBarGaugeProps> {
	constructor(props: PropInterface) {
		super(props);
		this.state = {
			labels: {},
			tooltip: {},
			values: [],
		};
	}

	componentDidUpdate(prevProps: PropInterface) {
		if (prevProps.data !== this.props.data) {
			this.setState({
				labels: {
					font: {
						size: 12,
					},
					formatFunction: (value: number): string => {
						return translateEngToNep(value) + '';
					},
					indent: 10,
				},
				tooltip: {
					classname: 'myTooltip',
					formatFunction: (value: string) => {
						const realVal = parseInt(value, 10);
						const player = this.state.values!.indexOf(realVal);
						//@ts-ignore
						const currentRow = this.props.data[player];
						//@ts-ignore
						return currentRow.NirmanCharan;
					},
					visible: true,
				},
				values: this.props.data.map(el => {
					//@ts-ignore
					return el.NirmanCharanNumber;
				}),
			});
		}
	}

	public render() {
		return (
			<JqxBarGauge
				// @ts-ignore
				width={400}
				height={400}
				max={15}
				labels={this.state.labels}
				values={this.state.values}
				// baseValue={33}
				// startAngle={180}
				// endAngle={-65}
				title={this.state.title}
				colorScheme={'scheme02'}
				tooltip={this.state.tooltip}
			/>
		);
	}
}
export default ReportGauge;
