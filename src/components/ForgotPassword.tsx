import React, { useState, useEffect } from 'react';
import { Grid, Segment, Form, Button, Loader, Modal } from 'semantic-ui-react';
import ErrorDisplay from './shared/ErrorDisplay';
import { FormikValues } from 'formik';
import { withFormik, FormikProps } from 'formik';
import * as Yup from 'yup';
import EBPS from '../assets/images/EBPS.jpg';
import { postForgotPassword } from '../store/actions/loginForm';
import { showToast, isEmpty } from '../utils/functionUtils';

import { connect } from 'react-redux';
import { store } from '..';
import { UIState } from '../interfaces/ReduxInterfaces';
import { forgotPassword } from '../utils/data/genericData';
import { useCleanup } from '../hooks/useCleanup';
import Helmet from 'react-helmet';

interface ForgotFormValues {
	email: string;
}

interface OtherProps {
	title?: string;
	ui: UIState;
	success: {};
}

interface ForgotFormProps {
	initialEmail?: string;
	ui: UIState;
	success: {};
}

const ForgotPassword = (props: OtherProps & FormikProps<FormikValues>) => {
	const [isModalOpen, setModalOpen] = useState(false);

	useEffect(() => {
		if (!isEmpty(props.success)) {
			showToast('Email Sent');
		}
	}, [props.success]);

	useCleanup();

	const { values, errors, touched, handleChange, handleBlur, handleSubmit, ui } = props;

	return (
		<React.Fragment>
			<Helmet title="EBPS - Forgot Password" />
			<div
				style={{
					backgroundImage: `url(${EBPS})`,
					backgroundSize: 'cover',
					transform: 'translateY(14px)',
				}}
				className="forgot-password"
			>

				<Grid textAlign="center" style={{ height: '100vh' }} verticalAlign="middle">
					<Grid.Column style={{ maxWidth: 450 }} className="login-background">
						<Form size="large" className="adminLogin-form">
							<h3>Forgot your password?</h3>
							<Segment>
								{ui.errors && <ErrorDisplay message={ui.errors.message} />}
								<h4>Let us help you</h4>
								<Form.Field>
									<Form.Input
										autoFocus
										icon="mail"
										iconPosition="left"
										type="text"
										name="email"
										onChange={handleChange}
										onBlur={handleBlur}
										value={values.email}
										error={errors.email}
										placeholder="Email"
									/>
								</Form.Field>
								<Button onClick={() => setModalOpen(true)} fluid size="large" disabled={ui.loading || !!(errors.email && touched.email)}>
									Submit
									<Loader active={ui.loading} size="small" inline />
								</Button>
								<Modal open={isModalOpen}>
									<Modal.Content>
										<h2>{forgotPassword.title}</h2>
									</Modal.Content>
									<Modal.Actions>
										<Button
											color="red"
											onClick={() => setModalOpen(false)}
											negative
											labelPosition="left"
											icon="remove"
											content={forgotPassword.no}
											title="Decline"
										/>
										<Button
											onClick={() => {
												handleSubmit();
												setModalOpen(false);
											}}
											color="green"
											positive
											labelPosition="left"
											icon="checkmark"
											content={forgotPassword.yes}
											title="Confirm"
										/>
									</Modal.Actions>
								</Modal>
								<br />
							</Segment>
						</Form>
					</Grid.Column>
				</Grid>
			</div>
		</React.Fragment>
	);
};

const Forgot = withFormik<ForgotFormProps, ForgotFormValues>({
	mapPropsToValues: props => ({
		email: props.initialEmail || '',
	}),

	validationSchema: Yup.object().shape({
		email: Yup.string()
			.email('Email not valid')
			.required('Email is required'),
	}),

	handleSubmit({ email }: ForgotFormValues, { props, setSubmitting, setErrors }) {
		setSubmitting(true);
		console.log(props);
		try {
			// @ts-ignore
			store.dispatch(postForgotPassword({ email }));
			setSubmitting(false);
		} catch (err) {
			setSubmitting(false);
			window.scrollTo(0, 0);
			showToast('Something went wrong !!');
		}
	},
})(ForgotPassword);

//@ts-ignore
const mapDispatchToProps = { postForgotPassword };
//@ts-ignore
const mapStateToProps = state => {
	return {
		getError: state.root.login.loginError,
		getSubmit: state.root.login.submitBtn,
		ui: state.root.ui,
		errors: state.root.ui.errors,
		success: state.root.formData.success,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Forgot);
