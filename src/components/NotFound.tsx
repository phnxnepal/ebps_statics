import React from 'react';

import HeadMenu from '../components/publicComponents/PublicHeader';
import Footer from '../components/loggedInComponents/Footer';
import { Grid, Icon } from 'semantic-ui-react';

export default function NotFound() {
  return (
    <React.Fragment>
      <HeadMenu />

      <Grid centered style={{ paddingTop: '100px' }}>
        <Grid.Row>
          <Icon name="warning sign" size="huge"/>
        </Grid.Row>
        <Grid.Row><h2>Page Not Found.</h2></Grid.Row>
      </Grid>

      <Footer />
    </React.Fragment>
  );
}
