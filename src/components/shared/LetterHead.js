import React from 'react';
import NepalSarkarLogo from '../../assets/images/Nepal_sarkar_logo.webp';
import IllamLogo from '../../assets/images/illam_logo.png';
import kagesworiNagarpalika from '../../assets/images/kagesworiNagarpalika.png';
// import VisitNepalLogo from '../../assets/images/visit-nepal-2020.png';
import { illamlang, birtamodLang } from '../../utils/data/letterHeadLang';
import { isIllam, isKankai, isBirtamod, isSundarHaraicha, isKageyshowri } from '../../utils/clientUtils';
import { getDocUrl } from '../../utils/config';
import { FaxPhoneSection } from './formComponents/FaxPhoneSection';
import { FlexSingleRight } from '../uiComponents/FlexDivs';

export const LetterHead = ({ userInfo, needsProvince = false }) => {
	return (
		<>
			{isIllam ? (
				<div className="letterHead">
					<h1 className="heading">
						<div className="nepal-sarkar-logo">
							<img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />
						</div>
						<div className="head1">{illamlang.head}</div>
						<div className="headname">{userInfo.organization.name}</div>
						<div className="headorg">{userInfo.organization.officeName}</div>
						{/* <div className="headprovince">{userInfo.organization.province}</div> */}
						<div className="illam-logo">
							<img src={IllamLogo} alt="illam-logo" />
						</div>
					</h1>
					<address className="headAdd">
						<h3>{userInfo.organization.address}</h3>
						<h4>
							{userInfo.organization.province}, {illamlang.country}
						</h4>
					</address>
				</div>
			) : isKankai ? (
				<div className="letterHead">
					<h1 className="heading">
						<div className="nepal-sarkar-logo">
							<img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />
						</div>
						<div className="headname">{userInfo.organization.name}</div>
						<div className="headorg">{userInfo.organization.officeName}</div>
						{/* <div className="headprovince">{userInfo.organization.province}</div> */}
						{/* <div className="illam-logo">
							<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
						</div> */}
					</h1>
					<address className="headAdd headAdd-color">
						<h3>{userInfo.organization.address}</h3>
						<h4>
							{userInfo.organization.province}, {illamlang.country}
						</h4>
					</address>
				</div>
			) : isBirtamod ? (
				<div className="letterHead">
					<h1 className="heading">
						<div className="nepal-sarkar-logo">
							<img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />
						</div>
						<div className="headname">{userInfo.organization.name}</div>
						<div className="headorg">{userInfo.organization.officeName}</div>
						{/* <div className="illam-logo">
							<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
						</div> */}
					</h1>
					<address className="headAdd headAdd-color">
						<h3>{userInfo.organization.address}</h3>
						{needsProvince && (
							<h4>
								{userInfo.organization.province}, {illamlang.country}
							</h4>
						)}
					</address>
				</div>
			) : (
				<div className="letterHead">
					<h1 className="heading">
						<div className="nepal-sarkar-logo">
							<img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />
						</div>
						<div>{userInfo.organization.name}</div>
						<div>{userInfo.organization.officeName}</div>
						{/* <div className="illam-logo">
							<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
						</div> */}
					</h1>
					<address>
						<h3>{userInfo.organization.address}</h3>
						<h4>
							{userInfo.organization.province}, {illamlang.country}
						</h4>
					</address>
				</div>
			)}
		</>
	);
};

/**
 *
 * @param {object} compact This has a bug. the meaning is reversed @todo fix later
 */
export const LetterHeadFlex = ({ userInfo, compact = false, needsProvince = false, needsOfficeName = true, hideLogo = false }) => {
	return (
		<div className={`flex-container ${compact ? 'letterHead' : 'letterHead-compact'}`}>
			<>
				{isBirtamod ? (
					<div className="flex-item-space-between">
						<div className="flex-item-left">
							<div className="certificate-logo">{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}</div>
						</div>
						<div className="flex-item-grow-middle">
							<div className="heading">
								<span className="medium bold">{`${userInfo.organization.name}${
									needsOfficeName ? '' : birtamodLang.headingSuffix
								}`}</span>
								{needsOfficeName && <span className="large bold">{userInfo.organization.officeName}</span>}
								<span className={needsOfficeName ? 'medium bold' : 'medium bold no-color'}>{userInfo.organization.address}</span>
								{needsProvince && (
									<span className={needsOfficeName ? 'small bold' : 'small bold no-color'}>
										{userInfo.organization.province}, {illamlang.country}
									</span>
								)}
							</div>
						</div>
						<div className="flex-item-right">
							{/* <div className="certificate-logo-big">
								<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
							</div> */}
						</div>
					</div>
				) : isIllam ? (
					<div className="flex-item-space-between">
						<div className="flex-item-left">
							<div className="certificate-logo">{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}</div>
						</div>
						<div className="flex-item-grow-middle">
							<div className="heading">
								<div className="small bold">{illamlang.head}</div>
								<div className={needsOfficeName ? 'headname' : 'large bold'}>{`${userInfo.organization.name}${
									needsOfficeName ? '' : birtamodLang.headingSuffix
								}`}</div>
								{needsOfficeName && <div className={'headorg'}>{userInfo.organization.officeName}</div>}
								<span className={needsOfficeName ? 'small bold' : 'small bold no-color'}>{userInfo.organization.address}</span>
								{needsProvince && (
									<span className={needsOfficeName ? 'tiny bold' : 'tiny bold no-color'}>
										{userInfo.organization.province}, {illamlang.country}
									</span>
								)}
							</div>
						</div>
						<div className="flex-item-right">
							<div className="certificate-logo-big">
								<img src={IllamLogo} alt="illam-logo" />
							</div>
						</div>
					</div>
				) : isSundarHaraicha ? (
					<div className="flex-item-space-between">
						<div className="flex-item-left">
							<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
								{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}
							</div>
						</div>{' '}
						<div className="flex-item-grow-middle heading">
							<div className="large bold">{userInfo.organization.name}</div>
							<div className="medium bold">{userInfo.organization.officeName}</div>
							<div className="small no-color">
								{userInfo.organization.address}, {userInfo.organization.province}, {illamlang.country}
							</div>
						</div>
						<div className="flex-item-right">
							<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
								{/* <img src={IllamLogo} alt="illam-logo" /> */}
							</div>
						</div>
						{/* <div className="illam-logo">
							<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
						</div> */}
					</div>
				) : isKankai ? (
					<div className="flex-item-space-between">
						<div className="flex-item-left">
							<div className="certificate-logo">{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}</div>
						</div>
						<div className="flex-item-grow-middle heading">
							<div className="large bold">{userInfo.organization.name}</div>
							<div className="medium bold">{userInfo.organization.officeName}</div>
							<div className="small no-color">{userInfo.organization.address}</div>
							<div className="small no-color">
								{userInfo.organization.province}, {illamlang.country}
							</div>
						</div>
						<div className="flex-item-right">
							<div className="certificate-logo-big"></div>
						</div>
					</div>
				) : (
					<div className="flex-item-space-between">
						<div className="flex-item-left">
							<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
								{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}
							</div>
						</div>{' '}
						<div className="flex-item-grow-middle heading">
							<div className="large bold">{userInfo.organization.name}</div>
							<div className="medium bold">{userInfo.organization.officeName}</div>
							<div className="small no-color">
								{userInfo.organization.address}, {userInfo.organization.province}, {illamlang.country}
							</div>
						</div>
						<div className="flex-item-right">
							{!isKageyshowri ? null : <img className="certificate-logo-small" src={kagesworiNagarpalika} alt="nepal-sarkar-logo" />}
							<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
								{/* <img src={IllamLogo} alt="illam-logo" /> */}
							</div>
						</div>
						{/* <div className="illam-logo">
							<img src={VisitNepalLogo} alt="Visit Nepal 2020" />
						</div> */}
					</div>
				)}
			</>
		</div>
	);
};

export const LetterHeadPhoto = ({ compact, userInfo, photo, hideLogo = false }) => {
	return (
		<div className={`flex-container ${compact ? 'letterHead-compact' : 'letterHead'} margin-top-class`}>
			<div className="flex-item-space-between">
				<div className="flex-item-left">
					<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
						{hideLogo ? null : <img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />}
					</div>
				</div>{' '}
				<div className="flex-item-grow-middle heading">
					<div className="large bold">{userInfo.organization.name}</div>
					<div className="medium bold">{userInfo.organization.officeName}</div>
					<div className="small no-color">
						{userInfo.organization.address}, {userInfo.organization.province}, {illamlang.country}
					</div>
				</div>
				<div className="flex-item-right">
					{!isKageyshowri ? null : <img className="certificate-logo-small" src={kagesworiNagarpalika} alt="nepal-sarkar-logo" />}
					<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
						<div className="certificate-image">{photo && <img src={`${getDocUrl()}${photo}`} alt="imageFile" />}</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export const LetterHeadPhone = ({ compact, userInfo, phoneData }) => {
	return (
		<div className={`flex-container ${compact ? 'letterHead-compact' : 'letterHead'} margin-top-class`}>
			<div className="flex-item-space-between">
				<div className="flex-item-left">
					<div className={compact ? 'certificate-logo-small' : 'certificate-logo'}>
						<img src={NepalSarkarLogo} alt="nepal-sarkar-logo" />
					</div>
				</div>{' '}
				<div className="flex-item-grow-middle heading">
					<div className="large bold">{userInfo.organization.name}</div>
					<div className="medium bold">{userInfo.organization.officeName}</div>
					<div className="small no-color">
						{userInfo.organization.address}, {userInfo.organization.province}, {illamlang.country}
					</div>
				</div>
				<div className="flex-item-right">
					<FaxPhoneSection data={phoneData} />
				</div>
			</div>
		</div>
	);
};
