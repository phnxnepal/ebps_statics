import React from 'react';
import { Input, Label } from 'semantic-ui-react';
import { Field } from 'formik';

const FormikInput = ({ name, handleChange, label = '' }) => {
  return (
    <Input>
      {label && <Label>{label}</Label>}
      <Field name={name} onChange={handleChange} />
    </Input>
  );
};

export default FormikInput;
