import React from 'react';
import { Field } from 'formik';

export const EnglishField = props => {
	return <Field className="english-div-field" {...props} />;
};
