import React from 'react';
import { Field } from 'formik';
import { Label, Form } from 'semantic-ui-react';

export const NumberInput = ({ label, error, name, ...rest }) => {
	return (
		<Form.Field error={!!error}>
			{label && label}
			<Field name={name} type="number" {...rest} />
			<NumberInput.Error error={error} />
		</Form.Field>
	);
};

NumberInput.Error = ({ error }) => {
	if (error) {
		return (
			<Label pointing prompt size="large">
				{error}
			</Label>
		);
	}
	return null;
};
