import React from 'react';
import { useTranslatorIm } from '../../../hooks/useTranslator';
import { useCursorReset } from '../../../hooks/useCursorReset';
import { Field, getIn } from 'formik';
import { Label, Form } from 'semantic-ui-react';

export class FieldHelper {
	static compare(v1, v2) {
		if (!v1 && !v2) return true;
		else return v1 === v2;
	}

	static areFieldPropsEqual(prevProps, nextProps) {
		return FieldHelper.compare(prevProps.value, nextProps.value) && FieldHelper.compare(prevProps.error, nextProps.error);
	}
}

const NepaliInputField = React.memo(({ label, error, setFieldValue, name, value, hasSlash = false }) => {
	const [className, valueState, handleChange] = useTranslatorIm(value, setFieldValue, error, name, hasSlash);
	const [inputRef, setCursorPosition] = useCursorReset(valueState);

	return (
		<Form.Field error={!!error}>
			<span>{label}</span>
			<input
				ref={inputRef}
				name={name}
				type="text"
				onChange={(e) => {
					setCursorPosition(e.target.selectionStart);
					handleChange(e);
				}}
				className={`bpf-inputType ${className}`}
				value={valueState || value}
			/>

			{error && (
				<Label pointing prompt size="large">
					{error}
				</Label>
			)}
		</Form.Field>
	);
}, FieldHelper.areFieldPropsEqual);

export const NepaliInput = React.memo(({ name, label }) => {
	return (
		<Field>
			{({ form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);
				return <NepaliInputField label={label} error={error} value={value} name={name} setFieldValue={form.setFieldValue} />;
			}}
		</Field>
	);
});

export const NepaliInputSlash = React.memo(({ name, label }) => {
	return (
		<Field>
			{({ form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);
				return <NepaliInputField label={label} error={error} value={value} name={name} setFieldValue={form.setFieldValue} hasSlash={true} />;
			}}
		</Field>
	);
});
