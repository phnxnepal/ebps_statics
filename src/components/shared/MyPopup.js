import React, { Component } from 'react';
import { Popup } from 'semantic-ui-react';

class MyPopup extends Component {
  render() {
    let { content, children, ...rest } = this.props;
    return (
      <Popup
        inverted
        position="bottom left"
        content={this.props.content}
        trigger={this.props.children}
        {...rest}
      />
    );
  }
}

export default MyPopup;
