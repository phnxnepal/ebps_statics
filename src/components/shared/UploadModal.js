// import React, { Component } from 'react';
// import { Button, Form, Input, Divider, Label, List } from 'semantic-ui-react';
// import { connect } from 'react-redux';
// import { postFile, getUploadedFiles } from '../../../store/actions/fileActions';
// import { getPermitAndUserData } from '../../../store/actions/formActions';
// import api from '../../../utils/api';
// import { getBackUrl } from '../../../utils/config';
// import FallbackComponent from '../../shared/FallbackComponent';
// import { Formik } from 'formik';
// import { hasMultipleFiles, showToast } from '../../../utils/functionUtils';
// import ErrorDisplay from '../../shared/ErrorDisplay';

// import * as Yup from 'yup';
// import { validateFile } from '../../../utils/validationUtils';

// class UploadModal extends Component {

//     componentDidMount() {
//         // this.props.getPermitAndUserData();
//         this.props.getUploadedFiles();
//     }

//     render() {

//             const { fileCategories } = this.props;

//             const schema = Yup.object().shape({
//                 uploadFile: validateFile
//             });

//             return (
//                 <Form loading={this.props.loading}>
//                     <h3>File Upload</h3>
//                     {fileCategories.map(category => (
//                         <Formik
//                             key={category.id}
//                             validationSchema={schema}
//                             onSubmit={async (values, actions) => {
//                                 actions.setSubmitting(true);
//                                 // console.log('values', values);
//                                 const data = new FormData();

//                                 // const selectedFile = this.state[
//                                 //     `selectedFile_${category.id}`
//                                 // ];

//                                 const selectedFile = values.uploadFile;
//                                 if (selectedFile) {
//                                     // for (
//                                     //     var x = 0;
//                                     //     x < selectedFile.length;
//                                     //     x++
//                                     // ) {
//                                     //     data.append('file', selectedFile[x]);
//                                     // }

//                                     selectedFile.forEach(file =>
//                                         data.append('file', file)
//                                     );

//                                     data.append('fileType', category.id);
//                                     data.append(
//                                         'applicationNo',
//                                         this.props.permitData.applicantNo
//                                     );

//                                     try {
//                                         await this.props.postFile(
//                                             `${getBackUrl()}${api.fileUpload}${
//                                                 this.props.permitData
//                                                     .applicantNo
//                                             }`,
//                                             data
//                                         );

//                                         if (
//                                             this.props.success &&
//                                             this.props.success.success
//                                         ) {
//                                             showToast(
//                                                 this.props.success.data
//                                                     .message || 'File uploaded'
//                                             );
//                                             this.setState(() => ({
//                                                 needToGet: true
//                                             }));
//                                         }
//                                         actions.setSubmitting(false);
//                                     } catch (err) {
//                                         actions.setSubmitting(false);
//                                         console.log('File upload failed', err);
//                                     }
//                                 } else {
//                                     actions.setError(
//                                         'Please select a file before uploading.'
//                                     );
//                                 }
//                                 console.log('data to send', data, selectedFile);
//                             }}
//                         >
//                             {({
//                                 handleSubmit,
//                                 errors,
//                                 setFieldValue
//                                 // values
//                             }) => {
//                                 // console.log('errors', errors, values);
//                                 return (
//                                     <>
//                                         <div>
//                                             {this.props.errors && (
//                                                 <ErrorDisplay
//                                                     message={
//                                                         this.props.errors
//                                                             .message
//                                                     }
//                                                 />
//                                             )}
//                                             <Divider horizontal>
//                                                 {category.name}
//                                             </Divider>
//                                             <Form.Field
//                                                 error={errors.uploadFile}
//                                             >
//                                                 <Input
//                                                     type="file"
//                                                     name="uploadFile"
//                                                     id={category.id}
//                                                     multiple={hasMultipleFiles(
//                                                         category.isMultiple
//                                                     )}
//                                                     onChange={e => {
//                                                         // this.onChangeHandler(e);

//                                                         // const files =
//                                                         //     e.target.files;

//                                                         // console.log(
//                                                         //     'files ===   ',
//                                                         //     files,
//                                                         //     Array.from(files)
//                                                         // );

//                                                         // for (var x = 0; x < files.length; x++) {
//                                                         //   console.log('file list la ', files[x].name);
//                                                         //   setFieldValue(`uploadFile.${x}.name`, files[x].name);
//                                                         // //   setFieldValue(`uploadFile.${x}.type`, files[x].type);
//                                                         // }
//                                                         setFieldValue(
//                                                             'uploadFile',
//                                                             Array.from(
//                                                                 e.target.files
//                                                             )
//                                                         );
//                                                     }}
//                                                 />
//                                                 {errors.uploadFile && (
//                                                     <Label
//                                                         pointing
//                                                         prompt
//                                                         size="large"
//                                                     >
//                                                         {errors.uploadFile}
//                                                     </Label>
//                                                 )}
//                                             </Form.Field>
//                                             <List relaxed>
//                                                 <List.Header>
//                                                     Existing Files
//                                                 </List.Header>
//                                                 {files &&
//                                                 files.length > 0 &&
//                                                 files.some(
//                                                     file =>
//                                                         file.fileType.id ===
//                                                         category.id
//                                                 ) ? (
//                                                     files
//                                                         .filter(
//                                                             file =>
//                                                                 file.fileType
//                                                                     .id ===
//                                                                 category.id
//                                                         )
//                                                         .map(el => (
//                                                             // <Image
//                                                             //   size="medium"
//                                                             //   bordered
//                                                             //   alt="document"
//                                                             //   src={`${getDocUrl()}${el.fileUrl}`}
//                                                             // />
//                                                             <List.Item
//                                                                 key={el.fileUrl}
//                                                                 content={
//                                                                     el.fileUrl
//                                                                 }
//                                                                 icon="file alternate outline"
//                                                             />
//                                                         ))
//                                                 ) : (
//                                                     <List.Item>
//                                                         No files uploaded.
//                                                     </List.Item>
//                                                 )}
//                                             </List>
//                                             <Button
//                                                 primary
//                                                 type="submit"
//                                                 id={category.id}
//                                                 onClick={handleSubmit}
//                                             >
//                                                 Upload
//                                             </Button>
//                                         </div>
//                                     </>
//                                     // {
//                                     /* <p>
//               <Input
//               type="file"
//               name="file"
//               multiple
//               onChange={this.onChangeHandler}
//               />
//               </p>
//               {this.props.files && this.props.files.length > 0 && (
//                 <Image
//                 size="medium"
//                 bordered
//                 alt="document"
//                 src={`${docUrl}/7677/7677000001/3141779828_1844587828964957_5331080997419089920_n.jpg	`}
//                 />
//                 )}
//                 <Button primary type="submit" onClick={this.onUpload}>
//                 Upload
//               </Button> */
//                                     // }
//                                 );
//                             }}
//                         </Formik>
//                     ))}
//                 </Form>
//             );
//         } else {
//             return (
//                 <FallbackComponent
//                     errors={this.props.errors}
//                     loading={this.props.loading}
//                 />
//             );
//         }
//     }
// }

// const mapDispatchToProps = {
//     postFile,
//     getPermitAndUserData,
//     getUploadedFiles
// };

// const mapStateToProps = state => ({
//     permitData: state.root.formData.permitData,
//     userData: state.root.formData.userData,
//     files: state.root.formData.files,
//     fileCategories: state.root.formData.fileCategories,
//     success: state.root.formData.success,
//     loading: state.root.ui.loading,
//     errors: state.root.ui.errors
// });

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(UploadModal);
