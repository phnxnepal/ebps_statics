import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, TextArea, Label } from 'semantic-ui-react';
import { translate, getInputType } from '../../utils/langUtils';
import { isStringEmpty } from '../../utils/stringUtils';

const EbpsTextareaField = ({ labelName, name, value, setFieldValue, onChange, error, ...rest }) => {
	const [valueState, setValueState] = useState(value);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleChange = e => {
		// As some bug is preventing the first letter from being set in value.
		const value1 = translate(e, getInputType());
		// const value1 = e.target.value
		setValueState(value1);
		setFieldValue(name, value1);
	};

	return (
		<Form.Field error={!!error}>
			<span>{labelName}</span>
			<TextArea
				name={name}
				value={valueState}
				className={`bpf-inputType ${className}`}
				onChange={setFieldValue ? handleChange : onChange}
				{...rest}
				style={{ minHeight: 150 }}
			/>
			{error && (
				<Label pointing prompt size="large">
					{error}
				</Label>
			)}
		</Form.Field>
	);
};

EbpsTextareaField.propTypes = {
	label: PropTypes.string,
	error: PropTypes.string,
	setFieldValue: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
};

export default EbpsTextareaField;
