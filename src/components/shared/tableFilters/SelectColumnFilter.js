import React from 'react';

export const SelectColumnFilter = ({ column: { filterValue, setFilter, preFilteredRows, defaultOption, id } }) => {
	// Calculate the options for filtering
	// using the preFilteredRows
	const options = React.useMemo(() => {
		const options = new Set();
		preFilteredRows.forEach((row) => {
			options.add(row.values[id]);
		});
		return [...options.values()];
	}, [id, preFilteredRows]);

	// Render a multi-select box
	return (
		<select
			className="ui selection dropdown filter-column"
			value={filterValue}
			onChange={(e) => {
				setFilter(e.target.value || undefined);
			}}
		>
			<option value="">{defaultOption}</option>
			{options.map((option, i) => (
				<option className='item' key={i} value={option}>
					{option}
				</option>
			))}
		</select>
	);
};
