import React, { useState, useEffect } from 'react';
import { approveReject } from '../../utils/data/genericData';
import { SectionHeader } from '../uiComponents/Headers';
import { UserType } from '../../utils/userTypeUtils';
import { getLocalStorage } from '../../utils/secureLS';
import { LSKey } from '../../utils/enums/localStorageKeys';
import { FormUrlFull } from '../../utils/enums/url';

export const RayaForms = Object.freeze([
	FormUrlFull.NOTE_ORDER_PLINTH_LEVEL,
	FormUrlFull.PLINTH_LEVEL_TECH_APP,
	FormUrlFull.SUPERSTRUCTURE_NOTE_ORDER,
	FormUrlFull.SUPERSTRUCTURE_KO_NIRMAN_KARYA,
	FormUrlFull.CERTIFICATE_INSTRUCTION_VIEW,
	FormUrlFull.SANSODHAN_SUPERSTRUCTURE_IJAJAT,
])

type Comments = {
	comment: string;
	remark: string;
	commentBy: string;
	commentDate: string;
	userType: UserType;
	commentByDesignation: string;
};

type Props = {
	comments: Comments[];
	setApprovalRef: () => {};
};

export const ApprovalComments = React.forwardRef(({ comments, setApprovalRef }: Props, ref) => {
	const [validComments, setValidComments] = useState<Comments[]>([]);

	useEffect(() => {
		const userTypeMaster = JSON.parse(getLocalStorage(LSKey.USER_TYPE_ALL));
		setValidComments(
			comments
				? comments
						.filter((row) => row.remark === 'APPROVED_COMMENT')
						.map((row) => ({
							...row,
							commentByDesignation: userTypeMaster
								? userTypeMaster.find((uT: { userType: UserType }) => uT.userType === row.userType).designation
								: '',
						}))
						.sort((a, b) => (a.commentDate < b.commentDate ? -1 : 1))
				: []
		);
	}, [comments]);

	if (validComments && validComments.length > 0) {
		return (
			<div ref={setApprovalRef}>
				<SectionHeader>
					<h4 className="underline left-align">{approveReject.approve.rayaHaru}</h4>
				</SectionHeader>
				{validComments.map((comment, idx) => (
					<div className="raya-section" key={idx}>
						<span>
							{comment.commentBy} ({comment.commentByDesignation}) को राय: {comment.comment}
						</span>
					</div>
				))}
			</div>
		);
	} else return null;
});
