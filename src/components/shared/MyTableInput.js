import React, { Component, useState } from 'react';
import { isStringEmpty } from '../../utils/stringUtils';
import {
  translate,
  getInputType,
  translateNepToEng
} from '../../utils/langUtils';
import { Form, Label } from 'semantic-ui-react';
import { getIn } from 'formik';
import { calculateArea } from '../../utils/mathUtils';

export class MyTableInput extends Component {
  render() {
    let {
      error,
      calc,
      setFieldValue,
      onChange,
      name,
      value,
      values,
      widthname,
      areaname,
      lengthname,
      ...rest
    } = this.props;

    const className = !isStringEmpty(value)
      ? error
        ? 'error'
        : 'success'
      : 'text-input';

    const handleChange = e => {
      //   As some bug is preventing the first letter from being set in value.

      const value1 = translate(e, getInputType());
      setFieldValue(name, value1);

      if (calc) {
        setFieldValue(
          areaname,
          translate(
            calculateArea(
              translateNepToEng(value1),
              translateNepToEng(getIn(values, widthname || lengthname))
            ),
            getInputType()
          )
        );
      }
    };
    return (
      <Form.Field error={error}>
        <input
          {...rest}
          name={name}
          onChange={handleChange}
          value={value}
          className={className}
        />
        {error && (
          <Label pointing prompt size="large">
            {error}
          </Label>
        )}
      </Form.Field>
    );
  }
}

export default MyTableInput;
