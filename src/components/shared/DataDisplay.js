import React from 'react'
import { Label } from 'semantic-ui-react';

export const DataDisplay = ({ value }) => {
    return (
        <Label basic>
           {value} 
        </Label>
    )
}
