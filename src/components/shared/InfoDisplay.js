import React from 'react';
import { Message } from 'semantic-ui-react';

const InfoDisplay = ({ message }) => {
  return (
    <div>
      <Message
        positive
        compact
        icon="check square"
        header="Form Approved"
        content={message}
      />
      <br />
    </div>
  );
};

export default InfoDisplay;
