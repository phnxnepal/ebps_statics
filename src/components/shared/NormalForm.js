import React, { Component, } from 'react';
import { Form } from 'semantic-ui-react';

class NormalForm extends Component {
  render() {
    return (
      <Form.Field
        style={{ lineHeight: '30px', display: 'inline-block' }}
        required
        inline
      >
        <span> {this.props.label}</span>
        <input
          type="text"
          id={this.props.id}
          // style={{
          //   width: '400px',
          //   height: '10px',
          //   border: 'none',
          //   marginLeft: '10px',
          //   borderRadius: '0px',
          //   borderBottom: '2px dotted black'
          // }}
        />
      </Form.Field>
    );
  }
}

export default NormalForm;
