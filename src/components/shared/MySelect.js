import React, { Component } from 'react';
import Select from 'react-select';

class MySelect extends Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange(this.props.id, value);
  };
  render() {
    return (
      <Select
        id={this.props.id}
        options={this.props.options}
        // isMulti
        onChange={this.handleChange}
        // onBlur={this.handleBlur}
        value={this.props.value}
        // maxMenuHeight={130}

        // defaultMenuIsOpen={true}
      />
    );
  }
}

export default MySelect;
