import React from 'react';
import { toast, Slide } from 'react-toastify';
import { Icon } from 'semantic-ui-react';
// import { Grid } from 'semantic-ui-react';

export const showToast = message => {
	toast.success(
		<div>
			{/* <Icon name="check" /> */}
			{message}
		</div>,
		{ transition: Slide, pauseOnFocusLoss: false }
	);
};
