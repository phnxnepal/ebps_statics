import React from 'react';
import { floorData } from '../../../utils/data/genericData';

export const BlockFloorComponents = ({ floorArray, formattedFloors, children }) => {
	if (floorArray.hasBlocks) {
		return (
			<>
				{Object.entries(formattedFloors).map(([block, floorObj]) => (
					<div key={block}>
						{floorData.block} {block}
						<>{children(floorObj, block)}</>
					</div>
				))}
			</>
		);
	}
	return <div>{children(formattedFloors)}</div>;
};

export const BlockFloorComponentsV2 = ({ floorArray, formattedFloors, children }) => {
	if (floorArray.hasBlocks) {
		return (
			<>
				{Object.entries(formattedFloors).map(([block, floorObj]) => (
					<>{children(floorObj, block)}</>
				))}
			</>
		);
	}
	return <>{children(formattedFloors)}</>;
};

export const BlockComponents = ({ blockLabel = floorData.block, floorArray, blocks, children }) => {
	if (floorArray.hasBlocks) {
		return (
			<>
				{blocks.map((block) => (
					<div key={block}>
						{blockLabel} {block} <>{children(block)}</>
					</div>
				))}
			</>
		);
	} else {
		return <>{children(null)}</>;
	}
};

/**
 *
 * @param {floorArray} param0
 * @param {blocks} param1
 * @param {children} param2
 *
 * Unformatted version
 */
export const BlockComponentsV2 = ({ floorArray, blocks, children }) => {
	if (floorArray.hasBlocks) {
		return <>{blocks.map((block) => children(block))}</>;
	} else {
		return <>{children(null)}</>;
	}
};
