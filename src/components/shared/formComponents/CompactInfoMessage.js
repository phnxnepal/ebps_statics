import React from 'react'
import { Message } from 'semantic-ui-react'

export const CompactInfoMessage = ({ content }) => {
    return (
       <Message floating info compact icon="warning circle" className="compact-info-message" content={content} />
    )
}
