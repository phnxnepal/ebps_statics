import React from 'react';
import { Field, getIn } from 'formik';

export const RadioInput = ({ name, option, space = false, label = '' }) => {
	return (
		<Field name={name} key={`${name}-${option}`}>
			{({ field, form }) => {
				return (
					<div className={`ui radio checkbox ${space && 'prabidhik'}`}>
						<input
							type="radio"
							name={name}
							value={option}
							defaultChecked={getIn(form.values, name) === option}
							onChange={form.handleChange}
						/>
						<label>{label ? label : option}</label>
					</div>
				);
			}}
		</Field>
	);
};
