import React, { useCallback } from 'react';
import { Formik } from 'formik';

/**
 * Formik with prop getters
 */
export const CustomFormik = ({ children, ...rest }) => {
	const getFieldProps = useCallback((name, values, errors) => {
		return {
			name,
			value: values[name],
			error: errors[name],
		};
	}, []);
	const getSetFieldProps = useCallback((name, values, errors, setFieldValue) => {
		return {
			name,
			value: values[name],
			error: errors[name],
			setFieldValue,
		};
	}, []);
	return (
		<div>
			<Formik {...rest}>
				{(props) => {
					return children({
						...props,
						getSetFieldProps: (name) => getSetFieldProps(name, props.values, props.errors, props.setFieldValue),
						getProps: (name) => getFieldProps(name, props.values, props.errors),
					});
				}}
			</Formik>
		</div>
	);
};
