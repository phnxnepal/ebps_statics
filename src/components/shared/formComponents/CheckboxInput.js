import React from 'react';
import { getIn, Field } from 'formik';
import { Checkbox, Form } from 'semantic-ui-react';

export const CheckboxInput = ({ options, name, inline = false }) => {
	return (
		<Field>
			{({ _, form }) => {
				const fieldValue = getIn(form.values, name);
				return (
					<Form.Group grouped={!inline} inline={inline}>
						{options.map((option) => (
							<Form.Field key={option}>
								<Checkbox
									checkbox
									label={option}
									value={option}
									name={name}
									defaultChecked={fieldValue && Array.isArray(fieldValue) && fieldValue.includes(option)}
									onChange={(e) => {
										const value = option;
										if (Array.isArray(fieldValue)) {
											if (fieldValue.includes(value)) {
												form.setFieldValue(
													name,
													fieldValue.filter((el) => el !== value)
												);
											} else {
												form.setFieldValue(name, fieldValue.concat(value));
											}
										} else {
											form.setFieldValue(name, [value]);
										}
									}}
								/>
							</Form.Field>
						))}
					</Form.Group>
				);
			}}
		</Field>
	);
};
