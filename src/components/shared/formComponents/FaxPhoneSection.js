import React from 'react';
import { faxPhoneData } from '../../../utils/data/faxPhoneLang';

export const FaxPhoneSection = ({ data }) => {
	return (
		<div style={{ textAlign: 'left', fontSize: '0.9em' }}>
			<div>
				{faxPhoneData.fax} {data.fax}
			</div>
			<div>
				{faxPhoneData.phone} {data.phone1}
			</div>
			<div>
				{faxPhoneData.phone} {data.phone2}
			</div>
		</div>
	);
};
