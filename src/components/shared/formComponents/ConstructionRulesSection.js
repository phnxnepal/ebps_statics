import React from 'react';
import { constructionRulesData } from '../../../utils/data/commonForm/constructionRules';
import { SectionHeader } from '../../uiComponents/Headers';

const reqData = constructionRulesData;
export const ConstructionRulesSection = () => {
	return (
		<div>
			<SectionHeader>
				<h3 className="underline">{reqData.heading}</h3>
			</SectionHeader>
			<br />
			<div>{reqData.dataline1}</div>
			<div>{reqData.dataline2}</div>
			<div>{reqData.dataline3}</div>
			<div>{reqData.dataline4}</div>
			<div>{reqData.dataline5}</div>
			<div>{reqData.dataline6}</div>
			<div>{reqData.dataline7}</div>
			<div>{reqData.dataline8}</div>
			<div>{reqData.dataline9}</div>
		</div>
	);
};
