import React, { useState } from 'react';
import { Dropdown } from 'semantic-ui-react';
import { translate, getInputType } from '../../../utils/langUtils';

type TValue = boolean | number | string | (boolean | number | string)[] | undefined;
type Props = {
	label?: string;
	options: { key?: string; value: string; text?: string; label?: string }[];
	name: string;
	handleChange: (value: TValue) => void;
	value: TValue;
	bold?: boolean;
};

export const SearchableDropdown = ({ label, options, name, handleChange, value, bold = false }: Props) => {
	const [searchQuery, setSearchQuery] = useState<string | undefined>('');

	return (
		<div>
			<span>{bold ? <b>{label}: </b> : label}</span>
			<Dropdown
				search
				searchQuery={searchQuery}
				//@ts-ignore
				onSearchChange={(e) => setSearchQuery(translate(e.target.value, getInputType()))}
				selection
				options={options}
				name={name}
				value={value}
				onChange={(e, { value }) => {
					const selectedOption = options.find((el) => el.value === value);
					const selectedValue = selectedOption ? selectedOption.text || selectedOption.label : searchQuery || '';
					setSearchQuery(selectedValue);
					handleChange(value);
				}}
				selectOnNavigation={false}
				onClick={() => setSearchQuery('')}
			/>
		</div>
	);
};
