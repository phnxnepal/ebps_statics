import React from 'react';
import PropTypes from 'prop-types';
import { Button, Label, Icon, Modal, Progress } from 'semantic-ui-react';
import { isEmpty, getUserStatusNepali } from '../../utils/functionUtils';
import { saveButton } from '../../utils/data/genericData';
import { postFileValidation, postFileWithProgress, deleteFile } from '../../store/actions/fileActions';
import { connect } from 'react-redux';
import { FormUrlFull } from '../../utils/enums/url';
import PermitPhotoUpload from '../loggedInComponents/forms/mapPermitComponents/PermitPhotoUpload';
import MultiFileUpload from '../loggedInComponents/forms/MultiFileUpload';
import { getBackUrl } from '../../utils/config';
import api from '../../utils/api';
import { updateFileState } from '../../utils/fileUtils';

export class SaveButtonValidation extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			open: false,
			isFileUploading: false,
			submitting: false,
			files: [],
		};
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			this.setState({ open: false });
		}
	}

	handleSubmitModal = () => {
		this.setState({ submitting: true });
		const uploadFiles = async () => {
			await Promise.all(
				this.state.files.map(async (fileObj) => {
					const selectedFile = fileObj.files;
					const data = new FormData();
					const applicationNo = this.props.permitData.applicantNo;
					if (selectedFile) {
						selectedFile.forEach((file) => data.append('file', file));

						data.append('fileType', fileObj.fileCatId);
						data.append('applicationNo', applicationNo);

						try {
							return this.props.postFileWithProgress(`${getBackUrl()}${api.fileUpload}${applicationNo}`, data, fileObj.fileCatName);
						} catch (err) {
							console.log('File upload failed', err);
							return [];
						}
					} else {
						console.log('Please select a file before uploading.');
						return [];
					}
				})
			);
			this.setState({ files: [], isFileUploading: false });
			this.props.handleSubmit();
		};

		if (this.state.files.length > 0) {
			this.setState({ isFileUploading: true });
			uploadFiles();
		} else {
			this.props.handleSubmit();
		}
		this.setState({ submitting: false });
	};

	handleFileChange = (fileObject) => {
		this.setState((prevState) => {
			return {
				files: updateFileState(prevState.files, fileObject),
			};
		});
	};

	handleModalOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	render() {
		const {
			hasSavePermission,
			hasDeletePermission,
			isSaveDisabled,
			handleSubmit,
			prevData,
			errors,
			formName,
			fileCategories,
			validateForm,
			formUrl = '',
			fileLoading,
		} = this.props;

		const { open, isFileUploading, submitting } = this.state;
		const isSave = isEmpty(prevData);
		const needsFile = fileCategories && fileCategories.some((category) => category.viewUrl && category.viewUrl.trim() === formUrl.trim());

		return hasSavePermission ? (
			<>
				<Modal key="save-confirmation" closeIcon open={open} onClose={this.handleClose}>
					<Modal.Header>{saveButton.uploadFileReminder}</Modal.Header>
					{isFileUploading ? (
						<Modal.Content>
							<div>{saveButton.fileUploading}</div>
							{fileLoading &&
								Object.entries(fileLoading).map(([category, progressObject]) => (
									<Progress percent={progressObject.percent} progress success indicating>
										{category}
									</Progress>
								))}
						</Modal.Content>
					) : (
						<Modal.Content scrolling>
							{formUrl === FormUrlFull.BUILD_PERMIT && <PermitPhotoUpload />}
							<MultiFileUpload
								url={formUrl}
								hasDeletePermission={hasDeletePermission}
								fileCategories={fileCategories}
								handleFileChange={this.handleFileChange}
							/>
						</Modal.Content>
					)}
					<Modal.Actions>
						<Button disabled={isFileUploading || submitting} negative onClick={this.handleClose}>
							Exit
						</Button>
						<Button
							type="submit"
							positive
							icon="checkmark"
							loading={isFileUploading || submitting}
							disabled={isFileUploading || submitting}
							labelPosition="right"
							content={isSave ? 'Save Form' : 'Update Form'}
							onClick={this.handleSubmitModal}
						/>
					</Modal.Actions>
				</Modal>
				<Button
					primary
					type="button"
					disabled={isSaveDisabled}
					onClick={() => {
						if (!isEmpty(errors) && !(errors.hasOwnProperty('uploadFile') && Object.keys(errors).length === 1)) {
							const form = document.forms[formName || 0];
							for (let i = 0; i < form.length; i++) {
								if (
									Object.keys(errors).some((errorField) => {
										/**
										 * @todo doesn't work when a field name is a substring of other.
										 */
										return form[i].name.includes(errorField);
									})
								) {
									form[i].focus();
									break;
								}
							}
						} else {
							if (needsFile) {
								if (validateForm) {
									validateForm().then((errors) => {
										if (isEmpty(errors) || (errors.hasOwnProperty('uploadFile') && Object.keys(errors).length === 1)) {
											this.handleModalOpen();
										}
									});
								} else {
									this.handleModalOpen();
								}
							} else {
								handleSubmit();
							}
						}
					}}
				>
					{isSave ? 'Save' : 'Update'}
				</Button>
				{isSaveDisabled && (
					<Label basic color="green" size="large">
						<Icon name="check" />
						{getUserStatusNepali('A')}
					</Label>
				)}
			</>
		) : null;
	}
}

const mapStateToProps = (state) => ({
	fileCategories: state.root.formData.fileCategories,
	fileLoading: state.root.ui.fileLoading,
	success: state.root.formData.success,
	permitData: state.root.formData.permitData,
});

const mapDispatchToProps = {
	postFileValidation,
	postFileWithProgress,
	deleteFile,
};

SaveButtonValidation.propTypes = {
	errors: PropTypes.object.isRequired,
	formUrl: PropTypes.string.isRequired,
	hasSavePermission: PropTypes.bool.isRequired,
	prevData: PropTypes.object.isRequired,
	hasDeletePermission: PropTypes.bool.isRequired,
	isSaveDisabled: PropTypes.bool.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	validateForm: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(SaveButtonValidation);
