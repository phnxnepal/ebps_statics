import React from 'react';
import { Message } from 'semantic-ui-react';

const ErrorDisplay = ({ message }) => {
	return (
		<div className="english-div">
			<Message negative compact icon="warning" header="Error" content={message} />
			<br />
		</div>
	);
};

export const CompactError = ({ message }) => <Message floating negative className="compact-error-message" icon="warning circle" header="Error" content={message} />;

export default ErrorDisplay;
