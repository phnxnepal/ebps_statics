import React, { useState } from 'react';
import { Modal, Button, Loader, Dimmer, Form, Segment, Label, Icon } from 'semantic-ui-react';
import { showToast } from '../../utils/functionUtils';
import ErrorDisplay from './ErrorDisplay';
import { getClassWiseNextUrl } from '../../utils/urlUtils';
import { approveReject } from '../../utils/data/genericData';
import { Formik } from 'formik';
import { EbpsTextAreaMultiLang } from './EbpsForm';

const ApprovalWithCommentModal = ({ open, onClose, successUrl, url, approvalAction, loadingModal, formUrl, history, content, title }) => {
	const [error, setError] = useState('');
	const [isNepali, setIsNepali] = useState(false);

	const handleClose = () => {
		onClose();
		setError('');
	};

	return (
		<div>
			<Modal key="approval-modal" closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{title ? title : approveReject.approve.title}</Modal.Header>
				<>
					<Dimmer active={loadingModal} inverted>
						<Loader active={loadingModal} inline="centered">
							Sending Approval...
						</Loader>
					</Dimmer>
					<>
						<Formik
							onSubmit={(values, actions) => {
								values.status = 'A';
								approvalAction(url, values)
									.then((res) => {
										// console.log('res-- res', res);
										if (res.data && res.data.error) {
											setError(res.data.error.message);
										} else {
											showToast(res.data.message || 'Application approved.');
											handleClose();
											const nextUrl = getClassWiseNextUrl(formUrl);
											history.push(nextUrl);
										}
										// console.log('error', error);
									})
									.catch((err) => {
										console.log('Message', err);
										setError('Network Error');
									});
							}}
							render={({ handleSubmit, errors, setFieldValue, handleChange, values }) => {
								return (
									<Form loading={loadingModal}>
										{errors.networkError && <ErrorDisplay message={errors.networkError} />}
										{error && <ErrorDisplay message={error} />}
										<Modal.Content scrolling>
											<Segment>
												<h3>{content ? content : approveReject.approve.content}</h3>
												<EbpsTextAreaMultiLang
													isNepali={isNepali}
													name="comment"
													onChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.comment}
													error={errors.comment}
													label={approveReject.approve.comment}
													toggler={
														<Label
															as="a"
															className="lang-selector"
															onClick={() => setIsNepali((prevState) => [prevState[0], !prevState[1]])}
														>
															<Icon name="keyboard" />
															<span>{isNepali[1] ? 'ने' : 'en'}</span>
														</Label>
													}
													cols="100"
													rows="4"
													pointing="top"
												/>
											</Segment>
										</Modal.Content>
										<Modal.Actions>
											<Segment textAlign="right">
												<Button negative onClick={handleClose}>
													{approveReject.approve.close}
												</Button>
												<Button
													positive
													icon="checkmark"
													labelPosition="right"
													content={approveReject.approve.save}
													onClick={handleSubmit}
												/>
											</Segment>
										</Modal.Actions>
									</Form>
								);
							}}
						/>

						{/* <Modal.Actions>
							<Button negative onClick={handleClose}>
								{approveReject.approve.close}
							</Button>
							<Button
								positive
								icon="checkmark"
								labelPosition="right"
								content={approveReject.approve.save}
								onClick={(e) => {
									successUrl()
										.then((res) => {
											// console.log('res-- res', res);
											if (res.data && res.data.error) {
												setError(res.data.error.message);
											} else {
												showToast(res.data.message || 'Application approved.');
												handleClose();
												const nextUrl = getClassWiseNextUrl(formUrl);
												history.push(nextUrl);
											}
											// console.log('error', error);
										})
										.catch((err) => {
											console.log('Message', err);
											setError('Network Error');
										});
								}}
							/>
						</Modal.Actions> */}
					</>
				</>
			</Modal>
		</div>
	);
};

export default ApprovalWithCommentModal;
