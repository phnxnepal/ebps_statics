import React from "react";
import { Dropdown } from "semantic-ui-react";

const UnitDropdown = ({ label, options, setFieldValue, value, name }) => {
  return (
    <>
      {label && `${label}`}{" "}
      <div className="brackets" style={{ display: "inline-flex" }}>
        {`(`}
        <Dropdown
          name={name}
          onChange={(e, { value }) => setFieldValue(name, value)}
          value={value}
          options={options}
        />
        {" ) "}
      </div>{" "}
    </>
  );
};

export default UnitDropdown;
