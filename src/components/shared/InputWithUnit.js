import React from 'react';
import { Input, Form } from 'semantic-ui-react';

const InputWithUnit = props => {
  return (
    // <Form.Input width={5}>
      <Input
        label={{ basic: true, content: props.unit }}
        labelPosition="right"
        placeholder=""
        value={props.value}
        name={props.name}
      />
    // </Form.Input>
  );
};

export default InputWithUnit;
