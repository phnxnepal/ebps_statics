import React from 'react';
import { Form, Input, Dropdown, Label } from 'semantic-ui-react';
import { lengthconvertor, areaconvertor, convertArea, convertLength, calculateArea } from '../../utils/mathUtils';
import { Field, getIn } from 'formik';
import { isStringEmpty } from '../../utils/stringUtils';
import { distanceOptions, areaUnitOptions, squareUnitOptions } from '../../utils/dataUtils';
import { DEFAULT_UNIT_LENGTH } from '../../utils/constants';

/**
 * Input Component to be used for numerical input fields with
 * selectable unit values
 *
 * @param {string} labelName Label name if needed
 * @param {string} name Name for the actual field
 * @param {functon} onChange Formik @see handleChange function
 * @param {string} value Value for the actual field
 * @param {function} setFieldValue Formik @see setFieldValue function
 * @param {array} options Unit field dropdown options
 * @param {string} nameUnit Unit Field name
 * @param {string} unitvalue Value for the unit field
 * @param {string} error Error if any
 */
const EbpsUnitLabelValue = ({
	labelName,
	name,
	onChange,
	value,
	setFieldValue,
	options,
	nameUnit,
	unitvalue,
	blurFieldName,
	blurFieldValue,
	error,
}) => {
	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				{labelName && <span>{labelName}</span>}
				<Input
					type="number"
					step="0.1"
					label={
						<Dropdown
							defaultValue={unitvalue}
							options={options}
							name={nameUnit}
							onChange={(e, { value }) => setFieldValue(nameUnit, value)}
							// value={unitvalue}
						/>
					}
					labelPosition="right"
					value={value}
					name={name}
					onChange={onChange}
					onBlur={(e) => {
						if (!error && blurFieldName) {
							setFieldValue(blurFieldName, blurFieldValue);
						}
					}}
				/>
				{error && (
					<div>
						<Label pointing prompt size="large">
							{error}
						</Label>
					</div>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export const LabelValue = ({
	labelName,
	name,
	onChange,
	value,
	setFieldValue,
	options,
	nameUnit,
	unitvalue,
	blurFieldName,
	blurFieldValue,
	error,
}) => {
	const convertvalue = value;
	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				{labelName && <span>{labelName}</span>}
				<Input
					type="number"
					step="0.1"
					label={
						<Dropdown
							value={unitvalue}
							options={options}
							name={nameUnit}
							onChange={(e, { value }) => {
								const prevunit = unitvalue;
								setFieldValue(name, lengthconvertor(convertvalue, value, prevunit));
								setFieldValue(nameUnit, value);
							}}
							// value={unitvalue}
						/>
					}
					labelPosition="right"
					value={value}
					name={name}
					onChange={onChange}
					onBlur={(e) => {
						if (!error && blurFieldName) {
							setFieldValue(blurFieldName, blurFieldValue);
						}
					}}
				/>
				{error && (
					<div>
						<Label pointing prompt size="large">
							{error}
						</Label>
					</div>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export const LabelValueIm = ({ labelName, name, options, unitName, blurFieldName, blurFieldValue }) => {
	return (
		<Form.Group widths="equal">
			{labelName && <span>{labelName}</span>}
			<Field>
				{({ field, form }) => {
					const valueError = getIn(form.errors, name);
					const unitError = getIn(form.errors, unitName);
					const fieldValue = getIn(form.values, name);
					const unitValue = getIn(form.values, unitName);
					return (
						<Form.Field error={!!unitError || !!valueError}>
							<Input
								type="number"
								step="0.1"
								label={
									<Dropdown
										value={unitValue}
										options={options}
										name={unitName}
										onChange={(e, { value }) => {
											const prevunit = unitValue;
											form.setFieldValue(name, lengthconvertor(fieldValue, value, prevunit));
											form.setFieldValue(unitName, value);
										}}
									/>
								}
								labelPosition="right"
								value={fieldValue}
								name={name}
								onChange={field.onChange}
								onBlur={(e) => {
									if (!valueError && blurFieldName) {
										form.setFieldValue(blurFieldName, blurFieldValue);
									}
								}}
							/>
							{(valueError || unitError) && <span className="tableError">{`${valueError || ''} ${unitError || ''}`}</span>}
						</Form.Field>
					);
				}}
			</Field>
		</Form.Group>
	);
};

export const NumberLabelValue = ({ labelName, name, options, unitName }) => {
	return (
		<Form.Group>
			{labelName && <span>{labelName}</span>}
			<Field>
				{({ field, form }) => {
					const valueError = getIn(form.errors, name);
					const unitError = getIn(form.errors, unitName);
					const fieldValue = getIn(form.values, name);
					const unitValue = getIn(form.values, unitName);
					return (
						<Form.Field error={!!unitError || !!valueError}>
							<Input type="number" step="0.1" className="compact-input" value={fieldValue} name={name} onChange={field.onChange} />{' '}
							<Dropdown
								value={unitValue}
								options={options}
								name={unitName}
								onChange={(e, { value }) => {
									const prevunit = unitValue;
									form.setFieldValue(name, lengthconvertor(fieldValue, value, prevunit));
									form.setFieldValue(unitName, value);
								}}
							/>
							{(valueError || unitError) && (
								<div>
									<span className="tableError">{`${valueError || ''} ${unitError || ''}`}</span>
								</div>
							)}
						</Form.Field>
					);
				}}
			</Field>
		</Form.Group>
	);
};

export const DashedUnitInput = ({ name, unitName, label, compact = false }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				// const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className} ${compact && 'compact-form-input'}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(form.values, unitName);
									form.setFieldValue(name, lengthconvertor(field.value, value, previousUnit));
									form.setFieldValue(unitName, value);
								}}
								value={getIn(form.values, unitName)}
								options={distanceOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedAreaUnitInput = ({ name, unitName, label, compact = false }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className} ${compact && 'compact-form-input'}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(values, unitName);
									form.setFieldValue(name, areaconvertor(field.value, value, previousUnit));
									form.setFieldValue(unitName, value);
								}}
								value={getIn(form.values, unitName)}
								options={areaUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const CustomDashedAreaUnitInput = ({ name, unitName, label }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = values[unitName];
									form.setFieldValue(name, areaconvertor(field.value, value, previousUnit));
									form.setFieldValue(unitName, value);
								}}
								value={form.values[unitName]}
								options={squareUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMultipleAreaUnitInput = ({ name, unitName, label, squareOptions }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = values[unitName];
									form.setFieldValue(
										name,
										areaconvertor(
											field.value,
											value.includes('SQUARE') ? value : `SQUARE ${value}`,
											previousUnit.includes('SQUARE') ? previousUnit : `SQUARE ${previousUnit}`
										)
									);
									form.setFieldValue(unitName, value);
								}}
								value={form.values[unitName]}
								options={squareOptions ? squareOptions : areaUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMultiUnitAreaInput = ({ name, unitName, label, squareOptions, relatedFields = [] }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(values, unitName);
									form.setFieldValue(
										name,
										areaconvertor(
											field.value,
											value.includes('SQUARE') ? value : `SQUARE ${value}`,
											previousUnit.includes('SQUARE') ? previousUnit : `SQUARE ${previousUnit}`
										)
									);
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) =>
											form.setFieldValue(
												fieldName,
												lengthconvertor(
													getIn(values, fieldName),
													value.replace('SQUARE').trim(),
													previousUnit.replace('SQUARE').trim()
												)
											)
										);
									}
									form.setFieldValue(unitName, value);
								}}
								value={getIn(values, unitName)}
								options={squareOptions ? squareOptions : areaUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMultiUnitLengthInput = ({ name, unitName, label, relatedFields = [], areaField }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';

				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(values, unitName);
									form.setFieldValue(
										name,
										lengthconvertor(field.value, value.replace('SQUARE').trim(), previousUnit.replace('SQUARE').trim())
									);
									if (areaField) {
										form.setFieldValue(
											areaField,
											areaconvertor(
												getIn(values, areaField),
												value.includes('SQUARE') ? value : `SQUARE ${value}`,
												previousUnit.includes('SQUARE') ? previousUnit : `SQUARE ${previousUnit}`
											)
										);
									}
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) =>
											form.setFieldValue(
												fieldName,
												lengthconvertor(
													getIn(values, fieldName),
													value.replace('SQUARE').trim(),
													previousUnit.replace('SQUARE').trim()
												)
											)
										);
									}
									form.setFieldValue(unitName, value);
								}}
								value={getIn(form.values, unitName)}
								options={distanceOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMultiUnitArea1Input = ({ name, unitName, label, squareOptions, relatedFields = [] }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = values[unitName];
									form.setFieldValue(
										name,
										areaconvertor(
											field.value,
											value.includes('SQUARE') ? value : `SQUARE ${value}`,
											previousUnit.includes('SQUARE') ? previousUnit : `SQUARE ${previousUnit}`
										)
									);
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) =>
											form.setFieldValue(
												fieldName,
												lengthconvertor(
													values[fieldName],
													value.replace('SQUARE').trim(),
													previousUnit.replace('SQUARE').trim()
												)
											)
										);
									}
									form.setFieldValue(unitName, value);
								}}
								value={form.values[unitName]}
								options={squareOptions ? squareOptions : areaUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMultiUnitLength1Input = ({ name, unitName, label, relatedFields = [], areaField }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = values[unitName];
									form.setFieldValue(
										name,
										lengthconvertor(field.value, value.replace('SQUARE').trim(), previousUnit.replace('SQUARE').trim())
									);
									if (areaField) {
										form.setFieldValue(
											areaField,
											areaconvertor(
												values[areaField],
												value.includes('SQUARE') ? value : `SQUARE ${value}`,
												previousUnit.includes('SQUARE') ? previousUnit : `SQUARE ${previousUnit}`
											)
										);
									}
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) =>
											form.setFieldValue(
												fieldName,
												lengthconvertor(
													values[fieldName],
													value.replace('SQUARE').trim(),
													previousUnit.replace('SQUARE').trim()
												)
											)
										);
									}
									form.setFieldValue(unitName, value);
								}}
								value={form.values[unitName]}
								options={distanceOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedMasterUnitInput = ({ name, unitName, label, placeName, masterData }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = form.errors[name];
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && `${label}`}
						<Input
							name={name}
							// onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className}`}
							value={field.value}
						/>{' '}
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{`( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const reqNadi = masterData.find((nadi) => nadi.placeName === getIn(values, placeName));
									if (reqNadi) {
										if (value === 'METRE') {
											form.setFieldValue(name, reqNadi.setBackMeter);
										} else {
											form.setFieldValue(name, reqNadi.setBackFoot);
										}
									}

									form.setFieldValue(unitName, value);
								}}
								value={getIn(form.values, unitName)}
								options={distanceOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedAreaInputWithRelatedUnits = ({ name, unitName, label, needsBold = false, squareOptions, relatedFields = [], compact = false }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						{label && needsBold ? <b>{label}</b> : label}{' '}
						<Input
							name={name}
							onChange={form.handleChange}
							className={`dashedForm-control bpf-inputType ${className} ${compact && 'compact-form-input'}`}
							value={field.value}
						/>
						<div className="brackets" style={{ display: 'inline-flex' }}>
							{` ( `}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(values, unitName);
									form.setFieldValue(name, convertArea(field.value, value, previousUnit));
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) => {
											if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
												form.setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
											} else {
												form.setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
											}
										});
									}
									form.setFieldValue(unitName, value);
								}}
								value={getIn(values, unitName)}
								options={squareOptions ? squareOptions : areaUnitOptions}
							/>
							{' ) '}
						</div>{' '}
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedLengthInputWithRelatedUnitsAndCalc = React.memo(
	({ name, unitName, label, relatedFields = [], otherField, areaField, compact = false }) => {
		return (
			<Field name={name}>
				{({ field, form }) => {
					const error = getIn(form.errors, name);
					const values = form.values;
					const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';

					return (
						<InputRelatedUnitCalc
							label={label}
							name={name}
							handleChange={form.handleChange}
							className={className}
							values={form.values}
							setFieldValue={form.setFieldValue}
							error={error}
							fieldValue={field.value}
							unitValue={getIn(values, unitName)}
							unitName={unitName}
							relatedFields={relatedFields}
							compact={compact}
							otherField={otherField}
							areaField={areaField}
						/>
					);
				}}
			</Field>
		);
	}
);

export const InputRelatedUnitCalc = React.memo(
	({
		label,
		name,
		handleChange,
		compact,
		className,
		values,
		fieldValue,
		unitValue,
		unitName,
		relatedFields,
		error,
		setFieldValue,
		otherField,
		areaField,
	}) => {
		return (
			<>
				{label && `${label}`}
				<Input
					name={name}
					onChange={(e) => {
						if (otherField && areaField) {
							handleChange(e);
							setFieldValue(areaField, calculateArea(e.target.value, getIn(values, otherField)));
						} else {
							handleChange(e);
						}
					}}
					className={`dashedForm-control bpf-inputType ${className} ${compact && 'compact-form-input'}`}
					value={fieldValue}
				/>{' '}
				<div className="brackets" style={{ display: 'inline-flex' }}>
					{`( `}
					<Dropdown
						name={unitName}
						onChange={(e, { value }) => {
							const previousUnit = getIn(values, unitName);
							setFieldValue(name, convertLength(fieldValue, value, previousUnit));
							if (relatedFields && relatedFields.length > 0) {
								relatedFields.forEach((fieldName) => {
									if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
										setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
									} else {
										setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
									}
								});
							}
							setFieldValue(unitName, value);
						}}
						value={unitValue}
						options={distanceOptions}
					/>
					{' ) '}
				</div>{' '}
				{error && (
					<Label pointing="left" prompt size="large" basic color="red">
						{error}
					</Label>
				)}
			</>
		);
	},
	(prev, next) => {
		return (
			prev.fieldValue === next.fieldValue && prev.unitValue === next.unitValue && prev.error === next.error && prev.classname === next.className
		);
	}
);

export const DashedLengthInputWithRelatedUnits = React.memo(({ name, unitName, label, relatedFields = [], compact = false }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				const values = form.values;
				const className = isStringEmpty(field.value) ? (error ? 'error' : 'success') : 'text-input';

				return (
					<InputRelatedUnit
						label={label}
						name={name}
						handleChange={form.handleChange}
						className={className}
						values={form.values}
						setFieldValue={form.setFieldValue}
						error={error}
						fieldValue={field.value}
						unitValue={getIn(values, unitName)}
						unitName={unitName}
						relatedFields={relatedFields}
						compact={compact}
					/>
				);
			}}
		</Field>
	);
});

export const InputRelatedUnit = React.memo(
	({ label, name, handleChange, compact, className, values, fieldValue, unitValue, unitName, relatedFields, error, setFieldValue }) => {
		return (
			<>
				{label && `${label}`}
				<Input
					name={name}
					onChange={handleChange}
					className={`dashedForm-control bpf-inputType ${className} ${compact && 'compact-form-input'}`}
					value={fieldValue}
				/>{' '}
				<div className="brackets" style={{ display: 'inline-flex' }}>
					{`( `}
					<Dropdown
						name={unitName}
						onChange={(e, { value }) => {
							const previousUnit = getIn(values, unitName) || DEFAULT_UNIT_LENGTH;
							setFieldValue(name, convertLength(fieldValue, value, previousUnit));
							if (relatedFields && relatedFields.length > 0) {
								relatedFields.forEach((fieldName) => {
									if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
										setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
									} else {
										setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
									}
								});
							}
							setFieldValue(unitName, value);
						}}
						value={unitValue}
						options={distanceOptions}
					/>
					{' ) '}
				</div>{' '}
				{error && (
					<Label pointing="left" prompt size="large" basic color="red">
						{error}
					</Label>
				)}
			</>
		);
	},
	(prev, next) => {
		return (
			prev.fieldValue === next.fieldValue && prev.unitValue === next.unitValue && prev.error === next.error && prev.classname === next.className
		);
	}
);

export const UnitDropdownWithRelatedFields = ({ options, unitName, relatedFields = [], brackets = true }) => {
	return (
		<Field>
			{({ field, form }) => {
				const values = form.values;
				return (
					<div className={brackets ? 'brackets' : undefined} style={{ display: 'inline-flex' }}>
						{brackets && `( `}
						<Dropdown
							name={unitName}
							onChange={(e, { value }) => {
								const previousUnit = getIn(values, unitName) || DEFAULT_UNIT_LENGTH;
								if (relatedFields && relatedFields.length > 0) {
									relatedFields.forEach((fieldName) => {
										if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
											getIn(values, fieldName) &&
												form.setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
										} else {
											getIn(values, fieldName) &&
												form.setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
										}
									});
								}
								form.setFieldValue(unitName, value);
							}}
							value={getIn(form.values, unitName)}
							options={options ? options : distanceOptions}
						/>
						{brackets && ' ) '}
					</div>
				);
			}}
		</Field>
	);
};

export default EbpsUnitLabelValue;
