import React from 'react';
import { Form, Select, Label, Dropdown } from 'semantic-ui-react';
import { Field, getIn } from 'formik';

const EbpsSelect = ({ label, options, value, id, name, setFieldValue, ...rest }) => {
	return (
		<Form.Group widths="equal">
			<Form.Field>
				<div className="EbpsCustomSelect">
					{label && <span>{label}</span>}
					<Select options={options} id={id} value={value} name={name} onChange={(e, { value }) => setFieldValue(name, value)} {...rest} />
				</div>
			</Form.Field>
		</Form.Group>
	);
};

export const SelectInput = ({ label, options, name, needsZIndex = false, compact = false }) => {
	let className = '';
	if (compact) className += 'compact ';
	if (needsZIndex) {
		className += 'zIndexSelect';
	} else {
		className += 'EbpsCustomSelect';
	}
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				return (
					<Form.Group widths="equal">
						<Form.Field error={!!error}>
							<div className={className}>
								{label && <span>{label}</span>}
								<Select
									options={options}
									value={getIn(form.values, name)}
									name={name}
									onChange={(e, { value }) => form.setFieldValue(name, value)}
								/>
							</div>
							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
};

export const DashedSelect = ({ label, options, name, scrolling = false }) => {
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				return (
					<>
						{label && <>{label}</>}
						<Dropdown
							name={name}
							options={options}
							scrolling={scrolling}
							onChange={(e, { value }) => form.setFieldValue(name, value)}
							className={`dashedForm-control`}
							value={getIn(form.values, name)}
						/>
						{error && (
							<Label pointing="left" prompt size="large" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const SearchableSelectInput = ({ label, options, name, needsZIndex = false }) => {
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				return (
					<Form.Group widths="equal">
						<Form.Field error={!!error}>
							<div className={needsZIndex ? 'zIndexSelect' : 'EbpsCustomSelect'}>
								{label && <span>{label}</span>}
								<Dropdown
									options={options}
									selection
									search
									value={getIn(form.values, name)}
									name={name}
									onChange={(e, { value }) => form.setFieldValue(name, value)}
								/>
							</div>
							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
};

export const SetbackSelect = ({ label, options, name, unitField, setbackField, setbackMaster }) => {
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				const values = form.values;
				return (
					<Form.Group widths="equal" className="inline-group">
						<Form.Field error={!!error}>
							{/* <div className="EbpsCustomSelect"> */}
							{label && <span>{label}</span>}
							<Select
								options={options}
								value={getIn(values, name)}
								name={name}
								onChange={(e, { value }) => {
									form.setFieldValue(name, value);
									const reqNadi = setbackMaster.find((nadi) => nadi.placeName === value);
									if (reqNadi) {
										if (getIn(values, unitField) === 'METRE') {
											form.setFieldValue(setbackField, reqNadi.setBackMeter);
										} else {
											form.setFieldValue(setbackField, reqNadi.setBackFoot);
										}
									} else {
										form.setFieldValue(setbackField, 0);
									}
								}}
							/>
							{/* </div> */}
							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
};
export default EbpsSelect;
