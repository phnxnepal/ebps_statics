import React from 'react';
import EbpsForm from './EbpsForm';

const RadioGroup = ({
  heading,
  name,
  valuesObj,
  otherChecked,
  handleChange,
  handleFilter,
}) => {
  return (
    <div className="inline field">
      <span>{heading}</span>
      {valuesObj.map(obj => (
        <div className="ui radio checkbox" key={obj.value} style={{marginRight: 10}}>
          <input
            type="radio"
            name={name}
            value={obj.value}
            id={obj.value}
            defaultChecked={obj.defaultChecked}
            onChange={handleChange}
          />
          <label>{obj.value}</label>
        </div>
      ))}
      {!!otherChecked && (
        <span><EbpsForm
          name={`${name}Other`}
          placeholder="Additional Information..."
          onChange={handleChange}
        /></span>
      )}
    </div>
  );
};

export default RadioGroup;
