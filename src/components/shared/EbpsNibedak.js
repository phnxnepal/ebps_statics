import React from 'react';
import PropTypes from 'prop-types';
import { Form, Label } from 'semantic-ui-react';
// import { translate, getInputType } from '../../utils/langUtils';
// import { isStringEmpty } from '../../utils/stringUtils';
// import { translateWithSlash } from './../../utils/langUtils';
import { useTranslatorCoupled } from '../../hooks/useTranslator';
import { Field, getIn } from 'formik';
import { getClassName } from '../../utils/formUtils';

const EbpsNibedak = React.memo(({ label, error, setFieldValue, coupledFieldName, name, value, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorCoupled(value, setFieldValue, error, name, coupledFieldName);

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					{...rest}
					name={name}
					type="text"
					onChange={e => handleChange(e)}
					// onBlur={setFieldValue(coupledFieldName, valueState)}
					className={`${className}`}
					value={valueState || value}
				/>

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

EbpsNibedak.propTypes = {
	label: PropTypes.string,
	error: PropTypes.string,
	setFieldValue: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
};

export const CoupledInputField = React.memo(({ label, coupledFields, name }) => {
	return (
		<Field name={name}>
			{({ form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);
				const className = getClassName(value, error);
				return (
					<Form.Group widths="equal">
						<Form.Field error={!!error}>
							<span>{label}</span>
							<input
								name={name}
								onChange={form.handleChange}
								onBlur={() => {
									if (coupledFields && coupledFields.length > 0) {
										coupledFields.forEach(field => form.setFieldValue(field, value));
									}
								}}
								className={`bpf-inputType ${className}`}
								value={value}
							/>

							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
});

CoupledInputField.propTypes = {
	label: PropTypes.string,
	name: PropTypes.string.isRequired,
	coupledFields: PropTypes.array.isRequired,
};

export default EbpsNibedak;
