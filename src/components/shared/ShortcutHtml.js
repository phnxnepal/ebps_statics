import React, { Component } from 'react';

export class ShortcutHtml extends Component {
  render() {
    return <span style={{ color: 'grey' }}>{` ${this.props.content}`}</span>;
  }
}

export default ShortcutHtml;
