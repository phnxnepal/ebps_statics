import React, { useEffect, useState } from 'react';
import { getIn, Field } from 'formik';
import { Form, Label, Input, Dropdown } from 'semantic-ui-react';
import { isStringEmpty } from '../../utils/stringUtils';
import { calculateAmount1, calculateTotal, calculateDiscount, convertArea, convertLength, round, getFormattedNumber } from '../../utils/mathUtils';
import { distanceOptions } from '../../utils/dataUtils';
import { validateMoreThanZeroNumberField, validateZeroOrMoreNumberField } from '../../utils/validationUtils';
import { isKamalamai, isMechi, isBirtamod, isInaruwa, isKankai } from '../../utils/clientUtils';
// import { getAreaUnitValueFromLength } from '../../utils/functionUtils';

const needsDharauti = !isKamalamai && !isBirtamod && !isMechi && !isInaruwa && !isKankai;
export const needToCalculateAnyaAmt = isMechi;

export const calculateAnyaAmount = (anyaAmt, ...areas) => {
	if (needToCalculateAnyaAmt) {
		const total = areas.reduce((acc, area) => getFormattedNumber(area) + acc, 0.0);
		return round(total * 0.11);
	} else return getFormattedNumber(anyaAmt);
};

export const getSarsaphaiDastur = (detailsTotal) => {
	return round(detailsTotal * 0.1);
};

export const getBatabaranDastur = (detailsTotal) => {
	return round((getSarsaphaiDastur(detailsTotal) + detailsTotal) * 0.01);
};

const setTotalAmounts = (setFieldValue, detailsTotal, totalAmt) => {
	const batabaranDasturAmt = getBatabaranDastur(detailsTotal);
	const sarsaphaiDasturAmt = getSarsaphaiDastur(detailsTotal);

	setFieldValue('batabaranDasturAmt', batabaranDasturAmt);
	setFieldValue('sarsaphaiDasturAmt', sarsaphaiDasturAmt);
	if (isKamalamai) {
		setFieldValue('totalAmt', round(totalAmt + batabaranDasturAmt + sarsaphaiDasturAmt));
	} else if (needsDharauti) {
		setFieldValue(`totalAmt`, round(totalAmt + totalAmt / 2));
	} else {
		setFieldValue(`totalAmt`, round(totalAmt));
	}
};

const amountFields = ['drAmount', 'jrAmount', 'depositAmount', 'jDepositAmount'];

const grandTotalFields = ['anyeAmt', 'aminAmt', 'formAmt', 'applicationAmt'];

const RajasowTableInput = React.memo(({ name, index, values, mulFieldName, amountFieldName, floorFieldName, floor, setFieldValue, error }) => {
	const [totalAreaExcluded, setTotalAreaExcluded] = useState(0);

	const otherRowField = name.includes('dArea') ? `${index}.jArea` : `${index}.dArea`;

	useEffect(() => {
		const totalArea =
			values.details &&
			values.details
				.filter((row, idx) => idx !== index)
				.reduce((acc, rw) => acc + getFormattedNumber(rw.dArea) + getFormattedNumber(rw.jArea), 0) + getFormattedNumber(getIn(values.details, otherRowField));
		setTotalAreaExcluded(round(totalArea));
	}, [index, values.details, otherRowField]);

	const value = getIn(values, name);

	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
	const amountArr = values.details.map((dt, dtIndex) => {
		const retArr = [];
		amountFields
			.filter((field) => {
				return !(amountFieldName.includes(field) && index === dtIndex);
			})
			.forEach((fld) => {
				return retArr.push(dt[fld]);
			});

		return retArr;
	});
	const grandTotalArr = grandTotalFields
		.filter((row) => row !== 'anyeAmt')
		.map((fld) => {
			return values[fld];
		});

	return (
		<Form.Field error={!!error}>
			<input
				placeholder="0.00"
				name={name}
				onChange={(e) => {
					setFieldValue(name, e.target.value);
					if (floorFieldName && floor && !getIn(values, floorFieldName)) {
						setFieldValue(floorFieldName, floor);
					}

					const amount = calculateAmount1(e.target.value, getIn(values, mulFieldName));
					setFieldValue(amountFieldName, amount);

					const detailsTotal = calculateTotal(...amountArr.flat(), amount);
					setFieldValue(`detailsTotal`, detailsTotal);

					const anyeAmt = calculateAnyaAmount(values.anyeAmt, totalAreaExcluded, e.target.value);
					setFieldValue(`anyeAmt`, anyeAmt);

					const [discountAmt, totalAmt] = calculateDiscount(getIn(values, 'discountPercent'), grandTotalArr, detailsTotal);
					const actualTotalAmt = totalAmt + anyeAmt;

					setFieldValue(`discountAmount`, discountAmt);
					setFieldValue(`totalAmtBeforeDharauti`, actualTotalAmt);
					setFieldValue(`dharautiAmt`, actualTotalAmt / 2);
					setTotalAmounts(setFieldValue, detailsTotal, actualTotalAmt);
				}}
				className={`bpf-inputType ${className}`}
				value={value}
			/>

			{error && <span className="tableError">{error}</span>}
		</Form.Field>
	);
});

export const RajasowRateInput = React.memo(
	({ name, index, values, mulFieldName, amountFieldName, floorFieldName, floor, setFieldValue, error, isHeightRelevantFloor, noZero = true }) => {
		const value = getIn(values, name);

		const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
		const amountArr = values.details.map((dt, dtIndex) => {
			const retArr = [];
			amountFields
				.filter((field) => {
					return !(amountFieldName.includes(field) && index === dtIndex);
				})
				.forEach((fld) => {
					return retArr.push(dt[fld]);
				});

			return retArr;
		});
		const grandTotalArr = grandTotalFields.map((fld) => {
			return values[fld];
		});

		return (
			<Form.Field error={!!error}>
				<Field
					validate={(isHeightRelevantFloor && noZero) ? validateMoreThanZeroNumberField : validateZeroOrMoreNumberField}
					placeholder="0.00"
					name={name}
					onChange={(e) => {
						setFieldValue(name, e.target.value);
						if (floorFieldName && floor && !getIn(values, floorFieldName)) {
							setFieldValue(floorFieldName, floor);
						}

						const amount = calculateAmount1(e.target.value, getIn(values, mulFieldName));
						setFieldValue(amountFieldName, amount);

						const detailsTotal = calculateTotal(...amountArr.flat(), amount);
						setFieldValue(`detailsTotal`, detailsTotal);

						const [discountAmt, totalAmt] = calculateDiscount(getIn(values, 'discountPercent'), grandTotalArr, detailsTotal);
						setFieldValue(`discountAmount`, discountAmt);
						setFieldValue(`totalAmtBeforeDharauti`, totalAmt);
						setFieldValue(`dharautiAmt`, totalAmt / 2);
						setTotalAmounts(setFieldValue, detailsTotal, totalAmt);
					}}
					className={`bpf-inputType ${className}`}
					value={value}
				/>

				{error && <span className="tableError">{error}</span>}
			</Form.Field>
		);
	}
);

export const RajasowTable2Input = React.memo(({ name, values, setFieldValue, error }) => {
	const value = getIn(values, name);

	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	let retArr = [];
	grandTotalFields
		.filter((field) => {
			return !name.includes(field);
		})
		.forEach((fld) => {
			return retArr.push(values[fld]);
		});

	return (
		<Form.Field error={!!error}>
			<input
				placeholder="0.00"
				name={name}
				onChange={(e) => {
					setFieldValue(name, e.target.value);
					const [, totalAmt] = calculateDiscount(
						getIn(values, 'discountPercent'),
						[...retArr, e.target.value],
						getIn(values, 'detailsTotal')
					);

					setFieldValue(`totalAmtBeforeDharauti`, totalAmt);
					setFieldValue(`dharautiAmt`, totalAmt / 2);


					// setFieldValue(`batabaranDasturAmt`, totalAmt / 2);
					// setFieldValue(`dharautiAmt`, getIn(values, 'detailsTotal'))
					if (isKamalamai) {
						setFieldValue(
							'totalAmt',
							round(totalAmt + getBatabaranDastur(getIn(values, 'detailsTotal')) + getSarsaphaiDastur(getIn(values, 'detailsTotal')))
						);
					} else if (needsDharauti) {
						setFieldValue(`totalAmt`, round(totalAmt + totalAmt / 2));
					} else {
						setFieldValue(`totalAmt`, round(totalAmt));
					}
					// setFieldValue(`totalAmt`, calculateTotal(...retArr, e.target.value, getIn(values, 'detailsTotal')));
				}}
				className={`bpf-inputType ${className}`}
				value={value}
			/>

			{error && <span className="tableError">{error}</span>}
		</Form.Field>
	);
});

export const RajasowDiscountPercent = React.memo(({ name, values, setFieldValue, error }) => {
	const value = getIn(values, name);
	const detailsTotal = parseFloat(getIn(values, 'detailsTotal') || 0.0);

	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	let retArr = [];
	grandTotalFields
		.filter((field) => {
			return !name.includes(field);
		})
		.forEach((fld) => {
			return retArr.push(values[fld]);
		});

	return (
		<Form.Field error={!!error}>
			<Input
				labelPosition="right"
				label={{ basic: true, content: '%' }}
				style={{ width: '100px' }}
				placeholder="0.00"
				type="number"
				step="0.1"
				name={name}
				onChange={(e) => {
					const input = e.target.value;

					setFieldValue(name, input);

					const [discountAmt, totalAmt] = calculateDiscount(input, retArr, detailsTotal);

					setFieldValue('discountAmount', discountAmt);
					setFieldValue(`totalAmtBeforeDharauti`, totalAmt);
					setFieldValue(`dharautiAmt`, totalAmt / 2);
					setFieldValue(`totalAmt`, totalAmt + totalAmt / 2);
				}}
				className={`bpf-inputType ${className}`}
				value={value}
			/>

			{error && (
				<Label pointing prompt size="large">
					{error}
				</Label>
			)}
		</Form.Field>
	);
});

export const RajasowUnitDropdownWithRelatedFields = ({ options, areaFields = [], unitName, relatedFields = [] }) => {
	return (
		<Field>
			{({ field, form }) => {
				const values = form.values;
				let retArr = [];
				grandTotalFields.forEach((fld) => {
					return retArr.push(values[fld]);
				});
				return (
					<div className="brackets" style={{ display: 'inline-flex' }}>
						{` ( `}
						{/* {needToCalculateAnyaAmt ? (
							getAreaUnitValueFromLength(values[unitName])
						) : ( */}
							<Dropdown
								name={unitName}
								onChange={(e, { value }) => {
									const previousUnit = getIn(values, unitName);
									let detailsTotal = getIn(values, 'detailsTotal');
									if (relatedFields && relatedFields.length > 0) {
										relatedFields.forEach((fieldName) => {
											if (fieldName === 'detailsTotal') {
												detailsTotal = convertArea(getIn(values, fieldName), value, previousUnit);
												form.setFieldValue(fieldName, detailsTotal);
											} else if (
												fieldName.toUpperCase().includes('area'.toUpperCase()) ||
												areaFields.find((fld) => fieldName.includes(fld))
											) {
												getIn(values, fieldName) &&
													form.setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
											} else {
												getIn(values, fieldName) &&
													form.setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
											}
										});
									}

									const [discountAmt, totalAmt] = calculateDiscount(getIn(values, 'discountPercent'), [...retArr], detailsTotal);

									// form.setFieldValue(`totalAmt`, totalAmt);
									form.setFieldValue(`discountAmount`, discountAmt);
									form.setFieldValue(`totalAmtBeforeDharauti`, totalAmt);
									form.setFieldValue(`dharautiAmt`, totalAmt / 2);
									setTotalAmounts(form.setFieldValue, detailsTotal, totalAmt);
									form.setFieldValue(unitName, value);
								}}
								value={getIn(form.values, unitName)}
								options={options ? options : distanceOptions}
							/>
						{/* )} */}
						{' ) '}
					</div>
				);
			}}
		</Field>
	);
};

// class RajasowTableInput extends React.Component {
// 	constructor(props) {
// 		super(props);
// 		this.state = {
// 			currVal: this.props.value,
// 			amount: this.props.amountValue
// 		};
// 	}

// 	setCurrVal = value => {
// 		this.setState({ currVal: value });
// 	};

// 	setAmount = value => {
// 		this.setState({ amount: value });
// 	};

// 	render() {
// 		const {
// 			name,
// 			index,
// 			mulFieldName,
// 			amountFieldName,
// 			floorFieldName,
// 			floor,
// 			setFieldValue
// 		} = this.props;

// 		const { currVal, amount } = this.state;

// 		return (
// 			<Field name={name}>
// 				{({ field, form }) => {
// 					const error = getIn(form.errors, name);
// 					const values = form.values;
// 					const amountArr = values.details.map((dt, dtIndex) => {
// 						const retArr = [];
// 						amountFields
// 							.filter(field => {
// 								return !(amountFieldName.includes(field) && index === dtIndex);
// 							})
// 							.forEach(fld => {
// 								return retArr.push(dt[fld]);
// 							});

// 						return retArr;
// 					});

// 					console.log('amountArr', amountArr);
// 					// const amountFields = values.details.map()
// 					const className = !isStringEmpty(field.value)
// 						? error
// 							? 'error'
// 							: 'success'
// 						: 'text-input';
// 					return (
// 						<Form.Field error={!!error}>
// 							<input
// 								placeholder="0.00"
// 								name={name}
// 								value={field.value}
// 								className={className}
// 								onChange={e => {
// 									this.setCurrVal(e.target.value);
// 									setFieldValue(name, currVal);
// 									// if (
// 									// 	floorFieldName &&
// 									// 	floor &&
// 									// 	!getIn(values, floorFieldName)
// 									// ) {
// 									// 	form.setFieldValue(floorFieldName, floor);
// 									// }

// 									this.setAmount(
// 										calculateAmount1(currVal, getIn(values, mulFieldName))
// 									);
// 									const currentAmount = calculateAmount1(
// 										e.target.value,
// 										getIn(values, mulFieldName)
// 									);

// 									setFieldValue(amountFieldName, amount);
// 									console.log(
// 										'amount aryy',
// 										...amountArr.flat(),
// 										'current amount',
// 										amount
// 									);
// 									setFieldValue(
// 										`detailsTotal`,
// 										calculateTotal(
// 											// getIn(values, amountFieldName),
// 											// values.detailsTotal
// 											...amountArr.flat(),
// 											amount
// 										)
// 										// false
// 									);
// 								}}
// 							/>
// 							{error && (
// 								<Label pointing prompt size="large">
// 									{error}
// 								</Label>
// 							)}
// 						</Form.Field>
// 					);
// 				}}
// 			</Field>
// 		);
// 	}
// }

export default RajasowTableInput;
