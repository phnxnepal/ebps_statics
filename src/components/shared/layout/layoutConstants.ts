export enum LayoutTypes {
    INPUT = 'input',
    INPUT_DATE = 'input-date',
    INPUT_LANG = 'input-lang',
    YES_NO_RADIO_LIST = 'yes-no-radio-list'
}