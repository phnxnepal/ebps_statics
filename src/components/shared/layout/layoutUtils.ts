export const concatValues = (values: string[]) => {
    return {
        data: values.join(' ')
    }
}