import React from 'react';
import { DashedLangInput } from '../DashedFormInput';
import { getIn } from 'formik';
import { LayoutTypes } from './layoutConstants';
import EbpsTextareaField from '../MyTextArea';

export const RenderLayout = ({ layout, values, setFieldValue, errors, handleChange }) => {
	const renderYesNoSection = section => {
		return (
			<ol key="yes-no-section">
				{section.list.map((item, idx) => (
					<li key={idx}>
						{item.label}
						{item.otherName ? (
							<>
								{section.options.map(option => (
									<div className="ui radio checkbox" key={option.key}>
										<input
											type="radio"
											name={item.name}
											value={option.value}
											onChange={handleChange}
											checked={getIn(values, item.name) === option.value}
										/>
										<label>{option.text}</label>
									</div>
								))}
								{getIn(values, item.name) === section.options[1].value && (
									<EbpsTextareaField
										placeholder={item.placeholder}
										name={item.otherName}
										setFieldValue={setFieldValue}
										value={values.sadakMapdandaPalanaAdditional}
										error={errors.sadakMapdandaPalanaAdditional}
									/>
								)}
							</>
						) : (
							<>
								<EbpsTextareaField
									placeholder={item.placeholder}
									name={item.name}
									setFieldValue={setFieldValue}
									value={values.sadakMapdandaPalanaAdditional}
									error={errors.sadakMapdandaPalanaAdditional}
								/>
							</>
						)}
					</li>
				))}
			</ol>
		);
	};

	return (
		<div>
			{layout && layout.map((section, index) => {
				switch (section.type) {
					case LayoutTypes.INPUT_LANG:
						return <DashedLangInput key={index} name={section.name} value={getIn(values, section.name)} setFieldValue={setFieldValue} />;
					case LayoutTypes.YES_NO_RADIO_LIST:
						return renderYesNoSection(section);
					default:
						return <React.Fragment key={index}>{`${section.data} `}</React.Fragment>;
				}
			})}
		</div>
	);
};
