import React from 'react';
import { Form, Input } from 'semantic-ui-react';

const EbpsLabelInput = ({ inputLabel, labelName, ...rest }) => {
  return (
    <Form.Group widths="equal">
      <Form.Field>
        {labelName && <span>{labelName}</span>}
        <Input label={inputLabel} labelPosition="right" {...rest} />
      </Form.Field>
    </Form.Group>
  );
};

export default EbpsLabelInput;
