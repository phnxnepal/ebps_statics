import React, { Component } from 'react';
import { saveButton } from '../../utils/data/genericData';

// Redux
import { connect } from 'react-redux';
import { putApprovalByUrl } from '../../store/actions/formActions';

// Components
// import ApprovalModal from './ApprovalModal';
import ApprovalModal from './ApprovalWithCommentModal';
import RejectionModal from './RejectionModal';

// Functions
import { getBackUrl } from '../../utils/config';
import SingleFileView from '../loggedInComponents/forms/SingleFIleView';
import { NoticePaymentFileView, GenericFileView } from './file/NoticePaymentFileView';
import { NIBEDAK_PHOTO_TITLE } from '../../utils/constants';

class ApprovalGroup extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isApprovalModalOpen: false,
			isRejectionModalOpen: false,
			isApprovalModalConfirmOpen: false,
		};
	}

	componentDidMount() {
		this.props.onRef && this.props.onRef(this);
	}

	componentWillUnmount() {
		this.props.onRef && this.props.onRef(undefined);
	}

	handleApprovalModalOpen = () => {
		this.setState({
			isApprovalModalOpen: true,
		});
	};

	handleApprovalModalConfirmOpen = () => {
		this.setState({
			isApprovalModalConfirmOpen: true,
		});
	};

	handleApprovalModalConfirmClose = () => {
		this.setState({
			isApprovalModalConfirmOpen: false,
		});
	};

	handleApprovalModalClose = () => {
		this.setState({
			isApprovalModalOpen: false,
		});
	};

	handleRejectionModalOpen = () => {
		this.setState({
			isRejectionModalOpen: true,
		});
	};

	handleRejectionModalClose = () => {
		this.setState({
			isRejectionModalOpen: false,
		});
	};

	// Notice payment
	handleNoticeApproval = (action) => {
		this.setState({
			noticeApproval: action,
		});
	};

	// Map permit
	handleMapPermitApproval = (action) => {
		this.setState({
			permitApproval: action,
		});
	};

	render() {
		return (
			<>
				<ApprovalModal
					open={this.state.isApprovalModalOpen}
					onClose={this.handleApprovalModalClose}
					formUrl={this.props.formUrl}
					successUrl={() =>
						this.props.putApprovalByUrl(`${getBackUrl()}${this.props.url}`, {
							status: 'A',
						})
					}
					url={`${getBackUrl()}${this.props.url}`}
					approvalAction={this.props.putApprovalByUrl}
					history={this.props.parentHistory}
					loadingModal={this.props.loadingModal}
				/>

				<ApprovalModal
					open={this.state.noticeApproval}
					onClose={() => this.handleNoticeApproval(false)}
					formUrl={this.props.formUrl}
					title={saveButton.approveConfirm}
					successUrl={() =>
						this.props.putApprovalByUrl(`${getBackUrl()}${this.props.url}`, {
							status: 'A',
						})
					}
					url={`${getBackUrl()}${this.props.url}`}
					approvalAction={this.props.putApprovalByUrl}
					history={this.props.parentHistory}
					loadingModal={this.props.loadingModal}
					content={<NoticePaymentFileView prevData={this.props.noticePaymentData} />}
				/>

				<ApprovalModal
					open={this.state.permitApproval}
					onClose={() => this.handleMapPermitApproval(false)}
					formUrl={this.props.formUrl}
					title={saveButton.approveConfirm}
					successUrl={() => {
						if (this.props.approvalAppNo) {
							return this.props.putApprovalByUrl(`${getBackUrl()}${this.props.url}/${this.props.approvalAppNo}`, {
								status: 'A',
							});
						} else {
							return this.props.putApprovalByUrl(`${getBackUrl()}${this.props.url}`, {
								status: 'A',
							});
						}
					}}
					url={
						this.props.approvalAppNo ? `${getBackUrl()}${this.props.url}/${this.props.approvalAppNo}` : `${getBackUrl()}${this.props.url}`
					}
					approvalAction={this.props.putApprovalByUrl}
					history={this.props.parentHistory}
					loadingModal={this.props.loadingModal}
					content={
						<>
							<GenericFileView url={this.props.noticePaymentData && this.props.noticePaymentData.photo} title={NIBEDAK_PHOTO_TITLE} />

							<SingleFileView url={this.props.formUrl} fileCategories={this.props.fileCategories} />
						</>
					}
				/>

				<ApprovalModal
					open={this.state.isApprovalModalConfirmOpen}
					onClose={this.handleApprovalModalConfirmClose}
					formUrl={this.props.formUrl}
					title={saveButton.approveConfirm}
					successUrl={() =>
						this.props.putApprovalByUrl(`${getBackUrl()}${this.props.url}`, {
							status: 'A',
						})
					}
					url={`${getBackUrl()}${this.props.url}`}
					approvalAction={this.props.putApprovalByUrl}
					history={this.props.parentHistory}
					loadingModal={this.props.loadingModal}
					content={<SingleFileView url={this.props.formUrl} fileCategories={this.props.fileCategories} />}
				/>
				<RejectionModal
					open={this.state.isRejectionModalOpen}
					onClose={this.handleRejectionModalClose}
					rejectionUrl={this.props.putApprovalByUrl}
					history={this.props.parentHistory}
					loadingModal={this.props.loadingModal}
					url={`${getBackUrl()}${this.props.url}`}
				/>

				<>
					{/* <Button
            // disabled={initialValues.erStatus === 'A'}
            color="green"
            onClick={this.handleApprovalModalOpen}
          >
            Approve
          </Button>
          <Button
            // disabled={initialValues.erStatus === 'A'}
            color="red"
            onClick={this.handleRejectionModalOpen}
          >
            Reject
          </Button> */}
				</>
			</>
		);
	}
}

const mapDispatchToProps = { putApprovalByUrl };
const mapStateToProps = (state) => {
	return {
		loadingModal: state.root.ui.loadingModal,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ApprovalGroup);
