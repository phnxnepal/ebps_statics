import React from "react";
import ReactToPrint from "react-to-print";
// import { Button } from "semantic-ui-react";
import { connect } from "react-redux";
import ShortcutHtml from "./ShortcutHtml";

class Printthis extends React.Component {
  render() {
    let { content, ui, shortcut, ...rest } = this.props;
    return (
      <ReactToPrint
        trigger={() =>
          ui ? (
            ui
          ) : (
            <span className="printt" color="instagram">
              Print {shortcut && <ShortcutHtml content="[alt + p]" />}
            </span>
          )
        }
        pageStyle="@page { size: A4;} @media print{body{text-align: justify}}}"
        content={content}
        {...rest}
      />
    );
  }
}

const mapStateToProps = state => ({
  shortcut: state.root.general.shortcut
});

export default connect(mapStateToProps)(Printthis);
