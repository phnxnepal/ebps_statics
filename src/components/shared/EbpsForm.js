import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Label, Input } from 'semantic-ui-react';
// import { translate, getInputType } from '../../utils/langUtils';
import { isStringEmpty } from '../../utils/stringUtils';
// import { translateWithSlash } from './../../utils/langUtils';
import { useTranslator, useTranslatorWithSlash, useTranslatorIm, useNumTranslator, useTranslatorCoupled } from '../../hooks/useTranslator';
import Cleave from 'cleave.js/react';
import { areaCleaveOption } from '../../utils/optionUtils';
import { Field, getIn } from 'formik';
import { getClassName } from '../../utils/formUtils';
import { useCursorReset } from '../../hooks/useCursorReset';
import { FlexSpaceBetween } from '../uiComponents/FlexDivs';

const EbpsForm = React.memo(({ label, error, setFieldValue, onChange, name, value, compactError = false, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorIm(value, setFieldValue, error, name);
	const [inputRef, setCursorPosition] = useCursorReset(valueState);

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					ref={inputRef}
					{...rest}
					name={name}
					type="text"
					onChange={(e) => {
						setCursorPosition(e.target.selectionStart);
						if (setFieldValue) handleChange(e);
						else onChange(e);
					}}
					className={`bpf-inputType ${className}`}
					value={valueState || value}
				/>
				{error && (
					<>
						{compactError ? (
							<span className="tableError">{error}</span>
						) : (
							<Label pointing prompt size="large">
								{error}
							</Label>
						)}
					</>
				)}
			</Form.Field>
		</Form.Group>
	);
});

EbpsForm.propTypes = {
	label: PropTypes.string,
	error: PropTypes.string,
	setFieldValue: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
};

export const EbpsFormIm = React.memo(({ label, error, setFieldValue, onChange, name, value, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorIm(value, setFieldValue, error, name);

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					{...rest}
					name={name}
					type="text"
					onChange={setFieldValue ? handleChange : onChange}
					className={`bpf-inputType ${className}`}
					value={valueState || value}
				/>

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsSlashForm = React.memo(({ label, error, setFieldValue, onChange, name, value, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorWithSlash(value, setFieldValue, error, name);
	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					{...rest}
					name={name}
					type="text"
					onChange={setFieldValue ? handleChange : onChange}
					className={`bpf-inputType ${className}`}
					value={valueState || value}
				/>

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

EbpsSlashForm.propTypes = {
	label: PropTypes.string,
	error: PropTypes.string,
	setFieldValue: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
};

export const EbpsNumberForm = React.memo(({ label, error, onChange, disabled, name, value, isTable = false, ...rest }) => {
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					disabled={disabled}
					{...rest}
					name={name}
					type="text"
					onChange={onChange}
					className={`bpf-inputType ${className}`}
					value={value}
				/>

				{error &&
					(isTable ? (
						<span className="tableError">{error}</span>
					) : (
						<Label pointing prompt size="large">
							{error}
						</Label>
					))}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsNormalFormIm = ({ label, name }) => {
	return (
		<Field>
			{({ field, form }) => {
				const value = getIn(form.values, name);
				const error = getIn(form.errors, name);
				const className = getClassName(value, error);
				return (
					<Form.Group widths="equal">
						<Form.Field error={!!error}>
							{label && <span>{label}</span>}
							<input name={name} type="text" onChange={form.handleChange} className={`bpf-inputType ${className}`} value={value} />

							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
};

export const EbpsNormalForm = React.memo(({ label, error, onChange, name, value, ...rest }) => {
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				{label && <span>{label}</span>}
				<input {...rest} name={name} type="text" onChange={onChange} className={`bpf-inputType ${className}`} value={value} />

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsNormalFormInline = React.memo(({ label, error, onChange, name, value, ...rest }) => {
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<Form.Group inline>
			<Form.Field error={!!error}>
				{label && <span>{label}</span>}
				<input {...rest} name={name} type="text" onChange={onChange} className={`bpf-inputType ${className}`} value={value} />

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsFormInline = React.memo(({ span, error, setFieldValue, onChange, name, value }) => {
	const [className, valueState, handleChange] = useTranslator(value, setFieldValue, error, name);

	return (
		<Form.Group inline>
			<Form.Field error={!!error}>
				{span && <span>{span}</span>}
				<Input name={name} onChange={setFieldValue ? handleChange : onChange} className={`bpf-inputType ${className}`} value={valueState} />

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsField = React.memo(({ label, error, setFieldValue, onChange, name, value }) => {
	const [className, valueState, handleChange] = useTranslator(value, setFieldValue, error, name);

	return (
		<Form.Field error={!!error}>
			{label && label}
			<Input name={name} onChange={setFieldValue ? handleChange : onChange} className={`bpf-inputType ${className}`} value={valueState} />

			{error && (
				<Label pointing prompt size="large">
					{error}
				</Label>
			)}
		</Form.Field>
	);
});

export const EbpsTextArea = React.memo(({ label, error, setFieldValue, onChange, name, value, placeholder, cols, rows, pointing }) => {
	const [className, valueState, handleChange] = useTranslator(value, setFieldValue, error, name);

	return (
		<Form.Group inline>
			<Form.Field error={!!error}>
				{label && <label>{label}</label>}
				<textarea
					placeholder={placeholder}
					cols={cols ? cols : '150'}
					rows={rows ? rows : '20'}
					style={{ border: 'solid 1px #85131A' }}
					name={name}
					onChange={setFieldValue ? handleChange : onChange}
					className={`bpf-inputType ${className}`}
					value={valueState}
				/>

				{error && (
					<Label pointing={pointing ? pointing : 'left'} prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

export const EbpsTextAreaMultiLang = React.memo(
	({ label, error, setFieldValue, onChange, name, value, placeholder, cols, rows, pointing, isNepali = true, toggler }) => {
		const [className, valueState, handleChange] = useTranslator(value, setFieldValue, error, name);

		return (
			<Form.Group inline>
				<Form.Field error={!!error}>
					{label && (
						<FlexSpaceBetween>
							<b>{label}</b>
							{toggler}
						</FlexSpaceBetween>
					)}
					{isNepali ? (
						<textarea
							placeholder={placeholder}
							cols={cols ? cols : '150'}
							rows={rows ? rows : '20'}
							name={name}
							onChange={handleChange}
							className={`bpf-inputType text-area ${className}`}
							value={valueState}
						/>
					) : (
						<textarea
							placeholder={placeholder}
							cols={cols ? cols : '150'}
							rows={rows ? rows : '20'}
							name={name}
							onChange={onChange}
							className={`bpf-inputType text-area ${className}`}
							value={value}
						/>
					)}
					{error && (
						<Label pointing={pointing ? pointing : 'left'} prompt size="large">
							{error}
						</Label>
					)}
				</Form.Field>
			</Form.Group>
		);
	}
);

export const PermitAreaInput = React.memo(
	({ label, error, setFieldValue, onChange, name, value, placeholder, dashed = false, unitComponent = null }) => {
		const [className, valueState, handleChange] = useNumTranslator(value, setFieldValue, error, name);
		const [option, setOption] = useState(0);

		/**
		 * @TODO revert temporary input values.
		 */
		useEffect(() => {
			try {
				setOption(areaCleaveOption.find((opt) => opt.key === placeholder).index);
			} catch (err) {
				setOption(0);
			}
		}, [placeholder, setOption]);

		return (
			<Form.Group widths="equal" inline>
				<Form.Field error={!!error}>
					<span>{label}</span>
					{(option === 3 || option === 2) && (
						<input
							// {...rest}
							name={name}
							type="text"
							onChange={setFieldValue ? handleChange : onChange}
							className={`bpf-inputType ${className} ${dashed ? 'dashedForm-control' : ''}`}
							placeholder={placeholder}
							value={valueState || value}
						/>
					)}
					{option === 0 && (
						<Cleave
							name={name}
							className={`bpf-inputType ${className} ${dashed ? 'dashedForm-control' : ''}`}
							value={valueState || value}
							onChange={setFieldValue ? handleChange : onChange}
							placeholder={placeholder}
							options={{ delimiter: '-', blocks: [2, 2, 2, 2] }}
						/>
					)}
					{option === 4 && (
						<Cleave
							name={name}
							className={`bpf-inputType ${className} ${dashed ? 'dashedForm-control' : ''}`}
							value={valueState || value}
							onChange={setFieldValue ? handleChange : onChange}
							placeholder={placeholder}
							options={{ delimiter: '-', blocks: [2, 2, 2, 5] }}
						/>
					)}
					{option === 1 && (
						<Cleave
							name={name}
							className={`bpf-inputType ${className} ${dashed ? 'dashedForm-control' : ''}`}
							value={valueState || value}
							onChange={setFieldValue ? handleChange : onChange}
							placeholder={placeholder}
							options={{
								suffix: placeholder,
								delimiter: '-',
								blocks: [2, 2, 5],
							}}
						/>
					)}

					{error && (
						<span className="tableError">
							{error}
						</span>
					)}

					{unitComponent && unitComponent}
				</Form.Field>
			</Form.Group>
		);
	}
);

export const EbpsCoupledForm = React.memo(({ label, error, setFieldValue, onChange, name, otherField, value, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorCoupled(value, setFieldValue, error, name, otherField);
	const [inputRef, setCursorPosition] = useCursorReset(valueState);

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<input
					ref={inputRef}
					{...rest}
					name={name}
					type="text"
					onChange={(e) => {
						setCursorPosition(e.target.selectionStart);
						if (setFieldValue) handleChange(e);
						else onChange(e);
					}}
					className={`bpf-inputType ${className}`}
					value={valueState || value}
				/>

				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
});

EbpsCoupledForm.propTypes = {
	label: PropTypes.string,
	error: PropTypes.string,
	setFieldValue: PropTypes.func.isRequired,
	name: PropTypes.string.isRequired,
	otherField: PropTypes.string.isRequired,
};

export default EbpsForm;
