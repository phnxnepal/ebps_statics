import React from 'react';
import { Sticky, Icon, Message } from 'semantic-ui-react';
import Printthis from './Printthis';
import ApprovalGroup from './ApprovalGroup';
import { HistoryContent } from './HistoryComponent';
import { ApprovalStatus, ActionComments } from './ApprovalStatusDisplay';
import { getUserRole, brack } from '../../utils/functionUtils';
import GenericFloatingWindow from './GenericFloatingWindow';
import {
	makeprintformat,
	// makespecialprintformat
} from '../../utils/functionUtils';
import JqxMenu from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxmenu';
import SingleFileUpload from '../loggedInComponents/forms/SingleFileUpload';
import { connect } from 'react-redux';
import ShortcutHtml from './ShortcutHtml';
import ForwardToNextForm from '../loggedInComponents/forms/ForwardToNextForm';
import ListFiles from '../loggedInComponents/forms/ListFiles';
import { getFormMasterId } from '../../utils/urlUtils';
import { FormUrlFull } from '../../utils/enums/url';
import { NoticePaymentFileView, GenericFileView } from './file/NoticePaymentFileView';
import { getLocalStorage } from '../../utils/secureLS';
import { RENEW_DATA, NIBEDAK_PHOTO_TITLE } from '../../utils/constants';
import { getRenewInfo } from '../../utils/dataUtils';
import { taskList } from '../../utils/data/genericData';
import { translateEngToNep } from '../../utils/langUtils';
import { UserType } from '../../utils/userTypeUtils';
import PermitPhotoUpload from '../loggedInComponents/forms/mapPermitComponents/PermitPhotoUpload';

// const ApprovalButtonDisplay = (UserMain: string, serStatus: string, erStatus: string, enterBy: string) => {
// 	console.log(UserMain, erStatus, serStatus, enterBy);

// 	if (UserMain == 'A') {
// 		if (serStatus == 'A' || serStatus === 'S' || enterBy == 'SubEngineer') {
// 			return true;
// 		} else {
// 			return false;
// 		}
// 	} else if (UserMain == 'R') {
// 		// if (erStatus == 'A') {
// 		//   return true;
// 		// } else {
// 		//   return false;
// 		// }
// 		// Rajasow doesn't have approve or reject permission.
// 		return false;
// 	} else {
// 		return true;
// 	}
// };
interface ActionBarProps {
	isVisible: boolean;
	historyList?: Object[];
	comments?: Object[];
	formUrl: string;
	files: Object[];
	hasSavePermission: boolean;
	hasApprovePermission: boolean;
	statuses: {};
	isRejected: boolean;
	approvalRef: Object;
}

interface ActionBarState {
	approvemodal: boolean;
	rejectmodal: boolean;
	showAllCategory: boolean;
	showAllCategoryUpload: boolean;
}

class ActionBar extends React.PureComponent<ActionBarProps, ActionBarState> {
	private myMenu = React.createRef<JqxMenu>();

	constructor(props: ActionBarProps) {
		super(props);

		this.state = {
			approvemodal: false,
			rejectmodal: false,
			showAllCategory: false,
			showAllCategoryUpload: false,
		};
	}

	//@ts-ignore
	renderNotification = isRejected => {
		if (isRejected) {
			return (
				<span>
					<Icon color="red" name="warning circle" />
				</span>
			);
		} else {
			return undefined;
		}
	};

	componentDidMount() {}

	render() {
		const {
			//@ts-ignore
			content,
			isVisible,
			//@ts-ignore
			stickycontext,
			//@ts-ignore
			setPrintContent,
			//@ts-ignore
			parentHistory,
			//@ts-ignore
			approvalUrl,
			hasApprovePermission,
			hasSavePermission,
			historyList = [],
			comments = [],
			statuses,
			isRejected,
			//@ts-ignore
			formUrl,
			//@ts-ignore
			fileCategories,
			files,
			//@ts-ignore
			onBeforeGetContent,
			//@ts-ignore
			shortcut,
			//@ts-ignore
			setFileUploadRef,
			//@ts-ignore
			prevData,
			//@ts-ignore
			approvalAppNo,
			//@ts-ignore
			appNoError,
			approvalRef,
		} = this.props;

		const isNoticePayment = formUrl === FormUrlFull.NOTICE_PAYMENT_APPLICATION;
		const isMapPermit = formUrl === FormUrlFull.BUILD_PERMIT;

		const renewData = getLocalStorage(RENEW_DATA);
		let needsUrgentRenew = false;
		let renewDays = '0';
		if (renewData && getUserRole() === UserType.DESIGNER) {
			const { urgent, daysRemaining } = getRenewInfo(JSON.parse(renewData));
			needsUrgentRenew = urgent;
			renewDays = translateEngToNep(daysRemaining);
		}

		// let recoveryDOM = '';
		let dupContent = '';
		// let UserMain = getUserRole();

		const isDisabled = !!appNoError;

		let printContent = onBeforeGetContent ? (
			<Printthis
				content={() => dupContent}
				onBeforeGetContent={() => {
					// console.log('content k cha', content);
					setPrintContent(true);
					dupContent = content.cloneNode(true);
					brack(dupContent);
					//@ts-ignore
					Object.values(onBeforeGetContent).forEach(each => {
						//@ts-ignore
						return makeprintformat(dupContent, ...each);
						// }
					});
					if (approvalRef) {
						//@ts-ignore
						dupContent.append(approvalRef)
					}
				}}
				onBeforePrint={() => {
					setPrintContent(false);
					// content.replaceWith(recoveryDOM);
					// content = recoveryDOM;
				}}
			/>
		) : (
			<Printthis
				content={() => content}
				onBeforeGetContent={() => {
					setPrintContent(true);
				}}
				onBeforePrint={() => {
					setPrintContent(false);
				}}
			/>
		);

		return (
			<div style={{ margin: '-20px', marginTop: '-30px' }}>
				<ApprovalGroup
					//@ts-ignore
					onRef={el => (this.approveRef = el)}
					parentHistory={parentHistory}
					noticePaymentData={isNoticePayment || isMapPermit ? prevData : undefined}
					url={approvalUrl}
					fileCategories={fileCategories}
					formUrl={formUrl}
					//@ts-ignore
					open={this.props.isApprovalModalOpen}
					//@ts-ignore
					onClose={this.props.handleApprovalModalClose}
					//@ts-ignore
					approvalAppNo={approvalAppNo}
				/>

				<Sticky context={stickycontext} offset={42}>
					<JqxMenu ref={this.myMenu} width={'100%'} height={35}>
						<ul>
							{isVisible && hasApprovePermission && (
								/**
								 * @TODO confirm why this is needed.
								 */
								// && ApprovalButtonDisplay(UserMain, serStatus, erStatus, enterBy)
								<li style={{ color: '#099e2c', pointerEvents: isDisabled ? 'none' : 'auto', opacity: isDisabled ? 0.3 : 1 }}>
									<Icon name="thumbs up" />
									<span
										className="approve"
										//@ts-ignore
										onClick={() => {
											//@ts-ignore
											if (isNoticePayment) this.approveRef.handleNoticeApproval(true);
											//@ts-ignore
											else if (isMapPermit) this.approveRef.handleMapPermitApproval(true);
											else {
												fileCategories &&
												fileCategories.some(
													//@ts-ignore
													category => category.viewUrl && category.viewUrl.trim() === formUrl
												)
													? // @ts-ignore
													  //@ts-ignore
													  this.approveRef.handleApprovalModalConfirmOpen()
													: //@ts-ignore
													  //@ts-ignore
													  this.approveRef.handleApprovalModalOpen();
											}
										}}
									>
										Approve
										{/* 
                    //@ts-ignore */}
										{shortcut && <ShortcutHtml content="[alt + a]" />}
									</span>
								</li>
							)}
							{isVisible && hasApprovePermission && (
								/**
								 * @TODO confirm if this is needed
								 */
								// && ApprovalButtonDisplay(UserMain, serStatus, erStatus, enterBy)
								<li style={{ color: '#c70000', pointerEvents: isDisabled ? 'none' : 'auto', opacity: isDisabled ? 0.3 : 1 }}>
									<Icon name="thumbs down" />

									<span
										className="reject"
										//@ts-ignore
										onClick={() => this.approveRef.handleRejectionModalOpen()}
									>
										Reject
										{/* 
                    //@ts-ignore */}
										{shortcut && <ShortcutHtml content="[alt + r]" />}
									</span>
								</li>
							)}
							{!isNoticePayment &&
								hasSavePermission &&
								fileCategories &&
								fileCategories.some(
									//@ts-ignore
									category => category.viewUrl && category.viewUrl.trim() === formUrl
								) && (
									<li
										id="menuItemFileUpload"
										className="jqx-item jqx-menu-item-top jqx-rc-all"
										role="menuitem"
										style={{ float: 'left' }}
									>
										{/* //@ts-ignore */}
										<GenericFloatingWindow
											label="Upload File"
											icon="file"
											setFileUploadRef={setFileUploadRef}
											button={true}
											//@ts-ignore
											allupload={x => this.setState({ showAllCategoryUpload: x })}
										>
											<>
												{isMapPermit && <PermitPhotoUpload />}
												<SingleFileUpload url={formUrl} fileCategories={fileCategories} prevData={prevData} files={files} />
											</>
										</GenericFloatingWindow>
									</li>
								)}
							{/* 
              //@ts-ignore */}
							{/* {this.state.showAllCategory && ( */}
							{/* <li> */}
							{/* //@ts-ignore */}

							{/* <GenericFloatingWindow
                    label="All File Category"
                    icon="file"
                    autoOpen={true}
                    onclose={true}
                    //@ts-ignore
                    allcategory={x => this.setState({ showAllCategory: x })}
                  >
                    <ListFiles />
                  </GenericFloatingWindow>
                </li>
              )} */}
							{/* 
              //@ts-ignore */}
							{/* {this.state.showAllCategoryUpload && ( */}
							{/* <li> */}
							{/* //@ts-ignore */}
							{/* <GenericFloatingWindow
                    label="All File Category Upload"
                    icon="file"
                    autoOpen={true}
                    onclose={true}
                    //@ts-ignore
                    allupload={x => this.setState({ showAllCategoryUpload: x })}
                  >
                    <FileUpload />
                  </GenericFloatingWindow>
                </li> */}
							{/* )} */}
							{isNoticePayment ? (
								<li
									id="menuItemFileUpload"
									className="jqx-item jqx-menu-item-top jqx-rc-all"
									role="menuitem"
									style={{ float: 'left' }}
								>
									<GenericFloatingWindow label="View File" icon="file code">
										<NoticePaymentFileView prevData={prevData} />
									</GenericFloatingWindow>
								</li>
							) : formUrl === FormUrlFull.BUILD_PERMIT ? (
								fileCategories &&
								fileCategories.some(
									//@ts-ignore
									category => category.viewUrl && category.viewUrl.trim() === formUrl
								) && (
									<li
										id="menuItemFileUpload"
										className="jqx-item jqx-menu-item-top jqx-rc-all"
										role="menuitem"
										style={{ float: 'left' }}
									>
										<GenericFloatingWindow
											label="View File"
											icon="file code"
											setFileUploadRef={setFileUploadRef}
											button={true}
											//@ts-ignore
											allcategory={x => this.setState({ showAllCategory: x })}
										>
											<>
												<GenericFileView url={prevData.photo} width={350} title={NIBEDAK_PHOTO_TITLE} />
												<ListFiles url={getFormMasterId(formUrl)} Durl={formUrl} prevData={prevData} files={files} />
											</>
										</GenericFloatingWindow>
									</li>
								)
							) : (
								fileCategories &&
								fileCategories.some(
									//@ts-ignore
									category => category.viewUrl && category.viewUrl.trim() === formUrl
								) && (
									<li
										id="menuItemFileUpload"
										className="jqx-item jqx-menu-item-top jqx-rc-all"
										role="menuitem"
										style={{ float: 'left' }}
									>
										{/* 
                  //@ts-ignore */}
										<GenericFloatingWindow
											label="View File"
											icon="file code"
											setFileUploadRef={setFileUploadRef}
											button={true}
											//@ts-ignore
											allcategory={x => this.setState({ showAllCategory: x })}
										>
											<ListFiles url={getFormMasterId(formUrl)} Durl={formUrl} prevData={prevData} />
										</GenericFloatingWindow>
									</li>
								)
							)}
							<li>
								<Icon name="print" />
								{printContent}
							</li>
							<li>
								{/* 
                //@ts-ignore */}
								<GenericFloatingWindow label="History" icon="history" historyList={historyList}>
									{/* 
                  //@ts-ignore */}
									<HistoryContent />
								</GenericFloatingWindow>
							</li>
							<li>
								{/* 
                //@ts-ignore */}
								<GenericFloatingWindow
									notification={this.renderNotification(isRejected)}
									label="Comments"
									icon="comments"
									//@ts-ignore
									comments={comments}
								>
									<ActionComments />
								</GenericFloatingWindow>
							</li>
							<li>
								<Icon name="tag" />
								<ApprovalStatus
									isRejected={isRejected}
									statuses={statuses}
									formUrl={formUrl}
								/>
							</li>
							<li>
								<GenericFloatingWindow label="Forward to Next" icon="arrow alternate circle right">
									<ForwardToNextForm history={parentHistory} location={{ pathname: formUrl }} modal={true} />
								</GenericFloatingWindow>
							</li>
							<li>
								{!isVisible && (
									<span
										style={{
											color: 'rgb(158,0,0)',
											position: 'absolute',
											right: '15px',
											fontWeight: 'bold',
										}}
									>
										<Icon name="warning sign" />
										यो निबेदन तपाइको कार्यसूचीमा छैन।
									</span>
								)}
							</li>
						</ul>
					</JqxMenu>
					{needsUrgentRenew && (
						<JqxMenu>
							<Message
								floating
								negative
								className="renewBar"
								icon="warning circle"
								header={taskList.renewalReminder.heading}
								content={`${taskList.renewalReminder.contentPrefix} ${renewDays} ${taskList.renewalReminder.contentSuffix}`}
							/>
						</JqxMenu>
					)}
				</Sticky>
			</div>
		);
	}
}

//@ts-ignore
const mapStateToProps = state => ({
	shortcut: state.root.general.shortcut,
});

export default connect(mapStateToProps)(ActionBar);
