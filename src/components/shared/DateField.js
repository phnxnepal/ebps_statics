import React, { useState } from 'react';
import Cleave from 'cleave.js/react';
import { Form, Label } from 'semantic-ui-react';
import { isStringEmpty } from '../../utils/stringUtils';
import { getInputType, translateNum } from '../../utils/langUtils';
import { Field, getIn } from 'formik';

export const DateField = ({ label, handleChange, width, value, className, ...rest }) => {
	if (value === null) {
		value = undefined;
	}
	return (
		<>
			<label>{label}</label>
			<Cleave
				style={{ width: width || '20%' }}
				placeholder="YYYY-mm-dd"
				options={{
					date: true,
					delimiter: '-',
					datePattern: ['Y', 'm', 'd'],
				}}
				onChange={handleChange}
				value={value}
				{...rest}
			/>
		</>
	);
};

export const EbpsFormDateField = ({ label, handleChange, name, value, error, ...rest }) => {
	if (value === null) {
		value = undefined;
	}
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<Cleave
					{...rest}
					name={name}
					className={`bpf-inputType ${className}`}
					value={value}
					onChange={handleChange}
					placeholder="YYYY-MM-DD"
					options={{
						numericOnly: true,
						delimiter: '-',
						blocks: [4, 2, 2],
					}}
				/>
				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export const DashedLangDateField = ({ inline, label, handleChange, setFieldValue, name, value, error, compact = false }) => {
	const [valueState, setValueState] = useState(value);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleValueChange = e => {
		const value1 = translateNum(e, getInputType());
		setValueState(value1);
		setFieldValue(name, value1);
	};
	if (value === null) {
		value = undefined;
	}

	return (
		<Form.Group className={inline ? 'inline-group' : null}>
			<Form.Field error={!!error}>
				{label && <span>{label}</span>}
				<Cleave
					name={name}
					className={`dashedForm-control bpf-inputType ${className} ${compact ? 'compact-form-input' : undefined}`}
					value={value}
					onChange={setFieldValue ? handleValueChange : handleChange}
					placeholder="YYYY-MM-DD"
					options={{
						delimiter: '-',
						blocks: [4, 2, 2],
					}}
				/>
				{error && (
					<Label pointing="left" prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export const CompactDashedLangDate = ({ ...rest }) => {
	return <DashedLangDateField inline={true} compact={true} {...rest} />;
};

export const DashedDateField = ({ inline, label, name, compact = false }) => {
	return (
		<Field>
			{({ _, form }) => {
				const value = getIn(form.values, name) || '';
				const error = getIn(form.errors, name);
				const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<Form.Group className={inline ? 'inline-group' : null}>
						<Form.Field error={!!error}>
							{label && <span>{label}</span>}
							<Cleave
								name={name}
								className={`dashedForm-control bpf-inputType ${className} ${compact ? 'compact-form-input' : undefined}`}
								value={value}
								onChange={form.handleChange}
								placeholder="YYYY-MM-DD"
								options={{
									delimiter: '-',
									blocks: [4, 2, 2],
								}}
							/>
							{error && (
								<Label pointing="left" prompt size="large">
									{error}
								</Label>
							)}
						</Form.Field>
					</Form.Group>
				);
			}}
		</Field>
	);
};

export const LangDateField = ({ inline, label, handleChange, setFieldValue, name, value, disabled, error }) => {
	const [valueState, setValueState] = useState(value);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleValueChange = e => {
		// As some bug is preventing the first letter from being set in value.
		const value1 = translateNum(e, getInputType());
		// const value1 = e.target.value
		setValueState(value1);
		setFieldValue(name, value1);
	};
	if (value === null) {
		value = undefined;
	}
	return (
		<Form.Group widths="equal" className={inline ? 'inline-group' : null}>
			<Form.Field error={!!error}>
				<span>{label}</span>
				<Cleave
					name={name}
					className={`bpf-inputType ${className}`}
					value={value}
					onChange={setFieldValue ? handleValueChange : handleChange}
					placeholder="YYYY-MM-DD"
					options={{
						delimiter: '-',
						blocks: [4, 2, 2],
					}}
					disabled={disabled}
				/>
				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export const FiscalYearField = ({ label, handleChange, name, value, error }) => {
	if (value === null) {
		value = undefined;
	}
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!error}>
				<span>{label}</span>
				<Cleave
					name={name}
					className={`bpf-inputType ${className}`}
					value={value}
					onChange={handleChange}
					placeholder="YYYY-YYYY"
					options={{
						numericOnly: true,
						delimiter: '-',
						blocks: [4, 4],
					}}
				/>
				{error && (
					<Label pointing prompt size="large">
						{error}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
};

export default DateField;
