import React from 'react';
import { Button, Label, Icon, Modal } from 'semantic-ui-react';
import { isEmpty, getUserStatusNepali } from '../../utils/functionUtils';
import { saveButton } from '../../utils/data/genericData';
import SingleFileUpload from '../loggedInComponents/forms/SingleFileUpload';
import { connect } from 'react-redux';
import { getDisableStatus } from '../../utils/urlUtils';
import { FormUrlFull } from '../../utils/enums/url';
import PermitPhotoUpload from '../loggedInComponents/forms/mapPermitComponents/PermitPhotoUpload';

// const SaveButton1 = ({ hasSavePermission, handleSubmit, prevData, fileUploadRef }) => {
// 	const isSave = isEmpty(prevData);
// 	const { erStatus, serStatus, rwStatus } = prevData;
// 	let isDisabled = false;
// 	try {
// 		//   anyone accept and not anyone reject
// 		isDisabled = (erStatus === 'A' || serStatus === 'A' || rwStatus === 'A') && !(erStatus === 'R' || serStatus === 'R' || rwStatus === 'R');
// 	} catch {
// 		isDisabled = false;
// 	}
// 	return hasSavePermission ? (
// 		<>
// 			<Button
// 				primary
// 				disabled={isDisabled}
// 				onClick={() => {
// 					if (fileUploadRef) {
// 						fileUploadRef.current.open();
// 					}
// 					handleSubmit();
// 				}}
// 			>
// 				{isSave ? 'Save' : 'Update'}
// 			</Button>
// 			{isDisabled && (
// 				<Label basic color="green" size="large">
// 					<Icon name="check" />
// 					{getUserStatusNepali('A')}
// 				</Label>
// 			)}
// 		</>
// 	) : null;
// };

export class SaveButton extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			open: false,
		};
	}

	handleSubmitModal = () => {
		this.handleClose();
		this.props.handleSubmit();
	};

	handleModalOpen = () => {
		this.setState({ open: true });
	};
	handleClose = () => {
		this.setState({ open: false });
	};
	render() {
		const {
			hasSavePermission,
			handleSubmit,
			prevData,
			errors,
			formName,
			// fileUploadRef,
			fileCategories,
			validateForm,
			formUrl = '',
		} = this.props;

		const { open } = this.state;
		const isSave = isEmpty(prevData);
		const { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus } = prevData || {};
		let isDisabled = false;
		const needsFile =
			fileCategories && fileCategories.some(category => category.viewUrl && category.viewUrl.trim() === formUrl.trim());

		try {
			//   anyone accept and not anyone reject
			isDisabled = getDisableStatus(formUrl, { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus });
			// (erStatus === 'A' || serStatus === 'A' || rwStatus === 'A') &&
			// !(erStatus === 'R' || serStatus === 'R' || rwStatus === 'R');
		} catch {
			isDisabled = false;
		}
		return hasSavePermission ? (
			<>
				<Modal key="save-confirmation" closeIcon open={open} onClose={this.handleClose}>
					<Modal.Header>{saveButton.uploadFileReminder}</Modal.Header>
					<Modal.Content scrolling>
						{/* <h3>{saveButton.uploadFileReminder}</h3> */}
						{ formUrl === FormUrlFull.BUILD_PERMIT && <PermitPhotoUpload />}
						<SingleFileUpload url={formUrl} fileCategories={fileCategories} prevData={prevData}/>
					</Modal.Content>
					<Modal.Actions>
						<Button negative onClick={this.handleClose}>
							Exit
						</Button>
						<Button positive icon="checkmark" labelPosition="right" content={isSave ? 'Save Form' : 'Update Form'} onClick={this.handleSubmitModal} />
					</Modal.Actions>
				</Modal>
				<Button
					primary
					type="submit"
					disabled={isDisabled}
					onClick={() => {
						if (!isEmpty(errors)) {
							const form = document.forms[formName || 0];
							for (let i = 0; i < form.length; i++) {
								// console.log(form[i].name, Object.keys(errors));

								if (
									Object.keys(errors).some(errorField => {
										// console.log(
										// 	'onegai dakara',
										// 	errorField,
										// 	form[i].name.includes(errorField)
										// );
										/**
										 * @todo doesn't work when a field name is a substring of other.
										 */
										return form[i].name.includes(errorField);
									})
								) {
									// console.log('nanda?');

									form[i].focus();
									break;
								}
							}
						} else {
							if (needsFile) {
								if (validateForm) {
									validateForm().then(errors => {
										if (isEmpty(errors)) {
											this.handleModalOpen();
										}
									});
								} else {
									this.handleModalOpen();
								}
							} else {
								handleSubmit();
							}
						}
					}}
				>
					{isSave ? 'Save' : 'Update'}
				</Button>
				{isDisabled && (
					<Label basic color="green" size="large">
						<Icon name="check" />
						{getUserStatusNepali('A')}
					</Label>
				)}
			</>
		) : null;
	}
}

const mapStateToProps = state => ({
	fileCategories: state.root.formData.fileCategories,
});

export default connect(mapStateToProps)(SaveButton);
