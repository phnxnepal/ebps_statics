import React from 'react';
import { Loader, Message, Dimmer } from 'semantic-ui-react';

const FallbackComponent = props => {
	return (
		<div className="sitee-main">
			<Loader active={props.loading} inline="centered">
				<b>कृपया प्रतीक्षा गर्नुहोस्</b>
			</Loader>
			{props.errors && typeof props.errors.message === 'string' && (
				<Message negative className="english-div">
					<Message.Header>Error</Message.Header>
					<p>{props.errors.message}</p>
				</Message>
			)}
		</div>
	);
};

export const Loading = ({ loading }) => {
	return (
		<Dimmer active={loading} inverted>
			<Loader active={loading} inline="centered">
				<b>कृपया प्रतीक्षा गर्नुहोस्</b>
			</Loader>
		</Dimmer>
	);
};

export default FallbackComponent;
