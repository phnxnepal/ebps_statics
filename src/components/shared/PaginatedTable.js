import React from 'react';
import { Table, Menu, Icon } from 'semantic-ui-react';

export const PaginatedTable = ({ headerLabels, data, children }) => {
	return (
		<Table>
			<Table.Header>
				<Table.Row>
					{headerLabels.map(label => (
						<Table.HeaderCell>{label}</Table.HeaderCell>
					))}
					<Table.HeaderCell>Actions</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				{data.length < 1 ? (
					<p>No Data Found.</p>
				) : (
					data.map(row => (
						<Table.Row>
							{Object.values(row).map(val => (
								<Table.Cell>{val}</Table.Cell>
							))}
							<Table.Cell>{children}</Table.Cell>
						</Table.Row>
					))
				)}
			</Table.Body>
			{/**
			 * @TODO Implement later
			 */
			/* <Table.Footer>
				<Table.Row>
					<Table.HeaderCell colSpan="3">
						<Menu floated="right" pagination>
							<Menu.Item as="a" icon>
								<Icon name="chevron left" />
							</Menu.Item>
							<Menu.Item as="a">1</Menu.Item>
							<Menu.Item as="a">2</Menu.Item>
							<Menu.Item as="a">3</Menu.Item>
							<Menu.Item as="a">4</Menu.Item>
							<Menu.Item as="a" icon>
								<Icon name="chevron right" />
							</Menu.Item>
						</Menu>
					</Table.HeaderCell>
				</Table.Row>
			</Table.Footer> */}
		</Table>
	);
};
