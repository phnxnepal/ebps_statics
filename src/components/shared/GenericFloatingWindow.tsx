import * as React from 'react';
import JqxWindow from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxwindow';
import { Icon, SemanticICONS } from 'semantic-ui-react';
import { connect } from 'react-redux';
import ShortcutHtml from './ShortcutHtml';

//@ts-ignore
const mapStateToProps = state => ({
  shortcut: state.root.general.shortcut
});

class GenericFloatingWindow extends React.PureComponent<{
  icon: SemanticICONS;
  label: string;
  children: any;
  shortcut: boolean;
  setFileUploadRef?: any;
  autoOpen?: any;
  allcategory?: any;
  onclose?: any;
  allupload?: any;
  button?: any;
  notification?: any;
}> {
  private myWindow = React.createRef<JqxWindow>();

  componentDidMount() {
    this.props.setFileUploadRef && this.props.setFileUploadRef(this.myWindow);
  }
  componentWillUnmount() {
    this.myWindow.current!.destroy();
    // this.myWindow.current!.focus();
  }

  public render() {
    const {
      label,
      icon,
      children,
      //@ts-ignore
      button,
      shortcut,
      allupload,
      allcategory,
      notification,
      ...rest
    } = this.props;


    return (
      <>
        <span className={label} onClick={() => this.onWindowToggle()}>
          <Icon name={icon} />
          {label} {notification && notification}
          {shortcut && (
            <ShortcutHtml
              content={`[alt+${label.substring(0, 1).toLowerCase()}]`}
            />
          )}
        </span>

        <JqxWindow
          ref={this.myWindow}
          width={700}
          height={500}
          minWidth={200}
          maxWidth={1200}
          minHeight={200}
          position={'center'}
          maxHeight={900}
          //@ts-ignore
          autoOpen={this.props.autoOpen || false}
          title={label}
          showCollapseButton={true}
          // onClose={() => {
          //   //@ts-ignore
          //   this.props.allcategory && this.props.allcategory(false);
          //   this.props.allupload && this.props.allupload(false);
          // }}
        >
          <div>
            {/* {button && allupload && (
              //@ts-ignore
              <button onClick={() => allupload(true)}>All Category</button>
            )}
            {button && allcategory && (
              //@ts-ignore
              <button onClick={() => allcategory(true)}>All Category</button>
            )} */}

            {React.cloneElement(children, { ...rest })}
          </div>
        </JqxWindow>
      </>
    );
  }

  onWindowToggle = (): void => {
    this.myWindow.current!.isOpen()
      ? this.myWindow.current!.close()
      : this.myWindow.current!.open();
  };
}

export default connect(mapStateToProps)(GenericFloatingWindow);
