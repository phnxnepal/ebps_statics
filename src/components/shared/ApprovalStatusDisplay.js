import React from 'react';
import { Segment, Label, Grid, Icon, Feed, Popup, Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { getFormMasterObj } from '../../utils/urlUtils';
import { getApprovalStatusObject } from '../../utils/userTypeUtils';
import { getNepaliDate } from '../../utils/dateUtils';
import { translateEngToNep } from '../../utils/langUtils';
import { approvalStatus } from '../../utils/data/genericData';
import { isStringEmpty } from '../../utils/stringUtils';

const getVal = statusCode => {
	if (statusCode === 'A') {
		return { label: 'Approved', color: 'teal' };
	} else if (statusCode === 'R') {
		return { label: 'Rejected', color: 'red' };
	} else {
		return { label: 'Pending', color: 'orange' };
	}
};

const ApprovalStatusDisplay = ({ comments, erStatus, serStatus }) => {
	// console.log('erstatus', erStatus);
	const { label: erLabel, color: erColor } = getVal(erStatus);
	const { label: serLabel, color: serColor } = getVal(serStatus);

	// let validComment = comments || [];

	/**
	 * @todo Confirm
	 *  */

	// if (comments) {
	//   if (erStatus === 'R') {
	//     const latest = comments
	//       .filter(row => row.userType === 'A')
	//       .sort((a, b) => new Date(b.commentDate) - new Date(a.commentDate));

	//     if (latest && latest.length > 0) {
	//       validComment.push(latest[0]);
	//     }
	//   }

	//   if (serStatus === 'R') {
	//     const latest = comments
	//       .filter(row => row.userType === 'B')
	//       .sort((a, b) => new Date(b.commentDate) - new Date(a.commentDate));

	//     if (latest && latest.length > 0) {
	//       validComment.push(latest[0]);
	//     }
	//   }
	// }

	return (
		<>
			<br />
			<Grid rows={2} divided style={{ marginTop: '10px' }}>
				<Grid.Row>
					<Segment.Group compact raised>
						<Segment>
							<h3>Approval Status</h3>
						</Segment>
						<Segment>
							<Label>Engineer </Label>
							<Label as="a" color={erColor}>
								{erLabel}
							</Label>
						</Segment>
						<Segment>
							<Label>Sub-Engineer </Label>
							<Label as="a" color={serColor}>
								{serLabel}
							</Label>
						</Segment>
					</Segment.Group>
				</Grid.Row>
				<Grid.Row>
					<ActionComments comment={comments} />
				</Grid.Row>
			</Grid>
		</>
	);
};

ApprovalStatusDisplay.propTypes = {
	erStatus: PropTypes.string.isRequired,
	serStatus: PropTypes.string.isRequired,
};

export default ApprovalStatusDisplay;

export const RajaswaApprovalStatusDisplay = ({ rStatus, status }) => {
	// console.log('erstatus', erStatus);
	const { label: rLabel, color: rColor } = getVal(rStatus);
	const { label: statusLabel, color: statusColor } = getVal(status);
	return (
		<div>
			<br />
			<Segment.Group compact raised>
				<Segment>
					<h4>Approval Status</h4>
				</Segment>
				<Segment>
					<Label>Rajaswa </Label>
					<Label as="a" color={rColor} tag>
						{rLabel}
					</Label>
				</Segment>
				<Segment>
					<Label>Engineer </Label>
					<Label as="a" color={statusColor} tag>
						{statusLabel}
					</Label>
				</Segment>
			</Segment.Group>
		</div>
	);
};

export const ApprovalStatus = ({ formUrl, statuses, isRejected }) => {
	const getStatus = () => {
		if (isRejected) {
			return (
				<span>
					Status <Icon color="red" name="warning circle" />
				</span>
			);
		} else {
			return <span>Status</span>;
		}
	};

	const statusBy = getFormMasterObj(formUrl);

	const renderStatus = (userType, statusCode, name, userDesignation) => {
		const { approvalFieldName, label, className, statusLabel, color, icon } = getApprovalStatusObject(userType, statusCode);

		if (statusBy.enterBy === userType || statusBy[approvalFieldName] === 'N') {
			return null;
		} else {
			return (
				<>
					{/* <Grid.Row textAlign="center"> */}
					{/* <Label as="a" className={className} image>
							{label} <div style={{ maxWidth: '10px'}}>{'ramedsdfsdfsdfsdfsdfsdf sdfsdfsdfsh'}</div> <Label color={color}>{statusLabel}</Label>
						</Label> */}
					<Table.Row>
						<Table.Cell>
							<Label className={className}>
								{name && !isStringEmpty(name) ? `${name} - ${userDesignation || label}` : userDesignation || label}
							</Label>
						</Table.Cell>
						<Table.Cell>
							<Label color={color}>
								<Icon name={icon} />
								{statusLabel}
							</Label>
						</Table.Cell>
					</Table.Row>
					{/* </Grid.Row> */}
				</>
			);
		}
	};

	return (
		<Popup
			trigger={getStatus()}
			// flowing
			hoverable
			flowing
			position="bottom center"
			// style={{ width: '450px' }}
			className="popups"
		>
			<Grid centered divided>
				<Table>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>{approvalStatus.user}</Table.HeaderCell>
							<Table.HeaderCell>{approvalStatus.status}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{statuses &&
							statuses.map(({ status, name, userType, userDesignation }) => (
								<React.Fragment key={`${userType}-${name}`}>{renderStatus(userType, status, name, userDesignation)}</React.Fragment>
							))}
					</Table.Body>
				</Table>
			</Grid>
		</Popup>
	);
};

export const ActionComments = ({ comments = [] }) => {
	if (comments.length > 0) {
		return (
			<Segment>
				<div className="section-header">
					<h3 className="left-align">Rejection Comments</h3>
				</div>

				<Feed>
					{comments.filter(row => row.remark !== "APPROVED_COMMENT").map((comment, idx) => (
						<Feed.Event key={idx}>
							<Feed.Label>
								<Icon name="warning circle" color="red" />
							</Feed.Label>
							<Feed.Content>
								<Feed.Summary>
									<div className="flex-item-space-between">
										<Feed.User>
											Rejected By: {comment.commentBy} {`(${comment.userTypeName})`}
										</Feed.User>
										<Feed.Date>
											<Icon name="calendar check outline" />
											मिति: {getNepaliDate(comment.commentDate.split(' ')[0])}{' '}
											{translateEngToNep(comment.commentDate.split(' ')[1])}
										</Feed.Date>
									</div>
								</Feed.Summary>
								<Feed.Extra content>
									<div>
										Comment <p>{comment.comment}</p>
									</div>
									<div>
										Remark: <p>{comment.remark}</p>
									</div>
								</Feed.Extra>
							</Feed.Content>
						</Feed.Event>
					))}
				</Feed>
			</Segment>
		);
	} else {
		return <Segment>No Comments Found.</Segment>;
	}
};
