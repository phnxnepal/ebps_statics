import React from 'react';
import { Button, Label, Icon, Modal } from 'semantic-ui-react';
import { isEmpty, getUserStatusNepali } from '../../utils/functionUtils';
import { saveButton } from '../../utils/data/genericData';
import SingleFileUpload from '../loggedInComponents/forms/SingleFileUpload';
import { connect } from 'react-redux';
import { getFileCategories } from '../../store/actions/fileActions';

class SaveButtonAdd extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			open: false
		};
	}

	componentDidMount() {
		// console.log('mount? ', this.props);
		if (isEmpty(this.props.fileCategories)) {
			// console.log('here ==');
			this.props.getFileCategories();
		}
	}

	handleSubmitModal = () => {
		this.handleClose();
		this.props.handleSubmit();
	};

	handleModalOpen = () => {
		this.setState({ open: true });
	};
	handleClose = () => {
		this.setState({ open: false });
	};
	render() {
		const { handleSubmit, errors, formName, fileCategories } = this.props;

		const { open } = this.state;

		return (
			<>
				<Modal
					key="save-confirmation"
					closeIcon
					open={open}
					onClose={this.handleClose}
				>
					<Modal.Header>{saveButton.uploadFileReminder}</Modal.Header>
					<Modal.Content scrolling>
						{/* <h3>{saveButton.uploadFileReminder}</h3> */}
						<SingleFileUpload
							url={'/user/forms/map-permit-application-edit'}
							fileCategories={fileCategories}
						/>
					</Modal.Content>
					<Modal.Actions>
						<Button negative onClick={this.handleClose}>
							Exit
						</Button>
						<Button
							positive
							icon="checkmark"
							labelPosition="right"
							content="Save Form"
							onClick={this.handleSubmitModal}
						/>
					</Modal.Actions>
				</Modal>
				<Button
					primary
					type="submit"
					onClick={() => {
						if (!isEmpty(errors)) {
							const form = document.forms[formName || 0];
							for (let i = 0; i < form.length; i++) {
								if (
									Object.keys(errors).some(errorField => {
										// console.log(
										// 	'onegai dakara',
										// 	errorField,
										// 	form[i].name.includes(errorField)
										// );
										/**
										 * @todo doesn't work when a field name is a substring of other.
										 */
										return form[i].name.includes(errorField);
									})
								) {
									// console.log('nanda?');

									form[i].focus();
									break;
								}
							}
						} else {
							this.handleModalOpen();
						}
					}}
				>
					Save
				</Button>
			</>
		);
	}
}

const mapDispatchToProps = {
	getFileCategories
};

const mapStateToProps = state => ({
	fileCategories: state.root.formData.fileCategories
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SaveButtonAdd);
