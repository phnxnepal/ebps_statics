import React from 'react';
import { Button } from 'semantic-ui-react';
import { SectionHeader } from '../uiComponents/Headers';

export class ErrorBoundary extends React.Component {
	constructor(props) {
		super(props);
		this.state = { hasError: false };
	}

	static getDerivedStateFromError(error) {
		// Update state so the next render will show the fallback UI.
		return { hasError: true };
	}

	componentDidCatch(error, errorInfo) {
		// You can also log the error to an <tab>error reporting service
		console.log(error, errorInfo);
	}

	render() {
		if (this.state.hasError) {
			if (!this.props.hideError) {
				// You can render any custom fallback UI
				return (
					<SectionHeader>
						<h3>
							केही प्राविधिक कठिनाइहरू छन्। कृपया{' '}
							<Button
								type="button"
								className="primary-btn"
								icon="refresh"
								size="tiny"
								content="Refresh"
								onClick={() => window.location.reload(false)}
							/>
							गर्नुहोस्.
						</h3>
					</SectionHeader>
				);
			} else return null;
		}

		return this.props.children;
	}
}
