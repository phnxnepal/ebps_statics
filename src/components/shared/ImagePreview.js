import React from 'react';
import IconPdf from '../../assets/images/pdf.svg';
import { getSplitUrl } from '../../utils/functionUtils';
import { getDocUrl } from '../../utils/config';

const ImagePreview = ({ fileUrl }) => {
	const pdfValidate = getSplitUrl(fileUrl, '.');
	const fileTitleName = getSplitUrl(fileUrl, '/');
	return (
		<div
			key={fileUrl}
			className={
				pdfValidate === 'pdf'
					? 'eachPreviewFile pdfFilePreview'
					: 'eachPreviewFile'
			}
			title={fileTitleName}
		>
			{pdfValidate === 'pdf' ? (
				<img src={IconPdf} alt="pdfIconFile" />
			) : (
				<img src={`${getDocUrl()}${fileUrl}`} alt="imageFile" />
			)}
		</div>
	);
};

export default ImagePreview;
