import React, { useState } from 'react';
import { Modal, Button, Loader, Dimmer } from 'semantic-ui-react';
import { showToast } from '../../utils/functionUtils';
import ErrorDisplay from './ErrorDisplay';
import { getClassWiseNextUrl } from '../../utils/urlUtils';
import { approveReject } from '../../utils/data/genericData';

const ApprovalModal = ({ open, onClose, successUrl, loadingModal, formUrl, history, content, title }) => {
	const [error, setError] = useState('');

	const handleClose = () => {
		onClose();
		setError('');
	};

	return (
		<div>
			<Modal key="approval-modal" closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{title ? title : approveReject.approve.title}</Modal.Header>
				{error && <ErrorDisplay message={error} />}
				<>
					<Dimmer active={loadingModal} inverted>
						<Loader active={loadingModal} inline="centered">
							Sending Approval...
						</Loader>
					</Dimmer>
					<>
						<Modal.Content scrolling>{content ? content : <h3>{approveReject.approve.content}</h3>}</Modal.Content>
						<Modal.Actions>
							<Button negative onClick={handleClose}>
								{approveReject.approve.close}
							</Button>
							<Button
								positive
								icon="checkmark"
								labelPosition="right"
								content={approveReject.approve.save}
								onClick={e => {
									successUrl()
										.then(res => {
											// console.log('res-- res', res);
											if (res.data && res.data.error) {
												setError(res.data.error.message);
											} else {
												showToast(res.data.message || 'Application approved.');
												handleClose();
												const nextUrl = getClassWiseNextUrl(formUrl);
												history.push(nextUrl);
											}
											// console.log('error', error);
										})
										.catch(err => {
											console.log('Message', err);
											setError('Network Error');
										});
								}}
							/>
						</Modal.Actions>
					</>
				</>
			</Modal>
		</div>
	);
};

export default ApprovalModal;
