import React, { useState } from 'react';
import { Form, Input, Label } from 'semantic-ui-react';
import IconPdf from '../../assets/images/pdf.svg';
import { Field } from 'formik';
import Viewer from 'react-viewer';
import { getDocUrl } from '../../utils/config';
import { ImageDisplayPreview } from './file/FileView';

const FileUploadInput = ({ hasMultipleFiles, fileCatId, error, setFieldValue, name }) => {
	return (
		<Form.Field error={!!error}>
			<Input
				type="file"
				name={name}
				id={fileCatId}
				multiple={hasMultipleFiles}
				onChange={e => {
					setFieldValue(name, Array.from(e.target.files));
				}}
			/>
			{error && (
				<Label pointing prompt size="large">
					{error}
				</Label>
			)}
		</Form.Field>
	);
};

FileUploadInput.propTypes = {};

export const FileInputWithPreview = ({ hasMultipleFiles, fileCatId, name }) => {
	// const [imageOpen, setimageOpen] = useState(false);
	return (
		<Field name={name}>
			{({ field, form }) => {
				const handleChange = e => {
					const arrFiles = Array.from(e.target.files);
					form.setFieldValue(name, arrFiles);
					const multipleFiles = arrFiles.map((file, index) => {
						const src = window.URL.createObjectURL(file);
						return { file, id: index, src };
					});

					form.setFieldValue('imagePreviewUrl', multipleFiles);
				};

				const { errors, values } = form;

				return (
					<Form.Field error={!!errors[field.name]}>
						<Input type="file" name={name} id={`file_${fileCatId}`} multiple={hasMultipleFiles} onChange={handleChange} />
						{errors[field.name] && (
							<Label pointing prompt size="large">
								{errors[field.name]}
							</Label>
						)}
						{values.imagePreviewUrl && (
							<div className="previewFileUpload">
								{values.imagePreviewUrl.map((value, index) => (
									<div key={index} className={'eachPreviewFile'}>
										<ImageDisplayPreview isInput={true} src={value.src} alt={value.file.name} type={value.file.type} title={value.file.name} />
									</div>
								))}
							</div>
						)}
					</Form.Field>
				);
			}}
		</Field>
	);
};

export const FileInputWithMultiplePreview = ({ hasMultipleFiles, fileCatId, name }) => {
	const [imageOpen, setimageOpen] = useState(false);
	return (
		<Field name={name}>
			{({ field, form }) => {
				const handleChange = e => {
					const arrFiles = Array.from(e.target.files);
					form.setFieldValue(name, arrFiles);
					const multipleFiles = arrFiles.map((file, index) => {
						const src = window.URL.createObjectURL(file);
						return { file, id: index, src };
					});

					form.setFieldValue(`imagePreviewUrl_${fileCatId}`, multipleFiles);
				};

				const { errors, values } = form;

				return (
					<Form.Field error={!!errors[field.name]}>
						<Input type="file" name={name} id={`file_${fileCatId}`} multiple={hasMultipleFiles} onChange={handleChange} />
						{errors[field.name] && (
							<Label pointing prompt size="large">
								{errors[field.name]}
							</Label>
						)}
						{values[`imagePreviewUrl_${fileCatId}`] && (
							<div className="previewFileUpload">
								{values[`imagePreviewUrl_${fileCatId}`].map((value, index) => (
									<div key={index} className={value.file.type === 'application/pdf' ? 'eachPreviewFile pdfFilePreview' : 'eachPreviewFile'}>
										{value.file.type === 'application/pdf' ? (
											<img src={IconPdf} alt="pdfIcon" title={value.file.name} />
										) : (
											<img src={value.src} alt="previewFile" title={value.file.name} onClick={() => setimageOpen(true)} />
										)}
										<Viewer
											visible={imageOpen}
											onClose={() => setimageOpen(false)}
											images={[{ src: `${getDocUrl()}${value.fileUrl}` }]}
											activeIndex={0}
											zIndex={10000}
										/>
									</div>
								))}
							</div>
						)}
					</Form.Field>
				);
			}}
		</Field>
	);
};

export default FileUploadInput;
