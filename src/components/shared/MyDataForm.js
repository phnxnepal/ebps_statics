import React from 'react';
import { Form, Label } from 'semantic-ui-react';
import { Field } from 'formik';

const MyDataForm = ({
  label,
  name,
  className,
  placeholder,
  error,
  validate
}) => {
  return (
    <Form.Group widths="equal">
      <Form.Field error={error}>
        <span>{label}</span>
        <Field
          name={name}
          className={className}
          validate={validate}
          placeholder={placeholder}
        />
        {error && (
          <Label pointing prompt size="large">
            {error}
          </Label>
        )}
      </Form.Field>
    </Form.Group>
  );
};

export default MyDataForm;
