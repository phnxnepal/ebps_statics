import React from 'react';
import { Form, Label } from 'semantic-ui-react';
import { useClassName } from '../../hooks/useClassName';
import { Field, getIn } from 'formik';
import { getClassName } from '../../utils/formUtils';

const TableInput = ({ error, className, ...rest }) => {
	return (
		<Form.Field error={!!error}>
			<input {...rest} className={className} />
			{error && (
				<Label pointing prompt size="small">
					{error}
				</Label>
			)}
		</Form.Field>
	);
};

export const TableInputField = ({ error, value, ...rest }) => {
	const className = useClassName(value, error);
	return (
		<Form.Field error={!!error} className="table">
			<Field {...rest} className={className} />
			{error && <span className="tableError">{error}</span>}
		</Form.Field>
	);
};

export const TableStyledInput = ({ error, value, ...rest }) => {
	const className = useClassName(value, error);
	return (
		<Form.Field error={!!error}>
			<input {...rest} value={value} className={className} />
			{error && (
				<Label pointing prompt size="small">
					{error}
				</Label>
			)}
		</Form.Field>
	);
};

export const TableInputIm = ({ name, label, sizeClass }) => {
	return (
		<Field>
			{({ form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);

				return (
					<Form.Field error={!!error} inline>
						{label && <label>{label}</label>}
						<Field name={name} className={getClassName(value, error) + ' ' + sizeClass} />
						{error && <span className="tableError">{error}</span>}
					</Form.Field>
				);
			}}
		</Field>
	);
};

export default TableInput;
