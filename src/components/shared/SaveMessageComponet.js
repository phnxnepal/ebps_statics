import React from 'react';
import { Segment, Message } from 'semantic-ui-react';

const SaveMessageComponet = props => {
  return (
    <Segment>
      <Message success compact>
        <Message.Header>Saved</Message.Header>
        <p>Data Sucessfully Saved</p>
      </Message>
    </Segment>
  );
};

export default SaveMessageComponet;
