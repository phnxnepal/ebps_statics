import { Formik } from "formik";
import React, { Component } from "react";
import { post, put } from "../../api/index";
// import { loginError, setSubmitted } from '../../../../ERP/src/Finance/store/action/loginForm';
import { connect } from "react-redux";

class Formikk extends Component {
  render() {
    return (
      <Formik
        initialValues={this.props.initial}
        enableReinitialize={true}
        // validate={values => {
        //   let errors = {};
        //   if (!values.email) {
        //     errors.email = 'Required';
        //   } else if (
        //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        //   ) {
        //     errors.email = 'Invalid email address';
        //   }
        //   return errors;
        // }}
        onSubmit={(values, { resetForm }) => {
          // console.log("value submit vako(from Formik)");
          // console.log(values);
          resetForm();
          if (this.props.apiMethod) {
            this.props.apiMethod == "post"
              ? post(
                this.props.api,
                this.props.filename,
                this.props.method,
                values,
                this.props.history
              )
              : put(
                this.props.api,
                this.props.filename,
                this.props.method,
                values,
                this.props.history
              );
          } else {
            post(
              this.props.api,
              this.props.filename,
              this.props.method,
              values,
              this.props.history
            );
          }

          this.props.setLoginError();
          this.props.setSubmitted();
        }}
      >
        {this.props.children}
      </Formik>
    );
  }
}

// @ts-ignore
const mapDispatchToProps = dispatch => {
  return {
    // @ts-ignore
    // setLoginError: () => dispatch(loginError()),
    // setSubmitted: () => dispatch(setSubmitted(true))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Formikk);
