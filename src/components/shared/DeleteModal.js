import React, { useState } from 'react';
import { Modal, Button, Loader, Dimmer } from 'semantic-ui-react';
import { showToast, showErrorToast } from '../../utils/functionUtils';
import ErrorDisplay from './ErrorDisplay';
import { approveReject } from '../../utils/data/genericData';
//import { deleteFile } from './../../store/actions/fileActions';

const DeleteModal = ({ open, onClose, loadingModal, content, title, deleteFile }) => {
	const [error, setError] = useState('');

	const handleClose = () => {
		onClose();
		setError('');
	};
	return (
		<div>
			<Modal key="approval-modal" closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{title ? title : approveReject.delete.title}</Modal.Header>
				{error && <ErrorDisplay message={error} />}
				<>
					<Dimmer active={loadingModal} inverted>
						<Loader active={loadingModal} inline="centered">
							Deleting Files...
						</Loader>
					</Dimmer>
					<>
						<Modal.Content scrolling>{content ? content : <h3>{approveReject.delete.content}</h3>}</Modal.Content>
						<Modal.Actions>
							<Button negative onClick={handleClose}>
								{approveReject.delete.close}
							</Button>
							<Button
								positive
								icon="checkmark"
								labelPosition="right"
								content={approveReject.delete.save}
								onClick={e => {
									deleteFile()
										.then(res => {
											// console.log('res-- res', res);
											if (res.data && res.data.message) {
												showToast(res.data.message);
												handleClose();
												// console.log('new');
											} else {
												showErrorToast('Invalid FileUrl');
											}
										})
										.catch(err => {
											showErrorToast(err);
											setError('Network Error');
										});
								}}
							/>
						</Modal.Actions>
					</>
				</>
			</Modal>
		</div>
	);
};

export default DeleteModal;
