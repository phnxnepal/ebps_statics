import React, { useState } from 'react';
import Cleave from 'cleave.js/react';
import { getInputType, translateNum } from '../../utils/langUtils';
import { isStringEmpty } from '../../utils/stringUtils';
import { Label, Form } from 'semantic-ui-react';

export const TimeField = ({
  inline,
  label,
  handleChange,
  value,
  error,
  setFieldValue,
  name
}) => {
  const [valueState, setValueState] = useState(value);
  const className = !isStringEmpty(valueState)
    ? error
      ? 'error'
      : 'success'
    : 'text-input';

  const handleValueChange = e => {
    const value1 = translateNum(e, getInputType());
    setValueState(value1);
    setFieldValue(name, value1);
  };

  if (value === null) {
    value = undefined;
  }
  return (
    <Form.Group widths='equal' className={inline ? 'inline-group' : null}>
      <Form.Field error={!!error}>
        {label && <label>{label}</label>}
        <Cleave
          name={name}
          className={`dashedForm-control  bpf-inputType ${className}`}
          value={value}
          onChange={setFieldValue ? handleValueChange : handleChange}
          placeholder='00:00'
          options={{
            // numericOnly: true,
            delimiter: 'ː',
            blocks: [2, 2]
          }}
        />
        {error && (
          <Label pointing='left' prompt size='large' basic color='red'>
            {error}
          </Label>
        )}
      </Form.Field>
    </Form.Group>
  );
};
