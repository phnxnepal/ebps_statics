import React from 'react'
import { Radio } from 'semantic-ui-react';


const MyRadioBox = ({ labelName, ...rest }) => {
    return ( 
        <Radio {...rest} label={labelName} />
    );
}
 
export default MyRadioBox;