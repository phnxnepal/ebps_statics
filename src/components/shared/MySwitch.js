import React, { Component } from 'react';
import Switch from 'react-switch';

class Switchh extends Component {
  handleChange = value => {
    // this is going to call setFieldValue and manually update values.topcis
    this.props.onChange(this.props.id, value);
  };

  render() {
    return (
      <Switch
        height={25}
        width={50}
        // onChange={this.props.onChange}
        onChange={this.handleChange}
        checked={this.props.checked}
      />
    );
  }
}

export default Switchh;
