import { FormattedMessage } from 'react-intl';
import React from 'react';

export const getMessage = (id, defaultMessage) => (
  <FormattedMessage id={id} defaultMessage={defaultMessage} />
);
