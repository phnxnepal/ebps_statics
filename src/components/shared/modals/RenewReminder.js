import React from 'react';
import { Modal, Button, Icon } from 'semantic-ui-react';
import { taskList } from '../../../utils/data/genericData';

export const RenewReminder = ({ renewModalOpen, setRenewModalOpen, daysRemaining }) => {
	return (
		<Modal className="renewModal" open={renewModalOpen} onClose={() => setRenewModalOpen(false)} style={{ width: '50%' }}>
			<Modal.Header>{taskList.renewalReminder.heading}</Modal.Header>
			<Modal.Content scrolling>
				<Icon name="warning circle" />
				<span>{`${taskList.renewalReminder.contentPrefix} ${daysRemaining} ${taskList.renewalReminder.contentSuffix}`}</span>
			</Modal.Content>
			<Modal.Actions>
				<Button
					primary
					// icon="checkmark"
					className="primaryButton"
					onClick={() => {
						setRenewModalOpen(false);
					}}
				>
					Ok
				</Button>
			</Modal.Actions>
		</Modal>
	);
};
