import React, { useState } from 'react';
import { Modal, Button, Form, Grid } from 'semantic-ui-react';
import { setLocalStorage } from '../../../utils/secureLS';
import { KEYBOARD, UNICODE, PREETI } from '../../../utils/constants';
import { getInputType } from '../../../utils/langUtils';

export const KeyboardSelectionModal = ({ langModalOpen, setLangModalOpen }) => {
	// const currentLang = useCallback(() => {
	// 	return getInputType();
	// }, [])
	const currentLang = getInputType();

	const [lang, setLang] = useState(currentLang);

	return (
		<div>
			<Modal open={langModalOpen} onClose={() => setLangModalOpen(false)} style={{ width: '50%' }}>
				<Modal.Header>Select Keyboard Layout</Modal.Header>
				<Modal.Content scrolling>
					<Grid centered>
						<Grid.Row>
							<Grid.Column width={8} verticalAlign="middle">
								<Form.Field>
									<div className="ui radio checkbox" onClick={() => setLang(UNICODE)}>
										<input type="radio" tabindex="0" value={UNICODE} checked={lang === UNICODE} />
										<label style={{ paddingTop: '2.5px', cursor: 'pointer' }}>Romanized</label>
									</div>
								</Form.Field>
							</Grid.Column>
							<Grid.Column width={8} verticalAlign="middle">
								<Form.Field>
									<div className="ui radio checkbox" onClick={() => setLang(PREETI)}>
										<input type="radio" tabindex="0" value={PREETI} checked={lang === PREETI} readOnly />
										<label style={{ paddingTop: '2.5px', cursor: 'pointer' }}>Traditional</label>
									</div>
								</Form.Field>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Modal.Content>
				<Modal.Actions>
					<Button
						primary
						// icon="checkmark"
						className="primaryButton"
						onClick={() => {
							setLocalStorage(KEYBOARD, lang);
							setLangModalOpen(false);
						}}
					>
						Ok
					</Button>
				</Modal.Actions>
			</Modal>
		</div>
	);
};
