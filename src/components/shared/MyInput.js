import React, { Component } from 'react';
import { Form, Input } from 'semantic-ui-react';
import MyPopup from './MyPopup';
import MySelect from './MySelect';

// const customStyles = {
//   control: () => ({
//     // none of react-select's styles are passed to <Control />
//     width: 200
//   })
// };

class MyInput extends Component {
  render() {
    return (
      <Form.Field required={this.props.required}>
        {this.props.tooltip && (
          <MyPopup content={this.props.tooltip}>
            <label>{this.props.label}</label>
          </MyPopup>
        )}
        {this.props.select ? (
          <MySelect
            id={this.props.id}
            options={this.props.options}
            value={this.props.value}
            onChange={this.props.onChange}
          />
        ) : (
          <Input
            id={this.props.id}
            icon={this.props.icon}
            placeholder={this.props.placeholder}
            value={this.props.value}
            onChange={this.props.onChange}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                e.preventDefault();
                // this.props.icon && click();
                document.getElementById(this.props.id + '1').click();
              }
            }}
          />
        )}
      </Form.Field>
    );
  }
}

export default MyInput;
