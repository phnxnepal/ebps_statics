import React from 'react';
import { Field, getIn } from 'formik';
import { Checkbox } from 'semantic-ui-react';

export default function FormikCheckbox(props) {
	return (
		<Field name={props.name}>
			{({ field, form }) => {
				if (!field.value) {
					field.value = [];
				}
				return (
					//   <label>
					<Checkbox
						style={props.space ? { paddingRight: '1em' } : undefined}
						label={props.value}
						type="checkbox"
						{...props}
						checked={field.value.includes(props.value)}
						onChange={() => {
							if (field.value.includes(props.value)) {
								const nextValue = field.value.filter(value => value !== props.value);
								form.setFieldValue(props.name, nextValue);
							} else {
								const nextValue = field.value.concat(props.value);
								form.setFieldValue(props.name, nextValue);
							}
						}}
					/>
					// {props.value}
					//   </label>
				);
			}}
		</Field>
	);
}

export const FormikCheckboxNoLabel = props => {
	return (
		<Field name={props.name}>
			{({ field, form }) => {
				if (!field.value) {
					field.value = [];
				}
				return (
					//   <label>
					<Checkbox
						// label={props.value}
						type="checkbox"
						{...props}
						checked={field.value.includes(props.value)}
						onChange={() => {
							if (field.value.includes(props.value)) {
								const nextValue = field.value.filter(value => value !== props.value);
								form.setFieldValue(props.name, nextValue);
							} else {
								const nextValue = field.value.concat(props.value);
								form.setFieldValue(props.name, nextValue);
							}
						}}
					/>
					// {props.value}
					//   </label>
				);
			}}
		</Field>
	);
};

export const FormikCheckboxIm = ({ name, label, space }) => {
	return (
		<Field name={name}>
			{({ field, form }) => {
				if (!field.value) {
					field.value = [];
				}
				const value = getIn(form.values, name);
				const error = getIn(form.errors, name);
				return (
					<>
						<Checkbox
							style={space ? { paddingRight: '1em' } : undefined}
							label={label}
							type="checkbox"
							checked={value && value.includes(label)}
							onChange={() => {
								if (value && value.includes(label)) {
									const nextValue =
										value &&
										Array.from(value.split(','))
											.filter(val => val !== label)
											.join(',');
									form.setFieldValue(name, nextValue);
								} else {
									const nextValue =
										value &&
										Array.from(value.split(','))
											.concat(label)
											.join(',');
									form.setFieldValue(name, nextValue);
								}
							}}
						/>
						{error && <span className="tableError">{error}</span>}
					</>
				);
			}}
		</Field>
	);
};
