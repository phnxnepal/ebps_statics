import React from 'react';
import { Form, Label, Dropdown } from 'semantic-ui-react';
import { useClassName } from '../../hooks/useClassName';
import { Field, getIn } from 'formik';
import { getClassName } from '../../utils/formUtils';
import { convertArea, convertLength } from '../../utils/mathUtils';
import { distanceOptions } from '../../utils/dataUtils';

const MultifieldUnitDropDown = ({ unitName, setFieldValue, values, errors, label, areaFields, relatedFields, options, brackets }) => {
	return (
		<div className={brackets ? 'brackets' : undefined} style={{ display: 'inline-flex' }}>
			{brackets && `( `}
			<Dropdown
				name={unitName}
				onChange={(e, { value }) => {
					const previousUnit = getIn(values, unitName);

					if (relatedFields && relatedFields.length > 0) {
						relatedFields.forEach((fieldName) => {
							// if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
							// 	getIn(values, fieldName) && setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
							// } else {
							getIn(values, fieldName) && setFieldValue(fieldName, convertLength(getIn(values, fieldName), value, previousUnit));
							// }
						});
					}
					if (areaFields && areaFields.length > 0) {
						areaFields.forEach((areafieldName) => {
							// if (fieldName.toUpperCase().includes('area'.toUpperCase()) || fieldName === 'plinthDetails') {
							// 	getIn(values, fieldName) && setFieldValue(fieldName, convertArea(getIn(values, fieldName), value, previousUnit));
							// } else {
							getIn(values, areafieldName) &&
								setFieldValue(areafieldName, convertArea(getIn(values, areafieldName), value, previousUnit));
							// }
						});
					}

					setFieldValue(unitName, value);
				}}
				value={getIn(values, unitName)}
				options={options ? options : distanceOptions}
			/>
			{brackets && ' ) '}
		</div>
	);
};
export default MultifieldUnitDropDown;
