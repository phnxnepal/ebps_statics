import React, { useState } from 'react';
import { Input, Label } from 'semantic-ui-react';
import { isStringEmpty } from '../../utils/stringUtils';
import { translate, getInputType, translateWithSlash } from '../../utils/langUtils';
import { Field, getIn } from 'formik';
// import { validateAdhikarValue } from '../loggedInComponents/formValidationSchemas/mapTechDesViewValidation';
// import { numberMessages } from '../../utils/data/validationData';

export default function DashedFormInput({ name, value, handleChange, error }) {
	return (
		<>
			<Input name={name} className="dashedForm-control" onChange={handleChange} value={value} />
			{error && (
				<Label pointing="left" prompt size="large" basic color="red">
					{error}
				</Label>
			)}
		</>
	);
}

export const DashedLangInput = ({ name, line, value, setFieldValue, handleChange, error, compact = false, ...rest }) => {
	const [valueState, setValueState] = useState(value);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleValueChange = (e) => {
		// As some bug is preventing the first letter from being set in value.
		const value1 = translate(e, getInputType());
		// const value1 = e.target.value
		setValueState(value1);
		setFieldValue(name, value1);
	};
	return (
		<>
			<Input
				{...rest}
				name={name}
				onChange={setFieldValue ? handleValueChange : handleChange}
				className={`dashedForm-control bpf-inputType ${className} ${line} ${compact ? 'compact-form-input' : undefined}`}
				value={valueState || value}
			/>
			{error && (
				<Label pointing="left" prompt size="large" basic color="red">
					{error}
				</Label>
			)}
		</>
	);
};

export const CompactDashedLangInput = ({ ...rest }) => {
	return <DashedLangInput compact={true} {...rest} />;
};

export const DashedLangInputWithSlash = ({ name, value, setFieldValue, handleChange, error, compact = false }) => {
	const [valueState, setValueState] = useState(value);
	const className = !isStringEmpty(valueState) ? (error ? 'error' : 'success') : 'text-input';

	const handleValueChange = (e) => {
		// As some bug is preventing the first letter from being set in value.
		const value1 = translateWithSlash(e, getInputType());
		// const value1 = e.target.value
		setValueState(value1);
		setFieldValue(name, value1);
	};
	return (
		<>
			<Input
				name={name}
				onChange={setFieldValue ? handleValueChange : handleChange}
				className={`dashedForm-control bpf-inputType ${className} ${compact ? 'compact-form-input' : undefined}`}
				value={valueState}
			/>
			{error && (
				<Label pointing="left" prompt size="large" basic color="red">
					{error}
				</Label>
			)}
		</>
	);
};

export const DashedNormalInput = ({ name, value, handleChange, error }) => {
	const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';

	return (
		<>
			<Input name={name} onChange={handleChange} className={`dashedForm-control bpf-inputType ${className}`} value={value} />
			{error && (
				<Label pointing="left" prompt size="small" basic color="red">
					{error}
				</Label>
			)}
		</>
	);
};

export const DashedNormalInputIm = ({ name, compact, ...rest }) => {
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);
				const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						<Input
							name={name}
							onChange={field.onChange}
							className={`dashedForm-control bpf-inputType ${className}  ${compact ? 'compact-form-input' : undefined}`}
							value={value}
							{...rest}
						/>
						{error && (
							<Label pointing="left" prompt size="small" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};

export const DashedReadOnlyField = ({ name }) => {
	return (
		<Field>
			{({ field, form }) => {
				const error = getIn(form.errors, name);
				const value = getIn(form.values, name);
				const className = !isStringEmpty(value) ? (error ? 'error' : 'success') : 'text-input';
				return (
					<>
						<Input name={name} readOnly={true} className={`dashedForm-control bpf-inputType ${className}`} value={value} />
						{error && (
							<Label pointing="left" prompt size="small" basic color="red">
								{error}
							</Label>
						)}
					</>
				);
			}}
		</Field>
	);
};
