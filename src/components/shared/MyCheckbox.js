import React from 'react';

const MyCheckBox = ({ name, labelName, Value, ...rest}) => {
    return ( 
        <div className="ui checkbox">
            <input {...rest} type="checkbox" name={name} value={Value}/>
            <label>{labelName}</label>
        </div>
    );
}
 
export default MyCheckBox;