import React, { Component } from 'react';
import mouseTrap from 'react-mousetrap';
import { connect } from 'react-redux';
import { setShortcut } from '../../store/actions/general';

class Hotkey extends Component {
  bindthem = (key, id) => {
    this.props.bindShortcut(key, () => {
      let ele = document.getElementsByClassName(id)[0];
      if (ele) ele.click();
    });
  };
  componentDidMount = () => {
    let { setShortcut } = this.props;

    this.bindthem('alt+a', 'approve');
    this.bindthem('alt+r', 'reject');
    this.bindthem('alt+u', 'Upload File');
    this.bindthem('alt+p', 'printt');
    this.bindthem('alt+h', 'History');
    this.bindthem('alt+c', 'Comments');
    this.bindthem('alt+r', 'View File');
    this.props.bindShortcut('f2', setShortcut);
  };
  render() {
    return <React.Fragment />;
  }
}
const mapStateToProps = state => ({
  shortcut: state.root.general.shortcut
});

const mapDispatchToProps = {
  setShortcut
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(mouseTrap(Hotkey));
