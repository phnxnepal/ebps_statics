import React, { Component } from 'react';
import { Icon, Feed, Message } from 'semantic-ui-react';
import { getNepaliDate } from '../../utils/dateUtils';
import { translateEngToNep } from '../../utils/langUtils';

class HistorySidebar extends Component {
	state = {
		isHistoryOpen: false,
	};

	handleHistorySidebar = () => {
		this.setState(prevState => ({
			isHistoryOpen: !prevState.isHistoryOpen,
		}));
	};

	render() {
		const { isHistoryOpen } = this.state;
		const { history: historyList = [] } = this.props;

		return (
			<div className="historyContent-wrap">
				<div className="historyTrigger" onClick={this.handleHistorySidebar}>
					<Icon name="history" size="large" />
				</div>
				{isHistoryOpen && (
					<div className="mainHistoryContent">
						<div className="innerHistoryContent">
							<HistoryContent historyList={historyList} />
						</div>
					</div>
				)}
			</div>
		);
	}
}

const historyData = {
	approved: 'स्वीकृत भएको',
	rejected: 'अस्वीकृत गरिएको',
};

const getHistoryData = status => {
	if (status === historyData.approved) return { color: 'green', icon: 'check' };
	else if (status === historyData.rejected) return { color: 'red', icon: 'close' };
	else return { color: 'purple', icon: 'save' };
};

export const HistoryContent = ({ historyList }) => (
	<>
		{historyList.length > 0 ? (
			<Feed>
				{historyList.map((history, index) => {
					const { color, icon } = getHistoryData(history.status || '');
					return (
						<Feed.Event key={index}>
							<Feed.Label>
								<Icon name="history" />
							</Feed.Label>
							<Feed.Content>
								<Feed.Summary>
									<Feed.User>
										Action By: {history.enterBy} {`(${history.userType})`}
									</Feed.User>
									<Feed.Date className="kalimati-feed-date">
										<b>
											<Icon name="calendar check outline" />
											मिति: {getNepaliDate(history.actionDate.split(' ')[0])} {/* <Icon name="wait" /> */}
											{/* समय :  */}
											{translateEngToNep(history.actionDate.split(' ')[1])}
										</b>
									</Feed.Date>
								</Feed.Summary>
								<Feed.Meta>
									Status:{' '}
									{history.status && (
										<>
											<span style={{ color: color }}>
												<Icon name={icon} color={color}></Icon>
												{history.status}
											</span>
										</>
									)
									// ) : (
									// 	<>
									// 		{history.status && history.status === 'अस्वीकृत गरिएको' ? (
									// 			<>
									// 				<Icon name="close" color="red"></Icon>
									// 				<span style={{ color: 'red' }}>{history.status}</span>
									// 			</>
									// 		) : (
									// 			<>
									// 				<Icon.Group>
									// 					<Icon name="file" color="purple" />
									// 					<Icon corner name="add" />
									// 				</Icon.Group>{' '}
									// 				<span style={{ color: 'purple' }}>{history.status}</span>
									// 			</>
									// 		)}
									// 	</>
									// )}
									}
									{/* <br /> */}
								</Feed.Meta>
							</Feed.Content>
						</Feed.Event>
					);
				})}
			</Feed>
		) : (
			<Message info>
				<p>No History Found.</p>
			</Message>
		)}
	</>
);

export default HistorySidebar;
