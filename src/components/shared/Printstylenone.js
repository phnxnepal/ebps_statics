import React, { Component } from 'react';

class Printstylenone extends Component {
  state = {
    view: false
  };
  render() {
    return (
      <div style={{ display: 'none' }} {...this.props}>
        {this.props.children}
      </div>
    );
  }
}

export default Printstylenone;
