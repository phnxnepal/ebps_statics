import React, { useState } from 'react';
import { Modal, Button, Loader, Dimmer, Form, Segment, Radio, Icon, Label } from 'semantic-ui-react';

import { Formik } from 'formik';
import * as Yup from 'yup';

import { showToast } from '../../utils/functionUtils';
import ErrorDisplay from './ErrorDisplay';
import { EbpsTextAreaMultiLang } from './EbpsForm';
import { validateString } from '../../utils/validationUtils';
import { approveReject } from '../../utils/data/genericData';

const validationSchema = Yup.object().shape({
	comment: validateString,
	remark: validateString,
});

const RejectionModal = ({ open, onClose, rejectionUrl, url, history, loadingModal }) => {
	const [error, setError] = useState('');
	const [isNepali, setIsNepali] = useState([false, false]);

	const handleClose = () => {
		setError('');
		onClose();
	};

	return (
		<div>
			<Modal key="rejection-modal" closeIcon open={open} onClose={handleClose}>
				<Modal.Header>{approveReject.reject.title}</Modal.Header>
				<>
					<Dimmer active={loadingModal} inverted>
						<Loader active={loadingModal} inline="centered">
							Sending Rejection...
						</Loader>
					</Dimmer>
					<Formik
						validationSchema={validationSchema}
						onSubmit={(values, actions) => {
							actions.setErrors({});
							actions.setSubmitting(true);
							values.status = 'R';

							rejectionUrl(url, values)
								.then((res) => {
									if (res.data && res.data.message) {
										showToast('Form rejection sent.');
										handleClose();
										history.push('/user/task-list');
										actions.setSubmitting(false);
									} else if (res.data && res.data.error) {
										setError(res.data.error.message);
									} else {
										setError(
											// 'Invalid user type. Only Sub-Engineer or Engineer can approve.'
											'Something went wrong.'
										);
									}
								})
								.catch((err) => {
									console.log('ERROR', err, typeof err);
									setError('Network Error');
									// actions.setErrors({ networkError: 'Network error.'});
								});

							actions.setSubmitting(false);
						}}
						render={({ handleSubmit, errors, setFieldValue, values, handleChange }) => {
							return (
								<Form loading={loadingModal}>
									{errors.networkError && <ErrorDisplay message={errors.networkError} />}
									{error && <ErrorDisplay message={error} />}
									<Modal.Content scrolling>
										<Segment>
											<h3>
												{/* <strong> */}
												{approveReject.reject.content}
												{/* </strong> */}
											</h3>
											<EbpsTextAreaMultiLang
												isNepali={isNepali[0]}
												name="comment"
												onChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.comment}
												error={errors.comment}
												label={approveReject.reject.comment}
												toggler={
													<Label as="a" className="lang-selector" onClick={() => setIsNepali((prevState) => [!prevState[0], prevState[1]])}>
														<Icon name="keyboard" />
														<span>{isNepali[0] ? 'ने' : 'en'}</span>
													</Label>
												}
												cols="100"
												rows="4"
												pointing="top"
											/>
											<EbpsTextAreaMultiLang
												isNepali={isNepali[1]}
												name="remark"
												onChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.remark}
												error={errors.remark}
												label={approveReject.reject.remarks}
												toggler={
													<Label as="a" className="lang-selector" onClick={() => setIsNepali((prevState) => [prevState[0], !prevState[1]])}>
														<Icon name="keyboard" />
														<span>{isNepali[1] ? 'ने' : 'en'}</span>
													</Label>
												}
												cols="100"
												rows="4"
												pointing="top"
											/>
										</Segment>
									</Modal.Content>
									<Modal.Actions>
										<Segment textAlign="right">
											<Button negative onClick={handleClose}>
												{approveReject.reject.close}
											</Button>
											<Button
												positive
												icon="checkmark"
												labelPosition="right"
												content={approveReject.reject.save}
												onClick={handleSubmit}
											/>
										</Segment>
									</Modal.Actions>
								</Form>
							);
						}}
					/>
				</>
			</Modal>
		</div>
	);
};

export default RejectionModal;
