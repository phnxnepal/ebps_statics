import React from 'react';
// import { isDesigner } from '../../utils/functionUtils';

const FormWrapper = ({ children, isFormDisabled, isVisible, hasPermission = true }) => {
	const isDisabled = !hasPermission || !isVisible || isFormDisabled;

	return (
		<fieldset
			className="borderlessFieldset"
			style={{
				pointerEvents: isDisabled ? 'none' : 'auto',
				opacity: isDisabled ? 0.99 : 1,
			}}
			disabled={isDisabled}
		>
			{children}
		</fieldset>
	);
};

export default FormWrapper;
