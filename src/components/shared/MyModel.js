import React, { Component } from 'react';
import { Modal } from 'semantic-ui-react';

export class RootModel extends Component {
  render() {
    return (
      <Modal
        centered={false}
        size="large"
        closeOnEscape={false}
        closeOnDimmerClick={false}
        open={this.props.open}
        onClose={this.props.close}
      >
        <Modal.Header>{this.props.header}</Modal.Header>

        <Modal.Content scrolling>
          <Modal.Description>{this.props.children}</Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}

export default RootModel;
