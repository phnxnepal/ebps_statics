import React from 'react';
import { Input, Label, Dropdown } from 'semantic-ui-react';
import { getIn } from 'formik';

export const LabeledInput = props => (
	<Input labelPosition={props.suffix ? 'right' : undefined}>
		{props.prefix && (
			<Label
				basic
				style={{
					whiteSpace: 'pre-wrap',
					wordWrap: 'break-word',
					width: '80px',
				}}
			>
				{props.prefix}
			</Label>
		)}
		<input style={{ width: '80px' }} name={props.name} onChange={props.handleChange} value={props.value} />
		{props.suffix && (
			<Label
				basic
				style={{
					whiteSpace: 'pre-wrap',
					wordWrap: 'break-word',
					width: '80px',
				}}
			>
				{props.suffix}
			</Label>
		)}
	</Input>
);

export const LabeledInputWithVariableUnit = React.memo(props => (
	<Input labelPosition={props.suffix ? 'right' : undefined}>
		{props.prefix && (
			<Label
				basic
				style={{
					whiteSpace: 'pre-wrap',
					wordWrap: 'break-word',
					width: '80px',
				}}
			>
				{props.prefix}
			</Label>
		)}
		<input style={{ width: '80px' }} className="english-div-field" name={props.name} onChange={props.handleChange} value={props.value} />
		{props.suffix && (
			<Label
				basic
				style={{
					whiteSpace: 'pre-wrap',
					wordWrap: 'break-word',
					width: '80px',
				}}
			>
				{props.suffix.includes(':') ? (
					<Dropdown
						name={props.unitField}
						onChange={(e, { value }) => props.setFieldValue(props.unitField, value)}
						value={getIn(props.values, props.unitField)}
						options={props.suffix.split(':').map(opt => {
							return { value: opt, text: opt };
						})}
					/>
				) : (
					props.suffix
				)}
			</Label>
		)}
	</Input>
));
