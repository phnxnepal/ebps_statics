import React, { Component } from 'react';
import { Loader, Dimmer } from 'semantic-ui-react';

export class Loading extends Component {
  render() {
    return (
      <div
        className="ok"
        style={{
          position: 'fixed',
          top: 0,
          left: 0,
          width: '100vw',
          height: '100vh',
          zIndex: 2000,
          display: this.props.loading ? 'block' : 'none'
        }}
      >
        <Dimmer active={this.props.loading}>
          <Loader inline="centered">
            <b>प्रिन्ट तयार भइरहेको छ। धैर्यवान हुनुहोस् {this.props.text}</b>
          </Loader>
        </Dimmer>
      </div>
    );
  }
}

export default Loading;
