import React, { useState } from 'react';
import { Icon, Button } from 'semantic-ui-react';
import { getSplitUrl } from '../../../utils/functionUtils';
import { getDocUrl } from '../../../utils/config';
import NoImage from '../../../assets/images/no-image.png';
import IconPdf from '../../../assets/images/pdf.svg';
import Viewer from 'react-viewer';
import {
	SUPPORTED_FORMATS_IMAGE,
	IMAGE,
	PDF,
	SUPPORTED_FORMATS_FILE,
	AUTOCAD,
	SUPPORTED_IMAGE_EXTENSTIONS,
} from '../../../utils/constants/fileConstants';

// let urlArr = [];

export const FileView = ({ el, deletable = true, width }) => {
	const [imageOpen, setImageOpen] = useState(false);

	const pdfValidate = getSplitUrl(el.fileUrl, '.');
	const fileTitleName = getSplitUrl(el.fileUrl, '/');
	return (
		<div
			key={el.fileUrl}
			className={pdfValidate === 'pdf' ? 'eachPreviewFile  viewFileSuccess pdfFilePreview' : 'eachPreviewFile viewFileSuccess'}
			title={fileTitleName}
		>
			{pdfValidate === 'pdf' ? (
				<>
					<a href={`${getDocUrl()}${el.fileUrl}`} target="_blank" rel="noopener noreferrer">
						<img src={IconPdf} alt="pdfIconFile" />
					</a>
					{deletable && (
						<Icon
							name="close"
							// corner="top right"
							className="closePdfFile"
							// onClick={() => this.handleDeleteModalOpen(el.id)}
						/>
					)}
				</>
			) : (
				<>
					<img src={`${getDocUrl()}${el.fileUrl}?${Date.now()}`} alt="imageFile" width={width} onClick={() => setImageOpen(true)} />
					{deletable && (
						<Icon
							name="close"
							className="closeImg"
							// onClick={() => this.handleDeleteModalOpen(el.id)}
						/>
					)}
				</>
			)}
			<Viewer
				visible={imageOpen}
				onClose={() => setImageOpen(false)}
				images={[{ src: `${getDocUrl()}${el.fileUrl}` }]}
				activeIndex={0}
				zIndex={10000}
			/>
		</div>
	);
};

export const FileViewThumbnail = ({ el, deletable = true }) => {
	const [imageOpen, setImageOpen] = useState(false);

	const pdfValidate = getSplitUrl(el.fileUrl, '.');
	const fileTitleName = getSplitUrl(el.fileUrl, '/');
	return (
		<div className="previewFileUpload">
			<div
				key={el.fileUrl}
				className={pdfValidate === 'pdf' ? 'eachPreviewFile  viewFileSuccess pdfFilePreview' : 'eachPreviewFile viewFileSuccess'}
				title={fileTitleName}
			>
				{pdfValidate === 'pdf' ? (
					<>
						<a href={`${getDocUrl()}${el.fileUrl}`} target="_blank" rel="noopener noreferrer">
							<img src={IconPdf} alt="pdfIconFile" />
						</a>
						{deletable && (
							<Icon
								name="close"
								// corner="top right"
								className="closePdfFile"
								// onClick={() => this.handleDeleteModalOpen(el.id)}
							/>
						)}
					</>
				) : (
					<>
						<img src={`${getDocUrl()}${el.fileUrl}`} alt="imageFile" onClick={() => setImageOpen(true)} />
						{deletable && (
							<Icon
								name="close"
								className="closeImg"
								// onClick={() => this.handleDeleteModalOpen(el.id)}
							/>
						)}
					</>
				)}
				<Viewer
					visible={imageOpen}
					onClose={() => setImageOpen(false)}
					images={[{ src: `${getDocUrl()}${el.fileUrl}` }]}
					activeIndex={0}
					zIndex={10000}
				/>
			</div>
		</div>
	);
};

export const FileViewText = ({ el, viewText }) => {
	const [imageOpen, setImageOpen] = useState(false);
	return (
		<>
			<Button inverted color="purple" icon="eye" size="tiny" onClick={() => setImageOpen(true)} title={viewText} />
			<Viewer
				visible={imageOpen}
				onClose={() => setImageOpen(false)}
				images={[{ src: `${getDocUrl()}${el.fileUrl}` }]}
				activeIndex={0}
				zIndex={10000}
			/>
		</>
	);
};

export const getFileType = url => {
	try {
		const ext = getSplitUrl(url, '.').toLowerCase();
		return SUPPORTED_IMAGE_EXTENSTIONS.includes(ext) ? IMAGE : ext === 'pdf' ? PDF : SUPPORTED_FORMATS_FILE.includes(ext) ? AUTOCAD : '';
	} catch {
		return '';
	}
};

export const ImageDisplay = ({ src, alt, width }) => {
	return (
		<img
			title={alt}
			width={width ? width : undefined}
			src={`${getDocUrl()}${src}`}
			onError={e => {
				e.target.onerror = null;
				e.target.src = NoImage;
			}}
			alt={alt}
		/>
	);
};

export const ImageDisplayInline = ({ src, label, alt, height = 80 }) => {
	return (
		<span style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
			{label}
			<img
				title={alt}
				height={height ? height : undefined}
				src={`${getDocUrl()}${src}`}
				onError={e => {
					e.target.onerror = null;
					e.target.src = NoImage;
					e.target.height = 30;
				}}
				style={{ paddingLeft: '5px' }}
				alt={alt}
			/>
		</span>
	);
};

export const ImageInlineNoLabel = ({ src, alt, height = 80 }) => {
	return (
		<img
			title={alt}
			height={height ? height : undefined}
			src={`${getDocUrl()}${src}`}
			onError={e => {
				e.target.onerror = null;
				e.target.src = NoImage;
				e.target.height = 30;
			}}
			style={{ paddingLeft: '5px' }}
			alt={alt}
		/>
	);
};

export const ImageDisplayWithViewer = ({ src, alt, type = undefined, width }) => {
	const [imageOpen, setImageOpen] = useState(false);
	const fileType = type || getFileType(src);

	if (fileType === IMAGE || SUPPORTED_FORMATS_IMAGE.includes(fileType)) {
		return (
			<>
				<img
					title={alt}
					width={width ? width : undefined}
					src={`${getDocUrl()}${src}`}
					onClick={() => setImageOpen(true)}
					onError={e => {
						e.target.onerror = null;
						e.target.src = NoImage;
					}}
					alt={alt}
				/>
				<Viewer visible={imageOpen} onClose={() => setImageOpen(false)} images={[{ src: `${getDocUrl()}${src}` }]} activeIndex={0} zIndex={10000} />
			</>
		);
	} else if (fileType === PDF || fileType === 'application/pdf') {
		return (
			<a href={`${getDocUrl()}${src}`} target="_blank" rel="noopener noreferrer">
				<img title={alt} className="pdf-preview" src={IconPdf} alt={alt} />
			</a>
		);
	} else {
		return (
			<a href={`${getDocUrl()}${src}`} target="_blank" rel="noopener noreferrer">
				{/* <img className="pdf-preview" src={IconPdf} alt={alt} /> */}
				<Icon style={{ fontSize: '2.5em' }} name="file" />
			</a>
		);
	}
};

export const ImageDisplayPreview = ({ src, alt, isInput, type, ...rest }) => {
	const fileType = type || getFileType(src);
	const fileSrc = isInput ? src : `${getDocUrl()}${src}`;

	if (fileType === IMAGE || SUPPORTED_FORMATS_IMAGE.includes(fileType)) {
		return (
			<img
				src={fileSrc}
				onError={e => {
					e.target.onerror = null;
					e.target.src = NoImage;
				}}
				{...rest}
				alt={alt}
			/>
		);
	} else if (fileType === PDF || fileType === 'application/pdf') {
		return (
			<a href={`${getDocUrl()}${src}`} target="_blank" rel="noopener noreferrer">
				<img className="pdf-preview" src={IconPdf} alt={alt} />
			</a>
		);
	} else {
		return (
			<a href={`${getDocUrl()}${src}`} target="_blank" rel="noopener noreferrer">
				{/* <img className="pdf-preview" src={IconPdf} alt={alt} /> */}
				<Icon style={{ fontSize: '2.5em' }} name="file" />
			</a>
		);
	}
};
