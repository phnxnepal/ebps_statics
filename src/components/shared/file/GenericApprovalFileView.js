import React from 'react';
import { ImageDisplayWithViewer } from './FileView';
import { Grid, Divider, List } from 'semantic-ui-react';
import { CompactInfoMessage } from '../formComponents/CompactInfoMessage';
import { fileData } from '../../../utils/data/genericData';

export const GenericApprovalFileView = ({ fileCategories, files, url }) => {
	const requiredCategories = fileCategories && fileCategories.filter(cat => cat.viewUrl === url);

	return requiredCategories && requiredCategories.length > 0 ? (
		requiredCategories.map((category, index) => {
			const requiredFiles = files && files.length > 0 ? files.filter(file => file.fileType.id === category.id) : [];
			return (
				<Grid key={index}>
					<Grid.Row centered>
						<Divider horizontal>{category.name}</Divider>
					</Grid.Row>
					<Grid.Row centered>
						<List relaxed>
							{requiredFiles.length > 0 ? (
								requiredFiles.map(el => (
									<React.Fragment key={el.fileUrl}>
										<ImageDisplayWithViewer width={900} src={el.fileUrl} alt={el.fileType.name} />
									</React.Fragment>
								))
							) : (
								<CompactInfoMessage content={fileData.noFilesUploaded} />
							)}
						</List>
					</Grid.Row>
				</Grid>
			);
		})
	) : (
		<Grid>
			<Grid.Row centered>
				<CompactInfoMessage content={fileData.noFileCategory} />
			</Grid.Row>
		</Grid>
	);
};
