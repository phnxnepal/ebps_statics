import React from 'react';
import { noticePaymentLang } from '../../../utils/data/noticePaymentApplicationLang';
import { FileView, FileViewThumbnail, ImageDisplayWithViewer } from './FileView';
import { Grid, Divider } from 'semantic-ui-react';
import { toArray } from '../../../utils/dataUtils';
import { CompactInfoMessage } from '../formComponents/CompactInfoMessage';

export const NoticePaymentFileView = ({ prevData }) => {
	const hasFile1 = prevData && prevData.fileUrl1 && Array.isArray(toArray(prevData.fileUrl1));
	const hasFile2 = prevData && prevData.fileUrl2;
	return (
		<div>
			<div style={{ pageBreakAfter: 'always' }}>
				<Grid>
					<Grid.Row centered>
						<Divider horizontal>{noticePaymentLang.fileTitle2}</Divider>
					</Grid.Row>
					<Grid.Row centered>
						{hasFile2 ? (
							<ImageDisplayWithViewer width={900} src={prevData.fileUrl2} alt={noticePaymentLang.fileTitle2} />
						) : (
							<CompactInfoMessage content={noticePaymentLang.noFilesUploaded} />
						)}
					</Grid.Row>
				</Grid>
			</div>
			<div className="remove-on-print">
				<Grid>
					<Grid.Row centered>
						<Divider horizontal>{noticePaymentLang.fileTitle}</Divider>
					</Grid.Row>
					<Grid.Row centered>
						{hasFile1 ? (
							toArray(prevData.fileUrl1).map((fl, index) => {
								return (
									<span style={{ marginRight: '10px' }}>
										<FileViewThumbnail key={index} el={{ fileUrl: fl }} deletable={false} />
									</span>
								);
							})
						) : (
							<CompactInfoMessage content={noticePaymentLang.noFilesUploaded} />
						)}
					</Grid.Row>
				</Grid>
			</div>
		</div>
	);
};

export const GenericFileView = ({ url, title, width = 500 }) => {
	return (
		<div>
			<Grid>
				<Grid.Row centered>
					<Divider horizontal>{title}</Divider>
				</Grid.Row>
				<Grid.Row centered>
					<FileView el={{ fileUrl: url }} deletable={false} width={width} />
				</Grid.Row>
			</Grid>
		</div>
	);
};
