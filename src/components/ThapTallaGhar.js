import React, { Component } from 'react';
import { Formik, getIn, Field } from 'formik';
import { Form, Segment, Table, TableCell, Message, Button } from 'semantic-ui-react';
import EbpsForm from './shared/EbpsForm';
import FormikCheckbox from './shared/FormikCheckbox';
import { buildingPermitApplicationForm } from '../utils/data/mockLangFile';
import { isEmpty, showToast, setBuildPermit, getUserInfoObj } from '../utils/functionUtils';

// Redux
import { connect } from 'react-redux';
import { postFormData } from '../store/actions/buildingPermitAction';
import { getFileCategories } from '../store/actions/fileActions';
import { setBuildPermitAdd as setPermit, getDirtyTaskList as getTaskList, getBuildPermitNew } from '../store/actions/dashboardActions';
import { LangDateField } from './shared/DateField';
import { LabelValue } from './shared/EbpsUnitLabelValue';
import { setLocalStorage, getLocalStorage } from '../utils/secureLS';
import { MENU_LIST, DEFAULT_UNIT_LENGTH as DefaultUnitLength, WARD_MASTER } from '../utils/constants';
import { getGroupName, floorMappingFlat, prepareMultiInitialValues } from '../utils/dataUtils';
import { landAreaTypeOptions, spouseTypeOptions, chaChainaNepaliOptions } from '../utils/optionUtils';
import { RadioInput } from './shared/formComponents/RadioInput';
import FallbackComponent from './shared/FallbackComponent';
import { FloorTable, FloorTableHeader } from './loggedInComponents/forms/mapPermitComponents/FloorTable';
import { ConstructionTypeRadio, ConstructionTypeRadioBiratnagar } from './loggedInComponents/forms/mapPermitComponents/ConstructionTypeRadio';
import { BuildingPermitKamalamaiSchema, BuildingPermitSchema } from './loggedInComponents/formValidationSchemas/buildPermitValidation';
import { getCurrentDate } from '../utils/dateUtils';
import MyMapComponent from './loggedInComponents/OpenStreamMap';
import { WardAddressSection } from './loggedInComponents/forms/mapPermitComponents/WardAddressSection';
import BuildPermitAddFileUpload from './loggedInComponents/forms/mapPermitComponents/BuildPermitAddFileUpload';
import { NibedakSection } from './loggedInComponents/forms/mapPermitComponents/NibedakSection';
import { PermitSubjectParagraph } from './loggedInComponents/forms/mapPermitComponents/PermitSubjectParagraph';
import { pronounOptions, BuildPermitHelper as Helper } from './loggedInComponents/forms/mapPermitComponents/BuildPermitHelpers';
import { addPermitPrompt } from '../utils/data/genericData';
import { Prompt } from 'react-router';
import { CommercialTable } from './loggedInComponents/forms/mapPermitComponents/CommercialTable';
import { isKamalamai, isBirtamod, isBiratnagar } from '../utils/clientUtils';
import { FlexSingleRight } from './uiComponents/FlexDivs';
import { mapData } from '../utils/data/mapPermit';
import { UNITS } from '../utils/enums/unit';
import { ApplicantDetails } from './loggedInComponents/forms/mapPermitComponents/ApplicantDetails';
import { LocationDetails } from './loggedInComponents/forms/mapPermitComponents/LocationDetails';
import { PermitSectionHeader } from './loggedInComponents/forms/mapPermitComponents/PermitSectionHeader';

const landPropsName = [
	{ key: 1, value: 'निजी', text: 'निजी' },
	{ key: 2, value: 'गुठी', text: 'गुठी' },
	{ key: 3, value: 'साझा', text: 'साझा' },
	{ key: 4, value: 'सरकारी', text: 'सरकारी' },
	{ key: 5, value: 'संयुक्त', text: 'संयुक्त' },
];

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

const DEFAULT_UNIT_LENGTH = isBirtamod ? UNITS.METRE.LENGTH : DefaultUnitLength;
const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
let initialValues = {
	buildingJoinRoadType: [permitFormLang.form_step2.checkBox_option.option_1],
	buildingJoinRoadTypeOther: '',
	purposeOfConstructionOther: '',
	constructionTypeOther: '',
	mohada: [permitFormLang.form_step5.checkBox_option3.option_1],
	constructionFinishingOther: '',
	foharArrangementOther: '',
	newMunicipal: getUserInfoObj() && getUserInfoObj().organization.name,
	applicantAddress: getUserInfoObj() && getUserInfoObj().organization.address,
	floor: [{ floorUnit: DEFAULT_UNIT_LENGTH }],
	dhalUnit: DEFAULT_UNIT_LENGTH,
	highTensionLineUnit: DEFAULT_UNIT_LENGTH,
	pipelineUnit: DEFAULT_UNIT_LENGTH,
	pipeline: permitFormLang.form_step8.checkBox_option.option_1,
	doPipelineConnection: permitFormLang.form_step8.checkBox_option2.option_1,
	isHighTensionLine: permitFormLang.form_step9.checkBox_option.option_1,
	isLowTensionLine: permitFormLang.form_step9.checkBox_option.option_1,
	surrounding: [
		{ side: 1, sideUnit: DEFAULT_UNIT_LENGTH },
		{ side: 2, sideUnit: DEFAULT_UNIT_LENGTH },
		{ side: 3, sideUnit: DEFAULT_UNIT_LENGTH },
		{ side: 4, sideUnit: DEFAULT_UNIT_LENGTH },
	],

	purposeOfConstruction: permitFormLang.form_step5.checkBox_option.option_1,
	constructionType: permitFormLang.form_step5.checkBox_option2[2].value,
	// mohada: permitFormLang.form_step5.checkBox_option3.option_1,
	constructionFinishing: permitFormLang.form_step5.checkBox_option4.option_1,
	dhalNikasArrangement: permitFormLang.form_step6.checkBox_option.option_1,
	foharArrangement: permitFormLang.form_step7.checkBox_option.option_1,
	// Dropdowns
	applicantMs: pronounOptions[0].value,
	ownershipName: landPropsName[0].value,
	landAreaType: landAreaTypeOptions[0].value,
	spouseType: spouseTypeOptions[0],
	kittaNo: [''],
	landArea: [''],
	landDetails: [''],
};

class ThapTallaGhar extends Component {
	state = {
		purposeOfConstruction: permitFormLang.form_step5.checkBox_option.option_1,
		constructionType: permitFormLang.form_step5.checkBox_option2[2].value,
		// mohada: permitFormLang.form_step5.checkBox_option3.option_1,
		constructionFinishing: permitFormLang.form_step5.checkBox_option4.option_1,
		dhalNikasArrangement: permitFormLang.form_step6.checkBox_option.option_1,
		foharArrangement: permitFormLang.form_step7.checkBox_option.option_1,
		pipeline: permitFormLang.form_step8.checkBox_option.option_1,
		doPipelineConnection: permitFormLang.form_step8.checkBox_option2.option_1,
		isHighTensionLine: permitFormLang.form_step9.checkBox_option.option_1,
		isLowTensionLine: permitFormLang.form_step9.checkBox_option.option_1,
		inputPurpOfConsFilter: false,
		inputConsFinishFilter: false,
		inputDhalArrgmntFilter: true,
		inputFoharArrgmntFilter: false,
		inputPipelineArrgmntFilter: true,
		inputElecArrgmntFilter: true,
		inputRoadTypeFilter: false,
		open: false,
		filesUploaded: false,
		saved: false,
		mapPosition: [27.700769, 85.30014],
		marker: { lat: 27.700769, lng: 85.30014 },
		savedPermitData: '',
		wardOptions: [],
		hasBlocks: chaChainaNepaliOptions[1].value,
	};

	componentDidMount() {
		const wardOptions = JSON.parse(getLocalStorage(WARD_MASTER)).map((ward) => {
			return { key: ward.id, value: ward.name, text: ward.name };
		});
		this.props.getFileCategories();
		const permitId = this.props.match.params.id;
		this.props.getBuildPermitNew(permitId);
		this.setState({ wardOptions });
	}

	handleMarkerClick = (e) => {
		this.setState({ marker: e.latlng });
	};

	handleFindOnMap = (lat, lng) => {
		if (!lat || !lng) {
			return;
		}
		this.setState({ marker: { lat, lng }, mapPosition: [lat, lng] });
	};

	handleModalOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	handleFilesUploaded = () => {
		this.setState({ filesUploaded: true });
	};

	render() {
		if (this.props.permitData && this.props.fileCategories) {
			// if (isStringEmpty(initialValues.applicantDate)) {
			// 	initialValues.applicantDate = getCurrentDate(true);
			// }

			// if (isStringEmpty(initialValues.newMunicipal)) {
			// 	initialValues.newMunicipal = getUserInfoObj() && getUserInfoObj().organization.name;
			// }

			let initVal = {};
			initVal = this.props.permitData;

			// Object.keys(initVal).forEach(key => {
			// 	if (initVal[key] === null || initVal[key] === undefined) {
			// 		initVal[key] = '';
			// 	}
			// });
			/**
			 * @todo refactor this code block
			 */
			if (!Array.isArray(this.props.permitData.buildingJoinRoadType)) {
				if (this.props.permitData.buildingJoinRoadType && this.props.permitData.buildingJoinRoadType.includes(',')) {
					initVal.buildingJoinRoadType = this.props.permitData.buildingJoinRoadType.split(',');
				} else if (initVal.buildingJoinRoadType) {
					initVal.buildingJoinRoadType = [this.props.permitData.buildingJoinRoadType];
				}
			}

			if (!Array.isArray(this.props.permitData.mohada)) {
				if (this.props.permitData.mohada && this.props.permitData.mohada.includes(',')) {
					initVal.mohada = this.props.permitData.mohada.split(',');
				} else if (initVal.mohada) {
					initVal.mohada = [this.props.permitData.mohada];
				}
			}

			if (!Array.isArray(this.props.permitData.kittaNo)) {
				if (this.props.permitData.kittaNo && this.props.permitData.kittaNo.includes(',')) {
					initVal.kittaNo = this.props.permitData.kittaNo.split(', ');
					initVal.landDetails = Array.apply(null, { length: initVal.kittaNo.length }).map(() => '');
				} else if (initVal.kittaNo) {
					initVal.kittaNo = [this.props.permitData.kittaNo];
					initVal.landDetails = [''];
				}
			}

			if (!Array.isArray(this.props.permitData.landArea)) {
				if (this.props.permitData.landArea && this.props.permitData.landArea.includes(',')) {
					initVal.landArea = this.props.permitData.landArea.split(', ');
				} else if (initVal.landArea) {
					initVal.landArea = [this.props.permitData.landArea];
				}
			}

			const floorClone = initVal.floor.slice() || [];

			floorMappingFlat.forEach((fl, index) => {
				if (initVal.floor && floorClone) {
					initVal.floor[index] = floorClone.find((fll) => {
						if (fll) {
							initVal.floorUnit = fll.floorUnit
							return parseInt(fll.floor) === index;
						} else {
							return false;
						}
					});
				}
			});

			const initialValues1 = prepareMultiInitialValues(
				{ obj: { lat: 27.700769, lng: 85.30014 }, reqFields: [] },
				{ obj: initialValues, reqFields: [] },
				{ obj: initVal, reqFields: [] },
				{ obj: { constructionType: permitFormLang.form_step5.checkBox_option2[2].value, applicantDate: getCurrentDate(true) }, reqFields: [] }
			);

			// const existingFloors = floorClone.map((floor) => floor && floor.floor) || [];

			return (
				<React.Fragment>
					<BuildPermitAddFileUpload
						fileCategories={this.props.fileCategories}
						savedPermitData={this.state.savedPermitData}
						open={this.state.open}
						success={this.props.success}
						handleClose={this.handleClose}
						history={this.props.history}
						handleFilesUploaded={this.handleFilesUploaded}
					/>
					<div className="information-bar">
						<Segment>
							<div className="infoBarTitle">
								<p>{permitFormLang.form_naksa}</p>
							</div>
						</Segment>
					</div>
					<div className="buildingPermit-applicationForm">
						<Formik
							initialValues={initialValues1}
							validateOnBlur
							validationSchema={isKamalamai ? BuildingPermitKamalamaiSchema : BuildingPermitSchema}
							onSubmit={(values, actions) => {
								actions.setSubmitting(true);

								const { filteredData } = Helper.formatEditPostValues(values, this.state.marker);

								filteredData.oldApplicationNo = this.props.permitData.id;

								this.props
									.postFormData(filteredData)
									.then((res) => {
										window.scroll(0, 0);
										if (!isEmpty(this.props.errors)) {
											actions.setErrors('Error');
										} else {
											showToast('Data for build permit saved successfully. Please upload the necessary files.');

											const buildingPermitObj = res.data.obj;

											setBuildPermit(buildingPermitObj);

											//------
											this.props
												.setPermit(buildingPermitObj)
												.then((response) => {
													const menu = response.data.menu;

													const groupName = getGroupName();
													menu.forEach((mn) => {
														if (mn.viewURL && mn.viewURL.trim() === '/user/forms/forward-to-next') {
															try {
																mn.formName = `${mn.formName} ${
																	groupName.find((grp) => String(grp.id) === String(mn.groupId)).name
																}`;
															} catch (err) {
																console.log('Unable to fetch group names master.', err);
															}
															// console.log('goutout', mn.formName);
														}
													});
													setLocalStorage(MENU_LIST, JSON.stringify(menu));
													this.props.getTaskList();
												})
												.catch((err) => {
													console.log(err);
												});

											this.setState({
												open: true,
												saved: true,
												savedPermitData: { ...buildingPermitObj, photo: this.props.permitData.photo },
											});

											// this.props.history.push('/');
											// console.log('Data updated', res);
										}
										actions.setSubmitting(false);
									})
									.catch((err) => {
										//   showToast(err)
										actions.setSubmitting(false);
									});
							}}
							render={(props) => {
								// const floorDetails = permitFormLang.form_step9.floor_details;
								const memberDetails = permitFormLang.form_step10.member_details;
								let memberIndex = 0;

								// const hasChanged = !deepEqual(props.values, initialValues);
								return (
									<Form
										// onSubmit={props.handleSubmit}
										loading={props.isSubmitting}
									>
										<Prompt
											when={this.state.saved && !this.state.filesUploaded}
											message={() => addPermitPrompt.confirmationMessage}
										/>
										{!isEmpty(this.props.errors) && (
											<Message negative>
												<Message.Header>Error</Message.Header>
												<p>{this.props.errors.message}</p>
											</Message>
										)}
										<PermitSubjectParagraph applicantMs={props.values.applicantMs} />
										{/* </p> */}
										<PermitSectionHeader content={permitFormLang.form_tapasil} />
										<ApplicantDetails setFieldValue={props.setFieldValue} errors={props.errors} values={props.values} />
										<WardAddressSection
											data={isBiratnagar ? permitFormLang.form_step2_biratnagar : permitFormLang.form_step2}
											newMunicipal={initVal.newMunicipal}
											setFieldValue={props.setFieldValue}
											errors={props.errors}
											values={props.values}
											wardOptions={this.state.wardOptions}
										/>
										{/* Section 3 and 4-- */}
										<LocationDetails
											setFieldValue={props.setFieldValue}
											errors={props.errors}
											values={props.values}
											landPropsName={landPropsName}
											handleChange={props.handleChange}
										/>
										{/* Section 5 --- */}
										<PermitSectionHeader content={permitFormLang.form_step5.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_1}</span>
											{Object.values(permitFormLang.form_step5.checkBox_option).map((option) => (
												<div className="ui radio checkbox" key={option}>
													<input
														type="radio"
														name="purposeOfConstruction"
														value={option}
														defaultChecked={props.values.purposeOfConstruction === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.purposeOfConstruction === permitFormLang.form_step5.checkBox_option.option_3 && (
												<EbpsForm
													name="purposeOfConstructionOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													error={props.errors.purposeOfConstructionOther}
												/>
											)}
										</div>
										<div className="frmCheckbox-wrap">
											{isBiratnagar ? (
												<ConstructionTypeRadioBiratnagar
													values={props.values}
													errors={props.errors}
													otherName="constructionTypeOther"
													setFieldValue={props.setFieldValue}
													fieldLabel={permitFormLang.form_step5.fieldName_2}
												/>
											) : (
												<ConstructionTypeRadio fieldLabel={permitFormLang.form_step5.fieldName_2} />
											)}
										</div>
										<LangDateField
											label={permitFormLang.form_step5.fieldName_3}
											name="oldMapDate"
											setFieldValue={props.setFieldValue}
											value={props.values.oldMapDate}
											error={props.errors.oldMapDate}
										/>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_4}</span>
											<FormikCheckbox
												name="mohada"
												labelName={permitFormLang.form_step5.checkBox_option3.option_1}
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_1}
												defaultChecked="checked"
											/>
											<FormikCheckbox
												name="mohada"
												labelName={permitFormLang.form_step5.checkBox_option3.option_2}
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_2}
											/>
											<FormikCheckbox
												name="mohada"
												labelName={permitFormLang.form_step5.checkBox_option3.option_3}
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_3}
											/>
											<FormikCheckbox
												name="mohada"
												labelName={permitFormLang.form_step5.checkBox_option3.option_4}
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_4}
											/>
											{/* {Object.values(
                      permitFormLang.form_step5.checkBox_option3
                    ).map(option => (
                      <div className='ui radio checkbox'>
                        <input
                          type='radio'
                          name='mohada'
                          value={option}
                          id={option}
                          defaultChecked={props.values.mohada === option}
                          onChange={props.handleChange}
                        />
                        <label>{option}</label>
                      </div>
                    ))} */}
										</div>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_5}</span>
											{Object.values(permitFormLang.form_step5.checkBox_option4).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="constructionFinishing"
														value={option}
														defaultChecked={props.values.constructionFinishing === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.constructionFinishing === permitFormLang.form_step5.checkBox_option4.option_6 && (
												<EbpsForm
													name="constructionFinishingOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													error={props.errors.constructionFinishingOther}
												/>
											)}
										</div>

										{/* Section 6 ---  */}
										<PermitSectionHeader content={permitFormLang.form_step6.heading} />
										<div className="frmCheckbox-wrap">
											{Object.values(permitFormLang.form_step6.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="dhalNikasArrangement"
														value={option}
														defaultChecked={props.values.dhalNikasArrangement === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.dhalNikasArrangement === permitFormLang.form_step6.checkBox_option.option_3 && (
												<div className="sewageMgmt">
													<span>{permitFormLang.form_step6.fieldName_1}</span>
													<LabelValue
														name="dhalNikasArrangementOther"
														onChange={props.handleChange}
														value={props.values.dhalNikasArrangementOther}
														setFieldValue={props.setFieldValue}
														options={distanceOptions}
														nameUnit="dhalUnit"
														unitvalue={props.values.dhalUnit}
													/>
												</div>
											)}
										</div>

										{/* Section 7 --- */}
										<PermitSectionHeader content={permitFormLang.form_step7.heading} />
										<div className="frmCheckbox-wrap">
											{Object.values(permitFormLang.form_step7.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="foharArrangement"
														value={option}
														defaultChecked={props.values.foharArrangement === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.foharArrangement === permitFormLang.form_step7.checkBox_option.option_3 && (
												<EbpsForm
													name="foharArrangementOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													error={props.errors.foharArrangementOther}
												/>
											)}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step8.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step8.fieldName_1}</span>
											{Object.values(permitFormLang.form_step8.checkBox_option).map((option) => (
												<RadioInput key={option} name="pipeline" option={option} />
											))}
										</div>
										{props.values.pipeline === permitFormLang.form_step8.checkBox_option.option_1 ? (
											<div className="pipelineMgmt">
												<span>{permitFormLang.form_step8.fieldName_2}</span>
												<LabelValue
													name="pipelineDistance"
													onChange={props.handleChange}
													value={props.values.pipelineDistance}
													setFieldValue={props.setFieldValue}
													options={distanceOptions}
													nameUnit="pipelineUnit"
													unitvalue={props.values['pipelineUnit']}
												/>
											</div>
										) : null}
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step8.fieldName_3}</span>
											{Object.values(permitFormLang.form_step8.checkBox_option2).map((option) => (
												<RadioInput key={option} name="doPipelineConnection" option={option} />
											))}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step9.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step9.fieldName_1}</span>
											{Object.values(permitFormLang.form_step9.checkBox_option).map((option) => (
												<RadioInput key={option} name="isHighTensionLine" option={option} />
											))}
										</div>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step9.fieldName_2}</span>
											{Object.values(permitFormLang.form_step9.checkBox_option).map((option) => (
												<RadioInput key={option} name="isLowTensionLine" option={option} />
											))}
										</div>
										{/* {this.state.inputElecArrgmntFilter === true ? ( */}
										<div className="elecMgmt">
											<span>{permitFormLang.form_step9.fieldName_3}</span>
											<LabelValue
												name="isHighTensionLineDistance"
												onChange={props.handleChange}
												value={props.values.isHighTensionLineDistance}
												setFieldValue={props.setFieldValue}
												options={distanceOptions}
												nameUnit="highTensionLineUnit"
												unitvalue={props.values['highTensionLineUnit']}
											/>
										</div>
										{/* ) : null} */}
										<div>
											<b>{permitFormLang.form_tallaBibarab}</b>
										</div>
										<div style={{ marginLeft: '30px' }}>
											<div>
												<b>
													{permitFormLang.form_step9.floor_details.block}
													{chaChainaNepaliOptions.map((option) => (
														<React.Fragment key={option.key}>
															<div className={`ui radio checkbox ${'prabidhik'}`}>
																<input
																	type="radio"
																	value={option.value}
																	checked={this.state.hasBlocks === option.value}
																	onChange={(e) => {
																		props.setFieldValue('floor', [])
																		this.setState({ hasBlocks: option.value });
																	}}
																/>
																<label>{option.value}</label>
															</div>
														</React.Fragment>
													))}
												</b>
											</div>
											{/* {props.values.purposeOfConstruction === permitFormLang.form_step5.checkBox_option.option_2 ? ( */}
											{this.state.hasBlocks === chaChainaNepaliOptions[0].value ? (
												<CommercialTable
													setFieldValue={props.setFieldValue}
													handleChange={props.handleChange}
													values={props.values}
													errors={props.errors}
												/>
											) : (
												<Table celled compact collapsing striped style={{ maxWidth: '1000px' }}>
													<FloorTableHeader
														errors={props.errors}
														values={props.values}
														setFieldValue={props.setFieldValue}
													/>
													<FloorTable {...props} />
												</Table>
											)}
										</div>
										<div>
											<PermitSectionHeader content={permitFormLang.form_step10.heading} />
											{props.errors.member && <span className="tableError">{props.errors.member}</span>}
											<div style={{ marginLeft: '30px' }} className={`field ${props.errors.member ? 'error' : ''}`}>
												<Table celled compact collapsing>
													<Table.Header>
														<Table.Row textAlign="center">
															{Object.keys(memberDetails.table_heading).map((key) => (
																<Table.HeaderCell key={key}>{memberDetails.table_heading[key]}</Table.HeaderCell>
															))}
														</Table.Row>
													</Table.Header>
													<Table.Body>
														{Object.keys(memberDetails.table_subheading).map((key) => {
															memberIndex++;
															return (
																<Table.Row key={key}>
																	<TableCell>
																		<label htmlFor={`member[${memberIndex - 1}].member`}>
																			{memberDetails.table_subheading[key]}
																		</label>
																		<input
																			id={`member[${memberIndex - 1}].member`}
																			name={`member[${memberIndex - 1}].member`}
																			hidden
																			value={`member[${memberIndex - 1}].member`}
																		/>
																	</TableCell>
																	<TableCell>
																		<EbpsForm
																			style={{ width: '180px' }}
																			name={`member[${memberIndex - 1}].memberName`}
																			setFieldValue={props.setFieldValue}
																			value={getIn(props.values, `member[${memberIndex - 1}].memberName`)}
																			error={getIn(props.errors, `member[${memberIndex}].memberName`)}
																		/>
																	</TableCell>
																	<TableCell>
																		<EbpsForm
																			style={{ width: '180px' }}
																			name={`member[${memberIndex - 1}].relation`}
																			setFieldValue={props.setFieldValue}
																			value={getIn(props.values, `member[${memberIndex - 1}].relation`)}
																			error={getIn(props.errors, `member[${memberIndex}].relation`)}
																		/>
																	</TableCell>
																</Table.Row>
															);
														})}
													</Table.Body>
												</Table>
											</div>
										</div>
										<NibedakSection
											setFieldValue={props.setFieldValue}
											values={props.values}
											errors={props.errors}
											wardOptions={this.state.wardOptions}
										/>
										<FlexSingleRight>
											<Form.Group inline style={{ paddingBottom: 10, paddingTop: 10 }}>
												<Form.Field>
													<label>
														<b>Latitude</b>
													</label>
													<Field name="lat" type="number" />
												</Form.Field>
												<Form.Field>
													<label>
														<b>Longitude</b>
													</label>
													<Field name="lng" type="number" />
												</Form.Field>
												<Form.Button
													primary
													icon="search"
													content={mapData.find}
													onClick={this.handleFindOnMap.bind(this, props.values.lat, props.values.lng)}
												/>
											</Form.Group>
										</FlexSingleRight>
										<MyMapComponent
											handleMarkerClick={(e) => {
												if (e.latlng) {
													props.setFieldValue('lat', e.latlng.lat);
													props.setFieldValue('lng', e.latlng.lng);
													this.handleMarkerClick(e);
												}
											}}
											marker={this.state.marker}
											position={this.state.mapPosition}
										/>
										<Button primary content="Save" disabled={this.state.saved} onClick={props.handleSubmit} />
										{this.state.saved && (
											<Button primary basic onClick={this.handleModalOpen}>
												UploadFile
											</Button>
										)}
									</Form>
								);
							}}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	postFormData,
	setPermit,
	getTaskList,
	getFileCategories,
	getBuildPermitNew,
};
const mapStateToProps = (state) => {
	return {
		fileCategories: state.root.formData.fileCategories,
		permitData: state.root.formData.permitData,
		success: state.root.formData.success,
		errors: state.root.ui.errors,
		loading: state.root.ui.loading,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(ThapTallaGhar);
