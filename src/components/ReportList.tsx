import * as React from 'react';
import { Header, Button } from 'semantic-ui-react';
import { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
import { connect } from 'react-redux';
import FallbackComponent from './shared/FallbackComponent';
import ErrorDisplay from './shared/ErrorDisplay';
import { getReportPermits } from './../store/actions/ReportActions';
import RootHistoryGrid from './loggedInComponents/Grid/RootHistoryGrid';
import { isEmpty, getUserTypeValueNepali } from './../utils/functionUtils';
import { setPrint } from '../store/actions/general';
import { UIState } from '../interfaces/ReduxInterfaces';
import JqxGrid from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid/react_jqxgrid';
import { getInputType, translate } from '../utils/langUtils';
import { TableWidths } from '../utils/enums/ui';
import { getOptionText } from '../utils/dataUtils';
import { historyDatafield, discardHistory } from '../utils/data/ebps-setup/discardSetupData';
import Helmet from 'react-helmet';
import { getFormMasterById } from '../utils/urlUtils';
import { constructionTypeSelectOptions, applicationStatusReportOptions } from '../utils/optionUtils';
import { getReportMapping, getReportKey } from '../utils/reportUtils';

interface IState {
	id: string;
	reportData: IReportState;
	pageable: boolean;
	groupable: boolean;
	showfilterrow: boolean;
	isDiscard: boolean;
}
interface IReportState {
	id: number;
	name: string;
	value: [];
}

interface IComponentProps {
	reportPermits: {};
	getReportPermits(fy: string, wardNo: string, constructionType: string): void;
}

class ReportList extends React.PureComponent<IDataTableProps & UIState & IComponentProps, IState> {
	private tableRef = React.createRef<JqxGrid>();

	constructor(props: IDataTableProps & UIState & IComponentProps) {
		super(props);

		this.state = {
			id: '',
			reportData: { id: 0, name: '', value: [] },
			pageable: true,
			groupable: true,
			showfilterrow: true,
			isDiscard: false,
		};

		this.handlePrint = this.handlePrint.bind(this);
	}

	componentDidMount() {
		document.title = `${document.title} - Report List`;

		//@ts-ignore
		let id = this.props.match.params.id;
		//@ts-ignore
		let fyear = this.props.match.params.code || 7677;
		//@ts-ignore
		let wardNo = this.props.match.params.wardNo || '';
		//@ts-ignore
		let constructionType = this.props.match.params.constructionType || '';

		this.setState({ id });

		if (isEmpty(this.props.reportPermits)) {
			this.props.getReportPermits(fyear, wardNo === 'allWards' ? '' : wardNo, constructionType === 'all' ? '' : constructionType);
		} else {
			const reportKey = getReportKey(parseInt(id));
			const requiredRow = Object.entries(this.props.reportPermits).find(row => {
				return row[0] === reportKey;
			});

			if (requiredRow) {
				const { label } = getReportMapping(requiredRow[0]);
				//@ts-ignore
				this.setState({
					reportData: {
						id: id,
						name: label,
						value: requiredRow[1],
					},
					isDiscard: requiredRow[0] === 'discard',
				});
			}
			const filterFields = document.getElementsByClassName('jqx-filter-input');

			for (var i = 1; i <= filterFields.length; i++) {
				if (filterFields[i]) {
					filterFields[i].addEventListener('input', function() {
						//@ts-ignore
						this.value = translate(this.value, getInputType());
					});
				}
			}
		}
	}

	//@ts-ignore
	componentDidUpdate(prevProps) {
		if (this.props.reportPermits && this.props.reportPermits !== prevProps.reportPermits) {
			const reportKey = getReportKey(parseInt(this.state.id));

			const requiredRow = Object.entries(this.props.reportPermits).find(row => {
				return row[0] === reportKey;
			});

			if (requiredRow) {
				const { label } = getReportMapping(requiredRow[0]);

				//@ts-ignore
				this.setState({
					reportData: {
						id: this.state.id,
						name: label,
						value: requiredRow[1],
					},
					isDiscard: requiredRow[0] === 'discard',
				});
			}
		}

		const filterFields = document.getElementsByClassName('jqx-filter-input');

		for (var i = 1; i <= filterFields.length; i++) {
			if (filterFields[i]) {
				filterFields[i].addEventListener('input', function() {
					//@ts-ignore
					this.value = translate(this.value, getInputType());
				});
			}
		}
	}

	componentWillUnmount() {
		const filterFields = document.getElementsByClassName('jqx-filter-input');
		for (var i = 1; i <= filterFields.length; i++) {
			if (filterFields[i]) {
				filterFields[i].removeEventListener('input', function() {
					//@ts-ignore
					this.value = translate(this.value, getInputType());
				});
			}
		}
		document.title = 'EBPS';
	}

	setPrintContent = (value: any) => {
		setPrint(value);
	};

	datafield = [
		{ name: 'applicantNo', type: 'string' },
		{ name: 'applicantName', type: 'string' },
		{ name: 'applicantAddress', type: 'string' },
		{ name: 'nibedakName', type: 'string' },
		{ name: 'applicantMobileNo', type: 'string' },
		{ name: 'applicationStatus', type: 'string' },
		{ name: 'applicationActionBy', type: 'string' },
		//{ name: 'yourStatus', type: 'string' },
		{ name: 'applicationFormName', type: 'string' },
		{ name: 'applicantDate', type: 'string' },
		{ name: 'constructionType', type: 'string' },
		//{ name: 'forwardTo', type: 'string' },
	];

	columns = [
		{
			text: 'शाखा दर्ता नं.',
			dataField: 'applicantNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_NO,
			pinned: 'true',
		},
		{
			text: 'जग्गाधनीको नाम',
			dataField: 'applicantName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			pinned: 'true',
		},

		{
			text: 'जग्गाधनीको ठेगाना',
			dataField: 'applicantAddress',
			cellsalign: 'center',
			align: 'center',
			width: '150',
		},
		{
			text: 'निवेदकको नाम',
			dataField: 'nibedakName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
		},
		{
			text: 'सम्पर्क फोन.',
			dataField: 'applicantMobileNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.MOBILE_NO,
		},
		{
			text: 'तह',
			dataField: 'applicationActionBy',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_ACTION_BY,
			filterType: 'list',
		},
		{
			text: 'स्थिति फारम',
			dataField: 'applicationFormName',
			cellsalign: 'center',
			align: 'center',
			filterType: 'list',
			// width: '300',
			// cellclassname: 'alignment',
			// cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
		},
		{
			text: 'अवस्था',
			dataField: 'applicationStatus',
			cellsalign: 'center',
			align: 'center',
			width: '120',
		},
		{
			text: 'दर्ता मिती.',
			dataField: 'applicantDate',
			cellsalign: 'center',
			align: 'center',
			width: '100',
		},
		{
			text: 'भवनको किसिम',
			dataField: 'constructionType',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.CONSTRUCTION_TYPE,
			pinned: 'true',
			filterType: 'list',
		},
		// {
		// 	text: 'तह स्थिति',
		// 	dataField: 'forwardTo',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: '250',
		// },
		// {
		// 	text: 'पूर्ण अवस्था',
		// 	dataField: 'yourStatus',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: '120',
		// },
	];

	private handlePrint(title: string) {
		const gridContent = this.tableRef.current!.exportdata('html');
		const newWindow = window.open('', '', 'width=800, height=500');
		const document = newWindow!.document.open();
		const pageContent =
			'<!DOCTYPE html>\n' +
			'<html>\n' +
			'<head>\n' +
			'<meta charset="utf-8" />\n' +
			`<title>${title} रिपोर्ट</title>\n` +
			'</head>\n' +
			'<body>\n' +
			'<h3>' +
			title +
			' रिपोर्ट</h3>' +
			gridContent +
			'\n</body>\n</html>';
		document.write(pageContent);
		document.close();
		newWindow!.print();
	}

	render() {
		//@ts-ignore
		const { reportPermits } = this.props;

		//@ts-ignore
		let Mdata = { ...this.state.reportData } || [];
		let Id = this.state.id;

		if (reportPermits) {
			//@ts-ignore
			// const Mdata = Object.keys(reportPermits).map((m: any, index) => {
			// 	return {
			// 		id: index,
			// 		Name: m,
			// 		value: reportPermits[m],
			// 	};
			// });

			let Maindata = Mdata;

			//@ts-ignore
			Maindata.value = Maindata.value.map((row: any) => {
				return {
					...row,
					//@ts-ignore
					applicationFormName: getFormMasterById(row.applicationAction).name,
					applicationActionBy: getUserTypeValueNepali(row.applicationActionBy),
					constructionType: getOptionText(row.constructionType, constructionTypeSelectOptions),
					applicationStatus: getOptionText(row.applicationStatus, applicationStatusReportOptions),
				};
			});

			return (
				<React.Fragment>
					<Helmet title={Maindata.name} />
					{// for props.errors
					// @ts-ignore
					this.props.errors && (
						//@ts-ignore
						<ErrorDisplay message={this.props.errors.message} />
					)}
					<div className="mapRegistration-dataTableList">
						{/* 
                    		//@ts-ignore */}
						<div ref={r => (this.RPrint = r)}>
							<Header>
								{/* <Header.Content>{Maindata && Maindata.name}</Header.Content> */}
								<Header.Content>{Maindata && Maindata.name}</Header.Content>
							</Header>
							<div className="pointer jqx-tables">
								{/* 
              				// @ts-ignore */}
								{this.state.isDiscard ? (
									// @ts-ignore
									<RootHistoryGrid
										//@ts-ignore
										Grid={this.tableRef}
										//@ts-ignore
										localdata={Maindata.value}
										//@ts-ignore
										source={historyDatafield}
										columns={discardHistory}
										// groups={false}
										groupable={false}
										rowsheight={40}
										// pageable={this.state.pageable}
										showfilterrow={true}
										onRowselect={false}
									/>
								) : (
									// @ts-ignore
									<RootHistoryGrid
										//@ts-ignore
										Grid={this.tableRef}
										//@ts-ignore
										localdata={Maindata.value}
										datafield={this.datafield}
										columns={this.columns}
										// groups={false}
										groupable={false}
										rowsheight={40}
										// pageable={this.state.pageable}
										showfilterrow={true}
										onRowselect={false}
									/>
								)}
							</div>
						</div>
						<br />

						<Button color="violet" onClick={() => this.handlePrint(Maindata ? Maindata.name : '')}>
							Print
						</Button>
						{/* <Printthis
							ui={
								<div
									style={{
										cursor: 'pointer',
										display: 'inline-block',
									}}
								>
									<Button color="violet">Print</Button>
								</div>
							}
							//@ts-ignore
							content={() => this.RPrint}
							onBeforeGetContent={() => {
								this.setState({ pageable: false, groupable: false, showfilterrow: false }, () => {
									//@ts-ignore
									this.RPrint = this.RPrint.cloneNode(true);
								});

								//@ts-ignore
								brack(this.RPrint);

								makeprintformat(
									//@ts-ignore
									this.RPrint,
									'15dayspecial'
								);
							}}
							onAfterPrint={() => this.setState({ pageable: true, groupable: true, showfilterrow: true })}
						/> */}
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getReportPermits,
	setPrint: (res: any) => setPrint(res),
};
//@ts-ignore
const mapStateToProps = state => ({
	reportPermits: state.root.dashboard.reportPermits,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});
export default connect(mapStateToProps, mapDispatchToProps)(ReportList);
