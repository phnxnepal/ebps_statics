import React, { useState } from 'react';
import { Accordion, Icon, Dropdown } from 'semantic-ui-react';
import { MENU_GROUPS } from '../../utils/menuUtils';
import { getMessage } from '../shared/getMessage';
import { headfoot } from '../../utils/data/headFootLangFile';

const hf_data = headfoot.headfoot_data;
const messageId = 'headfoot.headfoot_data';

export const ProcessSidebarMenu = ({ menuItems }) => {
	const [activeIndex, setActiveIndex] = useState(-1);
	const [processActiveIndex, setProcessActiveIndex] = useState(-1);

	const handleClick = (e, titleProps) => {
		const { index } = titleProps;
		const newIndex = activeIndex === index ? -1 : index;

		setActiveIndex(newIndex);
	};

	const handleProcessClick = (e, titleProps) => {
		const { index } = titleProps;
		const newIndex = processActiveIndex === index ? -1 : index;

		setProcessActiveIndex(newIndex);
	};
	return (
		<Accordion inverted>
			<Accordion.Title active={activeIndex === 0} index={0} onClick={handleClick}>
				<span>
					<Icon name="tasks" style={{ marginRight: '5px' }} />
					प्रक्रिया
					<Icon name="dropdown" />
				</span>
			</Accordion.Title>
			<Accordion.Content active={activeIndex === 0}>
				{menuItems[MENU_GROUPS.NAYA_NIRMAN] && menuItems[MENU_GROUPS.NAYA_NIRMAN].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 0} index={0} onClick={handleProcessClick}>
							<span>
								<Icon name="add" />
								{getMessage(`${messageId}.header.nayaDarta`, hf_data.header.nayaDarta)}
								<Icon name="dropdown" />
							</span>
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 0}>
							{/* <Dropdown item pointing="left"> */}
							<Dropdown.Menu>
								{menuItems[MENU_GROUPS.NAYA_NIRMAN].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
							{/* </Dropdown> */}
						</Accordion.Content>
					</>
				)}
				{menuItems[MENU_GROUPS.PURANO_GHAR] && menuItems[MENU_GROUPS.PURANO_GHAR].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 1} index={1} onClick={handleProcessClick}>
							<span>
								<Icon name="edit" />
								{getMessage(`${messageId}.header.puranoDarta`, hf_data.header.puranoDarta)}
								<Icon name="dropdown" />
							</span>
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 1}>
							<Dropdown.Menu className="left-fix">
								{menuItems[MENU_GROUPS.PURANO_GHAR].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
						</Accordion.Content>
					</>
				)}
				{menuItems[MENU_GROUPS.TALLA_THAP] && menuItems[MENU_GROUPS.TALLA_THAP].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 2} index={2} onClick={handleProcessClick}>
							<span>
								<Icon name="building outline" />
								{getMessage(`${messageId}.header.tallaThap`, hf_data.header.tallaThap)}
								<Icon name="dropdown" />
							</span>
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 2}>
							<Dropdown.Menu>
								{menuItems[MENU_GROUPS.TALLA_THAP].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
						</Accordion.Content>
					</>
				)}
				{menuItems[MENU_GROUPS.NAMSARI] && menuItems[MENU_GROUPS.NAMSARI].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 3} index={3} onClick={handleProcessClick}>
							<Icon name="exchange" />
							{getMessage(`${messageId}.header.namsari`, hf_data.header.namsari)}
							<Icon name="dropdown" />
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 3}>
							<Dropdown.Menu>
								{menuItems[MENU_GROUPS.NAMSARI].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
						</Accordion.Content>
					</>
				)}
				{menuItems[MENU_GROUPS.DISCARD] && menuItems[MENU_GROUPS.DISCARD].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 4} index={4} onClick={handleProcessClick}>
							<Icon name="user delete" />
							{getMessage(`${messageId}.header.discard`, hf_data.header.discard)}
							<Icon name="dropdown" />
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 4}>
							<Dropdown.Menu>
								{menuItems[MENU_GROUPS.DISCARD].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
						</Accordion.Content>
					</>
				)}
				{menuItems[MENU_GROUPS.MYAD] && menuItems[MENU_GROUPS.MYAD].length > 0 && (
					<>
						<Accordion.Title active={processActiveIndex === 5} index={5} onClick={handleProcessClick}>
							<Icon name="add" />
							{getMessage(`${messageId}.header.myad`, hf_data.header.myad)}
							<Icon name="dropdown" />
						</Accordion.Title>
						<Accordion.Content active={processActiveIndex === 5}>
							<Dropdown.Menu>
								{menuItems[MENU_GROUPS.MYAD].map((el, index) => (
									<React.Fragment key={index}>{el}</React.Fragment>
								))}
							</Dropdown.Menu>
						</Accordion.Content>
					</>
				)}
			</Accordion.Content>
		</Accordion>
	);
};
