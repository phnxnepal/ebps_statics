import React, { useState } from 'react';
import { Accordion, Icon } from 'semantic-ui-react';
import { getMessage } from '../shared/getMessage';
import { headfoot } from '../../utils/data/headFootLangFile';
import { useHistory } from 'react-router';
import { UserType } from '../../utils/userTypeUtils';

const hf_data = headfoot.headfoot_data;
const messageId = 'headfoot.headfoot_data';

export const UtilitySidebarMenu = ({ userType }) => {
	const [activeIndex, setActiveIndex] = useState(-1);
	const history = useHistory();

	const handleClick = (e, titleProps) => {
		const { index } = titleProps;
		const newIndex = activeIndex === index ? -1 : index;

		setActiveIndex(newIndex);
	};

	const handleItemClick = e => {
		e.preventDefault();

		const id = e.currentTarget.id;

		history.push(`/user/${id}`);
	};
	return (
		<Accordion inverted>
			<Accordion.Title active={activeIndex === 0} index={0} onClick={handleClick}>
				<span>
					<Icon name="dropdown" />
					<Icon name="tasks" style={{ marginRight: '5px' }} />
					Utilities
				</span>
			</Accordion.Title>
			<Accordion.Content active={activeIndex === 0}>
				<Accordion.Title id="forms/all-history" onClick={handleItemClick}>
					<span>
						<Icon name="history" />
						{getMessage(`${messageId}.history.allHistory`, hf_data.history.allHistory)}
					</span>
				</Accordion.Title>
				<Accordion.Title id="forms/all-comment" onClick={handleItemClick}>
					<span>
						<Icon name="comment" />
						{getMessage(`${messageId}.history.allComments`, hf_data.history.allComments)}
					</span>
				</Accordion.Title>
				{userType === UserType.ADMIN && (
					<Accordion.Title id="forms/activity-log" onClick={handleItemClick}>
						<span>
							<Icon name="list" />
							{getMessage(`${messageId}.history.activity-log`, hf_data.history.activityLog)}
						</span>
					</Accordion.Title>
				)}
				<Accordion.Title id="forms/file-upload" onClick={handleItemClick}>
					<span>
						<Icon name="cloud upload" />
						{getMessage(`${messageId}.files.upload`, hf_data.files.upload)}
					</span>
				</Accordion.Title>
				<Accordion.Title id="forms/file-list" onClick={handleItemClick}>
					<span>
						<Icon name="file alternate outline" />
						{getMessage(`${messageId}.files.view`, hf_data.files.view)}
					</span>
				</Accordion.Title>
				<Accordion.Title id="forms/print-all" onClick={handleItemClick}>
					<span>
						<Icon name="print" />
						{getMessage(`${messageId}.files.print`, hf_data.files.print)}
					</span>
				</Accordion.Title>
			</Accordion.Content>
		</Accordion>
	);
};
