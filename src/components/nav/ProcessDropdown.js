import React, { useEffect, useState } from 'react';
import { Dropdown, Icon } from 'semantic-ui-react';
import { getMessage } from '../shared/getMessage';
import { headfoot } from '../../utils/data/headFootLangFile';
import { NavLink } from 'react-router-dom';
import { ConstructionTypeValue } from '../../utils/enums/constructionType';
import { getUserRole } from '../../utils/functionUtils';
import { MenuId, getMenuUsers, MENU_GROUPS, ProcessMenuObject } from '../../utils/menuUtils';
import { ProcessSidebarMenu } from './ProcessSidebarMenu';

const hf_data = headfoot.headfoot_data;
const messageId = 'headfoot.headfoot_data';

const getItems = (handleItemClick, group, userType) => {
	const items = [
		{
			id: ProcessMenuObject[MenuId.PERMIT_ADD].id,
			component: (
				<Dropdown.Item
					icon="add"
					text={hf_data.header.nayaPrakriya}
					id="building-permit"
					as={NavLink}
					to={ProcessMenuObject[MenuId.PERMIT_ADD].url}
				/>
			),
			group: MENU_GROUPS.NAYA_NIRMAN,
		},
		{
			id: MenuId.PERMIT_ADD_TASK,
			component: (
				<Dropdown.Item
					icon="search"
					as={NavLink}
					to={{
						pathname: `/user/task-search/${MENU_GROUPS.NAYA_NIRMAN}`,
						state: { searchParams: { constructionType: ConstructionTypeValue.NAYA_NIRMAN } },
					}}
					text={hf_data.header.search}
				/>
			),
			group: MENU_GROUPS.NAYA_NIRMAN,
		},
		{
			id: MenuId.PERMIT_OLD_ADD,
			component: <Dropdown.Item icon="add" text={hf_data.header.nayaPrakriya} id="building-permit-purano" onClick={handleItemClick} />,
			group: MENU_GROUPS.PURANO_GHAR,
		},
		{
			id: MenuId.PERMIT_OLD_ADD_TASK,
			component: (
				<Dropdown.Item
					icon="search"
					as={NavLink}
					to={{
						pathname: `/user/task-search/${MENU_GROUPS.PURANO_GHAR}`,
						state: { searchParams: { constructionType: ConstructionTypeValue.PURANO_GHAR } },
					}}
					text={hf_data.header.search}
				/>
			),
			group: MENU_GROUPS.PURANO_GHAR,
		},
		{
			id: MenuId.ADD_STOREY_ADD,
			component: <Dropdown.Item icon="add" as={NavLink} to={{ pathname: ProcessMenuObject[MenuId.ADD_STOREY_ADD].url }} text={hf_data.header.nayaPrakriya} />,
			group: MENU_GROUPS.TALLA_THAP,
		},
		{
			id: MenuId.ADD_STOREY_TASK,
			component: <Dropdown.Item icon="edit" text={hf_data.header.adhyapadi} id="completed-list" onClick={handleItemClick} />,
			group: MENU_GROUPS.TALLA_THAP,
		},
		{
			id: MenuId.ADD_STOREY_SEARCH,
			component: (
				<Dropdown.Item
					icon="search"
					text={hf_data.header.search}
					as={NavLink}
					to={{
						pathname: `/user/task-search/${MENU_GROUPS.TALLA_THAP}`,
						state: { searchParams: { constructionType: ConstructionTypeValue.TALLA_THAP } },
					}}
				/>
			),
			group: MENU_GROUPS.TALLA_THAP,
		},
		{
			id: MenuId.NAMSARI_ADD,
			component: <Dropdown.Item icon="add" as={NavLink} to={{ pathname: ProcessMenuObject[MenuId.NAMSARI_ADD].url }} text={hf_data.header.nayaPrakriya} />,
			group: MENU_GROUPS.NAMSARI,
		},
		{
			id: MenuId.NAMSARI_TASK,
			component: (
				<Dropdown.Item icon="search" as={NavLink} to={{ pathname: `/user/task-search/${MENU_GROUPS.NAMSARI}` }} text={hf_data.header.search} />
			),
			group: MENU_GROUPS.NAMSARI,
		},
		{
			id: MenuId.DISCARD_ADD,
			component: (
				<Dropdown.Item icon="add" as={NavLink} to={{ pathname: ProcessMenuObject[MenuId.DISCARD_ADD].url }} text={hf_data.header.nayaPrakriya} />
			),
			group: MENU_GROUPS.DISCARD,
		},
		{
			id: MenuId.MYAD_ADD,
			component: (
				<Dropdown.Item icon="add" as={NavLink} to={{ pathname: ProcessMenuObject[MenuId.MYAD_ADD].url }} text={hf_data.header.nayaPrakriya} />
			),
			group: MENU_GROUPS.MYAD,
		},
		{
			id: MenuId.MYAD_APPROVE,
			component: (
				<Dropdown.Item icon="check square" as={NavLink} to={{ pathname: ProcessMenuObject[MenuId.MYAD_APPROVE].url }} text={hf_data.header.myadThapApprove} />
			),
			group: MENU_GROUPS.MYAD,
		},
	];

	if (group === undefined) {
		return items;
	}
	return items.filter(item => item.group === group && getMenuUsers(item.id).includes(userType));
};

export const ProcessDropdown = React.memo(({ handleItemClick, userMenu, isSideBar = false }) => {
	const [userType, setUserType] = useState('');
	const [menuItems, setMenuItems] = useState({});

	useEffect(() => {
		setUserType(getUserRole());
	}, []);

	useEffect(() => {
		const userWiseMenu = getItems(handleItemClick, undefined, undefined).filter(menu => {
			return ProcessMenuObject[menu.id] && userMenu.map(mn => mn.url).includes(ProcessMenuObject[menu.id].url);
		});
		setMenuItems(
			Object.values(MENU_GROUPS)
				.map(group => {
					return { [group]: userWiseMenu.filter(mn => mn.group === group).map(el => el.component) };
				})
				// .filter(menu => {
				// 	return ProcessMenuObject[menu.id] && ['/user/building-permit'].includes(ProcessMenuObject[menu.id].url);
				// })
				.reduce((acc, next) => {
					return { ...acc, ...next };
				}, {})
		);
	}, [userType, handleItemClick, userMenu]);

	if (isSideBar) {
		return <ProcessSidebarMenu menuItems={menuItems} />
	}
	return (
		<Dropdown
			item
			trigger={
				<>
					<Icon name="tasks" style={{ marginRight: '5px' }} />
					<span> प्रक्रिया</span>
				</>
			}
		>
			<Dropdown.Menu className="menu-dropdown">
				{menuItems[MENU_GROUPS.NAYA_NIRMAN] && menuItems[MENU_GROUPS.NAYA_NIRMAN].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="add" color="black" />
								{getMessage(`${messageId}.header.nayaDarta`, hf_data.header.nayaDarta)}
							</span>
						}
					>
						<Dropdown.Menu>
							{menuItems[MENU_GROUPS.NAYA_NIRMAN].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
				{menuItems[MENU_GROUPS.PURANO_GHAR] && menuItems[MENU_GROUPS.PURANO_GHAR].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="edit" color="black" />
								{getMessage(`${messageId}.header.puranoDarta`, hf_data.header.puranoDarta)}
							</span>
						}
					>
						<Dropdown.Menu className="left-fix">
							{menuItems[MENU_GROUPS.PURANO_GHAR].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
				{menuItems[MENU_GROUPS.TALLA_THAP] && menuItems[MENU_GROUPS.TALLA_THAP].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="building outline" color="black" />
								{getMessage(`${messageId}.header.tallaThap`, hf_data.header.tallaThap)}
							</span>
						}
					>
						<Dropdown.Menu>
							{menuItems[MENU_GROUPS.TALLA_THAP].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
				{menuItems[MENU_GROUPS.NAMSARI] && menuItems[MENU_GROUPS.NAMSARI].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="exchange" color="black" />
								{getMessage(`${messageId}.header.namsari`, hf_data.header.namsari)}
							</span>
						}
					>
						<Dropdown.Menu>
							{menuItems[MENU_GROUPS.NAMSARI].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
				{menuItems[MENU_GROUPS.DISCARD] && menuItems[MENU_GROUPS.DISCARD].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="user delete" color="black" />
								{getMessage(`${messageId}.header.discard`, hf_data.header.discard)}
							</span>
						}
					>
						<Dropdown.Menu>
							{menuItems[MENU_GROUPS.DISCARD].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
				{menuItems[MENU_GROUPS.MYAD] && menuItems[MENU_GROUPS.MYAD].length > 0 && (
					<Dropdown
						item
						pointing="left"
						trigger={
							<span color="black">
								<Icon name="add" color="black" />
								{getMessage(`${messageId}.header.myad`, hf_data.header.myad)}
							</span>
						}
					>
						<Dropdown.Menu>
							{menuItems[MENU_GROUPS.MYAD].map((el, index) => (
								<React.Fragment key={index}>{el}</React.Fragment>
							))}
						</Dropdown.Menu>
					</Dropdown>
				)}
			</Dropdown.Menu>
		</Dropdown>
	);
});
