import * as React from 'react';
import { Segment, Header, Icon, Grid, Button } from 'semantic-ui-react';
import JqxDataTable, { IDataTableProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxdatatable';
// Redux
import { connect } from 'react-redux';
import { getTaskList, setBuildPermit, getLocalDataByKey } from '../store/actions/dashboardActions';
import { getReportData } from '../store/actions/ReportActions';

import FallbackComponent, { Loading } from './shared/FallbackComponent';
import { Loader, Dimmer } from 'semantic-ui-react';
import { isEmpty, showErrorToast, getUserTypeValueNepali, getUserInfoObj } from '../utils/functionUtils';
import { dashboardLang } from '../utils/data/dashboardLangFile';
import { getMessage } from './shared/getMessage';
import { CURRENT_BUILD_PERMIT, MENU_LIST, BUILDING_CLASS, KEYBOARD, FISCAL_YEAR_ALL, REJECTED_STATUS } from '../utils/constants';
import { getGroupName, getRenewInfo } from '../utils/dataUtils';
import { setLocalStorage, getLocalStorage } from '../utils/secureLS';
import RootGrid from './loggedInComponents/Grid/RootGrid';
import { KeyboardSelectionModal } from './shared/modals/KeyboardSelectionModal';
import { taskList } from '../utils/data/genericData';
import { translateEngToNepWithZero } from '../utils/langUtils';
import { RenewReminder } from './shared/modals/RenewReminder';
import { TableWidths } from '../utils/enums/ui';
import { getFormMasterUrl, LocalAPIArray, LocalAPI, getRejectedBy, getFormMasterById } from '../utils/urlUtils';
import { UIState } from '../interfaces/ReduxInterfaces';
import { RenewalReminder } from './dashboardComponents/RenewalReminder';
import { ConstructionTypePermit, ConstructionTypeValue } from '../utils/enums/constructionType';
import { PermitObject, FiscalYear, ReportObject } from '../interfaces/DashboardInterfaces';
import { isStringEmpty } from '../utils/stringUtils';
import { CompactInfoMessage } from './shared/formComponents/CompactInfoMessage';
import { SemanticICONS } from 'semantic-ui-react/dist/commonjs/generic';
import { ReportStats } from './dashboardComponents/ReportStats';
import { cellClass, applicationStatusCellClass, namsariTaskListColumns, namsariTaskListDataField } from '../utils/jqxUtils';
import { Link } from 'react-router-dom';
import { MENU_GROUPS } from '../utils/menuUtils';
import Helmet from 'react-helmet';
import { formatReport } from '../utils/reportUtils';
import { selectNeedsPublicSignup } from '../store/selectors/organizationSelectors';
import { UserType } from '../utils/userTypeUtils';
import AdminDashboard from './AdminDashboard';
import { LSKey } from '../utils/enums/localStorageKeys';
import { NamsariStatus } from '../utils/enums/apiTypes';
import { handlePermitResponse, namsariFilter } from '../utils/applicationUtils';

const dash_lang = dashboardLang.dashboard_data;
const messageId = 'dashboardLang.dashboard_data';

interface TaskListProps {
	reportLoading: boolean;
	buildPermits: PermitObject[];
	reportPermits: object;
	fiscalYears: FiscalYear[];
	needsPublicSignup: boolean;
	getTaskList: Function;
	getLocalDataByKey: Function;
	getReportData: Function;
	setBuildPermit: Function;
}

interface TaskListState {
	localLoading: boolean;
	langModalOpen: boolean;
	renewModalOpen: boolean;
	fiscalYears: FiscalYear[];
	reportPermits: object[];
	buildPermits: PermitObject[];
	puranoGharPermits: PermitObject[];
	nayaGharPermits: PermitObject[];
	tallaThapPermits: PermitObject[];
	namsariPermits: PermitObject[];
	isAdmin: boolean;
	isDesigner: boolean;
}

type SearchParams = {
	menuGroup: string;
	cType: ConstructionTypeValue;
};

class Tasks extends React.PureComponent<UIState & TaskListProps, IDataTableProps & TaskListState> {
	private tableRef = React.createRef<JqxDataTable>();

	constructor(props: UIState & TaskListProps) {
		super(props);

		let isAdmin = false, isDesigner = false;
		try {
			const userObj = getUserInfoObj();
			isAdmin = [UserType.ORGANIZATION_ADMIN, UserType.TECH_ADMIN].includes(userObj.userType);
			isDesigner = (userObj.userType === UserType.DESIGNER);
		} catch {}

		this.state = {
			localLoading: false,
			langModalOpen: false,
			renewModalOpen: false,
			fiscalYears: [],
			buildPermits: [],
			reportPermits: [],
			puranoGharPermits: [],
			nayaGharPermits: [],
			tallaThapPermits: [],
			namsariPermits: [],
			isAdmin,
			isDesigner,
		};

		this.onSelectionInfo = this.onSelectionInfo.bind(this);
		this.setLangModalOpen = this.setLangModalOpen.bind(this);
		this.setRenewModalOpen = this.setRenewModalOpen.bind(this);
	}

	componentDidMount() {
		document.title = `${document.title} - Dashboard`;
		if (isEmpty(getLocalStorage(KEYBOARD))) {
			this.setState({ langModalOpen: true });
		}
		//@ts-ignore
		if (!this.state.isAdmin) {
			this.setState({ localLoading: true });
			this.props.getTaskList();
		}
		this.props.getLocalDataByKey(new LocalAPIArray([new LocalAPI(FISCAL_YEAR_ALL, 'fiscalYears')]));
	}

	//@ts-ignore
	componentDidUpdate(prevProps: UIState & TaskListProps, prevState: TaskListState) {
		//@ts-ignore
		if (prevProps.renewalData !== this.props.renewalData) {
			//@ts-ignore
			if (this.props.renewalData) {
				//@ts-ignore
				const { needsRenew } = getRenewInfo(this.props.renewalData);
				if (needsRenew && !this.props.needsPublicSignup) {
					this.setState({ renewModalOpen: true });
				}
			}
		}

		if (prevProps.buildPermits !== this.props.buildPermits && this.props.buildPermits) {
			const formattedPermits: PermitObject[] = [
				...this.props.buildPermits.map((permit: PermitObject) => {
					return {
						...permit,
						applicationActionBy: getUserTypeValueNepali(permit.applicationActionBy),
						//@ts-ignore
						applicationFormName: getFormMasterById(permit.yourForm).name,
					};
				}),
			];

			const nonNameTransferPermits: PermitObject[] = formattedPermits.filter(namsariFilter());
			const puranoGharPermits = nonNameTransferPermits
				.filter((permit) => permit.constructionType === ConstructionTypePermit.PURANO_GHAR)
				.slice(0, 10);
			const nayaGharPermits = nonNameTransferPermits
				.filter((permit) => permit.constructionType !== ConstructionTypePermit.PURANO_GHAR && permit.constructionType !== ConstructionTypePermit.TALLA_THAP)
				.slice(0, 10);
			const tallaThapPermits = nonNameTransferPermits
				.filter((permit) => permit.constructionType === ConstructionTypePermit.TALLA_THAP)
				.slice(0, 10);
			const namsariPermits = formattedPermits.filter(namsariFilter(true)).slice(0, 10);

			this.setState({
				buildPermits: formattedPermits,
				puranoGharPermits,
				nayaGharPermits,
				localLoading: false,
				tallaThapPermits,
				namsariPermits,
			});
		}

		if (prevProps.fiscalYears !== this.props.fiscalYears) {
			this.setState({ fiscalYears: this.props.fiscalYears });
		}

		if (prevState.fiscalYears !== this.state.fiscalYears) {
			const { fiscalYears } = this.state;
			if (Array.isArray(fiscalYears) && fiscalYears.length > 0) {
				this.props.getReportData(fiscalYears[0].yearCode);
			}
		}

		if (prevProps.reportPermits !== this.props.reportPermits && this.props.reportPermits) {
			const reportPermits: ReportObject[] = formatReport(this.props.reportPermits, this.state.isDesigner);
			this.setState({ reportPermits });
		}
	}

	componentWillUnmount() {
		document.title = 'EBPS';
	}

	updateFiscalYear = (value: any) => {
		this.props.getReportData(value);
	};

	setLangModalOpen(value: boolean) {
		this.setState({ langModalOpen: value });
	}

	setRenewModalOpen(value: boolean) {
		this.setState({ renewModalOpen: value });
	}
	// cellClassTaha = (row: any, dataField: any, cellText: any, rowData: any) => {
	//   const cellValue = rowData[dataField];
	//   if (cellValue === 'डिजाइनर') {
	//     return 'designer';
	//   } else if (cellValue === 'सब-ईन्जिनियर') {
	//     return 'subEngineer';
	//   } else if (cellValue === 'ईन्जिनियर') {
	//     return 'engineer';
	//   } else if (cellValue === 'अधिकृत') {
	//     return 'admin';
	//   } else {
	//     return 'others';
	//   }
	// };

	datafield = [
		{ name: 'applicantNo', type: 'string' },
		{ name: 'applicantName', type: 'string' },
		{ name: 'applicantAddress', type: 'string' },
		{ name: 'nibedakName', type: 'string' },
		{ name: 'applicantMobileNo', type: 'string' },
		{ name: 'applicationStatus', type: 'string' },
		{ name: 'applicationActionBy', type: 'string' },
		{ name: 'yourStatus', type: 'string' },
		{ name: 'applicationFormName', type: 'string' },
		{ name: 'applicantDate', type: 'string' },
		{ name: 'constructionType', type: 'string' },
		{ name: 'forwardTo', type: 'string' },
	];

	columns = [
		{
			text: 'शाखा दर्ता नं.',
			dataField: 'applicantNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_NO,
			cellclassname: cellClass,
			pinned: 'true',
		},
		{
			text: 'जग्गाधनीको नाम',
			dataField: 'applicantName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			cellclassname: cellClass,
			pinned: 'true',
		},

		{
			text: 'जग्गाधनीको ठेगाना',
			dataField: 'applicantAddress',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			cellclassname: cellClass,
		},
		{
			text: 'निवेदकको नाम',
			dataField: 'nibedakName',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			cellclassname: cellClass,
		},
		{
			text: 'सम्पर्क फोन.',
			dataField: 'applicantMobileNo',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.MOBILE_NO,
			cellclassname: cellClass,
		},
		{
			text: 'तह',
			dataField: 'applicationActionBy',
			cellsalign: 'center',
			align: 'center',
			width: TableWidths.APPLICATION_ACTION_BY,
			filtertype: 'list',
			cellclassname: cellClass,
			// cellclassname: cellClassTaha
		},
		{
			text: 'तह स्थिति',
			dataField: 'forwardTo',
			cellsalign: 'center',
			align: 'center',
			width: '250',
			cellclassname: cellClass,
			// filtertype: 'list',
		},
		{
			text: 'अवस्था',
			dataField: 'yourStatus',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			cellclassname: cellClass,
			filtertype: 'list',
		},
		{
			text: 'स्थिति फारम',
			dataField: 'applicationFormName',
			cellsalign: 'center',
			align: 'center',
			width: '350',
			cellclassname: cellClass,
			// cellclassname: 'alignment',
			filtertype: 'list',
			// cellsrenderer: (row: any, columnfield: any, value: any, defaulthtml: any, columnproperties: any, rowdata: any): any => value.formName,
		},
		{
			text: 'दर्ता मिति',
			dataField: 'applicantDate',
			cellsalign: 'center',
			align: 'center',
			width: '100',
			cellclassname: cellClass,
		},
		// {
		// 	text: 'भवनको किसिम',
		// 	dataField: 'constructionType',
		// 	cellsalign: 'center',
		// 	align: 'center',
		// 	width: TableWidths.CONSTRUCTION_TYPE,
		// 	pinned: 'true',
		// 	filtertype: 'list',
		// },
		{
			text: 'पूर्ण अवस्था',
			dataField: 'applicationStatus',
			cellsalign: 'center',
			cellclassname: applicationStatusCellClass,
			align: 'center',
			width: TableWidths.APPLICATION_STATUS,
			filtertype: 'list',
		},
	];

	renderTaskList = (data: PermitObject[], headings: string, icon: SemanticICONS, searchParams: SearchParams) => {
		// console.log(data);
		return (
			<Segment>
				<Loading loading={this.props.loading || this.state.localLoading} />
				<div className="app-table-task">
					<Header className="app-table-header flex-item-space-between">
						<div>
							<Icon name={icon} />
							<Header.Content>{headings}</Header.Content>
						</div>
						<Button
							as={Link}
							to={{
								pathname: `/user/task-search/${searchParams.menuGroup}`,
								state: { searchParams: { constructionType: searchParams.cType } },
							}}
							icon="eye"
							basic
							content="थप हेर्नुहोस्"
							className="basic-primary-btn"
							size="tiny"
						/>
					</Header>
					{data.length > 0 ? (
						<div className="pointer">
							{/* 
              // @ts-ignore */}
							<RootGrid
								//@ts-ignore
								localdata={data}
								datafield={this.datafield}
								columns={this.columns}
								groupable={false}
								rowsheight={60}
								pageable={false}
								// showfilterrow={true}
								// autorowheight={true}
								// @ts-ignore
								onRowClick={
									// e => {
									// console.log(e);
									this.onSelectionInfo
									// (e);
									// console.log(e);
									// }
								}
							/>
						</div>
					) : (
						<span>
							<CompactInfoMessage content={taskList.headings.noData} />
						</span>
					)}
				</div>
			</Segment>
		);
	};

	renderNamsariTaskList = (data: PermitObject[], headings: string, icon: SemanticICONS, searchParams: SearchParams) => {
		return (
			<Segment>
				<Loading loading={this.props.loading || this.state.localLoading} />
				<div className="app-table-task">
					<Header className="app-table-header flex-item-space-between">
						<div>
							<Icon name={icon} />
							<Header.Content>{headings}</Header.Content>
						</div>
						<Button
							as={Link}
							to={{
								pathname: `/user/task-search/${searchParams.menuGroup}`,
								state: { searchParams: { constructionType: searchParams.cType } },
							}}
							icon="eye"
							basic
							content="थप हेर्नुहोस्"
							className="basic-primary-btn"
							size="tiny"
						/>
					</Header>
					{data.length > 0 ? (
						<div className="pointer">
							{/* 
              // @ts-ignore */}
							<RootGrid
								//@ts-ignore
								localdata={data}
								datafield={namsariTaskListDataField}
								columns={namsariTaskListColumns}
								groupable={false}
								rowsheight={60}
								pageable={false}
								// showfilterrow={true}
								// autorowheight={true}
								// @ts-ignore
								onRowClick={
									// e => {
									// console.log(e);
									this.onSelectionInfo
									// (e);
									// console.log(e);
									// }
								}
							/>
						</div>
					) : (
						<span>
							<CompactInfoMessage content={taskList.headings.noData} />
						</span>
					)}
				</div>
			</Segment>
		);
	};

	render() {
		//@ts-ignore
		const { renewalData, loading, errors, needsPublicSignup } = this.props;
		const { isAdmin } = this.state;

		if (errors) {
			return <FallbackComponent errors={errors} loading={loading} />;
		} else {
			let urgent = false;
			let daysRemaining: string = '0';
			if (renewalData && !needsPublicSignup) {
				const { daysRemaining: days, urgent: urg } = getRenewInfo(renewalData);
				urgent = urg;
				daysRemaining = translateEngToNepWithZero(days);
			}
			return (
				<React.Fragment>
					<Helmet title="EBPS - Dashboard" />
					<KeyboardSelectionModal
						langModalOpen={
							// @ts-ignore
							this.state.langModalOpen
						}
						setLangModalOpen={this.setLangModalOpen}
					/>
					<RenewReminder
						renewModalOpen={this.state.renewModalOpen}
						setRenewModalOpen={this.setRenewModalOpen}
						daysRemaining={daysRemaining}
					/>
					{/* {errors && <ErrorDisplay message={errors.message} />} */}
					{urgent && <RenewalReminder daysRemaining={daysRemaining} />}
					<Dimmer active={loading && !this.props.reportLoading} inverted page>
						<Loader active={loading && !this.props.reportLoading} inline="centered">
							{getMessage(`${messageId}.datafetch`, dash_lang.datafetch)}
						</Loader>
					</Dimmer>
					<Grid className="white-background compact-grid">
						<Grid.Column computer={16}>
							<ReportStats
								loading={this.props.reportLoading || this.props.loading}
								updateFiscalYear={this.updateFiscalYear}
								fiscalYears={this.state.fiscalYears}
								data={this.state.reportPermits}
							/>
						</Grid.Column>
						{isAdmin ? (
							<AdminDashboard />
						) : (
							<>
								<Grid.Column mobile={16} tablet={16} computer={8}>
									{this.renderTaskList(this.state.puranoGharPermits, taskList.headings.puranoDarta, 'list', {
										menuGroup: MENU_GROUPS.PURANO_GHAR,
										cType: ConstructionTypeValue.PURANO_GHAR,
									})}
								</Grid.Column>
								<Grid.Column mobile={16} tablet={16} computer={8}>
									{this.renderTaskList(this.state.nayaGharPermits, taskList.headings.nayaDarta, 'list', {
										menuGroup: MENU_GROUPS.NAYA_NIRMAN,
										cType: ConstructionTypeValue.NAYA_NIRMAN,
									})}
								</Grid.Column>
								<Grid.Column mobile={16} tablet={16} computer={8}>
									{this.renderTaskList(this.state.tallaThapPermits, taskList.headings.tallaThap, 'building', {
										menuGroup: MENU_GROUPS.TALLA_THAP,
										cType: ConstructionTypeValue.TALLA_THAP,
									})}
								</Grid.Column>
								<Grid.Column mobile={16} tablet={16} computer={8}>
									{this.renderNamsariTaskList(this.state.namsariPermits, taskList.headings.namsari, 'exchange', {
										menuGroup: MENU_GROUPS.NAMSARI,
										cType: ConstructionTypeValue.NAYA_NIRMAN,
									})}
								</Grid.Column>
							</>
						)}
					</Grid>
				</React.Fragment>
			);
		}
	}

	//@ts-ignore
	private onSelectionInfo(e) {
		// gets selected row indexes. The method returns an Array of indexes.
		const index = e.args.row.applicantNo;

		window.scroll(0, 0);
		const currentPermit = this.props.buildPermits.find((permit: PermitObject) => permit.applicantNo === index);

		if (currentPermit) {
			// const hasDesignerChanged: boolean = currentPermit.designerChangeStatus ? currentPermit.designerChangeStatus === 'Y' : false;
			// const newDesignerName: string = currentPermit.newDesignerName ? currentPermit.newDesignerName : '';
			this.props
				.setBuildPermit(currentPermit)
				// @ts-ignore
				.then((response) => {
					if (currentPermit.yourAction) {
						//@ts-ignore
						handlePermitResponse(response, currentPermit);
					} else {
						throw new Error('View URL not set for this user and build permit.');
					}
					if (!isEmpty(currentPermit) && !isEmpty(currentPermit.yourForm)) {
						const formUrl: string =
							//@ts-ignore
							currentPermit.isRejected === 'Y' ? currentPermit.applicationAction.viewURL : getFormMasterUrl(currentPermit.yourForm);

						if (currentPermit.isRejected === 'Y') {
							const rejectedBy = getRejectedBy(currentPermit);
							setLocalStorage(REJECTED_STATUS, JSON.stringify({ rejectedBy }));
						}
						//@ts-ignore
						this.props.history.push(formUrl.trim());
						// }, 1000);
					}
				})
				.catch((err: any) => {
					console.log(err);
				});
		} else {
			showErrorToast('Permit not found');
		}
	}
}

const mapDispatchToProps = { getTaskList, setBuildPermit, getLocalDataByKey, getReportData };
//@ts-ignore
const mapStateToProps = (state) => ({
	fiscalYears: state.root.dashboard.fiscalYears,
	needsPublicSignup: selectNeedsPublicSignup(state.root.dashboard),
	reportPermits: state.root.dashboard.report ? state.root.dashboard.report.data : {},
	reportLoading: state.root.dashboard.report ? state.root.dashboard.report.loading : false,
	buildPermits: state.root.dashboard.buildPermits,
	currentPermit: state.root.dashboard.currentPermit,
	renewalData: state.root.dashboard.renewalData,
	errors: state.root.ui.errors,
	loading: state.root.ui.loading,
});
export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
