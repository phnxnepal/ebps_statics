import { rajaswoFormLang } from "../../../utils/data/rajaswoDetailLang";
import { floorMappingFlat } from "../../../utils/dataUtils";
import { isStringEmpty } from "../../../utils/stringUtils";
import { round, areaconvertor } from "../../../utils/mathUtils";

export const detailsField = [
    'dArea',
    'depositAmount',
    'jDepositAmount',
    'depositRate',
    'jDepositRate',
    'drAmount',
    'drRate',
    'floor',
    'jArea',
    'jType',
    'jrAmount',
    'jrRate',
    'totalAmount',
];

export const hasDharauti = (details: [{ depositAmount: number, depositRate: number }]) => {
    let hasValue = false;
    details.forEach((row) => (hasValue = hasValue || row.depositAmount > 0 || row.depositRate > 0));
    return hasValue;
};

type FloorOptionType = {
    key: number, text: string, value: number
}

export const getFloorOptions = () => {
    let retFloor: FloorOptionType[] = [];
    Object.values(rajaswoForm.floorMappingFlat)
        .splice(existingFloorsArr.length)
        .forEach((row) => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));

    Object.values(rajaswoForm.floorMapping.otherFloors).map((row) => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));
    return retFloor.filter((row) => row.key !== undefined);
};

type DetailType = {
    floor: number
}

export const getFloorOptionsDy = (details: DetailType[]) => {
    let retFloor: FloorOptionType[] = [];

    const existingFloors: number[] = details.reduce((acc: number[], next) => {
        acc.push(next.floor);
        return acc;
    }, []);

    floorMappingFlat
        .filter((fl) => !existingFloors.includes(fl.floor))
        .map((row) => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));
    // Object.values(rajaswoForm.floorMapping.otherFloors).map(row =>
    // 	retFloor.push({ key: row.floor, text: row.value, value: row.floor })
    // );
    return retFloor.filter((row) => row.key !== undefined);
};

const rajaswoForm = rajaswoFormLang;

export const jTypeOptions = [
    { key: '1', text: 'जस्ता', value: 'जस्ता' },
    { key: '2', text: 'अन्य', value: 'अन्य' },
];

export const initDetails = (row: any) =>
    detailsField.forEach((field) => {
        if (isStringEmpty(row[field])) {
            row[field] = 0;
        }
    });

type RajasowMasterType = {
    floor: number, floorType: string, constructionType: string, dasturRate: number
}

export const getInitDetails = (floor: { floor: number, block: string, area: number }, master: RajasowMasterType[], constructionType: string, convertToFeet = false) => {
    let row: any = {};
    const currentDasturRow = master.find(
        (mRow) => mRow.floor === floor.floor && mRow.floorType === 'F' && constructionType === mRow.constructionType
    );
    const currentJastaRow = master.find((mRow) => mRow.floor === floor.floor && mRow.floorType === 'J' && constructionType === mRow.constructionType);

    detailsField.forEach((field) => (row[field] = 0));
    
    row.floor = floor.floor;
    row.block = floor.block;
    row.dArea = convertToFeet ? areaconvertor(floor.area, 'SQUARE FEET', 'SQUARE METRE') : floor.area;
    if (currentDasturRow) {
        row.drRate = round(currentDasturRow.dasturRate);
        row.drAmount = round(row.dArea * currentDasturRow.dasturRate);
    }
    row.jrRate = currentJastaRow ? round(currentJastaRow.dasturRate) : 0;
    return row;
};

const existingFloorsArr = Object.values(rajaswoForm.table_body);

export const additionalFloorOptions = getFloorOptions();
export const Opt = [
    { key: 'a1', value: 'आवासिय क्षेत्र', text: 'आवासिय क्षेत्र' },
    { key: 'a2', value: 'व्यापारिक क्षेत्र', text: 'व्यापारिक क्षेत्र' },
    { key: 'a3', value: 'कृषि क्षेत्र', text: 'कृषि क्षेत्र' },
    { key: 'd4', value: 'औद्योगिक क्षेत्र', text: 'औद्योगिक क्षेत्र' },
    { key: 'a5', value: 'शैक्षिक क्षेत्र', text: 'शैक्षिक क्षेत्र' },
    { key: 'a6', value: ' संस्थागत क्षेत्र', text: 'संस्थागत क्षेत्र' },
];
