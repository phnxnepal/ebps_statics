import React from 'react';
import { Header } from 'semantic-ui-react';
import { getUserTypeValueNepali } from '../../utils/functionUtils';
import FallbackComponent from '../shared/FallbackComponent';
import { historyComment } from '../../utils/data/headFootLangFile';
import { getMessage } from '../shared/getMessage';

//redux
import { connect } from 'react-redux';
import { postFormDataByUrl, getMultipleFormDataByUrl } from '../../store/actions/formActions';
import JqxGrid, { IGridProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import api from '../../utils/api';
import ErrorDisplay from '../shared/ErrorDisplay';
// import RootGrid from "./Grid/RootGrid";
import RootGrid from './Grid/RootGrid';
import Helmet from 'react-helmet';
import { UserTypeMaster, UserType } from '../../utils/userTypeUtils';
import { getLocalStorage } from '../../utils/secureLS';
import { LSKey } from '../../utils/enums/localStorageKeys';

const history_data = historyComment.hc_data;
const messageId = 'historyComment.hc_data';

interface CommentState {
	commentData: object;
	userTypeMaster: UserTypeMaster[];
}
class Allcomments extends React.PureComponent<{}, IGridProps & CommentState> {
	constructor(props: {}) {
		super(props);
		this.state = {
			commentData: [],
			userTypeMaster: [],
		};
	}

	//@ts-ignore
	componentDidUpdate(prevProps) {
		const { userTypeMaster } = this.state;
		const getUserTypeNepali = (userType: UserType) => {
			const userTypeObject = userTypeMaster.find((uT) => uT.userType === userType);
			if (userTypeObject) {
				return userTypeObject.designation;
			} else {
				getUserTypeValueNepali(userType);
			}
		};

		//@ts-ignore
		if (this.props.prevData !== prevProps.prevData) {
			//@ts-ignore
			this.setState({
				commentData: Object.assign(
					[],
					//@ts-ignore
					this.props.prevData.map((row) => {
						return {
							...row,
							userType: getUserTypeNepali(row.userType),
						};
					})
				),
			});
		}
	}

	//@ts-ignore
	private myGrid = React.createRef<JqxGrid>();
	componentDidMount() {
		const userTypeMaster = JSON.parse(getLocalStorage(LSKey.USER_TYPE_ALL));
		this.setState({ userTypeMaster });
		//@ts-ignore
		this.props.getMultipleFormDataByUrl([{ api: api.allcomments, objName: 'allComments', form: true }], (data) => data);
	}

	datafield = [
		{ name: 'name', type: 'string' },
		{ name: 'actionDate', type: 'string' },
		{ name: 'actionTime', type: 'string' },
		{ name: 'status', type: 'string' },
		{ name: 'userType', type: 'string' },
		{ name: 'comment_by', type: 'string' },
		{ name: 'comment', type: 'string' },
		{ name: 'remark', type: 'string' },
	];

	columns = [
		{
			text: 'फारम नाम',
			dataField: 'name',
			// width: '280',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
		},
		{
			text: 'मिति',
			datafield: 'actionDate',
			width: '90',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'समय',
			datafield: 'actionTime',
			width: '70',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'प्रयोगकर्ताको प्रकार',
			datafield: 'userType',
			width: '120',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
		},
		,
		{
			text: 'प्रयोगकर्ताको नाम',
			datafield: 'comment_by',
			width: '130',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'टिप्पणी',
			datafield: 'comment',
			width: '180',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'कैफियत',
			datafield: 'remark',
			width: '180',
			cellsalign: 'center',
			align: 'center',
		},
	];
	render() {
		//@ts-ignore
		if (this.props.prevData) {
			return (
				<div className="view-Form">
					<Helmet>
						<title>
							All Comments
						</title>
					</Helmet>
					{/* 
        //@ts-ignore */}
					{this.props.errors && (
						//@ts-ignore
						<ErrorDisplay message={this.props.errors.message} />
					)}
					<Header as="h3" dividing>
						{getMessage(`${messageId}.cmts`, history_data.cmts)}
					</Header>

					{/* 
        //@ts-ignore */}
					<RootGrid
						//@ts-ignore
						localdata={this.state.commentData}
						datafield={this.datafield}
						columns={this.columns}
						groupable={false}
						showfilterrow={true}
					/>
				</div>
			);
		} else {
			return (
				<FallbackComponent
					//@ts-ignore
					loading={this.props.loading}
					//@ts-ignore
					errors={this.props.errors}
				/>
			);
		}
	}
}
//@ts-ignore
const mapStateToProps = (state) => ({
	prevData: state.root.formData.allComments,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
});
const mapDispatchToProps = { getMultipleFormDataByUrl, postFormDataByUrl };

export default connect(mapStateToProps, mapDispatchToProps)(Allcomments);
