import { FormUrlFull } from "../../../utils/enums/url";


export const requireFields = {
    [String(FormUrlFull.DESIGN_APPROVAL)]: [
        'constructionType',
        'buildingClass',
        'applicationNo',
        'designerName',
        'designerDesignation',
        'designerDate',
    ]
}

export class RequiredFields {
    static getRequiredFields(formUrl: string) {
        return requireFields[formUrl]
    }
}