import React, { PureComponent } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Menu, Dropdown, Header, Icon, Sidebar } from 'semantic-ui-react';
import { logoutUser } from '../../store/actions/loginForm';
import Logo from './../../assets/images/Nepal_sarkar_logo.webp';
import { headfoot } from '../../utils/data/headFootLangFile';

import { connect } from 'react-redux';
import { getUserInfoObj, getBuildPermitObj, isEmpty, getUserTypeValueNepali } from '../../utils/functionUtils';
import { getCurrentDate, getADDate } from '../../utils/dateUtils';
import { getMessage } from '../shared/getMessage';
import { FISCAL_YEAR } from '../../utils/constants';
import { translateDate, translateEngToNep } from '../../utils/langUtils';
import { getLocalStorage } from '../../utils/secureLS';
import { KeyboardSelectionModal } from '../shared/modals/KeyboardSelectionModal';
import { UserType, needsFormAccess } from '../../utils/userTypeUtils';
import { toggleNavbar } from '../../store/actions/uiActions';
import { SetupDropdown } from '../ebps-setup/SetupDropdown';
import { getLastUrl } from '../../utils/urlUtils';
import { ProcessDropdown } from '../nav/ProcessDropdown';
import { UtilitySidebarMenu } from '../nav/UtilitySidebarMenu';
import { MenuList } from '../../utils/enums/apiTypes';

const hf_data = headfoot.headfoot_data;
const messageId = 'headfoot.headfoot_data';

interface HeaderProps {
	isNavOpen: boolean;
	toggleNavbar: (value: boolean) => {};
	userMenu: MenuList;
}

interface HeaderState {
	hasPermit: boolean;
	activeItem: string;
	username: string;
	langModalOpen: boolean;
	needsSetupMenu: boolean;
	userInfo: { designation: string, organization: string }
}

class HeaderComponent extends PureComponent<HeaderProps, HeaderState> {
	constructor(props: HeaderProps) {
		super(props);
		this.state = {
			activeItem: '',
			username: '',
			hasPermit: false as boolean,
			langModalOpen: false as boolean,
			needsSetupMenu: this.props.userMenu.some(row => row.menu_type === 'S') || false,
			userInfo: { designation: '', organization: '' },
		};
	}

	componentDidMount() {
		const userInfoObj = getUserInfoObj();
		let designation = '', organization = '', username = ''

		try {
			designation = userInfoObj.info.userType.designationNepali || userInfoObj.info.userType.designation
			organization = userInfoObj.organization.name;
			username = userInfoObj.userName;
		} catch {
			designation = '';
			organization = '';
			username = '';
		}

		this.setState(() => ({
			hasPermit: !isEmpty(getBuildPermitObj()),
			username,
			//@ts-ignore
			activeItem: getLastUrl(this.props.location.pathname),
			userInfo: { designation, organization }
		}));
	}

	componentDidUpdate(prevProps: HeaderProps) {
		//@ts-ignore
		if (this.props.location.pathname !== prevProps.location.pathname) {
			this.setState(() => ({
				hasPermit: !isEmpty(getBuildPermitObj()),
				//@ts-ignore
				activeItem: getLastUrl(this.props.location.pathname),
			}));
		}
	}

	handleItemClick = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
		e.preventDefault();

		const id: string = e.currentTarget.id;

		this.props.toggleNavbar(false);
		//@ts-ignore
		this.props.history.push(`/user/${id}`);
	};

	handleSetupMenu = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
		e.preventDefault();

		const id: string = e.currentTarget.id;

		this.props.toggleNavbar(false);
		//@ts-ignore
		this.props.history.push(`/admin/${id}`);
	};

	handleFileMenu = (e: React.MouseEvent<HTMLSpanElement, MouseEvent>): void => {
		e.preventDefault();

		const id: string = e.currentTarget.id;

		this.props.toggleNavbar(false);
		//@ts-ignore
		this.props.history.push(`/user/forms/${id}`);
	};

	setLangModalOpen = (value: boolean): void => {
		this.setState(() => ({
			langModalOpen: value,
		}));
	};

	handleLogout = () => {
		//@ts-ignore
		this.props.logoutUser();

		//@ts-ignore
		this.props.history.push('/');
	};
	render() {
		const { activeItem, hasPermit, langModalOpen, username, userInfo, needsSetupMenu } = this.state;

		const { userMenu } = this.props;

		let fiscalYear = '';

		try {
			//@ts-ignore
			fiscalYear = JSON.parse(getLocalStorage(FISCAL_YEAR)).yearName || '';
			fiscalYear = translateDate(fiscalYear);
		} catch {
			fiscalYear = '';
		}

		// console.log('userinfo', getUserInfoObj());

		//@ts-ignore
		const { userType: userType = '' } = getUserInfoObj();

		return (
			<React.Fragment>
				<KeyboardSelectionModal langModalOpen={langModalOpen} setLangModalOpen={this.setLangModalOpen} />
				<Menu inverted className="ebpsMain-header fixed">
					{//@ts-ignore
					// @ts-ignore
					this.state.hasPermit && (
						<Menu.Item>
							<Icon
								name="bars"
								size="large"
								onClick={
									//@ts-ignore
									this.props.onToggle
								}
							/>
						</Menu.Item>
					)}
					<Menu.Item>
						<Header style={{ color: 'white' }}>
							<Link to="/user/task-list">
								<div className="logo-div">
									<span className="comp-logo">
										<img src={Logo} alt="phoenix-logo" />
									</span>
									<span>{userInfo.organization}</span>
								</div>
							</Link>
						</Header>
					</Menu.Item>

					<Menu.Item className="header-date">
						{getMessage(`${messageId}.header.nepaliFY`, hf_data.header.nepaliFY)}
						{fiscalYear}
						{' | '}
						{getMessage(`${messageId}.header.bs`, hf_data.header.bs)}
						{getCurrentDate(true)}
						{' | '}
						{getMessage(`${messageId}.header.ad`, hf_data.header.ad)}
						{translateEngToNep(getADDate())}
					</Menu.Item>

					<Menu.Menu position="right" className="navbar-toggle">
						{needsSetupMenu && (
							// @ts-ignore
							<SetupDropdown handleSetupMenu={this.handleSetupMenu} userMenu={userMenu} />
						)}
						<Menu.Item>
							<Icon name="bars" size="large" onClick={() => this.props.toggleNavbar(!this.props.isNavOpen)} />
						</Menu.Item>
					</Menu.Menu>

					<Menu.Menu position="right" className="navbar-items">
						<Menu.Item
							icon="clipboard outline"
							name="Dashboard"
							id="task-list"
							active={activeItem === 'task-list'}
							onClick={this.handleItemClick}
						/>
						{!this.state.hasPermit && needsFormAccess(userType) && (
							// @ts-ignore
							<ProcessDropdown handleItemClick={this.handleItemClick} userMenu={userMenu} />
						)}
						<Menu.Item
							name="Application"
							icon="search"
							id="application-list"
							active={activeItem === 'application-list'}
							onClick={this.handleItemClick}
							// component={NavLink}
							// to={'/login'}
						/>
						{/* {[UserType.ORGANIZATION_ADMIN, UserType.TECH_ADMIN, UserType.ADMIN].includes(userType) && ( */}
						{needsSetupMenu && (
							// @ts-ignore
							<SetupDropdown handleSetupMenu={this.handleSetupMenu} userMenu={userMenu} />
						)}
						{[UserType.ADMIN, UserType.ENGINEER].includes(userType) && (
							<Menu.Item name="Report" id="report" active={activeItem === 'report'} onClick={this.handleItemClick} />
						)}
						{hasPermit && (
							<Dropdown compact item text="Utilities" className="english-div">
								<Dropdown.Menu>
									<Dropdown.Item id="forms/all-history" onClick={this.handleItemClick}>
										<span color="black">
											<Icon name="history" color="black" />
											{getMessage(`${messageId}.history.allHistory`, hf_data.history.allHistory)}
										</span>
									</Dropdown.Item>
									<Dropdown.Item id="forms/all-comment" onClick={this.handleItemClick}>
										<span color="black">
											<Icon name="comment" color="black" />
											{getMessage(`${messageId}.history.allComments`, hf_data.history.allComments)}
										</span>
									</Dropdown.Item>
									{userType === UserType.ADMIN && (
										<Dropdown.Item id="forms/activity-log" onClick={this.handleItemClick}>
											<span color="black">
												<Icon name="list" color="black" />
												{getMessage(`${messageId}.history.activity-log`, hf_data.history.activityLog)}
											</span>
										</Dropdown.Item>
									)}
									<Dropdown.Item id="file-upload" onClick={this.handleFileMenu}>
										<span color="black">
											<Icon name="cloud upload" color="black" />
											{getMessage(`${messageId}.files.upload`, hf_data.files.upload)}
										</span>
									</Dropdown.Item>
									<Dropdown.Item id="file-list" onClick={this.handleFileMenu}>
										<span color="black">
											<Icon name="file alternate outline" color="black" />
											{getMessage(`${messageId}.files.view`, hf_data.files.view)}
										</span>
									</Dropdown.Item>
									<Dropdown.Item id="print-all" onClick={this.handleFileMenu}>
										<span color="black">
											<Icon name="print" color="black" />
											{getMessage(`${messageId}.files.print`, hf_data.files.print)}
										</span>
									</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
						)}
						<Dropdown
							compact
							item
							trigger={
								<>
									<Icon name="user circle" />
									<div className="profile-menu-div">
										<p className="menu-text">{`${username}`}</p>
										<p className="profile-menu-post menu-text">{`${getUserTypeValueNepali(userType)}`}</p>
									</div>
								</>
							}
							className="english-div no-margin-menu-item"
						>
							<Dropdown.Menu>
								{/* <span id="profile" >
                    {/*{username} - {getUserTypeValue(userType)} 
                  </span> */}
								<Dropdown.Item id="profile" onClick={this.handleItemClick}>
									<span>
										<Icon name="user" color="black" />
										{getMessage(`${messageId}.profile.profile`, hf_data.profile.profile)}
									</span>
								</Dropdown.Item>
								{/* <Dropdown.Item className="coloricon" text="My Profile"  onClick={this.handleItemClick} icon="user outline"/> */}

								<Dropdown.Item id="change-password" onClick={this.handleItemClick}>
									<span color="black ">
										<Icon name="key" />
										{getMessage(`${messageId}.profile.changePassword`, hf_data.profile.changePassword)}
									</span>
								</Dropdown.Item>
								<Dropdown.Item
									icon="keyboard"
									text={getMessage(`${messageId}.profile.keyboardLayout`, hf_data.profile.keyboardLayout)}
									onClick={() => this.setLangModalOpen(true)}
								/>
								<Dropdown.Item
									icon="sign-out"
									text={getMessage(`${messageId}.profile.logout`, hf_data.profile.logout)}
									onClick={this.handleLogout}
								/>
							</Dropdown.Menu>
						</Dropdown>
					</Menu.Menu>
				</Menu>
				<aside className="drawer-menu">
					<nav className="sidenav" style={{ width: this.props.isNavOpen ? '150px' : '0px' }}>
						<Icon name="window close outline" size="big" className="closebtn" onClick={() => this.props.toggleNavbar(false)} />
						<Sidebar.Pushable>
							<Sidebar
								icon="labeled"
								vertical
								inverted
								visible={this.props.isNavOpen}
								width="thin"
								as={Menu}
								onHide={() => this.props.toggleNavbar(false)}
							>
								<Menu.Item>
									<Icon name="user circle" size="large" />
									{`${username} - ${userInfo.designation}`}
								</Menu.Item>
								<Menu.Item id="task-list" active={activeItem === 'task-list'} onClick={this.handleItemClick}>
									<span>
										<Icon name="clipboard outline" />
										Dashboard
									</span>
								</Menu.Item>
								{!this.state.hasPermit && needsFormAccess(userType) && (
									// @ts-ignore
									<ProcessDropdown handleItemClick={this.handleItemClick} userMenu={userMenu} isSideBar={true} />
								)}
								<Menu.Item id="application-list" active={activeItem === 'application-list'} onClick={this.handleItemClick}>
									<span>
										<Icon name="search" />
										Application
									</span>
								</Menu.Item>
								{[UserType.ADMIN, UserType.ENGINEER].includes(userType) && (
									<Menu.Item name="Report" id="report" active={activeItem === 'report'} onClick={this.handleItemClick} />
								)}
								{hasPermit && <UtilitySidebarMenu userType={userType} />}
								<Menu.Item id="profile" active={activeItem === 'profile'} onClick={this.handleItemClick}>
									<span>
										<Icon name="user" />
										{getMessage(`${messageId}.profile.profile`, hf_data.profile.profile)}
									</span>
								</Menu.Item>
								<Menu.Item id="change-password" active={activeItem === 'change-password'} onClick={this.handleItemClick}>
									<span>
										<Icon name="key" />
										{getMessage(`${messageId}.profile.changePassword`, hf_data.profile.changePassword)}
									</span>
								</Menu.Item>
								<Menu.Item onClick={() => this.setLangModalOpen(true)}>
									<span>
										<Icon name="keyboard" />
										{getMessage(`${messageId}.profile.keyboardLayout`, hf_data.profile.keyboardLayout)}
									</span>
								</Menu.Item>
								<Menu.Item onClick={this.handleLogout}>
									<span>
										<Icon name="sign-out" />
										{getMessage(`${messageId}.profile.logout`, hf_data.profile.logout)}
									</span>
								</Menu.Item>
							</Sidebar>
						</Sidebar.Pushable>
					</nav>
				</aside>
			</React.Fragment>
		);
	}
}

//@ts-ignore
const mapStateToProps = state => {
	return {
		login: state.root.login,
		isNavOpen: state.root.ui.isNavOpen,
	};
};

const mapDispatchToProps = { logoutUser, toggleNavbar };

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(props => <HeaderComponent {...props} />));
