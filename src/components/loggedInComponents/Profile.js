import React, { Component } from 'react';
import { Segment, Header, Divider } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { getUserData } from '../../store/actions/loginForm';
import { isEmpty, getUserTypeValueNepali } from '../../utils/functionUtils';
import FallbackComponent from '../shared/FallbackComponent';
import { getMessage } from '../shared/getMessage';
import { profile } from '../../utils/data/headFootLangFile';
import { ImageDisplay } from '../shared/file/FileView';
import Helmet from 'react-helmet';

const profile_lang = profile.profile_data;
const messageId = 'profile.profile_data';

class Profile extends Component {
	componentDidMount() {
		this.props.getUserData();
	}

	render() {
		if (!isEmpty(this.props.userData.userInfo) && !isEmpty(this.props.userData.userData)) {
			const { userName, organization, userType } = this.props.userData.userInfo;

			const userInfos = this.props.userData.userData[0];

			return (
				<div className="profile-div">
					<Helmet>
						<title>EBPS - Profile</title>
					</Helmet>
					<Segment raised>
						<Header>
							{userName} - {getUserTypeValueNepali(userType)}
						</Header>
						{/* <Segment raised> */}
						{/* <Header as="h3"> */}
						<fieldset className="fieldsetDesign" style={{ border: '0px solid #d4d4d5' }}>
							<b>User Details</b>
							<div
								className="profile-image-div"
								style={{
									float: 'right',
								}}
							>
								<ImageDisplay src={userInfos.photo} alt={'user photo'} />
							</div>
							<p>
								<br />
								<b>{getMessage(`${messageId}.u_name`, profile_lang.u_name)}</b>
								{userName}
							</p>

							<p>
								<b>{getMessage(`${messageId}.address`, profile_lang.address)}</b>
								{userInfos.address}
							</p>
							<p>
								<b>{getMessage(`${messageId}.contact`, profile_lang.contact)}</b>
								{userInfos.mobile}
							</p>
							<p>
								<b>{getMessage(`${messageId}.email`, profile_lang.email)}</b>
								{userInfos.email}
							</p>
							<p>
								<b>{getMessage(`${messageId}.educationQualification`, profile_lang.educationQualification)}</b>
								{userInfos.educationQualification}
							</p>
							<p>
								<b>{getMessage(`${messageId}.joinDate`, profile_lang.joinDate)}</b>
								{userInfos.joinDate}
							</p>
							<p>
								<b>{getMessage(`${messageId}.licenseNo`, profile_lang.licenseNo)}</b>
								{userInfos.licenseNo}
							</p>
							<p>
								<b>{getMessage(`${messageId}.municipalRegNo`, profile_lang.municipalRegNo)}</b>
								{userInfos.municipalRegNo}
							</p>
							<p>
								<b>{getMessage(`${messageId}.signature`, profile_lang.signature)}</b>
								<div className="profile-image-div">
									<ImageDisplay src={userInfos.signature} alt="signature" />
								</div>
							</p>
							<p>
								<b>{getMessage(`${messageId}.stamp`, profile_lang.stamp)}</b>
								<div className="profile-image-div">
									<ImageDisplay src={userInfos.stamp} alt="imageFile" />
								</div>
							</p>
						</fieldset>
						<Divider />
						<fieldset className="fieldsetDesign" style={{ border: '0px solid #d4d4d5' }}>
							<b>{'Organization Details'}</b>

							<p>
								<br />
								<b>{getMessage(`${messageId}.org_name`, profile_lang.org_name)}</b>
								{organization.name}
							</p>
							<p>
								<b>{getMessage(`${messageId}.office_name`, profile_lang.office_name)}</b>
								{organization.officeName}
							</p>

							<p>
								<b>{getMessage(`${messageId}.address`, profile_lang.address)}</b>
								{organization.address}
							</p>

							<p>
								<b>{getMessage(`${messageId}.province`, profile_lang.province)}</b>
								{organization.province}
							</p>
						</fieldset>
						{/* </Header> */}
					</Segment>
					{/* </Segment> */}
				</div>
			);
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}

const mapStateToProps = state => ({
	userData: state.root.login,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

const mapDispatchToProps = {
	getUserData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
