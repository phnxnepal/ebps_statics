import * as Yup from 'yup';
import { validateNullableNumber, validateNepaliDate, validateNumber, validateNullableNepaliDate } from '../../../utils/validationUtils';

export const SuperStruConsSchema = Yup.object().shape({
	// gharNo: validateNumber,
	// nakshaPassDastur: validateNumber,
	// jariwanaRate: validateNumber,
	supStrucDate: validateNepaliDate,
	noteOrderDate: validateNepaliDate,
	allowanceDate: validateNepaliDate,
	plinthDetails: validateNullableNumber,
	buildCoverArea: validateNullableNumber,
	coverageDetails: validateNullableNumber,
	buildingArea: validateNullableNumber,
	coverageArea: validateNullableNumber,
	houseLength: validateNullableNumber,
	houseBreadth: validateNullableNumber,
	houseHeight: validateNullableNumber,
	buildFloorInfo: validateNullableNumber,
	buildApprovFloor: validateNullableNumber,
	buildHeightInfo: validateNullableNumber,
	buildApprovHeight: validateNullableNumber,
});

export const TallaThapIjajatSchema = Yup.object().shape({
	// gharNo: validateNumber,
	// nakshaPassDastur: validateNumber,
	// jariwanaRate: validateNumber,
	supStrucDate: validateNepaliDate,
	noteOrderDate: validateNepaliDate,
	plinthDetails: validateNullableNumber,
	buildCoverArea: validateNullableNumber,
	coverageDetails: validateNullableNumber,
	buildingArea: validateNullableNumber,
	coverageArea: validateNullableNumber,
	houseLength: validateNullableNumber,
	houseBreadth: validateNullableNumber,
	houseHeight: validateNullableNumber,
	buildFloorInfo: validateNullableNumber,
	buildApprovFloor: validateNullableNumber,
	buildHeightInfo: validateNullableNumber,
	buildApprovHeight: validateNullableNumber,
});

export const SundarSuperStruConsSchema = Yup.object().shape({
	// gharNo: validateNumber,
	// nakshaPassDastur: validateNumber,
	// jariwanaRate: validateNumber,
	supStrucDate: validateNepaliDate,
	noteOrderDate: validateNepaliDate,
	// allowanceDate: validateNepaliDate,
	plinthDetails: validateNullableNumber,
	buildCoverArea: validateNullableNumber,
	coverageDetails: validateNullableNumber,
	buildingArea: validateNullableNumber,
	coverageArea: validateNullableNumber,
	houseLength: validateNullableNumber,
	houseBreadth: validateNullableNumber,
	houseHeight: validateNullableNumber,
	buildFloorInfo: validateNullableNumber,
	buildApprovFloor: validateNullableNumber,
	buildHeightInfo: validateNullableNumber,
	buildApprovHeight: validateNullableNumber,
});


export const BiratnagarSuperStruConsSchema = Yup.object().shape({
	// gharNo: validateNumber,
	// nakshaPassDastur: validateNumber,
	// jariwanaRate: validateNumber,
	supStrucDate: validateNepaliDate,
	noteOrderDate: validateNullableNepaliDate,
	allowanceDate: validateNullableNepaliDate,
	// allowanceDate: validateNepaliDate,
	plinthDetails: validateNullableNumber,
	buildCoverArea: validateNullableNumber,
	coverageDetails: validateNullableNumber,
	buildingArea: validateNullableNumber,
	coverageArea: validateNullableNumber,
	houseLength: validateNullableNumber,
	houseBreadth: validateNullableNumber,
	houseHeight: validateNullableNumber,
	buildFloorInfo: validateNullableNumber,
	buildApprovFloor: validateNullableNumber,
	buildHeightInfo: validateNullableNumber,
	buildApprovHeight: validateNullableNumber,
});

export const InaruwaSchema = Yup.object().shape({
	dasturAmount: validateNumber,
	jariwanaAmount: validateNumber
});

export const SuperStructConstBirtamodSchema = Yup.object().shape({
	supStrucDate: validateNepaliDate,
	noteOrderDate: validateNepaliDate,
	allowanceDate: validateNepaliDate,
	plinthDetails: validateNullableNumber,
	buildCoverArea: validateNullableNumber,
	coverageDetails: validateNullableNumber,
	buildingArea: validateNullableNumber,
	coverageArea: validateNullableNumber,
	houseLength: validateNullableNumber,
	houseBreadth: validateNullableNumber,
	houseHeight: validateNullableNumber,
	buildFloorInfo: validateNullableNumber,
	buildApprovFloor: validateNullableNumber,
	buildHeightInfo: validateNullableNumber,
	buildApprovHeight: validateNullableNumber,
	gharDhaniDate: validateNepaliDate,
});
export const KamalamaiSchema = Yup.object().shape({
	date: validateNepaliDate,
	jaggaDate: validateNepaliDate,
});

export default SuperStruConsSchema;
