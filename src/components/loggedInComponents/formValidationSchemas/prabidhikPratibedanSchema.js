import * as Yup from 'yup';
import { isKageyshowri } from '../../../utils/clientUtils';
import { appListDataFields } from '../../../utils/jqxUtils';
import {
	validateNullableZeroNumber,
	validateNullableNepaliDate,
	validateMinDistanceField,
	validateArrayNullableNumber,
	validateRequiredNullableNormalNumber,
} from '../../../utils/validationUtils';

const numberFields = ['publicPropertyRequiredDistance', 'batoChaudai'];

const numberSchema = numberFields.map((row) => {
	return { [row]: validateNullableZeroNumber };
});

export const prabidhikSchemaKamalamai = (isPuranoGhar) =>
	Yup.object().shape(
		Object.assign(
			{
				prabidhikPratibedanDate: validateNullableNepaliDate,
				surjaminDate: validateNullableNepaliDate,
				landLength: isKageyshowri ? validateRequiredNullableNormalNumber : null,
				buildingFloorHeight: isKageyshowri ? validateRequiredNullableNormalNumber : null,
				landWidth: isKageyshowri ? validateRequiredNullableNormalNumber : null,
				buildingFloorHeight: validateRequiredNullableNormalNumber,
				publicPropertyDistance: isPuranoGhar ? validateNullableZeroNumber : validateMinDistanceField('publicPropertyRequiredDistance'),
				highTension: Yup.array().of(
					Yup.object().shape({
						value: validateNullableZeroNumber,
					})
				),
				setBack: Yup.array().of(
					Yup.object()
						.shape({
							distanceFromRoadCenter: validateArrayNullableNumber,
							distanceFromRoadBoundary: validateArrayNullableNumber,
							distanceFromRoadRightEdge: validateArrayNullableNumber,
						})
						.nullable()
				),
			},
			...numberSchema
		)
	);
