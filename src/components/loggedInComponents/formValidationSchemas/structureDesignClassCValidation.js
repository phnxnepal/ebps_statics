import * as Yup from 'yup';
import {
	validateString,
	// validateNullableNormalNumber,
	// validateRequiredNullableNormalNumber,
	validateNumber,
	validateNullableNumber,
} from '../../../utils/validationUtils';
import {
	classCplinthArea,
	commonMessages,
	classCStorey,
	numberMessages,
	classCBuildingHeight,
	classCBeamSpan,
	classCPanelArea,
	classCOverallDimension,
	classCRedundancy,
	classCCantilever,
	classCGradeofConcrete,
	classCTraverseReinfo,
	classCJointReinfo,
} from '../../../utils/data/validationData';

// let numberValidate, stringValidate;

// numberValidate = Yup.number().lessThan(max: 1000).typeError('Must be a number').positive('Must be positive number').required('Required field.');
// stringValidate =  Yup.string().required('Required field');

export const validatePlinthArea = Yup.number()
	.max(1000, classCplinthArea.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateStoreyNo = Yup.number()
	.max(3, classCStorey.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateBuildingHeight = Yup.number()
	.max(11, classCBuildingHeight.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateBeamSpan = Yup.number()
	.max(4.5, classCBeamSpan.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validatePanelArea = Yup.number()
	.max(13.5, classCPanelArea.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateOverallDimension = buildingBreadth => {
	return Yup.number().when(buildingBreadth, (buildingBreadthValue, schema) => {
		if (buildingBreadthValue) {
			return Yup.string()
				.typeError(numberMessages.number)
				.test('lengthThreeTimes', classCOverallDimension.max, value => parseFloat(value) <= 3 * parseFloat(buildingBreadthValue));
		}
	});
};

export const validateMinNoofBays = Yup.number()
	.typeError(numberMessages.number)
	.positive(numberMessages.positive)
	.min(2, classCRedundancy.min);

export const validateCantilever = Yup.number()
	.max(1, classCCantilever.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateGradeofConcrete = Yup.number()
	.typeError(numberMessages.number)
	.min(20, classCGradeofConcrete.min);

export const validateTraverseReinfo = Yup.number()
	.typeError(numberMessages.number)
	.min(8, classCTraverseReinfo.min);

export const validateJointReinfo = Yup.number()
	.max(8, classCJointReinfo.max)
	.typeError(numberMessages.number)
	.min(0, numberMessages.positive);

export const validateDimensionBreadth = Yup.number().typeError(numberMessages.number);

export const StructureDesignCSchema = Yup.object().shape({
	'1_A_1_qty': validatePlinthArea.required(commonMessages.required),
	'1_A_2_noofstorey_qty': validateStoreyNo.required(commonMessages.required),
	'1_A_2_totalheightofbuilding_qty': validateBuildingHeight.required(commonMessages.required),
	'1_A_3_qty': validateBeamSpan.required(commonMessages.required),
	'1_A_4_qty': validatePanelArea.required(commonMessages.required),
	'1_A_5_Length_qty': validateOverallDimension('1_A_5_Breadth_qty').required(commonMessages.required),
	'1_A_5_Breadth_qty': validateNumber,
	'1_A_6_MinNoofBays_qty': validateMinNoofBays.required(commonMessages.required),
	'1_A_7_qty': validateString,
	'1_A_8_qty': validateString,
	'1_A_9_qty': validateString,
	'1_A_10_qty': validateString,
	'1_A_11_qty': validateCantilever.required(commonMessages.required),
	'1_A_12_qty': validateString,
	'1_B_14_qty': validateString.required(commonMessages.required),
	'1_B_17_qty': validateTraverseReinfo,
	'1_B_18_qty': validateString,
	'1_B_23_noofreinforcementatjoint_qty': validateJointReinfo,
	'1_C_26_sill-band-and-lintel-band': validateString,
	'1_C_26_no-of': validateString,
	'1_C_26_bars-with': validateString,
	'1_C_26_c-hook': validateString,
});

export const StructureDesigneCBirtamodSchema = hasBlocks => {
	if (hasBlocks) {
		return Yup.object().shape({
			'1_A_1_qty': validatePlinthArea.nullable(),
			// '1_A_2_noofstorey_qty': validateStoreyNo.nullable(),
			'1_A_2_noofstorey_qty': Yup.array()
				.of(validateNullableNumber)
				.nullable(),
			'1_A_2_totalheightofbuilding_qty': Yup.array()
				.of(validateBuildingHeight.nullable())
				.nullable(),
			'1_A_3_qty': validateBeamSpan.nullable(),
			'1_A_4_qty': validatePanelArea.nullable(),
			'1_A_5_Length_qty': validateOverallDimension('1_A_5_Breadth_qty').nullable(),
			'1_A_5_Breadth_qty': validateNullableNumber,
			'1_A_6_MinNoofBays_qty': validateMinNoofBays.nullable(),
			// '1_A_7_qty': validateString,
			// '1_A_8_qty': validateString,
			// '1_A_9_qty': validateString,
			// '1_A_10_qty': validateString,
			'1_A_11_qty': validateCantilever.nullable(),
			// '1_A_12_qty': validateString,
			// '1_B_14_qty': validateString.nullable(),
			'1_B_17_qty': validateTraverseReinfo,
			// '1_B_18_qty': validateString,
			'1_B_23_noofreinforcementatjoint_qty': validateJointReinfo,
			// '1_C_26_sill-band-and-lintel-band': validateString,
			// '1_C_26_no-of': validateString,
			// '1_C_26_bars-with': validateString,
			// '1_C_26_c-hook': validateString,
		});
	} else {
		return Yup.object().shape({
			'1_A_1_qty': validatePlinthArea.nullable(),
			// '1_A_2_noofstorey_qty': validateStoreyNo.nullable(),
			'1_A_2_noofstorey_qty': validateNullableNumber,
			'1_A_2_totalheightofbuilding_qty': validateBuildingHeight.nullable(),
			'1_A_3_qty': validateBeamSpan.nullable(),
			'1_A_4_qty': validatePanelArea.nullable(),
			'1_A_5_Length_qty': validateOverallDimension('1_A_5_Breadth_qty').nullable(),
			'1_A_5_Breadth_qty': validateNullableNumber,
			'1_A_6_MinNoofBays_qty': validateMinNoofBays.nullable(),
			// '1_A_7_qty': validateString,
			// '1_A_8_qty': validateString,
			// '1_A_9_qty': validateString,
			// '1_A_10_qty': validateString,
			'1_A_11_qty': validateCantilever.nullable(),
			// '1_A_12_qty': validateString,
			// '1_B_14_qty': validateString.nullable(),
			'1_B_17_qty': validateTraverseReinfo,
			// '1_B_18_qty': validateString,
			'1_B_23_noofreinforcementatjoint_qty': validateJointReinfo,
			// '1_C_26_sill-band-and-lintel-band': validateString,
			// '1_C_26_no-of': validateString,
			// '1_C_26_bars-with': validateString,
			// '1_C_26_c-hook': validateString,
		});
	}
};

export const StructureDesignCPurano = Yup.object().shape({
	'1_A_1_qty': validatePlinthArea,
	'1_A_2_noofstorey_qty': validateNullableNumber,
	// '1_A_2_noofstorey_qty': validateStoreyNo,
	'1_A_2_totalheightofbuilding_qty': validateBuildingHeight,
	'1_A_3_qty': validateBeamSpan,
	'1_A_4_qty': validatePanelArea,
	'1_A_5_Length_qty': validateOverallDimension('1_A_5_Breadth_qty'),
	'1_A_5_Breadth_qty': validateDimensionBreadth,
	'1_A_6_MinNoofBays_qty': validateMinNoofBays,
	'1_A_11_qty': validateCantilever,
	// '1_B_14_qty': validateGradeofConcrete,
	'1_B_17_qty': validateTraverseReinfo,
	'1_B_23_noofreinforcementatjoint_qty': validateJointReinfo,
});

export default StructureDesignCSchema;
