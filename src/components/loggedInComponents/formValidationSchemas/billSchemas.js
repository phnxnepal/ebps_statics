import { validateNumber, validateNullableOfficialReqNumbers, validateString, validateNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';

export const billSchema = Yup.object().shape(
	Object.assign({
		vuktaniRakam: validateNumber,
		rashidNumber: validateNullableOfficialReqNumbers,
		receiveSign: validateString,
		miti: validateNepaliDate,
	})
);
