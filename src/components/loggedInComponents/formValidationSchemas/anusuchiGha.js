import * as Yup from 'yup';
import {
	validateNullableOfficialNumbers,
	validateNotReqdPhone,
	validateWardNo,
	// validateNormalNumber,
	validateNepaliDate,
	validatePhoneNumber,
	validateNullableOfficialReqNumbers,
	validateString,
	normalNepaliDate,
} from '../../../utils/validationUtils';

// const AnusuchiGhaData = [];

// const numNepaliData = [
// 	'dEngineeringCouncil',
// 	'dMunicipalRegNo'
// ]

const numNepaliWithSlashData = ['dMunicipalRegNo', 'tDarta', 'mDarta'];

const validPhone = ['dMobile', 'tPhone', 'mPhone'];

const strData = ['thapTalla'];

// const numData = [];

// const numericalNepaliSchema = numNepaliData.map(row => {
// 	return {
// 		[row]: validateNullableNumber
// 	};
// });

const numericalWithSlashNepaliSchema = numNepaliWithSlashData.map((row) => {
	return {
		[row]: validateNullableOfficialNumbers,
	};
});

// const numericalSchema = numData.map(row => {
// 	return {
// 		[row]: validateNormalNumber,
// 	};
// });

// const requiredSchema = AnusuchiGhaData.map(row => {
// 	return {
// 		[row]: validateString,
// 	};
// });

const validPhoneSchema = validPhone.map((row) => {
	return {
		[row]: validateNotReqdPhone,
	};
});

const formDataSchema = strData.map((row) => {
	return {
		[row]: validateWardNo,
	};
});

export const anusuchiGhaSchema = Yup.object().shape(
	Object.assign(
		{
			dDate: validateNepaliDate,
		},
		// ...requiredSchema,
		// ...numericalSchema,
		// ...numericalNepaliSchema,
		...validPhoneSchema,
		...numericalWithSlashNepaliSchema,
	)
);
export const kagesworiGhaSchema = Yup.object().shape(
	Object.assign(
		{
			dDate: validateNepaliDate,
		},
		// ...requiredSchema,
		// ...numericalSchema,
		// ...numericalNepaliSchema,
		...validPhoneSchema,
		...numericalWithSlashNepaliSchema,
	)
);

export const kamalamaiAnuGhaSchema = Yup.object().shape(
	Object.assign(
		{
			dMobile: validateNotReqdPhone,
			dDate: validateNepaliDate,
			dMunicipalRegNo: validateNullableOfficialNumbers,
			mPhone: validatePhoneNumber,
			mDarta: validateNullableOfficialReqNumbers,
			mAddress: validateString,
			mistiriName: validateString,

			// Dakarmi Fields Required in Kamalamai
			tPhone: validateNotReqdPhone,
			tDarta: validateNullableOfficialNumbers,
		},
	)
);
