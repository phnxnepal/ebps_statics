import * as Yup from 'yup';
import {
	validateNullableNumber,
	validateArrayNullableNumber,
	// validateString,
	validateNormalNumber,
	validateNullableZeroNumber,
	// validateNullableNormalNumber
} from '../../../utils/validationUtils';
import {
	commonMessages,
	numberMessages,
} from '../../../utils/data/validationData';
import { isStringEmpty } from '../../../utils/stringUtils';
import { yupToFormErrors } from 'formik';

const validateStringNullable = Yup.string()
	.typeError(commonMessages.required)
	.nullable();

const validateMinDistanceField = requiredDistanceField => {
	return Yup.number()
		.when(requiredDistanceField, (requiredDistance, schema) => {
			if (requiredDistance) {
				return Yup.string()
					.matches(/^(\d+.\d+|\d*)$/, numberMessages.number)
					.test(
						'sadak',
						numberMessages.moreThan,
						value => parseFloat(value) >= parseFloat(requiredDistance)
					);
			}
		})
		.typeError(numberMessages.number)
};

export const MapTechValidateSchema = (floorDetailKeys, floorSchema, buildingHeightSchema, isPuranoGhar = false) => Yup.object().shape({
	landLength: validateNullableNumber,
	landWidth: validateNullableNumber,
	fieldDetails: validateNullableNumber,
	fieldLength: validateNullableNumber,
	fieldWidth: validateNullableNumber,
	plinthDetails: validateNullableNumber,
	plinthLength: validateNullableNumber,
	plinthWidth: validateNullableNumber,
	coverageHeight: validateNullableNumber,
	coverageWidth: validateNullableNumber,
	descriptionFloor: validateNullableNumber,
	descriptionFloorWidth: validateNullableNumber,
	requiredDistance0: validateNullableNumber,
	requiredDistance1: validateNullableNumber,
	requiredDistance2: validateNullableNumber,
	requiredDistance3: validateNullableNumber,
	floorDetails: Yup.array().of(
		Yup.object()
			.shape(floorDetailKeys)
			.nullable()
	),
	floor: Yup.array().of(
		Yup.object()
			.shape(floorSchema)
			.nullable()
	).nullable(),
	setBack: Yup.array().of(
		Yup.object()
			.shape({
				distanceFromRoadCenter: validateArrayNullableNumber,
				distanceFromRoadBoundary: validateArrayNullableNumber,
				distanceFromRoadRightEdge: validateArrayNullableNumber,
			})
			.nullable()
	),
	wallToBorder: Yup.array().of(
		Yup.object()
			.shape({
				minimumSpacing: validateArrayNullableNumber,
				existsingSpacing: validateArrayNullableNumber,
				windowDoorExists: validateStringNullable,
				roadExists: validateStringNullable,
			})
			.nullable()
	),
	// publicProperty: Yup.array().of(
	// 	Yup.object()
	// 		.shape({
	// 			propValue: validateNullableNumber,
	// 		})
	// 		.nullable()
	// ),
	publicPropertyDistance: isPuranoGhar ? validateNullableZeroNumber : validateMinDistanceField('publicPropertyRequiredDistance'),
	cantilever: Yup.array().of(
		Yup.object()
			.shape({
				frontSide: validateArrayNullableNumber,
				backSide: validateArrayNullableNumber,
				rightSide: validateArrayNullableNumber,
				leftSide: validateArrayNullableNumber,
			})
			.nullable()
	),
	highTension: Yup.array().of(
		Yup.object().shape({
			value: validateArrayNullableNumber,
		}).nullable()
	),
	/**
	 * @TODO Find a better way to loop through array later
	 */
	sadakAdhikarKshytra0: isPuranoGhar ? validateArrayNullableNumber : validateMinDistanceField('requiredDistance0').required(commonMessages.required),
	sadakAdhikarKshytra1: isPuranoGhar ? validateArrayNullableNumber : validateMinDistanceField('requiredDistance1').required(commonMessages.required),
	sadakAdhikarKshytra2: isPuranoGhar ? validateArrayNullableNumber : validateMinDistanceField('requiredDistance2').required(commonMessages.required),
	sadakAdhikarKshytra3: isPuranoGhar ? validateArrayNullableNumber : validateMinDistanceField('requiredDistance3').required(commonMessages.required),
	otherArea: validateNullableNumber,
	availableArea: validateNullableNumber,
	...buildingHeightSchema,
	// buildingHeight: validateNumber,
	// buildingDetailfloor: stringValidate,
	allowable: validateNormalNumber,
	coveragePercent: validateNormalNumber,
	coverageDetails: validateNormalNumber,
	coverageArea: validateNullableNumber,
	// publicPropertyName: validateString,
});

export const validateAdhikarValue = (value, limit) => {
	let error;

	if (isStringEmpty(value)) return '';

	if (!/^[0-9]+$/.test(value)) return numberMessages.number;

	if (isStringEmpty(limit)) return '';

	if (parseFloat(value) < parseFloat(limit)) {
		error = numberMessages.moreThan;
	}

	yupToFormErrors(error);
	return error;
};

export default MapTechValidateSchema;
