import {
	validateNullableZeroNumber,
	validateNullableNepaliDate,
	validateString,
	validateNullableOfficialReqNumbers,
	validateWardNo,
	validatePhoneNumber,
	validateNumberWithDash,
} from '../../../utils/validationUtils';
import { unitMessages, surroundingErrors, commonMessages, memberErrors, landDetailsErrors, floorErrors } from '../../../utils/data/validationData';
import * as Yup from 'yup';

export const validateSurrounding = Yup.array().of(
	Yup.object().shape({
		feet: validateNullableZeroNumber,
		sideUnit: Yup.string().when('feet', {
			is: (val) => val,
			then: Yup.string().typeError(unitMessages.required).required(unitMessages.required),
		}),
	})
);

export const validateSurroundingNotice = Yup.array().of(
	Yup.object().shape({
		feet: validateNullableZeroNumber,
		afnoJagga: validateNullableZeroNumber,
		sideUnit: Yup.string().when(['feet', 'afnoJagga'], {
			is: (feet, afnoJagga) => !!(feet || afnoJagga),
			then: Yup.string().required(unitMessages.required).typeError(unitMessages.required),
			otherwise: Yup.string().nullable(),
		}),
	})
);

export const validateSurroundingKamalamai = Yup.array().of(
	Yup.object().shape({
		feet: validateNullableZeroNumber,
		actualSetback: validateNullableZeroNumber,
		roadWidth: validateNullableZeroNumber,
		roadStandardSetback: validateNullableZeroNumber,
		distanceFromHighTensionLine: validateNullableZeroNumber,
		distanceFromRiver: validateNullableZeroNumber,
		distanceFromBorder: validateNullableZeroNumber,
		landScapeType: Yup.string().nullable(),
		landScapeName: Yup.string().nullable(),
		doorOrWindows: Yup.string().nullable(),
		sideUnit: Yup.string().when('feet', {
			is: (val) => val,
			then: Yup.string().required(unitMessages.required),
		}),
		kittaNo: Yup.string().when(
			[
				'actualSetback',
				'feet',
				'roadWidth',
				'roadStandardSetback',
				'distanceFromHighTensionLine',
				'distanceFromRiver',
				'distanceFromBorder',
				'landScapeType',
				'landScapeName',
				'doorOrWindows',
			],
			{
				is: (
					actualSetback,
					feet,
					roadWidth,
					roadStandardSetback,
					distanceFromHighTensionLine,
					distanceFromRiver,
					distanceFromBorder,
					landScapeType,
					landScapeName,
					doorOrWindows
				) =>
					!!(
						actualSetback ||
						feet ||
						roadWidth ||
						roadStandardSetback ||
						distanceFromHighTensionLine ||
						distanceFromRiver ||
						distanceFromBorder ||
						landScapeType ||
						landScapeName ||
						doorOrWindows
					),
				then: Yup.string().required(surroundingErrors.kittaNoRequired),
			}
		),
	})
);

// Yup Validation Schemas
const strData = [
	'applicantName',
	'applicantAddress',
	'fathersSpouseName',
	'oldMunicipal',
	'sadak',
	'nearestLocation',
	'buildingJoinRoad',
	// 'kittaNo',
	// 'landArea',
	'landRegNo',
	'citizenshipNo',
	'nibedakName',
	'nibedakSadak',
	'applicantDate',
];

const numNepaliData = [
	// 'newWardNo',
	// 'oldWardNo',
	// 'landArea',
	'tol',
	// 'nibedakTol'
];

const numSlashData = ['citizenshipNo'];

const validPhone = ['applicantMobileNo'];

const validWardNo = ['newWardNo', 'oldWardNo', 'nibedakTol'];

// const numData = [''];

const formDataSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});

const numericalNepaliSchema = numNepaliData.map((row) => {
	return {
		[row]: validateNumberWithDash,
	};
});

const validPhoneSchema = validPhone.map((row) => {
	return {
		[row]: validatePhoneNumber,
	};
});

const validWardNoSchema = validWardNo.map((row) => {
	return {
		[row]: validateWardNo,
	};
});

// const numericalSchema = numData.map(row => {
//   return {
//     [row]: validateNormalNumber
//   };
// });

const validSlashSchema = numSlashData.map((row) => {
	return {
		[row]: validateNullableOfficialReqNumbers,
	};
});

export const BuildingPermitSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: validateSurrounding,
			kittaNo: Yup.array(validateString).required(commonMessages.required),
			landArea: Yup.array(validateString).required(commonMessages.required),
			landDetails: Yup.array().min(1, landDetailsErrors.atLeastOne),
			floor: Yup.array()
				.of(
					Yup.object().shape({
						length: validateNullableZeroNumber,
						width: validateNullableZeroNumber,
						height: validateNullableZeroNumber,
						area: validateNullableZeroNumber,
					})
				)
				.test('test floor length', floorErrors.atLeastOne, (v) => v.length > 0),
			landCertificateIssueDate: validateNullableNepaliDate,
			landPassDate: validateNullableNepaliDate,
			citizenshipIssueDate: validateNullableNepaliDate,
			oldMapDate: validateNullableNepaliDate,
			floorUnit: validateString,
			// applicantDateBS: validateNepaliDate,
		},
		...formDataSchema,
		// ...numericalSchema,
		...numericalNepaliSchema,
		...validPhoneSchema,
		...validWardNoSchema,
		...validSlashSchema
	)
);

export const BuildingPermitKamalamaiSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: validateSurroundingKamalamai,
			kittaNo: Yup.array(validateString).required(commonMessages.required),
			landArea: Yup.array(validateString).required(commonMessages.required),
			landDetails: Yup.array().min(1, landDetailsErrors.atLeastOne),
			member: Yup.array().required(commonMessages.required).min(4, memberErrors.atLeastFour),
			floor: Yup.array()
				.of(
					Yup.object().shape({
						length: validateNullableZeroNumber,
						width: validateNullableZeroNumber,
						height: validateNullableZeroNumber,
						area: validateNullableZeroNumber,
					})
				)
				.test('test floor length', floorErrors.atLeastOne, (v) => v.length > 0),
			landCertificateIssueDate: validateNullableNepaliDate,
			landPassDate: validateNullableNepaliDate,
			citizenshipIssueDate: validateNullableNepaliDate,
			oldMapDate: validateNullableNepaliDate,
			floorUnit: validateString,
			// applicantDateBS: validateNepaliDate,
		},
		...formDataSchema,
		// ...numericalSchema,
		...numericalNepaliSchema,
		...validPhoneSchema,
		...validWardNoSchema,
		...validSlashSchema
	)
);
