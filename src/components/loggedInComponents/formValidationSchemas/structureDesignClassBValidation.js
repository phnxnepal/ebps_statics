import * as Yup from 'yup';
import { validateString, validateRequiredNullableNormalNumber } from '../../../utils/validationUtils';

// let numberValidate, stringValidate;

// numberValidate =  Yup.number().typeError('Must be a number').positive('Must be positive number').required('Required field.');
// stringValidate =  Yup.string().required('Required field');

export const StructureDesignBSchema = hasBlock => {
	if (hasBlock) {
		return Yup.object().shape({
			'1_1_qty': Yup.array().of(validateRequiredNullableNormalNumber).nullable(),
			'1_2_qty': Yup.array().of(validateRequiredNullableNormalNumber).nullable(),
			// '1_3_qty': validateString,
			// '1_4_qty': validateString,
			// ['1_6_qty']: validateRequiredNullableNormalNumber,
			'2_1_qty': validateString,
			propOccpuType: validateString,
		});
	} else {
		return Yup.object().shape({
			'1_1_qty': validateRequiredNullableNormalNumber,
			'1_2_qty': validateRequiredNullableNormalNumber,
			// '1_3_qty': validateString,
			// '1_4_qty': validateString,
			// ['1_6_qty']: validateRequiredNullableNormalNumber,
			'2_1_qty': validateString,
			propOccpuType: validateString,
		});
	}
};

export const StructureDesignBBirtamodSchema = Yup.object().shape({
	'1_1_qty': validateRequiredNullableNormalNumber,
	'1_2_qty': validateRequiredNullableNormalNumber,
	// '1_3_qty': validateString,
	// '1_4_qty': validateString,
	// ['1_6_qty']: validateRequiredNullableNormalNumber,
	'2_1_qty': validateString,
	propOccpuType: validateString,
});

export default StructureDesignBSchema;
