import * as Yup from 'yup';
import { validateNotReqdPhone, validateNepaliDate } from '../../../utils/validationUtils';

let stringValidation;

stringValidation = Yup.string()
	.typeError('शब्द​ हुनु पर्छ')
	.required('यो भर्नु आवश्यक छ');

export const SurjaminValidateSchema = Yup.object().shape({
	aminName: stringValidation,
	prashashanName: stringValidation,
	wardOfficerPost: stringValidation,
	wardOfficerName: stringValidation,
	wardOfficerNumber: validateNotReqdPhone,
	surjMuchulkaDate: validateNepaliDate,
	surjaminDate: validateNepaliDate,
});

export default SurjaminValidateSchema;
