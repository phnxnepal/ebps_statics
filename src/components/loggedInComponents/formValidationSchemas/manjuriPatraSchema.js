import * as Yup from 'yup';
import { validateNotReqdPhone, validateNepaliDate, validateNullableOfficialNumbers, validateString } from '../../../utils/validationUtils';

export const ManjuriPatraSchema = Yup.object().shape(
	Object.assign({
        dName: validateString,
        dMobile: validateNotReqdPhone,
        dMunicipalRegNo: validateNullableOfficialNumbers,
		dDate: validateNepaliDate,
	})
);
