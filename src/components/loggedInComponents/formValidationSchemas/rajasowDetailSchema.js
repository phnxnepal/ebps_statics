import * as Yup from 'yup';
import {
	validateMinDistanceField,
	validateMoreThanZeroNumber,
	validateNumber,
	validateNullableNumber,
	validateNullableNormalNumber,
	validateNullableZeroNumber,
} from '../../../utils/validationUtils';

export const getRajasowSchema = (isKankai, getSchema, isPuranoGhar) => {
	// const numData = ['allowableHeight'];
	// const numData = ['allowableHeight', 'constructionHeight', 'requiredDistance', 'namedMapdanda', 'namedAdhikar'];
	const notreqData = [
		// 'namedMapdanda',
		'namedMeasurement',
		// 'namedAdhikar',
		'namedBorder',
		'namedDistance',
		// 'requiredDistance',
		'unnamedMapdanda',
		'unnamedMeasurement',
		'unnamedAdhikar',
		'unnamedBorder',
		'unnamedDistance',
		'constructionHeight',
		'requiredDistance',
		'namedMapdanda',
		'namedAdhikar',
	];
	// const detailsField = [
	// 	'dArea',
	// 	'depositAmount',
	// 	'depositRate',
	// 	'drAmount',
	// 	'drRate',
	// 	'floor',
	// 	'jArea',
	// 	'jType',
	// 	'jrAmount',
	// 	'jrRate',
	// 	'totalAmount',
	// ];

	const detailsData = ['detailsTotal', 'anyeAmt', 'aminAmt', 'applicationAmt', 'totalAmt', 'formAmt', 'jaribanaDasturAmt'];

	// const strData = ['roadName'];

	// const numSchema = numData.map((rows) => {
	// 	return {
	// 		[rows]: validateNumber,
	// 	};
	// });
	const notreqSchema = notreqData.map((rows) => {
		return {
			[rows]: validateNullableNormalNumber,
		};
	});

	const detailSchema = detailsData.map((rows) => {
		return {
			[rows]: validateMoreThanZeroNumber,
		};
	});

	// const strSchema = strData.map(rows => {
	// 	return {
	// 		[rows]: validateString,
	// 	};
	// });

	const rajaswoSchema = Yup.object().shape(
		Object.assign({ ...getSchema(['allowableHeight', 'constructionHeight'], validateNumber) }, ...notreqSchema, ...detailSchema, {
			floor: Yup.array().of(
				Yup.object().shape({
					...getSchema(['length', 'width', 'area'], validateMoreThanZeroNumber),
					// length: validateMoreThanZeroNumber,
					// width: validateMoreThanZeroNumber,

					// area: validateMoreThanZeroNumber,
				})
			),
			details: Yup.array().of(
				Yup.object().shape({
					dArea: validateMoreThanZeroNumber,
					// drRate: validateNotLessThanOneNumber.required(),
					drAmount: validateMoreThanZeroNumber,
					depositRate: validateMoreThanZeroNumber,
					jDepositRate: validateMoreThanZeroNumber,
					depositAmount: validateMoreThanZeroNumber,
					jDepositAmount: validateMoreThanZeroNumber,
					jArea: validateMoreThanZeroNumber,
					jrAmount: validateMoreThanZeroNumber,
					jrRate: validateMoreThanZeroNumber,
				})
			),
			// sadakAdhikarKshytra: validateMinDistanceField('requiredDistance').required(commonMessages.required),
			sadakAdhikarKshytra: isPuranoGhar ? validateNullableZeroNumber : validateMinDistanceField('requiredDistance').nullable(),
		})
	);

	const rajaswoKankaiSchema = Yup.object().shape(
		Object.assign({ ...getSchema(['allowableHeight', 'constructionHeight'], validateNumber) }, ...notreqSchema, ...detailSchema, {
			requiredDistance: validateNullableNumber,
			namedMapdanda: validateNullableNumber,
			namedAdhikar: validateNullableNumber,
			// allowableHeight: validateNumber,
			// constructionHeight: validateNumber,
			floor: Yup.array().of(
				Yup.object().shape({
					...getSchema(['length', 'width', 'area'], validateMoreThanZeroNumber),
					// length: validateMoreThanZeroNumber,
					// width: validateMoreThanZeroNumber,

					// area: validateMoreThanZeroNumber,
				})
			),
			details: Yup.array().of(
				Yup.object().shape({
					dArea: validateMoreThanZeroNumber,
					// drRate: validateNotLessThanOneNumber.required(),
					drAmount: validateMoreThanZeroNumber,
					depositRate: validateMoreThanZeroNumber,
					jDepositRate: validateMoreThanZeroNumber,
					depositAmount: validateMoreThanZeroNumber,
					jDepositAmount: validateMoreThanZeroNumber,
					jArea: validateMoreThanZeroNumber,
					jrAmount: validateMoreThanZeroNumber,
					jrRate: validateMoreThanZeroNumber,
				})
			),
			sadakAdhikarKshytra: isPuranoGhar ? validateNullableZeroNumber : validateMinDistanceField('requiredDistance').nullable(),
		})
	);

	return isKankai ? rajaswoKankaiSchema : rajaswoSchema;
};

export const getRajasowTippaniSchema = (getSchema) => {
	const detailsData = ['detailsTotal', 'anyeAmt', 'aminAmt', 'applicationAmt', 'totalAmt', 'formAmt', 'jaribanaDasturAmt'];

	const detailSchema = detailsData.map((rows) => {
		return {
			[rows]: validateMoreThanZeroNumber,
		};
	});

	const rajaswoSchema = Yup.object().shape(
		Object.assign(...detailSchema, {
			floor: Yup.array().of(
				Yup.object().shape({
					...getSchema(['length', 'width', 'area'], validateMoreThanZeroNumber),
				})
			),
			details: Yup.array().of(
				Yup.object().shape({
					dArea: validateMoreThanZeroNumber,
					// drRate: validateNotLessThanOneNumber.required(),
					drAmount: validateMoreThanZeroNumber,
					depositRate: validateMoreThanZeroNumber,
					jDepositRate: validateMoreThanZeroNumber,
					depositAmount: validateMoreThanZeroNumber,
					jDepositAmount: validateMoreThanZeroNumber,
					jArea: validateMoreThanZeroNumber,
					jrAmount: validateMoreThanZeroNumber,
					jrRate: validateMoreThanZeroNumber,
				})
			),
		})
	);
	return rajaswoSchema;
};
