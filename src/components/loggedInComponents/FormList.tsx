import React, { Component } from 'react';
import { Segment, Header, Form, Input } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { getUserData } from '../../store/actions/loginForm';

class FormList extends Component {
  componentDidMount() {
    //@ts-ignore
    this.props.getUserData();
  }

  render() {
    //@ts-ignore
    const { userName, address, joinDate } = this.props.userData;

    // console.log('profile props', this.props);
    return (
      <Segment raised>
        <Header as="h3" block>
          डिजाइनर
        </Header>
        <Segment>
          <Form.Field
            control={Input}
            label="Username"
            value={userName}
            disabled
          />
          <Form.Field
            control={Input}
            label="Address"
            value={address}
            disabled
          />
          <Form.Field
            control={Input}
            label="Joined on"
            value={joinDate}
            disabled
          />
        </Segment>
      </Segment>
    );
  }
}

//@ts-ignore
const mapStateToProps = state => ({
  userData: state.root.login
});

//@ts-ignore
const mapDispatchToProps = {
  getUserData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FormList);
