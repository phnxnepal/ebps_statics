import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { Input, Header, Icon, Divider } from 'semantic-ui-react';
import { useLocation } from 'react-router';
import { getSetupMenuByUser } from '../../utils/menuUtils';
import { UserType } from '../../utils/userTypeUtils';

const SetupSidebar = ({ userType, routes, setupTags }) => {
	const [stateRoutes, setStateRoutes] = useState(routes);
	const { pathname } = useLocation();

	useEffect(() => {
		const mapScroll = document.querySelector('.sideBar.active');
		mapScroll && mapScroll.scrollIntoView({ behavior: 'smooth', block: 'start' });
	}, [pathname]);

	useEffect(() => {
		// if (userType === UserType.ENGINEER) {
		// 	const userTypeMenu = getSetupMenuByUser(userType);
		// 	setStateRoutes(routes.filter(menu => userTypeMenu.includes(menu.id)));
		// } else {
			setStateRoutes(routes)
		// }
	}, [userType, routes]);

	const handleSearch = searchString => {
		const searchRegex = new RegExp(searchString, 'gi');
		let routesToSearch = routes;
		if (userType === UserType.ENGINEER) {
			const userTypeMenu = getSetupMenuByUser(userType);
			routesToSearch = routes.filter(menu => userTypeMenu.includes(menu.id));
		}

		setStateRoutes(
			routesToSearch.filter(row => {
				return row.id.match(searchRegex) || row.path.match(searchRegex) || row.layout.match(searchRegex) || row.tag.match(searchRegex);
			})
		);
	};

	return (
		<aside className="main-sidebar-navigation">
			<div className="sidebarSearchBar">
				<Input placeholder="Search..." name="search" onChange={(e, { value }) => handleSearch(value)} />
			</div>
			<nav className="sidebar-navigation">
				<ul className="sidebar-primary-menus">
					{setupTags
						.filter(obj => stateRoutes.some(menu => menu.tag === obj.tag))
						.map(({ tag, label, icon }, index) => (
							<React.Fragment key={index}>
								<Header as="h4" className="header-removemargin">
									<Icon name={icon} />
									{label}
								</Header>
								{stateRoutes
									.filter(item => item.tag === tag)
									.map((row, key) => {
										const value = row.name;
										return (
											<li key={key}>
												<NavLink to={`${row.layout}${row.path}`} className="sideBar">
													<Icon name={row.icon} /> {value}
												</NavLink>
												<Divider style={{ margin: 0 }} />
											</li>
										);
									})}
							</React.Fragment>
						))}
				</ul>
			</nav>
		</aside>
	);
};

export default SetupSidebar;
