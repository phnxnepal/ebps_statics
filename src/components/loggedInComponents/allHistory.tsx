import React from 'react';
import { Header } from 'semantic-ui-react';
import { getUserTypeValueNepali, getUserStatusNepali } from '../../utils/functionUtils';
import FallbackComponent from '../shared/FallbackComponent';
import { historyComment } from '../../utils/data/headFootLangFile';
import { getMessage } from '../shared/getMessage';

//redux
import { connect } from 'react-redux';
import { postFormDataByUrl, getMultipleFormDataByUrl } from '../../store/actions/formActions';
import api from '../../utils/api';
import ErrorDisplay from '../shared/ErrorDisplay';
// import RootGrid from "./Grid/RootGrid";
import JqxGrid, { IGridProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import RootGrid from './Grid/RootGrid';
import Helmet from 'react-helmet';
import { getLocalStorage } from '../../utils/secureLS';
import { LSKey } from '../../utils/enums/localStorageKeys';
import { UserTypeMaster, UserType } from '../../utils/userTypeUtils';

const history_data = historyComment.hc_data;
const messageId = 'historyComment.hc_data';

interface HistoryState {
	dataChanged: boolean;
	historyData: object;
	userTypeMaster: UserTypeMaster[];
}

class allHistory extends React.PureComponent<{}, IGridProps & HistoryState> {
	constructor(props: {}) {
		super(props);
		this.state = {
			dataChanged: false,
			historyData: [],
			userTypeMaster: [],
		};
	}

	componentDidMount() {
		const userTypeMaster = JSON.parse(getLocalStorage(LSKey.USER_TYPE_ALL));
		this.setState({ userTypeMaster });
		//@ts-ignore
		this.props.getMultipleFormDataByUrl([{ api: api.allhistory, objName: 'allHistory', form: true }], (data) => data);
	}

	//@ts-ignore
	componentDidUpdate(prevProps, prevState) {
		const { userTypeMaster } = this.state;

		const getUserTypeNepali = (userType: UserType) => {
			const userTypeObject = userTypeMaster.find((uT) => uT.userType === userType);
			if (userTypeObject) {
				return userTypeObject.designation;
			} else {
				getUserTypeValueNepali(userType);
			}
		};

		//@ts-ignore
		if (this.props.prevData !== prevProps.prevData) {
			this.setState({
				historyData: Object.assign(
					[],
					//@ts-ignore
					this.props.prevData.map((row) => {
						return {
							...row,
							status: getUserStatusNepali(row.status),
							userType: getUserTypeNepali(row.userType),
						};
					})
				),
			});
		}
	}

	//@ts-ignore
	private myGrid = React.createRef<JqxGrid>();

	datafield = [
		{ name: 'name', type: 'string' },
		{ name: 'actionDate', type: 'string' },
		{ name: 'actionTime', type: 'string' },
		{ name: 'status', type: 'string' },
		{ name: 'userType', type: 'string' },
		{ name: 'enterBy', type: 'string' },
	];

	columns = [
		{
			text: 'फारम नाम',
			dataField: 'name',
			// width: '350',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
		},
		{
			text: 'मिति',
			datafield: 'actionDate',
			width: '100',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'समय',
			datafield: 'actionTime',
			width: '90',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'स्थिति',
			datafield: 'status',
			cellsalign: 'center',
			align: 'center',
			width: '150',
			filtertype: 'list',
		},
		{
			text: 'प्रयोगकर्ताको प्रकार',
			datafield: 'userType',
			width: '130',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
		},
		{
			text: 'प्रयोगकर्ताको नाम',
			datafield: 'enterBy',
			width: '180',
			cellsalign: 'center',
			align: 'center',
		},

		// {
		//   text: getMessage(`${messageId}.date`, history_data.date),
		//   datafield: 'actionDate'
		// },
		// {
		//   text: getMessage(`${messageId}.time`, history_data.time),
		//   datafield: 'actionTime'
		// },
		// {
		//   text: getMessage(`${messageId}.status`, history_data.status),
		//   datafield: 'status'
		// },
		// {
		//   text: getMessage(`${messageId}.user`, history_data.user),
		//   datafield: 'userType'
		// }
	];

	render() {
		//@ts-ignore
		if (this.props.prevData) {
			//@ts-ignore

			return (
				<div className="view-Form">
					<Helmet>
						<title>All History</title>
					</Helmet>
					{/* 
          //@ts-ignore */}
					{this.props.errors && (
						//@ts-ignore
						<ErrorDisplay message={this.props.errors.message} />
					)}
					<Header as="h3" dividing>
						{getMessage(`${messageId}.hist`, history_data.hist)}
					</Header>
					{/* 
          //@ts-ignore */}
					<RootGrid
						//@ts-ignore
						localdata={this.state.historyData}
						datafield={this.datafield}
						columns={this.columns}
						groupable={false}
						showfilterrow={true}
					/>
				</div>
			);
		} else {
			return (
				<FallbackComponent
					//@ts-ignore
					loading={this.props.loading}
					//@ts-ignore
					errors={this.props.errors}
				/>
			);
		}
	}
}

//@ts-ignore
const mapStateToProps = (state) => ({
	prevData: state.root.formData.allHistory,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
});
const mapDispatchToProps = { getMultipleFormDataByUrl, postFormDataByUrl };

export default connect(mapStateToProps, mapDispatchToProps)(allHistory);
