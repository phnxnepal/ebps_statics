import React, { Component } from 'react';
import Printthis from '../shared/Printthis';
import { setPrint } from '../../store/actions/general';
import { connect } from 'react-redux';
import Printstylenone from '../shared/Printstylenone';
import MapPermintApplicationView from './forms/MapPermintApplicationView';
import { makeprintformat, getBuildPermitObj } from '../../utils/functionUtils';
import Loading from '../shared/Loading';
import { Header } from 'semantic-ui-react';
import ShortcutHtml from '../shared/ShortcutHtml';
import StructureDesignClassBPrintForm from './forms/anusuchiKha/StructureDesignClassBPrintForm';
import StructureDesignClassCPrintView from './forms/anusuchiKha/StructureDesignClassCPrintView';
import { PrintIdentifiers, PrintParams } from '../../utils/printUtils';
import { round } from '../../utils/mathUtils';
import Helmet from 'react-helmet';

// let printcol = [];
let b = 0;
let final = [];
const onBeforeGetContent = {
  ...PrintParams.INLINE_FIELD,
  param1: ['getElementsByTagName', 'input', 'value'],
  param2: ['getElementsByClassName', 'ui basic label', 'innerText'],
  param3: ['getElementsByClassName', 'ui label', 'innerText'],
  param4: ['getElementsByClassName', 'ui dropdown', 'innerText'],
  param5: ['getElementsByTagName', 'textarea', 'value'],
  param6: [PrintIdentifiers.GENERIC_REMOVE_ON_PRINT],
  param7: ['15dayspecial'],
  param8: ['removeOnPrint'],
};

class PrintAll extends Component {
  constructor(props) {
    super(props);

    const sortedRoutes = this.props.routes.slice().filter(route => String(route.position) !== '100').map(route => route.hasChild ? route.children : route).flat().map(route => ({...route, position: round(Number(route.position) * 100) })).sort((a, b) => {
      return Number(a.position) > Number(b.position) ? 1 : -1;
    })

    const permitData = getBuildPermitObj();
    this.state = {
      printcol: {},
      position: {},
      loading: true,
      show: true,
      sortedRoutes,
      permitData
    };
    // console.log(this.props.routes);
    // this.totalComponent =  this.props.routes.slice().filter(route => String(route.position) !== '100').sort((a, b) => {
    //   return Number(a.position) * 100 > Number(b.position) * 100 ? 1 : -1;
    // }).reduce(
    //   (acc, each) =>
    //     acc +
    //     (each.hasChild ? each.children.length : String(each.position) !== '100' && 1),
    //   0
    // );
    this.totalComponent = sortedRoutes.length
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    let a = {...nextState.printcol}
    // console.log(a);
    // console.log(Object.keys(a).length);
    // console.log(this.totalComponent);

    if (Object.keys(a).length === this.totalComponent) {
      // document.getElementsByClassName(
      //   'ok'
      // )[0].firstChild.firstChild.firstChild.innerText = a.filter(
      //   each => each
      // ).length;
      if (++b === 1) {
        final = { ...a };
        this.setState({ loading: false });
        this.setState({ show: false });
      }
    }

    if (this.state.loading !== nextState.loading) {
      return true;
    }

    return false;
  };

  append = () => {
    // let printcol = this.state.printcol.filter(each => each);

    let root = document.createElement('div');

    // for (let i = 0; i < final.length; i++) {
    //   final[i].style.pageBreakAfter = 'always';
    //   root.appendChild(final[i]);
    // }

    Object.entries(final).sort((a, b) => Number(a[0]) > Number(b[0]) ? 1 : -1).forEach(([key, div]) => 
     { div.style.pageBreakAfter = 'always';
      root.appendChild(div)}

      )

    return root;
  };

  render() {
    // const { routes } = this.props;
    const {sortedRoutes} = this.state;

    // console.log('sosos', sortedRoutes);
    // debugger;
    let collection =
      this.state.show &&
      sortedRoutes.map((prop, key) => {
        // if (key < 2) {
        // if (String(prop.position) !== '100') {
          if (prop.hasChild) {
            return prop.children.map(child => {
              let Torender = child.component;

              if (prop.path === '/user/forms/structure-design-classB-form') {
                Torender = StructureDesignClassBPrintForm
              }

              if (prop.path === '/user/forms/structure-design-classc-form') {
                Torender = StructureDesignClassCPrintView
              }

              return (
                <Torender
                  {...this.props}
                  key={key + Math.random()}
                  setRef={ref => {
                    // printcol[child.id - 1] = ref;
                    this.setState(prev => {
                      let a = { ...prev.printcol };
                      a[`${child.position}.${child.id}`] = ref;
                      return {
                        printcol: a,
                        // position: { ...prev.position, [`${child.position}.${child.id}`]: child.name }
                      };
                    });
                  }}
                />
              );
            });
          } else {
            let Torender = prop.component;

            if (prop.path === '/user/forms/map-permit-application-edit') {
              Torender = MapPermintApplicationView;
            }

            

            return (
              <Torender
                {...this.props}
                key={key + Math.random()}
                setRef={ref => {
                  // printcol[prop.id - 1] = ref;
                  this.setState(prev => {
                    let a = { ...prev.printcol };
                    a[`${prop.position}.${prop.id}`] = ref;
                    return {
                      printcol: a,
                        // position: { ...prev.position, [`${prop.position}.${prop.id}`]: prop.name }
                    };
                  });
                }}
              />
            );
          }
        // } else return null;
        // }
      });

    return (
      <>
				<Helmet title={this.state.permitData.id} />
        <Loading loading={this.state.loading}> </Loading>
        {!this.state.loading && (
          <Printthis
            ui={
              <div style={{ cursor: 'pointer', display: 'inline-block' }}>
                <Header as="h3" block>
                  Continue Printing
                  {this.props.shortcut && <ShortcutHtml content="[alt + p]" />}
                </Header>{' '}
              </div>
            }
            content={() => this.content}
            onBeforeGetContent={() => {
              this.props.setPrint(true);
              this.content = this.append();

              Object.values(onBeforeGetContent).forEach(each => {
                makeprintformat(this.content, ...each);
              });
            }}
            onBeforePrint={() => {
              this.props.setPrint(false);
            }}
          />
        )}

        <Printstylenone> {collection} </Printstylenone>
      </>
    );
  }
}

const mapDispatchToProps = {
  setPrint: res => setPrint(res)
};
const mapStateToProps = state => {
  return {
    printcontent: state.root.general.printcontent,
    shortcut: state.root.general.shortcut
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PrintAll);
