import React from 'react';
import { Header } from 'semantic-ui-react';
import { historyComment } from '../../utils/data/headFootLangFile';
import { getMessage } from '../shared/getMessage';

//redux
import { connect } from 'react-redux';
import { getDataByUrl } from '../../store/actions/dashboardActions';

import api from '../../utils/api';
import ErrorDisplay from '../shared/ErrorDisplay';
// import RootGrid from "./Grid/RootGrid";
import JqxGrid, { IGridProps } from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import RootGrid from './Grid/RootGrid';
import { ActivitySearchForm } from './shared/SearchForm';
import { UIState } from '../../interfaces/ReduxInterfaces';
import { OrganizationUser } from '../../interfaces/DashboardInterfaces';

const history_data = historyComment.hc_data;
const messageId = 'historyComment.hc_data';

interface ReduxFunctions {
	getDataByUrl: Function;
}

type Api = { api: string };
type ReduxData = { activityData: object[]; apiList: Api[]; userList: OrganizationUser[] };

type Props = UIState & ReduxFunctions & ReduxData;

interface LocalState {
	dataChanged: boolean;
	data: object[];
	apiList: object[];
	userList: object[];
}

type State = IGridProps & LocalState;

class ActivityLog extends React.PureComponent<Props, State> {
	constructor(props: Props) {
		super(props);
		this.state = {
			dataChanged: false,
			data: [],
			apiList: [],
			userList: [],
		};
	}

	componentDidMount() {
		this.props.getDataByUrl([{ api: api.apiList, objName: 'apiList' }]);
		this.props.getDataByUrl([{ api: api.organizationUser, objName: 'userList' }]);
	}

	//@ts-ignore
	componentDidUpdate(prevProps) {
		//@ts-ignore
		if (this.props.activityData !== prevProps.activityData && this.props.activityData && Array.isArray(this.props.activityData)) {
			this.setState({
				data: this.props.activityData.map(data => {
					//@ts-ignore
					const accessDate = new Date(data.accessTime);
					return {...data, accessTime: accessDate.toUTCString()}
				}),
			});
		}

		if (this.props.apiList !== prevProps.apiList) {
			this.setState({ apiList: this.props.apiList.map(api => ({ value: api.api, text: api.api })).concat({ value: '', text: 'Select an API' }) });
		}

		if (this.props.userList !== prevProps.userList) {
			this.setState({
				userList: this.props.userList.map(user => ({ value: user.userName, text: user.userName })).concat({ value: '', text: 'Select a user' }),
			});
		}
	}

	//@ts-ignore
	private myGrid = React.createRef<JqxGrid>();

	datafield = [
		{ name: 'loginBy', type: 'string' },
		{ name: 'api', type: 'string' },
		{ name: 'method', type: 'string' },
		{ name: 'activity', type: 'string' },
		{ name: 'accessTime', type: 'string' },
	];

	columns = [
		{
			text: 'Login By',
			dataField: 'loginBy',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
		},
		{
			text: 'API ',
			datafield: 'api',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
			width: '300',
		},
		{
			text: 'API Method',
			datafield: 'method',
			cellsalign: 'center',
			align: 'center',
			filtertype: 'list',
			width: '80',
		},
		{
			text: 'Activity',
			datafield: 'activity',
			cellsalign: 'center',
			align: 'center',
		},
		{
			text: 'Access Time',
			datafield: 'accessTime',
			cellsalign: 'center',
			align: 'center',
		},

		// {
		//   text: getMessage(`${messageId}.date`, history_data.date),
		//   datafield: 'actionDate'
		// },
		// {
		//   text: getMessage(`${messageId}.time`, history_data.time),
		//   datafield: 'actionTime'
		// },
		// {
		//   text: getMessage(`${messageId}.status`, history_data.status),
		//   datafield: 'status'
		// },
		// {
		//   text: getMessage(`${messageId}.user`, history_data.user),
		//   datafield: 'userType'
		// }
	];

	render() {
		//@ts-ignore
		// if (this.props.prevData) {
		//@ts-ignore
		const { loading, errors } = this.props;
		const { apiList, userList } = this.state;
		return (
			<div className="english-div">
				{errors && <ErrorDisplay message={errors.message} />}
				<ActivitySearchForm
					heading="Search Activity Log"
					getDataByUrl={this.props.getDataByUrl}
					loading={loading}
					apiList={apiList}
					userList={userList}
				/>
				<Header as="h3" dividing>
					{getMessage(`${messageId}.hist`, history_data.hist)}
				</Header>
				{/* 
          //@ts-ignore */}
				<RootGrid
					//@ts-ignore
					localdata={this.state.data}
					datafield={this.datafield}
					columns={this.columns}
					groupable={false}
					showfilterrow={true}
				/>
			</div>
		);
	}
}

//@ts-ignore
const mapStateToProps = state => ({
	activityData: state.root.dashboard.activity,
	apiList: state.root.dashboard.apiList,
	userList: state.root.dashboard.userList,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
});

const mapDispatchToProps = { getDataByUrl };

export default connect(mapStateToProps, mapDispatchToProps)(ActivityLog);
