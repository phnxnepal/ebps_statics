import React, { Component } from 'react';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import Leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';

Leaflet.Icon.Default.imagePath = '../node_modules/leaflet';

delete Leaflet.Icon.Default.prototype._getIconUrl;

Leaflet.Icon.Default.mergeOptions({
	iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
	iconUrl: require('leaflet/dist/images/marker-icon.png'),
	shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

class MyMapComponent extends Component {
	constructor() {
		super();
		this.state = {
			zoom: 15,
		};
	}
	render() {
        const { position, marker } = this.props;
		return (
			<Map style={{ width: '100%', height: '300px' }} center={position} zoom={this.state.zoom} onClick={this.props.handleMarkerClick}>
				<TileLayer
					url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
				/>
				{marker && (
					<Marker position={marker}>
						<Popup>
							<span>Location</span>
						</Popup>
					</Marker>
				)}
			</Map>
		);
	}
}

export default MyMapComponent;
