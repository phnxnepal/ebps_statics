import React, { Component } from 'react';

import { Form } from 'semantic-ui-react';
import { Formik, Field } from 'formik';
import api from '../../utils/api';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess } from '../../utils/dataUtils';
import ErrorDisplay from '../shared/ErrorDisplay';
import * as Yup from 'yup';
import { validateNumber, validateString, validateNullableOfficialReqNumbers, validateNepaliDate } from '../../utils/validationUtils';
import EbpsForm, { EbpsNumberForm } from '../shared/EbpsForm';
import { rajaswoFormLang } from '../../utils/data/rajaswoDetailLang';
import { LangDateField } from '../shared/DateField';
import { isStringEmpty } from '../../utils/stringUtils';
import { getCurrentDate } from '../../utils/dateUtils';
import { getUserRole } from './../../utils/functionUtils';
import { NepaliNumberToWord } from '../../utils/nepaliAmount';
import { GenericApprovalFileView } from '../shared/file/GenericApprovalFileView';
import { FormUtility } from '../../utils/formUtils';
import { FormUrlFull } from '../../utils/enums/url';
import SaveButtonValidation from '../shared/SaveButtonValidation';
import FormContainerV2 from '../../containers/base/FormContainerV2';

const rajaswoForm = rajaswoFormLang;
const numSlashData = ['rashidNumber'];

const validSlashSchema = numSlashData.map(row => {
	return {
		[row]: validateNullableOfficialReqNumbers,
	};
});

const schema = Yup.object().shape(
	Object.assign(
		{
			// uploadFile: validateFile,
			amtReceivedDate: validateNepaliDate,
			// amountInWords: validateString,
			amtOfDharauti: validateNumber,
			receiveSign: validateString,
		},
		...validSlashSchema
	)
);
class RajaswoVoucherComponent extends Component {
	constructor(props) {
		super(props);

		let jsonData = checkError(this.props.prevData);
		const rajaswaEntry = getJsonData(this.props.otherData.rajaswaEntryV);
		const amtOfDharauti = isStringEmpty(rajaswaEntry.totalAmt) ? 0 : rajaswaEntry.totalAmt;

		// console.log('rajaa', rajaswaEntry, this.state);

		let RInfo = '';
		if (getUserRole() === 'R') {
			RInfo = this.props.userData.userName;
		}

		const initialValues = prepareMultiInitialValues(
			{
				obj: { amountInWords: NepaliNumberToWord.getNepaliWord(amtOfDharauti) },
				reqFields: ['amountInWords'],
			},
			{
				obj: { receiveSign: RInfo },
				reqFields: [],
			},
			{
				obj: { ...{ amtOfDharauti: amtOfDharauti }, ...jsonData },
				reqFields: [],
			}
		);

		// console.log('jsondata', jsonData, initialValues);
		// const udata = this.props.userData;

		if (isStringEmpty(initialValues.amtReceivedDate)) {
			initialValues.amtReceivedDate = getCurrentDate(true);
		}

		this.state = {
			initialValues,
		};
	}

	render() {
		const { initialValues } = this.state;
		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<Formik
				initialValues={initialValues}
				validationSchema={schema}
				onSubmit={async (values, { setSubmitting }) => {
					// values.applicationNo = this.props.permitData.applicantNo;
					const data = new FormData();

					const selectedFile = values.uploadFile;

					// let isFileSelected = false;
					if (selectedFile) {
						for (var x = 0; x < selectedFile.length; x++) {
							data.set('file', selectedFile[x]);
						}
						// isFileSelected = true;
						values.uploadFile && delete values.uploadFile;
					}

					Object.keys(values).forEach(key => {
						data.set(key, values[key]);
					});

					try {
						await this.props.postAction(`${api.rajaswaVoucher}${this.props.permitData.applicantNo}`, data, false, false);

						window.scroll(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast(this.props.success.data.message || 'Data saved successfully');

							// setTimeout(() => {
							// 	this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname));
							// }, 2000);
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							setSubmitting(false);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
				render={({ handleChange, values, isSubmitting, handleSubmit, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
						<div ref={this.props.setRef}>
							<div className="rajaswosSubForm forRajaswoOffice">
								<br />
								<h1>
									<center>{rajaswoForm.rajaswoVoucer.formTitle}</center>
								</h1>
								<br />
								<h3>
									<span>{rajaswoForm.formSecond.heading}</span>
								</h3>
								<div>
									<EbpsNumberForm
										label={rajaswoForm.formSecond.content_lin1}
										name="amtOfDharauti"
										disabled={true}
										onChange={handleChange}
										value={values.amtOfDharauti}
										error={errors.amtOfDharauti}
									/>
									{/* {initialValues.amountInWords && ( */}
									<Form.Field>
										{rajaswoForm.formSecond.content_lin2}
										<Field name="amountInWords" value={NepaliNumberToWord.getNepaliWord(values.amtOfDharauti)} readOnly />
									</Form.Field>
									{/* )} */}
									{/* {rajaswoForm.formSecond.content_lin3} */}
									{/* <Field name="amtReceivedDate" className="dashedForm-control" /> */}
									<LangDateField
										label={rajaswoForm.formSecond.content_lin3}
										name="amtReceivedDate"
										setFieldValue={setFieldValue}
										value={values.amtReceivedDate}
										error={errors.amtReceivedDate}
									/>
									<EbpsNumberForm
										label={rajaswoForm.formSecond.content_lin4}
										name="rashidNumber"
										onChange={handleChange}
										value={values.rashidNumber}
										error={errors.rashidNumber}
									/>
									<EbpsForm
										name="receiveSign"
										label={rajaswoForm.formSecond.content_lin5}
										value={values['receiveSign']}
										onChange={handleChange}
										error={errors.receiveSign}
										setFieldValue={setFieldValue}
									/>
									{/* {rajaswoForm.formSecond.content_lin4} */}
									{/* <Field name="rashidNumber"/> */}
									{/* {rajaswoForm.formSecond.content_lin5} */}
									{/* <Field name="receiveSign" /> */}
								</div>
							</div>
							<br />
						</div>
						<br />

						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			/>
		);
	}
}

const RajaswoVoucher = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.rajaswaVoucher, objName: 'rajaswoVoucher', form: true },
			{ api: api.rajaswaEntry, objName: 'rajaswaEntryV', form: false },
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		formUtility={new FormUtility(FormUrlFull.RAJASOW_VOUCHER)}
		// hasFile={true}
		hasFileView={true}
		render={props =>
			props.hasSavePermission ? (
				<RajaswoVoucherComponent {...props} parentProps={parentProps} />
			) : (
				<div ref={props.setRef}>
					<GenericApprovalFileView
						fileCategories={props.fileCategories}
						files={props.files}
						url={FormUrlFull.RAJASOW_VOUCHER}
						prevData={checkError(props.prevData)}
					/>
				</div>
			)
		}
	/>
);

export default RajaswoVoucher;
