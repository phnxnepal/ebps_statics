import React from 'react';
import { headfoot } from '../../utils/data/headFootLangFile';
import { getMessage } from '../shared/getMessage';

const hf_data = headfoot.headfoot_data.footer;
const messageId = 'headfoot.headfoot_data.footer';

const Footer: React.FC = () => {
	const currentYear = new Date().getFullYear();
	return (
		<React.Fragment>
			<footer className="site-footer">
				{getMessage(`${messageId}.footerNepali`, hf_data.footerNepali)}{' '}
				<span className="english-div">
					&copy; 2019-{currentYear} {getMessage(`${messageId}.footerEng`, hf_data.footerEng)}
				</span>
			</footer>
		</React.Fragment>
	);
};

export default Footer;
