import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput, DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import {
	getJsonData,
	prepareMultiInitialValues,
	floorMappingFlat,
	handleSuccess,
	checkError,
	FloorArray,
	floorMappingNepali,
} from '../../../../utils/dataUtils';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { DashedMultiUnitLengthInput, DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import _ from 'lodash';
import { ilamNewCertificate } from '../../../../utils/data/illam/ilamnayapramanpatradata';
import { FooterSignature } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { SectionHeader } from '../../../uiComponents/Headers';

const illamData = ilamNewCertificate;

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];
const sanhita_opt = [
	{ key: 1, value: 'पूर्ण पालना', text: 'पूर्ण पालना' },
	{ key: 2, value: 'आंसिक पालना', text: 'आंसिक पालना' },
	{ key: 3, value: 'नभएको', text: 'नभएको' },
];
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

const convertbib = (inputIndex) => {
	if (inputIndex === 'A') {
		return '(क)';
	} else if (inputIndex === 'B') {
		return '(ख)';
	} else if (inputIndex === 'C') {
		return '(ग)';
	} else {
		return '(घ)';
	}
};

class BuildingFinishCertificateView extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_LENGTH } = this.props;

		let initialValues = {};
		const mapTech = getJsonData(otherData.mapTech);
		const rajaswo = getJsonData(otherData.rajaswo);
		const plinthlvl = getJsonData(otherData.plinthlvl);
		const supernirmaanKarya = getJsonData(otherData.supernirmaanKarya);
		// const organization = otherData.organization;
		const anuSucKaJsonData = otherData.anukaMaster;
		const desApprovJsonData = otherData.designApprovalData;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let inputIndex = buildingClass ? buildingClass.id : '';

		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		const formattedFloors = floorArray.getFormattedFloors();
		const floorCount = floorArray.getHeightRelevantFloors();
		const floorMax =
			floorCount && floorCount.length > 0 && floorMappingNepali.find((fl) => fl.floor === _.maxBy(floorCount, 'floor').floor).value;

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['floor', 'photo'],
			},
			{
				obj: {
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['purposeOfConstruction', 'allowable', 'cType'],
			},
			{
				obj: rajaswo,
				reqFields: ['requiredDistance', 'sadakAdhikarUnit', 'sadakAdhikarKshytra'],
			},
			{
				obj: plinthlvl,
				reqFields: ['letterSubmitDate'],
			},
			{
				obj: supernirmaanKarya,
				reqFields: ['nirmanKaryaDate'],
			},
			{
				obj: {
					buildclass: inputIndex,
				},
				reqFields: [],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		const initVal = merge(initialValues, initialValues1);
		// console.log(initVal);
		this.state = {
			initVal,
			floorArray,
			floorMax,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray, floorMax } = this.state;
		const user_info = this.props.userData;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="print-small-font">
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<div
										style={{
											display: 'flex',
											justifyContent: 'space-between',
										}}
									>
										<div>
											<div>
												{illamData.header.pa_sa}
												<DashedLangInputWithSlash
													name="BuildPasa"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.BuildPasa}
													error={errors.BuildPasa}
												/>
											</div>
											<div>
												{illamData.header.cha_na}
												<DashedLangInputWithSlash
													name="Buildcana"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.Buildcana}
													error={errors.Buildcana}
													inline={true}
												/>
											</div>
										</div>
										<SectionHeader>
											<h3 className="underline bottom-margin top-margin">{illamData.header.title}</h3>
										</SectionHeader>
										{/* </div> */}
										<div>
											<div style={{ textAlign: 'right' }}>
												{illamData.header.date}
												<DashedLangDateField name="Bdate" value={values.Bdate} setFieldValue={setFieldValue} inline={true} />
											</div>
										</div>
									</div>
									<br />
									<div className="no-margin-field">
										{illamData.data.shree} {permitData.nibedakName}
										{','}
										{illamData.data.sabik} {userData.organization.name}
										{illamData.data.wada} {permitData.oldWardNo}
										{illamData.data.haal} {permitData.newMunicipal}
										{illamData.data.wada} {permitData.newWardNo}
										{', '}
										<DashedLangInput
											name="jilla"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.jilla}
											className="dashedForm-control"
										/>
										{illamData.data.jilla} {userData.organization.province}
										{/* <span className="ui input dashedForm-control " /> */}
										{illamData.data.pradesh}
									</div>
									<br />
									<div className="no-margin-field">
										<span className="indent-span">{illamData.content.content_1}</span>
										{userData.organization.name} {illamData.data.wada}
										{permitData.newWardNo} {illamData.content.content_2}
										{permitData.kittaNo} {illamData.content.content_3}
										{permitData.landArea} {permitData.landAreaType}
										{illamData.content.content_4} {floorMax}
										{illamData.content.content_5}
										<DashedLangDateField
											name="letterSubmitDate"
											value={values.letterSubmitDate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{illamData.content.content_6}
										<DashedLangDateField
											name="nirmanKaryaDate"
											value={values.nirmanKaryaDate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{illamData.content.content_7}
									</div>
									<table className="borderless-table">
										<tr>
											<th>{illamData.content.content_8}</th>
											<th>{illamData.content.content_9}</th>
											<th>{illamData.content.content_10}</th>
											<th>{illamData.content.content_11}</th>
											<th>{illamData.content.content_12}</th>
										</tr>

										{permitData.floor &&
											permitData.floor.length > 0 &&
											permitData.floor.map((floor, index) => (
												<>
													<tr>
														<td>{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}</td>
														<td>
															<DashedLengthInputWithRelatedUnits
																name={`floor.${index}.length`}
																unitName={`floorUnit`}
																relatedFields={[...floorArray.getAllFields(`floor.${index}.length`)]}
															/>
														</td>
														{/* </div> */}
														<td>
															<DashedLengthInputWithRelatedUnits
																name={`floor.${index}.width`}
																unitName={`floorUnit`}
																relatedFields={[...floorArray.getAllFields(`floor.${index}.width`)]}
															/>
														</td>
														<td>
															<DashedLengthInputWithRelatedUnits
																name={`floor.${index}.height`}
																unitName={`floorUnit`}
																relatedFields={[...floorArray.getAllFields(`floor.${index}.height`)]}
															/>
														</td>
														<td>
															<DashedAreaInputWithRelatedUnits
																name={`floor.${index}.area`}
																unitName={`floorUnit`}
																squareOptions={squareUnitOptions}
																relatedFields={[...floorArray.getAllFields(`floor.${index}.area`)]}
															/>
														</td>
													</tr>

													{/* {` ${getUnitValue(floor.floorUnit)}`} */}
												</>
											))}
									</table>

									<div>
										{illamData.content.content_13}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.purposeOfConstruction}
											className="dashedForm-control"
										/>
										<span style={{ marginLeft: '8rem' }}>
											{illamData.content.content_14}
											{illamData.content.content_14_input.map((input) => (
												// <Table.Cell>
												<div className="ui radio checkbox prabidhik">
													<input
														type="radio"
														name="buildclass"
														defaultChecked={values.buildclass === input}
														value={input}
													/>
													<label>{convertbib(input)}</label>
												</div>
												// </Table.Cell>
											))}
										</span>
									</div>

									<div>
										{illamData.content.content_15}
										<DashedLangInput
											name="cType"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.cType}
											className="dashedForm-control"
										/>
										<span style={{ marginLeft: '8rem' }}>
											{illamData.content.content_16}
											<DashedLangInput
												name="allowable"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.allowable}
												className="dashedForm-control"
											/>
										</span>
									</div>

									<div>
										{illamData.content.content_17}
										<Select
											options={sanhita_opt}
											name="sanhita"
											placeholder="पूर्ण पालना"
											onChange={(e, { value }) => setFieldValue('sanhita', value)}
											value={values['sanhita']}
											error={errors.sanhita}
											// defaultValue={props.values['nikash']}
										/>
									</div>

									<div>
										<DashedMultiUnitLengthInput
											name="requiredDistance"
											unitName="sadakAdhikarUnit"
											relatedFields={['sadakAdhikarKshytra']}
											squareOptions={distanceOptions}
											label={illamData.content.content_18}
										/>
										<DashedMultiUnitLengthInput
											name="sadakAdhikarKshytra"
											unitName="sadakAdhikarUnit"
											relatedFields={['requiredDistance']}
											areaField={'requiredDistance'}
											label={illamData.content.content_19}
										/>
									</div>

									<div>
										{illamData.content.content_20}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>

									<FooterSignature
										designations={[
											illamData.footer.footer_1,
											illamData.footer.footer_2,
											illamData.footer.footer_3,
											illamData.footer.footer_4,
										]}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishView = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthlvl',
				form: false,
			},
			{
				api: api.supernirmaanKarya,
				objName: 'supernirmaanKarya',
				form: false,
			},
		]}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param1: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateView {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishView;
