import React, { Component, useState, useEffect } from 'react';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import api from '../../../utils/api';
import { isEmpty, showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { Formik, FieldArray, getIn } from 'formik';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { SectionHeader } from '../../uiComponents/Headers';
import { DashedLangInput, DashedNormalInputIm } from '../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, checkError, surroundingMappingFlat, distanceOptions, handleSuccess } from '../../../utils/dataUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { Form, Table, Button } from 'semantic-ui-react';
import { napiNirikshyanData, landAreaCalculationData } from '../../../utils/data/napiNirikshyanPratibedanData';
import { RadioInput } from '../../shared/formComponents/RadioInput';
import { DashedUnitInput, DashedAreaUnitInput, UnitDropdownWithRelatedFields } from '../../shared/EbpsUnitLabelValue';
import EbpsTextareaField from '../../shared/MyTextArea';
import { FlexSingleLeft, FlexSingleRight } from '../../uiComponents/FlexDivs';
import UnitDropdown from '../../shared/UnitDropdown';
import { PermitAreaInput } from '../../shared/EbpsForm';
import { areaUnitOptions, landAreaTypeOptionsKamalamai } from '../../../utils/optionUtils';
import { getSaveByUserDetails } from '../../../utils/formUtils';
import { DetailedSignature } from './formComponents/FooterSignature';
import { validateString, validateNullableZeroNumber, validateArrayNullableNumber } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { round, getFormattedNumber } from '../../../utils/mathUtils';
import { RoadSetbackTable } from './mapTechFormComponents/RoadSetbackTable';
import { landDetailsData } from '../../../utils/data/mapPermit';
import { BuildPermitHelper } from './mapPermitComponents/BuildPermitHelpers';
import { commonMessages, landDetailsErrors } from '../../../utils/data/validationData';

const data = napiNirikshyanData;
const landData = landAreaCalculationData;

const validationSchema = Yup.object().shape({
	kittaNo: Yup.array(validateString).required(commonMessages.required),
	landArea: Yup.array(validateString).required(commonMessages.required),
	landDetails: Yup.array().min(1, landDetailsErrors.atLeastOne),
	landAreaFeet: validateNullableZeroNumber,
	applicantName: validateString,
	applicantAddress: validateString,
	nibedakName: validateString,
	landAreaCalculation: Yup.array().of(
		Yup.object().shape({
			sideA: validateNullableZeroNumber,
			sideB: validateNullableZeroNumber,
			sideC: validateNullableZeroNumber,
			area: validateNullableZeroNumber,
		})
	),
	setBack: Yup.array().of(
		Yup.object()
			.shape({
				distanceFromRoadCenter: validateArrayNullableNumber,
				distanceFromRoadBoundary: validateArrayNullableNumber,
				distanceFromRoadRightEdge: validateArrayNullableNumber,
			})
			.nullable()
	),
});

class NapiNirikshyanPratibedanComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, permitData, enterByUser, otherData, userData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		const json_data = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const [landAreaValue] = BuildPermitHelper.splitIntoArray(permitData, 'landArea');
		const [kittaNoValue, hasMultipleKittaNo] = BuildPermitHelper.splitIntoArray(permitData, 'kittaNo');
		let landDetails = [''];
		if (hasMultipleKittaNo) {
			landDetails = Array.apply(null, { length: landAreaValue.length }).map(() => '');
		}
		initVal = prepareMultiInitialValues(
			{
				obj: {
					plinthAreaUnit: DEFAULT_UNIT_AREA,
					otherMohodaUnit: DEFAULT_UNIT_LENGTH,
					gharMohodaUnit: DEFAULT_UNIT_LENGTH,
					sideAUnit: DEFAULT_UNIT_LENGTH,
					sideBUnit: DEFAULT_UNIT_LENGTH,
					sideCUnit: DEFAULT_UNIT_LENGTH,
					areaUnit: DEFAULT_UNIT_AREA,
					landAreaFeetUnit: DEFAULT_UNIT_AREA,
					setBackUnit: DEFAULT_UNIT_LENGTH,
					plinthDetailsUnit: DEFAULT_UNIT_AREA,
					nirikshyanDate: getCurrentDate(true),
					landAreaCalculation: [{ sn: '1', sideA: 0, sideB: 0, sideC: 0, area: 0, shape: 'Rectangle', sym: '' }],
					/**
					 * for @fields subName and subDesignation
					 */
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['isHighTensionLine', 'surrounding', 'applicantName', 'nibedakName', 'applicantAddress', 'landAreaType'],
			},
			{
				obj: { landArea: landAreaValue, kittaNo: kittaNoValue, landDetails },
				reqFields: [],
			},
			{ obj: mapTech, reqFields: ['plinthDetails', 'plinthDetailsUnit', 'setBackUnit', 'setBack'] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const user_info = this.props.userData;
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, useSignatureImage } = this.props;
		return (
			<div className="view-Form">
				{/* {reduxErrors && <ErrorDisplay message={reduxErrors.message} />} */}
				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={validationSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.aminKoSthalgatPratibedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors, handleChange }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<br />
									<SectionHeader>
										<h2 className="underline end-section">{data.title}</h2>
									</SectionHeader>
								</div>
								<div className="no-margin-field">
									<div>
										{data.jaggaDhani}{' '}
										<DashedLangInput
											name="applicantName"
											setFieldValue={setFieldValue}
											value={values.applicantName}
											error={errors.applicantName}
										/>
									</div>
									<div>
										{data.naksaWala}{' '}
										<DashedLangInput
											name="nibedakName"
											setFieldValue={setFieldValue}
											value={values.nibedakName}
											error={errors.nibedakName}
										/>
									</div>
									<div>
										{data.nirmanSthan}{' '}
										<DashedLangInput
											name="applicantAddress"
											setFieldValue={setFieldValue}
											value={values.applicantAddress}
											error={errors.applicantAddress}
										/>
									</div>
									<div>
										{data.nirmanJaggaBibaran}
										<div className="div-indent">
											<FieldArray
												name="landDetails"
												render={(arrayHelpers) => (
													<div>
														{errors.landDetails && <span className="tableError">{errors.landDetails}</span>}
														{values.landDetails && values.landDetails.length > 0 ? (
															values.landDetails.map((row, index) => (
																<Form.Group inline className="">
																	{data.kittaNo}
																	<DashedLangInput
																		compact={true}
																		name={`kittaNo.${index}`}
																		error={getIn(errors, `kittaNo.${index}`)}
																		value={getIn(values, `kittaNo.${index}`)}
																		setFieldValue={setFieldValue}
																	/>
																	<PermitAreaInput
																		label={data.area}
																		placeholder={values.landAreaType}
																		dashed={true}
																		name={`landArea.${index}`}
																		error={getIn(errors, `landArea.${index}`)}
																		value={getIn(values, `landArea.${index}`)}
																		setFieldValue={setFieldValue}
																		unitComponent={
																			<>
																				<UnitDropdown
																					name="landAreaType"
																					value={values.landAreaType}
																					error={errors.landAreaType}
																					setFieldValue={setFieldValue}
																					options={landAreaTypeOptionsKamalamai}
																				/>{' '}
																				{' / '}
																				<DashedAreaUnitInput
																					compact={true}
																					name="landAreaFeet"
																					unitName="landAreaFeetUnit"
																				/>
																			</>
																		}
																	/>
																	<Form.Button
																		size="tiny"
																		className="danger-text-button remove-on-print"
																		content={landDetailsData.removeRow}
																		onClick={() => arrayHelpers.remove(index)}
																		icon="close"
																	/>
																	<Form.Button
																		content={landDetailsData.addRow}
																		size="tiny"
																		className="light-text-button remove-on-print"
																		onClick={() => {
																			arrayHelpers.insert(index + 1, '');
																			setFieldValue(`kittaNo.${index + 1}`, '');
																			setFieldValue(`landArea.${index + 1}`, '');
																		}}
																		icon="plus"
																	/>
																</Form.Group>
															))
														) : (
															<div>
																<Form.Button
																	content={landDetailsData.addNewRow}
																	size="tiny"
																	className="light-text-button"
																	onClick={() => {
																		arrayHelpers.push('');
																		setFieldValue(`kittaNo.${0}`, '');
																		setFieldValue(`landArea.${0}`, '');
																		// setFieldValue(`landDetails.${0}`, '');
																	}}
																	icon="plus"
																/>
															</div>
														)}
													</div>
												)}
											/>
											{/* <div>
												{data.kittaNo}{' '}
												<DashedLangInput
													name="kittaNo"
													setFieldValue={setFieldValue}
													value={values.kittaNo}
													error={errors.kittaNo}
												/>
											</div>
											<div className="no-margin-field"></div> */}
										</div>
									</div>
									<div>
										{data.nirmanHune}{' '}
										<DashedLangInput
											name="constructionName"
											setFieldValue={setFieldValue}
											value={values.constructionName}
											error={errors.constructionName}
										/>{' '}
										{data.plinthArea} <DashedAreaUnitInput name="plinthDetails" unitName="plinthDetailsUnit" />
									</div>
									<div className=" div-indent">
										<RoadSetbackTable
											title={data.roadSetbackTitle}
											values={values}
											errors={errors}
											setFieldValue={setFieldValue}
											handleChange={handleChange}
										/>
									</div>
									<div>
										{data.jaggaPrakriti}{' '}
										<EbpsTextareaField
											placeholder="....."
											name="landDescription"
											setFieldValue={setFieldValue}
											value={values.landDescription}
											error={errors.landDescription}
										/>
									</div>
									<div>
										{data.highTensionLine}{' '}
										{data.highTensionLineOptions.map((option) => (
											<RadioInput key={option.value} name="isHighTensionLine" option={option.value} label={option.label} />
										))}
									</div>
									<div className="no-margin-field">
										{data.surrounding}
										{values.surrounding &&
											values.surrounding.map((surr, idx) => (
												<div className="div-indent" key={idx}>
													{data.surroundingLetters[idx]}{' '}
													{surroundingMappingFlat.find((fl) => fl.side === surr.side) &&
														surroundingMappingFlat.find((fl) => fl.side === surr.side).value}
													<DashedUnitInput name={`surrounding.${idx}.feet`} unitName={`surrounding.${idx}.sideUnit`} />
												</div>
											))}
									</div>
									<div>
										{data.mohoda} <DashedUnitInput name="gharMohoda" unitName="gharMohodaUnit" />
									</div>
									<div>
										{data.nirmanMohoda} <DashedLangInput name="otherMohodaName" setFieldValue={setFieldValue} />{' '}
										{data.nirmanMohoda1}
										<DashedUnitInput name="otherMohoda" unitName="otherMohodaUnit" />
									</div>
									<div>{data.anurodh}</div>
									<div>
										{data.anya}{' '}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>
									<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors}>
										<DetailedSignature.Date name="nirikshyanDate" label={data.nirikshyanDate} />
										<DetailedSignature.Name className="div-indent" name="subName" label={data.pratibedanPeshName} />
										<DetailedSignature.Designation className="div-indent" name="subDesignation" label={data.designation} />
										<DetailedSignature.Signature
											className="div-indent"
											name="subSignature"
											label={data.signature}
											showSignature={useSignatureImage}
										/>
									</DetailedSignature>
								</div>

								{/* Land Area Calculation Start----  */}
								<div style={{ pageBreakBefore: 'always' }}>
									<LandAreaCalculationSection setFieldValue={setFieldValue} values={values} errors={errors} />
								</div>
								{/* Land Area Calculation End ----  */}
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

export const LandAreaCalculationSection = ({ setFieldValue, values, errors }) => {
	const [grandTotal, setGrandTotal] = useState(0);

	useEffect(() => {
		const total = values.landAreaCalculation.reduce((acc, row) => round(acc + getFormattedNumber(row.area)), 0.0);
		setGrandTotal(total);
	}, [values.landAreaCalculation]);

	const getUnitOption = (key) => {
		if (key === 'shapeUnit')
			return [
				{ key: 1, value: 'Rectangle', text: 'Rectangle' },
				{ key: 2, value: 'Triangle', text: 'Triangle' },
				{ key: 3, value: 'Square', text: 'Square' },
			];
		else if (key === 'areaUnit') return areaUnitOptions;
		else return distanceOptions;
	};

	const getRelatedFields = (unit) => {
		const fields = [];
		for (let i = 0; i < values.landAreaCalculation.length; i++) {
			fields.push(`landAreaCalculation.${i}.${unit.replace('Unit', '')}`);
		}
		return fields;
	};

	return (
		<div>
			<SectionHeader>
				<h3>{landData.notice}</h3>
			</SectionHeader>
			{landData.ownerName}: <DashedLangInput name="ownerName" setFieldValue={setFieldValue} value={values.ownerName} error={errors.ownerName} />
			<SectionHeader>
				<h2 className="english-div">{landData.title}</h2>
			</SectionHeader>
			<FieldArray
				name="landAreaCalculation"
				render={(arrayHelpers) => (
					<div>
						<Table className="certificate-ui-table">
							<Table.Header>
								<Table.Row>
									{landData.table.map((header, idx) => (
										<Table.HeaderCell key={idx} rowSpan={idx <= 1 ? 2 : 1}>
											{header}
										</Table.HeaderCell>
									))}
								</Table.Row>
								<Table.Row>
									{landData.tableUnit.map((unit, idx) => (
										<Table.HeaderCell key={idx}>
											{unit === 'shapeUnit' ? (
												landData.shapeValueLabel
											) : (
												<UnitDropdownWithRelatedFields
													setFieldValue={setFieldValue}
													unitName={unit}
													relatedFields={getRelatedFields(unit)}
													options={getUnitOption(unit)}
												/>
											)}
										</Table.HeaderCell>
									))}
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{values.landAreaCalculation &&
									values.landAreaCalculation.map((row, idx) => (
										<Table.Row key={idx}>
											<Table.Cell>{row.sn}</Table.Cell>
											<Table.Cell>
												<DashedLangInput
													name={`landAreaCalculation.${idx}.sym`}
													setFieldValue={setFieldValue}
													value={getIn(values, `landAreaCalculation.${idx}.sym`)}
													error={getIn(errors, `landAreaCalculation.${idx}.sym`)}
													compact
												/>
											</Table.Cell>
											<Table.Cell>
												<UnitDropdown
													name={`landAreaCalculation.${idx}.shape`}
													setFieldValue={setFieldValue}
													value={getIn(values, `landAreaCalculation.${idx}.shape`)}
													options={getUnitOption('shapeUnit')}
												/>
											</Table.Cell>
											<Table.Cell>
												<DashedNormalInputIm name={`landAreaCalculation.${idx}.sideA`} compact={true} />
											</Table.Cell>
											<Table.Cell>
												<DashedNormalInputIm name={`landAreaCalculation.${idx}.sideB`} compact={true} />
											</Table.Cell>
											<Table.Cell>
												<DashedNormalInputIm name={`landAreaCalculation.${idx}.sideC`} compact={true} />
											</Table.Cell>
											<Table.Cell>
												<DashedNormalInputIm name={`landAreaCalculation.${idx}.area`} compact={true} />
											</Table.Cell>
										</Table.Row>
									))}
								<Table.Row>
									<Table.Cell></Table.Cell>
									<Table.Cell colSpan="5">
										<b>Total Area of Land</b>
									</Table.Cell>
									<Table.Cell>{grandTotal}</Table.Cell>
								</Table.Row>
							</Table.Body>
						</Table>
						<FlexSingleRight>
							<Button
								className="remove-on-print"
								onClick={() => {
									arrayHelpers.push({});
									setFieldValue(`landAreaCalculation.${values.landAreaCalculation.length}`, {
										sn: values.landAreaCalculation.length + 1,
										sym: '',
										shape: 'Rectangle',
										sideA: 0,
										sideB: 0,
										sideC: 0,
										area: 0,
									});
								}}
							>
								{landData.addRowButton}
							</Button>
						</FlexSingleRight>
					</div>
				)}
			/>
			<div>
				<FlexSingleLeft>{landData.note}</FlexSingleLeft>
				<FlexSingleLeft>
					{landData.conversion.map((data, idx) => (
						<div key={idx}>{data}</div>
					))}
				</FlexSingleLeft>
			</div>
		</div>
	);
};
const NapiNirikshyanPratibedan = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.aminKoSthalgatPratibedan,
				objName: 'aminKoSthalgatPratibedan',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			...PrintParams.REMOVE_ON_PRINT,
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			// param8: ['15dayspecial']

			// param8: [
			//   'getElementsByClassName',
			//   'ui dropdown',
			//   'innerText',

			// ]
		}}
		useInnerRef={true}
		render={(props) => <NapiNirikshyanPratibedanComponent {...props} parentProps={parentProps} />}
	/>
);

export default NapiNirikshyanPratibedan;
