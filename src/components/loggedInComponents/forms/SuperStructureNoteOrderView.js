import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { superstructurenodeorderview } from '../../../utils/data/mockLangFile';
import { isEmpty } from '../../../utils/functionUtils';
import { DashedLangDateField } from './../../shared/DateField';
import { DashedLangInputWithSlash } from '../../shared/DashedFormInput';

import { getMessage } from '../../shared/getMessage';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import * as Yup from 'yup';
import { showToast } from '../../../utils/functionUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import { prepareMultiInitialValues } from './../../../utils/dataUtils';
import { DashedAreaUnitInput } from '../../shared/EbpsUnitLabelValue';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import { DetailedSignature, getDesignations } from './formComponents/FooterSignature';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { Client, isBiratnagar } from '../../../utils/clientUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import { isInaruwa } from './../../../utils/clientUtils';
import { footerInputSignature } from '../../../utils/data/genericFormData';
import { getSaveByUserDetails, getApproveByObject } from '../../../utils/formUtils';
import { PrintParams } from '../../../utils/printUtils';

const ssnov_data = superstructurenodeorderview.superstructurenodeorderview_data;
const messageId = 'superstructurenodeorderview.superstructurenodeorderview_data';

const milneyOpt = [
	{ key: 'a1', value: 'दिन मिल्ने', text: 'दिन मिल्ने' },
	{ key: 'a2', value: 'पारीत', text: 'पारीत' },
];
const mapdandaOpt = [
	{ key: 'a1', value: 'लगाउने', text: 'लगाउने' },
	{ key: 'a2', value: 'मापदण्ड ', text: 'मापदण्ड' },
];

const SuperStructureNoteSchema = Yup.object().shape(
	Object.assign({
		dates: validateNullableNepaliDate,
		peshGarneDate: validateNullableNepaliDate,
		Allowancedate: validateNullableNepaliDate,
		erDate: validateNullableNepaliDate,
		chiefDate: validateNullableNepaliDate,
	})
);

class SuperStructureNoteOrderViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { userData, prevData, otherData, enterByUser } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const rujuData = getApproveBy(0);
		const swikritData = getApproveBy(1);

		const json_data = getJsonData(prevData);
		const AllowancePaperData = getJsonData(otherData.AllowancePaper);
		const plintLevelTechApplicationData = getJsonData(otherData.PlintLevelTechApplication);

		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }

		// let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			{
				obj: AllowancePaperData,
				reqFields: ['allowanceDate'],
			},
			{
				obj: plintLevelTechApplicationData,
				reqFields: ['techPersonName'],
			},
			{
				obj: {
					dinaMilney: 'पारीत',
					mapdanda: 'लगाउने',
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			// {
			// 	obj: serInfo,
			// 	reqFields: [],
			// },
			// { obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{ obj: json_data, reqFields: [] },
			{
				obj: {
					erName: rujuData.name,
					erDesignation: rujuData.designation,
					erSignature: rujuData.signature,
					erDate: rujuData.date,
					chiefName: swikritData.name,
					chiefDesignation: swikritData.designation,
					chiefSignature: swikritData.signature,
					chiefDate: swikritData.date,
				},
				reqFields: [],
			}
		);

		initVal.superStructureUnit = 'METRE';
		initVal.otherAreaUnit = 'SQUARE METRE';

		if (isStringEmpty(initVal.dates)) {
			initVal.dates = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.peshGarneDate)) {
			initVal.peshGarneDate = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			orgCode,
			useSignatureImage,
		} = this.props;
		const { initVal } = this.state;
		const pdata = this.props.permitData;
		const udata = userData;
		const user_info = this.props.userData;

		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={SuperStructureNoteSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.SuperStructureNoteOrder}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								// 	getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm, handleChange }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<div className="NJ-Main superStruConsView build-gap">
									<LetterHeadFlex userInfo={user_info} />

									{renderHeader(orgCode, values, handleChange, setFieldValue, errors)}
									<div style={{ textAlign: 'justify ' }}>
										{/* <span> */}
										<div className="no-margin-field">
											<span style={{ marginLeft: '30px' }}>{udata.organization.name}</span>
											{getMessage(`${messageId}.wada`, ssnov_data.wada)}
											<span>{pdata.nibedakTol}</span>
											{getMessage(`${messageId}.data_1`, ssnov_data.data_1)}
											<span>{pdata.nibedakName}</span>
											{getMessage(`${messageId}.data_2`, ssnov_data.data_2)}
											<span>{pdata.oldMunicipal}</span>
											{getMessage(`${messageId}.data_3`, ssnov_data.data_3)}
											<span>{pdata.newWardNo}</span>
											{getMessage(`${messageId}.data_4`, ssnov_data.data_4)}
											<span>{pdata.buildingJoinRoad}</span>
											{getMessage(`${messageId}.data_5`, ssnov_data.data_5)}
											<span>{pdata.kittaNo}</span>
											{getMessage(`${messageId}.data_6`, ssnov_data.data_6)}
											<span>{pdata.landArea}</span> {pdata.landAreaType}
											{getMessage(`${messageId}.data_7`, ssnov_data.data_7)}
											<DashedLangDateField
												label={udata.enterDate}
												name="allowanceDate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.allowanceDate}
												error={errors.allowanceDate}
											/>
											{getMessage(`${messageId}.data_8`, ssnov_data.data_8)} <span>{values.techPersonName}</span>
											{getMessage(`${messageId}.data_9`, ssnov_data.data_9)}{' '}
											<Select
												placeholder="दिन मिल्ने"
												// defaultValue='को'
												name="dinaMilney"
												options={milneyOpt}
												value={values.dinaMilney}
												onChange={(e, { value }) => setFieldValue('dinaMilney', value)}
											/>{' '}
											{values.dinaMilney === 'दिन मिल्ने' ? (
												ssnov_data.dinaMilney.first
											) : (
												<>
													{ssnov_data.parit.first}{' '}
													<Select
														placeholder="लगाउने"
														// defaultValue='को'
														name="mapdanda"
														options={mapdandaOpt}
														value={values.mapdanda}
														onChange={(e, { value }) => setFieldValue('mapdanda', value)}
													/>{' '}
													{values.mapdanda === 'लगाउने' ? (
														ssnov_data.parit.second
													) : (
														<>
															{ssnov_data.data_10}{' '}
															<DashedAreaUnitInput
																name="otherArea"
																unitName="otherAreaUnit"
																// label={mapTech_data.mapTech_13}
															/>
															{getMessage(`${messageId}.data_10_1`, ssnov_data.data_10_1)}
														</>
													)}
												</>
											)}
										</div>
									</div>
									<br />
									<br />
									{isInaruwa || isBiratnagar ? '' : renderFooter({ values, errors, setFieldValue, useSignatureImage })}
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const renderHeader = (orgCode, values, handleChange, setFieldValue, errors) => {
	if (orgCode === Client.INARUWA) {
		return (
			<>
				<SectionHeader>
					<h3>
						<span>{getMessage(`${messageId}.title_3`, ssnov_data.title_3)}</span>
						<br />
						<span>{getMessage(`${messageId}.title_1`, ssnov_data.title_1)}</span>
					</h3>

					<h3 className="underline">
						<span>{getMessage(`${messageId}.title`, ssnov_data.title)}</span>
					</h3>
				</SectionHeader>
				<br />
			</>
		);
	} else if (orgCode === Client.BIRATNAGAR) {
		return (
			<>
				<SectionHeader>
					<h3>{ssnov_data.gharNaksaUpasakha}</h3>
					<div style={{ textAlign: 'right' }}>
						<DashedLangDateField
							label={ssnov_data.date}
							name="dates"
							setFieldValue={setFieldValue}
							className="dashedForm-control"
							inline={true}
							value={values.dates}
						/>
					</div>

					<h3>{getMessage(`${messageId}.title_3`, ssnov_data.title_1)}</h3>
					<br />
					<h3 className="underline">{getMessage(`${messageId}.title`, ssnov_data.title)}</h3>
				</SectionHeader>
				<br />
			</>
		);
	} else if (orgCode === Client.MECHI) {
		return (
			<>
				<SectionHeader>
				<h3 className="underline">
					<span>{getMessage(`${messageId}.title_1`, ssnov_data.title_1)}</span>
					<br />
					<span>{getMessage(`${messageId}.title_2`, ssnov_data.title_2)}</span>
				</h3>
				</SectionHeader>
				<div style={{ textAlign: 'right' }}>
					<DashedLangDateField
						label={ssnov_data.date}
						name="dates"
						setFieldValue={setFieldValue}
						className="dashedForm-control"
						inline={true}
						value={values.dates}
					/>
				</div>
				<div className="tipandi-letterHead">
					<SectionHeader>
						<h3 className="underline">
							<span>{getMessage(`${messageId}.title`, ssnov_data.title)}</span>
						</h3>
					</SectionHeader>
					<br />
				</div>
			</>
		);
	}else {
		return (
			<>
				<SectionHeader>
					<h3 className="underline">
						<span>{getMessage(`${messageId}.title_1`, ssnov_data.title_1)}</span>
						<br />
						<span>{getMessage(`${messageId}.title_2`, ssnov_data.title_2)}</span>
					</h3>
				</SectionHeader>
				<div style={{ textAlign: 'right' }}>
					<DashedLangDateField
						label={ssnov_data.date}
						name="dates"
						setFieldValue={setFieldValue}
						className="dashedForm-control"
						inline={true}
						value={values.dates}
					/>
				</div>
				<div className="tipandi-letterHead">
					<div style={{ textAlign: 'left' }}>
						{getMessage(`${messageId}.pasa`, ssnov_data.pasa)}{' '}
						<DashedLangInputWithSlash
							name="SuperpatraSankhya"
							className="dashedForm-control"
							handleChange={handleChange}
							setFieldValue={setFieldValue}
							value={values.SuperpatraSankhya}
							error={errors.SuperpatraSankhya}
						/>
						<br />
						{getMessage(`${messageId}.chana`, ssnov_data.chana)}{' '}
						<DashedLangInputWithSlash
							name="SuperchalaniNumber"
							className="dashedForm-control"
							handleChange={handleChange}
							setFieldValue={setFieldValue}
							value={values.SuperchalaniNumber}
							error={errors.SuperchalaniNumber}
						/>
					</div>
					<SectionHeader>
						<h3 className="underline">
							<span>{getMessage(`${messageId}.title`, ssnov_data.title)}</span>
						</h3>
					</SectionHeader>
					<br />
				</div>
			</>
		);
	}
};

const renderFooter = ({ values, errors, setFieldValue, useSignatureImage }) => {
	const designations = getDesignations('peshGarneDate');
	return (
		<div className="signature-div flex-item-space-between">
			{designations.map((designation, index) => (
				<div key={index} style={{ textAlign: 'left' }}>
					<p>{designation.label}</p>
					<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors} designation={designation}>
						<DetailedSignature.Signature label={footerInputSignature.sahi} showSignature={useSignatureImage} />
						<DetailedSignature.Name />
						<DetailedSignature.Designation />
						<DetailedSignature.Date />
					</DetailedSignature>
				</div>
			))}
		</div>
	);
};

const SuperStructureNoteOrderView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.SuperStructureNoteOrder,
				objName: 'superStructNoteOrder',
				form: true,
			},
			{
				api: api.superStructureBuild,
				objName: 'superBuild',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'AllowancePaper',
				form: false,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'PlintLevelTechApplication',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			// param1: ['getElementsByTagName', 'input', 'value'],
			// param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param1: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		render={(props) => <SuperStructureNoteOrderViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNoteOrderView;
