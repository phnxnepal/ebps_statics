import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Message, Modal, Button } from 'semantic-ui-react';
import { anushuchi_ka } from '../../../../utils/data/mockLangFile';
import * as Yup from 'yup';

import { SelectInput } from '../../../shared/Select';
import { getMessage } from '../../../shared/getMessage';
import api from '../../../../utils/api';
import { checkError, handleSuccess, prepareMultiInitialValues, cleanFormData, FloorArray } from '../../../../utils/dataUtils';
import { translateDate } from '../../../../utils/langUtils';
import { BUILDING_CLASS, BUILDING_CLASS_AREA_LIMIT_FEET, BUILDING_CLASS_AREA_LIMIT_METER } from '../../../../utils/constants';
import { validateString } from '../../../../utils/validationUtils';
import { setLocalStorage } from '../../../../utils/secureLS';
import { FormUrlFull } from '../../../../utils/enums/url';
import { computeClassWiseNextUrl } from '../../../../utils/urlUtils';
import { isEmpty, getUserTypeValueNepali } from '../../../../utils/functionUtils';
import { constructionTypeSelectOptions } from '../../../../utils/data/genericData';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { checkClient, Client, isBirtamod, isKageyshowri } from '../../../../utils/clientUtils';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation, footerInputSignature } from '../../../../utils/data/genericFormData';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { UserType } from '../../../../utils/userTypeUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { DashedDateField } from '../../../shared/DateField';
import { PrintParams } from '../../../../utils/printUtils';
import { RequiredFields } from '../../utility/RequiredFields';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { CompactInfoMessage } from '../../../shared/formComponents/CompactInfoMessage';
import { SignatureImage } from '../formComponents/SignatureImage';

const anusuchikaLang = anushuchi_ka.designApproval;
const messageId = 'anushuchi_ka.designApproval';
// const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

// let anusuchikaOptions = [];

const strData = ['constructionType', 'buildingClass'];

const formDataSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});

const DesignApprovalSchema = Yup.object().shape(Object.assign(...formDataSchema));

class DesignApprovalComponent extends Component {
	constructor(props) {
		super(props);
		const { orgCode, userData, permitData, otherData, prevData } = props;
		const userRole = userData.userType;
		const floorArray = new FloorArray(permitData.floor);
		const floorUnit = floorArray.getFloorUnit();
		const groundFloor = floorArray.getFloorByFloor(1);
		const areaLimitMeter = isBirtamod ? 97.548 : BUILDING_CLASS_AREA_LIMIT_METER;
		const areaLimitFeet = isBirtamod ? 1050 : BUILDING_CLASS_AREA_LIMIT_FEET;

		let needsClassB = false;

		if (groundFloor) {
			if (floorUnit === 'METRE' && groundFloor.area > areaLimitMeter) {
				needsClassB = true;
			} else if (floorUnit === 'FEET' && groundFloor.area > areaLimitFeet) {
				needsClassB = true;
			}
		}
		let desInfo = {
			designerName: '',
			designerDesignation: '',
		};

		if (userRole === UserType.DESIGNER) {
			desInfo.designerName = userData.userName;
			desInfo.designerDesignation = getUserTypeValueNepali(userData.userType);
			desInfo.designerDate = getCurrentDate();
		}

		const anusuchikaOptions = otherData.anusuchiKaMaster.map((row) => ({
			value: row.id,
			text: `${row.nameNepali} ${row.name}`,
		}));

		const initialValues = prepareMultiInitialValues(
			{ obj: desInfo, reqFields: [] },
			{
				obj: {
					constructionType: getConstructionTypeValue(permitData.constructionType),
					buildingClass: anusuchikaOptions[needsClassB ? 1 : 2].value,
				},
				reqFields: [],
			},
			{ obj: cleanFormData(checkError(prevData), RequiredFields.getRequiredFields(FormUrlFull.DESIGN_APPROVAL)), reqFields: [] }
		);
		this.state = {
			isSundarHaraicha: checkClient(orgCode, Client.SUNDARHARAICHA),
			initialValues,
			anusuchikaOptions,
			needsClassB: needsClassB && userRole === UserType.DESIGNER,
			groundFloor: { area: groundFloor.area, limitUnit: floorUnit === 'METRE' ? areaLimitMeter + ' वर्ग मिटर' : areaLimitFeet + ' वर्ग फिट' },
			openModal: false,
		};
	}

	renderAreaLimitMessage() {
		return (
			<>
				{anusuchikaLang.areaLimitMessage} <b>{this.state.groundFloor.limitUnit}</b> {anusuchikaLang.areaLimitMessage2}
			</>
		);
	}

	render() {
		const { isSundarHaraicha, initialValues, anusuchikaOptions, needsClassB, openModal, userData } = this.state;
		const { staticFiles } = this.props;
		const detailed_Info = this.props.permitData;
		return (
			<div className="anusuchika-Form">
				<Formik
					initialValues={initialValues}
					enableReinitialize={true}
					validationSchema={DesignApprovalSchema}
					onSubmit={async (values, actions) => {
						// console.log('Form values', values);
						actions.setSubmitting(true);

						const datatosend = (({
							constructionType,
							buildingClass,
							applicationNo,
							designerName,
							designerDesignation,
							designerDate,
						}) => ({
							constructionType,
							buildingClass,
							applicationNo,
							designerName,
							designerDesignation,
							designerDate,
						}))(values);

						try {
							await this.props.postAction(api.designApproval, datatosend, true, true, true, values.buildingClass);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								setLocalStorage(BUILDING_CLASS, datatosend.buildingClass);
								let nextUrl = computeClassWiseNextUrl(FormUrlFull.DESIGN_APPROVAL, datatosend.buildingClass);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success, nextUrl);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={(props) => {
						return (
							<Form loading={props.isSubmitting}>
								<Modal open={openModal} onClose={() => this.setState({ openModal: false })} style={{ width: '50%' }}>
									<Modal.Header>{anusuchikaLang.areaLimitHeader}</Modal.Header>
									<Modal.Content scrolling>{this.renderAreaLimitMessage()}</Modal.Content>
									<Modal.Actions>
										<Button secondary onClick={() => this.setState({ openModal: false })}>
											{anusuchikaLang.cancelText}
										</Button>
										<Button
											primary
											icon="checkmark"
											className="primaryButton"
											onClick={() => {
												props.handleSubmit();
												this.setState({ openModal: false });
											}}
										>
											{anusuchikaLang.goForward}
										</Button>
									</Modal.Actions>
								</Modal>
								{!isEmpty(this.props.errors) && (
									<Message negative>
										<Message.Header>Error</Message.Header>
										<p>{this.props.errors.message}</p>
									</Message>
								)}
								<div ref={this.props.setRef}>
									<FormHeading userinfo={this.props.userData} isSundarHaraicha={isSundarHaraicha} />
									<FormBody
										isSundarHaraicha={isSundarHaraicha}
										isKageyshowri={isKageyshowri}
										user_info={this.props.userData}
										permitData={this.props.permitData}
										detailed_Info={detailed_Info}
										setFieldValue={props.setFieldValue}
										values={props.values}
										errors={props.errors}
										anusuchikaOptions={anusuchikaOptions}
										staticFiles={staticFiles}
									/>
								</div>
								<br />
								<SaveButtonValidation
									errors={props.errors}
									formUrl={this.props.formUrl}
									hasSavePermission={this.props.hasSavePermission}
									hasDeletePermission={this.props.hasDeletePermission}
									isSaveDisabled={this.props.isSaveDisabled}
									prevData={checkError(this.props.prevData)}
									handleSubmit={() => {
										if (needsClassB && props.values.buildingClass !== 'B') {
											this.setState({ openModal: true });
										} else {
											props.handleSubmit();
										}
									}}
									validateForm={props.validateForm}
								/>
								{needsClassB && props.values.buildingClass !== 'B' && <CompactInfoMessage content={this.renderAreaLimitMessage()} />}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const FormHeading = React.memo(({ userinfo, isSundarHaraicha }) => {
	const UserInfoData = userinfo;
	if (isSundarHaraicha) {
		return (
			<>
				<div className="section-header">
					<h3 className="top-margin">{getMessage(`${messageId}.ka_heading`, anusuchikaLang.ka_heading)}</h3>
				</div>
				<div className="margin-section">
					<div>
						{getMessage(`${messageId}.ka_municipal`, anusuchikaLang.ka_municipal)}
						{UserInfoData && UserInfoData.organization.name}
					</div>
					<LetterSalutation
						lines={[letterSalutation.prashasakhiya, `${UserInfoData.organization.name} ${letterSalutation.karyalaya}`, `${UserInfoData.organization.address} ${UserInfoData.organization.province}`]}
					/>
				</div>
				<div className="section-header margin-section">
					<h4 className="underline">{getMessage(`${messageId}.sundarHaraicha_subject`, anusuchikaLang.sundarHaraicha_subject)} </h4>
				</div>
			</>
		);
	} else {
		return (
			<>
				<h2 style={{ textAlign: 'center' }}>{getMessage(`${messageId}.ka_heading`, anusuchikaLang.ka_heading)}</h2>

				<span style={{ textAlign: 'center' }}>
					<h3>{getMessage(`${messageId}.ka_subheading1`, anusuchikaLang.ka_subheading1)}</h3>
					<h3>{getMessage(`${messageId}.ka_subheading2`, anusuchikaLang.ka_subheading2)}</h3>
				</span>

				<span style={{ textAlign: 'left' }}>			
					<LetterSalutation
							lines={[letterSalutation.prashasakhiya, `${UserInfoData.organization.name} ${letterSalutation.karyalaya}`, `${UserInfoData.organization.address} ${UserInfoData.organization.province}`]}
					/>
				</span>
				<br />
				<br />
				<span style={{ textAlign: 'center' }}>
					<h4>{getMessage(`${messageId}.ka_subject`, anusuchikaLang.ka_subject)} </h4>
				</span>
			</>
		);
	}
});

const FormBody = ({ detailed_Info, user_info, permitData, isSundarHaraicha, isKageyshowri, setFieldValue, values, errors, anusuchikaOptions, staticFiles }) => {
	if (isSundarHaraicha) {
		return (
			<>
				<div className="margin-section">
					{user_info && user_info.organization.name} {getMessage(`${messageId}.ka_data_3`, anusuchikaLang.ka_data_3)}{' '}
					{detailed_Info.newWardNo} {anusuchikaLang.sadak} {permitData.nibedakSadak}{' '}
					{getMessage(`${messageId}.ka_data_4`, anusuchikaLang.ka_data_4)}
					{detailed_Info.kittaNo}
					{getMessage(`${messageId}.ka_data_5`, anusuchikaLang.ka_data_5)}
					{detailed_Info.landArea} {detailed_Info.landAreaType}
					{getMessage(`${messageId}.ka_data_6`, anusuchikaLang.ka_data_6)}
					<div className="select-forForm" style={{ display: 'inline-block' }}>
						<SelectInput options={constructionTypeSelectOptions} name="constructionType" />
					</div>
					{getMessage(`${messageId}.ka_maincontent`, anusuchikaLang.ka_maincontent)}
				</div>
				<div className="margin-section">
					<FormFooter />
					<SelectInput options={anusuchikaOptions} name="buildingClass" />
				</div>
				<div className="flex-item-space-between">
					<FormOwnerDetails detailInfo={permitData} staticFiles={staticFiles} />
					<FlexSingleRight>
						<div className="section-header">
							<p className="normal left-align underline">{footerInputSignature.paramarsa}</p>
						</div>
						<div>
							{footerInputSignature.name}
							<DashedLangInput name="designerName" setFieldValue={setFieldValue} value={values.designerName} />
						</div>
						<div>
							{footerInputSignature.sahi}
							<span className="ui input dashedForm-control" />
						</div>
						<div>
							{footerInputSignature.pad}
							<DashedLangInput name="designerDesignation" setFieldValue={setFieldValue} value={values.designerDesignation} />
						</div>
						<div className="no-margin-field">
							{footerInputSignature.date}
							<DashedDateField
								label={''}
								name="designerDate"
								inline={true}
								value={values.designerDate || ''}
								error={errors.designerDate}
							/>
						</div>
					</FlexSingleRight>
				</div>
			</>
		);
	}
	
	// else if(isKageyshowri){
	// 	return <>
	// 		{' '}{user_info.organization.name}{' '}
	// 		{' '}{anushuchi_ka.kageyshowriData.wadaNo}{' '}
	// 		{' '}{detailed_Info.newWardNo}{' '}
	// 		{' '}{anushuchi_ka.kageyshowriData.data_1}{' '}
	// 		{' '}{detailed_Info.kittaNo}{' '}
	// 		{' '}{anushuchi_ka.kageyshowriData.chhetrafal}{' '}
	// 		{' '}{detailed_Info.landArea}{' '}
	// 		{' '}{anushuchi_ka.kageyshowriData.data_2}{' '}
			
	// 	</>
	// }
	else
		return (
			<>
				{getMessage(`${messageId}.ka_data_1`, anusuchikaLang.ka_data_1)}
				<span>{detailed_Info.oldMunicipal}</span>
				{getMessage(`${messageId}.ka_data_2`, anusuchikaLang.ka_data_2)}
				<span>{user_info && user_info.organization.name}</span>
				{getMessage(`${messageId}.ka_data_3`, anusuchikaLang.ka_data_3)}
				<span>{detailed_Info.newWardNo}</span>
				{getMessage(`${messageId}.ka_data_4`, anusuchikaLang.ka_data_4)}
				<span>{detailed_Info.kittaNo}</span>
				{getMessage(`${messageId}.ka_data_5`, anusuchikaLang.ka_data_5)}
				<span>
					{detailed_Info.landArea} {detailed_Info.landAreaType}
				</span>
				{getMessage(`${messageId}.ka_data_6`, anusuchikaLang.ka_data_6)}
				{/* {printformat( */}
				<div className="select-forForm" style={{ display: 'inline-block', marginRight: '5px' }}>
					<SelectInput options={constructionTypeSelectOptions} name="constructionType" />
				</div>
				<span>{getMessage(`${messageId}.ka_maincontent`, anusuchikaLang.ka_maincontent)}</span>
				<br />
				<br />
				<FormOwnerDetails detailInfo={permitData} staticFiles={staticFiles} />
				<br />
				<br />
				<br />
				<br />
				<FormFooter />
				{/* {printformat( */}
				<SelectInput options={anusuchikaOptions} name="buildingClass" />
			</>
		);
};

const FormOwnerDetails = (props) => {
	const dataInfo = props.detailInfo;
	const { staticFiles } = props;
	return (
		<>
			<span>
				{getMessage(`${messageId}.ownerName`, anusuchikaLang.ownerName)}
				<span>{dataInfo.nibedakName}</span>
				<br />
				{getMessage(`${messageId}.ownerAddress`, anusuchikaLang.ownerAddress)}
				<span>{dataInfo.applicantAddress}</span>
				<br />
				{getMessage(`${messageId}.ownerPhone`, anusuchikaLang.ownerPhone)}
				<span>{dataInfo.applicantMobileNo}</span>
				<br />
				<SignatureImage
					value={staticFiles.ghardhaniSignature}
					showSignature={true}
					label={getMessage(`${messageId}.ownerSignature`, anusuchikaLang.ownerSignature)}
				/>
				<br />
				{getMessage(`${messageId}.ka_date`, anusuchikaLang.ka_date)}
				<span>{translateDate(dataInfo.applicantDateBS)}</span>
			</span>
		</>
	);
};

const FormFooter = () => (
	<>
		<h4 style={{ textAlign: 'center' }}>{getMessage(`${messageId}.ka_footer1`, anusuchikaLang.ka_footer1)}</h4>
	</>
);

const DesignApproval = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.designApproval, objName: 'designApproval', form: true },
			{
				api: api.anusuchiKaMaster,
				objName: 'anusuchiKaMaster',
				form: false,
				utility: true,
			},
		]}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
			param2: ['getElementsByClassName', 'dashedForm-control', 'innerText'],
		}}
		checkPreviousFiles={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <DesignApprovalComponent {...props} parentProps={parentProps} />}
	/>
);

export default DesignApproval;
