import React from 'react';
import { Form, Table, Grid, Divider } from 'semantic-ui-react';

import { Formik, Field, getIn } from 'formik';

import * as Yup from 'yup';

// import * as Yup from 'yup';
import { naksaData, noObjectionData } from './../../../../utils/data/nakxsaData';
import { DashedLangDateField } from '../../../shared/DateField';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { prepareMultiInitialValues, getJsonData, floorMappingFlat, handleSuccess, FloorArray } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import FormikCheckbox from '../../../shared/FormikCheckbox';
import TableInput from '../../../shared/TableInput';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { checkError } from './../../../../utils/dataUtils';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { validateNepaliDate } from '../../../../utils/validationUtils';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { getDocUrl } from '../../../../utils/config';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { isInaruwa } from '../../../../utils/clientUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleLeft } from './../../../uiComponents/FlexDivs';
import { ShreeDropdown } from '../formComponents/ShreeDropdown';
import { shreeOptions } from '../../../../utils/optionUtils';
import { getSaveByUserDetails, getApproveByUserDetails, getApproveIndex } from '../../../../utils/formUtils';

const stringify = value => {
	if (value) return JSON.stringify(value);
	else return '';
};

let TableData = naksaData.structureDesign.table.rows;
const customInputs = naksaData.structureDesign.customInputs;
// const footerData = naksaData.structureDesign.footer;
const noObjData = noObjectionData;
const tableData = noObjectionData.table;
const bodyData = noObjectionData.body;
const footerData = noObjectionData.footer;

const arrayValueIds = [21, 30, 7];
const objectValueIds = [29, 4, 20, 16, 25];
const toArray = field => {
	if (field.includes('[')) {
		const returnVal = field.replace(/[[\]]+/g, '');
		const retArr = returnVal.split(',');
		return retArr.map(val => val.trim());
	} else return field;
};
class NoObjectionSheet extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			toggle: {
				index: 0,
				value: TableData.row8.input_1[0],
			},
		};
	}

	handleFilterInputChangeRoofStyle = e => {
		const updateRoofStyle = e.target.value;
		if ('अन्य' === updateRoofStyle) {
			this.setState({
				inputRoofStyleFilter: true,
			});
		} else {
			this.setState({ inputRoofStyleFilter: false });
		}
		this.setState({ RoofStyle: updateRoofStyle });
	};

	render() {
		const { permitData, userData, prevData, otherData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, enterByUser, useSignatureImage, approveByUsers, orgCode } = this.props;
		const floorArray = new FloorArray(permitData.floor);
		const mapTech = getJsonData(otherData.mapTechnicalDescription);
		const mapCheckReport = otherData.mapCheckReport;
		const user_info = this.props.userData;
		const pulledData = {
			details: [
				{
					description: customInputs.buildingClass,
					designData: otherData.designApprovalData.buildingClass,
				},
				{
					description: customInputs.purposeOfConstruction,
					// designData: permitData.purposeOfConstruction,
					designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther },
				},
				{
					description: customInputs.constructionType,
					designData: getConstructionTypeValue(permitData.constructionType),
				},
				// {
				// 	description: purposeOfConstructionOther,
				// 	designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther }
				// },
				{
					description: customInputs.floorNumber,
					designData: mapTech.buildingDetailfloor,
				},
			],
		};

		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// } else if (!isStringEmpty(checkError(prevData).enterBy)) {
		// 	serInfo.subName = prevData.enterBy;
		// 	serInfo.subDesignation = getUserTypeValueNepali(UserType.SUB_ENGINEER);
		// }

		const approveInfo = getApproveByUserDetails(approveByUsers, userData, prevData);
		const jariIndex = getApproveIndex(0, formUrl, orgCode, approveInfo);


		const initialDetails = prepareMultiInitialValues(
			{
				obj: getSaveByUserDetails(enterByUser, userData),
				reqFields: [],
			},
			{
				obj: approveInfo[jariIndex] ? {jariSignature: approveInfo[jariIndex].signature} : {},
				reqFields: [],
			},
			{
				obj: { noObjectionDate: getCurrentDate(true), numberOfFloors: floorArray.getTopFloor() && floorArray.getTopFloor().englishCount, shree: shreeOptions[0].value },
				reqFields: [],
			},
			{ obj: prepareInitialVaules(mapCheckReport, pulledData), reqFields: [] }
		);

		initialDetails.details.forEach(row => {
			if (arrayValueIds.includes(row.descriptionId)) {
				row.designData = toArray(row.designData);
			} else if (objectValueIds.includes(row.descriptionId)) {
				try {
					row.designData = JSON.parse(row.designData);
				} catch {
					return row.designData;
				}
			}
		});

		// to maintain index integrity
		mapCheckReport.details && mapCheckReport.details.map((row, inputIndex) => (row.inputIndex = inputIndex));

		// let prevDatas = mapCheckReport.enterBy == null && mapCheckReport.enterDate == null ? {} : mapCheckReport;

		const MapCheckSchema = Yup.object().shape(
			Object.assign({
				noObjectionDate: validateNepaliDate,
			})
		);

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}

				<Formik
					initialValues={initialDetails}
					validationSchema={MapCheckSchema}
					onSubmit={async (values, actions) => {
						// values.details.filter(row => row.descriptionId === 9).map(row => (row.designData = this.state.toggle.value));
						actions.setSubmitting(true);
						values.details[28].designData = stringify(values.details[28].designData);
						values.details[3].designData = stringify(values.details[3].designData);
						values.details[19].designData = stringify(values.details[19].designData);
						values.details[24].designData = stringify(values.details[24].designData);
						values.details[15].designData = stringify(values.details[15].designData);

						try {
							await this.props.postAction(`${api.mapCheckReport}${permitData.applicantNo}`, values);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, values, handleChange, setFieldValue, isSubmitting, errors, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								<div ref={this.props.setRef}>
									{isInaruwa ? (
										<>
											<NoObjectionBodyInaruwa
												permitData={permitData}
												userData={userData}
												user_info={user_info}
											/>
										</>
									) : (
											<>
												<div>
													<div className="section-header">
														<h2 className="bottom-margin">{userData.organization.name}</h2>
														<h3>{noObjData.salutation2}</h3>
													</div>
													<br />
													<div className="section-header">
														<h3 className="underline">{noObjData.title}</h3>
													</div>
												</div>
												<div className="flex-item-space-between">
													<div>
														<LetterSalutation lines={[noObjData.salutation1, noObjData.salutation2, userData.organization.name]} />
													</div>
													<div>
														<br />
														<div className="certificate-image">
															{permitData.photo && <img src={`${getDocUrl()}${permitData.photo}`} alt="imageFile" />}
														</div>
													</div>
												</div>
												<div>
													{noObjectionData.body.mahodaya},
												<div>
														{bodyData.sabik} {permitData.oldMunicipal} {bodyData.halWada} {permitData.newWardNo} {bodyData.sadak}{' '}
														{permitData.buildingJoinRoad} {bodyData.kitta} {permitData.kittaNo} {bodyData.chetraphal}{' '}
														{permitData.landArea} {permitData.landAreaType} {bodyData.letter}
													</div>
												</div>
											</>
										)}

									<br />

									<div className="section-header">
										<h3 className="underline">{tableData.heading}</h3>
									</div>
									<div className="NayaJhanchPachi">
										<Table celled>
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>{TableData.row1.sn}</Table.HeaderCell>
													<Table.HeaderCell>{TableData.row1.name}</Table.HeaderCell>
													<Table.HeaderCell>{TableData.row1.input_1}</Table.HeaderCell>
													<Table.HeaderCell>{TableData.row1.input_2}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{mapCheckReport.details &&
													mapCheckReport.details
														.filter(row => row.type === '' || row.type === 'P : G')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																<Table.Cell>{row.description}</Table.Cell>
																{parseInt(row.sn) === 9 ? (
																	<Table.Cell>
																		{TableData.row8.input_1.map((input, index) => (
																			<div key={index} className="ui radio checkbox">
																				<input
																					type="radio"
																					value={TableData.row8.input_1[index]}
																					name={`details.${row.inputIndex}.designData`}
																					checked={
																						getIn(values, `details.${row.inputIndex}.designData`) ===
																						input
																					}
																					onChange={e => {
																						setFieldValue(`details.${row.inputIndex}.designData`, input);
																					}}
																				/>
																				<label htmlFor={`details.${row.inputIndex}.designData`}>
																					{input}
																				</label>
																			</div>
																		))}
																	</Table.Cell>
																) : !Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell>
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																			[
																				row.description === customInputs.buildingClass && (
																					<Table.Cell key={'building-class'}>
																						{TableData.row2.input_1.map((input, index) => (
																							// <Table.Cell>
																							<div key={index} className="ui radio checkbox">
																								<input
																									type="radio"
																									name={`details.${row.inputIndex}.designData`}
																									value={input}
																									checked={
																										getIn(
																											values,
																											`details.${row.inputIndex}.designData`
																										) === input
																									}
																									onChange={handleChange}
																								/>
																								<label htmlFor={`details.${row.inputIndex}.designData`}>
																									{input}
																								</label>
																							</div>
																							// </Table.Cell>
																						))}
																					</Table.Cell>
																				),
																				row.description === customInputs.floorNumber && (
																					<Table.Cell key="floor-number">
																						{TableData.row5.input_1.map((input, index) => (
																							// <Table.Cell>
																							<div key={index} className="ui radio checkbox">
																								<input
																									type="radio"
																									name={`details.${row.inputIndex}.designData`}
																									value={input}
																									checked={
																										getIn(
																											values,
																											`details.${row.inputIndex}.designData`
																										) === input
																									}
																									onChange={handleChange}
																								/>
																								<label htmlFor={`details.${row.inputIndex}.designData`}>
																									{input}
																								</label>
																							</div>
																							// </Table.Cell>
																						))}
																					</Table.Cell>
																				),
																				row.description === customInputs.buildingHeight && (
																					<Table.Cell key="building-height">
																						{TableData.row6.input_1.map((input, index) => (
																							// <Table.Cell>
																							<div key={index}>
																								<label htmlFor={`details.${row.inputIndex}.designData`}>
																									{input}
																								</label>
																								<input
																									type="text"
																									className="dashedForm-control"
																									name={`details.${row.inputIndex}.designData.${index}`}
																									// id={`details.${row.inputIndex}.designData.${index}`}
																									value={getIn(
																										values,
																										`details.${row.inputIndex}.designData.${index}`
																									)}
																									onChange={handleChange}
																								/>
																							</div>
																							// </Table.Cell>
																						))}
																					</Table.Cell>
																				),
																				row.description === customInputs.purposeOfConstruction && (
																					<Table.Cell key="purpose-of-contruction">
																						{TableData.row3.input_1.map((input, index) => (
																							// <Table.Cell>
																							<div key={index} className="ui radio checkbox">
																								<input
																									type="radio"
																									name={`details.${row.inputIndex}.designData.option`}
																									value={input}
																									checked={
																										getIn(
																											values,
																											`details.${row.inputIndex}.designData.option`
																										) === input
																									}
																									onChange={handleChange}
																								/>
																								<label
																									htmlFor={`details.${row.inputIndex}.designData.option`}
																								>
																									{input}
																								</label>
																							</div>
																							// </Table.Cell>
																						))}
																						{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																							'अन्य' && (
																								<input
																									type="text"
																									className="dashedForm-control"
																									name={`details.${row.inputIndex}.designData.other`}
																									// id={`details.${row.inputIndex}.designData.${index}`}
																									value={getIn(
																										values,
																										`details.${row.inputIndex}.designData.other`
																									)}
																									onChange={handleChange}
																								/>
																								// <DashedLangInput
																								// 	// inline={true}
																								// 	name={`details.${row.inputIndex}.designData.other`}
																								// 	placeholder="Additional Information..."
																								// 	setFieldValue={setFieldValue}
																								// 	value={values.purposeOfConstructionOther}
																								// 	error={errors.purposeOfConstructionOther}
																								// />
																							)}
																					</Table.Cell>
																				),
																				row.description === customInputs.constructionType && (
																					<Table.Cell key="construction-type">
																						<ConstructionTypeRadio
																							name={`details.${row.inputIndex}.designData`}
																							space={true}
																						/>
																					</Table.Cell>
																				),
																			]
																		)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}
												{getIn(values, `details.8.designData`) === TableData.row8.input_1[0] &&
													mapCheckReport.details &&
													mapCheckReport.details
														.filter(row => row.type === 'P')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																<Table.Cell>{row.description}</Table.Cell>
																{!Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell>
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																		[
																			row.description === customInputs.pillarDetails && (
																				<Table.Cell>
																					{TableData.row17.input_1.map((input, optionIndex) => (
																						// <Table.Cell>
																						<div key={optionIndex} className="ui checkbox">
																							<FormikCheckbox
																								// type="checkbox"
																								name={`details.${row.inputIndex}.designData.option`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData.option`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																							{getIn(
																								values,
																								`details.${row.inputIndex}.designData.option`
																							) &&
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								).includes(input) && (
																									<Table.Row>
																										<Table.Cell>
																											<TableInput
																												name={`details.${row.inputIndex}.designData.value.${optionIndex}`}
																												value={getIn(
																													values,
																													`details.${row.inputIndex}.designData.value.${optionIndex}`
																												)}
																												onChange={handleChange}
																											/>
																										</Table.Cell>
																									</Table.Row>
																								)}
																						</div>
																						// </Table.Cell>
																					))}
																				</Table.Cell>
																			),
																			row.description === customInputs.concreteGrade && (
																				<Table.Cell>
																					{TableData.row20.input_1.map((input, index) => (
																						// <Table.Cell>
																						<div key={index} className="ui radio checkbox">
																							<input
																								type="radio"
																								name={`details.${row.inputIndex}.designData.option`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData.option`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																							<label
																								htmlFor={`details.${row.inputIndex}.designData.option`}
																							>
																								{input}
																							</label>
																						</div>
																						// </Table.Cell>
																					))}
																					{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																						'अन्य' && (
																							<input
																								type="text"
																								className="dashedForm-control"
																								name={`details.${row.inputIndex}.designData.other`}
																								// id={`details.${row.inputIndex}.designData.${index}`}
																								value={getIn(
																									values,
																									`details.${row.inputIndex}.designData.other`
																								)}
																								onChange={handleChange}
																							/>
																						)}
																				</Table.Cell>
																			),
																			row.description === customInputs.concreteBand && (
																				<Table.Cell>
																					{TableData.row31.input_1.map((input, index) => (
																						// <Table.Cell>
																						<div key={index} className="ui checkbox">
																							<FormikCheckbox
																								// type="checkbox"
																								name={`details.${row.inputIndex}.designData`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																						</div>
																						// </Table.Cell>
																					))}
																				</Table.Cell>
																			),
																		]
																	)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}
												{getIn(values, `details.8.designData`) === TableData.row8.input_1[1] &&
													mapCheckReport.details
														.filter(row => row.type === 'G')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																{/* <Table.Cell>{row.description}</Table.Cell> */}
																{row.description === customInputs.garoBibaran ? (
																	<Table.Cell>
																		<Table.Row>{row.description}</Table.Row>
																		{permitData.floor.map(floor => (
																			<Table.Row key={floor.floor}>
																				<Table.Cell>
																					{floorMappingFlat.find(fl => fl.floor === floor.floor).value}
																				</Table.Cell>
																			</Table.Row>
																		))}
																	</Table.Cell>
																) : (
																		<Table.Cell>{row.description}</Table.Cell>
																	)}
																{!Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell>
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																		[
																			row.description === customInputs.concreteGradeGaro && (
																				<Table.Cell>
																					{TableData.row20.input_1.map(input => (
																						// // <Table.Cell>
																						<RadioInput
																							name={`details.${row.inputIndex}.designData.option`}
																							option={input}
																						/>
																						// </Table.Cell>
																					))}
																					{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																						'अन्य' && (
																							<input
																								type="text"
																								className="dashedForm-control"
																								name={`details.${row.inputIndex}.designData.other`}
																								// id={`details.${row.inputIndex}.designData.${index}`}
																								value={getIn(
																									values,
																									`details.${row.inputIndex}.designData.other`
																								)}
																								onChange={handleChange}
																							/>
																						)}
																				</Table.Cell>
																			),
																			row.description === customInputs.concreteBandGaro && (
																				<Table.Cell>
																					{TableData.row31.input_1.map(input => (
																						// <Table.Cell>
																						<div key={input} className="ui checkbox">
																							<FormikCheckbox
																								// type="checkbox"
																								name={`details.${row.inputIndex}.designData`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																						</div>
																						// </Table.Cell>
																					))}
																				</Table.Cell>
																			),
																			row.description === customInputs.garoBibaran && (
																				<Table.Cell>
																					{TableData.row30.input_1.map((input, optionIndex) => (
																						// <Table.Cell>
																						<React.Fragment key={optionIndex}>
																							<FormikCheckbox
																								// type="checkbox"
																								name={`details.${row.inputIndex}.designData.option`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData.option`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																							{getIn(
																								values,
																								`details.${row.inputIndex}.designData.option`
																							) &&
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								).includes(input) &&
																								permitData.floor.map(floor => {
																									// const index = optionIndex + floor.floor;
																									// console.table({'option': optionIndex, 'floor': floor.floor, 'index': index});
																									return (
																										<Table.Row key={floor.floor}>
																											{/* {
																							floorMappingFlat.find(
																								fl => fl.floor === floor.floor
																							).value
																						} */}
																											<Table.Cell>
																												<TableInput
																													name={`details.${row.inputIndex}.designData.value.${floor.floor}.${optionIndex}`}
																													value={getIn(
																														values,
																														`details.${row.inputIndex}.designData.value.${floor.floor}.${optionIndex}`
																													)}
																													onChange={handleChange}
																												/>
																											</Table.Cell>
																										</Table.Row>
																									);
																								})}
																						</React.Fragment>
																						// </Table.Cell>
																					))}
																				</Table.Cell>
																			),
																			row.description === customInputs.cornerStiching && (
																				<Table.Cell>
																					{TableData.row33.input_1.map((input, index) => (
																						// <Table.Cell>
																						<div key={index} className="ui radio checkbox">
																							<input
																								type="radio"
																								name={`details.${row.inputIndex}.designData`}
																								value={input}
																								checked={
																									getIn(
																										values,
																										`details.${row.inputIndex}.designData`
																									) === input
																								}
																								onChange={handleChange}
																							/>
																							<label htmlFor={`details.${row.inputIndex}.designData`}>
																								{input}
																							</label>
																						</div>
																						// </Table.Cell>
																					))}
																				</Table.Cell>
																			),
																		]
																	)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}
											</Table.Body>
										</Table>
									</div>
									{isInaruwa ? (
										<>
											<FooterSignatureMultiline designations={footerData.signatureInaruwa} />

										</>
									) : (
											<>
												<FooterSignatureMultiline designations={footerData.signature} signatureImages={useSignatureImage && [values.subSignature, values.jariSignature]}/>
												<br />
												<div>
													{footerData.section1}{' '}
													<DashedLangInput
														name="numberOfFloors"
														setFieldValue={setFieldValue}
														value={values.numberOfFloors}
														error={errors.numberOfFloors}
													/>{' '}
													{footerData.section2}
												</div>
												<br />
												<Grid columns="2" className="flex-item-flex-start">
													<Grid.Row>
														<Grid.Column>
															{footerData.gharDhani}: {permitData.nibedakName}
														</Grid.Column>
														<Grid.Column>
															{footerData.address}: {permitData.applicantAddress}
														</Grid.Column>
													</Grid.Row>
													<Grid.Row>
														<Grid.Column>
															{footerData.sahi}: <span className="ui input dashedForm-control" />
														</Grid.Column>
														<Grid.Column>
															{footerData.date}
															{': '}
															<DashedLangDateField
																name="noObjectionDate"
																inline={true}
																setFieldValue={setFieldValue}
																value={values.noObjectionDate}
																error={errors.noObjectionDate}
															/>
														</Grid.Column>
													</Grid.Row>
												</Grid>
											</>
										)}
									<Divider />
									<div>{footerData.note}</div>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}
const prepareInitialVaules = (prevData, pulledData) => {
	const ret = { details: [] };

	if (prevData.details && prevData.details.length > 0) {
		prevData.details.forEach((row, index) => {
			const pulledVal = pulledData.details.find(el => el.description === row.description);
			if (row.descriptionId === 8) {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? getConstructionTypeValue(row.designData) : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			} else {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? row.designData : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			}
		});
	}

	return ret;
};

const NoObjectionBodyInaruwa = ({ permitData, userData, user_info }) => {
	return (
		<>
			<div>
				<LetterHeadFlex userInfo={user_info} />

				<div className="flex-item-space-between">

					<div>
						{noObjectionData.bodyInaruwa.pramanPatra}

					</div>
					<div>
						<SectionHeader>
							<h2>{noObjectionData.bodyInaruwa.title_1}</h2>
							<h2>{noObjectionData.bodyInaruwa.title_2}</h2>
							<h2>{noObjectionData.bodyInaruwa.title_3}</h2>
						</SectionHeader>
					</div>
					<div className="certificate-image">
						{permitData.photo && <img src={`${getDocUrl()}${permitData.photo}`} alt="imageFile" />}
					</div>
				</div>
				<FlexSingleLeft>
					<ShreeDropdown name="shree" />{permitData.nibedakName}<br />
					{noObjectionData.bodyInaruwa.address}{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}{noObjectionData.bodyInaruwa.basney}
				</FlexSingleLeft>
			</div>
			{noObjectionData.bodyInaruwa.content_1},
			<div>
				<span className='indent-span'>{noObjectionData.bodyInaruwa.content_2}</span>
				{userData.organization.name}
				{noObjectionData.bodyInaruwa.content_3}{permitData.newWardNo}
				{noObjectionData.bodyInaruwa.content_4} {permitData.oldMunicipal}
				{noObjectionData.bodyInaruwa.content_5} {permitData.newMunicipal}
				{noObjectionData.body.kitta}{permitData.kittaNo}
				{noObjectionData.bodyInaruwa.content_6}
			</div>
		</>
	)
}
const NoObjectionView = parentProps => (
	<FormContainerV2
		api={[
			// { api: api.allowancePaper, objName: 'allowancePaper', form: true }
			{
				api: api.noObjectionSheet,
				objName: 'noObjectionSheet',
				form: true,
			},
			{
				api: api.mapCheckReport,
				objName: 'mapCheckReport',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTechnicalDescription',
				form: false,
			},
			// {
			//     api: api.anusuchiKaMaster,
			//     objName: 'buildingClass',
			//     form: false,
			//     utility: true
			// }
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		prepareData={data => data}
		parentProps={parentProps}
		render={props => <NoObjectionSheet {...props} parentProps={parentProps} />}
	/>
);

export default NoObjectionView;
