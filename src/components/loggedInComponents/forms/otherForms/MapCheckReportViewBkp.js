import React, { Component } from 'react';
import { Form, Table, Input, Label, Button, Tab } from 'semantic-ui-react';

import { mapTechnical } from '../../../../utils/data/mockLangFile';
import { Formik, Field } from 'formik';

// Redux
import { connect } from 'react-redux';
import { getPermitAndUserData } from '../../../../store/actions/nakxsaAction';
import { postFormDataByUrl } from '../../../../store/actions/formActions';
import { showToast } from '../../../../utils/functionUtils';
import FallbackComponent from '../../../shared/FallbackComponent';
import { DataDisplay } from '../../../shared/DataDisplay';
import { SemanticToastContainer } from 'react-semantic-toasts';

import * as Yup from 'yup';
import { naksaData } from '../../../../utils/data/nakxsaData';
import DateField from '../../../shared/DateField';
import ErrorDisplay from '../../../shared/ErrorDisplay';
const mapTech_data = mapTechnical.mapTechnicalDescription;

let data = naksaData.structureDesign;
let TableData = naksaData.structureDesign.table.rows;

class MapCheckReportView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: {
        index: 0,
        value: TableData.row8.input_1[0]
      }
    };
  }
  componentDidMount() {
    this.props.getPermitAndUserData();
  }

  handleFilterInputChangeRoofStyle = e => {
    const updateRoofStyle = e.target.value;
    if ('अन्य' === updateRoofStyle) {
      this.setState({
        inputRoofStyleFilter: true
      });
    } else {
      this.setState({ inputRoofStyleFilter: false });
    }
    this.setState({ RoofStyle: updateRoofStyle });
  };

  handleOptionToggle = (e, index) => {
    // console.log('state toggle', this.state.toggle);
    this.setState({
      toggle: {
        index: index,
        value: e.target.value
      },
      bhawanType: TableData.row7.input_1[0]
    });
  };

  render() {
    // console.log('naksa props', this.props);
    // console.log('naksa state', this.state);

    const { bhawanType } = this.state;

    const validationSchema = Yup.object().shape({
      date: Yup.date()
    });

    const checkedBhawanType = [];
    checkedBhawanType[0] =
      bhawanType === TableData.row7.input_1[0] ? 'defaultChecked' : null;
    checkedBhawanType[1] =
      bhawanType === TableData.row7.input_1[1] ? 'defaultChecked' : null;

    if (this.props.permitData && this.props.userData) {
      // const initialDetails = Array(18).fill({ value: '', remarks: 'a' });
      const initialDetails = [];
      // console.log('initialDetals', initialDetails);

      const { permitData, userData } = this.props;
      return (
        <div className="NJ-Main">
          {this.props.errors && (
            <ErrorDisplay message={this.props.errors.message} />
          )}
          <div className="NJ-header">
            <p>{data.heading}</p>
            <p>{userData.organization.name}</p>
          </div>
          {initialDetails ? (
            <MapCheckReportViewForm
              permitData={permitData}
              userData={userData}
              checkedBhawanType={checkedBhawanType}
              initialDetails={initialDetails}
              state={this.state}
              props={this.props}
              handleOptionToggle={this.handleOptionToggle}
              validationSchema={validationSchema}
            />
          ) : (
              <MapCheckReportViewForm
                permitData={permitData}
                userData={userData}
                checkedBhawanType={checkedBhawanType}
                state={this.state}
                props={this.props}
                handleOptionToggle={this.handleOptionToggle}
                validationSchema={validationSchema}
              />
            )}
        </div>
      );
    } else {
      return (
        <FallbackComponent
          loading={this.props.loading}
          errors={this.props.errors}
        />
      );
    }
  }
}

const MapCheckReportViewForm = ({
  permitData,
  initialDetails,
  userData,
  checkedBhawanType,
  state,
  props,
  handleOptionToggle,
  validationSchema
}) => (
    <Formik
      initialValues={{
        bhawanType: TableData.row7.input_1[0],
        details: initialDetails || []
      }}
      validationSchema={validationSchema}
      onSubmit={async (values, actions) => {
        // const { toggle } = this.state
        // const { "details.5.value } = toggle.value
        values.details[5] = {};
        values.details[5].value = state.toggle.value;
        values = { ...values, ...state };
        // console.log('data to send', values);

        const designData = [];
        const remark = [];
        const details = [];
        values.details.map((row, index) => {
          details.push({
            descriptionId: index,
            designData: row.value,
            remark: row.remarks
          });
          // remark.push(row.remarks)
        });

        // console.log('Data preproces', details);
        const dataToSend = {
          // designData: 'asdf',
          // remark: 'asdf',
          // applicationNo: permitData.applicantNo,
          // enterDate: '2017-12-12',
          // enterBy: 'asdf',
          // serName: 'asdf',
          // serDate: '2012-12-12',
          // serStatus: 's',
          // erName: 'e',
          // erDate: '2017-12-12',
          // erStatus: '2',
          // landDetails: [
          //   {
          //     applicantname: '1',
          //     area: '1',
          //     ward: '1',
          //     kittano: '1',
          //     road: '1'
          //   }
          // ],
          // floorDetails: [],
          details: [
            { descriptionId: 31, designData: 'Good', remark: 'Description id 31 remark' }
          ]
          // details: details
          // id:"23",
          // sn: '12',
          // type: 'aas',
          // buildingDescription: 'asdf',
          // descriptionId: 123,
          // designData: [{value: 'asf'}, {value: 'asdf'}],
          // remark: [{remark: 'asdf'}, { remark: 'asdfss'}]
        };
        // console.log('Data to send', dataToSend);
        actions.setSubmitting(true);
        try {
          await props.postFormDataByUrl(
            `api/Processing/Phase1/MapCheckReport/${permitData.applicantNo}`,
            dataToSend
          );

          actions.setSubmitting(false);

          window.scrollTo(0, 0);
          if (props.success) {
            showToast('Your data has been successfully');
          }
        } catch (err) {
          // remark: 'asdf',
          // console.log('err struct form ', err);
          actions.setSubmitting(false);
          window.scrollTo(0, 0);
        }
      }}
      render={({ handleSubmit, values, handleChange, isSubmitting, errors }) => {
        // console.log('form props naksa--', values, errors);
        // console.log('state -- ', state);
        return (
          <Form onSubmit={handleSubmit} loading={isSubmitting}>
            <SemanticToastContainer />

            <div className="NJ-title">
              <h5>{data.subheading1}</h5>
              <p>{data.subheading2}</p>
              <Form.Field>
                <div className="NJ-right">
                  <Form.Input error={errors.date}>
                    <DateField
                      name="date"
                      handleChange={handleChange}
                      label={data.date}
                    />
                  </Form.Input>
                </div>
              </Form.Field>
            </div>

            <p className="NJ-header">
              <p>{data.main_head}</p>
              <span>{userData.organization.name}</span> {data.ward_no}{' '}
              <span>{permitData.newWardNo}</span>, {data.kitta_no}{' '}
              <span>{permitData.kittaNo}</span> {data.area}{' '}
              <span>{permitData.landArea}</span> {data.main_data}{' '}
              <span>{permitData.applicantName}</span> {data.main_data1}
            </p>

            <h5 className="NJ-title">{data.table.heading}</h5>
            <div className="NayaJhanchPachi">
              <Table celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>{TableData.row1.sn}</Table.HeaderCell>
                    <Table.HeaderCell>{TableData.row1.name}</Table.HeaderCell>
                    <Table.HeaderCell>{TableData.row1.input_1}</Table.HeaderCell>
                    <Table.HeaderCell>{TableData.row1.input_2}</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  <Table.Row>
                    <Table.Cell>{TableData.row2.sn}</Table.Cell>
                    <Table.Cell>{TableData.row2.name}</Table.Cell>
                    <Table.Cell>
                      {TableData.row2.input_1.map(input => (
                        <div class="ui radio checkbox">
                          <input
                            type="radio"
                            value={values.input_1}
                            name="bhawan_diff"
                            id={`bhawanbuild_${input}`}
                            defaultChecked
                            onChange={handleChange}
                          />
                          <label for={`bhawanbuild_${input}`}>{input}</label>
                        </div>
                      ))}
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <input
                          name={`${TableData.row2.name}_1`}
                          value={values.input_2}
                          onChange={handleChange}
                        />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  {Object.keys(TableData)
                    .splice(2, 2)
                    .map((key, index) => {
                      const currentRow = TableData[key];
                      return (
                        <Table.Row>
                          <Table.Cell>{currentRow.sn}</Table.Cell>
                          <Table.Cell>{currentRow.name}</Table.Cell>
                          <Table.Cell>
                            {/* <Input label="आवासीय" /> */}
                            <Form.Field>
                              <Field name={`details.${index}.value`} />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell>
                            <Form.Field>
                              <Field name={`details.${index}.remarks`} />
                            </Form.Field>
                          </Table.Cell>
                        </Table.Row>
                      );
                    })}
                  <Table.Row>
                    <Table.Cell>{TableData.row5.sn}</Table.Cell>
                    <Table.Cell>{TableData.row5.name}</Table.Cell>
                    <Table.Cell>
                      {TableData.row5.input_1.map(input => (
                        <Input labelPosition="right" type="text">
                          <Label>{input.prefix}</Label>
                          <Field
                            className="smallInput"
                            name={`details.2.value`}
                            // value={values.input_1}
                            onChange={handleChange}
                          />
                          <Label basic>{input.postfix}</Label>
                        </Input>
                      ))}
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <input
                          name={`details.2.remarks`}
                          value={values.input_2}
                          onChange={handleChange}
                        />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  <Table.Row>
                    <Table.Cell>{TableData.row6.sn}</Table.Cell>
                    <Table.Cell>{TableData.row6.name}</Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <input
                          name={`details.3.value`}
                          value={values.input_1}
                          onChange={handleChange}
                        />
                      </Form.Field>
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <input
                          name={`details.3.remarks`}
                          value={values.input_2}
                          onChange={handleChange}
                        />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  {/* Toggle Options   */}
                  <Table.Row>
                    <Table.Cell>{TableData.row7.sn}</Table.Cell>
                    <Table.Cell>{TableData.row7.name}</Table.Cell>
                    <Table.Cell>
                      {TableData.row7.input_1.map((input, index) => (
                        <div class="ui radio checkbox">
                          <input
                            type="radio"
                            name="bhawanType"
                            value={input}
                            defaultChecked={checkedBhawanType[index]}
                            onChange={handleChange}
                          />
                          <label for={`bhawantype_${input}`}>{input}</label>
                        </div>
                      ))}
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <input
                          name={`details.4.remarks`}
                          value={values.input_2}
                          onChange={handleChange}
                        />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  <Table.Row>
                    <Table.Cell>{TableData.row8.sn}</Table.Cell>
                    <Table.Cell>{TableData.row8.name}</Table.Cell>
                    <Table.Cell>
                      {TableData.row8.input_1.map((input, index) => (
                        <div class="ui radio checkbox">
                          <input
                            type="radio"
                            value={TableData.row8.input_1[index]}
                            name={`details.5.value`}
                            checked={state.toggle.index === index}
                            onChange={e => handleOptionToggle(e, index)}
                          />
                          <label for={`bhawanStructure_${input}`}>{input}</label>
                        </div>
                      ))}
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <Field name={`details.5.remarks`} />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  <Table.Row>
                    <Table.Cell>{TableData.row9.sn}</Table.Cell>
                    <Table.Cell>{TableData.row9.name}</Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <Field name={`details.6.value`} />
                      </Form.Field>
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <Field name={`details.6.remarks`} />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  <Table.Row>
                    <Table.Cell>{TableData.row10.sn}</Table.Cell>
                    <Table.Cell>{TableData.row10.name}</Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <Field name={`details.7.value`} />
                      </Form.Field>
                    </Table.Cell>
                    <Table.Cell>
                      <Form.Field>
                        <Field name={`details.7.remarks`} />
                      </Form.Field>
                    </Table.Cell>
                  </Table.Row>

                  {/* क Form.Field */}
                  {state.toggle.index === 0 && (
                    <>
                      <Table.Row>
                        <Table.Cell>{TableData.row11.sn}</Table.Cell>
                        <Table.Cell>{TableData.row11.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.8.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.8.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row12.sn}</Table.Cell>
                        <Table.Cell>{TableData.row12.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.9.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.9.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row13.sn}</Table.Cell>
                        <Table.Cell>{TableData.row13.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.10.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.10.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row14.sn}</Table.Cell>
                        <Table.Cell>{TableData.row14.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.11.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.11.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row15.sn}</Table.Cell>
                        <Table.Cell>{TableData.row15.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row15.input_1.map(input => (
                            <Input labelPosition="right" type="text">
                              <Field
                                className="smallInput"
                                name={`details.12.value`}
                              />
                              <Label>{input.prefix}</Label>
                            </Input>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.12.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row16.sn}</Table.Cell>
                        <Table.Cell>{TableData.row16.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.13.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.13.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row17.sn}</Table.Cell>
                        <Table.Cell>{TableData.row17.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.14.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.14.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row18.sn}</Table.Cell>
                        <Table.Cell>{TableData.row18.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.15.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.15.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row19.sn}</Table.Cell>
                        <Table.Cell>{TableData.row19.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.16.value`} />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.16.remarks`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row20.sn}</Table.Cell>
                        <Table.Cell>{TableData.row20.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row20.input_1.map((input, index) => (
                            <>
                              <Input labelPosition="right" type="text">
                                <Field
                                  className="smallInput"
                                  name={`details.17.value.${index}`}
                                />
                                <Label>{input}</Label>
                              </Input>
                              <br />
                              <br />
                            </>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            {TableData.row20.input_1.map((input, index) => (
                              <>
                                <Field name={`details.17.remarks`} />
                                <br />
                                <br />
                              </>
                            ))}
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row21.sn}</Table.Cell>
                        <Table.Cell>{TableData.row21.name}</Table.Cell>
                        <Table.Cell colSpan="2">
                          <Field name={`details.18.value`} />
                        </Table.Cell>
                      </Table.Row>
                    </>
                  )}

                  {/* ख Form --------------------------------- */}

                  {state.toggle.index === 1 && (
                    <>
                      <Table.Row>
                        <Table.Cell>{TableData.row22.sn}</Table.Cell>
                        <Table.Cell>{TableData.row22.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.19.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.19.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row23.sn}</Table.Cell>
                        <Table.Cell>{TableData.row23.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.20.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.20.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row24.sn}</Table.Cell>
                        <Table.Cell>{TableData.row24.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.21.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.21.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row25.sn}</Table.Cell>
                        <Table.Cell>{TableData.row25.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.22.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.22.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row26.sn}</Table.Cell>
                        <Table.Cell>{TableData.row26.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row26.input_1.map((input, index) => (
                            <>
                              <Input labelPosition="right" type="text">
                                <Field
                                  name={`details.23.value${index}`}
                                  onChange={handleChange}
                                />
                                <Label>{input}</Label>
                              </Input>
                              <br />
                              <br />
                            </>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.23.remarks`}
                              value={values.input_2}
                              onChange={handleChange}
                            />
                            <br />
                            <br />
                            <input
                              name={`${TableData.row26.name}_3`}
                              value={values.input_3}
                              onChange={handleChange}
                            />
                            <br />
                            <br />
                            <input
                              name={`${TableData.row26.name}_4`}
                              value={values.input_4}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row27.sn}</Table.Cell>
                        <Table.Cell>{TableData.row27.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.24.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.24.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row28.sn}</Table.Cell>
                        <Table.Cell>{TableData.row28.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.25.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.25.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row29.sn}</Table.Cell>
                        <Table.Cell>{TableData.row29.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row29.input_1.map(input => (
                            <Input labelPosition="right" type="text">
                              <Field
                                className="smallInput"
                                name={`details.26.value`}
                                onChange={handleChange}
                              />
                              <Label>{input.prefix}</Label>
                            </Input>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.26.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row30.sn}</Table.Cell>
                        <Table.Cell>{TableData.row30.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row30.input_1.map(input => (
                            <Table.Cell
                              style={{
                                width: '38.5%',
                                textAlign: 'center',
                                borderTop: '0'
                              }}
                            >
                              {input}
                            </Table.Cell>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            {/* <Field
                            name={`details.27.remarks`}
                            onChange={handleChange}
                          /> */}
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell />
                        <Table.Cell>{TableData.row30.name1}</Table.Cell>
                        <Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.height.0`}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.width.0`}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.length.0`}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.27.remarks.0`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell />
                        <Table.Cell>{TableData.row30.name2}</Table.Cell>
                        <Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.height.1`}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.width.1`}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field
                                name={`details.27.value.length.1`}
                                value={values.input_3}
                                onChange={handleChange}
                              />
                            </Form.Field>
                          </Table.Cell>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.27.remarks.1`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell />
                        <Table.Cell>{TableData.row30.name3}</Table.Cell>
                        <Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field name={`details.27.value.height.2`} />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field name={`details.27.value.width.2`} />
                            </Form.Field>
                          </Table.Cell>
                          <Table.Cell style={{ borderTop: '0' }}>
                            <Form.Field>
                              <Field name={`details.27.value.length.2`} />
                            </Form.Field>
                          </Table.Cell>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field name={`details.27.remarks.2`} />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row31.sn}</Table.Cell>
                        <Table.Cell>{TableData.row31.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row31.input_1.map(input => (
                            <div class="ui radio checkbox">
                              <Field
                                type="radio"
                                value={values.input_1}
                                name={`details.28.value`}
                                id={`kangreetlevel_${input}`}
                                onChange={handleChange}
                                defaultChecked
                              />
                              <label for={`kangreetlevel_${input}`}>
                                {input}
                              </label>
                            </div>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.28.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row32.sn}</Table.Cell>
                        <Table.Cell>{TableData.row32.name}</Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.29.value`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.29.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row33.sn}</Table.Cell>
                        <Table.Cell>{TableData.row33.name}</Table.Cell>
                        <Table.Cell>
                          {TableData.row33.input_1.map(input => (
                            <div class="ui radio checkbox">
                              <Field
                                type="radio"
                                value={values.input_1}
                                name={`details.30.value`}
                                id={`corner_${input}`}
                                defaultChecked
                              />
                              <label for={`corner_${input}`}>{input}</label>
                            </div>
                          ))}
                        </Table.Cell>
                        <Table.Cell>
                          <Form.Field>
                            <Field
                              name={`details.30.remarks`}
                              onChange={handleChange}
                            />
                          </Form.Field>
                        </Table.Cell>
                      </Table.Row>

                      <Table.Row>
                        <Table.Cell>{TableData.row34.sn}</Table.Cell>
                        <Table.Cell>{TableData.row34.name}</Table.Cell>
                        <Table.Cell colSpan="2">
                          <Field name={`details.31.value`} />
                        </Table.Cell>
                      </Table.Row>
                    </>
                  )}
                </Table.Body>
              </Table>
            </div>
            <div>
              <h5>{data.recommendtitle}</h5>
              <p>
                {data.fullname} {permitData.applicantName}{' '}
              </p>
              <p>
                {data.level} {permitData.applicationActionBy}
              </p>
              <p>{data.signature} .......................................</p>
              <p>
                {data.date} {permitData.applicantDate}
              </p>
            </div>
            <Button primary>Save</Button>
          </Form>
        );
      }}
    />
  );

const mapDispatchToProps = { getPermitAndUserData, postFormDataByUrl };
const mapStateToProps = state => ({
  permitData: state.root.formData.permitData,
  userData: state.root.formData.userData,
  loading: state.root.ui.loading,
  errors: state.root.ui.errors,
  success: state.root.formData.success
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapCheckReportView);
