import React, { useState, useEffect } from 'react';
import { naksaData } from '../../../../utils/data/nakxsaData';
import { Table } from 'semantic-ui-react';
import { toArray } from '../../../../utils/dataUtils';
import { getUnitValue } from '../../../../utils/functionUtils';
import { SectionHeader } from '../../../uiComponents/Headers';

const TableData = naksaData.structureDesign.table.rows;
const customInputs = naksaData.structureDesign.customInputs;
const heading = naksaData.structureDesign.table.heading;

export const MapCheckReportPrintTable = ({ details }) => {
	const [data, setData] = useState([]);

	useEffect(() => {
		if (details && Array.isArray(details) && details.length > 0) {
			const typeRow = details.find((row) => row.descriptionId === 9);
			const type = typeRow ? (typeRow.designData === TableData.row8.input_1[0] ? 'P' : 'G') : '';
			const typeFiltered = details.filter((row) => row.type === type || row.type === '');

			const formatted = typeFiltered.map((row) => {
				if (
					row.description === customInputs.landLength ||
					row.description === customInputs.landWidth ||
					row.description === customInputs.plinthArea
				) {
					try {
						const arr = toArray(row.designData);
						const designData = arr[0] ? `${arr[0]} ${getUnitValue(arr[1])}` : '';
						return { ...row, designData };
					} catch (error) {
						return row;
					}
				} else if (
					row.description === customInputs.purposeOfConstruction ||
					row.description === customInputs.concreteGrade ||
					row.description === customInputs.concreteGradeGaro
				) {
					try {
						const { option, other } = JSON.parse(row.designData);
						const designData = other || option;
						return { ...row, designData };
					} catch (error) {
						return row;
					}
				} else if (row.description === customInputs.buildingHeight) {
					try {
						const arr = toArray(row.designData);
						const designData1 = arr[0] ? `${arr[0]} ${getUnitValue(arr[2])}` : '';
						const designData2 = arr[1] ? `${arr[1]} ${getUnitValue(arr[3])}` : '';
						return { ...row, designData: `${TableData.row6.input_1[0]} ${designData1}, ${TableData.row6.input_1[1]} ${designData2}` };
					} catch (error) {
						return row;
					}
				} else if (row.description === customInputs.pillarDetails) {
					try {
						const { option = [], value = [] } = JSON.parse(row.designData);
						const designData =
							option &&
							Array.isArray(option) &&
							option.map((opt, idx) => opt && `${opt} ${value && value[idx] ? '(' + value[idx] + ')' : ''}`);
						return { ...row, designData };
					} catch (error) {
						return row;
					}
				} else return row;
			});
			setData(formatted);
		}
	}, [details]);

	return (
		<div className="no-page-break-inside">
			<SectionHeader>
				<h3>{heading}</h3>
			</SectionHeader>
			<Table className="certificate-ui-table">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{TableData.row1.sn}</Table.HeaderCell>
						<Table.HeaderCell>{TableData.row1.name}</Table.HeaderCell>
						<Table.HeaderCell>{TableData.row1.input_1}</Table.HeaderCell>
						<Table.HeaderCell>{TableData.row1.input_2}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{data.map((row) => (
						<Table.Row>
							<Table.Cell>{row.sn}</Table.Cell>
							<Table.Cell>{row.description}</Table.Cell>
							<Table.Cell>{String(row.designData)}</Table.Cell>
							<Table.Cell>{row.remark}</Table.Cell>
						</Table.Row>
					))}
				</Table.Body>
			</Table>
		</div>
	);
};
