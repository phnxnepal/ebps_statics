import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { CompactDashedLangDate } from '../../shared/DateField';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { SectionHeader } from '../../uiComponents/Headers';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { MyadApprovalData } from '../../../utils/data/MyadThapApprovalData';
import { LetterHead } from './../../shared/LetterHead';
import { PatraSankhya } from './formComponents/PatraSankhyaAndDate';
import { DashedLangDateField } from './../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { FlexSingleLeft } from './../../uiComponents/FlexDivs';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { showToast } from '../../shared/viewToast';

const myad = MyadApprovalData.myad_data;
const opt = [
    { key: 'ghar', value: 'घरको', text: 'घरको' },
    { key: 'parkhal', value: 'नयाँ पर्खालको', text: 'नयाँ पर्खालको' },
    { key: 'godam', value: 'गोदामको', text: 'गोदामको' },
];
const MyadApprovalSchema = Yup.object().shape(
    Object.assign({
        approveDate: validateNullableNepaliDate,
        supStrucDate: validateNullableNepaliDate,
        dates: validateNullableNepaliDate,
    })
);
class MyadApproval extends Component {
    constructor(props) {
        super(props);
        let initialValues;
        const { prevData, otherData } = props;
        const json_data = getJsonData(prevData);
        initialValues = prepareMultiInitialValues(
            //prev values
            { obj: getJsonData(otherData.superStructure), reqFields: ['supStrucDate'] },
            { obj: getJsonData(otherData.myadTippani), reqFields: ['dates'] },

            { obj: json_data, reqFields: [] }
        );
        if (isStringEmpty(initialValues.approveDate)) {
            initialValues.approveDate = getCurrentDate(true);
        }
        this.state = { initialValues: { ghar: opt[0].value, ...initialValues } }
    }
    render() {
        const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
        const { initialValues } = this.state;
        const user_info = this.props.userData;
        return (
            <Formik
                initialValues={initialValues}
                validationSchema={MyadApprovalSchema}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    values.applicationNo = permitData.applicationNo;
                    values.error && delete values.error;
                    try {
                        await this.props.postAction(`${api.MyadThapApproval}${permitData.applicationNo}`, values);
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        if (this.props.success && this.props.success.success) {
                            handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                        }
                    } catch (err) {
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        showToast('Something went wrong !!');
                    }
                }}
            >
                {({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors }) => (
                    <Form loading={isSubmitting} className="ui form">
                        {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

                        <div ref={this.props.setRef}>
                            <LetterHead userInfo={user_info} />
                            <PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
                            <FlexSingleRight>
                                {`${myad.date}`}
                                <DashedLangDateField
                                    compact={true}
                                    name="approveDate"
                                    value={values.approveDate}
                                    error={errors.approveDate}
                                    setFieldValue={setFieldValue}
                                    inline={true}
                                />
                            </FlexSingleRight>
                            <SectionHeader>
                                <h3 className="normal underline end-section">{myad.subject}</h3>
                            </SectionHeader>
                            <FlexSingleLeft>
                                {`${myad.salutation}`}
                                <span className="ui input dashedForm-control" />
                            </FlexSingleLeft>
                            <br />
                            <div style={{ textAlign: 'justify ' }}>
                                <span className="indent-span">
                                    {myad.content.content_1}
                                </span>
                                {userData.organization.name}
                                {myad.content.content_2}
                                {permitData.newWardNo}
                                {myad.content.content_5}
                                {permitData.kittaNo}
                                {myad.content.content_6}
                                {permitData.landArea}{permitData.landAreaType}
                                {myad.content.content_7}
                                <Select
                                    name="ghar"
                                    options={opt}
                                    onChange={(e, { value }) => {
                                        setFieldValue('ghar', value);
                                    }}
                                    value={values.ghar}
                                />
                                {myad.content.content_8}
                                <CompactDashedLangDate
                                    name="supStrucDate"
                                    setFieldValue={setFieldValue}
                                    value={values.supStrucDate}
                                    error={errors.supStrucDate}
                                />
                                {myad.content.content_9}
                                <CompactDashedLangDate
                                    name="dates"
                                    setFieldValue={setFieldValue}
                                    value={values.dates}
                                    error={errors.dates}
                                />
                                {myad.content.content_10}
                            </div>
                            <br />
                            <FlexSingleLeft>
                                <div>{myad.content.content_11}{permitData.applicationNo}</div>
                            </FlexSingleLeft>
                        </div>
                        <SaveButtonValidation
                            errors={errors}
                            validateForm={validateForm}
                            formUrl={formUrl}
                            hasSavePermission={hasSavePermission}
                            hasDeletePermission={hasDeletePermission}
                            isSaveDisabled={isSaveDisabled}
                            prevData={checkError(prevData)}
                            handleSubmit={handleSubmit}
                        />
                    </Form>
                )}
            </Formik>
        );
    }
}

const MyadThapApproval = parentProps => (
    <FormContainerV2
        api={[
            {
                api: api.MyadThapApproval,
                objName: 'myadApproval',
                form: true,
            },
            {
                api: api.superStructureConstruction,
                objName: 'superStructure',
                form: false,
            },
            {
                api: api.MyadThapTippani,
                objName: 'myadTippani',
                form: false,
            },
        ]}
        onBeforeGetContent={{
            ...PrintParams.INLINE_FIELD,
            ...PrintParams.TEXT_AREA,
            param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
            param7: ['getElementsByClassName', 'ui checkbox', 'label'],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
        }}
        prepareData={data => data}
        parentProps={parentProps}
        useInnerRef={true}
        render={props => <MyadApproval {...props} parentProps={parentProps} />}
    />
);
export default MyadThapApproval;
