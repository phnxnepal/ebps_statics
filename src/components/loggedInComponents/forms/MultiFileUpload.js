import React, { Component } from 'react';
import { Form, Divider, List, Icon, Label } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { postFileValidation, getUploadedFilesValidation } from '../../../store/actions/fileActions';

import { Field, getIn } from 'formik';
import { getSplitUrl, hasMultipleFiles } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';

import { validateFileField, validateOptionalFileField } from '../../../utils/validationUtils';
import { getDocUrl } from '../../../utils/config';
import Viewer from 'react-viewer';
import DeleteModal from '../../shared/DeleteModal';
import { deleteFile } from '../../../store/actions/fileActions';
import { ImageDisplayPreview } from '../../shared/file/FileView';
// import { IMAGE } from '../../../utils/constants/fileConstants';
// let urlArr = [];

const getValidationSchema = (isRequired, hasFiles) => {
	if (hasFiles) {
		return validateOptionalFileField;
	} else if (isRequired && isRequired === 'Y') {
		return validateFileField;
	} else {
		return validateOptionalFileField;
	}
};

export class MultiFileUpload extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currImg: 0,
			id: '',
			isDeleteModalOpen: false,
			applicationNo: props.permitData && props.permitData.applicationNo,
		};
	}

	// componentDidMount() {
	// this.props.getUploadedFilesValidation(this.state.applicationNo);
	// }

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success && this.props.success && this.state.applicationNo) {
			this.props.getUploadedFilesValidation(this.state.applicationNo);
		}
	}

	handleChange = (e, fileCatId) => {
		const arrFiles = Array.from(e.target.files);
		this.props.handleFileChange({ fileCatId, files: arrFiles });
	};

	handleDeleteModalOpen = id => {
		this.setState({
			isDeleteModalOpen: true,
			id: id,
		});
	};

	handleDeleteModalClose = () => {
		this.setState({
			isDeleteModalOpen: false,
		});
	};

	render() {
		const { files, hasDeletePermission } = this.props;

		return (
			<React.Fragment>
				<Form loading={this.props.loading}>
					<>
						{this.props.fileCategories
							.filter(category => category.viewUrl && category.viewUrl.trim() === this.props.url)
							.map(category => {
								const categoryFiles = files && files.length > 0 ? files.filter(file => file.fileType.id === category.id) : [];

								return (
									<div key={category.id}>
										{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
										<Divider horizontal>{category.name}</Divider>{' '}
										<Field>
											{({ field, form }) => {
												const handleChange = e => {
													const arrFiles = Array.from(e.target.files);
													this.props.handleFileChange({ fileCatId: category.id, fileCatName: category.name, files: arrFiles });
													form.setFieldValue(`uploadFile._${category.id}`, arrFiles);
													const multipleFiles = arrFiles.map((file, index) => {
														const src = window.URL.createObjectURL(file);
														return { file, id: index, src };
													});

													form.setFieldValue(`imagePreviewUrl._${category.id}`, multipleFiles);
												};

												const error = getIn(form.errors, `uploadFile._${category.id}`);
												const values = form.values;
												return (
													<Form.Field error={!!error}>
														<Field
															type="file"
															name={`uploadFile._${category.id}`}
															id={`file_${category.id}`}
															multiple={hasMultipleFiles(category.isMultiple)}
															value={undefined}
															onChange={handleChange}
															validate={getValidationSchema(category.isRequired, categoryFiles.length > 0)}
														/>
														{error && (
															<Label pointing prompt size="large">
																{error}
															</Label>
														)}
														{getIn(values, `imagePreviewUrl._${category.id}`) && (
															<div className="previewFileUpload">
																{getIn(values, `imagePreviewUrl._${category.id}`).map((value, index) => (
																	<div key={index} className={'eachPreviewFile'}>
																		<ImageDisplayPreview
																			isInput={true}
																			src={value.src}
																			alt={value.file.name}
																			type={value.file.type}
																			title={value.file.name}
																		/>
																	</div>
																))}
															</div>
														)}
													</Form.Field>
												);
											}}
										</Field>
										{/* ); */}
										<List relaxed>
											<List.Header>Existing Files</List.Header>
											<div className="previewFileUpload">
												{categoryFiles.length > 0 ? (
													categoryFiles.map(el => {
														const fileTitleName = getSplitUrl(el.fileUrl, '/');
														return (
															<div key={el.fileUrl} className="eachPreviewFile viewFileSuccess" title={fileTitleName}>
																<>
																	<ImageDisplayPreview
																		src={el.fileUrl}
																		alt={fileTitleName}
																		onClick={() =>
																			this.setState({
																				imageOpen: true,
																				currImg: `${getDocUrl()}${el.fileUrl}`,
																			})
																		}
																	/>
																	{hasDeletePermission && (
																		<Icon
																			name="close"
																			className="closeImg"
																			onClick={() => this.handleDeleteModalOpen(el.id)}
																		/>
																	)}
																</>
															</div>
														);
													})
												) : (
													<List.Item>No files uploaded.</List.Item>
												)}
											</div>
										</List>
										<Viewer
											visible={this.state.imageOpen}
											onClose={() => this.setState({ imageOpen: false })}
											images={[{ src: this.state.currImg }]}
											// activeIndex={this.state.currImg}
											zIndex={10000}
										/>
									</div>
								);
							})}
					</>
					<DeleteModal
						open={this.state.isDeleteModalOpen}
						onClose={this.handleDeleteModalClose}
						history={this.props.parentHistory}
						loadingModal={this.props.loadingModal}
						id={this.state.id}
						deleteFile={() => this.props.deleteFile(this.state.id)}
					/>
				</Form>
			</React.Fragment>
		);
	}
}

const mapDispatchToProps = {
	postFileValidation,
	getUploadedFilesValidation,
	deleteFile,
};

const mapStateToProps = state => ({
	files: state.root.formData.files,
	fileCategories: state.root.formData.fileCategories,
	success: state.root.formData.success,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	permitData: state.root.formData.permitData,
});

export default connect(mapStateToProps, mapDispatchToProps)(MultiFileUpload);
