import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { dosrocharanPrabidhikview } from '../../../utils/data/mockLangFile';
import { Formik, Field, getIn } from 'formik';
import api from '../../../utils/api';
// import merge from 'lodash/merge';
import { isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { DashedLangDateField } from '../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import EbpsTextareaField from '../../shared/MyTextArea';
import { EbpsTextArea } from '../../shared/EbpsForm';
import {
	DashedMultiUnitLengthInput,
	DashedUnitInput,
	DashedAreaInputWithRelatedUnits,
	DashedLengthInputWithRelatedUnits,
} from '../../shared/EbpsUnitLabelValue';
import * as Yup from 'yup';
import { validateNullableNepaliDate, validateNumber } from '../../../utils/validationUtils';
import { FinishCertificateBlockFloors } from './certificateComponents/CertificateFloorInputs';
import { getConstructionTypeText } from '../../../utils/enums/constructionType';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { SectionHeader } from '../../uiComponents/Headers';
import { PrintParams } from '../../../utils/printUtils';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { BlockComponents } from '../../shared/formComponents/BlockFloorComponents';

const dpv = dosrocharanPrabidhikview.dosrocharanPrabidhikview_data;
// const formData = BuildingFinishCertificateData.formdata;
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];
const numData = ['roomCount', 'windowCount', 'doorCount', 'shutterCount'];

const numberDataSchema = numData.map(row => {
	return {
		[row]: validateNumber,
	};
});

const dosroprabidhikSchema = Yup.object().shape(
	Object.assign(
		{
			dDate: validateNullableNepaliDate,
			pramanpatra: validateNullableNepaliDate,
		},
		...numberDataSchema
	)
);
class DosrocharanPrabidhikViewComponent extends Component {
	constructor(props) {
		super(props);
		let initialValues = {};
		const prevData = this.props.prevData;

		const { permitData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;
		const dabedan = getJsonData(this.props.otherData.dabedan);
		const rajaswo = getJsonData(this.props.otherData.rajaswo);
		const mapTech = getJsonData(this.props.otherData.mapTech);
		const prabhidik = getJsonData(this.props.otherData.prabhidik);
		// const building_class = this.props.otherData.designApproval;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		initialValues.sanso = dpv.data40[0];
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		const initialValues1 = prepareMultiInitialValues(
			{
				obj: {
					floorUnit: floorArray.getFloorUnit(),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingArea', floorArray.getBottomFloor()),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', floorArray.getTopFloor()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{
				obj: dabedan,
				reqFields: ['dosrocharanDate'],
			},
			{
				obj: prabhidik,
				reqFields: [
					'constructionType',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'requiredDistance',
					'sadakAdhikarUnit',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'publicPropertyDistance',
					'publicPropertyUnit',
				],
			},
			{
				obj: rajaswo,
				reqFields: ['roadName'],
			},
			{
				obj: mapTech,
				reqFields: ['purposeOfConstruction', 'buildingHeight'],
			},
			// { obj: { floorNumber: floorArray.getTopFloor().nepaliCount }, reqFields: [] },
			{
				obj: getJsonData(prevData),
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.dDate)) {
			initialValues.dDate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues.pramanpatra)) {
			initialValues.pramanpatra = getCurrentDate(true);
		}

		initialValues1.constructionType = getConstructionTypeText(initialValues1.constructionType || permitData.constructionType)

		this.state = { initialValues, initialValues1, floorArray, formattedFloors, blocks: floorArray.getBlocks() };
	}
	render() {
		const { initialValues, initialValues1, floorArray, formattedFloors, blocks } = this.state;
		const { permitData: pdata, userData: udata, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div className="pilenthLevelTechViewWrap">
				<Formik
					initialValues={{ ...initialValues, ...initialValues1 }}
					// initialValues={{ initVal }}
					validationSchema={dosroprabidhikSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(api.DosrocharanPrabidhikView, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message ||
								// 	'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(
								// 			this.props.parentProps.location
								// 				.pathname
								// 		)
								// 	);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, setFieldValue, isSubmitting, handleChange, handleSubmit, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterSalutation
										lines={[
											dpv.data1,
											udata.organization.name,
											dpv.data2,
											`${udata.organization.address}, ${udata.organization.province}`,
										]}
									/>
									<FlexSingleRight>
										{dpv.data0}
										<DashedLangDateField
											name="dDate"
											inline={true}
											onChange={handleChange}
											value={values['dDate']}
											setFieldValue={setFieldValue}
										/>
									</FlexSingleRight>

									<SectionHeader>
										<h3 className="underline end-section">{dpv.data3}</h3>
									</SectionHeader>

									<div className="no-margin-field">
										&emsp;&emsp;{udata.organization.name}
										{dpv.data4}
										{pdata.oldWardNo}
										{dpv.data5}
										{pdata.nibedakName}
										{dpv.data6}
										{pdata.applicantName}
										{dpv.data7}
										{pdata.oldMunicipal}
										{dpv.data8}
										{udata.organization.name}
										{dpv.data4}
										{pdata.oldWardNo}
										{dpv.data9}
										{pdata.tol}
										{dpv.data10}
										{pdata.buildingJoinRoad}
										{dpv.data11}
										{pdata.kittaNo}
										{dpv.data12}
										{pdata.landAreaType}
										{dpv.data13}
										{pdata.constructionType && getConstructionTypeText(pdata.constructionType, true)}
										{dpv.data14}
										<DashedLangDateField
											name="pramanpatra"
											handleChange={handleChange}
											value={values.pramanpatra}
											error={errors.pramanpatra}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{dpv.data15}
									</div>
									<br />
									<div>
										<span>{dpv.data16}</span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{dpv.data17}
											<DashedLangInput
												name="pratibedanName"
												value={values.subName}
												setFieldValue={setFieldValue}
												error={errors.pratibedanName}
											/>
										</span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{dpv.data18}
											<DashedLangInput
												name="pratibedanPad"
												value={values.subDesignation}
												setFieldValue={setFieldValue}
												error={errors.pratibedanPad}
											/>
										</span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{dpv.data19}
											<span className="ui input dashedForm-control" />
										</span>
									</div>
									<br />
									<div>
										<p>{dpv.data20}</p>

										<div>
											{dpv.data21}
											<DashedLangInput
												name="constructionType"
												setFieldValue={setFieldValue}
												value={values.constructionType}
												className="dashedForm-control"
											/>
											{dpv.data22}
											<DashedLangInput
												name="roof"
												setFieldValue={setFieldValue}
												value={values.roof}
												className="dashedForm-control"
											/>
											{dpv.data23}
											<DashedLangInput
												name="purposeOfConstruction"
												setFieldValue={setFieldValue}
												value={values.purposeOfConstruction}
												className="dashedForm-control"
											/>
										</div>

										<div>
											{dpv.data24}
											<DashedLangInput
												name="roadName"
												setFieldValue={setFieldValue}
												value={values.roadName}
												className="dashedForm-control"
											/>
											<DashedMultiUnitLengthInput
												name="namedMapdanda"
												unitName="sadakAdhikarUnit"
												relatedFields={['requiredDistance', 'namedAdhikar']}
												label={dpv.data25}
											/>

											<DashedMultiUnitLengthInput
												name="namedAdhikar"
												unitName="sadakAdhikarUnit"
												relatedFields={['requiredDistance', 'namedMapdanda']}
												label={dpv.data26}
											/>
											<DashedMultiUnitLengthInput
												name="requiredDistance"
												unitName="sadakAdhikarUnit"
												relatedFields={['namedAdhikar', 'namedMapdanda']}
												label={dpv.data27}
											/>
										</div>
										<br />
										<div>
											{dpv.data_three}
											<div className="div-indent">
												<FinishCertificateBlockFloors
													formattedFloors={formattedFloors}
													floorArray={floorArray}
													showAreaColumn={false}
												/>
											</div>
										</div>
										<div>
											{dpv.data28_0}
											<div className="div-indent">
												<BlockComponents floorArray={floorArray} blocks={blocks}>
													{block => {
														const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
														const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
														const floorNumber = floorArray.getReducedFieldName('floorNumber', block);
														return (
															<>
																{floorArray.getLength() > 0 && (
																	<>
																		{dpv.data28_1}
																		<DashedLangInput
																			name={floorNumber}
																			setFieldValue={setFieldValue}
																			value={getIn(values, floorNumber)}
																		/>

																		{dpv.data29}
																		<DashedLengthInputWithRelatedUnits
																			name={buildingHeight}
																			unitName="floorUnit"
																			relatedFields={[
																				...floorArray.getAllFields(),
																				...floorArray.getAllReducedBlockFields(
																					['buildingArea', 'buildingHeight'],
																					buildingHeight
																				),
																			]}
																		/>

																		{dpv.data30}
																		<DashedAreaInputWithRelatedUnits
																			name={buildingArea}
																			unitName="floorUnit"
																			squareOptions={squareUnitOptions}
																			relatedFields={[
																				...floorArray.getAllFields(),
																				...floorArray.getAllReducedBlockFields(
																					['buildingArea', 'buildingHeight'],
																					buildingArea
																				),
																			]}
																		/>
																	</>
																)}
															</>
														);
													}}
												</BlockComponents>
											</div>
										</div>

										<div>
											{dpv.data32}
											<DashedLangInput
												name="roomCount"
												setFieldValue={setFieldValue}
												value={values.roomCount}
												error={errors.roomCount}
											/>
											{dpv.data33}
											<DashedLangInput
												name="windowCount"
												setFieldValue={setFieldValue}
												value={values.windowCount}
												error={errors.windowCount}
											/>
											{dpv.data34}
											<DashedLangInput
												name="doorCount"
												setFieldValue={setFieldValue}
												value={values.doorCount}
												error={errors.doorCount}
											/>
											{dpv.data35}
											<DashedLangInput
												name="shutterCount"
												setFieldValue={setFieldValue}
												value={values.shutterCount}
												error={errors.shutterCount}
											/>
										</div>
										<div>
											<DashedUnitInput name="isHighTensionLineDistance" label={dpv.data36} unitName="highTensionLineUnit" />
										</div>
										<div>
											<DashedUnitInput name="publicPropertyDistance" label={dpv.data37} unitName="publicPropertyUnit" />
										</div>
										<div>
											{dpv.data38}
											{dpv.data40.map((name, index) => (
												<div key={index} className="ui radio checkbox prabidhik">
													<Field
														type="radio"
														name="sanso"
														id={index}
														defaultChecked={values.sanso === name}
														value={name}
														onClick={handleChange}
													/>
													<label>{name}</label>
												</div>
											))}
											{values.sanso === dpv.data40[1] && (
												<EbpsTextareaField
													placeholder={dpv.plor_else_placeholder}
													name="anusarAdditional"
													setFieldValue={setFieldValue}
													value={values.anusarAdditional}
													error={errors.anusarAdditional}
												/>
											)}
										</div>
									</div>
									<br />
									<p style={{ textAlign: 'center' }}>
										<u>{dpv.data39}</u>
									</p>
									<EbpsTextArea
										placeholder="निवेदन"
										name="karyasampannaSuggestion"
										setFieldValue={setFieldValue}
										onChange={handleChange}
										value={values.karyasampannaSuggestion}
									/>
								</div>
								{/*  endoffulldiv */}
							</div>

							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				/>
			</div>
		);
	}
}

const DosrocharanPrabidhikView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.DosrocharanPrabidhikView,
				objName: 'dosroPrabidhik',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.DosrocharanAbedan,
				objName: 'dabedan',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		useInnerRef={true}
		render={props => <DosrocharanPrabidhikViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanPrabidhikView;
