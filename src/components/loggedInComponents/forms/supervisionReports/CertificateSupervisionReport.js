import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { requiredDataRCCFrameStructure, requiredDataLoadBearingStructureBuilding } from '../../../../utils/data/SuperVisionReportData';
import { Formik, getIn } from 'formik';
import { Form, Dropdown, List } from 'semantic-ui-react';
import { prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { RadioInput } from '../../../shared/formComponents/RadioInput';

import * as Yup from 'yup';
import { validateNullableNumber } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SignatureImage } from '../formComponents/SignatureImage';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { getUserRole, isEmpty } from '../../../../utils/functionUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { PrintParams } from '../../../../utils/printUtils';
const dataFS = requiredDataRCCFrameStructure;
const dataLBS = requiredDataLoadBearingStructureBuilding;
const mansoryType = ['stone', 'brick'];
const options = [
	{
		value: 'frameStructure',
		text: 'RCC Frame Structure Building',
	},
	{
		value: 'loadBearing',
		text: 'Load Bearing Structure Building',
	},
];

const initialValuesLBS = {
	widthOfStairCaseLBS: '',
	widthOfLandingLBS: '',
	numberOfFlightLBS: '',
	widthOfTreadAndHeightOfRiserLBS: '',
	sizeAndSpacingOfMainBarLBS: '',
	sizeAndSpacingOfDistributionBarLBS: '',
	sizeAndNumberOfRebarInZbeamLBS: '',
	sizeAndNumberOfRebarInShortColumnLBS: '',
	thicknessAndTypesOfMasonryWallLBS: '',
	thicknessOfSillBandAndSizeOfMainBarLBS: '',
	thicknessOfLintelBandAndSizeOfMainBarLBS: '',
	sizeAndSpacingOfStirrupsInStillBandLBS: '',
	gradeOfConcreteLBS: '',
	sizeOfBandLBS: '',
	sizeAndNoOfRebarInBandLBS: '',
	sizeAndSpacingOfStirrupsLBS: '',
	lapLengthAndLocationLBS: '',
	anchorageLengthInBeamColumnJunctionLBS: '',
	thicknessOfSlabLBS: '',
	sizeAndSpacingOfBottomRebarLBS: '',
	sizeAndSpacingOfTopRebarLBS: '',
	sizeAndSpacingOfChairLBS: '',
	typesOfRoofLBS: '',
	materialUsedInRoofLBS: '',
	geometryOfRoofTrussLBS: '',
	sizeAndGradeOfSteelUsedLBS: '',
	ifAnyHeader5LBS: '',
	clientNameLBS: '',
	licenseHolderMasonNameLBS: '',
	supervisorEngineersNameLBS: '',
};
const validationLBS = Yup.object().shape({
	widthOfStairCaseLBS: validateNullableNumber,
	widthOfLandingLBS: validateNullableNumber,
	numberOfFlightLBS: validateNullableNumber,
	widthOfTreadAndHeightOfRiserLBS: validateNullableNumber,
	sizeAndSpacingOfMainBarLBS: validateNullableNumber,
	sizeAndSpacingOfDistributionBarLBS: validateNullableNumber,
	sizeAndNumberOfRebarInZbeamLBS: validateNullableNumber,
	sizeAndNumberOfRebarInShortColumnLBS: validateNullableNumber,
	thicknessAndTypesOfMasonryWallLBS: validateNullableNumber,
	thicknessOfSillBandAndSizeOfMainBarLBS: validateNullableNumber,
	thicknessOfLintelBandAndSizeOfMainBarLBS: validateNullableNumber,
	sizeAndSpacingOfStirrupsInStillBandLBS: validateNullableNumber,
	gradeOfConcreteLBS: validateNullableNumber,
	sizeOfBandLBS: validateNullableNumber,
	sizeAndNoOfRebarInBandLBS: validateNullableNumber,
	sizeAndSpacingOfStirrupsLBS: validateNullableNumber,
	lapLengthAndLocationLBS: validateNullableNumber,
	anchorageLengthInBeamColumnJunctionLBS: validateNullableNumber,
	thicknessOfSlabLBS: validateNullableNumber,
	sizeAndSpacingOfBottomRebarLBS: validateNullableNumber,
	sizeAndSpacingOfTopRebarLBS: validateNullableNumber,
	sizeAndSpacingOfChairLBS: validateNullableNumber,
	typesOfRoofLBS: validateNullableNumber,
	materialUsedInRoofLBS: validateNullableNumber,
	geometryOfRoofTrussLBS: validateNullableNumber,
	sizeAndGradeOfSteelUsedLBS: validateNullableNumber,
	ifAnyHeader5LBS: validateNullableNumber,
	clientNameLBS: '',
	licenseHolderMasonNameLBS: '',
	supervisorEngineersNameLBS: '',
});
const initialValues = {
	buildingName: 'frameStructure',
	levelOfCompletion: '',
	sizeOfWallFootingFS: '',
	mansoryTypeFS: '',
	sizeOfPlinthBeamFS: '',
	siznoAndSizeOfRebarUsedInThePlinthBeamFSeOfPlinthBeamFS: '',
	sizeAndSpacingOfStirrupsFS: '',
	concreteAndRebarGradeFS: '',
	widthOfStairCaseFS: '',
	widthOfLandingFS: '',
	numberOfFlightFS: '',
	widthOfTreadAndHeightOfRiserFS: '',
	sizeAndSpacingOfMainBarFS: '',
	sizeAndSpacingOfDistributionBarFS: '',
	sizeAndNumberOfRebarinZbeamFS: '',
	sizeAndNumberOfRebarinShortColumnFS: '',
	thicknessOfSillBandAndSizeOfmainBarFS: '',
	thicknessOfLintelbandAndSizeOfMainBarFS: '',
	sizeAndSpacingOfStirrupsInSillBandFS: '',
	gradeOfConcreteFS: '',
	sizeOfBeamFS: '',
	sizeAndNoOfRebarInBeamFS: '',
	sizeAndSpacingOfStirrupsHeader5FS: '',
	lapLengthAndLocationFS: '',
	anchorageLengthInBeamColumnJunctionFS: '',
	thicknessOfSlabFS: '',
	sizeAndSpacingOfBottomRebarFS: '',
	sizeAndSpacingOfTopRebarFS: '',
	sizeAndSpacingOfChairFS: '',
	clientnameFS: '',
	licenseHolderMasonNameFS: '',
	supervisorEngineerNameFS: '',
	ifAnyFS: '',
};
const validation = Yup.object().shape({
	sizeOfWallFootingFS: validateNullableNumber,
	mansoryTypeFS: validateNullableNumber,
	sizeOfPlinthBeamFS: validateNullableNumber,
	siznoAndSizeOfRebarUsedInThePlinthBeamFSeOfPlinthBeamFS: validateNullableNumber,
	sizeAndSpacingOfStirrupsFS: validateNullableNumber,
	concreteAndRebarGradeFS: validateNullableNumber,
	widthOfStairCaseFS: validateNullableNumber,
	widthOfLandingFS: validateNullableNumber,
	numberOfFlightFS: validateNullableNumber,
	widthOfTreadAndHeightOfRiserFS: validateNullableNumber,
	sizeAndSpacingOfMainBarFS: validateNullableNumber,
	sizeAndSpacingOfDistributionBarFS: validateNullableNumber,
	sizeAndNumberOfRebarinZbeamFS: validateNullableNumber,
	sizeAndNumberOfRebarinShortColumnFS: validateNullableNumber,
	thicknessOfSillBandAndSizeOfmainBarFS: validateNullableNumber,
	thicknessOfLintelbandAndSizeOfMainBarFS: validateNullableNumber,
	sizeAndSpacingOfStirrupsInSillBandFS: validateNullableNumber,
	gradeOfConcreteFS: validateNullableNumber,
	sizeOfBeamFS: validateNullableNumber,
	sizeAndNoOfRebarInBeamFS: validateNullableNumber,
	sizeAndSpacingOfStirrupsHeader5FS: validateNullableNumber,
	lapLengthAndLocationFS: validateNullableNumber,
	anchorageLengthInBeamColumnJunctionFS: validateNullableNumber,
	thicknessOfSlabFS: validateNullableNumber,
	sizeAndSpacingOfBottomRebarFS: validateNullableNumber,
	sizeAndSpacingOfTopRebarFS: validateNullableNumber,
	sizeAndSpacingOfChairFS: validateNullableNumber,
});
class SuperVisionReportComponent extends Component {
	constructor(props) {
		super(props);
		let initialVal = {};
		const { prevData, permitData, otherData, userData, hasDesignerChanged } = this.props;
		const anuGha = checkError(otherData.anuGha);

		let mapSideObj = {};
		if (getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR || hasDesignerChanged) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					supervisorEngineerName: userData.info.userName,
					supervisorEngineerSignature: userData.info.signature,
				};
			} else {
				mapSideObj = {
					supervisorEngineerName: '',
					supervisorEngineerSignature: '',
				};
			}
		} else {
			if (!isEmpty(anuGha)) {
				if (!isEmpty(anuGha.dName && anuGha.dDesignation && anuGha.dDate)) {
					mapSideObj = {
						supervisorEngineerName: anuGha.dName,
						supervisorEngineerSignature: anuGha.enterBy.signature,
					};
				} else
					mapSideObj = {
						supervisorEngineerName: anuGha.enterBy.userName,
						supervisorEngineerSignature: anuGha.enterBy.signature,
					};
			}
		}
		initialVal = prepareMultiInitialValues(
			{
				obj: initialValues,
				reqFields: [],
			},
			{
				obj: initialValuesLBS,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'applicantName'],
			},
			{
				obj: {
					mistiriName: !isEmpty(anuGha) ? anuGha.mistiriName : '',
				},
				reqFields: [],
			},
			{
				obj: mapSideObj,
				reqFields: [],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = {
			initialVal,
		};
	}
	render() {
		const { prevData, isSaveDisabled, hasDeletePermission, hasSavePermission, formUrl, staticFiles } = this.props;
		const { initialVal } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialVal}
					validationSchema={(validation, validationLBS)}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.CertificateSupervisionReport, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ values, errors, setFieldValue, handleSubmit, validateForm }) => (
						<Form>
							<Dropdown
								name="buildingName"
								onChange={(e, { value }) => setFieldValue('buildingName', value)}
								value={getIn(values, 'buildingName')}
								options={options}
							/>
							<div ref={this.props.setRef}>
								{values.buildingName === 'frameStructure' ? (
									<RccFrameStructureBuilding value={values} error={errors} setField={setFieldValue} />
								) : (
									<LoadBearingStructureBuilding value={values} error={errors} setField={setFieldValue} />
								)}
								<List>
									<List.Item>{dataFS.clientFS}</List.Item>
									<List.Item>
										{dataFS.nameFS}
										<DashedLangInput
											value={values.applicantName}
											error={errors.applicantName}
											setFieldValue={setFieldValue}
											name="applicantName"
										/>
									</List.Item>
									<List.Item>
										<SignatureImage label={dataFS.signatureFS} showSignature={true} value={staticFiles.ghardhaniSignature} />
									</List.Item>
									<br />
									<List.Item>{dataFS.licenseHolderMasonFS}</List.Item>
									<List.Item>
										{dataFS.nameFS}
										<DashedLangInput
											value={values.mistiriName}
											error={errors.mistiriName}
											setFieldValue={setFieldValue}
											name="mistiriName"
										/>
									</List.Item>
									<List.Item>
										<SignatureImage label={dataFS.signatureFS} showSignature={true} value={staticFiles.dakarmiSignature} />
									</List.Item>
									<br />
									<List.Item>{dataFS.supervisorEngineerFS}</List.Item>
									<List.Item>
										{dataFS.nameFS}
										<DashedNormalInputIm name="supervisorEngineerName" />
									</List.Item>
									<List.Item>
										<SignatureImage label={dataFS.signatureFS} showSignature={true} value={values.supervisorEngineerSignature} />
									</List.Item>
								</List>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const RccFrameStructureBuilding = (props) => {
	const { value, error, setField } = props;
	return (
		<div>
			<SectionHeader>
				<h2>{dataFS.superVisionReportFS}</h2>
				<h2>{dataFS.onFS}</h2>
				<h2>{dataFS.rccFrameSructureBuildingFS}</h2>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.typeOfBuildingFS}
					<DashedLangInput
						name="purposeOfConstruction"
						value={value.purposeOfConstruction}
						error={error.purposeOfConstruction}
						setFieldValue={setField}
					/>
				</List.Item>
				<List.Item>
					{dataFS.levelOfCompletionFS}{' '}
					<DashedLangInput
						name="levelOfCompletion"
						value={value.levelOfCompletion}
						error={error.levelOfCompletion}
						setFieldValue={setField}
					/>
				</List.Item>
			</List>
			<br />
			{/* header1 */}
			<SectionHeader>
				<h3 className="left-align">{dataFS.wallFootingFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.sizeOfWallFootingFS}
					<DashedNormalInputIm name="sizeOfWallFootingFS" />
				</List.Item>
				<List.Item>
					{dataFS.mansoryTypeFS}
					{mansoryType.map((type, i) => (
						<RadioInput key={i} name="mansoryTypeFS" option={type} space="true" />
					))}
				</List.Item>
			</List>
			<br />
			{/* header2 */}
			<SectionHeader>
				<h3 className="left-align">{dataFS.plinthBeamFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.sizeOfPlinthBeamFS}
					<DashedNormalInputIm name="sizeOfPlinthBeamFS" />
				</List.Item>
				<List.Item>
					{dataFS.noAndSizeOfRebarUsedInThePlinthBeamFS}
					<DashedNormalInputIm name="siznoAndSizeOfRebarUsedInThePlinthBeamFSeOfPlinthBeamFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfStirrupsFS}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsFS" />
				</List.Item>
				<List.Item>
					{dataFS.concreteAndRebarGradeFS}
					<DashedNormalInputIm name="concreteAndRebarGradeFS" />
				</List.Item>
			</List>
			{/* header 3 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{dataFS.starircaseFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.widthOfStairCaseFS}
					<DashedNormalInputIm name="widthOfStairCaseFS" />
				</List.Item>
				<List.Item>
					{dataFS.widthOfLandingFS}
					<DashedNormalInputIm name="widthOfLandingFS" />
				</List.Item>
				<List.Item>
					{dataFS.numberOfFlightFS}
					<DashedNormalInputIm name="numberOfFlightFS" />
				</List.Item>
				<List.Item>
					{dataFS.widthOfTreadAndHeightOfRiserFS}
					<DashedNormalInputIm name="widthOfTreadAndHeightOfRiserFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfMainBarFS}
					<DashedNormalInputIm name="sizeAndSpacingOfMainBarFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfDistributionBarFS}
					<DashedNormalInputIm name="sizeAndSpacingOfDistributionBarFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndNumberOfRebarinZbeamFS}
					<DashedNormalInputIm name="sizeAndNumberOfRebarinZbeamFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndNumberOfRebarinShortColumnFS}
					<DashedNormalInputIm name="sizeAndNumberOfRebarinShortColumnFS" />
				</List.Item>
			</List>
			{/* header 4 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{dataFS.wallInSuperStructureFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.thicknessOfSillBandAndSizeOfmainBarFS}
					<DashedNormalInputIm name="thicknessOfSillBandAndSizeOfmainBarFS" />
				</List.Item>
				<List.Item>
					{dataFS.thicknessOfLintelbandAndSizeOfMainBarFS}
					<DashedNormalInputIm name="thicknessOfLintelbandAndSizeOfMainBarFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfStirrupsInSillBandFS}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsInSillBandFS" />
				</List.Item>
				<List.Item>
					{dataFS.gradeOfConcreteFS}
					<DashedNormalInputIm name="gradeOfConcreteFS" />
				</List.Item>
			</List>
			{/* header 5 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{dataFS.floorBeamFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.sizeOfBeamFS}
					<DashedNormalInputIm name="sizeOfBeamFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndNoOfRebarInBeamFS}
					<DashedNormalInputIm name="sizeAndNoOfRebarInBeamFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfStirrupsHeader5FS}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsHeader5FS" />
				</List.Item>
				<List.Item>
					{dataFS.lapLengthAndLocationFS}
					<DashedNormalInputIm name="lapLengthAndLocationFS" />
				</List.Item>
				<List.Item>
					{dataFS.anchorageLengthInBeamColumnJunctionFS}
					<DashedNormalInputIm name="anchorageLengthInBeamColumnJunctionFS" />
				</List.Item>
			</List>
			{/* header 6 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{dataFS.slabFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataFS.thicknessOfSlabFS}
					<DashedNormalInputIm name="thicknessOfSlabFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfBottomRebarFS}
					<DashedNormalInputIm name="sizeAndSpacingOfBottomRebarFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfTopRebarFS}
					<DashedNormalInputIm name="sizeAndSpacingOfTopRebarFS" />
				</List.Item>
				<List.Item>
					{dataFS.sizeAndSpacingOfChairFS}
					<DashedNormalInputIm name="sizeAndSpacingOfChairFS" />
				</List.Item>
			</List>
			{/* header 7 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{dataFS.photographsFS}</h3>
			</SectionHeader>
			<List>
				<List.Item>{dataFS.foundationTrenchFS}</List.Item>
				<List.Item>{dataFS.reinforcementOfColumnAndFootingsFS}</List.Item>
				<List.Item>{dataFS.strapBeamFS}</List.Item>
				<List.Item>{dataFS.connectionDetailsOfColumnFS}</List.Item>
				<List.Item>{dataFS.connectionDetailsOfFootingFS}</List.Item>
				<List.Item>{dataFS.sillAndLintelBandsFS}</List.Item>
				<List.Item>{dataFS.beamColumnJunctionFS}</List.Item>
				<List.Item>{dataFS.slabReinforcementFS}</List.Item>
			</List>
			{/* header 8 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">
					{dataFS.ifAnyFS}

					<DashedNormalInputIm name="ifAnyFS" />
				</h3>
			</SectionHeader>
		</div>
	);
};

const LoadBearingStructureBuilding = (props) => {
	// const { value, error, setField } = props;
	return (
		<div>
			<SectionHeader>
				<h2>{dataLBS.superVisionReportLBS}</h2>
				<h2>{dataLBS.onLBS}</h2>
				<h2>{dataLBS.loadBearingStructureBuildingLBS}</h2>
			</SectionHeader>
			<br />
			{/* header 1 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.staircaseLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataLBS.widthOfStairCaseLBS}
					<DashedNormalInputIm name="widthOfStairCaseLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.widthOfLandingLBS}
					<DashedNormalInputIm name="widthOfLandingLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.numberOfFlightLBS}
					<DashedNormalInputIm name="numberOfFlightLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.widthOfTreadAndHeightOfRiserLBS}
					<DashedNormalInputIm name="widthOfTreadAndHeightOfRiserLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfMainBarLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfMainBarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfDistributionBarLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfDistributionBarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndNumberOfRebarInZbeamLBS}
					<DashedNormalInputIm name="sizeAndNumberOfRebarInZbeamLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndNumberOfRebarInShortColumnLBS}
					<DashedNormalInputIm name="sizeAndNumberOfRebarInShortColumnLBS" />
				</List.Item>
			</List>
			<br />
			{/* header 2 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.wallInSuperStructureLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataLBS.thicknessAndTypesOfMasonryWallLBS}
					<DashedNormalInputIm name="thicknessAndTypesOfMasonryWallLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.thicknessOfSillBandAndSizeOfMainBarLBS}
					<DashedNormalInputIm name="thicknessOfSillBandAndSizeOfMainBarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.thicknessOfLintelBandAndSizeOfMainBarLBS}
					<DashedNormalInputIm name="thicknessOfLintelBandAndSizeOfMainBarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfStirrupsInStillBandLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsInStillBandLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.gradeOfConcreteLBS}
					<DashedNormalInputIm name="gradeOfConcreteLBS" />
				</List.Item>
			</List>
			<br />
			{/* header 3 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.floorBandLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataLBS.sizeOfBandLBS}
					<DashedNormalInputIm name="sizeOfBandLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndNoOfRebarInBandLBS}
					<DashedNormalInputIm name="sizeAndNoOfRebarInBandLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfStirrupsLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.lapLengthAndLocationLBS}
					<DashedNormalInputIm name="lapLengthAndLocationLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.anchorageLengthInBeamColumnJunctionLBS}
					<DashedNormalInputIm name="anchorageLengthInBeamColumnJunctionLBS" />
				</List.Item>
			</List>
			<br />
			{/* header 4 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.slabLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataLBS.thicknessOfSlabLBS}
					<DashedNormalInputIm name="thicknessOfSlabLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfBottomRebarLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfBottomRebarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfTopRebarLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfTopRebarLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndSpacingOfChairLBS}
					<DashedNormalInputIm name="sizeAndSpacingOfChairLBS" />
				</List.Item>
			</List>
			<br />
			{/* header 5 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.roofLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{dataLBS.typesOfRoofLBS}
					<DashedNormalInputIm name="typesOfRoofLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.materialUsedInRoofLBS}
					<DashedNormalInputIm name="materialUsedInRoofLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.geometryOfRoofTrussLBS}
					<DashedNormalInputIm name="geometryOfRoofTrussLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.sizeAndGradeOfSteelUsedLBS}
					<DashedNormalInputIm name="sizeAndGradeOfSteelUsedLBS" />
				</List.Item>
				<List.Item>
					{dataLBS.ifAnyHeader5LBS}
					<DashedNormalInputIm name="ifAnyHeader5LBS" />
				</List.Item>
			</List>
			<br />
			{/* header 6 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.photographsLBS}</h3>
			</SectionHeader>
			<List>
				<List.Item>{dataLBS.foundationTrenchLBS}</List.Item>
				<List.Item>{dataLBS.reinforcementOfFootingsLBS}</List.Item>
				<List.Item>{dataLBS.strapBeamLB}</List.Item>
				<List.Item>{dataLBS.connectionDetailsOfFootingLBS}</List.Item>
				<List.Item>{dataLBS.connectionDetailsOfFootingPlinthLBS}</List.Item>
				<List.Item>{dataLBS.sillAndLintelBandsLBS}</List.Item>
				<List.Item>{dataLBS.cornerAndTBandLBS}</List.Item>
				<List.Item>{dataLBS.slabReinforcementLBS}</List.Item>
			</List>
			<br />
			{/* header 7 */}
			<SectionHeader>
				<h3 className="left-align">{dataLBS.ifAnyheader7}</h3>
			</SectionHeader>
		</div>
	);
};
const SuperVisionReport = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.CertificateSupervisionReport).setForm().getParams(), new ApiParam(api.anusuchiGha, 'anuGha')]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		fetchFiles={true}
		render={(props) => <SuperVisionReportComponent {...props} parentProps={parentProps} />}
	/>
);
export default SuperVisionReport;
