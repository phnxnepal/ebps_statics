import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik, getIn } from 'formik';
import { Form, List } from 'semantic-ui-react';
import { Dropdown } from 'semantic-ui-react';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { prepareMultiInitialValues, handleSuccess, checkError, getJsonData } from '../../../../utils/dataUtils';
import { requiredDataFrametructure, requiredDataLoadBearing } from '../../../../utils/data/SuperStructureSuperVisionData';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import * as Yup from 'yup';
import { validateNullableNumber } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SignatureImage } from '../formComponents/SignatureImage';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserRole, isEmpty } from '../../../../utils/functionUtils';
import { PrintParams } from '../../../../utils/printUtils';
const data = requiredDataFrametructure;
const data2 = requiredDataLoadBearing;
const radioOptions = ['Hand ', 'Machine(vibrator) '];
const foundationFormwork = ['Timber', 'Plywood', 'Steel'];
const soling = ['stone', 'brick'];
const mansoryWork = ['Stone Mansory', 'Brick Mansory'];
const options = [
	{
		value: 'frameStructure',
		text: 'Frame Structure Building',
	},
	{
		value: 'loadBearing',
		text: 'Load Bearing Structure Building',
	},
];
const initialVal = {
	buildingName: 'loadBearing',
	text: 'Load Bearing Structure Building',
	levelOfCompletion: '',
	isolatedFooting: '',
	combinedFooting: '',
	eccentricFooting: '',
	numberOfColumn: '',
	sizeIsolatedFooting: '',
	sizeCombineFooting: '',
	sizeEccentricFooting: '',
	typeOfSoling: '',
	thicknessAndGradeOfPCC: '',
	gradeOfConcrete: '',
	sizeOfColumn: '',
	noAndSizeOfRebarUsedInTheColumn: '',
	sizeAndSpacingOfStirrupsheader2: '',
	concreteAndRebarGradeheader2: '',
	sizeOfFoundationBeam: '',
	noAndSizeOfRebarUsedInTheFoundationalBeam: '',
	sizeAndSpacingOfStirrupsheader3: '',
	concreteAndRebarGradeheader3: '',
	sizeOfStrapBeam: '',
	sizeAndNumberOfRebar: '',
	sizeAndSpacingOfStirrupsheader4: '',
	concreteAndRebarGradeheader4: '',
	ifAny: '',
	client: '',
	licenseHolderMason: '',
	superVisorEngineer: '',
};
const initialValLoadBearing = {
	numberOfShortWallFooting: '',
	numberOfLongWallFooting: '',
	numberOfEccentricWallFooting: '',
	lengthaAndThicknessOfShortWallFooting: '',
	lengthaAndThicknessOfLongWallFooting: '',
	lengthAndThicknessOfEccentricFooting: '',
	thicknessAndGradeOfPCCform2: '',
	gradeOfConcreteAndRebarForFoundationAndPlinthBand: '',

	sizeOfFoundationBand: '',
	noAndSizeOfRebarUsedInTheFoundationBand: '',
	sizeAndSpacingOfStirrupsheader2: '',
	concreteAndRebarGradeheader2: '',

	sizeOfStrapBeam: '',
	sizeAndNumberOfMainRebar: '',
	sizeAndSpacingOfStirrupsheader3Form2: '',
	concreteAndRebarGradeheader3Form2: '',

	sizeOfPlinthBand: '',
	noAndSizeOfRebarUSedInThePlinthBand: '',
	sizeAndSpacingOfStirrupsHeader4Form2: '',
	concreteAndRebarGradeHeader4Form2: '',

	ifAnyForm2: '',
	clientForm2: '',
	licenseHolderMasonForm2: '',
	superVisorEngineerForm2: '',
};
const validation = Yup.object().shape({
	//HEADER 1
	isolatedFooting: validateNullableNumber,
	combinedFooting: validateNullableNumber,
	eccentricFooting: validateNullableNumber,
	numberOfColumn: validateNullableNumber,
	sizeIsolatedFooting: validateNullableNumber,
	sizeCombineFooting: validateNullableNumber,
	sizeEccentricFooting: validateNullableNumber,
	thicknessAndGradeOfPCC: validateNullableNumber,
	gradeOfConcrete: validateNullableNumber,

	//header 2
	sizeOfColumn: validateNullableNumber,
	noAndSizeOfRebarUsedInTheColumn: validateNullableNumber,
	sizeAndSpacingOfStirrupsheader2: validateNullableNumber,
	concreteAndRebarGradeheader2: validateNullableNumber,

	//header3
	sizeOfFoundationBeam: validateNullableNumber,
	noAndSizeOfRebarUsedInTheFoundationalBeam: validateNullableNumber,
	sizeAndSpacingOfStirrupsheader3: validateNullableNumber,
	concreteAndRebarGradeheader3: validateNullableNumber,

	//header 4
	sizeOfStrapBeam: validateNullableNumber,
	sizeAndNumberOfRebar: validateNullableNumber,
	sizeAndSpacingOfStirrupsheader4: validateNullableNumber,
	concreteAndRebarGradeheader4: validateNullableNumber,

	//header 5
	ifAny: '',
	client: '',
	licenseHolderMason: '',
	superVisorEngineer: '',
});
const validatonLoadBearingForm = Yup.object().shape({
	//header1
	numberOfShortWallFooting: validateNullableNumber,
	numberOfLongWallFooting: validateNullableNumber,
	numberOfEccentricWallFooting: validateNullableNumber,
	lengthaAndThicknessOfShortWallFooting: validateNullableNumber,
	lengthaAndThicknessOfLongWallFooting: validateNullableNumber,
	lengthAndThicknessOfEccentricFooting: validateNullableNumber,
	thicknessAndGradeOfPCCform2: validateNullableNumber,
	gradeOfConcreteAndRebarForFoundationAndPlinthBand: validateNullableNumber,
	//header2
	sizeOfFoundationBand: validateNullableNumber,
	noAndSizeOfRebarUsedInTheFoundationBand: validateNullableNumber,
	sizeAndSpacingOfStirrupsheader2: validateNullableNumber,
	concreteAndRebarGradeheader2: validateNullableNumber,
	//header3
	sizeOfStrapBeam: validateNullableNumber,
	sizeAndNumberOfMainRebar: validateNullableNumber,
	sizeAndSpacingOfStirrupsheader3Form2: validateNullableNumber,
	concreteAndRebarGradeheader3Form2: validateNullableNumber,
	//header4
	sizeOfPlinthBand: validateNullableNumber,
	noAndSizeOfRebarUSedInThePlinthBand: validateNullableNumber,
	sizeAndSpacingOfStirrupsHeader4Form2: validateNullableNumber,
	concreteAndRebarGradeHeader4Form2: validateNullableNumber,
	//header5
	ifAnyForm2: '',
	clientForm2: '',
	licenseHolderMasonForm2: '',
	superVisorEngineerForm2: '',
});
class SuperStructureSupervisionReportComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};

		const { prevData, permitData, otherData, userData, hasDesignerChanged } = this.props;
		const anuGha = checkError(otherData.anuGha);

		let mapSideObj = {};
		if (getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR || hasDesignerChanged) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					supervisorEngineerName: userData.info.userName,
					supervisorEngineerSignature: userData.info.signature,
				};
			} else {
				mapSideObj = {
					supervisorEngineerName: '',
					supervisorEngineerSignature: '',
				};
			}
		} else {
			if (!isEmpty(anuGha)) {
				if (!isEmpty(anuGha.dName && anuGha.dDesignation && anuGha.dDate)) {
					mapSideObj = {
						supervisorEngineerName: anuGha.dName,
						supervisorEngineerSignature: anuGha.enterBy.signature,
					};
				} else
					mapSideObj = {
						supervisorEngineerName: anuGha.enterBy.userName,
						supervisorEngineerSignature: anuGha.enterBy.signature,
					};
			}
		}
		initVal = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: initialValLoadBearing,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'applicantName'],
			},
			{
				obj: {
					mistiriName: !isEmpty(anuGha) ? anuGha.mistiriName : '',
				},
				reqFields: [],
			},
			{
				obj: mapSideObj,
				reqFields: [],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}

	render() {
		const { initVal } = this.state;
		const { isSaveDisabled, prevData, formUrl, hasSavePermission, errors: propError, hasDeletePermission, staticFiles } = this.props;
		return (
			// <Dropdown
			// 	name={'buildingStructure'}
			// 	onChange={(e, { value }) => this.setState({ buildingStructure: value })}
			// 	value={this.state.buildingStructure}
			// 	options={options}
			// />
			<div ref={this.props.setRef}>
				{propError && <ErrorDisplay message={propError.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={(validation, validatonLoadBearingForm)}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.SuperStructureSupervisionReport, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ values, errors, setFieldValue, handleSubmit, validateForm }) => (
						<Form>
							<Dropdown
								name="buildingName"
								onChange={(e, { value }) => setFieldValue('buildingName', value)}
								value={getIn(values, 'buildingName')}
								options={options}
							/>
							<div ref={this.props.setRef}>
								{values.buildingName === 'frameStructure' ? (
									<FrameStructure value={values} error={errors} setField={setFieldValue} />
								) : (
									<LoadBearing value={values} error={errors} setField={setFieldValue} />
								)}
								<List>
									<List.Item>{data.client}</List.Item>
									<List.Item>
										{data.name}
										<DashedLangInput
											value={values.applicantName}
											error={errors.applicantName}
											setFieldValue={setFieldValue}
											name="applicantName"
										/>
									</List.Item>
									<List.Item>
										<SignatureImage label={data.signature} showSignature={true} value={staticFiles.ghardhaniSignature} />
									</List.Item>
									<br />
									<List.Item>{data.licenseHolderMason}</List.Item>
									<List.Item>
										{data.name}
										<DashedLangInput
											value={values.mistiriName}
											error={errors.mistiriName}
											setFieldValue={setFieldValue}
											name="mistiriName"
										/>
									</List.Item>
									<List.Item>
										<SignatureImage label={data.signature} showSignature={true} value={staticFiles.dakarmiSignature} />
									</List.Item>
									<br />
									<List.Item>{data.superVisorEngineer}</List.Item>
									<List.Item>
										{data.name}
										<DashedNormalInputIm name="supervisorEngineerName" />
									</List.Item>
									<List.Item>
										<SignatureImage label={data.signature} showSignature={true} value={values.supervisorEngineerSignature} />
									</List.Item>
								</List>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const FrameStructure = (props) => {
	const { value, error, setField } = props;
	return (
		<div>
			<SectionHeader>
				<h2>{data.superVision}</h2>
				<h2>{data.on}</h2>
				<h2>{data.frame}</h2>
			</SectionHeader>
			<br />
			<br />
			<List ordered>
				<List.Item>
					{data.typeOfBuilding}{' '}
					<DashedLangInput
						name="purposeOfConstruction"
						value={value.purposeOfConstruction}
						error={error.purposeOfConstruction}
						setFieldValue={setField}
					/>
				</List.Item>
				<List.Item>
					{data.levelOfCompletion}{' '}
					<DashedLangInput
						name="levelOfCompletion"
						value={value.levelOfCompletion}
						error={error.levelOfCompletion}
						setFieldValue={setField}
					/>
				</List.Item>
			</List>
			<br />
			<SectionHeader>
				<h3 className="left-align">{data.constructionWorkDetails}</h3>
				{/* header 1 */}
				<h3 className="left-align">{data.fundamentalWork}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data.isolatedFooting}
					<DashedNormalInputIm name="isolatedFooting" />
				</List.Item>
				<List.Item>
					{data.combinedFooting}
					<DashedNormalInputIm name="combinedFooting" />
				</List.Item>
				<List.Item>
					{data.eccentricFooting}
					<DashedNormalInputIm name="eccentricFooting" />
				</List.Item>

				<List.Item>
					{data.numberOfColumn}
					<DashedNormalInputIm name="numberOfColumn" />
				</List.Item>
				<List.Item>
					{data.sizeIsolatedFooting}
					<DashedNormalInputIm name="sizeIsolatedFooting" />
				</List.Item>
				<List.Item>
					{data.sizeCombineFooting}
					<DashedNormalInputIm name="sizeCombineFooting" />
				</List.Item>

				<List.Item>
					{data.sizeEccentricFooting}
					<DashedNormalInputIm name="sizeEccentricFooting" />
				</List.Item>
				<List.Item>
					{data.typeOfSoling}
					{soling.map((s, idx) => (
						<RadioInput key={idx} name="typeOfSoling" space={true} option={s} />
					))}
				</List.Item>
				<List.Item>
					{data.thicknessAndGradeOfPCC}
					<DashedNormalInputIm name="thicknessAndGradeOfPCC" />
				</List.Item>

				<List.Item>
					{data.gradeOfConcrete}

					<DashedNormalInputIm name="gradeOfConcrete" />
				</List.Item>
				<List.Item>
					{data.compactionMethod}
					{radioOptions.map((roptions, i) => (
						<RadioInput key={i} name="compactionMethod" space={true} option={roptions} />
					))}
				</List.Item>
				<List.Item>
					{data.formWorkUsedForFoundation}
					{foundationFormwork.map((foundation, i) => (
						<RadioInput key={i} name="formWorkUsedForFoundation" space={true} option={foundation} />
					))}
				</List.Item>
			</List>
			{/* header 2 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{data.columnReinforcement}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data.sizeOfColumn}
					<DashedNormalInputIm name="sizeOfColumn" />
				</List.Item>
				<List.Item>
					{data.noAndSizeOfRebarUsedInTheColumn}
					<DashedNormalInputIm name="noAndSizeOfRebarUsedInTheColumn" />
				</List.Item>
				<List.Item>
					{data.sizeAndSpacingOfStirrupsheader2}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsheader2" />
				</List.Item>

				<List.Item>
					{data.concreteAndRebarGradeheader2}
					<DashedNormalInputIm name="concreteAndRebarGradeheader2" />
				</List.Item>
			</List>
			<br />
			{/* header 3 */}
			<SectionHeader>
				<h3 className="left-align">{data.foundationBeam}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data.sizeOfFoundationBeam}
					<DashedNormalInputIm name="sizeOfFoundationBeam" />
				</List.Item>
				<List.Item>
					{data.noAndSizeOfRebarUsedInTheFoundationalBeam}
					<DashedNormalInputIm name="noAndSizeOfRebarUsedInTheFoundationalBeam" />
				</List.Item>
				<List.Item>
					{data.sizeAndSpacingOfStirrupsheader3}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsheader3" />
				</List.Item>

				<List.Item>
					{data.concreteAndRebarGradeheader3}
					<DashedNormalInputIm name="concreteAndRebarGradeheader3" />
				</List.Item>
			</List>
			<br />
			<SectionHeader>
				<h3 className="left-align">{data.strapBeam}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data.sizeOfStrapBeam}
					<DashedNormalInputIm name="sizeOfStrapBeam" />
				</List.Item>
				<List.Item>
					{data.sizeAndNumberOfRebar}
					<DashedNormalInputIm name="sizeAndNumberOfRebar" />
				</List.Item>
				<List.Item>
					{data.sizeAndSpacingOfStirrupsheader4}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsheader4" />
				</List.Item>

				<List.Item>
					{data.concreteAndRebarGradeheader4}
					<DashedNormalInputIm name="concreteAndRebarGradeheader4" />
				</List.Item>
			</List>
			<br />
			<SectionHeader>
				<h3 className="left-align">
					{data.ifAny}
					<DashedNormalInputIm name="ifAny" />
				</h3>
			</SectionHeader>
		</div>
	);
};

const LoadBearing = (props) => {
	const { value, error, setField } = props;
	return (
		<div>
			<SectionHeader>
				<h2>{data2.superVisionReportLB}</h2>
				<h2>{data2.onLB}</h2>
				<h2>{data2.loadBearingStructureBuilding}</h2>
			</SectionHeader>
			<br />
			<List ordered>
				<List.Item>
					{data2.typeOfBuilding}{' '}
					<DashedLangInput
						name="purposeOfConstruction"
						value={value.purposeOfConstruction}
						error={error.purposeOfConstruction}
						setFieldValue={setField}
					/>
				</List.Item>
				<List.Item>
					{data.levelOfCompletion}{' '}
					<DashedLangInput
						name="levelOfCompletion"
						value={value.levelOfCompletion}
						error={error.levelOfCompletion}
						setFieldValue={setField}
					/>
				</List.Item>
			</List>
			<br />
			<SectionHeader>
				<h3 className="left-align">{data2.constructionWorkDetails}</h3>
			</SectionHeader>
			{/* header 1 */}
			<SectionHeader>
				<h3 className="left-align">{data2.foundationWork}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data2.mansoryWork}
					{mansoryWork.map((ansory, i) => (
						<RadioInput key={i} name="mansoryWork" space={true} option={ansory} />
					))}
				</List.Item>
				<List.Item>
					{data2.numberOfShortWallFooting}
					<DashedNormalInputIm name="numberOfShortWallFooting" />
				</List.Item>
				<List.Item>
					{data2.numberOfLongWallFooting}
					<DashedNormalInputIm name="numberOfLongWallFooting" />
				</List.Item>

				<List.Item>
					{data2.numberOfEccentricWallFooting}
					<DashedNormalInputIm name="numberOfEccentricWallFooting" />
				</List.Item>
				<List.Item>
					{data2.lengthaAndThicknessOfShortWallFooting}
					<DashedNormalInputIm name="lengthaAndThicknessOfShortWallFooting" />
				</List.Item>
				<List.Item>
					{data2.lengthaAndThicknessOfLongWallFooting}
					<DashedNormalInputIm name="lengthaAndThicknessOfLongWallFooting" />
				</List.Item>

				<List.Item>
					{data2.lengthAndThicknessOfEccentricFooting}
					<DashedNormalInputIm name="lengthAndThicknessOfEccentricFooting" />
				</List.Item>
				<List.Item>
					{data2.typeOfSolingForm2}
					{soling.map((s, i) => (
						<RadioInput key={i} name="typeOfSolingForm2" space={true} option={s} />
					))}
				</List.Item>
				<List.Item>
					{data2.thicknessAndGradeOfPCCform2}
					<DashedNormalInputIm name="thicknessAndGradeOfPCCform2" />
				</List.Item>

				<List.Item>
					{data2.gradeOfConcreteAndRebarForFoundationAndPlinthBand}
					<DashedNormalInputIm name="gradeOfConcreteAndRebarForFoundationAndPlinthBand" />
				</List.Item>
				<List.Item>
					{data2.compactionMethodForm2}
					{radioOptions.map((roptions, i) => (
						<RadioInput key={i} name="compactionMethodForm2" space={true} option={roptions} />
					))}
				</List.Item>
				<List.Item>
					{data2.formworkUsedForFoundationForm2}
					{foundationFormwork.map((foundation, i) => (
						<RadioInput key={i} name="formworkUsedForFoundationForm2" space={true} option={foundation} />
					))}
				</List.Item>
			</List>
			{/* header 2 */}
			<br />
			<SectionHeader>
				<h3 className="left-align">{data2.foundationBand}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data2.sizeOfFoundationBand}
					<DashedNormalInputIm name="sizeOfFoundationBand" />
				</List.Item>
				<List.Item>
					{data2.noAndSizeOfRebarUsedInTheFoundationBand}
					<DashedNormalInputIm name="noAndSizeOfRebarUsedInTheFoundationBand" />
				</List.Item>
				<List.Item>
					{data2.sizeAndSpacingOfStirrupsheader2}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsheader2" />
				</List.Item>

				<List.Item>
					{data2.concreteAndRebarGradeheader2}
					<DashedNormalInputIm name="concreteAndRebarGradeheader2" />
				</List.Item>
			</List>
			<br />
			{/* header 3 */}
			<SectionHeader>
				<h3 className="left-align">{data2.strapBeam}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data2.sizeOfStrapBeam}
					<DashedNormalInputIm name="sizeOfStrapBeam" />
				</List.Item>
				<List.Item>
					{data2.sizeAndNumberOfMainRebar}
					<DashedNormalInputIm name="sizeAndNumberOfMainRebar" />
				</List.Item>
				<List.Item>
					{data2.sizeAndSpacingOfStirrupsheader3Form2}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsheader3Form2" />
				</List.Item>

				<List.Item>
					{data2.concreteAndRebarGradeheader3Form2}
					<DashedNormalInputIm name="concreteAndRebarGradeheader3Form2" />
				</List.Item>
			</List>
			<br />
			{/* header 4 */}
			<SectionHeader>
				<h3 className="left-align">{data2.plinthBand}</h3>
			</SectionHeader>
			<List>
				<List.Item>
					{data2.sizeOfPlinthBand}
					<DashedNormalInputIm name="sizeOfPlinthBand" />
				</List.Item>
				<List.Item>
					{data2.noAndSizeOfRebarUSedInThePlinthBand}
					<DashedNormalInputIm name="noAndSizeOfRebarUSedInThePlinthBand" />
				</List.Item>
				<List.Item>
					{data2.sizeAndSpacingOfStirrupsHeader4Form2}
					<DashedNormalInputIm name="sizeAndSpacingOfStirrupsHeader4Form2" />
				</List.Item>

				<List.Item>
					{data2.concreteAndRebarGradeHeader4Form2}
					<DashedNormalInputIm name="concreteAndRebarGradeHeader4Form2" />
				</List.Item>
			</List>
			<br />
			<SectionHeader>
				<h3 className="left-align">
					{data2.ifAnyForm2}
					<DashedNormalInputIm name="ifAnyForm2" />
				</h3>
			</SectionHeader>
		</div>
	);
};

const SuperStructureSupervisionReport = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.SuperStructureSupervisionReport).setForm().getParams(), new ApiParam(api.anusuchiGha, 'anuGha')]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		fetchFiles={true}
		render={(props) => <SuperStructureSupervisionReportComponent {...props} parentProps={parentProps} />}
	/>
);
export default SuperStructureSupervisionReport;
