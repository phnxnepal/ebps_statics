import React from 'react';
import api from '../../../utils/api';

import { PrintParams } from '../../../utils/printUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { GenericDesignerSansodhan } from './formComponents/GenericDesignerSansodhan';
import { designerSansodhan } from '../../../utils/data/genericFormData';

const SansodhanBibaran = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.sansodhanBibaranPahilo,
				objName: 'sansodhanBibaranPahilo',
				form: true,
			},
		]}
		onBeforeGetContent={PrintParams.TEXT_AREA}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFile={true}
		render={props => (
			<GenericDesignerSansodhan
				fieldName="nibedan"
				topic={designerSansodhan.sansodhanBibaranPahilo.topic}
				api={api.sansodhanBibaranPahilo}
				{...props}
				parentProps={parentProps}
			/>
		)}
	/>
);

export default SansodhanBibaran;
