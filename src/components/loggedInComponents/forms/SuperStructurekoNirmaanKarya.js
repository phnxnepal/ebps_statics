import React, { Component } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { olddosrocharanPrabidhikview } from '../../../utils/data/mockLangFile';
import { Formik } from 'formik';
import * as Yup from 'yup';
// import JSONDataFormContainer from '../../../containers/base/JSONDataFormContainer';
import api from '../../../utils/api';
import { isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { DashedLangDateField } from '../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { validateNepaliDate } from '../../../utils/validationUtils';
import { PrintParams } from '../../../utils/printUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';
import { getParagraph, getYesNoSection } from '../../../utils/clientConfigs/superStructurePrabidhikRaya';
import { RenderLayout } from '../../shared/layout/GenericLayout';
import { isKamalamai } from '../../../utils/clientUtils';
import { kamalamaiPlinthTechAppData } from '../../../utils/data/PilenthTechAppLang';
import { FooterSignature } from './formComponents/FooterSignature';
import { getApproveByObject, getSaveByUserDetails } from '../../../utils/formUtils';
import { ImageDisplayInline } from '../../shared/file/FileView';

const dpv = olddosrocharanPrabidhikview.dosrocharanPrabidhikview_data;

const initialValues = {
	sadakMapdandaPalana: dpv.radioOption.opt_1,
	setBackPalana: dpv.radioOption.opt_1,
	groundCoverage: dpv.radioOption.opt_1,
	mulMapdandaPalana: dpv.radioOption.opt_1,
	bhawanNirmanSanghita: dpv.radioOption.opt_1,
};

const Schema = Yup.object().shape({
	nirmanKaryaDate: validateNepaliDate,
});

class SuperStructurekoNirmaanKaryaViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { orgCode, permitData, enterByUser, userData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const asubData = getApproveBy(0);
		const serData = getApproveBy(1);
		const erData = getApproveBy(2);
		let jsonData = getJsonData(this.props.prevData);
		// initVal = jsonData;
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = this.props.userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(this.props.userData.userType);
		// }
		initVal = prepareMultiInitialValues(
			{ obj: { sakha: 'घर नक्सा शाखा' }, reqFields: [] },
			{
				obj: this.props.userData,
				reqFields: ['organization', 'userType', 'userName'],
			},
			{
				obj: {
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					erSignature: erData.signature,
					serSignature: serData.signature,
					asubSignature: asubData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initVal.prabhidikDate)) {
			initVal.prabhidikDate = getCurrentDate(true);
		}
		if (isStringEmpty(initVal.nirmanKaryaDate)) {
			initVal.nirmanKaryaDate = getCurrentDate(true);
		}
		this.state = { initVal, paragraph: getParagraph(orgCode, permitData), yesNoSection: getYesNoSection() };
	}
	render() {
		const { initVal, paragraph, yesNoSection } = this.state;
		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, useSignatureImage } = this.props;

		return (
			<div className="pilenthLevelTechViewWrap">
				<Formik
					initialValues={{ ...initialValues, ...initVal }}
					validationSchema={Schema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(api.supernirmaanKarya, values, true);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, setFieldValue, isSubmitting, handleChange, handleSubmit, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<SectionHeader>
										<h3 className="underline end-section">{dpv.heading.heading_1}</h3>
										<h3 className="underline end-section">{dpv.heading.heading_2}</h3>
									</SectionHeader>
									{paragraph.map((p, idx) => (
										<div key={idx}>{p.data}</div>
									))}
									<RenderLayout
										layout={yesNoSection}
										setFieldValue={setFieldValue}
										handleChange={handleChange}
										errors={errors}
										values={values}
									/>
									<div>
										{dpv.condate.condate_1}
										<DashedLangDateField
											inline={true}
											name="nirmanKaryaDate"
											value={values.nirmanKaryaDate}
											setFieldValue={setFieldValue}
											error={errors.nirmanKaryaDate}
										/>
										{dpv.condate.condate_2}
									</div>
									<br />
									<div>
										{useSignatureImage && values.subSignature ? (
											<ImageDisplayInline
												label={dpv.condate.condate_3}
												height={35}
												alt="user signature"
												src={values.subSignature}
											/>
										) : (
											<div>
												{dpv.condate.condate_3}
												<span className="ui input dashedForm-control" />
											</div>
										)}
									</div>
									{/* <div>
										{dpv.condate.condate_3}
										<span className="ui input dashedForm-control"></span>
									</div> */}

									<Grid columns={2}>
										<Grid.Row>
											<Grid.Column>
												<div className="inline field">
													<label>{dpv.info1.info_1}</label>
													<div className="ui input">
														<DashedLangInput
															name="subName"
															value={values.subName}
															setFieldValue={setFieldValue}
															onChange={handleChange}
														/>
													</div>
												</div>
												<div className="inline field">
													<label>{dpv.info1.info_2}</label>
													<div className="ui input">
														<DashedLangInput name="sakha" value={values.sakha} setFieldValue={setFieldValue} />
													</div>
												</div>
											</Grid.Column>
											<Grid.Column>
												<div className="inline field">
													<label>{dpv.info2.info_1}</label>
													<div className="ui input">
														<DashedLangInput
															name="subDesignation"
															value={values.subDesignation}
															setFieldValue={setFieldValue}
															onChange={handleChange}
														/>
													</div>
												</div>
												<div className="no-margin-field">
													{dpv.info2.info_2}
													<DashedLangDateField
														name="prabhidikDate"
														setFieldValue={setFieldValue}
														value={values.prabhidikDate}
														inline={true}
														// handleChange={handleChange}
													/>
												</div>
											</Grid.Column>
										</Grid.Row>
									</Grid>
									{isKamalamai ? (
										<div>
											<br />
											<SectionHeader>
												<h3>{kamalamaiPlinthTechAppData.footer.title}</h3>
											</SectionHeader>
											<FooterSignature
												designations={[
													kamalamaiPlinthTechAppData.footer.amin,
													kamalamaiPlinthTechAppData.footer.ser,
													kamalamaiPlinthTechAppData.footer.er,
												]}
												signatureImages={useSignatureImage && [values.asubSignature, values.serSignature, values.erSignature]}
											/>
										</div>
									) : null}
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				/>
			</div>
		);
	}
}

const SuperStructurekoNirmaanKarya = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.supernirmaanKarya,
				objName: 'supernirmaanKarya',
				form: true,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param1: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		render={(props) => <SuperStructurekoNirmaanKaryaViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructurekoNirmaanKarya;
