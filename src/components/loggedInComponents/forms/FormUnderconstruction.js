import React from 'react'
import { Grid, Icon } from 'semantic-ui-react';

const FormUnderconstruction = () => {
    return (
        <Grid centered>
            <Grid.Row><Icon name="warning sign" size="huge"/></Grid.Row>
            <Grid.Row><h3>This form is underconstruction.</h3></Grid.Row>
        </Grid>
    )
}

export default FormUnderconstruction
