import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { SansodhanStructure } from '../../../../utils/data/SansodhanSuperStructureData';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { getApproveByObject } from '../../../../utils/formUtils';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import api from '../../../../utils/api';
import { showToast } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangDateField } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { FooterSignature } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { PrintParams } from '../../../../utils/printUtils';
import { footerSignature } from '../../../../utils/data/genericFormData';

const ss = SansodhanStructure.structureDesign;

const SansodhanSuperSchema = Yup.object().shape({
	date: validateNullableNepaliDate,
	samsoNakxaDate: validateNullableNepaliDate,
	mapdandaHakDate: validateNullableNepaliDate,
	superStructureBuildDate: validateNullableNepaliDate,
});

class SansodhitSuperStructureIjajatSambandhamaComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, permitData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const naksaPassData = getApproveBy(0);
		const janchData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const json_data = getJsonData(prevData);
		const json_data2 = getJsonData(otherData.supStruCons);
		const superStructBuild = getJsonData(otherData.superStructBuild);
		// const allowancePaper = getJsonData(otherData.allowancePaper);

		const initialValues = prepareMultiInitialValues(
			{
				// Default values
				obj: {
					sansodhanSuperIijajatDate: getCurrentDate(true),
					date: getCurrentDate(true),
					samsoNakxaDate: getCurrentDate(true),
					mapdandaHakDate: getCurrentDate(true),
					gharDaniWares: 'gd',
					shreeName: permitData.nibedakName,
					nayaMiti: json_data2.superStruSubmitDate,
				},
				reqFields: [],
			},
			// {
			// 	obj: allowancePaper,
			// 	reqFields: ['gharNo'],
			// },
			{
				obj: superStructBuild,
				reqFields: ['superStructureBuildDate'],
			},
			{ obj: json_data, reqFields: [] },
			{
				obj: {
					naksaPassSignature: naksaPassData.signature,
					janchSignature: janchData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		this.state = {
			initialValues,
		};
	}
	render() {
		const { initialValues } = this.state;
		const { permitData, errors: reduxErrors, userData, useSignatureImage, getSaveButtonProps } = this.props;

		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={SansodhanSuperSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.SansodhitSuperStructureIjajatSambandhama}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, setFieldValue, values, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={userData} />

									<FlexSingleRight>
										<DashedLangDateField
											name="date"
											setFieldValue={setFieldValue}
											value={values.date}
											error={errors.date}
											label={ss.miti}
										/>
									</FlexSingleRight>
									<SectionHeader>
										<h3 className="underline">{ss.subject}</h3>
									</SectionHeader>
									<br />
									<>
										<div style={{ textAlign: 'justify' }} className="no-margin-field">
											<div>
												{ss.shree}
												<DashedLangInput
													name="shreeName"
													setFieldValue={setFieldValue}
													value={values.shreeName}
													error={errors.shreeName}
												/>

												<br />
											</div>
											{/* {userData.organization.name}
											{ss.content.content1}
											{permitData.nibedakTol}
											{ss.content.content2} */}
											<DashedLangInput
												name="additionalSalutation"
												setFieldValue={setFieldValue}
												value={values.additionalSalutation}
												error={errors.additionalSalutation}
											/>
											{/* {ss.content.content3}
											{permitData.nibedakSadak} */}
											<br />
											<br />
											{ss.content.content4}
											<DashedLangDateField
												inline={true}
												name="superStructureBuildDate"
												setFieldValue={setFieldValue}
												value={values.superStructureBuildDate}
												error={errors.superStructureBuildDate}
											/>
											{ss.content.maWard}
											{permitData.newWardNo}
											{ss.content.content8}
											{permitData.kittaNo}
											{ss.content.content9}
											{permitData.landArea} {permitData.landAreaType}
											{ss.content.content10}
											<DashedLangDateField
												inline={true}
												name="samsoNakxaDate"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.samsoNakxaDate}
												error={errors.samsoNakxaDate}
											/>
											{ss.content.content11}
											<br />
											<br />
											{ss.content.content12}
											<DashedLangDateField
												inline={true}
												name="mapdandaHakDate"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.mapdandaHakDate}
												error={errors.mapdandaHakDate}
											/>
											{ss.content.content13}
										</div>
										<FooterSignature
											designations={[footerSignature.nirikshanGariPesh, footerSignature.sipharis, footerSignature.swikrit]}
											signatureImages={
												useSignatureImage && [values.naksaPassSignature, values.janchSignature, values.pramukhSignature]
											}
										/>
									</>
								</div>
							</div>
							<br />
							<SaveButtonValidation errors={errors} validateForm={validateForm} handleSubmit={handleSubmit} {...getSaveButtonProps()} />
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SansodhitSuperStructureIjajatSambandhama = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.SansodhitSuperStructureIjajatSambandhama,
				objName: 'sanSupStructIijajat',
				form: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: false,
			},
			{
				api: api.superStructureBuild,
				objName: 'superStructBuild',
				form: false,
			},
			// {
			// 	api: api.allowancePaper,
			// 	objName: 'allowancePaper',
			// 	form: false,
			// },
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		render={(props) => <SansodhitSuperStructureIjajatSambandhamaComponent {...props} parentProps={parentProps} />}
	/>
);

export default SansodhitSuperStructureIjajatSambandhama;
