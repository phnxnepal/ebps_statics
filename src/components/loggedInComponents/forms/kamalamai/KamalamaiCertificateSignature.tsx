import React from 'react';
import { footerSignature } from '../../../../utils/data/genericFormData';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';

export const KamalamaiCertificateSignature = ({ images }: { images: [] }) => {
	return (
		<FooterSignatureMultiline
			designations={[
				[footerSignature.fieldNirikshan],
				[footerSignature.sipharis, `(${footerSignature.ser})`],
				[footerSignature.sipharis, `(${footerSignature.er})`],
				[footerSignature.swikrit, `(${footerSignature.chief})`],
			]}
			signatureImages={images}
		/>
	);
};
