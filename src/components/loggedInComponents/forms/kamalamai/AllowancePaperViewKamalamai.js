import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { Formik } from 'formik';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { AllowancePaperBodyKamalamai, KamalamaiSubject } from '../ijajatPatraComponents/AllowancePaperComponents';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { Form } from 'semantic-ui-react';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { AllowancePaperData } from '../../../../utils/data/AllowancePaperData';
import { FooterSignature } from '../formComponents/FooterSignature';
import { getSaveByUserDetails, getApproveByObject } from '../../../../utils/formUtils';
import { KamalamaiCertificateSignature } from './KamalamaiCertificateSignature';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';

const kamalamaiData = AllowancePaperData.kamalamaiData;

export class AllowanceKamalamaiComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, userData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;

		const jsonData = getJsonData(prevData);

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const floorMax = floorArray.getTopFloor().nepaliCount;

		const mapTech = getJsonData(otherData.mapTech);
		// const prabidhikPratibedan = getJsonData(otherData.prabidhikPratibedan);
		const RajaswoData = getJsonData(otherData.RajaswoData);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const desApprovJsonData = otherData.designApproval;
		const anuSucKaJsonData = otherData.anukaMaster;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
					floorMax,
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			// {
			// 	obj: prabidhikPratibedan,
			// 	reqFields: ['floor'],
			// },
			{
				obj: RajaswoData,
				reqFields: ['requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: mapTech,
				reqFields: [
					'constructionType',
					'roof',
					'roofOther',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'plinthDetails',
					'plinthLength',
					'plinthWidth',
					'plinthLengthUnit',
					'buildingHeight',
					'heightUnit',
				],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{
				obj: {
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					floorareaUnit: DEFAULT_UNIT_LENGTH,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					floorwidthUnit: DEFAULT_UNIT_LENGTH,
					floorheightUnit: DEFAULT_UNIT_LENGTH,
					batoUnit: DEFAULT_UNIT_LENGTH,
					bidhutUnit: DEFAULT_UNIT_LENGTH,
					nadiUnit: DEFAULT_UNIT_LENGTH,
					patraSankhya: '',
					chalaniNumber: '',
					gharNo: '',
				},
				reqFields: [],
			},

			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.allowanceDate)) {
			initialValues.allowanceDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		let anusuchikaOptions = [];

		otherData.anukaMaster.map((row) =>
			anusuchikaOptions.push({
				value: row.id,
				text: `${row.nameNepali} ${row.name}`,
			})
		);

		this.state = {
			initialValues,
			buildingClass,
			floorArray,
			formattedFloors,
			floorMax,
		};
	}
	render() {
		const {
			userData,
			permitData,
			prevData,
			formUrl,
			hasSavePermission,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues, buildingClass, floorArray, formattedFloors, floorMax } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					// validationSchema={AllowancePaperSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.allowancePaper, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="view-Form print-small-font">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<div className="flex-item-right">
										{kamalamaiData.date}
										<DashedLangDateField
											compact={true}
											name="allowanceDate"
											value={values.allowanceDate}
											error={errors.allowanceDate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</div>
									<KamalamaiSubject />
									<div>
										<AllowancePaperBodyKamalamai
											formattedFloors={formattedFloors}
											floorArray={floorArray}
											floorMax={floorMax}
											values={values}
											errors={errors}
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											buildingClass={buildingClass}
											permitData={permitData}
											userData={userData}
										/>
									</div>
									<FlexSingleRight>
										<FooterSignature
											signatureImages={[staticFiles.ghardhaniSignature]}
											designations={[kamalamaiData.signature.nakshawala]}
										/>
									</FlexSingleRight>
									<KamalamaiCertificateSignature
										images={
											useSignatureImage && [
												values.subSignature,
												values.sipharisSignature,
												values.swikritSignature,
												values.pramukhSignature,
											]
										}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const AllowancePaperViewKamalamai = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.allowancePaper, objName: 'allowancePaper', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabidhikPratibedan',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'RajaswoData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		fetchFiles={true}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <AllowanceKamalamaiComponent {...props} parentProps={parentProps} />}
	/>
);

export default AllowancePaperViewKamalamai;
