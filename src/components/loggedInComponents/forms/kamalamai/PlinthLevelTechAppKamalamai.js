import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import * as Yup from 'yup';
import { PlinthLevelTechLang, sundarData, kamalamaiPlinthTechAppData } from '../../../../utils/data/PilenthTechAppLang';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FooterSignature } from '../formComponents/FooterSignature';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { ImageDisplayInline } from '../../../shared/file/FileView';
import { SignatureImage } from '../formComponents/SignatureImage';

const PlinthTechLang = PlinthLevelTechLang;
const data = kamalamaiPlinthTechAppData;

const plinthLevelAppSchema = {
	plinthApprovalDate: validateNullableNepaliDate,
	date: validateNullableNepaliDate,
};

const schema = Yup.object().shape(Object.assign({ ...yesNoSectionSchema, ...plinthLevelAppSchema }));

class PlinthLevelTechAppKamalamaiComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, enterByUser, userData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const asubData = getApproveBy(0);
		const serData = getApproveBy(1);
		const erData = getApproveBy(2);

		const json_data = getJsonData(prevData);
		const plinthOwner = getJsonData(otherData.plinthOwner);
		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };
		// serInfo.subName = enterByUser.name;
		// serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					letterSubmitDate: getCurrentDate(true),
					date: getCurrentDate(true),
				},
				reqFields: [],
			},
			{
				obj: {
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: plinthOwner, reqFields: [] },
			{ obj: json_data, reqFields: [] },
			{
				obj: {
					erSignature: erData.signature,
					serSignature: serData.signature,
					asubSignature: asubData.signature,
				},
				reqFields: [],
			}
		);
		this.state = { initialValues };
	}

	render() {
		const {
			errors: reduxErrors,
			permitData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues } = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// console.log('log');
						try {
							await this.props.postAction(`${api.plintLevelTechApplication}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleSubmit, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h3 className="underline end-section">{data.heading}</h3>
								</SectionHeader>

								<br />
								<div className="no-margin-field margin-bottom">
									{data.address} {permitData.newMunicipal} {data.wardNo} {permitData.newWardNo} {data.kittaNo} {permitData.kittaNo}{' '}
									{data.area} {permitData.landArea} {permitData.landAreaType} {data.gharDhani} {permitData.applicantName}
									{data.bhawanDate}{' '}
									<CompactDashedLangDate name="date" setFieldValue={setFieldValue} value={values.date} error={errors.date} />{' '}
									{data.end}
								</div>
								<YesNoSection setFieldValue={setFieldValue} errors={errors} values={values} />
								<br />
								<div>{data.note}</div>

								<div className="margin-bottom">
									<div>
										{PlinthTechLang.sign}: <span className="ui input dashedForm-control" />
									</div>
									<div>
										{data.peshGarne}{' '}
										<DashedLangInput name="subName" setFieldValue={setFieldValue} value={values.subName} error={errors.subName} />
									</div>
									<Grid className="zero-padding-grid">
										<Grid.Row columns={2}>
											<Grid.Column>
												<div>
													{useSignatureImage && values.subSignature ? (
														<ImageDisplayInline
															label={PlinthTechLang.sign}
															height={35}
															alt="user signature"
															src={values.subSignature}
														/>
													) : (
														<div>
															{PlinthTechLang.sign}
															{': '}
															<span className="ui input dashedForm-control" />
														</div>
													)}
												</div>
											</Grid.Column>
											<Grid.Column>
												{data.nibedakNam} {permitData.applicantName}
											</Grid.Column>
										</Grid.Row>
										<Grid.Row columns={2}>
											<Grid.Column>
												{sundarData.signatureSection.date}:{' '}
												<DashedLangInput name="date" setFieldValue={setFieldValue} value={values.date} error={errors.date} />
											</Grid.Column>
											<Grid.Column>
												<SignatureImage
													value={staticFiles.ghardhaniSignature}
													label={PlinthTechLang.sign}
													showSignature={true}
												/>
												{/* {PlinthTechLang.sign}: <span className="ui input dashedForm-control" /> */}
											</Grid.Column>
										</Grid.Row>
									</Grid>
								</div>
								<br />
								<div>
									<SectionHeader>
										<h3>{data.footer.title}</h3>
									</SectionHeader>
									<FooterSignature
										designations={[data.footer.amin, data.footer.ser, data.footer.er]}
										signatureImages={useSignatureImage && [values.asubSignature, values.serSignature, values.erSignature]}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const PlinthLevelTechAppKamalamai = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthTechApp',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui checkbox', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByTagName', 'input', 'value'],
		}}
		fetchFiles={true}
		render={(props) => <PlinthLevelTechAppKamalamaiComponent {...props} parentProps={parentProps} />}
	/>
);

export default PlinthLevelTechAppKamalamai;
