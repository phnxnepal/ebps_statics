import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';

import { superstructureConstructionKamalamai } from '../../../../utils/data/mockLangFile';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';

import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { KamalamaiSchema } from '../../formValidationSchemas/superStructureConstructionValidation';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { DashedLangDateField } from './../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight } from './../../../uiComponents/FlexDivs';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { DashedMultiUnitLengthInput, DashedMultiUnitAreaInput } from '../../../shared/EbpsUnitLabelValue';
import { squareUnitOptions } from '../../../../utils/optionUtils';
import { PurposeOfConstructionRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import { AllowanceKamalamaiBlockFloors } from '../certificateComponents/CertificateFloorInputs';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { NirmanTypeAnyaRadio } from '../mapTechFormComponents/NirmanTypeRadio';
import { getApproveByObject, getSaveByUserDetails, getApprovalDate } from '../../../../utils/formUtils';
import { ApiParam } from '../../../../utils/paramUtil';
import { FormUrlFull } from '../../../../utils/enums/url';
import { KamalamaiCertificateSignature } from './KamalamaiCertificateSignature';
import { SignatureImageVertical } from '../formComponents/SignatureImage';

const superKamalamaiData = superstructureConstructionKamalamai.kamalamaiData;
const bodyData = superstructureConstructionKamalamai.kamalamaiData.body;
class SuperstructureKamalamaiViewComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData, otherData, enterByUser, userData, DEFAULT_UNIT_LENGTH } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const allowanceApprovalDate = getApprovalDate(FormUrlFull.ALLOWANCE_PAPER, otherData.allowancePaper);

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues = prepareMultiInitialValues(
			{
				obj: { plinthLengthUnit: DEFAULT_UNIT_LENGTH, floorUnit: DEFAULT_UNIT_LENGTH, heightUnit: DEFAULT_UNIT_LENGTH },
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			{
				obj: mapTech,
				reqFields: [
					'constructionType',
					'roof',
					'roofOther',
					'plinthDetails',
					'plinthLength',
					'plinthWidth',
					'plinthLengthUnit',
					'buildingHeight',
					'heightUnit',
					'buildingFloorHeight',
				],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingArea', floorArray.getBottomFloor()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
					...getSaveByUserDetails(enterByUser, userData),
					jaggaDate: allowanceApprovalDate,
				},
				reqFields: [],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.date)) {
			initialValues.date = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues.gharDhaniDate)) {
			initialValues.gharDhaniDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);
		this.state = {
			isSucess: false,
			initialValues,
			floorArray,
			formattedFloors,
		};
	}

	render() {
		const {
			permitData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues, floorArray, formattedFloors } = this.state;
		const user_info = this.props.userData;
		return (
			<Formik
				initialValues={initialValues}
				validationSchema={KamalamaiSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						await this.props.postAction(`${api.superStructureConstruction}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error in Structure Construction Save', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							<div>
								<LetterHeadFlex userInfo={user_info} />
								<br />
								<SectionHeader>
									<h2 className="underline">{superKamalamaiData.title_1}</h2>
									<h3>{superKamalamaiData.title_2}</h3>
								</SectionHeader>
								<div style={{ textAlign: 'right' }}>
									<DashedLangDateField
										label={superKamalamaiData.date}
										name="date"
										setFieldValue={setFieldValue}
										error={errors.date}
										value={values.date}
										inline={true}
									/>
								</div>
								<br />
								<div>
									{superKamalamaiData.data_1}
									{permitData.nibedakName}
								</div>
								<div className="no-margin-field" style={{ textAlign: 'justify' }}>
									{superKamalamaiData.data_2}
									{permitData.nibedakTol}
									{superKamalamaiData.data_3}
									{superKamalamaiData.data_4}
									{permitData.sadak}
									{superKamalamaiData.data_5}
									{permitData.kittaNo}
									{superKamalamaiData.data_6}
									{permitData.landArea}
									{permitData.landAreaType}
									{superKamalamaiData.data_7}
									<DashedLangDateField
										name="jaggaDate"
										setFieldValue={setFieldValue}
										error={errors.jaggaDate}
										value={values.jaggaDate}
										inline={true}
										compact={true}
									/>
									{superKamalamaiData.data_8}
								</div>

								<div>
									<ConstructionTypeRadio space={true} fieldLabel={bodyData.list1} />
								</div>
								<div>
									<DashedMultiUnitLengthInput
										name="plinthLength"
										unitName="plinthLengthUnit"
										relatedFields={['plinthWidth', 'buildingHeight']}
										areaField={'plinthDetails'}
										label={bodyData.list2}
									/>
									<DashedMultiUnitLengthInput
										name="plinthWidth"
										unitName="plinthLengthUnit"
										relatedFields={['plinthLength', 'buildingHeight']}
										areaField={'plinthDetails'}
										label={bodyData.chaudai}
									/>
									{bodyData.tala}
									<DashedLangInput
										name="buildingFloorHeight"
										setFieldValue={setFieldValue}
										value={values['buildingFloorHeight']}
										error={errors.buildingFloorHeight}
									/>
									<DashedMultiUnitLengthInput
										name="buildingHeight"
										unitName="heightUnit"
										relatedFields={['plinthLength', 'plinthWidth']}
										areaField={'plinthDetails'}
										label={bodyData.uchai}
									/>
									<DashedMultiUnitAreaInput
										name="plinthDetails"
										unitName="plinthLengthUnit"
										label={bodyData.area}
										squareOptions={squareUnitOptions}
										relatedFields={['plinthLength', 'plinthWidth', 'buildingHeight']}
									/>
								</div>

								<div>
									<PurposeOfConstructionRadio
										values={values}
										setFieldValue={setFieldValue}
										errors={errors}
										space={true}
										fieldLabel={`${bodyData.list3}:`}
									/>
								</div>

								<div>
									<NirmanTypeAnyaRadio
										fieldLabel={bodyData.list4}
										values={values}
										setFieldValue={setFieldValue}
										errors={errors}
										space={true}
									/>
								</div>
								<div>
									{bodyData.list8}
									<div className="div-indent">
										<AllowanceKamalamaiBlockFloors floorArray={floorArray} formattedFloors={formattedFloors} />
									</div>
								</div>
								<div>
									{bodyData.list9}
									<EbpsTextareaField
										placeholder="....."
										name="miscDesc"
										setFieldValue={setFieldValue}
										value={values.miscDesc}
										error={errors.miscDesc}
									/>
								</div>
								<KamalamaiCertificateSignature
									images={
										useSignatureImage && [
											values.subSignature,
											values.sipharisSignature,
											values.swikritSignature,
											values.pramukhSignature,
										]
									}
								/>
								<br />
								<div>{superKamalamaiData.footer_4}</div>
								<br />
								<FlexSingleRight>
									<SignatureImageVertical
										value={staticFiles.ghardhaniSignature}
										label={superKamalamaiData.footer_5}
										showSignature={true}
									/>
									{/* <DashedLangInput name="waresh" value={values.waresh} setFieldValue={setFieldValue} error={errors.waresh} />
									<br />
									{superKamalamaiData.footer_5} */}
								</FlexSingleRight>
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}
const SuperstructureConstructionKamalamai = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: true,
			},
			new ApiParam(api.allowancePaper, 'allowancePaper').getParams(),
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		fetchFiles={true}
		onBeforeGetContent={{
			//param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			//param9: ['getElementsByClassName', 'brackets', 'value'],
		}}
		render={(props) => <SuperstructureKamalamaiViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperstructureConstructionKamalamai;
