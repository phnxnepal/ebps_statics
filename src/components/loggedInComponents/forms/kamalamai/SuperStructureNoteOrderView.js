import React, { Component } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { PrintSelectors } from '../../../../utils/printUtils';
import { Form } from 'semantic-ui-react';
import { superstructurenodeorderview } from '../../../../utils/data/mockLangFile';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { getJsonData, getApprovalData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserRole, getUserTypeValueNepali, isEmpty, showToast } from '../../../../utils/functionUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getMessage } from '../../../shared/getMessage';
import { PrintParams } from '../../../../utils/printUtils';
import { DashedLangDateField, CompactDashedLangDate, DashedDateField } from '../../../shared/DateField';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { userdata } from '../../../../utils/data/ebps-setup/userformdata';

const ssnov_data = superstructurenodeorderview.superstructurenodeorderview_data;
const kamalamaiData = superstructurenodeorderview.kamalamaiData;
const messageId = 'superstructurenodeorderview.superstructurenodeorderview_data';

/**
 * @TODO remove unused variables
 */
const SuperStructureNoteSchema = Yup.object().shape(
	Object.assign({
		dates: validateNullableNepaliDate,
		peshGarneDate: validateNullableNepaliDate,
		Allowancedate: validateNullableNepaliDate,
		erDate: validateNullableNepaliDate,
		chiefDate: validateNullableNepaliDate,
	})
);

class SuperStructureNoteOrderKamalamaiViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { userData, prevData, otherData } = this.props;
		console.log({ userData, prevData, otherData })
		const json_data = getJsonData(prevData);
		const AllowancePaperData = getJsonData(otherData.AllowancePaper);

		let serInfo = {
			subName: '',
			subDesignation: '',
		};

		if (getUserRole() === UserType.SUB_ENGINEER) {
			serInfo.subName = userData.userName;
			serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		}

		let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			{
				obj: AllowancePaperData,
				reqFields: ['allowanceDate'],
			},
			{
				obj: { superStructureNoteOrderDate: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{ obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);

		initVal.superStructureUnit = 'METRE';
		initVal.otherAreaUnit = 'SQUARE METRE';

		if (isStringEmpty(initVal.dates)) {
			initVal.dates = getCurrentDate(true);
		}

		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		const user_info = this.props.userData;

		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={SuperStructureNoteSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.SuperStructureNoteOrder}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={user_info} />
								<FlexSingleRight>
									<DashedLangDateField
										label={ssnov_data.date}
										name="superStructureNoteOrderDate"
										setFieldValue={setFieldValue}
										// inline={true}
										compact={true}
										value={values.superStructureNoteOrderDate}
										error={errors.superStructureNoteOrderDate}
									/>
								</FlexSingleRight>
								<SectionHeader>
									<h2 className="underline end-section">
										<span>{getMessage(`${messageId}.title_1`, ssnov_data.title_1)}</span>
									</h2>
								</SectionHeader>
								<SectionHeader>
									<h3 className="end-section">
										<span>{kamalamaiData.bisaye}</span>
									</h3>
								</SectionHeader>
								<div className="no-margin-field margin-bottom">
									<span className="indent-span">{`${kamalamaiData.address} ${userData.organization.name} ${kamalamaiData.esthan} `}</span>
									{` ${permitData.applicantAddress}`} {kamalamaiData.wardNo} {permitData.newWardNo} {kamalamaiData.kittaNo}{' '}
									{permitData.kittaNo} {kamalamaiData.area} {permitData.landArea} {permitData.landAreaType}{' '}
									{kamalamaiData.gharDhani} {permitData.applicantName} {kamalamaiData.bhawanDate}{' '}
			
									<CompactDashedLangDate
										name="allowanceDate"
										setFieldValue={setFieldValue}
										value={values.allowanceDate}
										error={errors.allowanceDate}
									/>

									{kamalamaiData.prabidhikShree}{' '}
									<DashedLangInput
										name="prabidhikName"
										setFieldValue={setFieldValue}
										value={values.prabidhikName}
										error={errors.prabidhikName}
									/>
									{kamalamaiData.organization} {userData.organization.name} {kamalamaiData.end}
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SuperStructureNoteOrderKamalamaiView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.SuperStructureNoteOrder,
				objName: 'superStructNoteOrder',
				form: true,
			},
			{
				api: api.allowancePaper,
				objName: 'AllowancePaper',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param9: ['getElementsByClassName', PrintSelectors.DASHED_DROPDOWN],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		render={(props) => <SuperStructureNoteOrderKamalamaiViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNoteOrderKamalamaiView;
