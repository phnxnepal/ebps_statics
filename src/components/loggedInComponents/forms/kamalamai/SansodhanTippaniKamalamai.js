import React from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import GenericRajasowTippaniForm from './GenericRajasowTippaniForm';

const SansodhanTippaniKamalamai = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.sansodhanTippani).setForm().getParams(),
			new ApiParam(api.rajasowSetup, 'rajasowMaster')
				.setUtility()
				.setConcatToApi({ prefix: '?wardNo=', fieldName: 'newWardNo', needsTranslation: true })
				.getParams(),
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param4: ['15dayspecial'],
			param5: ['removeOnPrint'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <GenericRajasowTippaniForm {...props} parentProps={parentProps} postApi={api.sansodhanTippani} />}
	/>
);
export default SansodhanTippaniKamalamai;
