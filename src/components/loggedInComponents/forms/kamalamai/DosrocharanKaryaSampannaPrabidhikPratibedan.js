import React, { Component } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import api from '../../../../utils/api';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { kamalamaiDosrcharanPrabidhikPratibedan } from '../../../../utils/data/dosrocharanPrabidhik';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { PlinthLevelTechLang } from '../../../../utils/data/PilenthTechAppLang';

const PlinthTechLang = PlinthLevelTechLang;
const data = kamalamaiDosrcharanPrabidhikPratibedan;

const plinthLevelAppSchema = {
	plinthApprovalDate: validateNullableNepaliDate,
	date: validateNullableNepaliDate,
	sipharisDate: validateNullableNepaliDate,
};

const schema = Yup.object().shape(Object.assign({ ...yesNoSectionSchema, ...plinthLevelAppSchema }));

class DosrocharanKaryaSampannaPrabidhikPratibedanViewComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, enterByUser } = this.props;

		const json_data = getJsonData(prevData);
		const plinthOwner = getJsonData(otherData.plinthOwner);
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					letterSubmitDate: getCurrentDate(true),
					date: getCurrentDate(true),
					sipharisDate: getCurrentDate(true),
					sipharisName: '',
					sipharisDesignation: '',
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{ obj: plinthOwner, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = { initialValues };
	}

	render() {
		const { errors: reduxErrors, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialValues } = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// console.log('log');
						try {
							await this.props.postAction(`${api.plintLevelTechApplication}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleSubmit, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h3 className="underline end-section">{data.heading}</h3>
								</SectionHeader>

								<br />
								<div className="no-margin-field margin-bottom">
									{data.address} {permitData.newMunicipal} {data.wardNo} {permitData.newWardNo} {data.kittaNo} {permitData.kittaNo}{' '}
									{data.area} {permitData.landArea} {permitData.landAreaType} {data.gharDhani} {permitData.applicationName}{' '}
									{data.bhawanDate}{' '}
									<CompactDashedLangDate name="date" setFieldValue={setFieldValue} value={values.date} error={errors.date} />{' '}
									{data.end}
								</div>
								<YesNoSection setFieldValue={setFieldValue} errors={errors} values={values} />
								<br />
								<div className="margin-bottom">{data.note}</div>
								<div className="margin-bottom">
									<Grid className="zero-padding-grid">
										<Grid.Row columns={2}>
											<Grid.Column>
												<div>
													<b>{data.peshGarne}</b>
												</div>
												<div>
													{data.nameThar}
													<DashedLangInput
														name="subName"
														setFieldValue={setFieldValue}
														value={values.subName}
														error={errors.subName}
													/>
												</div>
												<div>
													{data.padh}
													<DashedLangInput
														name="subDesignation"
														setFieldValue={setFieldValue}
														value={values.subDesignation}
														error={errors.subDesignation}
													/>
												</div>
												<div>
													{PlinthTechLang.sign}: <span className="ui input dashedForm-control" />
												</div>
												<div>
													{data.peshGarneMiti}
													<CompactDashedLangDate
														name="date"
														setFieldValue={setFieldValue}
														value={values.date}
														error={errors.date}
													/>
												</div>
											</Grid.Column>
											<Grid.Column>
												<div>
													<b>{data.sipharisGarne}</b>
												</div>
												<div>
													{data.nameThar}
													<DashedLangInput
														name="sipharisName"
														setFieldValue={setFieldValue}
														value={values.subName}
														error={errors.subName}
													/>
												</div>
												<div>
													{data.padh}
													<DashedLangInput
														name="sipharisDesignation"
														setFieldValue={setFieldValue}
														value={values.subDesignation}
														error={errors.subDesignation}
													/>
												</div>
												<div>
													{PlinthTechLang.sign}: <span className="ui input dashedForm-control" />
												</div>
												<div>
													{data.sipharisMiti}
													<CompactDashedLangDate
														name="sipharisDate"
														setFieldValue={setFieldValue}
														value={values.date}
														error={errors.date}
													/>
												</div>
											</Grid.Column>
										</Grid.Row>
									</Grid>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const DosrocharanKaryaSampannaPrabidhikPratibedanView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				/**
				 * @TODO
				 */
				api: api.DosrocharanPrabidhikView,
				objName: 'dosroPrabidhikView',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui checkbox', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByTagName', 'input', 'value'],
		}}
		render={(props) => <DosrocharanKaryaSampannaPrabidhikPratibedanViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanKaryaSampannaPrabidhikPratibedanView;
