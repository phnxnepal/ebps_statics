import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import * as Yup from 'yup';

import { GharSurjaminData } from '../../../../utils/data/GharSurjaminData';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues, surroundingMappingFlat } from '../../../../utils/dataUtils';
import api from '../../../../utils/api';
import { showToast, getUserRole } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { GenericApprovalFileView } from '../../../shared/file/GenericApprovalFileView';
import { FormUrlFull } from '../../../../utils/enums/url';
import { WARD_MASTER } from '../../../../utils/constants';
import LongFormDate from '../formComponents/LongFormDate';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { DashedSelect } from '../../../shared/Select';
import { getLocalStorage } from '../../../../utils/secureLS';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserTypeValueNepali } from '../../../../utils/functionUtils';
import { PrintSelectors } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';

const data = GharSurjaminData.kamalamai;
const schema = Yup.object().shape({
	tamelDate: validateNullableNepaliDate,
});

class GharNakshaSurjaminKamalamaiComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData, userData, permitData } = this.props;
		let erInfo = {
			erName: '',
			erDesignation: '',
			erDate: '',
		};
		if (getUserRole() === UserType.ENGINEER) {
			erInfo.erName = userData.userName;
			erInfo.erDesignation = getUserTypeValueNepali(userData.userType);
			erInfo.erDate = userData.info.joinDate;
		}
		const json_data = getJsonData(prevData);
		const wardOption = JSON.parse(getLocalStorage(WARD_MASTER)).map((ward) => {
			return { key: ward.id, value: ward.name, text: ward.name };
		});
		const initialValues = prepareMultiInitialValues(
			{
				obj: erInfo,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['surrounding'],
			},
			{ obj: json_data, reqFields: [] }
		);

		this.state = { initialValues, wardOption };
	}
	render() {
		const { initialValues, wardOption } = this.state;
		const { permitData, prevData, userData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, errors: reduxErrors } = this.props;
		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.gharnakshaSurjaminMuchulka}`, values, true);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div className="section-header">
									<h2 className="end-section">{userData.organization.name}</h2>
									<h3 className="underline end-section">{data.title}</h3>
								</div>
								<div style={{ textAlign: 'justify' }}>
									{data.likhitam}{' '}
									{userData.organization.name}
									{data.wardNo}
									{permitData.newWardNo}
									{data.shree} {permitData.applicantName.trim()}{data.le} {userData.organization.name} {permitData.newWardNo} {data.sabik1} {permitData.oldMunicipal}{' '}
									{data.sabikGabisa} {permitData.oldWardNo} {data.line4}
									{permitData.kittaNo}
									{data.sadakSlash}


									{permitData.landArea} {permitData.landAreaType} {data.ko}{' '}

									{permitData.surrounding &&
										permitData.surrounding.map((surr, idx) => (
											<React.Fragment key={idx}>
												{surroundingMappingFlat.find((fl) => fl.side === surr.side).value}

												<DashedLangInput value={permitData.surrounding[idx].sandhiyar} name={`surrounding.${idx}.sandhiyar`}  />
											</React.Fragment>
										))}{' '}
						
									{[...Array(4 - permitData.surrounding.length)].map((x, i) =>
											<>
												{data.side[i]}
												<DashedLangInput name={`surrounding.${i}.sandhiyar`}  />
											</>
										)}

									{data.charKilla}{' '}
									<DashedLangInput
										name="charkilla"
										setFieldValue={setFieldValue}
										value={values.charKilla}
										error={errors.charKilla}
									/>{' '}
									{data.nimitta} {permitData.kittaNo} {data.jibi} {permitData.landArea} {permitData.landAreaType}{data.ko} {data.koJagga}{' '}
									<DashedLangInput
										name="ownerType"
										setFieldValue={setFieldValue}
										value={values.ownerType}
										error={errors.ownerType}
									/>{' '}
									{data.end}
								</div>
								<br />
								<div>
									<div className="section-header margin-section">
										<h3 className="underline">{data.line8_1}</h3>
									</div>
									{data.number.map((num, index) => (
										<div className="left-align" key={index}>
											{num} {userData.organization.name}
											{data.wardNo}
											<DashedSelect name={`ward.${index}`} options={wardOption} />
											{data.line2}
											<DashedLangInput
												compact={true}
												name={`address.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `address.${index}`)}
												error={getIn(errors, `address.${index}`)}
											/>
											{data.basne}
											
											<DashedLangInput
												compact={true}
												name={`age.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `age.${index}`)}
												error={getIn(errors, `age.${index}`)}
											/>

											{data.palika}
											<DashedLangInput
												compact={true}
												name={`age.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `age.${index}`)}
												error={getIn(errors, `age.${index}`)}
											/>
											{data.koShree}
											<DashedLangInput
												name={`person.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `person.${index}`)}
												error={getIn(errors, `person.${index}`)}
											/>
											{data.data_4}
										</div>
									))}
									<br />
									<div>
										<SectionHeader>
											<h3 className="underline">{data.sambandhit}</h3>
										</SectionHeader>
										<div>
											{userData.organization.name} {data.wardNo}
											<DashedSelect name="wardSambandhit" options={wardOption} /> {data.basne}{' '}
											<DashedLangInput
												compact={true}
												name="ageSambandhit"
												setFieldValue={setFieldValue}
												value={values.ageSambandhit}
												error={errors.ageSambandhit}
											/>{' '}
											{data.ka}{' '}
											<DashedLangInput
												name="nameSambandhit"
												setFieldValue={setFieldValue}
												value={values.ageSambandhit}
												error={errors.ageSambandhit}
											/>{' '}
											{data.data_4}
										</div>
									</div>
									<br />
									<div>
										<SectionHeader>
											<h3 className="underline">{data.rohabar}</h3>
										</SectionHeader>
										<div>
											{userData.organization.name} {data.wardNo}
											<DashedSelect name="wardRohabar" options={wardOption} /> {data.ward}{' '}
											<DashedLangInput
												name="wardPosition"
												setFieldValue={setFieldValue}
												value={values.wardPosition}
												error={errors.wardPosition}
											/>{' '}
											{data.data_4}
										</div>
									</div>
									<br />
									<div>
										<SectionHeader>
											<h3 className="underline">{data.kamtamelGarne}</h3>
										</SectionHeader>
										<div>
											{userData.organization.name} {data.karyalayeKa}{' '}
											<DashedLangInput
												name="kamtamelName"
												setFieldValue={setFieldValue}
												value={values.kamtamelName}
												error={errors.kamtamelName}
											/>
										</div>
									</div>
									<br />
									<LongFormDate />
									<br />
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const GharNaksaApproveView = (props) => {
	return (
		<div ref={props.setRef}>
			<GenericApprovalFileView
				fileCategories={props.fileCategories}
				files={props.files}
				url={FormUrlFull.GHAR_NAKSA_SURJAMIN}
				prevData={checkError(props.prevData)}
			/>
		</div>
	);
};

const GharNakshaSurjaminKamalamaiView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: true,
			},
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param9: ['getElementsByClassName', PrintSelectors.DASHED_DROPDOWN],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={(props) =>
				<GharNakshaSurjaminKamalamaiComponent {...props} parentProps={parentProps} />
			// props.hasSavePermission ? (
			// 		<GharNakshaSurjaminKamalamaiComponent {...props} parentProps={parentProps} />
			// 	) : (
			// 		<GharNaksaApproveView {...props} parentProps={parentProps} />
			// )
		}
	/>
);

export default GharNakshaSurjaminKamalamaiView;
