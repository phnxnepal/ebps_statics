import React, { Component } from 'react';
import { Form, Table, Select } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import { floorMappingFlat, prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import { round, calculateTotal } from '../../../../utils/mathUtils';
import RajasowTableInput, {
	getBatabaranDastur,
	getSarsaphaiDastur,
	RajasowTable2Input,
	RajasowRateInput,
	RajasowUnitDropdownWithRelatedFields,
} from '../../../shared/RajasowTableInput';
import { isEmpty } from '../../../../utils/functionUtils';
import { isKankai, isKamalamai, isMechi, isBirtamod } from '../../../../utils/clientUtils';
import { getRajasowTippaniSchema } from '../../formValidationSchemas/rajasowDetailSchema';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SectionHeader } from '../../../uiComponents/Headers';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { NepaliNumberToWord } from '../../../../utils/nepaliAmount';
import TableInput from '../../../shared/TableInput';
import { floorData } from '../../../../utils/data/genericData';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { rajaswoFormLang } from '../../../../utils/data/rajaswoDetailLang';
import { hasDharauti, initDetails, getInitDetails, jTypeOptions } from '../../formUtils/rajasowUtils';
import { areaUnitOptions } from '../../../../utils/optionUtils';

const rajaswoForm = rajaswoFormLang;

class GenericRajasowTippaniForm extends Component {
	constructor(props) {
		super(props);

		let initVal = {};
		const { permitData, prevData, otherData } = this.props;
		const rajasowMaster = otherData.rajasowMaster;
		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		initVal = getJsonData(prevData);
		if (isEmpty(initVal) || !initVal.details) {
			initVal = {};

			initVal.details = [];
			floorArray
				.getFloorsWithUnit()
				.forEach((row, index) => (initVal.details[index] = getInitDetails(row, rajasowMaster, permitData.constructionType)));

			try {
				const detailsTotal = initVal.details.reduce((acc, row) => (acc += row.drAmount), 0);
				const batabaranDastur = getBatabaranDastur(detailsTotal);
				const sarasaphaiDastur = getSarsaphaiDastur(detailsTotal);

				initVal.totalAmt = isKamalamai
					? round(batabaranDastur + sarasaphaiDastur + detailsTotal)
					: isMechi || isBirtamod
					? round(detailsTotal)
					: round(detailsTotal + detailsTotal / 2);
				initVal.dharautiAmt = round(detailsTotal / 2);
				initVal.totalAmtBeforeDharauti = detailsTotal;
				initVal.batabaranDasturAmt = getBatabaranDastur(detailsTotal);
				initVal.sarsaphaiDasturAmt = getSarsaphaiDastur(detailsTotal);
				initVal.detailsTotal = detailsTotal;
			} catch {
				initVal.totalAmt = 0;
				initVal.dharautiAmt = 0;
				initVal.totalAmtBeforeDharauti = 0;
				initVal.batabaranDasturAmt = 0;
				initVal.sarsaphaiDasturAmt = 0;
				initVal.detailsTotal = 0;
			}
		}

		initVal.details &&
			initVal.details.forEach((row, index) => {
				if (isEmpty(initVal.hasJasta)) {
					initVal.hasJasta = [];
				}

				initVal.hasJasta[index] = !!(row.jrAmount || row.jArea);
			});

		initVal.details = initVal.details.sort((a, b) =>
			a.block !== undefined && b.block !== undefined ? (a.block < b.block ? -1 : 1) : a.floor < b.floor ? -1 : 1
		);

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					jAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					dAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					// allowableHeightUnit: DEFAULT_UNIT_LENGTH,
					// constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					// floorlengthUnit: DEFAULT_UNIT_LENGTH,
					// floorWidthUnit: DEFAULT_UNIT_LENGTH,
					// floorareaUnit: DEFAULT_UNIT_LENGTH,
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['floor', 'buildingJoinRoad'] },
			{
				obj: {
					...floorArray.getInitialValue(null, 'constructionHeight', floorArray.getSumOfHeights()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width'], formattedFloors),
				},
				reqFields: [],
			},
			{ obj: initVal, reqFields: [] }
		);

		// /**
		//  * @todo remove later
		//  */
		// if (isEmpty(initialValues.jAreaUnit)) initialValues.jAreaUnit = DEFAULT_UNIT_AREA;
		// if (isEmpty(initialValues.dAreaUnit)) initialValues.dAreaUnit = DEFAULT_UNIT_AREA;

		this.state = {
			floorArray,
			initialValues,
			rajasowMaster,
			constructionType: permitData.constructionType,
			schema: getRajasowTippaniSchema(floorArray.getFloorSchema),
		};
	}

	getJastaRate = (floor, type) => {
		const typeValue = type === jTypeOptions[0].value ? 'J' : 'O';
		const currentJastaRow = this.state.rajasowMaster.find(
			(mRow) => mRow.floor === floor && mRow.floorType === typeValue && mRow.constructionType === String(this.state.constructionType)
		);
		if (currentJastaRow) return currentJastaRow.dasturRate;
		else return 0;
	};

	render() {
		const { floorArray, initialValues, schema } = this.state;
		const { permitData, prevData, hasDeletePermission, isSaveDisabled, hasSavePermission, formUrl } = this.props;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);

						values.applicationNo = permitData.applicantNo;
						values.details = values.details.filter((row) => !isEmpty(row));
						values.details &&
							values.details.forEach((row, index) => {
								if (row) {
									initDetails(row);
									row.totalAmount = calculateTotal(row.drAmount, row.depositAmount, row.jrAmount);
									if (row.floorText) delete row.floorText;
								} else {
									// values.details[index] = {};
									// values.details[index].dArea = 0;
									// values.details[index].drAmount = 0;
									// values.details[index].drRate = 0;
								}
							});

						values.error && delete values.error;

						try {
							await this.props.postAction(this.props.postApi, values, true);

							// console.log('response -- after post', response);
							window.scrollTo(0, 0);
							actions.setSubmitting(false);
							// console.log(this.props);

							if (this.props.success && this.props.success.success) {
								// showToast("Your data has been successfully");
								// this.props.parentProps.history.push(
								//   getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, isSubmitting, handleChange, values, setFieldValue, errors, validateForm }) => {
						const hasDhrt = hasDharauti(values.details);
						const dharautiClass = hasDhrt ? undefined : 'remove-on-print';
						return (
							<Form loading={isSubmitting}>
								<div ref={this.props.setRef}>
									<SectionHeader>
										<h2 className="top-margin">{rajaswoForm.heading}</h2>
									</SectionHeader>

									<div>
										<Table celled structured size="small" compact="very">
											<Table.Header>
												<HeaderFirstRow dharautiClass={dharautiClass} />
												<Table.Row>
													{/* Dhalan */}
													<Table.HeaderCell rowSpan="2" width={4}>
														{rajaswoForm.table_head.thead_2}
														{/* {' ( '} */}
														{/* <Dropdown
																// defaultValue="वर्ग मिटर"
																options={areaUnitOptions}
																name="dAreaUnit"
																onChange={(e, { value }) => setFieldValue('dAreaUnit', value)}
																value={values['dAreaUnit']}
															/> */}
														{/* {' )'} */}
														<RajasowUnitDropdownWithRelatedFields
															options={areaUnitOptions}
															unitName="dAreaUnit"
															areaFields={['drAmount', 'jrAmount', 'depositAmount', 'jDepositAmount', 'detailsTotal']}
															relatedFields={[
																'detailsTotal',
																...floorArray.getAllCustomsFields('', 'details', [
																	'dArea',
																	'jArea',
																	'drAmount',
																	'jrAmount',
																	'depositAmount',
																	'jDepositAmount',
																]),
															]}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell className={dharautiClass} colSpan="2">
														{rajaswoForm.table_head.thead_3}
													</Table.HeaderCell>
													{/* Other */}
													{/* <Table.HeaderCell rowSpan="2" width={3}>
														{rajaswoForm.table_head.thead_4} {' ( '}
														<Dropdown
															// defaultValue="वर्ग मिटर"
															options={areaOptions}
															name="jAreaUnit"
															onChange={(e, { value }) =>
																setFieldValue('jAreaUnit', value)
															}
															value={values['jAreaUnit']}
														/>
														{' )'}
													</Table.HeaderCell> */}
													{/* <Table.HeaderCell colSpan="2">
														{rajaswoForm.table_head.thead_5}
													</Table.HeaderCell> */}
													{/* Dhazrauti */}
												</Table.Row>
												<HeaderThirdRow dharautiClass={dharautiClass} />
											</Table.Header>
											<Table.Body>
												{initialValues.details
													// .filter(rw => rw.floor <= 2)
													.map((row, index) => {
														const isHeightRelevantFloor = row.floor < 11;
														return [
															<Table.Row key={index}>
																<Table.Cell>
																	<div>
																		{floorMappingFlat.find((fl) => row.floor === fl.floor).value}{' '}
																		{row.block && `${floorData.block} ${row.block}`}
																	</div>
																	{row.floor > 0 && (
																		<div
																			className="ui checkbox"
																			style={{
																				marginRight: 10,
																			}}
																		>
																			<input
																				type="checkbox"
																				name={`hasJasta.${index}`}
																				value={getIn(values, `hasJasta.${index}`)}
																				defaultChecked={getIn(values, `hasJasta.${index}`)}
																				onChange={(e) => {
																					setFieldValue(`details.${index}.jType`, jTypeOptions[0].value);
																					handleChange(e);
																				}}
																			/>
																			<label>{rajaswoForm.table_jasta}</label>
																		</div>
																	)}
																</Table.Cell>
																{/* Dhalan */}
																<Table.Cell>
																	<RajasowTableInput
																		name={`details.${index}.dArea`}
																		index={index}
																		values={values}
																		mulFieldName={`details.${index}.drRate`}
																		amountFieldName={`details.${index}.drAmount`}
																		error={getIn(errors, `details.${index}.dArea`)}
																		floorFieldName={`details.${index}.floor`}
																		setFieldValue={setFieldValue}
																		floor={row.floor}
																	/>
																</Table.Cell>
																<Table.Cell>
																	<RajasowRateInput
																		noZero={false}
																		name={`details.${index}.drRate`}
																		index={index}
																		values={values}
																		mulFieldName={`details.${index}.dArea`}
																		amountFieldName={`details.${index}.drAmount`}
																		error={getIn(errors, `details.${index}.drRate`)}
																		setFieldValue={setFieldValue}
																		isHeightRelevantFloor={isHeightRelevantFloor}
																	/>
																</Table.Cell>
																<Table.Cell>
																	<TableInput
																		readOnly={true}
																		placeholder="0.0"
																		name={`details.${index}.drAmount`}
																		value={getIn(values, `details.${index}.drAmount`)}
																		error={getIn(errors, `details.${index}.drAmount`)}
																	/>
																</Table.Cell>

																{/* Dharauti */}
																{/* <Table.Cell className={dharautiClass}>
																		<RajasowTableInput
																			name={`details.${index}.depositRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.dArea`}
																			amountFieldName={`details.${index}.depositAmount`}
																			error={getIn(errors, `details.${index}.depositRate`)}
																			setFieldValue={setFieldValue}
																		/>
																	</Table.Cell>
																	<Table.Cell className={dharautiClass}>
																		<TableInput
																			readOnly={true}
																			placeholder="0.00"
																			name={`details.${index}.depositAmount`}
																			value={getIn(values, `details.${index}.depositAmount`)}
																			error={getIn(errors, `details.${index}.depositAmount`)}
																			// onChange={handleChange}
																		/>
																	</Table.Cell> */}
															</Table.Row>,
															getIn(values, `hasJasta.${index}`) && (
																<Table.Row key={`jasta-${index}`}>
																	<Table.Cell>
																		<Form.Field inline>
																			<span>
																				{floorMappingFlat.find((fl) => row.floor === fl.floor).value}{' '}
																				{row.block && `${floorData.block} ${row.block}`}
																			</span>{' '}
																			{/* <Label basic> */}
																			<Select
																				className="fit-content-select"
																				// defaultValue={jTypeOptions[0].text}
																				options={jTypeOptions}
																				name={`details.${index}.jType`}
																				onChange={(e, { value }) => {
																					const jastaRate = this.getJastaRate(row.floor, value);
																					const jArea = getIn(values, `details.${index}.jArea`) || 0;
																					setFieldValue(`details.${index}.jType`, value);
																					setFieldValue(`details.${index}.jrRate`, jastaRate);
																					setFieldValue(
																						`details.${index}.jrAmount`,
																						round(jArea * jastaRate)
																					);
																				}}
																				value={getIn(values, `details.${index}.jType`)}
																			/>
																			{/* </Label> */}
																		</Form.Field>
																	</Table.Cell>
																	{/* Other */}
																	<Table.Cell>
																		<Form.Field>
																			{/* <Input labelPosition="right"> */}
																			{/* <TableInput
																				name={`details.${index}.jArea`}
																				value={getIn(values, `details.${index}.jArea`)}
																				onChange={e => {
																					setFieldValue(
																						`details.${index}.jArea`,
																						e.target.value
																					);
																					if (
																						!values[`details.${index}.floor`]
																					) {
																						setFieldValue(
																							`details.${index}.floor`,
																							row.floor
																						);
																					}
																					setFieldValue(
																						`details.${index}.jrAmount`,
																						calculateAmount(
																							'jrRate',
																							e.target.value,
																							index,
																							values,
																							'details'
																						),
																						false
																					);
																				}}
																				error={getIn(errors, `details.${index}.jArea`)}
																			/> */}
																			<RajasowTableInput
																				name={`details.${index}.jArea`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jrRate`}
																				amountFieldName={`details.${index}.jrAmount`}
																				error={getIn(errors, `details.${index}.jArea`)}
																				floorFieldName={`details.${index}.floor`}
																				setFieldValue={setFieldValue}
																				floor={row.floor}
																			/>
																			{/* </Input> */}
																		</Form.Field>
																	</Table.Cell>
																	<Table.Cell>
																		{/* <TableInput
																		placeholder="0.00"
																		name={`details.${index}.jrRate`}
																		value={getIn(values, `details.${index}.jrRate`)}
																		onChange={e => {
																			setFieldValue(
																				`details.${index}.jrRate`,
																				e.target.value
																			);
																			setFieldValue(
																				`details.${index}.jrAmount`,
																				calculateAmount(
																					'jArea',
																					e.target.value,
																					index,
																					values,
																					'details'
																				),
																				false
																			);
																		}}
																		error={getIn(errors, `details.${index}.jrRate`)}

																	/> */}
																		<RajasowRateInput
																			noZero={false}
																			name={`details.${index}.jrRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.jArea`}
																			amountFieldName={`details.${index}.jrAmount`}
																			error={getIn(errors, `details.${index}.jrRate`)}
																			setFieldValue={setFieldValue}
																			isHeightRelevantFloor={isHeightRelevantFloor}
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInput
																			readOnly={true}
																			placeholder="0.00"
																			name={`details.${index}.jrAmount`}
																			error={getIn(errors, `details.${index}.jrAmount`)}
																			value={getIn(values, `details.${index}.jrAmount`)}
																			// onChange={handleChange}
																		/>
																	</Table.Cell>

																	{/* Dharauti */}
																	{/* <Table.Cell className={dharautiClass}>
																			<RajasowTableInput
																				name={`details.${index}.jDepositRate`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jArea`}
																				amountFieldName={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositRate`)}
																				setFieldValue={setFieldValue}
																			/>
																		</Table.Cell>
																		<Table.Cell className={dharautiClass}>
																			<TableInput
																				placeholder="0.00"
																				name={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositAmount`)}
																				value={getIn(values, `details.${index}.jDepositAmount`)}
																				onChange={handleChange}
																			/>
																		</Table.Cell> */}
																</Table.Row>
															),
														];
													})}
												{/* Dynamic additional rows */}
												{/* ------------------------------------ */}

												<Table.Row>
													<Table.Cell className="print-col-span" data-colspan={hasDhrt ? '3' : '3'} colSpan={'3'}>
														{rajaswoForm.table_total}
													</Table.Cell>
													<Table.Cell>
														<TableInput
															placeholder="0.00"
															name="detailsTotal"
															onBlur={() => {
																if (values.details) {
																	const amt = values.details.reduce((amount, next) => {
																		if (next) {
																			return (
																				parseFloat(amount) +
																				calculateTotal(
																					next.drAmount,
																					next.jrAmount,
																					next.depositAmount,
																					next.jDepositAmount
																				)
																			);
																		} else {
																			return 0.0;
																		}
																	}, 0.0);
																	setFieldValue('detailsTotal', round(amt));
																}
															}}
															value={values.detailsTotal}
															error={errors.detailsTotal}
															onChange={handleChange}
														/>
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
									</div>
									<Table celled structured size="small" compact="very">
										<Table.Body>
											<Table.Row>
												<Table.Cell width={4}>{rajaswoForm.table_foot.Child_1}</Table.Cell>
												<Table.Cell width={6}>
													<RajasowTable2Input
														name="formAmt"
														values={values}
														setFieldValue={setFieldValue}
														error={errors.formAmt}
													/>
												</Table.Cell>
												<Table.Cell rowSpan={isKankai ? '9' : '8'}>{rajaswoForm.table_foot.Child_6}</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.Child_2}</Table.Cell>
												<Table.Cell>
													<RajasowTable2Input
														name="applicationAmt"
														values={values}
														setFieldValue={setFieldValue}
														error={errors.applicationAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.Child_3}</Table.Cell>
												<Table.Cell>
													<RajasowTable2Input
														name="aminAmt"
														values={values}
														setFieldValue={setFieldValue}
														error={errors.aminAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.Child_4}</Table.Cell>
												<Table.Cell>
													<RajasowTable2Input
														name="anyeAmt"
														values={values}
														setFieldValue={setFieldValue}
														error={errors.anyeAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.jaribanaDastur}</Table.Cell>
												<Table.Cell>
													<RajasowTable2Input
														name="jaribanaDasturAmt"
														values={values}
														setFieldValue={setFieldValue}
														error={errors.jaribanaDasturAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.sarsaphaiDastur}</Table.Cell>
												<Table.Cell>
													<TableInput
														readOnly={true}
														placeholder="0.00"
														name="sarsaphaiDasturAmt"
														value={values.sarsaphaiDasturAmt}
														error={errors.sarsaphaiDasturAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.batabaranDastur}</Table.Cell>
												<Table.Cell>
													<TableInput
														readOnly={true}
														placeholder="0.00"
														name="batabaranDasturAmt"
														value={values.batabaranDasturAmt}
														error={errors.batabaranDasturAmt}
													/>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell>{rajaswoForm.table_foot.totalAmt}</Table.Cell>
												<Table.Cell>
													<TableInput
														readOnly={true}
														placeholder="0.00"
														name="totalAmt"
														value={values.totalAmt}
														error={errors.totalAmt}
													/>
												</Table.Cell>
											</Table.Row>
										</Table.Body>
									</Table>
									{values.totalAmt && values.totalAmt > 0 ? (
										<div>
											{rajaswoForm.formSecond.amountInWordsTotal}: {NepaliNumberToWord.getNepaliWord(values.totalAmt)}
										</div>
									) : null}
								</div>

								<br />
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const HeaderFirstRow = React.memo(({ dharautiClass }) => (
	<Table.Row>
		{/* sn */}
		<Table.HeaderCell rowSpan="3">{rajaswoForm.table_head.thead_1}</Table.HeaderCell>
		<Table.HeaderCell colSpan="3">{rajaswoForm.table_head.thead_type.dhalan}</Table.HeaderCell>
		{/* <Table.HeaderCell colSpan="3" width={3}>
			{rajaswoForm.table_head.thead_type.other}
		</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass} rowSpan="2" colSpan="2">
			{rajaswoForm.table_head.thead_6}
		</Table.HeaderCell> */}
	</Table.Row>
));

const HeaderThirdRow = React.memo(({ dharautiClass }) => (
	<Table.Row>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_1}</Table.HeaderCell>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_2}</Table.HeaderCell>
		{/* <Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_1}
		</Table.HeaderCell>
		<Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_2}
		</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_1}</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_2}</Table.HeaderCell> */}
	</Table.Row>
));

export default GenericRajasowTippaniForm;
