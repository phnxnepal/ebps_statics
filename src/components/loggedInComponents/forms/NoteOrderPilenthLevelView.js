import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { PrintParams, PrintSelectors } from '../../../utils/printUtils';
// import merge from 'lodash/merge';
import { noteOrderPilenthLang } from '../../../utils/data/noteOrderPilenthLang';
import { DashedLangDateField, DashedDateField } from './../../shared/DateField';
import { getCurrentDate } from './../../../utils/dateUtils';
import { DashedLangInput, DashedNormalInput } from '../../shared/DashedFormInput';
import api from '../../../utils/api';
import { FloorBlockArray } from '../../../utils/floorUtils';
import merge from 'lodash/merge';
import * as Yup from 'yup';
import { showToast, getUserRole, getUserTypeValueNepali } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, surroundingMappingFlat } from '../../../utils/dataUtils';
import { translateDate, translateEngToNep } from '../../../utils/langUtils';
import { DashedUnitInput, DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from './../../shared/EbpsUnitLabelValue';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { naksaData } from '../../../utils/data/nakxsaData';
import { validateNepaliDate, validateNormalNepaliDateWithRange } from '../../../utils/validationUtils';
import { getConstructionTypeText } from '../../../utils/enums/constructionType';
import { UserType } from '../../../utils/userTypeUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { FlexSingleRight, FlexSpaceBetween } from '../../uiComponents/FlexDivs';
import { checkClient, Client, isBirtamod, isInaruwa, isKamalamai } from '../../../utils/clientUtils';
import { PatraSankhya } from './formComponents/PatraSankhyaAndDate';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';
import { ShreeDropdown } from './formComponents/ShreeDropdown';
import { shreeOptions } from '../../../utils/optionUtils';
import { SelectInput } from '../../shared/Select';
import { isStringEmpty } from '../../../utils/stringUtils';
import { DetailedSignature, getDesignations } from './formComponents/FooterSignature';
import { footerInputSignature } from '../../../utils/data/genericFormData';
import { getApproveByObject } from '../../../utils/formUtils';

const pilenthLang = noteOrderPilenthLang;
const sundarData = noteOrderPilenthLang.sundarHaraicha;

const opts = [
	{ key: 'shri', value: 'श्री ', text: 'श्री ' },
	{ key: 'shrimati', value: 'श्रीमती', text: 'श्रीमती' },
	{ key: 'sushri', value: 'सुश्री ', text: 'सुश्री ' },
];
const hamroOpt = [
	{ key: 1, value: 'मेरो', text: 'मेरो' },
	{ key: 2, value: 'हाम्रो', text: 'हाम्रो' },
];
// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];
const opt = [
	{ key: 'ma', value: 'बनेको ', text: 'बनेको ' },
	{ key: 'banne', value: 'बन्ने', text: 'बन्ने' },
];
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

const buildInput = 'भवन निर्माण अस्थाई स्वीकृति';
const bamojimInput = 'घर';

const getMs = (value) => {
	const ret = opt.filter((row) => row.value === value);
	return ret[0] ? ret[0].key : 'ma';
};
let data = naksaData.structureDesign.table.rows.row5.input_1;
const floorArr = [
	{
		data: data[0],
		value: data[0],
	},
	{
		data: data[10],
		value: '10',
	},
];

data.slice(1, 10).forEach((row, index) =>
	floorArr.push({
		data: row,
		value: data[index + 2],
	})
);
const NoteOrderPlinthSchema = Yup.object().shape(
	Object.assign({
		noteOrderDate: validateNepaliDate,
		miiti: validateNepaliDate,
		prititonerDate: validateNepaliDate,
	})
);

const sundarHaraichaSchema = Yup.object().shape({
	applicantDateBS: validateNormalNepaliDateWithRange,
	Sangyardate: validateNepaliDate,
	surjaminDate: validateNepaliDate,
});
class NoteOrderPilenthLevelViewComponent extends Component {
	constructor(props) {
		super(props);
		const { orgCode, userData, permitData, otherData, prevData, enterByUser, DEFAULT_UNIT_LENGTH } = props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const rujuData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const prabhidikPratibedhan = getJsonData(otherData.prabhidikPratibedhan);
		const gharNaksaSurjamin = getJsonData(otherData.gharNaksaSurjamin);
		const mapCheckReport = otherData.mapCheckReport;
		const maptech = getJsonData(otherData.maptech);
		const surjaminMuchulka = getJsonData(otherData.surjaminMuchulka);
		const notice15 = getJsonData(otherData.notice15);
		// const permitEdit = getJsonData(this.props.otherData.permitEdit)

		let serInfo = {
			subName: '',
			subDesignation: '',
		};

		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;

		if (getUserRole() === UserType.SUB_ENGINEER) {
			serInfo.subName = isStringEmpty(serInfo.subName) ? userData.userName : serInfo.subName;
			serInfo.subDesignation = isStringEmpty(serInfo.subDesignation) ? getUserTypeValueNepali(userData.userType) : serInfo.subDesignation;
		}

		let initialValues = prepareMultiInitialValues(
			{
				obj: {
					purbaSandhiyarUnit: 'METRE',
					paschimSandhiyarUnit: 'METRE',
					uttarSandhiyarUnit: 'METRE',
					dakshinSandhiyarUnit: 'METRE',
					lambaiUnit: 'METRE',
					chaudaiUnit: 'METRE',
					uchaiUnit: 'METRE',
					noteOrderDate: getCurrentDate(true),
					prititonerDate: getCurrentDate(true),
					miiti: translateDate(permitData.applicantDateBS),
					namedUnit: 'METRE',
					sideUnit: DEFAULT_UNIT_LENGTH,
					floor: { floorUnit: DEFAULT_UNIT_LENGTH },
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{
				obj: prabhidikPratibedhan,
				reqFields: ['namedAdhikar', 'namedUnit', 'floor'],
			},
			{
				obj: notice15,
				reqFields: ['Sangyardate'],
			},
			{
				obj: surjaminMuchulka,
				reqFields: ['surjaminDate'],
			},
			{
				obj: permitData,
				reqFields: ['constructionType', 'surrounding', 'applicantDateBS'],
			},
			{
				obj: {
					newBuild: buildInput,
					newBamojim: bamojimInput,
					saal: permitData && permitData.applicantDateBS && translateEngToNep(permitData.applicantDateBS.substring(0, 4)),
					selectShri: opts[0].value,
					shree: shreeOptions[0].value,
					mero: hamroOpt[0].value,
					new: opt[0].value,
				},
				reqFields: [],
			},
			{
				obj: gharNaksaSurjamin,
				reqFields: ['dakshinSandhiyar', 'uttarSandhiyar', 'paschimSandhiyar', 'purbaSandhiyar'],
			},
			{
				obj: mapCheckReport,
				reqFields: ['details'],
			},
			{ obj: maptech, reqFields: ['buildingDetailfloor'] },
			{
				obj: {
					erName: rujuData.name,
					erDesignation: rujuData.designation,
					erSignature: rujuData.signature,
					erDate: rujuData.date,
					chiefName: swikritData.name,
					chiefDesignation: swikritData.designation,
					chiefSignature: swikritData.signature,
					chiefDate: swikritData.date,
				},
				reqFields: [],
			},
			/** @todo confirm this */
			{
				obj: getJsonData(prevData),
				reqFields: [],
			}
		);

		initialValues.constructionType = getConstructionTypeText(initialValues.constructionType);

		this.state = {
			isSundarHaraicha: checkClient(orgCode, Client.SUNDARHARAICHA),
			initialValues,
			designations: getDesignations('prititonerDate'),
		};


		const prabhidik = getJsonData(otherData.prabhidik);
		const dosrocharan = getJsonData(otherData.dosrocharan);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['floor', 'photo'],
			},
			{
				obj: {
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
				},
				reqFields: [],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfAreas()),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'purposeOfConstruction', 'buildingHeight'],
			},
			{
				obj: prabhidik,
				reqFields: [
					'constructionType',
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'requiredDistance',
					'sadakAdhikarUnit',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'elecVolt',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{
				obj: dosrocharan,
				reqFields: ['roomCount', 'windowCount', 'doorCount', 'shutterCount'],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		initialValues = merge(initialValues, initialValues1);

	}

	render() {
		// let intialValues1 = {};

		const {
			permitData,
			handleChange,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;
		const { initialValues, isSundarHaraicha, designations } = this.state;
		const udata = userData;
		const user_info = this.props.userData;

		// console.log("x",getIn(mapCheckReport.details[5].designData))

		// if (isStringEmpty(intialValues.date)) {
		//     intialValues.date = getCurrentDate(true);
		// }

		// const initVal = merge(intialValues, intialValues1);

		// intialValues.constructionType = JSON.parse( prevData.jsonData).constructionType
		return (
			<div>
				<Formik
					initialValues={initialValues}
					validationSchema={isSundarHaraicha ? sundarHaraichaSchema : NoteOrderPlinthSchema}
					onSubmit={async (values, actions) => {
						console.log(values, actions)
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.noteorderPilengthLevel}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								// 	getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

							<div ref={this.props.setRef} className="print-small-font">
								<LetterHeadFlex userInfo={user_info} />

								{isSundarHaraicha ? (
									<>
										<NoteOrderSubHeadingSundarHaraicha setFieldValue={setFieldValue} values={values} errors={errors} />
										<NoteOrderPlinthBodySundarHaraicha
											setFieldValue={setFieldValue}
											values={values}
											errors={errors}
											permitData={permitData}
											udata={udata}
										/>
									</>
								) : isInaruwa ? (
									<>
										<NoteOrderSubHeadingSundarInaruwa />
										<NoteOrderPlinthBodyInaruwa
											setFieldValue={setFieldValue}
											values={values}
											errors={errors}
											permitData={permitData}
											udata={udata}
										/>
									</>
								) : isKamalamai ? (
									<>
										<NoteOrderSubHeadingSundarKamalamai setFieldValue={setFieldValue} values={values} errors={errors} />
										<NoteOrderPlinthBodyKamalamai
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											values={values}
											errors={errors}
											permitData={permitData}
											udata={udata}
											initialValue={initialValues}
										/>
									</>
								) : (
									<>
										<NoteOrderSubHeading setFieldValue={setFieldValue} values={values} errors={errors} />
										<NoteOrderPlinthBody
											setFieldValue={setFieldValue}
											values={values}
											errors={errors}
											permitData={permitData}
											udata={udata}
										/>
									</>
								)}
								{isSundarHaraicha ? (
									<div>
										<br />
										<div className="section-header">
											<p className="underline left-align normal">{sundarData.sandhiyarHaru}</p>
										</div>

										{surroundingMappingFlat.map((surr) => {
											let dataSurr = {};
											// let index = 0;
											if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
												dataSurr = values.surrounding.find((row) => row.side === surr.side) || {};
												// index = values.surrounding.indexOf(dataSurr);
											}
											return (
												<div key={surr.side}>
													{surr.value}: {dataSurr && dataSurr.sandhiyar}
												</div>
											);
										})}
									</div>
								) : isInaruwa ? (
									<></>
								) : isBirtamod ? (
									<FlexSpaceBetween>
										{designations.map((designation, index) => (
											<div key={index} style={{ textAlign: 'left' }}>
												<p>{designation.label}</p>
												<DetailedSignature
													setFieldValue={setFieldValue}
													values={values}
													errors={errors}
													designation={designation}
												>
													<DetailedSignature.Signature
														label={footerInputSignature.sahi}
														showSignature={useSignatureImage}
													/>
													<DetailedSignature.Name />
													<DetailedSignature.Designation />
													<DetailedSignature.Date />
												</DetailedSignature>
											</div>
										))}
									</FlexSpaceBetween>
								) : isKamalamai ? null : (
									<FlexSingleRight>
										<div>{pilenthLang.content.prititoner}:</div>
										<div>
											{pilenthLang.content.name}:
											<DashedLangInput
												name="subName"
												setFieldValue={setFieldValue}
												value={values.subName}
												error={errors.subName}
											/>
										</div>
										<div>
											{pilenthLang.content.position}:
											<DashedLangInput
												name="subDesignation"
												setFieldValue={setFieldValue}
												value={values.subDesignation}
												error={errors.subDesignation}
											/>
										</div>
										<div>
											{pilenthLang.date}:
											<DashedLangDateField
												name="prititonerDate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.prititonerDate}
												error={errors.prititonerDate}
											/>
										</div>
										<div>
											{pilenthLang.content.sign}:
											<span className="ui input dashedForm-control" />
										</div>
									</FlexSingleRight>
								)}
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const NoteOrderPlinthBody = ({ setFieldValue, udata, permitData, values, errors }) => {
	return (
		<div>
			{' '}
			<div className="section-header margin-section">
				<h3 className="underline">{pilenthLang.subject}</h3>
			</div>
			<div className="mainTipandi-content" style={{ textAlign: 'justify' }}>
				<span>{pilenthLang.content.content1}</span>
				<br />
				{pilenthLang.content.content2}
				{udata.organization.address}
				{', '}
				{udata.organization.name}, {pilenthLang.content.content3} {permitData.nibedakTol} {pilenthLang.content.content4}{' '}
				<Select name="selectShri" placeholder="श्री/श्रीमती/सुश्री" value={values.selectShri} options={opts} /> {permitData.nibedakName}
				{pilenthLang.content.content5}
				{permitData.applicantName}
				{pilenthLang.content.content6}
				{permitData.oldMunicipal}
				{pilenthLang.content.content3}
				{permitData.oldWardNo}
				{pilenthLang.content.content7}
				{udata.organization.name}
				{pilenthLang.content.content3}
				{permitData.newWardNo}
				{pilenthLang.content.content8}
				{permitData.kittaNo}
				{pilenthLang.content.content9}
				{permitData.landArea} {permitData.landAreaType}
				{pilenthLang.content.content10}
				{permitData.buildingJoinRoad}
				<DashedUnitInput name="namedAdhikar" unitName="namedUnit" label={pilenthLang.content.content11} />
				{pilenthLang.content.content12}
				{permitData.surrounding &&
					permitData.surrounding.map((surr, idx) => (
						<>
							{surroundingMappingFlat.find((fl) => fl.side === surr.side).value}
							<DashedUnitInput name={`surrounding.${idx}.feet`} unitName={`surrounding.${idx}.sideUnit`} />
						</>
					))}
				{pilenthLang.content.content13}
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.width', 'floor.height', 'floor.area']}
					name="floor.length"
					label={pilenthLang.content.length1}
					unitName="floor.floorUnit"
				/>
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.height', 'floor.area']}
					name="floor.width"
					label={pilenthLang.content.length2}
					unitName="floor.floorUnit"
				/>
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.width', 'floor.area']}
					name="floor.height"
					label={pilenthLang.content.length3}
					unitName="floor.floorUnit"
				/>
				{/* <DashedLangInput
												name="details.5.designData"
												className="dashedForm-control"
												setFieldValue={setFieldValue}
												value={values.buildingDetailfloor}
												// error={errors.details.designData[5]}
											/> */}
				{floorArr.find((row) => row.data === getIn(values, 'details.5.designData')) &&
					floorArr.find((row) => row.data === getIn(values, 'details.5.designData')).value}
				{pilenthLang.content.length4}, {pilenthLang.content.content9}
				<DashedAreaInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.width', 'floor.height']}
					name="floor.area"
					unitName="floor.floorUnit"
					squareOptions={squareUnitOptions}
				/>
				{pilenthLang.content.content15}
				<DashedLangInput name="saal" className="dashedForm-control" setFieldValue={setFieldValue} value={values.saal} error={errors.saal} />
				{pilenthLang.content.content16}
				<Select
					options={opt}
					name="new"
					onChange={(e, { value }) => {
						setFieldValue('new', value);
					}}
					value={values.new}
					placeholder="बनेको / बन्ने"
				/>{' '}
				<DashedLangInput
					name="constructionType"
					setFieldValue={setFieldValue}
					value={values.constructionType}
					error={errors.constructionType}
				/>
				{pilenthLang.content.content17}
				<DashedLangDateField
					name="miiti"
					setFieldValue={setFieldValue}
					value={values.miiti}
					error={errors.miiti}
					inline={true}
					className="dashedForm-control"
				/>
				{pilenthLang.content.content18}
				<DashedLangInput name=" juii" className="dashedForm-control" setFieldValue={setFieldValue} value={values.juii} error={errors.juii} />
				<DashedUnitInput name="namedAdhikar" unitName="namedUnit" label={pilenthLang.content.content19} />
				{pilenthLang.content.content20}
				<DashedLangInput
					name="newBamojim"
					className="dashedForm-control"
					setFieldValue={setFieldValue}
					value={values.newBamojim}
					error={errors.newBamojim}
				/>
				{pilenthLang[getMs(values['new'])].form_chu}
				{pilenthLang.content.content21}
				<DashedLangInput
					name="newBuild"
					className="dashedForm-control"
					setFieldValue={setFieldValue}
					value={values.newBuild}
					error={errors.newBuild}
				/>
				{pilenthLang.content.content22}
				<br />
				<br />
			</div>
		</div>
	);
};
const NoteOrderPlinthBodyInaruwa = ({ setFieldValue, udata, permitData, values, errors }) => {
	return (
		<div>
			{' '}
			<div className="section-header margin-section">
				<h3 className="underline">{pilenthLang.subject}</h3>
			</div>
			<div className="mainTipandi-content" style={{ textAlign: 'justify' }}>
				<br />
				{udata.organization.name}
				{pilenthLang.content.content3} {permitData.nibedakTol} {pilenthLang.content.content4} <ShreeDropdown name="shree" />
				{permitData.nibedakName}
				{pilenthLang.content.content5}
				<div className="select-forForm" style={{ display: 'inline-block' }}>
					<SelectInput options={hamroOpt} name="mero" />
				</div>
				{pilenthLang.content.content6}
				{permitData.oldMunicipal}
				{pilenthLang.inaruwa.haalWard}
				{permitData.newWardNo}
				{pilenthLang.inaruwa.ko}
				{permitData.sadak}
				{pilenthLang.inaruwa.kitta}
				{permitData.kittaNo}
				{pilenthLang.inaruwa.area}
				{permitData.landArea} {permitData.landAreaType}
				{pilenthLang.content.content10}
				{permitData.buildingJoinRoad}
				{pilenthLang.content.content11}
				<DashedUnitInput name="namedAdhikar" unitName="namedUnit" label={pilenthLang.content.content11} />
				{pilenthLang.content.content12}
				{permitData.surrounding &&
					permitData.surrounding.map((surr, idx) => (
						<>
							{surroundingMappingFlat.find((fl) => fl.side === surr.side).value}
							<DashedUnitInput name={`surrounding.${idx}.feet`} unitName={`surrounding.${idx}.sideUnit`} />
						</>
					))}
				{pilenthLang.content.content13}
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.width', 'floor.height', 'floor.area']}
					name="floor.length"
					label={pilenthLang.content.length1}
					unitName="floorUnit"
				/>
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.height', 'floor.area']}
					name="floor.width"
					label={pilenthLang.content.length2}
					unitName="floorUnit"
				/>
				<DashedLengthInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.width', 'floor.area']}
					name="floor.height"
					label={pilenthLang.content.length3}
					unitName="floorUnit"
				/>
				{floorArr.find((row) => row.data === getIn(values, 'details.5.designData')) &&
					floorArr.find((row) => row.data === getIn(values, 'details.5.designData')).value}
				{pilenthLang.content.length4}, {pilenthLang.content.content9}
				<DashedAreaInputWithRelatedUnits
					relatedFields={['floor.length', 'floor.width', 'floor.height']}
					name="floor.area"
					unitName="floorUnit"
					squareOptions={squareUnitOptions}
				/>
				{pilenthLang.content.content15}
				<DashedLangInput name="saal" className="dashedForm-control" setFieldValue={setFieldValue} value={values.saal} error={errors.saal} />
				{pilenthLang.content.content16}
				<div className="select-forForm" style={{ display: 'inline-block' }}>
					<SelectInput options={opt} name="new" />
				</div>{' '}
				<DashedLangInput
					name="constructionType"
					setFieldValue={setFieldValue}
					value={values.constructionType}
					error={errors.constructionType}
				/>
				{pilenthLang.content.content17}
				<DashedLangDateField
					name="miiti"
					setFieldValue={setFieldValue}
					value={values.miiti}
					error={errors.miiti}
					inline={true}
					className="dashedForm-control"
				/>
				{pilenthLang.inaruwa.content_1}
				<br />
				<br />
			</div>
		</div>
	);
};


const NoteOrderPlinthBodyKamalamai = ({ setFieldValue, udata, handleChange, permitData, values, errors, initialValue }) => {
	const floorArray = new FloorBlockArray(permitData.floor);
	return (
		<div>
			{udata.organization.name}
			{pilenthLang.kamalamai.nagarpalika}
			{permitData.newWardNo}
			{pilenthLang.kamalamai.basne}
			{permitData.applicantName}
			{pilenthLang.kamalamai.content1}
			{permitData.newWardNo}

			{pilenthLang.kamalamai.shabik}
			<DashedLangInput
				name="oldMunicipal"
				setFieldValue={setFieldValue}
				value={permitData.oldMunicipal}
				error={errors.oldMunicipal}
			/>


			{pilenthLang.kamalamai.palika}
			<DashedLangInput
				name="newWardNo"
				setFieldValue={setFieldValue}
				value={permitData.newWardNo}
				error={errors.newWardNo}
			/>

			{pilenthLang.kamalamai.kittaNo}

			<DashedNormalInput
				name="kittaNo"
				handleChange={handleChange}
				setFieldValue={setFieldValue}
				value={permitData.kittaNo}
				error={errors.kittaNo}
			/>

			{pilenthLang.kamalamai.jabi}
			<DashedLangInput
				name="landArea"
				setFieldValue={setFieldValue}
				value={permitData.landArea}
				error={errors.landArea}
			/>

			{`${pilenthLang.kamalamai.ko}, ${pilenthLang.kamalamai.purba}`}
			<DashedLangInput
				name={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : "dir0"}
				setFieldValue={setFieldValue}
				value={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : ""}
				error={errors.landArea}
			/>

			{pilenthLang.kamalamai.paschim}

			<DashedLangInput
				name={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : "dir1"}
				setFieldValue={setFieldValue}
				value={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : ""}
				error={errors.landArea}
			/>

			{pilenthLang.kamalamai.utthar}
			<DashedLangInput
				name={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : "dir2"}
				setFieldValue={setFieldValue}
				value={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : ""}
				error={errors.landArea}
			/>

			{pilenthLang.kamalamai.dakchin}
			<DashedLangInput
				name={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : "dir3"}
				setFieldValue={setFieldValue}
				value={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : ""}
				error={errors.landArea}
			/>


			{`${pilenthLang.kamalamai.content2} ${pilenthLang.kamalamai.lambae}`}

			<DashedLengthInputWithRelatedUnits
				name="lambae"
				unitName="floorUnit"
				relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight', 'buildingWidth']}
			/>{' '}

			{pilenthLang.kamalamai.chaudahi}

			<DashedLengthInputWithRelatedUnits
				name="chaudahi"
				unitName="floorUnit"
			/>

			{pilenthLang.kamalamai.uchahi}

			<DashedLengthInputWithRelatedUnits
				name="uchahi"
				unitName="floorUnit"
			/>


			{pilenthLang.kamalamai.ko}
			<DashedLangInput
				name="content1"
				handleChange={handleChange}
				error={errors.content1}
				value={values.content1}
				setFieldValue={setFieldValue}
			/>

			{pilenthLang.kamalamai.content3}
			<DashedLangDateField
				inline={true}
				label={pilenthLang.kamalamai.date}
				name="supStrucDate1"
				setFieldValue={setFieldValue}
				value={values.supStrucDate1}
				error={errors.supStrucDate1}
			/>

			{pilenthLang.kamalamai.content4}
			<DashedLangDateField
				inline={true}
				label={pilenthLang.kamalamai.date}
				name="supStrucDate2"
				setFieldValue={setFieldValue}
				value={values.supStrucDate2}
				error={errors.supStrucDate2}
			/>

			{pilenthLang.kamalamai.content5}
			<DashedLangInput 
				name="huda"
				setFieldValue={setFieldValue}
				value={values.huda}
				error={errors.huda}
				setFieldValue={setFieldValue} 
			/>

			{pilenthLang.kamalamai.content6}

		</div>
	);
};


const NoteOrderSubHeading = ({ setFieldValue, values, errors }) => {
	return (
		<div>
			<div className="section-header">
				<h2 className="underline">{pilenthLang.heading2}</h2>
			</div>

			<FlexSingleRight>
				{pilenthLang.date}:
				<DashedLangDateField
					name="noteOrderDate"
					inline={true}
					compact={true}
					setFieldValue={setFieldValue}
					value={values.noteOrderDate}
					error={errors.noteOrderDate}
				/>
			</FlexSingleRight>
			<br />
		</div>
	);
};
const NoteOrderSubHeadingSundarHaraicha = ({ setFieldValue, values, errors }) => {
	return (
		<div className="margin-section">
			<PatraSankhya setFieldValue={setFieldValue} values={values} errors={errors} />
			<div className="section-header">
				<h3 className="underline normal margin-section">{pilenthLang.subHeading}</h3>
				<br />
				<h3 className="underline normal margin-section">{pilenthLang.subHeading2}</h3>
				<br />
				<h3 className="underline">{pilenthLang.sundarHaraichaSubject}</h3>
			</div>
		</div>
	);
};
const NoteOrderSubHeadingSundarInaruwa = () => {
	return (
		<div>
			<SectionHeader>
				<h3>{pilenthLang.heading1}</h3>
				<h2>{pilenthLang.heading2}</h2>
			</SectionHeader>
			<br />
		</div>
	);
};

const NoteOrderSubHeadingSundarKamalamai = () => {
	return (
		<div>
			<SectionHeader>
				<h2>{pilenthLang.heading2}</h2>
				<br />
				<h3>{pilenthLang.kamalamai.subject}</h3>
			</SectionHeader>
			<br />
		</div>
	);
};
const NoteOrderPlinthBodySundarHaraicha = ({ setFieldValue, udata, permitData, values, errors }) => {
	return (
		<div className="no-margin-field">
			{udata.organization.name}, {pilenthLang.content.content3} {permitData.nibedakTol} {pilenthLang.content.sadak} {permitData.nibedakSadak}{' '}
			{pilenthLang.content.content4} <Select name="selectShri" value={values.selectShri} placeholder="श्री/श्रीमती/सुश्री " options={opts} />{' '}
			{permitData.nibedakName} {sundarData.leYasNaPa} <DashedDateField name="applicantDateBS" inline={true} compact={true} />{' '}
			{sundarData.oldMunicipal} {permitData.newMunicipal} {pilenthLang.content.content3} {permitData.newWardNo} {sundarData.sabik}{' '}
			{permitData.sabik} {sundarData.kittaNo} {permitData.kittaNo} {sundarData.area} {permitData.landArea} {permitData.landAreaType}{' '}
			{sundarData.notice15}{' '}
			<DashedLangDateField
				name="Sangyardate"
				setFieldValue={setFieldValue}
				value={values.Sangyardate}
				error={errors.Sangyardate}
				inline={true}
				compact={true}
			/>{' '}
			{sundarData.surjamin}{' '}
			<DashedLangDateField
				name="surjaminDate"
				setFieldValue={setFieldValue}
				value={values.surjaminDate}
				error={errors.surjaminDate}
				inline={true}
				compact={true}
			/>{' '}
			{sundarData.end}
		</div>
	);
};

const NoteOrderPilenthLevelView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteOrderPlinth',
				form: true,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: false,
			},
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: false,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'surjaminMuchulka',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'notice15',
				form: false,
			},
			{
				api: api.mapCheckReport,
				objName: 'mapCheckReport',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'maptech',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		hasFileView={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param9: ['getElementsByClassName', PrintSelectors.DASHED_DROPDOWN],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		render={(props) => <NoteOrderPilenthLevelViewComponent {...props} parentProps={parentProps} />}
	/>
);
export default NoteOrderPilenthLevelView;
