import React, { Component } from 'react';
import { uniq } from 'lodash';
import { buildingPermitApplicationForm } from '../../../utils/data/mockLangFile';
import { isEmpty, getUnitValue } from '../../../utils/functionUtils';
import { setPrint } from '../../../store/actions/general';
import { floorDetailsData } from '../../../utils/data/mockLangFile';
import { getPermitData } from '../../../store/actions/formActions';

// Redux
import { connect } from 'react-redux';
import { getFormData, getUserInfo } from '../../../store/actions/anusuchigaAction';
import FallbackComponent from '../../shared/FallbackComponent';
import { translateDate } from '../../../utils/langUtils';
import { getDataConstructionTypeAll } from '../../../utils/enums/constructionType';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { isKamalamai, isBiratnagar } from '../../../utils/clientUtils';
import { KamalamaiSurroundingView } from './mapPermitComponents/KamalamaiSurroundingSection';
import { SignatureImage } from './formComponents/SignatureImage';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { PermitSectionHeader } from './mapPermitComponents/PermitSectionHeader';
import { SectionHeader } from '../../uiComponents/Headers';
import { surroundingMappingFlat } from '../../../utils/dataUtils';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
const floor_details_data = floorDetailsData.floorDetailsDataTable;

const opt = [
	{ key: 'ma', value: 'मैले', text: 'मैले' },
	{ key: 'hami', value: 'हामीले', text: 'हामीले' },
	{ key: 'org', value: 'संघसंस्थाले', text: 'संघसंस्थाले' },
	{ key: 'org1', value: 'कार्यालयले', text: 'कार्यालयले' },
];

const getMs = (value) => {
	const ret = opt.filter((row) => row.value === value);
	return ret[0] ? ret[0].key : 'ma';
};

// const municipalName = getUserInfoObj() && getUserInfoObj().organization.name,

class MapPermintApplicationView extends Component {
	constructor(props) {
		super(props);
		const isBrtngr = isBiratnagar;
		this.state = {
			isSucess: false,
			isBrtngr,
			data: {
				section1: isBrtngr ? permitFormLang.form_step1_birtnagar : permitFormLang.form_step1,
				section2: isBrtngr ? permitFormLang.form_step2_biratnagar : permitFormLang.form_step2,
				section3: isBrtngr ? permitFormLang.form_step3_biratnagar : permitFormLang.form_step3,
				section4: isBrtngr ? permitFormLang.form_step4_birtanagar : permitFormLang.form_step4,
			},
		};
	}
	componentDidMount() {
		// console.log('map permit view called');

		this.props.getFormData();
		this.props.getPermitData();
		// this.props.getUserInfo();
	}

	render() {
		if (!isEmpty(this.props.formData.permitData) && !isEmpty(this.props.formData.userData)) {
			const detailed_Info = this.props.formData.permitData;
			const floorArray = new FloorBlockArray(this.props.formData.permitData.floor);
			const user_info = this.props.formData.userData;
			const blocks = uniq(this.props.formData.permitData.floor.map((fl) => fl && fl.block)).filter((fl) => fl);
			const {
				isBrtngr,
				data: { section1, section2, section3, section4 },
			} = this.state;

			return (
				<React.Fragment>
					<div ref={(ref) => this.props.setRef && this.props.setRef(ref)} className="print-small-font">
						<LetterSalutation
							lines={[
								permitFormLang.form_title,
								user_info.organization.name,
								user_info.organization.officeName,
								user_info.organization.address,
							]}
						/>
						<br />
						<SectionHeader>
							<h3 className="red">{permitFormLang.form_subject}</h3>
						</SectionHeader>
						<div>{permitFormLang.form_mahodaya}</div>
						<div>
							<span className="indent-span">{detailed_Info.applicantMs}</span>{' '}
							<span>
								{permitFormLang.form_content}
								{permitFormLang[getMs(['applicantMs'])].form_chu}
								{permitFormLang.form_content_1}
								{permitFormLang[getMs(['applicantMs'])].form_chu}
								{permitFormLang.form_content_2}
								{permitFormLang[getMs(['applicantMs'])].form_garnechu}
								{permitFormLang.form_content_3}
							</span>
						</div>
						<PermitSectionHeader content={permitFormLang.form_tapasil} />
						{isBrtngr ? (
							<div>
								<h5 className="mapview-tapsil">{section1.heading}</h5>
								<div className="div-indent">
									<div>
										{section1.fieldName_1} {detailed_Info.applicantName}
									</div>
									<div>
										{section1.fieldName_2} {detailed_Info.applicantAddress}
									</div>
								</div>
								<h5 className="mapview-tapsil">{section1.naksawalaDetails}</h5>
								<div className="div-indent">
									<div>
										{section1.naksawalaName} {detailed_Info.naksawalaName}
									</div>
									<div>
										{section1.naksawalaAddress} {detailed_Info.naksawalaAddress}
									</div>
									<div>
										{section1.fieldName_3} {detailed_Info.citizenshipNo} {section1.citizenshipIssueDate}
										{': '}
										{detailed_Info.citizenshipIssueDate} {section1.citizenshipIssueZone}: {detailed_Info.citizenshipIssueZone}
									</div>
									<div>
										{section1.spouseHead} {detailed_Info.spouseType}
										{section1.spouseTail} {detailed_Info.fathersSpouseName}
									</div>
								</div>
							</div>
						) : (
							<div>
								<h5 className="mapview-tapsil">{permitFormLang.form_step1.heading}</h5>
								<div className="div-indent">
									<div>
										{section1.fieldName_1} {detailed_Info.applicantName}
									</div>
									<div>
										{section1.fieldName_2} {detailed_Info.applicantAddress}
									</div>
									<div>
										{section1.fieldName_3} {detailed_Info.citizenshipNo}
									</div>
									<div>
										{section1.spouseHead} {detailed_Info.spouseType}
										{section1.spouseTail} {detailed_Info.fathersSpouseName}
									</div>
								</div>
							</div>
						)}
						<h5 className="mapview-header">{section2.heading}</h5>
						<div className="div-indent">
							<div>
								{section2.fieldName_1}
								{user_info.organization.name}
								{section2.fieldName_1_1} {detailed_Info.newWardNo}
							</div>
							<div>
								{section2.fieldName_2} {detailed_Info.oldMunicipal}
								<span style={{ marginLeft: '15px' }}>
									{section2.fieldName_3} {detailed_Info.oldWardNo}
								</span>
							</div>
							<div>
								{section2.fieldName_3_1} {detailed_Info.sadak}
							</div>
							<div>
								{section2.fieldName_4} {detailed_Info.nearestLocation}
								<br />
								<span style={{ marginLeft: '34px' }}>
									{section2.fieldName_5} {detailed_Info.buildingJoinRoad}
								</span>
							</div>
							<div>
								{section2.fieldName_6} {detailed_Info.buildingJoinRoadType}
								{detailed_Info.buildingJoinRoadType.includes(section2.checkBox_option.option_6) && (
									<span>
										{' '}
										{': '}
										{detailed_Info.buildingJoinRoadTypeOther}{' '}
									</span>
								)}
							</div>
						</div>
						<h5 className="mapview-header">{section3.heading}</h5>
						<div className="div-indent">
							<div>
								{section3.fieldName_1} {detailed_Info.landPassDate}
							</div>
							<div>
								{section3.fieldName_2} {detailed_Info.kittaNo}
							</div>
							<div>
								{section3.fieldName_3} {detailed_Info.ownershipName}
							</div>
							<div>
								{section3.fieldName_4} {detailed_Info.landArea}
								<span style={{ marginLeft: '15px' }}>
									{section3.fieldName_6} {detailed_Info.landRegNo}
								</span>
								<span style={{ marginLeft: '15px' }}>
									{section3.fieldName_7} {detailed_Info.landCertificateIssueDate}
								</span>
							</div>
						</div>
						{isBrtngr ? <div className="div-indent">{section4.heading}</div> : <h5 className="mapview-header">{section4.heading}</h5>}
						{detailed_Info.surrounding.length > 0 && (
							<div className={isBrtngr ? 'div-indent' : ''}>
								{detailed_Info.surrounding.sort((a, b) => a.side > b.side ? 1 : -1).map((row, index) => (
									<React.Fragment key={index}>
										<span style={{ marginLeft: '20px' }}>
											{Object.values(section4.directions)[index]}{' '}
											{surroundingMappingFlat.find((fl) => fl.side === row.side) &&
												surroundingMappingFlat.find((fl) => fl.side === row.side).value}
											{permitFormLang.maLambai}
											{row.feet} {getUnitValue(row.sideUnit)}{' '}
											<span style={{ marginLeft: '20px' }}>
												{section4.fieldName_poll_common1} {row.kittaNo}{' '}
											</span>
											<span style={{ marginLeft: '20px' }}>
												{section4.fieldName_poll_common2} {row.sandhiyar}
											</span>
										</span>
										<br />
									</React.Fragment>
								))}
								{isKamalamai && <KamalamaiSurroundingView surrounding={detailed_Info.surrounding} />}
							</div>
						)}
						<div className="no-page-break-inside">
							<h5 className="mapview-header">{permitFormLang.form_step5.heading}</h5>
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step5.fieldName_1} {detailed_Info.purposeOfConstruction}
								{detailed_Info.purposeOfConstruction.includes(permitFormLang.form_step5.checkBox_option.option_3) && (
									<span>
										{' '}
										{': '}
										{detailed_Info.purposeOfConstructionOther}{' '}
									</span>
								)}
							</span>
							<br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step5.fieldName_2} {getDataConstructionTypeAll(detailed_Info.constructionType)}
							</span>
							<br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step5.fieldName_3} {detailed_Info.oldMapDate}
							</span>
							<br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step5.fieldName_4} {detailed_Info.mohada}
							</span>
							<br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step5.fieldName_5} {detailed_Info.constructionFinishing}{' '}
								{detailed_Info.constructionFinishing.includes(permitFormLang.form_step5.checkBox_option4.option_6) && (
									<span>
										{' '}
										{': '}
										{detailed_Info.constructionFinishingOther}{' '}
									</span>
								)}
							</span>
						</div>
						<div className="no-page-break-inside">
							<h5 className="mapview-header">{permitFormLang.form_step6.heading}</h5>
							<span style={{ marginLeft: '20px' }}>
								{detailed_Info.dhalNikasArrangement}{' '}
								{detailed_Info.dhalNikasArrangement === permitFormLang.form_step6.checkBox_option.option_3 && (
									<span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{permitFormLang.form_step6.fieldName_1} {detailed_Info.dhalNikasArrangementOther}{' '}
											{getUnitValue(detailed_Info.dhalUnit)}
										</span>
									</span>
								)}
							</span>
						</div>
						<div className="no-page-break-inside">
							<h5 className="mapview-header">{permitFormLang.form_step7.heading}</h5>
							<span style={{ marginLeft: '20px' }}>
								{detailed_Info.foharArrangement} {/* {detailed_Info.foharArrangementOther} */}
								{detailed_Info.foharArrangement.includes(permitFormLang.form_step7.checkBox_option.option_3) && (
									<span>
										{' '}
										{': '}
										{detailed_Info.foharArrangementOther}{' '}
									</span>
								)}
							</span>
						</div>
						<h5 className="mapview-header">{permitFormLang.form_step8.heading}</h5>
						<span style={{ marginLeft: '20px' }}>
							{permitFormLang.form_step8.fieldName_1} {detailed_Info.pipeline}
							{detailed_Info.pipeline === permitFormLang.form_step8.checkBox_option.option_1 && (
								<span>
									<span style={{ marginLeft: '20px' }}>
										{permitFormLang.form_step8.fieldName_2} {detailed_Info.pipelineDistance}{' '}
										{getUnitValue(detailed_Info.pipelineUnit)}
									</span>
								</span>
							)}
							<br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step8.fieldName_3}
								{detailed_Info.doPipelineConnection}
							</span>
						</span>
						<h5 className="mapview-header">{permitFormLang.form_step9.heading}</h5>
						<span style={{ marginLeft: '20px' }}>
							{permitFormLang.form_step9.fieldName_1}
							{detailed_Info.isHighTensionLine} <br />
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step9.fieldName_2}
								{detailed_Info.isLowTensionLine} <br />
							</span>
							{/* {detailed_Info.isHighTensionLine ===
                permitFormLang.form_step9.checkBox_option.option_1 && (
                <span> */}
							<span style={{ marginLeft: '20px' }}>
								{permitFormLang.form_step9.fieldName_3} {detailed_Info.isHighTensionLineDistance}{' '}
								{getUnitValue(detailed_Info.highTensionLineUnit)}
							</span>
						</span>
						<h5 className="mapview-header">{permitFormLang.form_tallaBibarab}</h5>
						<div className="mapview-floor">
							{/* <span style={{ marginLeft: '20px' }}> */}
							{blocks && blocks.length > 0
								? blocks.map((block) => (
										<>
											<b>बलक {block}</b>
											{floorArray
												.getFloorsWithLabels()
												.filter((fl) => fl.block === block)
												.map((floor, index) => {
													return (
														<div key={index}>
															{floor.label.sn(index)} {floor.label.floorName}{' '}
															{floor_details_data.floorDetailsDataTable_heading.heading_2} {floor.length}{' '}
															{floor.unitNepali} {floor_details_data.floorDetailsDataTable_heading.heading_3}
															{floor.width} {floor.unitNepali}{' '}
															{floor_details_data.floorDetailsDataTable_heading.heading_4}
															{floor.height} {floor.unitNepali}{' '}
															{floor_details_data.floorDetailsDataTable_heading.heading_5}
															{floor.area} {floor.areaUnitNepali}
														</div>
													);
												})}
										</>
								  ))
								: floorArray.getFloorsWithLabels().map((floor, index) => {
										return (
											<div key={index}>
												{floor.label.sn(index)} {floor.label.floorName}{' '}
												{floor_details_data.floorDetailsDataTable_heading.heading_2} {floor.length} {floor.unitNepali}{' '}
												{floor_details_data.floorDetailsDataTable_heading.heading_3}
												{floor.width} {floor.unitNepali} {floor_details_data.floorDetailsDataTable_heading.heading_4}
												{floor.height} {floor.unitNepali} {floor_details_data.floorDetailsDataTable_heading.heading_5}
												{floor.area} {floor.areaUnitNepali}
											</div>
										);
								  })}
						</div>
						<h5 className="mapview-header">{permitFormLang.form_step10.heading}</h5>
						{detailed_Info.member && detailed_Info.member.length > 0 && (
							<>
								{detailed_Info.member.map((row, index) => (
									<React.Fragment key={index}>
										<span style={{ marginLeft: '25px' }}>
											{Object.values(permitFormLang.form_step10.member_details.table_subheading)[index]}
											{') '}
											<span>
												{permitFormLang.form_step10.member_details.table_heading.heading2}
												{' : '}
												{detailed_Info.member[index] && detailed_Info.member[index].memberName}{' '}
											</span>
											<span style={{ marginLeft: '20px' }}>
												{permitFormLang.form_step10.member_details.table_heading.heading3}
												{' : '}
												{detailed_Info.member[index] && detailed_Info.member[index].relation}
											</span>
										</span>
										<br />
									</React.Fragment>
								))}
							</>
						)}
						<div className="no-page-break-inside">
							<h5 className="mapview-header">{permitFormLang.petitioner_nibedak}</h5>
							<SignatureImage value={this.props.ghardhaniSignature} showSignature={true} label={permitFormLang.petitioner_signature} />
							<div>
								{permitFormLang.petitioner_name}
								{detailed_Info.nibedakName}
								<span style={{ marginLeft: '20px' }}>
									{permitFormLang.petitioner_age} {detailed_Info.tol}
								</span>
							</div>
							<div>
								{permitFormLang.petitioner_municipal}
								{detailed_Info.newMunicipal}
								<span style={{ marginLeft: '20px' }}>
									{permitFormLang.petitioner_vadaNo} {detailed_Info.nibedakTol}
								</span>
								<span style={{ marginLeft: '20px' }}>
									{permitFormLang.petitioner_road} {detailed_Info.nibedakSadak}
								</span>
							</div>
							<div>
								{permitFormLang.petitioner_phone}
								{detailed_Info.applicantMobileNo}
							</div>
							<div>
								{permitFormLang.petitioner_date}
								{translateDate(detailed_Info.applicantDateBS)}
							</div>
							<br />
							<div>
								{permitFormLang.petitioner_additional}
								<br />
								<span style={{ marginLeft: '20px' }}>{detailed_Info.nibedakAdditional}</span>
							</div>
						</div>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getFormData,
	getUserInfo,
	getPermitData,
	setPrint: (res) => setPrint(res),
};
const mapStateToProps = (state) => {
	return {
		formData: state.root.formData,
		errors: state.root.ui.errors,
		loading: state.root.ui.loading,
		printcontent: state.root.general.printcontent,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MapPermintApplicationView);
