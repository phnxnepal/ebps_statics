import React, { Component } from 'react';
import { Button, Form, Divider, List, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { postFile, getUploadedFiles } from '../../../store/actions/fileActions';
import { getPermitAndUserData } from '../../../store/actions/formActions';
import api from '../../../utils/api';

import { getBackUrl } from '../../../utils/config';
import { Formik } from 'formik';
import { hasMultipleFiles, showToast, getSplitUrl, isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';

import * as Yup from 'yup';
import { validateFile } from '../../../utils/validationUtils';
import { getDocUrl } from '../../../utils/config';
import { FileInputWithPreview } from '../../shared/FileUploadInput';
import Viewer from 'react-viewer';
import DeleteModal from './../../shared/DeleteModal';
import { deleteFile } from './../../../store/actions/fileActions';
import { getDeleteStatus } from './../../../utils/urlUtils';
import { ImageDisplayPreview } from '../../shared/file/FileView';
// import { IMAGE } from '../../../utils/constants/fileConstants';
// let urlArr = [];
export class SingleFileUpload extends Component {
	state = {
		currImg: 0,
		id: '',
		isDeleteModalOpen: false,
	};

	componentDidMount() {
		this.props.getPermitAndUserData();

		if (isEmpty(this.props.files)) {
			this.props.getUploadedFiles();
		}
		// if (this.props.files && this.props.files.length > 0) {
		// 	urlArr = this.props.files
		// 		.map(el => {
		// 			if (getFileType(el.fileUrl) === IMAGE) {
		// 				return { src: `${getDocUrl()}${el.fileUrl}` };
		// 			}
		// 		})
		// 		.filter(src => src);
		// }
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success && this.props.success) {
			this.props.getUploadedFiles();
		}
	}

	handleDeleteModalOpen = id => {
		this.setState({
			isDeleteModalOpen: true,
			id: id,
		});
	};

	handleDeleteModalClose = () => {
		this.setState({
			isDeleteModalOpen: false,
		});
	};

	render() {
		const schema = Yup.object().shape({
			uploadFile: validateFile,
		});
		const { files, userData, prevData, url } = this.props;

		let isDelete = false;
		try {
			isDelete = getDeleteStatus(url, prevData, userData);
		} catch {
			isDelete = false;
		}
		return (
			<React.Fragment>
				<Form loading={this.props.loading}>
					{/* <h3>File Upload</h3> */}
					{this.props.fileCategories
						.filter(category => category.viewUrl && category.viewUrl.trim() === this.props.url)
						.map(category => (
							<Formik
								key={category.id}
								validationSchema={schema}
								onSubmit={async (values, actions) => {
									actions.setSubmitting(true);

									const data = new FormData();

									const selectedFile = values.uploadFile;
									// console.log("selectedFile");
									// console.log(selectedFile);
									if (selectedFile) {
										selectedFile.forEach(file => data.append('file', file));

										data.append('fileType', category.id);
										data.append('applicationNo', this.props.permitData.applicantNo);

										try {
											await this.props.postFile(`${getBackUrl()}${api.fileUpload}${this.props.permitData.applicantNo}`, data);

											if (this.props.success && this.props.success.success) {
												showToast(this.props.success.data.message || 'File uploaded');
												this.setState(() => ({
													needToGet: true,
												}));
												actions.resetForm({
													imagePreviewUrl: [],
													uploadFile: '',
												});
												document.getElementById(`file_${category.id}`).value = '';
											}
											actions.setSubmitting(false);
										} catch (err) {
											actions.setSubmitting(false);
											console.log('File upload failed', err);
										}
									} else {
										actions.setError('Please select a file before uploading.');
									}
									// console.log("data to send", data, selectedFile);
								}}
							>
								{({ handleSubmit }) => {
									return (
										<>
											<div>
												{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
												<Divider horizontal>{category.name}</Divider>{' '}
												<FileInputWithPreview
													name="uploadFile"
													fileCatId={category.id}
													hasMultipleFiles={hasMultipleFiles(category.isMultiple)}
												/>
												<List relaxed>
													<List.Header>Existing Files</List.Header>
													<div className="previewFileUpload">
														{files && files.length > 0 && files.some(file => file.fileType.id === category.id) ? (
															files
																.filter(file => file.fileType.id === category.id)
																.map(el => {
																	const fileTitleName = getSplitUrl(el.fileUrl, '/');
																	return (
																		<div
																			key={el.fileUrl}
																			className="eachPreviewFile viewFileSuccess"
																			title={fileTitleName}
																		>
																			<>
																				<ImageDisplayPreview
																					src={el.fileUrl}
																					alt={fileTitleName}
																					onClick={() =>
																						this.setState({
																							imageOpen: true,
																							currImg: `${getDocUrl()}${el.fileUrl}`,
																						})
																					}
																				/>
																				{isDelete && (
																					<Icon
																						name="close"
																						className="closeImg"
																						onClick={() => this.handleDeleteModalOpen(el.id)}
																					/>
																				)}
																			</>
																		</div>
																	);
																})
														) : (
															<List.Item>No files uploaded.</List.Item>
														)}
													</div>
												</List>
												<Button primary type="submit" id={category.id} onClick={handleSubmit}>
													Upload
												</Button>
												<Viewer
													visible={this.state.imageOpen}
													onClose={() => this.setState({ imageOpen: false })}
													images={[{ src: this.state.currImg }]}
													// activeIndex={this.state.currImg}
													zIndex={10000}
												/>
											</div>
										</>
									);
								}}
							</Formik>
						))}
					<DeleteModal
						open={this.state.isDeleteModalOpen}
						onClose={this.handleDeleteModalClose}
						history={this.props.parentHistory}
						loadingModal={this.props.loadingModal}
						id={this.state.id}
						deleteFile={() => this.props.deleteFile(this.state.id)}
					/>
				</Form>
			</React.Fragment>
		);
	}
}

const mapDispatchToProps = {
	postFile,
	getPermitAndUserData,
	getUploadedFiles,
	deleteFile,
};

const mapStateToProps = state => ({
	permitData: state.root.formData.permitData,
	userData: state.root.formData.userData,
	files: state.root.formData.files,
	fileCategories: state.root.formData.fileCategories,
	success: state.root.formData.success,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleFileUpload);
