import React, { Component } from 'react';
import { sanitaryDesignLang } from '../../../utils/data/sanitaryDesignLang';
import {
  Table,
  Form,
  Input,
  Dropdown,
  Label
} from 'semantic-ui-react';
import { Formik } from 'formik';
import {  EnglishField as Field} from '../../shared/fields/EnglishField';
import api from '../../../utils/api';
import {
  showToast,
} from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import SanitaryValidateSchema from '../formValidationSchemas/sanitaryDesignValidation';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';

const sanitDesLang = sanitaryDesignLang;
const table1 = sanitDesLang.table1;
const table2 = sanitDesLang.table2;
const options = [
  { key: 'a', value: 'sq.meter', text: 'sq.meter' },
  { key: 'b', value: 'sq.foot', text: 'sq.foot' }
];

class SanitaryPlumbingDesignRequirementsComponent extends Component {

  constructor(props) {
    super(props)

    const { prevData } = this.props;
    this.state = {
      initVal: getJsonData(prevData)
    }
  }

  render() {
  
    const { initVal} = this.state;
    const { permitData, prevData, hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl} = this.props

    return (
      <div className="sanitaryPlumbingContent-wrap">
        <Formik
          initialValues={initVal}
          validationSchema={SanitaryValidateSchema}
          onSubmit={async (values, actions) => {
            actions.setSubmitting(true);
            values.applicationNo = permitData.applicantNo;
            values.error && delete values.error;
            try {
              await this.props.postAction(
                `${api.sanitaryDesign}${permitData.applicantNo}`,
                values
              );

              window.scrollTo(0, 0);
              actions.setSubmitting(false);

              if (this.props.success && this.props.success.success) {
                handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
              }
            } catch (err) {
              actions.setSubmitting(false);
              window.scrollTo(0, 0);
              showToast('Something went wrong !!');
            }
          }}
        >
          {({
            isSubmitting,
            handleSubmit,
            errors,
            handleChange,
            setFieldValue, validateForm
          }) => (
              <Form loading={isSubmitting} className="ui form">
                {this.props.errors && (
                  <ErrorDisplay message={this.props.errors.message} />
                )}
                <div ref={this.props.setRef}>
               <FormHeading />
                  <Table celled structured className="english-div">
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell width={5}>
                          {table1.thead_1}
                        </Table.HeaderCell>
                        <Table.HeaderCell width={3}>
                          {table1.thead_2}
                        </Table.HeaderCell>
                        <Table.HeaderCell width={3}>
                          {table1.thead_3}
                        </Table.HeaderCell>
                        <Table.HeaderCell width={2}>
                          {table1.thead_4}
                        </Table.HeaderCell>
                        <Table.HeaderCell width={2}>
                          {table1.thead_5}
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table1.section1.description}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table1.section1.description_2}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section1.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={errors.audtioNos ? 'error field' : 'field'}
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="audtioNos"
                                style={{
                                  width: '130px'
                                }}
                                autoComplete="off"
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.audtioNos && (
                              <div className="ui pointing above prompt label">
                                {errors.audtioNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={errors.audtioLit ? 'error field' : 'field'}
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="audtioLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.audtioLit && (
                              <div className="ui pointing above prompt label">
                                {errors.audtioLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.audtioWatStoCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="audtioWatStoCap" />
                            </div>
                            {errors.audtioWatStoCap && (
                              <div className="ui pointing above prompt label">
                                {errors.audtioWatStoCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="audtioWatStoCapRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table1.section1.td_cell_2}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section1.td_cell_3}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numBedDesCap"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Bed.</div>
                            </div>
                            {errors.numBedDesCap && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCapLit ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numBedDesCapLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.numBedDesCapLit && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCapLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCapWater ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="numBedDesCapWater" />
                            </div>
                            {errors.numBedDesCapWater && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCapWater}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="numBedDesCapRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section1.td_cell_4}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCap_1 ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numBedDesCap_1"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Bed.</div>
                            </div>
                            {errors.numBedDesCap_1 && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCap_1}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCapLit_1 ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numBedDesCapLit_1"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.numBedDesCapLit_1 && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCapLit_1}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numBedDesCapWater_1 ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="numBedDesCapWater_1" />
                            </div>
                            {errors.numBedDesCapWater_1 && (
                              <div className="ui pointing above prompt label">
                                {errors.numBedDesCapWater_1}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="numBedDesCapRmrk_1" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section1.td_cell_5}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.officeBuildNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="officeBuildNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Bed.</div>
                            </div>
                            {errors.officeBuildNos && (
                              <div className="ui pointing above prompt label">
                                {errors.officeBuildNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.officeBuildLit ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="officeBuildLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.officeBuildLit && (
                              <div className="ui pointing above prompt label">
                                {errors.officeBuildLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.officeBuildWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="officeBuildWatCap" />
                            </div>
                            {errors.officeBuildWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.officeBuildWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="officeBuildWatCapRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table1.section2.description}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section2.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.auditOfBuildDesCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="auditOfBuildDesCap"
                                style={{
                                  width: '90px'
                                }}
                              />
                              <div className="ui label">Nos of w.e.</div>
                            </div>
                            {errors.auditOfBuildDesCap && (
                              <div className="ui pointing above prompt label">
                                {errors.auditOfBuildDesCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.auditOfBuildLit ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="auditOfBuildLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.auditOfBuildLit && (
                              <div className="ui pointing above prompt label">
                                {errors.auditOfBuildLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.auditOfBuildDesCapWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field
                                type="text"
                                name="auditOfBuildDesCapWatCap"
                              />
                            </div>
                            {errors.auditOfBuildDesCapWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.auditOfBuildDesCapWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="auditOfBuildDesCapRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table1.section2.td_cell_2}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospitalDesCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospitalDesCap"
                                style={{
                                  width: '80px'
                                }}
                              />
                              <div className="ui label">Nos of urinal.</div>
                            </div>
                            {errors.hospitalDesCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospitalDesCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospitalLit ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospitalLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.hospitalLit && (
                              <div className="ui pointing above prompt label">
                                {errors.hospitalLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospitalWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospitalWatCap" />
                            </div>
                            {errors.hospitalWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospitalWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="hospitalRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>
                          {table2.thead_1}
                        </Table.Cell>
                        <Table.Cell>
                          {table2.thead_2}
                        </Table.Cell>
                        <Table.Cell>
                          {table2.thead_3}
                        </Table.Cell>
                        <Table.Cell>
                          {table2.thead_4}
                        </Table.Cell>
                        <Table.Cell>
                          {table2.thead_5}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section1.description}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section1.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numOfFloor ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numOfFloor"
                                style={{
                                  width: '90px'
                                }}
                              />
                              <div className="ui label">Nos of floor</div>
                            </div>
                            {errors.numOfFloor && (
                              <div className="ui pointing above prompt label">
                                {errors.numOfFloor}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numOfFloorRisers ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="numOfFloorRisers"
                                style={{
                                  width: '70px'
                                }}
                              />
                              <div className="ui label">Nos of wet risers.</div>
                            </div>
                            {errors.numOfFloorRisers && (
                              <div className="ui pointing above prompt label">
                                {errors.numOfFloorRisers}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.numOfFloorWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="numOfFloorWatCap" />
                            </div>
                            {errors.numOfFloorWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.numOfFloorWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="numOfFloorRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section1.td_cell_2}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.floorAreaMeter ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Input
                                fluid
                                labelPosition="right"
                              >
                                <input type="text"
                                className="english-div-field"
                                name="floorAreaMeter"
                                onChange={handleChange} />
                                <Label>

                                <Dropdown
                                  defaultValue="sq.meter"
                                  name="FloorAreaUnit"
                                  options={options}
                                  onChange={(e, { value }) =>
                                    setFieldValue('FloorAreaUnit', value)
                                  }
                                ></Dropdown>
                                </Label>
                              </Input>
                            </div>
                            {errors.floorAreaMeter && (
                              <div className="ui pointing above prompt label">
                                {errors.floorAreaMeter}
                              </div>
                            )}
                          </div>
                          {/* <div className={errors.floorAreaMeter ? 'error field' : 'field'}>
												<LabeledInput fluid
													suffix={<Dropdown defaultValue='sq.meter' options={options} />}

													labelPosition='right'
													name="floorAreaMeter"
												/>
												{errors.floorAreaMeter &&
													<div className="ui pointing above prompt label">
														{errors.floorAreaMeter}
													</div>}
											</div> */}
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.floorAreaRisers ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="floorAreaRisers"
                                style={{
                                  width: '70px'
                                }}
                              />
                              <div className="ui label">Nos of wet risers.</div>
                            </div>
                            {errors.floorAreaRisers && (
                              <div className="ui pointing above prompt label">
                                {errors.floorAreaRisers}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.floorAreaWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="floorAreaWatCap" />
                            </div>
                            {errors.floorAreaWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.floorAreaWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="floorAreaRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section1.td_cell_3}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.capUndGrTank ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="capUndGrTank" />
                            </div>
                            {errors.capUndGrTank && (
                              <div className="ui pointing above prompt label">
                                {errors.capUndGrTank}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.capUndGrTankSubDesLit
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="capUndGrTankSubDesLit"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Litres.</div>
                            </div>
                            {errors.capUndGrTankSubDesLit && (
                              <div className="ui pointing above prompt label">
                                {errors.capUndGrTankSubDesLit}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.capUndGrTankWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="capUndGrTankWatCap" />
                            </div>
                            {errors.capUndGrTankWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.capUndGrTankWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="capUndGrTankRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section2.description}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section2.td_cell_1}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section3.description}
                          &nbsp;
                        <div
                            className={
                              errors.gentsUserNoToilte ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="gentsUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.gentsUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.gentsUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section3.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.watClosCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="watClosCap" />
                            </div>
                            {errors.watClosCap && (
                              <div className="ui pointing above prompt label">
                                {errors.watClosCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.watClosNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="watClosNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.watClosNos && (
                              <div className="ui pointing above prompt label">
                                {errors.watClosNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.watClosNosWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="watClosNosWatCap" />
                            </div>
                            {errors.watClosNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.watClosNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="watClosNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section3.td_cell_2}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={errors.urinalCap ? 'error field' : 'field'}
                          >
                            <div className="ui input">
                              <Field type="text" name="urinalCap" />
                            </div>
                            {errors.urinalCap && (
                              <div className="ui pointing above prompt label">
                                {errors.urinalCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.urinalCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="urinalCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.urinalCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.urinalCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.urinalCapNosWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="urinalCapNosWatCap" />
                            </div>
                            {errors.urinalCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.urinalCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="urinalCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section3.td_cell_3}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={errors.basinCap ? 'error field' : 'field'}
                          >
                            <div className="ui input">
                              <Field type="text" name="basinCap" />
                            </div>
                            {errors.basinCap && (
                              <div className="ui pointing above prompt label">
                                {errors.basinCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.basinCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="basinCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.basinCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.basinCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.basinCapNosWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="basinCapNosWatCap" />
                            </div>
                            {errors.basinCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.basinCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="basinCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section4.description}
                          &nbsp;
                        <div
                            className={
                              errors.ladiesUserNoToilte ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="ladiesUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.ladiesUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.ladiesUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section4.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.ladWatCloCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="ladWatCloCap" />
                            </div>
                            {errors.ladWatCloCap && (
                              <div className="ui pointing above prompt label">
                                {errors.ladWatCloCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.ladWatCloCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="ladWatCloCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.ladWatCloCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.ladWatCloCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.ladWatCloCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="ladWatCloCapNosWatCap" />
                            </div>
                            {errors.ladWatCloCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.ladWatCloCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="ladWatCloCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section5.description}
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section5.description_2}
                          &nbsp;
                        <div
                            className={
                              errors.publicUserNoToilte ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="publicUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.publicUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.publicUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section5.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubWatClosCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubWatClosCap" />
                            </div>
                            {errors.pubWatClosCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubWatClosCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubWatClosNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="pubWatClosNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.pubWatClosNos && (
                              <div className="ui pointing above prompt label">
                                {errors.pubWatClosNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubWatClosNosWatCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubWatClosNosWatCap" />
                            </div>
                            {errors.pubWatClosNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubWatClosNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="pubWatClosNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section5.td_cell_2}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubUrinalCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubUrinalCap" />
                            </div>
                            {errors.pubUrinalCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubUrinalCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubUrinalCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="pubUrinalCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.pubUrinalCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.pubUrinalCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubUrinalCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubUrinalCapNosWatCap" />
                            </div>
                            {errors.pubUrinalCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubUrinalCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="pubUrinalCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section5.td_cell_3}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubBasinCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubBasinCap" />
                            </div>
                            {errors.pubBasinCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubBasinCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubBasinCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="pubBasinCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.pubBasinCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.pubBasinCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.pubBasinCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="pubBasinCapNosWatCap" />
                            </div>
                            {errors.pubBasinCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.pubBasinCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="pubBasinCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section6.description}
                          &nbsp;
                        <div
                            className={
                              errors.audiLadiesUserNoToilte
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="audiLadiesUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.audiLadiesUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.audiLadiesUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section6.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.audiLadWatCloCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="audiLadWatCloCap" />
                            </div>
                            {errors.audiLadWatCloCap && (
                              <div className="ui pointing above prompt label">
                                {errors.audiLadWatCloCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.audiLadWatCloCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="audiLadWatCloCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.audiLadWatCloCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.audiLadWatCloCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.audiLadWatCloCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field
                                type="text"
                                name="audiLadWatCloCapNosWatCap"
                              />
                            </div>
                            {errors.audiLadWatCloCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.audiLadWatCloCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="audiLadWatCloCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section7.description}
                          &nbsp;
                        <div
                            className={
                              errors.staffUserNoToilte ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="staffUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.staffUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.staffUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section7.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.staffWatCloCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="staffWatCloCap" />
                            </div>
                            {errors.staffWatCloCap && (
                              <div className="ui pointing above prompt label">
                                {errors.staffWatCloCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.staffWatCloCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="staffWatCloCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.staffWatCloCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.staffWatCloCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.staffWatCloCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="staffWatCloCapNosWatCap" />
                            </div>
                            {errors.staffWatCloCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.staffWatCloCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="staffWatCloCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell colSpan="5">
                          {table2.section8.description}
                          &nbsp;
                        <div
                            className={
                              errors.hospitUserNoToilte ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospitUserNoToilte"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos of users.</div>
                            </div>
                            {errors.hospitUserNoToilte && (
                              <div className="ui pointing above prompt label">
                                {errors.hospitUserNoToilte}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section8.td_cell_1}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWatCloCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiWatCloCap" />
                            </div>
                            {errors.hospiWatCloCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWatCloCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWatCloCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospiWatCloCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.hospiWatCloCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWatCloCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWatCloCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiWatCloCapNosWatCap" />
                            </div>
                            {errors.hospiWatCloCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWatCloCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="hospiWatCloCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section8.td_cell_2}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWasBasCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiWasBasCap" />
                            </div>
                            {errors.hospiWasBasCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWasBasCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWasBasCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospiWasBasCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.hospiWasBasCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWasBasCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiWasBasCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiWasBasCapNosWatCap" />
                            </div>
                            {errors.hospiWasBasCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiWasBasCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="hospiWasBasCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section8.td_cell_3}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiBathCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiBathCap" />
                            </div>
                            {errors.hospiBathCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiBathCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiBathCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospiBathCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.hospiBathCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiBathCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiBathCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiBathCapNosWatCap" />
                            </div>
                            {errors.hospiBathCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiBathCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="hospiBathCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>{table2.section8.td_cell_4}</Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiCleanCap ? 'error field' : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiCleanCap" />
                            </div>
                            {errors.hospiCleanCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiCleanCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiCleanCapNos ? 'error field' : 'field'
                            }
                          >
                            <div className="ui right labeled input">
                              <Field
                                type="text"
                                name="hospiCleanCapNos"
                                style={{
                                  width: '130px'
                                }}
                              />
                              <div className="ui label">Nos.</div>
                            </div>
                            {errors.hospiCleanCapNos && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiCleanCapNos}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <div
                            className={
                              errors.hospiCleanCapNosWatCap
                                ? 'error field'
                                : 'field'
                            }
                          >
                            <div className="ui input">
                              <Field type="text" name="hospiCleanCapNosWatCap" />
                            </div>
                            {errors.hospiCleanCapNosWatCap && (
                              <div className="ui pointing above prompt label">
                                {errors.hospiCleanCapNosWatCap}
                              </div>
                            )}
                          </div>
                        </Table.Cell>
                        <Table.Cell>
                          <Field type="text" name="hospiCleanCapNosRmrk" />
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </div>

                <br />
                	<SaveButtonValidation
                    errors={errors}
                    formUrl={formUrl}
                    hasSavePermission={hasSavePermission}
                    hasDeletePermission={hasDeletePermission}
                    isSaveDisabled={isSaveDisabled}
                    prevData={checkError(prevData)}
                    handleSubmit={handleSubmit}
                    validateForm={validateForm}
								/>
              </Form>
            )}
        </Formik>
      </div>
    );
  }
}



export const FormHeading = () => (
	<div className="centered-underlined-heading">
       <h3>{sanitDesLang.heading_1}
                    </h3>
                    <h5>
                     {sanitDesLang.heading_2}
                    </h5>
                    <h5>
                      
{sanitDesLang.heading_3}

                    </h5>
                    <h3 className="english-div">
                      {sanitDesLang.heading_4}
                    </h3>
                    <span className="english-div">{sanitDesLang.sub_desc}</span>
	</div>
);

const SanitaryPlumbingDesignRequirements = parentProps => (
  <FormContainerV2
    api={[{ api: api.sanitaryDesign, objName: 'sanitaryDesign', form: true }]}
    prepareData={data => data}
    useInnerRef={true}
    onBeforeGetContent={{
      param1: ['getElementsByTagName', 'input', 'value'],
      param3: ['getElementsByClassName', 'ui label', 'innerText']
    }}
    parentProps={parentProps}
    render={props => (
      <SanitaryPlumbingDesignRequirementsComponent
        {...props}
        parentProps={parentProps}
      />
    )}
  />
);

export default SanitaryPlumbingDesignRequirements;

