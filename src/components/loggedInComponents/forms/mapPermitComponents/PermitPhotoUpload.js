import React, { Component } from 'react';

// Redux
import { connect } from 'react-redux';
import { getNibedakPhoto, postNibedakPhoto } from '../../../../store/actions/buildingPermitAction';

import { Form, Divider, List, Button } from 'semantic-ui-react';
import { Formik } from 'formik';
import { getDocUrl } from '../../../../utils/config';
import api from '../../../../utils/api';
import { showToast, getSplitUrl } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { FileInputWithPreview } from '../../../shared/FileUploadInput';
import Viewer from 'react-viewer';
import FallbackComponent from '../../../shared/FallbackComponent';
import { NIBEDAK_PHOTO_TITLE } from '../../../../utils/constants';

import * as Yup from 'yup';
import { FILE_SIZE, SUPPORTED_FORMATS_IMAGE } from '../../../../utils/constants/fileConstants';
import { fileMessages } from '../../../../utils/data/validationData';

const validateNibedakPhoto = Yup.object().shape({
	photo: Yup.array()
		.typeError(fileMessages.required)
		.of(
			Yup.mixed()
				.test('fileSize', fileMessages.fileSize, value => {
					return value && value.size <= FILE_SIZE;
				})
				.test('fileFormat', fileMessages.photoError, value => value && SUPPORTED_FORMATS_IMAGE.includes(value.type))
		)
		.required(fileMessages.required),
});

class PermitPhotoUpload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imageOpen: false,
			src: '',
			existingPhoto: '',
			imageHash: Date.now(),
        }
    }

	componentDidMount() {
		this.props.getNibedakPhoto();
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			this.props.getNibedakPhoto();
		}

		if (prevProps.permitData !== this.props.permitData) {
			this.setState({ existingPhoto: this.props.permitData.photo, imageHash: Date.now() })
		}
	}

	render() {
		const { permitData, loading, errors } = this.props;

		if (permitData) {
			const photo = permitData.photo;
			// console.log('photo', this.props, photo);

			// let pdfValidate = '';
			let fileTitleName = '';

			fileTitleName = getSplitUrl(photo, '/');

			return (
				<React.Fragment>
					<Form loading={this.props.loading}>
						<Formik
							initialValues={{ photo }}
							validationSchema={validateNibedakPhoto}
							enableReinitialize
							onSubmit={async (values, actions) => {
								actions.setSubmitting(true);

								const data = new FormData();

								const selectedFile = values.photo;
								if (selectedFile) {
									selectedFile.forEach(file => data.append('photo', file));

									try {
										await this.props.postNibedakPhoto(`${api.buildPermit}${this.props.permitData.applicantNo}`, data);

										if (this.props.success && this.props.success.success) {
											showToast('File uploaded');
											// actions.resetForm({
											// 	imagePreviewUrl: [],
											// 	uploadFile: '',
											// });
										}
										actions.setSubmitting(false);
									} catch (err) {
										actions.setSubmitting(false);
										console.log('File upload failed', err);
									}
								} else {
									actions.setError('Please select a file before uploading.');
								}
							}}
						>
							{({ handleSubmit, errors, setFieldValue, values }) => {
								const {existingPhoto, imageHash} = this.state;

								return (
									<>
										<div>
											{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
											<Divider horizontal>{NIBEDAK_PHOTO_TITLE}</Divider> <FileInputWithPreview name="photo" fileCatId={'photo'} hasMultipleFiles={false} />
											<List relaxed>
												<List.Header>Existing Files</List.Header>
												<div className="previewFileUpload">
													{values.photo ? (
														<div key={existingPhoto} className="eachPreviewFile viewFileSuccess" title={fileTitleName}>
															<img
																src={`${getDocUrl()}${existingPhoto}?${imageHash}`}
																alt="imageFile"
																onClick={() =>
																	this.setState({
                                                                        imageOpen: true,
                                                                        src: `${getDocUrl()}${existingPhoto}?${imageHash}`, 
																	})
																}
															/>
														</div>
													) : (
														<List.Item>No files uploaded.</List.Item>
													)}
												</div>
											</List>
											<Button primary type="submit" onClick={handleSubmit}>
												Upload
											</Button>
											<Viewer
												visible={this.state.imageOpen}
												onClose={() => this.setState({ imageOpen: false })}
												images={[this.state.src ? { src: this.state.src, alt: 'nibedak photo'} : {src: `${getDocUrl()}${this.state.existingPhoto}`, alt: 'nibedak photo'} ]}
												zIndex={10000}
											/>
										</div>
									</>
								);
							}}
						</Formik>
					</Form>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent loading={loading} errors={errors} />;
		}
	}
}

const mapDispatchToProps = {
	getNibedakPhoto,
	postNibedakPhoto,
};

const mapStateToProps = state => ({
	permitData: state.root.formData.permitDataNew,
	success: state.root.formData.success,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

export default connect(mapStateToProps, mapDispatchToProps)(PermitPhotoUpload);
