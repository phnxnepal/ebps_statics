import React from 'react';
import { getIn, Field } from 'formik';
import { Select, Label, Form } from 'semantic-ui-react';
import { convertLandArea } from '../../../../utils/mathUtils';

export const LandAreaField = ({
	label,
	name = 'landAreaType',
	needsZIndex = false,
	options,
	valueFieldName = 'landArea',
	isMultiple = false,
	needsConversion = false,
}) => (
	<Field>
		{({ field, form }) => {
			const error = getIn(form.errors, name);
			const prevUnit = getIn(form.values, name);
			const area = getIn(form.values, valueFieldName);
			return (
				<Form.Group widths="equal">
					<Form.Field error={!!error}>
						<div className={needsZIndex ? 'zIndexSelect' : 'EbpsCustomSelect'}>
							{label && <span>{label}</span>}
							<Select
								options={options}
								value={getIn(form.values, name)}
								name={name}
								onChange={(e, { value }) => {
									if (needsConversion) {
										const nextUnit = value;
										if (isMultiple && Array.isArray(area)) {
											area.forEach((row, index) => {
												form.setFieldValue(`${valueFieldName}.${index}`, convertLandArea(nextUnit, prevUnit, row));
											});
										} else {
											form.setFieldValue(valueFieldName, convertLandArea(nextUnit, prevUnit, area));
										}
									}
									form.setFieldValue(name, value);
								}}
							/>
						</div>
						{error && (
							<Label pointing prompt size="large">
								{error}
							</Label>
						)}
					</Form.Field>
				</Form.Group>
			);
		}}
	</Field>
);
