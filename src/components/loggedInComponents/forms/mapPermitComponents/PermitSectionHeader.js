import React from 'react';
import { SectionHeader } from '../../../uiComponents/Headers';

export const PermitSectionHeader = ({ content }) => {
	return (
		<SectionHeader>
			<h4 className="left-align top-margin">{content}</h4>
		</SectionHeader>
	);
};
