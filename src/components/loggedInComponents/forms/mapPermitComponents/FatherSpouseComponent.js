import React from 'react';
import { useTranslatorIm } from '../../../../hooks/useTranslator';
import { useCursorReset } from '../../../../hooks/useCursorReset';
import { Label, Form } from 'semantic-ui-react';
// import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { spouseTypeSelectOptions } from '../../../../utils/optionUtils';
import { DashedSelect } from '../../../shared/Select';

export const FatherSpouseComponent = ({ setFieldValue, errors, labelHead, labelTail, values, ...rest }) => {
	const [className, valueState, handleChange] = useTranslatorIm(
		values.fathersSpouseName,
		setFieldValue,
		errors.fathersSpouseName,
		'fathersSpouseName'
	);
	const [inputRef, setCursorPosition] = useCursorReset(valueState);

	return (
		<Form.Group widths="equal">
			<Form.Field error={!!errors.fathersSpouseName}>
				<div>
					{labelHead}
					<DashedSelect options={spouseTypeSelectOptions} name="spouseType" />
					{labelTail}
				</div>
				<input
					ref={inputRef}
					{...rest}
					name={'fathersSpouseName'}
					type="text"
					onChange={(e) => {
						setCursorPosition(e.target.selectionStart);
						handleChange(e);
					}}
					className={`bpf-inputType ${className}`}
					value={valueState || values.fathersSpouseName}
				/>{' '}
				{/* {spouseTypeOptions.map((option) => (
					<React.Fragment key={option}>
						<RadioInput space={true} name={'spouseType'} option={option} label={option} />
					</React.Fragment>
				))} */}
				{errors.fathersSpouseName && (
					<Label pointing prompt size="large">
						{errors.fathersSpouseName}
					</Label>
				)}
			</Form.Field>
		</Form.Group>
	);
};
