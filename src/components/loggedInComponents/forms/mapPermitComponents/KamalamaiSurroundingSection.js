import React from 'react';
import { Table } from 'semantic-ui-react';

import { kamalamaiSurroundingData } from '../../../../utils/data/mapPermit';
import { getIn } from 'formik';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { distanceOptions, statusNepaliOption, landscapeTypeOptions } from '../../../../utils/optionUtils';
import { DashedSelect } from '../../../shared/Select';
import { getUnitValue } from '../../../../utils/functionUtils';

const data = kamalamaiSurroundingData;
const sides = [0, 1, 2, 3];
const BibaranType = {
	Feet: 0,
	Sandhiyar: 1,
	LandScapeType: 2,
	LandScapeName: 3,
	ActualSetback: 4,
	RoadWidth: 5,
	RoadStandardSetback: 6,
	DistanceFromHighTensionLine: 7,
	DistanceFromRiver: 8,
	DoorOrWindows: 9,
	DistanceFromBorder: 10,
};

const getLengthFields = (side) => [
	`surrounding.${side}.feet`,
	`surrounding.${side}.actualSetback`,
	`surrounding.${side}.roadWidth`,
	`surrounding.${side}.roadStandardSetback`,
	`surrounding.${side}.distanceFromHighTensionLine`,
	`surrounding.${side}.distanceFromRiver`,
	`surrounding.${side}.distanceFromBorder`,
];

export const KamalamaiSurroundingSection = ({ setFieldValue, values, errors }) => {
	const renderInput = (labelIdx) => {
		if (labelIdx === BibaranType.Feet) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm name={`surrounding.${side}.feet`} />
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.Sandhiyar) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedLangInput
						name={`surrounding.${side}.kittaNo`}
						setFieldValue={setFieldValue}
						value={getIn(values, `surrounding.${side}.kittaNo`)}
						error={getIn(errors, `surrounding.${side}.kittaNo`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.LandScapeType) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedSelect name={`surrounding.${side}.landScapeType`} options={landscapeTypeOptions} scrolling={true}/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.LandScapeName) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedLangInput
						name={`surrounding.${side}.sandhiyar`}
						setFieldValue={setFieldValue}
						value={getIn(values, `surrounding.${side}.sandhiyar`)}
						error={getIn(errors, `surrounding.${side}.sandhiyar`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.ActualSetback) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm name={`surrounding.${side}.actualSetback`} />
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.RoadWidth) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm
						name={`surrounding.${side}.roadWidth`}
						// unitName={`surrounding.${side}.sideUnit`}
						// setFieldValue={setFieldValue}
						// value={getIn(values, `surrounding.${side}.roadWidth`)}
						// error={getIn(errors, `surrounding.${side}.roadWidth`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.RoadStandardSetback) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm
						name={`surrounding.${side}.roadStandardSetback`}
						// unitName={`surrounding.${side}.sideUnit`}
						// setFieldValue={setFieldValue}
						// value={getIn(values, `surrounding.${side}.roadStandardSetback`)}
						// error={getIn(errors, `surrounding.${side}.roadStandardSetback`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.DistanceFromHighTensionLine) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm
						name={`surrounding.${side}.distanceFromHighTensionLine`}
						// unitName={`surrounding.${side}.sideUnit`}
						// setFieldValue={setFieldValue}
						// value={getIn(values, `surrounding.${side}.distanceFromHighTensionLine`)}
						// error={getIn(errors, `surrounding.${side}.distanceFromHighTensionLine`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.DistanceFromRiver) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm
						name={`surrounding.${side}.distanceFromRiver`}
						// unitName={`surrounding.${side}.sideUnit`}
						// setFieldValue={setFieldValue}
						// value={getIn(values, `surrounding.${side}.distanceFromRiver`)}
						// error={getIn(errors, `surrounding.${side}.distanceFromRiver`)}
					/>
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.DoorOrWindows) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedSelect name={`surrounding.${side}.doorOrWindows`} options={statusNepaliOption} />
					{/* <DashedLangInput
						name={`surrounding.${side}.doorOrWindows`}
						setFieldValue={setFieldValue}
						value={getIn(values, `surrounding.${side}.doorOrWindows`)}
						error={getIn(errors, `surrounding.${side}.doorOrWindows`)}
					/> */}
				</Table.Cell>
			));
		} else if (labelIdx === BibaranType.DistanceFromBorder) {
			return sides.map((side) => (
				<Table.Cell key={side}>
					<DashedNormalInputIm
						name={`surrounding.${side}.distanceFromBorder`}
						// unitName={`surrounding.${side}.sideUnit`}
						// setFieldValue={setFieldValue}
						// value={getIn(values, `surrounding.${side}.distanceFromBorder`)}
						// error={getIn(errors, `surrounding.${side}.distanceFromBorder`)}
					/>
				</Table.Cell>
			));
		} else return null;
	};

	return (
		<div>
			<br />
			<p>{data.title}</p>
			<Table className="certificate-ui-table">
				<Table.Header>
					<Table.Row>
						{data.tableHeader.map((header, idx) => (
							<Table.HeaderCell key={idx}>
								{header}
								{idx > 0 && (
									<>
										{' '}
										<UnitDropdownWithRelatedFields
											options={distanceOptions}
											unitName={`surrounding.${idx - 1}.sideUnit`}
											relatedFields={getLengthFields(idx - 1)}
										/>{' '}
										{getIn(errors, `surrounding.${idx - 1}.sideUnit`) && (
											<span className="tableError">{getIn(errors, `surrounding.${idx - 1}.sideUnit`)}</span>
										)}
									</>
								)}
							</Table.HeaderCell>
						))}
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{data.bibaranLabels.map((label, idx) => (
						<Table.Row key={idx}>
							<Table.Cell style={{ textAlign: 'left' }}>{label}</Table.Cell>
							{renderInput(idx)}
						</Table.Row>
					))}
				</Table.Body>
			</Table>
		</div>
	);
};

export const KamalamaiSurroundingView = ({ surrounding }) => {
	const renderInput = (labelIdx) => {
		if (!surrounding || surrounding.length < 1) {
			return null;
		} else if (labelIdx === BibaranType.Feet) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].feet}</Table.Cell>);
		} else if (labelIdx === BibaranType.Sandhiyar) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].sandhiyar}</Table.Cell>);
		} else if (labelIdx === BibaranType.LandScapeType) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].landScapeType}</Table.Cell>);
		} else if (labelIdx === BibaranType.LandScapeName) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].landScapeName}</Table.Cell>);
		} else if (labelIdx === BibaranType.ActualSetback) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].actualSetback}</Table.Cell>);
		} else if (labelIdx === BibaranType.RoadWidth) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].roadWidth}</Table.Cell>);
		} else if (labelIdx === BibaranType.RoadStandardSetback) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].roadStandardSetback}</Table.Cell>);
		} else if (labelIdx === BibaranType.DistanceFromHighTensionLine) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].distanceFromHightTensionLine}</Table.Cell>);
		} else if (labelIdx === BibaranType.DistanceFromRiver) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].distanceFromRiver}</Table.Cell>);
		} else if (labelIdx === BibaranType.DoorOrWindows) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].doorOrWindows}</Table.Cell>);
		} else if (labelIdx === BibaranType.DistanceFromBorder) {
			return sides.map((side) => <Table.Cell key={side}>{surrounding[side] && surrounding[side].distanceFromBorder}</Table.Cell>);
		} else return null;
	};

	return (
		<div>
			<br />
			<p>{data.title}</p>
			<Table className="certificate-ui-table">
				<Table.Header>
					<Table.Row>
						{data.tableHeader.map((header, idx) => (
							<Table.HeaderCell key={idx}>
								{header} {idx > 0 && <>{`(${getUnitValue(`surrounding.${idx - 1}.sideUnit`)})`}</>}
							</Table.HeaderCell>
						))}
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{data.bibaranLabels.map((label, idx) => (
						<Table.Row key={idx}>
							<Table.Cell style={{ textAlign: 'left' }}>{label}</Table.Cell>
							{renderInput(idx)}
						</Table.Row>
					))}
				</Table.Body>
			</Table>
		</div>
	);
};
