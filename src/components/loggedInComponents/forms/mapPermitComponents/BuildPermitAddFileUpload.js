import React from 'react';
import { Formik } from 'formik';
import { Modal, Button, Form } from 'semantic-ui-react';
import { saveButton } from '../../../../utils/data/genericData';
import MultiFileUpload from '../MultiFileUpload';
import NibdedakPhotoAdd from './NibedakPhotoAdd';
import { showToast, isEmpty } from '../../../../utils/functionUtils';
import { postNibedakPhoto } from '../../../../store/actions/buildingPermitAction';
import { postFileValidation } from '../../../../store/actions/fileActions';
import { getBackUrl } from '../../../../utils/config';
import api from '../../../../utils/api';
import { connect } from 'react-redux';
import { updateFileState } from '../../../../utils/fileUtils';

class BuildPermitAddFileUpload extends React.Component {
	state = {
		files: [],
		gotPhoto: false,
	};

	handleSuccess() {
		this.props.handleFilesUploaded();
		setTimeout(() => {
			showToast('File uploaded');
			this.props.handleClose();
			this.props.history.push('/user/forms/design-approval');
		}, 2000);
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.props.success !== prevProps.success && this.props.success.success && this.props.open) {
			this.handleSuccess();
		}

		if (this.state.gotPhoto !== prevState.gotPhoto && this.state.gotPhoto) {
			this.handleSuccess();
		}
	}

	handleSubmitModal = () => {
		this.props.handleClose();
		this.props.history.push('/user/forms/design-approval');
	};

	handleFileSubmitModal = async (values, actions) => {
		actions.setSubmitting(true);
		if (this.state.files.length > 0 && (values.photo || this.props.savedPermitData.photo)) {
			this.state.files.forEach(async (fileObj) => {
				const selectedFile = fileObj.files;
				const data = new FormData();
				const applicationNo = this.props.savedPermitData.id;
				if (selectedFile) {
					selectedFile.forEach((file) => data.append('file', file));

					data.append('fileType', fileObj.fileCatId);
					data.append('applicationNo', applicationNo);

					try {
						await this.props.postFileValidation(`${getBackUrl()}${api.fileUpload}${applicationNo}`, data);
					} catch (err) {
						console.log('File upload failed', err);
					}
				} else {
					console.log('Please select a file before uploading.');
				}
			});

			this.setState({ files: [] });
			const dataPhoto = new FormData();
			const selectedPhoto = values.photo;
			if (selectedPhoto) {
				selectedPhoto.forEach((file) => dataPhoto.append('photo', file));
				try {
					await this.props.postNibedakPhoto(`${api.buildPermit}${this.props.savedPermitData.id}`, dataPhoto);
				} catch (err) {
					console.log('File upload failed', err);
				}
			} else if (!isEmpty(this.props.savedPermitData.photo)) {
				this.setState({ gotPhoto: true });
			}
			actions.setSubmitting(false);
		}
	};

	handleFileChange = (fileObject) => {
		this.setState((prevState) => {
			return {
				files: updateFileState(prevState.files, fileObject),
			};
		});
	};
	render() {
		return (
			<div>
				<Formik
					key="file-upload"
					onSubmit={(values, actions) => this.handleFileSubmitModal(values, actions)}
					render={({ values, setFieldValue, isSubmitting, handleSubmit, errors }) => (
						<Modal key="save-confirmation" closeIcon open={this.props.open} onClose={this.props.handleClose}>
							<Modal.Header>{saveButton.uploadFileReminder}</Modal.Header>
							<Modal.Content scrolling>
								{/* <h3>{saveButton.uploadFileReminder}</h3> */}
								<Form loading={isSubmitting}>
									<NibdedakPhotoAdd
										permitData={this.props.savedPermitData}
										values={values}
										setFieldValue={setFieldValue}
										error={errors.photo}
									/>
									<MultiFileUpload
										url={'/user/forms/map-permit-application-edit'}
										fileCategories={this.props.fileCategories}
										handleFileChange={this.handleFileChange}
										hasDeletePermission={false}
									/>
								</Form>
							</Modal.Content>
							<Modal.Actions>
								<Button negative onClick={this.handleClose}>
									Exit
								</Button>
								<Button type="submit" positive icon="checkmark" labelPosition="right" content="Go Forward" onClick={handleSubmit} />
							</Modal.Actions>
						</Modal>
					)}
				/>
			</div>
		);
	}
}

const mapDispatchToProps = {
	postFileValidation,
	postNibedakPhoto,
};

const mapStateToProps = (state) => {
	return {
		fileCategories: state.root.formData.fileCategories,
		formData: state.root.formData,
		errors: state.root.ui.errors,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(BuildPermitAddFileUpload);
