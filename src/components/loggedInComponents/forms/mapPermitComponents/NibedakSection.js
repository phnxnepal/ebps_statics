import React from 'react';
import EbpsForm from '../../../shared/EbpsForm';
import { LangDateField } from '../../../shared/DateField';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { SelectInput } from '../../../shared/Select';
import EbpsTextareaField from '../../../shared/MyTextArea';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

export const NibedakSection = ({ setFieldValue, values, errors, wardOptions }) => {
	return (
		<div className="petitionerDetails">
			<h5 className="formFeild-mainLabel">{permitFormLang.petitioner_nibedak}</h5>
			<div className="petitionerSignature">{permitFormLang.petitioner_signature}</div>
			<div>
				<EbpsForm
					label={permitFormLang.petitioner_name}
					name="nibedakName"
					setFieldValue={setFieldValue}
					value={values['nibedakName']}
					error={errors.nibedakName}
				/>
			</div>
			<div>
				<EbpsForm label={permitFormLang.petitioner_age} name="tol" setFieldValue={setFieldValue} value={values['tol']} error={errors.tol} />
			</div>
			<div>
				<EbpsForm
					label={permitFormLang.petitioner_municipal}
					name="newMunicipal"
					value={values['newMunicipal']}
					setFieldValue={setFieldValue}
					// disabled
				/>
			</div>
			<div>
				<SelectInput label={permitFormLang.petitioner_vadaNo} name="nibedakTol" compact={true} options={wardOptions} />
			</div>
			<div>
				<EbpsForm
					label={permitFormLang.petitioner_road}
					name="nibedakSadak"
					setFieldValue={setFieldValue}
					error={errors.nibedakSadak}
					value={values['nibedakSadak']}
				/>
			</div>
			<div>
				<EbpsForm
					label={permitFormLang.petitioner_phone}
					name="applicantMobileNo"
					setFieldValue={setFieldValue}
					value={values['applicantMobileNo']}
					error={errors.applicantMobileNo}
				/>
			</div>
			<div>
				<LangDateField
					label={permitFormLang.petitioner_date}
					// name='applicantDate'
					// setFieldValue={setFieldValue}
					value={values.applicantDate}
					// error={errors.applicantDate}
					disabled={true}
				/>
			</div>
			<EbpsTextareaField
				labelName={permitFormLang.petitioner_additional}
				name="nibedakAdditional"
				setFieldValue={setFieldValue}
				value={values.name}
				// className={hasChanged ? (values['nibedakAdditional'] ? 'success' : 'error') : 'error'}
			/>
		</div>
	);
};
