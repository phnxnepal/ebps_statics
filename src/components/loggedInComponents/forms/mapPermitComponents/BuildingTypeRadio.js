import React from 'react';
import { buildingTypeOptions } from '../../../../utils/optionUtils';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { RadioInput } from '../../../shared/formComponents/RadioInput';

export const BuildingTypeRadio = ({
	name = 'buildingType',
	otherName = 'buildingTypeOther',
	space = false,
	fieldLabel,
	values,
	setFieldValue,
	errors,
}) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{buildingTypeOptions.map(option => (
				<React.Fragment key={option}>
					<RadioInput space={space} name={name} option={option} label={option} />
				</React.Fragment>
			))}

			{values[name] && values[name] === buildingTypeOptions[2] ? (
				<DashedLangInput name={otherName} setFieldValue={setFieldValue} value={values[otherName]} error={errors[otherName]} />
			) : null}
		</>
	);
};
