import React from 'react';
import { buildingRoadOptions } from '../../../../utils/optionUtils';
import { FormikCheckboxIm } from '../../../shared/FormikCheckbox';
import { DashedLangInput } from '../../../shared/DashedFormInput';

export const BuildingRoadCheckbox = ({
	name = 'buildingJoinRoadType',
	otherName = 'buildingJoinRoadTypeOther',
	space = false,
	fieldLabel,
	values,
	setFieldValue,
	errors,
}) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{buildingRoadOptions.map(option => (
				<React.Fragment key={option}>
					<FormikCheckboxIm space={space} name={name} label={option} />
				</React.Fragment>
			))}

			{values.buildingJoinRoadType && values.buildingJoinRoadType.includes(buildingRoadOptions[5]) ? (
				<DashedLangInput
					name={otherName}
					placeholder="Additional Information..."
					setFieldValue={setFieldValue}
					value={values.buildingJoinRoadTypeOther}
					error={errors.buildingJoinRoadTypeOther}
				/>
			) : null}
		</>
	);
};
