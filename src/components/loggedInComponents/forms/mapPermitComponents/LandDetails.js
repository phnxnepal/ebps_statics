import React from 'react';
import { Form } from 'semantic-ui-react';
import { FieldArray, getIn } from 'formik';
import EbpsForm, { PermitAreaInput } from '../../../shared/EbpsForm';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { landDetailsData } from '../../../../utils/data/mapPermit';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
export const LandDetails = ({ values, setFieldValue, errors, handleChange, form_step3 = permitFormLang.form_step3 }) => {
	return (
		<div>
			<FieldArray
				name="landDetails"
				render={(arrayHelpers) => (
					<div>
						{form_step3.landDetails}
						{errors.landDetails && <span className="tableError">{errors.landDetails}</span>}
						{values.landDetails && values.landDetails.length > 0 ? (
							values.landDetails.map((row, index) => (
								<div className="div-indent">
									<Form.Group inline className="land-details-form">
										<EbpsForm
											compactError={true}
											label={form_step3.kittaNo}
											name={`kittaNo.${index}`}
											error={getIn(errors, `kittaNo.${index}`)}
											value={getIn(values, `kittaNo.${index}`)}
											setFieldValue={setFieldValue}
										/>
										<PermitAreaInput
											label={form_step3.fieldName_5}
											placeholder={values.landAreaType}
											name={`landArea.${index}`}
											value={getIn(values, `landArea.${index}`)}
											error={getIn(errors, `landArea.${index}`)}
											onChange={handleChange}
										/>
										<Form.Button
											size="tiny"
											className="danger-text-button"
											content={landDetailsData.removeRow}
											onClick={() => arrayHelpers.remove(index)}
											icon="close"
										/>
										<Form.Button
											content={landDetailsData.addRow}
											size="tiny"
											className="light-text-button"
											onClick={() => {
												arrayHelpers.insert(index + 1, '');
												setFieldValue(`kittaNo.${index + 1}`, '');
												setFieldValue(`landArea.${index + 1}`, '');
											}}
											icon="plus"
										/>
									</Form.Group>
								</div>
							))
						) : (
							<div>
								<Form.Button
									content={landDetailsData.addNewRow}
									size="tiny"
									className="light-text-button"
									onClick={() => {
										arrayHelpers.push('');
										setFieldValue(`kittaNo.${0}`, '');
										setFieldValue(`landArea.${0}`, '');
										// setFieldValue(`landDetails.${0}`, '');
									}}
									icon="plus"
								/>
							</div>
						)}
					</div>
				)}
			/>
		</div>
	);
};
