import React from 'react';
import { constructionTypeOptions } from '../../../../utils/data/genericData';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { constructionTypeOptionsBiratnagar } from '../../../../utils/optionUtils';
import { DashedLangInput } from '../../../shared/DashedFormInput';

export const ConstructionTypeRadio = ({ name = 'constructionType', space = false, fieldLabel, options = constructionTypeOptions }) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{options.map((option) => (
				<RadioInput key={option.value} space={space} name={name} option={option.value} label={option.label} />
			))}
		</>
	);
};

export const ConstructionTypeRadioBiratnagar = ({
	name = 'constructionType',
	space = false,
	fieldLabel,
	values,
	otherName = 'constructionTypeOther',
	setFieldValue,
	errors,
}) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{constructionTypeOptionsBiratnagar.map((option) => (
				<RadioInput key={option.value} space={space} name={name} option={option.value} label={option.label} />
			))}

			{values[name] && values[name] === constructionTypeOptionsBiratnagar[8].value ? (
				<DashedLangInput name={otherName} setFieldValue={setFieldValue} value={values[otherName]} error={errors[otherName]} />
			) : null}
		</>
	);
};
