import React, { useState, useEffect } from 'react';
import { Form, Button, Label, Table, Grid } from 'semantic-ui-react';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { commonMessages, blockError } from '../../../../utils/data/validationData';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
const floorDetails = permitFormLang.form_step9.floor_details;

export const AddBlockFloorOption = ({ setBlockOption, setBlocks, blocks, removeBlock }) => {
	const [currentBlock, setCurrentBlock] = useState('');
	const [currentBlockError, setCurrentBlockError] = useState();

	useEffect(() => {
		if (blocks && blocks.length > 0) {
			setBlockOption(blocks.map((block) => ({ value: block, text: block, disabled: false })));
		}
	}, [blocks, setBlockOption]);

	return (
		<Grid columns="2">
			<Grid.Row>
				<Grid.Column mobile={16} tablet={11} computer={9}>
					<Form.Group inline>
						<Form.Field error={!!currentBlockError}>
							<span>{floorDetails.block}:</span>
							<input
								name="block"
								value={currentBlock}
								onChange={(e) => {
									setCurrentBlock(e.target.value);
									if (blocks.find((block) => block === e.target.value)) setCurrentBlockError(blockError.blockExists);
									else {
										setCurrentBlockError('');
									}
								}}
							/>
							{currentBlockError && (
								<Label prompt pointing size="large">
									{currentBlockError}
								</Label>
							)}
						</Form.Field>
						<Form.Field>
							<Button
								onClick={() => {
									if (isStringEmpty(currentBlock)) {
										setCurrentBlockError(commonMessages.required);
									} else if (isStringEmpty(currentBlockError)) {
										setBlocks([...blocks, currentBlock]);
										setCurrentBlock('');
										setCurrentBlockError('');
									}
								}}
							>
								{floorDetails.addBlocks}
							</Button>
						</Form.Field>
					</Form.Group>
				</Grid.Column>
				<Grid.Column mobile={16} tablet={5} computer={4}>
					{blocks && blocks.length > 0 ? (
						<Table key="blocks-table" celled compact="very" collapsing>
							<Table.Header>
								<Table.Row>
									<Table.HeaderCell colSpan="2">{floorDetails.availableBlocks}</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>
								{blocks.map((block) => (
									<Table.Row key={block}>
										<Table.Cell>{`${floorDetails.block} ${block}`}</Table.Cell>
										<Table.Cell>
											<Button
												className="icon-only-button"
												name="close"
												basic
												icon="delete"
												size="tiny"
												negative
												onClick={() => removeBlock(block)}
											/>
										</Table.Cell>
									</Table.Row>
								))}
							</Table.Body>
						</Table>
					) : (
						floorDetails.pleaseAddBlock
					)}
				</Grid.Column>
			</Grid.Row>
		</Grid>
	);
};
