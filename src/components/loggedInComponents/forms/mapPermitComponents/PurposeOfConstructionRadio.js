import React from 'react';
import {
	purposeOfConstructionOptions,
	purposeOfConstructionEnglishOptions,
	purposeOfConstructionEnglishOptionsV2,
} from '../../../../utils/optionUtils';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { DashedSelect } from '../../../shared/Select';

export const PurposeOfConstructionRadio = ({
	name = 'purposeOfConstruction',
	otherName = 'purposeOfConstructionOther',
	space = false,
	fieldLabel,
	values,
	setFieldValue,
	errors,
}) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{purposeOfConstructionOptions.map((option) => (
				<React.Fragment key={option}>
					<RadioInput space={space} name={name} option={option} label={option} />
				</React.Fragment>
			))}

			{values[name] && values[name] === purposeOfConstructionOptions[2] ? (
				<DashedLangInput name={otherName} setFieldValue={setFieldValue} value={values[otherName]} error={errors[otherName]} />
			) : null}
		</>
	);
};

export const PurposeOfConstructionEnglishDropdown = ({ name = 'buildingtype', otherName = 'buildingTypeOther', fieldLabel, values }) => {
	return (
		<>
			<DashedSelect label={fieldLabel} name={name} options={purposeOfConstructionEnglishOptions} />
			{values[name] && values[name] === purposeOfConstructionEnglishOptions[2].value ? (
				<DashedNormalInputIm name={otherName} placeholder="Additional Information" />
			) : null}
		</>
	);
};

export const PurposeOfConstructionEnglishRadio = ({
	name = 'purposeOfConstruction',
	otherName = 'purposeOfConstructionOther',
	space = false,
	fieldLabel,
	values,
}) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{purposeOfConstructionEnglishOptionsV2.map((option) => (
				<React.Fragment key={option.value}>
					<RadioInput space={space} name={name} option={option.value} label={option.text} />
				</React.Fragment>
			))}

			{values[name] && values[name] === purposeOfConstructionEnglishOptionsV2[3].value ? <DashedNormalInputIm name={otherName} /> : null}
		</>
	);
};
