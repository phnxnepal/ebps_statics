import React from 'react';
import { BuildPermitHelper, pronounOptions } from './BuildPermitHelpers';
import { SelectInput } from '../../../shared/Select';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

export const PermitSubjectParagraph = React.memo(
	({ applicantMs }) => {
		return (
			<div>
				<h3 className="app-subjectTitle">{permitFormLang.form_subject}</h3>
				<span className="mohodaye">{permitFormLang.form_mahodaya}</span>
				{/* <p> */}
				<div className="select-forForm">
					<SelectInput name="applicantMs" options={pronounOptions} />
				</div>
				{permitFormLang.form_content}
				{BuildPermitHelper.getMs(applicantMs)}
				{permitFormLang.form_content_1}
				{BuildPermitHelper.getMs(applicantMs)}
				{permitFormLang.form_content_2}
				{BuildPermitHelper.getMs(applicantMs, 'garnechu')}
				{permitFormLang.form_content_3}
			</div>
		);
	}
);
