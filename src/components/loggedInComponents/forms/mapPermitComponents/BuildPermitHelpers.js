import { isEmpty } from '../../../../utils/functionUtils';
import { selectPostValues } from '../../../../store/selectors/form/buildingPermit';
import { isArray } from 'util';
import { isStringEmpty } from '../../../../utils/stringUtils';

export const pronounOptions = [
	{ key: 'ma', value: 'मैले', text: 'मैले' },
	{ key: 'hami', value: 'हामीले', text: 'हामीले' },
	{ key: 'org', value: 'संघसंस्थाले', text: 'संघसंस्थाले' },
	{ key: 'org1', value: 'कार्यालयले', text: 'कार्यालयले' },
];

const pronounValue = {
	singular: {
		chu: 'छु ।',
		garnechu: 'गर्नेछु ।',
	},
	pural: {
		chu: 'छौं ।',
		garnechu: 'गर्नेछौं ।',
	},
};

export class BuildPermitHelper {
	static getMs(value, type = 'chu') {
		const option = pronounOptions.find((row) => row.value === value);
		if (option) {
			if (option.key === 'ma') {
				return pronounValue.singular[type];
			} else {
				return pronounValue.pural[type];
			}
		} else {
			return pronounValue.singular[type];
		}
	}

	static formatEditPostValues(values, marker) {
		const dataToSend = Object.assign({}, values);

		if (marker) {
			dataToSend.lat = marker.lat;
			dataToSend.lng = marker.lng;
		}

		if (Array.isArray(dataToSend.buildingJoinRoadType)) {
			dataToSend.buildingJoinRoadType = dataToSend.buildingJoinRoadType.join(',');
		}

		if (Array.isArray(dataToSend.mohada)) {
			dataToSend.mohada = dataToSend.mohada.join(',');
		}

		dataToSend.surrounding.forEach((side, index) => {
			if (side) {
				side.side = index + 1;
			}
		});

		dataToSend.surrounding = dataToSend.surrounding.filter((row, index) => !isEmpty(row) && (parseInt(row.side) < 5 || index < 4));

		dataToSend.floor.forEach((row, index) => {
			if (row) {
				row.floor = isStringEmpty(row.floor) ? index : row.floor;
				row.length = row.length || 0;
				row.width = row.width || 0;
				row.height = row.height || 0;
				row.area = row.area || 0;
				row.block = row.block || '';
				row.floorUnit = dataToSend.floorUnit;
			}
		});

		dataToSend.floor = dataToSend.floor.filter((fl) => !isEmpty(fl));
		if (dataToSend.citizenshipNo) dataToSend.nationalIdNo = dataToSend.citizenshipNo;

		dataToSend.kittaNo = dataToSend.kittaNo && isArray(dataToSend.kittaNo) ? dataToSend.kittaNo.join(', ') : dataToSend.kittaNo;
		dataToSend.landArea = dataToSend.landArea && isArray(dataToSend.landArea) ? dataToSend.landArea.join(', ') : dataToSend.landArea;
		delete dataToSend.landDetails;
		// data.newMunicipal = '';
		dataToSend.certificateArea = '';
		dataToSend.landCertificateNo = '';

		const filteredData = selectPostValues(dataToSend);

		return { filteredData, dataToSend };
	}

	static splitIntoArray(prevData, fieldName) {
		if (!Array.isArray(prevData[fieldName])) {
			if (prevData[fieldName] && prevData[fieldName].includes(',')) {
				return [prevData[fieldName].split(', '), true];
			} else {
				return [[prevData[fieldName]], false];
			}
		} else return prevData[fieldName];
	}
}
