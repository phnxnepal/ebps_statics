import React from 'react';
import EbpsNibedak from '../../../shared/EbpsNibedak';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import EbpsForm, { EbpsSlashForm } from '../../../shared/EbpsForm';
import { FatherSpouseComponent } from './FatherSpouseComponent';
import { isBiratnagar } from '../../../../utils/clientUtils';
import { LangDateField } from '../../../shared/DateField';
import { PermitSectionHeader } from './PermitSectionHeader';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

export const ApplicantDetails = ({ setFieldValue, errors, values }) => {
	return (
		<div>
			{isBiratnagar ? (
				<>
					<PermitSectionHeader content={permitFormLang.form_step1.heading} />
					<EbpsNibedak
						label={permitFormLang.form_step1.fieldName_1}
						name="applicantName"
						setFieldValue={setFieldValue}
						value={values.applicantName}
						error={errors.applicantName}
						coupledFieldName="nibedakName"
					/>
					<EbpsForm
						label={permitFormLang.form_step1.fieldName_2}
						name="applicantAddress"
						value={values.applicantAddress}
						setFieldValue={setFieldValue}
						error={errors.applicantAddress}
					/>
					<PermitSectionHeader content={permitFormLang.form_step1_birtnagar.naksawalaDetails} />
					<EbpsForm
						label={permitFormLang.form_step1_birtnagar.naksawalaName}
						name="naksawalaName"
						setFieldValue={setFieldValue}
						error={errors.naksawalaName}
						value={values.naksawalaName}
					/>
					<EbpsForm
						label={permitFormLang.form_step1_birtnagar.naksawalaAddress}
						name="naksawalaAddress"
						value={values.naksawalaAddress}
						setFieldValue={setFieldValue}
						error={errors.naksawalaAddress}
					/>
					<div>
						<EbpsSlashForm
							label={permitFormLang.form_step1_birtnagar.fieldName_3}
							name="citizenshipNo"
							setFieldValue={setFieldValue}
							error={errors.citizenshipNo}
							value={values.citizenshipNo}
						/>
						<LangDateField
							label={permitFormLang.form_step1_birtnagar.citizenshipIssueDate}
							name="citizenshipIssueDate"
							value={values.citizenshipIssueDate}
							setFieldValue={setFieldValue}
							error={errors.citizenshipIssueDate}
						/>
						<EbpsForm
							label={permitFormLang.form_step1_birtnagar.citizenshipIssueZone}
							name="citizenshipIssueZone"
							value={values.citizenshipIssueZone}
							setFieldValue={setFieldValue}
							error={errors.citizenshipIssueZone}
						/>
					</div>
					<FatherSpouseComponent
						labelHead={permitFormLang.form_step1_birtnagar.spouseHead}
						labelTail={permitFormLang.form_step1_birtnagar.spouseTail}
						setFieldValue={setFieldValue}
						errors={errors}
						values={values}
					/>
				</>
			) : (
				<>
					<PermitSectionHeader content={permitFormLang.form_step1.heading} />
					<EbpsNibedak
						label={permitFormLang.form_step1.fieldName_1}
						name="applicantName"
						setFieldValue={setFieldValue}
						error={errors.applicantName}
						value={values.applicantName}
						coupledFieldName="nibedakName"
					/>
					<EbpsForm
						label={permitFormLang.form_step1.fieldName_2}
						name="applicantAddress"
						value={values.applicantAddress}
						setFieldValue={setFieldValue}
						error={errors.applicantAddress}
					/>
					<EbpsSlashForm
						label={permitFormLang.form_step1.fieldName_3}
						name="citizenshipNo"
						setFieldValue={setFieldValue}
						error={errors.citizenshipNo}
						value={values.citizenshipNo}
					/>
					<FatherSpouseComponent
						labelHead={permitFormLang.form_step1.spouseHead}
						labelTail={permitFormLang.form_step1.spouseTail}
						setFieldValue={setFieldValue}
						errors={errors}
						values={values}
					/>
				</>
			)}
		</div>
	);
};
