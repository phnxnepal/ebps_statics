import React from 'react';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { LangDateField } from '../../../shared/DateField';
import { isKamalamai, isBiratnagar } from '../../../../utils/clientUtils';
import { landAreaTypeOptionsKamalamai, landAreaTypeOptions, distanceOptions } from '../../../../utils/optionUtils';
import { SelectInput } from '../../../shared/Select';
import { LandAreaField } from './LandAreaField';
import { LandDetails } from './LandDetails';
import EbpsForm from '../../../shared/EbpsForm';
import { KamalamaiSurroundingSection } from './KamalamaiSurroundingSection';
import { LabelValueIm } from '../../../shared/EbpsUnitLabelValue';
import { getIn } from 'formik';
import { PermitSectionHeader } from './PermitSectionHeader';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

export const LocationDetails = ({ setFieldValue, errors, values, handleChange, landPropsName }) => {
	const step4 = isBiratnagar ? permitFormLang.form_step4_birtanagar : permitFormLang.form_step4;
	const step3 = isBiratnagar ? permitFormLang.form_step3_biratnagar : permitFormLang.form_step3;
	return (
		<div>
			{isBiratnagar ? (
				<PermitSectionHeader content={permitFormLang.form_step4.heading} />
			) : (
				<PermitSectionHeader content={permitFormLang.form_step3.heading} />
			)}
			<LangDateField
				label={step3.fieldName_1}
				name="landPassDate"
				setFieldValue={setFieldValue}
				value={values.landPassDate}
				error={errors.landPassDate}
			/>
			<SelectInput label={step3.fieldName_3} options={landPropsName} name="ownershipName" />
			<LandAreaField
				label={step3.fieldName_4}
				options={isKamalamai ? landAreaTypeOptionsKamalamai : landAreaTypeOptions}
				needsConversion={!isKamalamai}
				isMultiple={true}
			/>
			<LandDetails setFieldValue={setFieldValue} handleChange={handleChange} values={values} errors={errors} form_step3={step3} />
			<EbpsForm label={step3.fieldName_6} name="landRegNo" setFieldValue={setFieldValue} value={values.landRegNo} error={errors.landRegNo} />
			<LangDateField
				label={step3.fieldName_7}
				name="landCertificateIssueDate"
				setFieldValue={setFieldValue}
				value={values.landCertificateIssueDate}
				error={errors.landCertificateIssueDate}
			/>

			{/* Section 4 --- */}
			{isKamalamai ? (
				<KamalamaiSurroundingSection setFieldValue={setFieldValue} values={values} errors={errors} />
			) : (
				<>
					{isBiratnagar ? (
						<div>{permitFormLang.form_step4_birtanagar.heading}</div>
					) : (
						<PermitSectionHeader content={permitFormLang.form_step4.heading} />
					)}
					{Object.values(step4.directionsLabel).map((direction, index) => (
						<div key={index}>
							<LabelValueIm
								labelName={direction}
								name={`surrounding[${index}].feet`}
								options={distanceOptions}
								unitName={`surrounding[${index}].sideUnit`}
								blurFieldName={`surrounding[${index}].side`}
								blurFieldValue={index + 1}
							/>
							<EbpsForm
								label={step4.fieldName_poll_common1}
								name={`surrounding[${index}].kittaNo`}
								setFieldValue={setFieldValue}
								error={getIn(errors, `surrounding[${index}].kittaNo`)}
								value={getIn(values, `surrounding[${index}].kittaNo`)}
							/>
							<EbpsForm
								label={step4.fieldName_poll_common2}
								name={`surrounding[${index}].sandhiyar`}
								setFieldValue={setFieldValue}
								error={getIn(errors, `surrounding[${index}].sandhiyar`)}
								value={getIn(values, `surrounding[${index}].sandhiyar`)}
							/>
						</div>
					))}
				</>
			)}
		</div>
	);
};
