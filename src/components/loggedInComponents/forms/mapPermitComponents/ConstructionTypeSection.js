import React from 'react';
import { isBiratnagar, isKamalamai } from '../../../../utils/clientUtils';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { chaChainaWithNValueOptions } from '../../../../utils/optionUtils';
import { YesNoOnlyRadio } from '../formComponents/YesNoRadio';
import { ConstructionTypeRadio, ConstructionTypeRadioBiratnagar } from './ConstructionTypeRadio';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
export function ConstructionTypeSection({props}) {
	return (
		<div>
			<div className="frmCheckbox-wrap">
				{isBiratnagar ? (
					<ConstructionTypeRadioBiratnagar
						values={props.values}
						errors={props.errors}
						otherName="constructionTypeOther"
						setFieldValue={props.setFieldValue}
						fieldLabel={permitFormLang.form_step5.fieldName_2}
					/>
				) : (
					<ConstructionTypeRadio fieldLabel={permitFormLang.form_step5.fieldName_2} />
				)}
			</div>
			<div className="frmCheckbox-wrap">
				{isKamalamai ? (
					<YesNoOnlyRadio name="earthquake" fieldLabel={permitFormLang.form_step5.isEarthquakeResistant} options={chaChainaWithNValueOptions} />
				) : null}
			</div>
		</div>
	);
}
