import React from 'react';

import { Form, Divider, List, Label } from 'semantic-ui-react';
import { Field, getIn } from 'formik';
import { getDocUrl } from '../../../../utils/config';
import { getSplitUrl } from '../../../../utils/functionUtils';
import Viewer from 'react-viewer';
import FallbackComponent from '../../../shared/FallbackComponent';
import { NIBEDAK_PHOTO_TITLE } from '../../../../utils/constants';

import { validateFileField, validateOptionalFileField } from '../../../../utils/validationUtils';
import { ImageDisplayPreview } from '../../../shared/file/FileView';
import { isStringEmpty } from '../../../../utils/stringUtils';

const getValidationSchema = (isRequired, hasFiles) => {
	if (hasFiles) {
		return validateOptionalFileField;
	} else if (isRequired) {
		return validateFileField;
	} else {
		return validateOptionalFileField;
	}
}

class NibdedakPhotoAdd extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			imageOpen: false,
			src: '',
			existingPhoto: '',
			imageHash: Date.now(),
		};
	}

	render() {
		const { permitData, loading, error, values, setFieldValue } = this.props;

		const { imageHash } = this.state;
		if (permitData) {
			const photo = permitData.photo;
			// console.log('photo', this.props, photo);

			// let pdfValidate = '';
			let fileTitleName = '';

			fileTitleName = getSplitUrl(photo, '/');

			return (
				<>
					<div>
						<Divider horizontal>{NIBEDAK_PHOTO_TITLE}</Divider>{' '}
						<Form.Field error={!!error}>
							<Field
								type="file"
								name="photo"
								id="file_photo"
								multiple={false}
								value={undefined}
								onChange={e => {
									const arrFiles = Array.from(e.target.files);
									setFieldValue('photo', arrFiles);
									const multipleFiles = arrFiles.map((file, index) => {
										const src = window.URL.createObjectURL(file);
										return { file, id: index, src };
									});

									setFieldValue(`imagePreviewUrl._photo`, multipleFiles);
								}}
								validate={getValidationSchema(true, !isStringEmpty(photo))}
							/>
							{error && (
								<Label pointing prompt size="large">
									{error}
								</Label>
							)}
							{getIn(values, `imagePreviewUrl._photo`) && (
								<div className="previewFileUpload">
									{getIn(values, `imagePreviewUrl._photo`).map((value, index) => (
										<div key={index} className={'eachPreviewFile'}>
											<ImageDisplayPreview
												isInput={true}
												src={value.src}
												alt={value.file.name}
												type={value.file.type}
												title={value.file.name}
											/>
										</div>
									))}
								</div>
							)}
						</Form.Field>
						<List relaxed>
							<List.Header>Existing Files</List.Header>
							<div className="previewFileUpload">
								{photo ? (
									<div key={photo} className="eachPreviewFile viewFileSuccess" title={fileTitleName}>
										<img
											src={`${getDocUrl()}${photo}?${imageHash}`}
											alt="imageFile"
											onClick={() =>
												this.setState({
													imageOpen: true,
													src: `${getDocUrl()}${photo}?${imageHash}`,
												})
											}
										/>
									</div>
								) : (
									<List.Item>No files uploaded.</List.Item>
								)}
							</div>
						</List>
						<Viewer
							visible={this.state.imageOpen}
							onClose={() => this.setState({ imageOpen: false })}
							images={[
								this.state.src
									? { src: this.state.src, alt: 'nibedak photo' }
									: { src: `${getDocUrl()}${this.state.existingPhoto}`, alt: 'nibedak photo' },
							]}
							zIndex={10000}
						/>
					</div>
				</>
			);
		} else {
			return <FallbackComponent loading={loading} errors={error} />;
		}
	}
}

export default NibdedakPhotoAdd;
