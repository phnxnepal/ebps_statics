import React from 'react';
// import { CoupledInputField } from '../../../shared/EbpsNibedak';
import EbpsForm, { EbpsCoupledForm } from '../../../shared/EbpsForm';
import { buildingRoadOptions } from '../../../../utils/optionUtils';
import FormikCheckbox from '../../../shared/FormikCheckbox';
import { Select, Form, Label } from 'semantic-ui-react';
import { SelectInput } from '../../../shared/Select';
import { PermitSectionHeader } from './PermitSectionHeader';

export const WardAddressSection = ({ data, newMunicipal, values, setFieldValue, errors, wardOptions }) => {
	return (
		<div className="margin-bottom">
			<PermitSectionHeader content={data.heading} />
			{/* <CoupledInputField label={`${data.fieldName_1}${newMunicipal}${data.fieldName_1_1}`} name="newWardNo" coupledFields={['nibedakTol']} /> */}
			<Form.Group widths="equal">
				<Form.Field error={!!errors.newWardNo}>
					<span>{`${data.fieldName_1}${newMunicipal}${data.fieldName_1_1}`}</span>
					<Select
						wrapSelection={true}
						name="newWardNo"
						options={wardOptions}
						onChange={(e, { value }) => {
							setFieldValue('newWardNo', value);
							setFieldValue('nibedakTol', value);
						}}
						value={values.newWardNo}
					/>
					{errors.newWardNo && (
						<Label pointing prompt size="large">
							{errors.newWardNo}
						</Label>
					)}
				</Form.Field>
			</Form.Group>
			<EbpsForm
				label={data.fieldName_2}
				name="oldMunicipal"
				setFieldValue={setFieldValue}
				value={values.oldMunicipal}
				error={errors.oldMunicipal}
			/>
			{/* <EbpsNormalFormIm label={data.fieldName_3} name="oldWardNo" /> */}
			<SelectInput label={data.fieldName_3} name="oldWardNo" compact={true} options={wardOptions} />
			<EbpsForm label={data.fieldName_3_1} name="sadak" setFieldValue={setFieldValue} value={values.sadak} error={errors.sadak} />
			<EbpsCoupledForm
				label={data.fieldName_4}
				name="nearestLocation"
				setFieldValue={setFieldValue}
				value={values.nearestLocation}
				error={errors.nearestLocation}
				otherField={'nibedakSadak'}
			/>
			<EbpsForm
				label={data.fieldName_5}
				name="buildingJoinRoad"
				setFieldValue={setFieldValue}
				value={values.buildingJoinRoad}
				error={errors.buildingJoinRoad}
			/>

			<div>
				<label>{data.fieldName_6}</label>
				{buildingRoadOptions.map((option) => (
					<React.Fragment key={option}>
						<FormikCheckbox space={true} name="buildingJoinRoadType" value={option} />
					</React.Fragment>
				))}

				{values.buildingJoinRoadType && values.buildingJoinRoadType.includes(buildingRoadOptions[5]) ? (
					<EbpsForm
						name="buildingJoinRoadTypeOther"
						setFieldValue={setFieldValue}
						value={values.buildingJoinRoadTypeOther}
						error={errors.buildingJoinRoadTypeOther}
					/>
				) : null}
			</div>
		</div>
	);
};
