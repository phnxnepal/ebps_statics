import React, { useState, useEffect } from 'react';
import { Table, Button } from 'semantic-ui-react';
import { FieldArray, getIn } from 'formik';
import { floorOptions } from '../../../../utils/dataUtils';
// import UnitDropdown from './../../../shared/UnitDropdown';
import { TableInputField } from './../../../shared/TableInput';
import { calculateArea } from '../../../../utils/mathUtils';
import { buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { validateNumberField } from '../../../../utils/validationUtils';
// import { isStringEmpty } from '../../../../utils/stringUtils';
// import { commonMessages, blockError } from '../../../../utils/data/validationData';
import { AddBlockFloorOption } from './AddBlockFloorOption';
import { FloorTableHeader } from './FloorTable';
// import { intersectionBy } from 'lodash';

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;
const floorDetails = permitFormLang.form_step9.floor_details;

export const CommercialTable = ({ handleChange, setFieldValue, values, errors, initialBlocks = [], isDisabled, isGroundFloor }) => {
	const [blockOption, setBlockOption] = useState([]);
	const [floorOption, setFloorOption] = useState(floorOptions);
	const [combinations, setCombinations] = useState([]);
	const [blocks, setBlocks] = useState(initialBlocks);

	// useEffect(() => {
	// let existingBlock = {};

	// floorOptions.forEach(flMaster => {
	// 	const flArray = values.floor ? values.floor.filter(f => String(f.floor) === String(flMaster.value)) : [];

	// 	if (flArray === undefined || flArray.length < 1) {
	// 		existingBlock[`_${flMaster.value}`] = [];
	// 	} else {
	// 		flArray.forEach(fl => {
	// 			if (fl && fl.floor !== undefined && fl.block !== undefined) {
	// 				if (existingBlock && existingBlock[`_${fl.floor}`]) {
	// 					existingBlock[`_${fl.floor}`].push(fl.block);
	// 				} else {
	// 					existingBlock[`_${fl.floor}`] = [fl.block];
	// 				}
	// 			}
	// 		});
	// 	}
	// });
	// // console.log('exis block after', existingBlock, 'input == ', Object.values(existingBlock));
	// // console.log('%c result ==', 'background: red; color: white', intersectionBy(...Object.values(existingBlock)));

	// const intersectedBlocks = intersectionBy(...Object.values(existingBlock));

	// const existingFloors = Object.entries(existingBlock)
	// 	.filter(bl => {
	// 		return bl[1].length === blocks.length;
	// 	})
	// 	.map(([floor]) => Number(floor.replace('_', '')));

	// setFloorOption(floorOptions.map(fl => ({ ...fl, disabled: existingFloors.includes(fl.value) })));
	// setBlockOption(blocks.map(bl => ({ value: bl, text: bl, disabled: intersectedBlocks.includes(bl) })));
	// }, [values.floor, blocks]);

	useEffect(() => {
		let combinations = [];
		floorOptions.forEach((fl) => {
			blocks.forEach((bl) => combinations.push([fl.value, bl]));
		});
		const available = combinations.filter((com) => {
			const condition = !values.floor.some((flV) => flV && String(flV.floor) === String(com[0]) && flV.block === com[1]);
			return condition;
		});

		setFloorOption(floorOptions.map((fl) => ({ ...fl, disabled: !available.some(([floor]) => String(floor) === String(fl.value)) })));
		setBlockOption(blocks.map((bl) => ({ value: bl, text: bl, disabled: !available.some(([_, block]) => String(block) === String(bl)) })));
		setCombinations(available);
	}, [blocks, values.floor]);

	const removeBlock = (blockToRemove) => {
		const floors = values.floor
		const filteredFloors = floors.filter(floor => floor.block === blockToRemove);
		setBlocks((blocks) => blocks.filter((blk) => blk !== blockToRemove));
		setFieldValue('floor', filteredFloors)
	};

	return (
		<>
			<AddBlockFloorOption setBlockOption={setBlockOption} setBlocks={setBlocks} removeBlock={removeBlock} blocks={blocks} />
			<Table celled compact="very" striped style={{ maxWidth: '1200px' }}>
				<FloorTableHeader errors={errors} values={values} setFieldValue={setFieldValue} isCommercial={true} />
				<FieldArray
					name="floor"
					render={(arrayHelpers) => (
						<Table.Body>
							{values.floor && values.floor.length > 0 ? (
								values.floor.map((row, index) => (
									<Table.Row key={index}>
										<Table.Cell textAlign="center">
											<>
												<div>
													{floorDetails.floor} {`(`}
													<select
														className="ui dropdown floor-select"
														name={`floor[${index}].floor`}
														onChange={(e) => setFieldValue(`floor[${index}].floor`, e.target.value)}
														value={getIn(values, `floor[${index}].floor`) || null}
													>
														{floorOption.map((option) => (
															<option
																key={option.value}
																disabled={option.disabled}
																value={option.value}
																className="item"
															>
																{option.text}
															</option>
														))}
													</select>{' '}
													{' ) '}
												</div>{' '}
												<div>
													{floorDetails.block} {`(`}
													<select
														className="ui dropdown floor-select"
														name={`floor[${index}].block`}
														onChange={(e) => setFieldValue(`floor[${index}].block`, e.target.value)}
														value={getIn(values, `floor[${index}].block`)}
													>
														{blockOption.map((option) => (
															<option
																key={option.value}
																disabled={option.disabled}
																value={option.value}
																className="item"
															>
																{option.text}
															</option>
														))}
													</select>{' '}
													{' ) '}
												</div>{' '}
												{/* {floorDetails.block}{' '}
													<div className="brackets english-div" style={{ display: 'inline-flex' }}>
														{`(`}
														<Dropdown
															name={`floor[${index}].block`}
															onChange={(e, { value }) => {
																setFieldValue(`floor[${index}].block`, value);
															}}
															value={getIn(values, `floor[${index}].block`)}
															options={blockOption}
														/>
														{' ) '}
													</div>{' '} */}
											</>
											{/* {' - '}
												<>
													{floorDetails.floor}{' '}
													<div className="brackets" style={{ display: 'inline-flex' }}>
														{`(`}
														<Dropdown
															name={`floor[${index}].floor`}
															onChange={(e, { value }) => setFieldValue(`floor[${index}].floor`, value)}
															value={getIn(values, `floor[${index}].floor`)}
															options={floorOption}
														/>
														{' ) '}
													</div>{' '}
												</> */}
										</Table.Cell>
										<Table.Cell>
											<TableInputField
												name={`floor[${index}].length`}
												onChange={(e) => {
													setFieldValue(`floor[${index}].length`, e.target.value);
													setFieldValue(
														`floor[${index}].area`,
														calculateArea(e.target.value, getIn(values, `floor[${index}].width`))
													);
												}}
												value={getIn(values, `floor[${index}].length`)}
												error={getIn(errors, `floor[${index}].length`)}
											/>
										</Table.Cell>
										<Table.Cell>
											<TableInputField
												name={`floor[${index}].width`}
												onChange={(e) => {
													setFieldValue(`floor[${index}].width`, e.target.value);
													setFieldValue(
														`floor[${index}].area`,
														calculateArea(e.target.value, getIn(values, `floor[${index}].length`))
													);
												}}
												value={getIn(values, `floor[${index}].width`)}
												error={getIn(errors, `floor[${index}].width`)}
											/>
										</Table.Cell>
										<Table.Cell>
											<TableInputField
												disabled={isDisabled}
												name={`floor[${index}].height`}
												value={getIn(values, `floor[${index}].height`)}
												onChange={isDisabled ? undefined : handleChange}
												error={getIn(errors, `floor[${index}].height`)}
												validate={isGroundFloor ? validateNumberField : undefined}
											/>
										</Table.Cell>
										<Table.Cell>
											<TableInputField
												disabled={isDisabled}
												name={`floor[${index}].area`}
												value={getIn(values, `floor[${index}].area`)}
												onChange={isDisabled ? undefined : handleChange}
												error={getIn(errors, `floor[${index}].area`)}
												validate={isGroundFloor ? validateNumberField : undefined}
											/>
										</Table.Cell>
										<Table.Cell textAlign="center">
											<div>
												<Button size="tiny" onClick={() => arrayHelpers.remove(index)} icon="minus" />
												<Button
													size="tiny"
													disabled={!floorOption.some((fl) => !fl.disabled)}
													title={
														!floorOption.some((fl) => !fl.disabled)
															? floorDetails.allFloorsAdded
															: floorDetails.commercialActions
													}
													onClick={() => {
														arrayHelpers.insert(index + 1, '');
														setFieldValue(`floor.${index + 1}`, {
															// floor: String(floorOption.find(fl => !fl.disabled).value),
															// block: blockOption.find(fl => !fl.disabled).value,
															floor: combinations && combinations[0][0],
															block: combinations && combinations[0][1],
														});
													}}
													icon="plus"
												/>
											</div>
										</Table.Cell>
									</Table.Row>
								))
							) : (
								<Table.Cell colSpan="16" textAlign="center">
									<Button
										size="medium"
										disabled={!floorOption.some((fl) => !fl.disabled)}
										title={!floorOption.some((fl) => !fl.disabled) ? floorDetails.allFloorsAdded : floorDetails.commercialActions}
										onClick={() => {
											arrayHelpers.push({});
											setFieldValue(`floor.${0}`, {
												// floor: String(floorOption.find(fl => !fl.disabled).value),
												// block: blockOption.find(fl => !fl.disabled).value,
												floor: combinations && combinations[0][0],
												block: combinations && combinations[0][1],
											});
										}}
									>
										{floorDetails.commercialActions}
									</Button>
								</Table.Cell>
							)}
						</Table.Body>
					)}
				/>
			</Table>
		</>
	);
};
