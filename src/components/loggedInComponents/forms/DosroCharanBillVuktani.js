import React from 'react';
import api from '../../../utils/api';
import { checkError } from '../../../utils/dataUtils';
import { GenericApprovalFileView } from '../../shared/file/GenericApprovalFileView';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { GenericBillVulktani } from './formComponents/GenericBillVulktani';
import { isKamalamai } from '../../../utils/clientUtils';
import { ApiParam } from '../../../utils/paramUtil';

const apis = isKamalamai
	? [{ api: api.dosroVuktani, objName: 'dosroBill', form: true }, new ApiParam(api.sansodhanSuperTippaniAdesh, 'rajasowTippani').getParams()]
	: [{ api: api.dosroVuktani, objName: 'dosroBill', form: true }];

const dosroVukatani = (parentProps) => (
	<FormContainerV2
		api={apis}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={(props) =>
			props.hasSavePermission ? (
				<GenericBillVulktani
					{...props}
					parentProps={parentProps}
					api={api.dosroVuktani}
					titleKey="dosroCharanBill"
					rajasowData={checkError(props.otherData.rajasowTippani)}
				/>
			) : (
				<div ref={props.setRef}>
					<GenericApprovalFileView
						fileCategories={props.fileCategories}
						files={props.files}
						url={props.formUrl}
						prevData={checkError(props.prevData)}
					/>
				</div>
			)
		}
	/>
);

export default dosroVukatani;
