import React, { Component } from 'react';
import { Formik } from 'formik';
import { DashedLangDateField } from '../../../shared/DateField';

import { Form, Message, Grid, Dropdown, Label } from 'semantic-ui-react';
import { anushuchi_gha } from '../../../../utils/data/mockLangFile';

import { isEmpty } from '../../../../utils/functionUtils';
import { DashedLangInput, DashedLangInputWithSlash, DashedNormalInput } from '../../../shared/DashedFormInput';

import { getMessage } from '../../../shared/getMessage';
import { SemanticToastContainer } from 'react-semantic-toasts';
import api from '../../../../utils/api';
import { checkError, filterKeys, handleSuccess } from '../../../../utils/dataUtils';
import { translateDate } from '../../../../utils/langUtils';
import { getCurrentDate, getNepaliDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { ImageDisplayInline } from '../../../shared/file/FileView';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { floorData } from '../../../../utils/data/genericData';
import { anusuchiGhaSchema, kamalamaiAnuGhaSchema, kagesworiGhaSchema } from '../../formValidationSchemas/anusuchiGha';
import { isKamalamai, isKageyshowri } from '../../../../utils/clientUtils';
import { SignatureImage } from '../formComponents/SignatureImage';

const data = anushuchi_gha.anushuchiGhaView;
const messageId = 'anusuchi_gha.anushuchiGhaView';

const initialValues = {
	thapTalla: '',
	// Anyya designer fields
	dName: '',
	dAdd: '',
	dDesignation: '',
	dMobile: '',
	dMunicipalRegNo: '',
	dConsultancyName: '',
	dEngineeringCouncil: '',
	// thekedar fields
	tAddress: '',
	tDarta: '',
	tPhone: '',
	thekdarName: '',
	// dakarmi fields
	mAddress: '',
	mDarta: '',
	mistiriName: '',
	mPhone: '',
};

class AnushuchiGhaComponent extends Component {
	constructor(props) {
		super(props);

		let anusuchiGha_data = {};

		const floorArray = new FloorBlockArray(this.props.permitData.floor);
		const topFloors = floorArray.getTopFloor();
		const floorMax = floorArray.formatBlockFloorData(topFloors, { prefix: floorData.block, mid: floorData.ma, suffix: floorData.talla });
		if (this.props.prevData) {
			anusuchiGha_data = checkError(this.props.prevData);
			anusuchiGha_data.enterBy = filterKeys(
				['userName', 'educationQualification', 'licenseNo', 'address', 'mobile', 'municipalRegNo', 'signature'],
				anusuchiGha_data.enterBy,
				anusuchiGha_data.dName
			);
			if (isStringEmpty(anusuchiGha_data.dDate)) {
				anusuchiGha_data.dDate = getCurrentDate(true);
			}
		}

		const designerinfo = checkError(this.props.otherData.anusuchiGaGha);

		const dUserName = designerinfo.enterBy ? designerinfo.enterBy.userName : '';

		const supOptions = [
			{ key: 'super', value: dUserName, text: dUserName },
			{ key: 'others', value: 'other', text: 'अन्य (तल उल्लेख गर्नुहोस)' },
		];
		this.state = {
			printcontent: this.props.printcontent,
			anusuchiGha_data,
			initialValues: { supervisorName: dUserName, ...initialValues, ...anusuchiGha_data },
			supOptions,
			designerinfo,
			floorMax,
		};
	}

	render() {
		const { supOptions, initialValues, designerinfo, floorMax, anusuchiGha_data } = this.state;
		const { staticFiles, useSignatureImage } = this.props;
		const user_info = this.props.userData;
		const detailed_Info = this.props.permitData;
		const building_class = this.props.otherData.designApprovalGha;

		return (
			<div className="anusuchigha-Form">
				<Formik
					initialValues={initialValues}
					enableReinitialize={true}
					validationSchema={isKamalamai ? kamalamaiAnuGhaSchema : isKageyshowri ? kagesworiGhaSchema : anusuchiGhaSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);

						values.applicationNo = designerinfo.applicationNo;
						if (values.supervisorName !== 'other') {
							values.dName = values.dDesignation = values.dDate = null;
						}

						try {
							await this.props.postAction(`${api.anusuchiGha}${this.props.permitData.applicantNo}`, values);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={(props) => {
						return (
							<Form loading={props.isSubmitting}>
								<SemanticToastContainer />

								{!isEmpty(this.props.errors) && (
									<Message negative>
										<Message.Header>Error</Message.Header>
										<p>{this.props.errors.message}</p>
									</Message>
								)}
								{/* {isEmpty(designerinfo) && (
									<Message negative>
										<Message.Header>Error</Message.Header>
										<p>Anushuchi Ga not filled.</p>
									</Message>
								)} */}
								<div ref={this.props.setRef}>
									<FormHeading />
									{getMessage(`${messageId}.gha_data_1`, data.gha_data_1)}
									<span>{detailed_Info.nibedakName}</span>
									{getMessage(`${messageId}.gha_data_2`, data.gha_data_2)}
									<span>{user_info.organization.name}</span>
									{getMessage(`${messageId}.gha_data_3`, data.gha_data_3)}
									<span>{detailed_Info.oldMunicipal}</span>
									{getMessage(`${messageId}.gha_data_4`, data.gha_data_4)}
									<span>{detailed_Info.newWardNo}</span>
									{getMessage(`${messageId}.gha_data_5`, data.gha_data_5)}
									<span>{detailed_Info.buildingJoinRoad}</span>
									{getMessage(`${messageId}.gha_data_6`, data.gha_data_6)}
									<span>{detailed_Info.kittaNo}</span>
									{getMessage(`${messageId}.gha_data_7`, data.gha_data_7)}
									<span>{detailed_Info.landArea}</span> <span>{detailed_Info.landAreaType}</span>
									{getMessage(`${messageId}.gha_data_8`, data.gha_data_8)}
									<span>{building_class.buildingClass}</span>
									{getMessage(`${messageId}.gha_data_9`, data.gha_data_9)}
									<span
										className="anusuchighaSpecial"
										style={{
											marginLeft: '0.8rem',
											marginRight: '0.8rem',
										}}
									>
										<Dropdown
											// placeholder={getMessage(
											//   `${messageId}.gha_supervisorChoose`,
											//   data.gha_supervisorChoose
											// )}
											value={props.values['supervisorName']}
											name="supervisorName"
											selection
											options={supOptions}
											onChange={(e, { value }) => {
												props.setFieldValue('supervisorName', value);
											}}
										/>
									</span>
									{getMessage(`${messageId}.gha_data_10`, data.gha_data_10)}
									{getMessage(`${messageId}.gha_data_11`, data.gha_data_11)}
									{floorMax}
									{/* {getMessage(`${messageId}.gha_data_12`, data.gha_data_12)} */}

									<span>{getMessage(`${messageId}.gha_maindata`, data.gha_maindata)}</span>
									<Grid>
										<Grid.Row>
											<Grid.Column width={8}>
												<FormHouseOwnerDetails
													ownerInfo={detailed_Info}
													ghardhaniSignature={staticFiles.ghardhaniSignature}
												/>
											</Grid.Column>
											<Grid.Column width={8}>
												<FormSupervisorDetails
													designerinfo={designerinfo}
													statee={this.props.printcontent}
													// dUname={props.values.supervisorName}
													{...props}
												/>
											</Grid.Column>
											<Grid.Column width={16}>
												<FormContractorDetails
													contractorinfo={anusuchiGha_data}
													statee={this.props.printcontent}
													dakarmiSignature={staticFiles.dakarmiSignature}
													nirmanByabasayeSignature={staticFiles.nirmanByabasayeSignature}
													useSignatureImage={useSignatureImage}
													{...props}
												/>
											</Grid.Column>
										</Grid.Row>
									</Grid>
									{/* </FormWrapper> */}
								</div>

								<br />
								<SaveButtonValidation
									errors={props.errors}
									formUrl={this.props.formUrl}
									isSaveDisabled={this.props.isSaveDisabled}
									hasDeletePermission={this.props.hasDeletePermission}
									hasSavePermission={this.props.hasSavePermission}
									prevData={checkError(this.props.prevData)}
									handleSubmit={props.handleSubmit}
									validateForm={props.validateForm}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const FormHeading = () => (
	<>
		<span style={{ textAlign: 'center' }}>
			<h2>{getMessage(`${messageId}.gha_heading`, data.gha_heading)}</h2>

			<h3>{getMessage(`${messageId}.gha_subheading1`, data.gha_subheading1)}</h3>
		</span>
	</>
);

const FormHouseOwnerDetails = (props) => {
	const OwnerData = props.ownerInfo;
	return (
		<>
			<span>
				{getMessage(`${messageId}.gha_ownerName`, data.gha_ownerName)}
				<span>{OwnerData.nibedakName}</span>
				<br />
				{/* `${permitData.newMunicipal} - ${permitData.nibedakTol}` */}
				{getMessage(`${messageId}.gha_ownerAdd`, data.gha_ownerAdd)}
				<span>
					{OwnerData.newMunicipal} - {OwnerData.nibedakTol}
				</span>
				<br />
				{getMessage(`${messageId}.gha_ownerPh`, data.gha_ownerPh)}
				<span>{OwnerData.applicantMobileNo}</span>
				<br />
				<SignatureImage
					value={props.ghardhaniSignature}
					showSignature={true}
					label={getMessage(`${messageId}.gha_ownerSignature`, data.gha_ownerSignature)}
				/>
				{getMessage(`${messageId}.gha_odate`, data.gha_odate)}
				<span>{translateDate(OwnerData.applicantDateBS)}</span>

				{/* <span>{OwnerData.applicantDate}</span> */}
			</span>
		</>
	);
};

const FormSupervisorDetails = (props) => {
	// for designer supervisor
	const {
		userName: userName = '',
		educationQualification: educationQualification = '',
		licenseNo: licenseNo = '',
		consultancyName: consultancyName = '',
		address: address = '',
		mobile: mobile = '',
		municipalRegNo: municipalRegNo = '',
		signature: signature = '',
	} = props.designerinfo && props.designerinfo.enterBy ? props.designerinfo.enterBy : {};

	const { enterDate = '' } = props.designerinfo && props.designerinfo ? props.designerinfo : {};

	// for other supervisior
	// const supervisorName = props.dUname;
	// console.log(props);

	return (
		<>
			<span>
				{getMessage(`${messageId}.gha_supervisor`, data.gha_supervisor)}
				<br />
				{getMessage(`${messageId}.gha_supName`, data.gha_supName)}

				{props.values.supervisorName === 'other' ? (
					<DashedLangInput name="dName" setFieldValue={props.setFieldValue} value={props.values['dName']} error={props.errors.dName} />
				) : (
					<span>{userName}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_supDesignation`, data.gha_supDesignation)}

				{props.values.supervisorName === 'other' ? (
					<DashedLangInput name="dDesignation" setFieldValue={props.setFieldValue} value={props.values['dDesignation']} />
				) : (
					<span>{educationQualification}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_supEngineeringCouncil`, data.gha_supEngineeringCouncil)}

				{props.values.supervisorName === 'other' ? (
					<>
						<DashedNormalInput name="dEngineeringCouncil" handleChange={props.handleChange} value={props.values['dEngineeringCouncil']} />

						{props.errors.dEngineeringCouncil && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.dEngineeringCouncil}
							</Label>
						)}
					</>
				) : (
					<span>{licenseNo}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_consultancyName`, data.gha_consultancyName)}
				{' : '}
				{props.values.supervisorName === 'other' ? (
					<>
						<DashedNormalInput name="dConsultancyName" handleChange={props.handleChange} value={props.values['dConsultancyName']} />

						{props.errors.dConsultancyName && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.dConsultancyName}
							</Label>
						)}
					</>
				) : (
					<span>{consultancyName}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_supAdd`, data.gha_supAdd)}

				{props.values.supervisorName === 'other' ? (
					<DashedLangInput name="dAdd" setFieldValue={props.setFieldValue} value={props.values['dAdd']} />
				) : (
					<span>{address}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_supPh`, data.gha_supPh)}

				{props.values.supervisorName === 'other' ? (
					<>
						<DashedLangInput name="dMobile" setFieldValue={props.setFieldValue} value={props.values['dMobile']} />
						{props.errors.dMobile && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.dMobile}
							</Label>
						)}
					</>
				) : (
					<span>{mobile}</span>
				)}

				<br />
				{getMessage(`${messageId}.gha_supMunicipalRegNo`, data.gha_supMunicipalRegNo)}

				{props.values.supervisorName === 'other' ? (
					<>
						<DashedLangInputWithSlash
							name="dMunicipalRegNo"
							setFieldValue={props.setFieldValue}
							value={props.values['dMunicipalRegNo']}
							error={props.errors.dMunicipalRegNo}
						/>
					</>
				) : (
					<span>{municipalRegNo}</span>
				)}

				<br />
				{props.values.supervisorName === 'other' ? (
					<>
						{getMessage(`${messageId}.gha_supSignature`, data.gha_supSignature)}
						<span
							style={{
								borderBottom: '1px dashed grey',
								width: '180px',
								display: 'inline-block',
							}}
						/>
					</>
				) : (
					<ImageDisplayInline
						label={getMessage(`${messageId}.gha_supSignature`, data.gha_supSignature)}
						src={signature}
						height={40}
						alt="user signature"
					/>
				)}

				{props.values.supervisorName === 'other' ? (
					<DashedLangDateField
						name="dDate"
						// onChange={props.handleChange}
						value={props.values['dDate']}
						error={props.errors['dDate']}
						setFieldValue={props.setFieldValue}
						label={getMessage(`${messageId}.gha_sdate`, data.gha_sdate)}
					/>
				) : (
					<span>
						{getMessage(`${messageId}.gha_sdate`, data.gha_sdate)}
						{getNepaliDate(enterDate)}
					</span>
				)}
			</span>
			<br />
			<br />
		</>
	);
};

const FormContractorDetails = (props) => {
	// const contractorData = props.contractorinfo;
	return (
		<Grid>
			<Grid.Row columns={2}>
				<Grid.Column>
					{getMessage(`${messageId}.gha_contractor`, data.gha_contractor)}
					<br />
					{getMessage(`${messageId}.gha_contractorName`, data.gha_contractorName)}
					<>
						<DashedLangInput name="thekdarName" setFieldValue={props.setFieldValue} value={props.values['thekdarName']} />
						{props.errors.thekdarName && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.thekdarName}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_contractorAdd`, data.gha_contractorAdd)}
					<>
						<DashedLangInput name="tAddress" setFieldValue={props.setFieldValue} value={props.values['tAddress']} />
						{props.errors.tAddress && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.tAddress}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_contractorPh`, data.gha_contractorPh)}
					<>
						<DashedLangInput name="tPhone" setFieldValue={props.setFieldValue} value={props.values['tPhone']} />
						{props.errors.tPhone && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.tPhone}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_contractorRegNo`, data.gha_contractorRegNo)}
					<>
						<DashedLangInputWithSlash name="tDarta" setFieldValue={props.setFieldValue} value={props.values['tDarta']} />
						{props.errors.tDarta && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.tDarta}
							</Label>
						)}
					</>
					<SignatureImage
						showSignature={props.useSignatureImage}
						value={props.nirmanByabasayeSignature}
						label={getMessage(`${messageId}.gha_contractorSignature`, data.gha_contractorSignature)}
					/>
					{/* {getMessage(`${messageId}.gha_contractor`, data.gha_contractor)} */}
					{/* <br />
					{getMessage(`${messageId}.gha_contractorSignature`, data.gha_contractorSignature)}
					<span
						style={{
							borderBottom: '1px dashed grey',
							width: '180px',
							display: 'inline-block',
						}}
					/> */}
				</Grid.Column>
				<br />
				<br />
				<Grid.Column>
					{getMessage(`${messageId}.gha_dakarmi`, data.gha_dakarmi)}
					<br />
					{getMessage(`${messageId}.gha_dakarmiName`, data.gha_dakarmiName)}
					<>
						<DashedLangInput
							name="mistiriName"
							setFieldValue={props.setFieldValue}
							value={props.values['mistiriName']}
							// error={props.errors.mistiriName}
						/>
						{props.errors.mistiriName && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.mistiriName}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_dakarmiAdd`, data.gha_dakarmiAdd)}
					<>
						<DashedLangInput name="mAddress" setFieldValue={props.setFieldValue} value={props.values['mAddress']} />
						{props.errors.mAddress && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.mAddress}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_dakarmiPh`, data.gha_dakarmiPh)}
					<>
						<DashedLangInput name="mPhone" setFieldValue={props.setFieldValue} value={props.values['mPhone']} />

						{props.errors.mPhone && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.mPhone}
							</Label>
						)}
					</>
					<br />
					{getMessage(`${messageId}.gha_dakarmiRegNo`, data.gha_dakarmiRegNo)}
					<>
						<DashedLangInputWithSlash name="mDarta" setFieldValue={props.setFieldValue} value={props.values['mDarta']} />
						{props.errors.mDarta && (
							<Label pointing="left" prompt size="large" basic color="red">
								{props.errors.mDarta}
							</Label>
						)}
					</>
					<SignatureImage
						showSignature={props.useSignatureImage}
						value={props.dakarmiSignature}
						label={getMessage(`${messageId}.gha_contractorSignature`, data.gha_contractorSignature)}
					/>
					{/* <br />

					{getMessage(`${messageId}.gha_dakarmiSignature`, data.gha_dakarmiSignature)}
					<span
						style={{
							borderBottom: '1px dashed grey',
							width: '180px',
							display: 'inline-block',
						}}
					/> */}
				</Grid.Column>
				{/* <Input name="" className="dashedForm-control" disabled /> */}
			</Grid.Row>
		</Grid>
	);
};

// const mapDispatchToProps = {
//   getFormData,
//   getUserInfo,
//   getOptionData,
//   getAnusuchighaData,
//   postFormData
// };
// const mapStateToProps = state => {
//   return {
//     formData: state.root.formData,
//     errors: state.root.ui.errors,
// loading: state.root.ui.loading
//   };
// };

const Anushuchigha = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.anusuchiGha, objName: 'anusuchiGha', form: true },
			{
				api: api.designApproval,
				objName: 'designApprovalGha',
				form: false,
			},
			{
				api: api.anusuchiGa,
				objName: 'anusuchiGaGha',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			// param2: 'anusuchighaSpecial'
			param2: [
				'getElementsByClassName',
				'ui selection dropdown',
				'innerText',
				// construction
			],
		}}
		fetchFiles={true}
		parentProps={parentProps}
		render={(props) => <AnushuchiGhaComponent {...props} parentProps={parentProps} />}
	/>
);

export default Anushuchigha;

// props.values.supervisorName === 'other'
// ? null
// : supOptions,
// props.values.supervisorName === 'other'
// ? props.values['dName']
// : props.values.supervisorName,
// this.props.printcontent
