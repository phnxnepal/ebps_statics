import React, { Component } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import FormContainerV2 from '../../../containers/base/FormContainerV2';
import api from '../../../utils/api';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { DashedLangDateField, CompactDashedLangDate } from './../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { prepareMultiInitialValues, getJsonData, handleSuccess, checkError } from './../../../utils/dataUtils';
import { showToast } from '../../../utils/functionUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import { validateNepaliDate, validateNullableNumber } from '../../../utils/validationUtils';

import { LayoutIjajat } from '../../../utils/data/LayoutPratibedanIjajatData';
import { Surroundings } from './certificateComponents/SurroundingComponents';
import { DashedLangInputWithSlash, DashedLangInput } from '../../shared/DashedFormInput';
import ErrorDisplay from './../../shared/ErrorDisplay';
import { DashedUnitInput, DashedLengthInputWithRelatedUnits } from './../../shared/EbpsUnitLabelValue';
import { ShreeDropdown } from './formComponents/ShreeDropdown';
import { RadioInput } from '../../shared/formComponents/RadioInput';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { isInaruwa } from '../../../utils/clientUtils';

const LayoutGariIjajat = LayoutIjajat.data;
const layoutNames = [
	'naapiLambai',
	'naapiChaudai',
	'fieldLambai',
	'fieldChaudai',
	'mapdandaLambai',
	'purwa',
	'uttar',
	'dakshin',
	'sabikLambai',
	'sabikChaudai',
];
const layoutNepali = ['date', 'allowanceDate', 'layoutMiti', 'layoutDate', 'sabikMiti'];
const LayoutSchema = layoutNames.map(rows => {
	return {
		[rows]: validateNullableNumber,
	};
});
const LayoutNepaliSchema = layoutNepali.map(rows => {
	return {
		[rows]: validateNepaliDate,
	};
});
const LayoutNameSchema = Yup.object().shape(Object.assign(...LayoutSchema, ...LayoutNepaliSchema, {}));
class LayoutGariPratibedanIjajatComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData, DEFAULT_UNIT_LENGTH } = this.props;
		const json_data = getJsonData(prevData);
		const allowance = getJsonData(otherData.allowance);
		initVal = prepareMultiInitialValues(
			{
				obj: {
					surrUnit: DEFAULT_UNIT_LENGTH,
					naapiUnit: DEFAULT_UNIT_LENGTH,
					fieldUnit: DEFAULT_UNIT_LENGTH,
					mapdandaUnit: DEFAULT_UNIT_LENGTH,
					sabikUnit: DEFAULT_UNIT_LENGTH,
				},
				reqFields: [],
			},
			{
				obj: allowance,
				reqFields: ['allowanceDate'],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.date)) {
			initVal.date = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const salutationData = [LayoutGariIjajat.layout_salutation_1, LayoutGariIjajat.layout_salutation_2, userData.organization.name];
		return (
			<>
				<Formik
					initialValues={initVal}
					validationSchema={LayoutNameSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.LayoutGariPratibedanIjaja}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterSalutation lines={salutationData} />
									<FlexSingleRight>
										{LayoutGariIjajat.miti}
										<DashedLangDateField
											compact={true}
											name="date"
											value={values.date}
											error={errors.date}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</FlexSingleRight>
									<SectionHeader>
										<h3 className="underline end-section">{LayoutGariIjajat.title}</h3>
									</SectionHeader>
									<div className="no-margin-field">
										{LayoutGariIjajat.mainData_1}
										<DashedLangInputWithSlash
											name="chaNo"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.chaNo}
											error={errors.chaNo}
										/>
										{LayoutGariIjajat.miti}
										<CompactDashedLangDate
											name="layoutMiti"
											value={values.layoutMiti}
											error={errors.layoutMiti}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_2}
										{userData.organization.name}
										{LayoutGariIjajat.mainData_3}
										{permitData.nibedakTol}
										{LayoutGariIjajat.mainData_4}
										<ShreeDropdown name="shree" options={LayoutGariIjajat.option} />
										{permitData.nibedakName}
										{LayoutGariIjajat.mainData_5}
										<CompactDashedLangDate
											name="allowanceDate"
											value={values.allowanceDate}
											error={errors.allowanceDate}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_6}
										<DashedLangInput
											name="pasa"
											setFieldValue={setFieldValue}
											value={values.pasa}
											className="dashedForm-control"
										/>
										{LayoutGariIjajat.mainData_7}
										<DashedLangInput
											name="chana"
											setFieldValue={setFieldValue}
											value={values.chana}
											className="dashedForm-control"
										/>
										{LayoutGariIjajat.mainData_8}
										<CompactDashedLangDate
											name="layoutDate"
											value={values.layoutDate}
											error={errors.layoutDate}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_9}
										<SectionHeader>
											<h4 className="underline">{LayoutGariIjajat.tapasilTitle}</h4>
										</SectionHeader>
										{LayoutGariIjajat.tapasilData_1}
										{permitData.applicantName}
										<br />
										{LayoutGariIjajat.tapasilData_2}
										{permitData.oldMunicipal}
										<br />
										{LayoutGariIjajat.tapasilData_3}
										{permitData.newWardNo}
										{LayoutGariIjajat.tapasilData_4}
										{permitData.kittaNo}
										{LayoutGariIjajat.tapasilData_5}
										{permitData.landArea}
										{permitData.landAreaType}
										<br />
										<DashedLengthInputWithRelatedUnits
											name="naapiLambai"
											label={LayoutGariIjajat.tapasilData_6}
											unitName="naapiUnit"
											relatedFields={['naapiChaudai']}
										/>
										<DashedLengthInputWithRelatedUnits
											name="naapiChaudai"
											label={LayoutGariIjajat.tapasilData_8}
											unitName="naapiUnit"
											relatedFields={['naapiLambai']}
										/>
										<br />
										<DashedLengthInputWithRelatedUnits
											name="fieldLambai"
											label={LayoutGariIjajat.tapasilData_7}
											unitName="fieldUnit"
											relatedFields={['fieldChaudai']}
										/>
										<DashedLengthInputWithRelatedUnits
											name="fieldChaudai"
											label={LayoutGariIjajat.tapasilData_8}
											unitName="fieldUnit"
											relatedFields={['fieldLambai']}
										/>
										<br />
										{LayoutGariIjajat.tapasilData_9}
										<DashedLangInput
											name="mapdanda"
											setFieldValue={setFieldValue}
											value={values.mapdanda}
											className="dashedForm-control"
										/>
										<br />
										<DashedUnitInput name="mapdandaLambai" label={LayoutGariIjajat.tapasilData_10} unitName="mapdandaUnit" />
										<br />
										{LayoutGariIjajat.tapasilData_11}
										<DashedLangInput
											name="setBack"
											setFieldValue={setFieldValue}
											value={values.setBack}
											className="dashedForm-control"
										/>
										<br />
										{LayoutGariIjajat.tapasilData_12}
										<Surroundings />
										<SectionHeader>
											<p className="left-align underline">{LayoutGariIjajat.tapasilData_13}</p>
										</SectionHeader>
										{LayoutGariIjajat.tapasilData_14}
										<DashedLangInput
											name="oneNaam"
											setFieldValue={setFieldValue}
											value={values.oneNaam}
											className="dashedForm-control"
										/>
										{LayoutGariIjajat.tapasilData_15}
										<span className="ui input signature-placeholder" />
										<br />
										{LayoutGariIjajat.tapasilData_14_1}
										<DashedLangInput
											name="twoNaam"
											setFieldValue={setFieldValue}
											value={values.twoNaam}
											className="dashedForm-control"
										/>
										{LayoutGariIjajat.tapasilData_15}
										<span className="ui input signature-placeholder" />
										<SectionHeader>
											<p className="underline left-align">{LayoutGariIjajat.tapasilData_16}</p>
										</SectionHeader>
										{LayoutGariIjajat.tapasilData_17}
										<br />

										<DashedLengthInputWithRelatedUnits
											name="sabikLambai"
											label={LayoutGariIjajat.lambai}
											unitName="sabikUnit"
											relatedFields={['sabikChaudai']}
										/>
										<DashedLengthInputWithRelatedUnits
											name="sabikChaudai"
											label={LayoutGariIjajat.tapasilData_8}
											unitName="sabikUnit"
											relatedFields={['sabikLambai']}
										/>
										{LayoutGariIjajat.tapasilData_18}
										<br />
										{LayoutGariIjajat.miti}
										<CompactDashedLangDate
											name="sabikMiti"
											value={values.sabikMiti}
											error={errors.sabikMiti}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.tapasilData_19}
										<DashedLangInput
											name="bafi"
											setFieldValue={setFieldValue}
											value={values.bafi}
											className="dashedForm-control"
										/>
										<br />
										{LayoutGariIjajat.tapasilData_20}
										{LayoutGariIjajat.sitePlan.map(option => (
											<React.Fragment key={option}>
												<RadioInput name="puranoGhar" option={option} label={option} />
											</React.Fragment>
										))}
										<br />
										{LayoutGariIjajat.tapasilData_21}
										{LayoutGariIjajat.preshitNaksa.map((name, index) => (
											<RadioInput key={index} option={name} name="preshit" space={true} />
										))}
										{values.preshit === LayoutGariIjajat.preshitNaksa[1] && (
											<div>
												{LayoutGariIjajat.tapasilData_22}
												<DashedLangInput
													name="preshitOpt"
													setFieldValue={setFieldValue}
													value={values.preshitOpt}
													error={errors.preshitOpt}
													line="full-line"
												/>
											</div>
										)}
										<br />
										<br />
										<SectionHeader>
											<p className="underline left-align">{LayoutGariIjajat.footer_1}</p>
										</SectionHeader>
										{LayoutGariIjajat.footer_2}
										<DashedLangInput
											name="layoutName"
											setFieldValue={setFieldValue}
											value={values.layoutName}
											className="dashedForm-control"
										/>
										<br />
										{LayoutGariIjajat.footer_3}
										<DashedLangInput
											name="layoutPad"
											setFieldValue={setFieldValue}
											value={values.layoutPad}
											className="dashedForm-control"
										/>
										<br />
										{LayoutGariIjajat.tapasilData_15}
										<span className="ui input signature-placeholder" />

										{!isInaruwa && (
											<>
												<br />
												{LayoutGariIjajat.footer_4}
												<br />
												<Grid columns={2}>
													<Grid.Row>
														<Grid.Column>
															{LayoutGariIjajat.footer_5}
															{permitData.nibedakName}
														</Grid.Column>
														<Grid.Column>
															{' '}
															{LayoutGariIjajat.tapasilData_15}
															<span className="ui input signature-placeholder" />
														</Grid.Column>
													</Grid.Row>
													<Grid.Row>
														<Grid.Column>
															{LayoutGariIjajat.footer_6}
															<DashedLangInput
																name="nirmanName"
																setFieldValue={setFieldValue}
																value={values.nirmanName}
																className="dashedForm-control"
															/>
														</Grid.Column>
														<Grid.Column>
															{LayoutGariIjajat.tapasilData_15}
															<span className="ui input signature-placeholder" />
														</Grid.Column>
													</Grid.Row>
												</Grid>
												<br />
											</>
										)}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const LayoutGariPratibedanIjajat = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.LayoutGariPratibedanIjaja,
				objName: 'layoutGariPratibedanIjaja',
				form: true,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param3: ['getElementsByTagName', 'textarea', 'value'],
			param4: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			// param5: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		useInnerRef={true}
		render={props => <LayoutGariPratibedanIjajatComponent {...props} parentProps={parentProps} />}
	/>
);

export default LayoutGariPratibedanIjajat;
