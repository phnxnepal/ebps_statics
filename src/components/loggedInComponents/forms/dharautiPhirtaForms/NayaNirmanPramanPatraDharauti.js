import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { Form, Select } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import * as Yup from 'yup';

import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../../utils/dataUtils';
import { isEmpty, showToast } from '../../../../utils/functionUtils';
import api from '../../../../utils/api';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput, DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';

import { dharautiData } from '../../../../utils/data/NirmanPramanPatraDharautiData';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangDateField } from '../../../shared/DateField';
const NaksaFileSchema = Yup.object().shape(
	Object.assign({
		date: validateNullableNepaliDate,
		Bdate: validateNullableNepaliDate,
	})
);
const NirmanDharautiData = dharautiData.data;
const opts = [
	{ key: 'structure', value: 'सुपरस्ट्रक्चर', text: 'सुपरस्ट्रक्चर' },
	{ key: 'compound', value: 'कम्पाउण्डवाल', text: 'कम्पाउण्डवाल' },
];
const maileOpt = [
	{ key: 'maile', value: 'मैले', text: 'मैले' },
	{ key: 'hamile', value: 'हामीले', text: 'हामीले' },
];
const getMs = (value) => {
	const ret = maileOpt.filter((row) => row.value === value);
	return ret[0] ? ret[0].key : 'maile';
};
class NirmanDharautiComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);
		const building = getJsonData(otherData.buildingFinish);
		initVal = prepareMultiInitialValues(
			{
				obj: building,
				reqFields: ['chalaniNumber', 'Bdate'],
			},
			{
				obj: {
					structure: opts[0].value,
					maile: maileOpt[0].value,
				},
				reqFields: [],
			},
			{ obj: json_data, reqFields: [] }
		);

		if (isStringEmpty(initVal.date)) {
			initVal.date = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const salutationData = [NirmanDharautiData.salutation, `${userData.organization.name}, ${userData.organization.address}`];
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={NaksaFileSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						// values.applicationNo = permitData.applicationNo;
						// values.error && delete values.error;
						try {
							await this.props.postAction(api.NayaNirmanPramanPatraDharautiPhirta, values, true);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<div>
									<SectionHeader>
										<h3 className="underline">{NirmanDharautiData.title}</h3>
									</SectionHeader>
									<LetterSalutation lines={salutationData} />
									<SectionHeader>
										<h3 className="normal underline end-section">{NirmanDharautiData.subject}</h3>
									</SectionHeader>
									<div style={{ textAlign: 'justify ' }}>
										{NirmanDharautiData.content_1}
										<br />
										<span className="indent-span"> {NirmanDharautiData.content_2}</span>
										{userData.organization.name}
										{NirmanDharautiData.content_3}
										<DashedLangInputWithSlash
											compact={true}
											name="chalaniNumber"
											setFieldValue={setFieldValue}
											value={getIn(values, 'chalaniNumber')}
											error={getIn(errors, 'chalaniNumber')}
										/>
										{NirmanDharautiData.content_4}
										<DashedLangDateField
											compact={true}
											name="Bdate"
											value={values.Bdate}
											error={errors.Bdate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{NirmanDharautiData.content_5}
										<Select
											name="structure"
											value={values.structure}
											onChange={(e, { value }) => setFieldValue('structure', value)}
											options={opts}
										/>
										{NirmanDharautiData.content_6}
										<Select
											name="maile"
											value={values.maile}
											onChange={(e, { value }) => setFieldValue('maile', value)}
											options={maileOpt}
										/>
										<DashedLangInput name="koChetrafal" setFieldValue={setFieldValue} value={values.koChetrafal} />
										{NirmanDharautiData.content_7}
										{permitData.landArea} {permitData.landAreaType}
										{NirmanDharautiData.content_8}
										{userData.organization.name}
										{NirmanDharautiData.content_9}
										{NirmanDharautiData[getMs(values['maile'])].first}
										{NirmanDharautiData.content_10}
										<Select
											name="maile"
											value={values.maile}
											onChange={(e, { value }) => setFieldValue('maile', value)}
											options={maileOpt}
										/>
										{NirmanDharautiData.content_10_1}
										<DashedLangInput
											name="amount"
											setFieldValue={setFieldValue}
											value={values.amount}
											className="dashedForm-control"
										/>
										{NirmanDharautiData.content_11}
										{NirmanDharautiData[getMs(values['maile'])].first}
										<br />
									</div>
									<br />
									<FlexSingleRight>
										<div className="no-margin-field">
											{NirmanDharautiData.footer_1}
											<br />
											{NirmanDharautiData.footer_2}
											{permitData.nibedakName}
											<br />
											{NirmanDharautiData.footer_3}
											<DashedLangInput
												name="wareshName"
												setFieldValue={setFieldValue}
												value={values.wareshName}
												className="dashedForm-control"
											/>
											<br />
											{NirmanDharautiData.footer_4}
											{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
											<br />
											{NirmanDharautiData.footer_5}
											<span className="ui input dashedForm-control" />
										</div>
									</FlexSingleRight>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const NirmanPramanPatraDharauti = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.NayaNirmanPramanPatraDharautiPhirta,
				objName: 'NayaNirmanPramanPatra',
				form: true,
			},
			{
				api: api.buildingFinish,
				objName: 'buildingFinish',
				form: false,
			},
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['15dayspecial'],
			param4: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		render={(props) => <NirmanDharautiComponent {...props} parentProps={parentProps} />}
	/>
);

export default NirmanPramanPatraDharauti;
