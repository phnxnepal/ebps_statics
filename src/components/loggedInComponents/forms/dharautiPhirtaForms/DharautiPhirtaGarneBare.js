import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik, Form } from 'formik';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { SansodhanBibaranInaruwaData } from '../../../../utils/data/SansodhanBibaranPahiloInaruwa';
import { CompactDashedLangDate, EbpsFormDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { checkError, handleSuccess, getJsonData, prepareMultiInitialValues } from '../../../../utils/dataUtils';
import { Table, TableCell } from 'semantic-ui-react';
import EbpsForm from '../../../shared/EbpsForm';
import { PatraSankhyaAndDate } from './../formComponents/PatraSankhyaAndDate';
import { validateNullableNepaliDate, validateNullableNormalNumber } from '../../../../utils/validationUtils';
import * as Yup from 'yup';
import { FooterSignature } from './../formComponents/FooterSignature';
import { EbpsNumberForm } from './../../../shared/EbpsForm';
import { PrintParams } from '../../../../utils/printUtils';
import { getApproveByObject } from '../../../../utils/formUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';

const inaruwaData = SansodhanBibaranInaruwaData.DharautiPhirtaData;
const DharautiSchema = Yup.object().shape(
	Object.assign({
		dharautiDate: validateNullableNepaliDate,
		dharautiCDate: validateNullableNepaliDate,
		tapasilDate: validateNullableNepaliDate,
		jafatDate: validateNullableNepaliDate,
		dharautiAmount: validateNullableNormalNumber,
		kattaAmount: validateNullableNormalNumber,
		khudraAmount: validateNullableNormalNumber,
	})
);
class DharautiPhirtaGarneBareComponent extends Component {
	constructor(props) {
		super(props);
		let initialValues = {};
		const { otherData, prevData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const erData = getApproveBy(0);
		const pramukhData = getApproveBy(1);

		const allowance = getJsonData(otherData.allowance);
		initialValues = prepareMultiInitialValues(
			{
				obj: allowance,
				reqFields: ['gharNo'],
			},
			{
				obj: getJsonData(prevData),
				reqFields: [],
			},
			{
				obj: {
					erSignature: erData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);
		if (isStringEmpty(initialValues.dharautiDate)) {
			initialValues.dharautiDate = getCurrentDate(true);
		}
		this.state = {
			initialValues,
		};
	}
	render() {
		const { userData, permitData, useSignatureImage, errors, getSaveButtonProps } = this.props;
		return (
			<>
				<div className="pilenthLevelTechViewWrap">
					{errors && <ErrorDisplay message={errors.message} />}
					<Formik
						initialValues={this.state.initialValues}
						validationSchema={DharautiSchema}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);
							try {
								await this.props.postAction(api.DharautiPhirtaBare, values, true);
								window.scroll(0, 0);
								if (this.props.success && this.props.success.success) {
									handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
								}
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('Error', err);
							}
						}}
					>
						{({ isSubmitting, values, handleChange, handleSubmit, errors, setFieldValue, validateForm }) => (
							<Form loading={isSubmitting} className="ui form">
								<div ref={this.props.setRef}>
									<LetterHeadFlex compact={true} userInfo={userData} />
									<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} dateFieldName="dharautiDate" />
									<div className="section-header margin-section">
										<h3 className="underline">{inaruwaData.subject}</h3>
									</div>
									{inaruwaData.content.content_1}
									<br />
									{userData.organization.name}
									<br />
									<div>
										<div className="no-margin-field margin-section" style={{ textAlign: 'justify' }}>
											<span className="indent-span">{inaruwaData.content.content_3}</span>
											{userData.organization.name} {inaruwaData.content.content_4} {permitData.newWardNo}{' '}
											{inaruwaData.content.content_5}
											<DashedLangInput
												name="gharNo"
												setFieldValue={setFieldValue}
												value={values.gharNo}
												error={errors.gharNo}
											/>{' '}
											{inaruwaData.content.content_6}
											{permitData.applicantName}
											{inaruwaData.content.content_8}
											<CompactDashedLangDate
												name="dharautiCDate"
												setFieldValue={setFieldValue}
												error={errors.dharautiCDate}
												value={values.dharautiCDate}
											/>{' '}
											{inaruwaData.content.content_9}
											<DashedLangInput
												name="dharautiRakam"
												value={values.dharautiRakam}
												setFieldValue={setFieldValue}
												error={errors.dharautiRakam}
											/>
											{inaruwaData.content.content_10}
										</div>
										<FooterSignature
											designations={[inaruwaData.content.content_11, inaruwaData.content.content_12]}
											signatureImages={useSignatureImage && [values.erSignature, values.pramukhSignature]}
										/>
										<div className="section-header margin-section">
											<h3>{inaruwaData.content.content_13}</h3>
										</div>
										<Table celled>
											<Table.Header>
												<Table.Row textAlign="center">
													<Table.HeaderCell>{inaruwaData.table.content_1}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_2}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_3}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_4}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_5}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_6}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<TableCell width="3">
													<EbpsFormDateField
														name="tapasilDate"
														value={values.tapasilDate}
														onChange={(e, { value }) => {
															setFieldValue('tapasilDate', value);
														}}
														handleChange={handleChange}
														error={errors.tapasilDate}
													/>
												</TableCell>
												<TableCell>
													<EbpsNumberForm
														name="dharautiAmount"
														value={values.dharautiAmount}
														onChange={handleChange}
														error={errors.dharautiAmount}
														isTable={true}
													/>
												</TableCell>
												<TableCell>
													<EbpsNumberForm
														name="kattaAmount"
														value={values.kattaAmount}
														onChange={handleChange}
														error={errors.kattaAmount}
														isTable={true}
													/>
												</TableCell>
												<TableCell>
													<EbpsNumberForm
														name="khudraAmount"
														value={values.khudraAmount}
														onChange={handleChange}
														error={errors.khudraAmount}
														isTable={true}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm name="jafat" setFieldValue={setFieldValue} value={values.jafat} error={errors.jafat} />
												</TableCell>
												<TableCell>
													<EbpsFormDateField
														name="jafatDate"
														value={values.jafatDate}
														onChange={(e, { value }) => {
															setFieldValue('jafatDate', value);
														}}
														handleChange={handleChange}
														error={errors.jafatDate}
													/>
												</TableCell>
											</Table.Body>
										</Table>
										<SaveButtonValidation
											errors={errors}
											validateForm={validateForm}
											handleSubmit={handleSubmit}
											{...getSaveButtonProps()}
										/>
									</div>
								</div>
							</Form>
						)}
					</Formik>
				</div>
			</>
		);
	}
}
const DharautiPhirtaGarneBare = (parentProps) => (
	<FormContainerV2
		/**
		 * @TODO change api after backend implementation
		 */
		api={[new ApiParam(api.DharautiPhirtaBare).setForm().getParams()]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', 'parent'],
			param2: ['getElementsByClassName', 'fields inline-group', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <DharautiPhirtaGarneBareComponent {...props} parentProps={parentProps} />}
	/>
);
export default DharautiPhirtaGarneBare;
