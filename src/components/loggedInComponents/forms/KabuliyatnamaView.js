import React, { Component } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { Form, Grid } from 'semantic-ui-react';
import { KabuliyatnamaData } from '../../../utils/data/KabuliyatnamaData';
import { getMessage } from '../../shared/getMessage';
import api from '../../../utils/api';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import { RohawarSignature } from './formComponents/NamaFormComponents';
import { isBirtamod } from '../../../utils/clientUtils';
import {
	FingerprintSignatureBirtamod,
	FingerprintSignature,
	FingerprintSignatureBirtamodVertical,
	FingerprintSignatureVertical,
} from './formComponents/FingerprintSignature';
import LongFormDate from './formComponents/LongFormDate';
import { namaFormData } from '../../../utils/data/namaFormData';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { DashedMultiUnitLengthInput, DashedAreaUnitInput } from '../../shared/EbpsUnitLabelValue';
import { validateNullableNumber } from '../../../utils/validationUtils';

import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
const Kdata = KabuliyatnamaData.structureDesign;

const messageId = 'KabuliyatnamaData.structureDesign';

const notreqData = ['namedMapdanda', 'namedAdhikar', 'sadakAdhikarKshytra', 'michiyekoDistance', 'newArea'];

const notreqSchema = notreqData.map((rows) => {
	return {
		[rows]: validateNullableNumber,
	};
});

const kabuliyatnamaSchema = Yup.object().shape(Object.assign(...notreqSchema, {}));

class KabuliyatnamaView extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		const rajasowData = getJsonData(otherData.rajasow);

		const initVal = prepareMultiInitialValues(
			{ obj: { newAreaUnit: DEFAULT_UNIT_AREA, sadakAdhikarUnit: DEFAULT_UNIT_LENGTH }, reqFields: [] },
			{ obj: rajasowData, reqFields: ['namedMapdanda', 'namedAdhikar', 'sadakAdhikarKshytra', 'sadakAdhikarUnit'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = { initVal };
	}
	render() {
		const { initVal } = this.state;
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;

		return (
			// <h4 className='NJ-Main'>
			<Formik
				initialValues={initVal}
				validationSchema={kabuliyatnamaSchema}
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);
					values.applicationNo = this.props.permitData.applicantNo;

					try {
						await this.props.postAction(api.kabuliyatnama, values, true);

						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, handleChange, handleSubmit, values, errors, setFieldValue, validateForm }) => (
					<Form loading={isSubmitting}>
						<div ref={this.props.setRef}>
							<div className="header-underlined-wrap">
								<h2>{getMessage(`${messageId}.heading`, Kdata.heading)}</h2>
							</div>{' '}
							<Grid>
								<Grid.Row>
									<Grid.Column width={4}>
										{isBirtamod ? <FingerprintSignatureBirtamodVertical /> : <FingerprintSignatureVertical />}
									</Grid.Column>{' '}
									<Grid.Column width={12} style={{ textAlign: 'justify' }}>
										{getMessage('namaFormData.commonData.main1', namaFormData.commonData.main1)}
										{userData.organization.address}{' '}
										{getMessage('namaFormData.commonData.main1', namaFormData.commonData.district)} {userData.organization.name}{' '}
										{getMessage('namaFormData.commonData.main1', namaFormData.commonData.ward)}
										{permitData.nibedakTol} {getMessage(`${messageId}.main3`, Kdata.main3)} {permitData.nibedakName}{' '}
										{getMessage(`${messageId}.main4`, Kdata.main4)}{' '}
										<DashedLangInput
											name="personRelation"
											setFieldValue={setFieldValue}
											value={values.personRelation}
											error={errors.personRelation}
										/>{' '}
										{getMessage(`${messageId}.main5`, Kdata.main5)} {permitData.tol}{' '}
										{/* <DashedLangInput name="personAge" setFieldValue={setFieldValue} value={values.personAge} error={errors.personAge} />{' '} */}
										{getMessage(`${messageId}.main6`, Kdata.main6)}{' '}
										<DashedLangInput
											name="personName"
											setFieldValue={setFieldValue}
											value={values.personName}
											error={errors.personName}
										/>{' '}
										{getMessage(`${messageId}.main7`, Kdata.main7)} {userData.organization.address}{' '}
										{getMessage(`${messageId}.main8`, Kdata.main8)} {Kdata.sabik} {permitData.oldMunicipal}{' '}
										{getMessage(`${messageId}.ward`, Kdata.ward)} {permitData.oldWardNo} {Kdata.hal} {permitData.newMunicipal}{' '}
										{getMessage(`${messageId}.ward`, Kdata.ward)}
										{permitData.newWardNo} {getMessage(`${messageId}.main9`, Kdata.main9)}{' '}
										{getMessage(`${messageId}.kitta`, Kdata.kitta)} {permitData.kittaNo}{' '}
										{/* {getMessage(`${messageId}.main10`, Kdata.main10)} */}
										{getMessage(`namaFormData.commonData.main1.area`, namaFormData.commonData.area)} {permitData.landArea}{' '}
										{permitData.landAreaType} {getMessage(`${messageId}.main11`, Kdata.main11)} {userData.organization.name}{' '}
										{getMessage(`${messageId}.main12`, Kdata.main12)}
										<DashedLangInput
											name="existingBuilding"
											setFieldValue={setFieldValue}
											value={values.existingBuilding}
											error={errors.existingBuilding}
										/>{' '}
										{getMessage(`${messageId}.main13`, Kdata.main13)}{' '}
										<DashedMultiUnitLengthInput
											name="namedMapdanda"
											unitName="sadakAdhikarUnit"
											relatedFields={['namedAdhikar', 'sadakAdhikarKshytra', 'michiyekoDistance']}
										/>
										{getMessage(`${messageId}.main14`, Kdata.main14)}{' '}
										<DashedMultiUnitLengthInput
											name="namedAdhikar"
											unitName="sadakAdhikarUnit"
											relatedFields={['namedMapdanda', 'sadakAdhikarKshytra', 'michiyekoDistance']}
										/>
										{getMessage(`${messageId}.main15`, Kdata.main15)}{' '}
										<DashedMultiUnitLengthInput
											name="sadakAdhikarKshytra"
											unitName="sadakAdhikarUnit"
											relatedFields={['namedMapdanda', 'namedAdhikar', 'michiyekoDistance']}
										/>
										{getMessage(`${messageId}.main16`, Kdata.main16)}{' '}
										<DashedLangInput
											name="existingBuilding1"
											setFieldValue={setFieldValue}
											value={values.existingBuilding1}
											error={errors.existingBuilding1}
										/>{' '}
										{getMessage(`${messageId}.main17`, Kdata.main17)}{' '}
										<DashedMultiUnitLengthInput
											name="michiyekoDistance"
											unitName="sadakAdhikarUnit"
											relatedFields={['namedMapdanda', 'namedAdhikar', 'sadakAdhikarKshytra']}
										/>
										{getMessage(`${messageId}.main18`, Kdata.main18)}{' '}
										<DashedLangInput
											name="existingBuilding2"
											setFieldValue={setFieldValue}
											value={values.existingBuilding2}
											error={errors.existingBuilding2}
										/>{' '}
										{getMessage(`${messageId}.main19`, Kdata.main19)}{' '}
										<DashedLangInput
											name="existingBuilding3"
											setFieldValue={setFieldValue}
											value={values.existingBuilding3}
											error={errors.existingBuilding3}
										/>{' '}
										{getMessage(`${messageId}.main20`, Kdata.main20)}{' '}
										<DashedAreaUnitInput name="newArea" unitName="newAreaUnit" />
										{getMessage(`${messageId}.main21`, Kdata.main21)} {userData.organization.name}
										{getMessage(`${messageId}.main21_2`, Kdata.main21_2)}
										<br />
										<br />
										<LongFormDate />
										<br /> {isBirtamod ? <FingerprintSignatureBirtamod /> : <FingerprintSignature />}
									</Grid.Column>
								</Grid.Row>
								<RohawarSignature setFieldValue={setFieldValue} values={values} errors={errors} />
							</Grid>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
			// </h4>
		);
	}
}

const Kabuliyatnama = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.kabuliyatnama, objName: 'kabuliyatnama', form: true },
			{
				api: api.rajaswaEntry,
				objName: 'rajasow',
				form: false,
			},
		]}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <KabuliyatnamaView {...props} parentProps={parentProps} />}
	/>
);
export default Kabuliyatnama;
