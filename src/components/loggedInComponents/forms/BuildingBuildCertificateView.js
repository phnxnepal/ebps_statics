import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput, DashedNormalInput } from '../../shared/DashedFormInput';
import { BuildingBuildCertificateData, mappingDirection } from '../../../utils/data/BuildingBuildCertificateData';
import ErrorDisplay from './../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { isStringEmpty } from './../../../utils/stringUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import EbpsTextareaField from '../../shared/MyTextArea';
import merge from 'lodash/merge';
import { DashedMultiUnitLengthInput, DashedUnitInput } from '../../shared/EbpsUnitLabelValue';
import { translateDate } from '../../../utils/langUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import * as Yup from 'yup';
import { validateNepaliDate } from '../../../utils/validationUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { FooterSignature, FooterSignatureMultiline } from './formComponents/FooterSignature';
import { CertificateSubHeading } from './certificateComponents/CertificateComponents';
import { PrintParams } from '../../../utils/printUtils';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { FinishCertificateBlockFloors } from './certificateComponents/CertificateFloorBlockInputs';
import { getUserTypeValueNepali } from '../../../utils/functionUtils';
import { UserType } from '../../../utils/userTypeUtils';
import { AllowancePaperData } from '../../../utils/data/AllowancePaperData';
import { isKamalamai, isKankai, isMechi } from '../../../utils/clientUtils';

const Detail = BuildingBuildCertificateData.details;
const formData = BuildingBuildCertificateData.formdata;
const kamalamaiData = AllowancePaperData.kamalamaiData;

const BuildingBuildSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNepaliDate,
	})
);

const field_9 = [
	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
];
class BuildingBuildCertificateView extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);

		const formattedFloors = floorArray.getFormattedFloors();
		let initialValues = {};

		const mapTech = getJsonData(otherData.mapTech);
		const prabhidik = getJsonData(otherData.prabhidik);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const certificateNote = getJsonData(otherData.certificateNote);
		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: [
					'floor',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'photo',
					'purposeOfConstruction',
					'purposeOfConstructionOther',
					'surrounding',
				],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingArea', floorArray.getBottomFloor()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: {
					namedUnit: 'METRE',
					highTensionLineUnit: 'METRE',
					publicPropertyUnit: 'METRE',
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'buildingHeight'],
			},
			{
				obj: { namsariName: certificateNote.shreeName || certificateNote.sasthaYaExtraName },
				reqFields: [],
			},
			{
				obj: prabhidik,
				reqFields: [
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'namedBorder',
					'namedUnit',
					'elecVolt',
					'publicProperty',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);

		if (isStringEmpty(initialValues.Bdate)) {
			initialValues.Bdate = getCurrentDate(true);
		}

		const initVal = merge(initialValues, initialValues1);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const user_info = this.props.userData;
		const { initVal, floorArray, formattedFloors } = this.state;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={BuildingBuildSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(`${api.buildingBuild}${this.props.permitData.nameTransaferId}`, values);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex needsProvince={true} userInfo={user_info} hideLogo={isMechi} />
									<CertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										title={BuildingBuildCertificateData.details.title}
										noImage={true}
									/>

									<div className="no-margin-field">
										{userData.organization.name} {Detail.wda}
										{permitData.newWardNo} {Detail.data1}
										{values.namsariName} {Detail.data2}
										{translateDate(permitData.applicantDateBS)} {Detail.data3}
										{permitData.applicationNo}
										{Detail.data4}
										{permitData.newWardNo}
										{Detail.data5}
										{permitData.sadak}
										{Detail.data6}
										{permitData.buildingJoinRoadType}
										{Detail.data7}
										{permitData.kittaNo}
										{Detail.data8}
										{permitData.landArea} {permitData.landAreaType}
										{Detail.data9}
										<DashedLangInput name="jagga" setFieldValue={setFieldValue} value={values.jagga} />
										{Detail.data10}
										<DashedLangDateField
											name="Bdate"
											handleChange={handleChange}
											value={values.Bdate}
											error={errors.Bdate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{Detail.data11}
										<DashedLangInput name="maa" setFieldValue={setFieldValue} value={values.maa} />
										{Detail.data12}
										{values.namsariName}
										{Detail.data13}
									</div>
									<div>
										{formData.fdata1}
										{values.namsariName}
										{formData.address}
										{permitData.newMunicipal} {permitData.nibedakTol} {permitData.nibedakSadak}
									</div>
									<div className="no-margin-field">
										{formData.fdata2}
										{values.surrounding &&
											values.surrounding.map((index, i) => (
												<div className="indent" key={i}>
													{mappingDirection.find((fl) => fl.direction === index.side).value}{' '}
													<DashedNormalInput
														name={`surrounding.${i}.kittaNo`}
														value={getIn(values, `surrounding.${i}.kittaNo`)}
														handleChange={handleChange}
														error={getIn(errors, `surrounding.${i}.kittaNo`)}
													/>
													{formData.sandhiyar}
													<DashedNormalInput
														name={`surrounding.${i}.sandhiyar`}
														value={getIn(values, `surrounding.${i}.sandhiyar`)}
														handleChange={handleChange}
														error={getIn(errors, `surrounding.${i}.sandhiyar`)}
													/>
												</div>
											))}
									</div>
									<div className="no-margin-field">
										{formData.fdata3}
										<DashedLangInput name="buildType" setFieldValue={setFieldValue} value={values.buildType} />
										{formData.fdata4}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
										{formData.fdata5}
										<DashedLangInput name="roofLen" setFieldValue={setFieldValue} value={values.roofLen} />
										{formData.fdata6}
										<DashedLangInput name="prayojan" setFieldValue={setFieldValue} value={values.prayojan} />
									</div>
									<div className="no-margin-field">
										{formData.fdata6_1}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											value={values.purposeOfConstruction}
										/>
										<br />
										{formData.fdata7}
										<DashedLangInput name="roomNo" setFieldValue={setFieldValue} value={values.roomNo} />
										{formData.fdata8}
										<DashedLangInput name="windowNo" setFieldValue={setFieldValue} value={values.windowNo} />
										{formData.fdata9}
										<DashedLangInput name="doorNo" setFieldValue={setFieldValue} value={values.doorNo} />
										{formData.fdata10}
										<DashedLangInput name="satarNo" setFieldValue={setFieldValue} value={values.satarNo} />
									</div>

									<DashedMultiUnitLengthInput
										name="namedMapdanda"
										unitName="namedUnit"
										relatedFields={['namedBorder', 'namedAdhikar']}
										label={formData.fdata13}
									/>

									<DashedMultiUnitLengthInput
										name="namedAdhikar"
										unitName="namedUnit"
										relatedFields={['namedBorder', 'namedMapdanda']}
										label={formData.fdata14}
									/>
									<DashedMultiUnitLengthInput
										name="namedBorder"
										unitName="namedUnit"
										relatedFields={['namedAdhikar', 'namedMapdanda']}
										label={formData.fdata15}
									/>
									<div>
										{formData.seven}
										<div className="div-indent">
											<FinishCertificateBlockFloors floorArray={floorArray} formattedFloors={formattedFloors} compact={true} />
										</div>
									</div>
									<div>
										{formData.fdata24}
										<DashedLangInput name="coverageDetails" setFieldValue={setFieldValue} value={values.coverageDetails} />
									</div>

									{/* {formData.fdata25} */}
									<DashedUnitInput name="isHighTensionLineDistance" label={formData.fdata25} unitName="highTensionLineUnit" />
									{formData.volt}
									<DashedLangInput name="elecVolt" setFieldValue={setFieldValue} value={values.elecVolt} />
									<br />

									<DashedUnitInput name="publicProperty.1.value" label={formData.fdata26} unitName="publicPropertyUnit" />

									{/* {formData.fdata26}
                                            <DashedLangInput
                                                name='publicProperty.1.value'
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                                value={getIn(values, 'publicProperty.1.value')}
                                                className='dashedForm-control'
                                            />
                                            <UnitDropdown
                                                name='publicPropertyUnit'
                                                setFieldValue={setFieldValue}
                                                value={values.publicPropertyUnit}
                                                options={distanceOptions}
                                            /> */}
									{formData.fdata27}
									<DashedLangInput name="publicPropertyName" setFieldValue={setFieldValue} value={values.publicPropertyName} />

									<div className="no-margin-field">
										{formData.fdata28}
										{/* <DashedLangInput
                                            name='nikash'
                                            setFieldValue={setFieldValue}
                                            inline={true}
                                            value={values.nikash}
                                            className='dashedForm-control'
                                        /> */}
										<Select
											options={field_9}
											name="nikash"
											placeholder="छ (भएको)"
											onChange={(e, { value }) => setFieldValue('nikash', value)}
											value={values['nikash']}
											error={errors.nikash}
											// defaultValue={props.values['nikash']}
										/>
									</div>
									<div>
										{formData.fdata29}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>
									<br />
									{isKamalamai ? (
										<FooterSignatureMultiline
											designations={[
												[kamalamaiData.signature.field],
												[kamalamaiData.signature.sifarish, `(${getUserTypeValueNepali(UserType.SUB_ENGINEER)})`],
												[kamalamaiData.signature.suikrit, `(${getUserTypeValueNepali(UserType.ENGINEER)})`],
												[`(${kamalamaiData.signature.footer_chief})`],
											]}
										/>
									) : isKankai ? (
										<FooterSignature designations={[formData.fdata30, formData.fdata31, formData.sakhaAdhikrit]} />
									) : (
										<FooterSignature designations={[formData.fdata30, formData.fdata31, formData.fdata32]} />
									)}
									<br />
									<p>
										{formData.fdata33}
										{/* <DashedLangInput
                                            name='OwnerSign'
                                            setFieldValue={setFieldValue}
                                            inline={true}
                                            value={values.OwnerSign}
                                            className='dashedForm-control'
                                        /> */}
										<span className="ui input dashedForm-control " />
									</p>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingBuildCertificate = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingBuild, objName: 'buildingBuild', form: true },
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
			{
				api: api.certificateNote,
				objName: 'certificateNote',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingBuildCertificateView {...props} parentProps={parentProps} />}
	/>
);
export default BuildingBuildCertificate;
