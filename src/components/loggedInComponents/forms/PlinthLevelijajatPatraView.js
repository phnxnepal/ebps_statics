import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput, DashedLangInputWithSlash } from '../../shared/DashedFormInput';

import api from '../../../utils/api';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { isStringEmpty } from './../../../utils/stringUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import UnitDropdown from './../../shared/UnitDropdown';
import _ from 'lodash';
import { DashedMultiUnitArea1Input, DashedMultiUnitLength1Input } from './../../shared/EbpsUnitLabelValue';
import { LetterHead, LetterHeadFlex } from '../../shared/LetterHead';
import { PlinthLevelijajatPatraData } from './../../../utils/data/PlinthLevelijajatPatraData';
import { ConstructionTypeRadio } from './mapPermitComponents/ConstructionTypeRadio';
import { getConstructionTypeValue } from '../../../utils/enums/constructionType';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';

const Adata = PlinthLevelijajatPatraData.structureDesign;

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

const genderOptions = [
	{ key: 1, value: 'श्री', text: 'श्री' },
	{ key: 2, value: 'श्रीमती', text: 'श्रीमती' },
	{ key: 3, value: 'सुश्री', text: 'सुश्री' },
];

const areaUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

class PlinthLevelijajatPatraView extends Component {
	render() {
		const { userData, permitData, otherData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;

		const jsonData = getJsonData(prevData);

		const otherJsonData = getJsonData(otherData.mapTech);
		const mapApp = getJsonData(otherData.mapApp);

		const sadakAdhikar = () => _.max(otherJsonData.sadakAdhikarKshytra);

		const setbackMaptech = () => {
			if (otherJsonData.setBack && otherJsonData.setBack.distanceFromRoadCenter) {
				return Math.max.apply(
					Math,
					otherJsonData.setBack.map(value => value.distanceFromRoadCenter)
				);
			}
			return null;
		};

		const desApprovJsonData = otherData.designApproval;

		const anuSucKaJsonData = otherData.anukaMaster;

		let buildingClass = anuSucKaJsonData.find(value => value.id === desApprovJsonData.buildingClass);

		let initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['constructionType', 'purposeOfConstruction'],
			},
			{
				obj: otherJsonData,
				reqFields: [
					'plinthWidth',
					'plinthWidthUnit',
					'plinthLengthUnit',
					'plinthLength',
					'plinthDetails',
					'roof',
					'plinthDetailsUnit',
					'coverageDetails',
					'sadakAdhikarUnit',
					'setBackUnit',
				],
			},
			{
				obj: mapApp,
				reqFields: ['constructionType'],
			},
			{
				obj: jsonData,
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.Allowancedate)) {
			initialValues.Allowancedate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		let anusuchikaOptions = [];

		otherData.anukaMaster.map(row =>
			anusuchikaOptions.push({
				value: row.id,
				text: `${row.nameNepali} ${row.name}`,
			})
		);

		// initialValues = jsonData;

		initialValues.plinthLengthUnit = 'METRE';

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.allowancePaper, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								//     this.props.success.data.message ||
								//     'Data saved successfully'
								// );

								// setTimeout(() => {
								//     this.props.parentProps.history.push(
								//         getNextUrl(
								//             this.props.parentProps.location
								//                 .pathname
								//         )
								//     );
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div className="NJ-Main superStruConsView">
									<LetterHeadFlex userInfo={userData} />
									<div className="NJ-right">
										<DashedLangDateField
											name="Allowancedate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.Allowancedate}
											label={Adata.date}
											className="dashedForm-control"
										/>
									</div>
									<div>
										<p>
											{Adata.ps}{' '}
											<DashedLangInputWithSlash
												name="patraSankhya"
												setFieldValue={setFieldValue}
												value={values['patraSankhya']}
											/>
										</p>
										<p>
											{Adata.ps1}{' '}
											<DashedLangInputWithSlash
												name="chalaniNumber"
												setFieldValue={setFieldValue}
												value={values['chalaniNumber']}
											/>
										</p>
									</div>
									<h3 className="tipandiSubject">
										<p>
											<span>{Adata.title}</span>
										</p>
									</h3>
									<div className="NJ-Main">
										<p>
											<Select placeholder="श्री" options={genderOptions} /> {permitData.nibedakName}
											<br />
											{userData.organization.name} {Adata.wardNo} {permitData.newWardNo} ,{permitData.oldMunicipal}
											<br />
											{Adata.main_data1}{' '}
											<DashedLangDateField
												name="Allowancedate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.Allowancedate}
												label={Adata.Allowancedate}
												className="dashedForm-control"
											/>{' '}
											{Adata.main_data2}
											<br /> {userData.organization.name} {Adata.wardNo} {permitData.newWardNo} ({Adata.sabik}{' '}
											{userData.organization.name} {Adata.wardNo} {permitData.newWardNo} {Adata.kitaa} {permitData.kittaNo}{' '}
											{Adata.area}
											{permitData.landArea}{' '}
										</p>
										<p />
										<p>
											<ConstructionTypeRadio space={true} fieldLabel={Adata.list1} />
										</p>
										<p>
											{Adata.list2}
											{Object.values(Adata.checkBox_option1).map((name, index) => (
												<div key={index} className="ui radio checkbox prabidhik">
													<Field type="radio" name="roof" id={index} defaultChecked={values.roof === name} value={name} />
													<label>{name}</label>
												</div>
											))}
										</p>
										<p>
											<DashedMultiUnitArea1Input
												name="plinthDetails"
												unitName="plinthLengthUnit"
												label={Adata.list3}
												squareOptions={areaUnitOptions}
												relatedFields={['plinthLength', 'plinthWidth']}
											/>
											<DashedMultiUnitLength1Input
												name="plinthLength"
												unitName="plinthLengthUnit"
												relatedFields={['plinthWidth']}
												areaField={'plinthDetails'}
												label={Adata.lambae}
											/>
											<DashedMultiUnitLength1Input
												name="plinthWidth"
												unitName="plinthLengthUnit"
												relatedFields={['plinthLength']}
												areaField={'plinthDetails'}
												label={Adata.chaudai}
											/>
											{/* <DashedMultiUnitLength1Input
                                                    name='uchai'
                                                    unitName='plinthLengthUnit'
                                                    relatedFields={[
                                                        'plinthLength',
                                                        'plinthWidth'
                                                    ]}
                                                    areaField={'plinthDetails'}
                                                    label={Adata.uchai}
                                                /> */}
										</p>
										<p>
											{Adata.list4} {permitData.purposeOfConstruction}
										</p>
										<p>
											<span>
												{Adata.list5}
												{buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
											</span>
										</p>
										<p>
											{Adata.list6}
											<span className="ui input dashedForm-control">{sadakAdhikar()}</span>
											<UnitDropdown
												name="sadakAdhikarUnit"
												setFieldValue={setFieldValue}
												value={values.sadakAdhikarUnit}
												options={distanceOptions}
											/>
											{Adata.list6_1}
											<span className="ui input dashedForm-control">{sadakAdhikar()}</span>
											<UnitDropdown
												name="sadakAdhikarUnit1"
												setFieldValue={setFieldValue}
												value={values.sadakAdhikarUnit1}
												options={distanceOptions}
											/>
										</p>
										<p>
											{Adata.list7}
											<span className="ui input dashedForm-control">{setbackMaptech()}</span>
											<UnitDropdown
												name="setBackUnit"
												setFieldValue={setFieldValue}
												value={values.setBackUnit}
												options={distanceOptions}
											/>
										</p>
										<p>{Adata.list8}</p>
										<p>
											{Adata.list9}
											<DashedLangInput
												name="Misc"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Misc}
												error={errors.Misc}
											/>
										</p>
										<br />
										<div
											className="firstalign"
											style={{
												marginLeft: '70px',
												marginRight: '70px',
											}}
										>
											<div style={{ textAlign: 'center' }}>
												<p>
													<span className="ui input dashedForm-control" />
												</p>
												<p>{Adata.list13}</p>
											</div>
											<div style={{ textAlign: 'center' }}>
												<p>
													<span className="ui input dashedForm-control" />
												</p>
												<p>{Adata.list14}</p>
											</div>
											<div style={{ textAlign: 'center' }}>
												<p>
													<span className="ui input dashedForm-control" />
												</p>
												<p>{Adata.list15}</p>
											</div>
										</div>
									</div>
									<p>{Adata.list16_1}</p>
									<p>{Adata.list16_2}</p>
									<br />
									<div
										className="firstalign"
										style={{
											marginLeft: '70px',
											marginRight: '70px',
											textAlign: 'center',
										}}
									>
										<div></div>
										<div>
											<p>
												<span className="ui input dashedForm-control" />
											</p>
											<p>{Adata.list17}</p>
										</div>
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const PlinthLevelijajatView = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.allowancePaper, objName: 'allowancePaper', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.buildPermit,
				objName: 'mapApp',
				form: false,
			},

			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={data => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={props => <PlinthLevelijajatPatraView {...props} parentProps={parentProps} />}
	/>
);

export default PlinthLevelijajatView;
