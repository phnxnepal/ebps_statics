import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { Formik } from 'formik';

import { certifiedData } from '../../../utils/data/certificateData';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import { showToast } from '../../../utils/functionUtils';
import api from '../../../utils/api';
import { getCurrentDate } from '../../../utils/dateUtils';
import { validateString, validateNepaliDate, validateNumber } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { isBirtamod, isBiratnagar } from '../../../utils/clientUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import SaveButtonValidation from './../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
const cert = certifiedData.structureDesign;

const stringData = ['shreeName', 'gharNo'];
const rupyaData = ['dastur'];

const stringSchema = stringData.map((row) => {
	return {
		[row]: validateString,
	};
});

const rupyaSchema = rupyaData.map((row) => {
	return {
		[row]: validateNumber,
	};
});

const newSchema = Yup.object().shape(
	Object.assign(
		{
			date: validateNepaliDate,
		},
		...rupyaSchema,
		...stringSchema
	)
);

class CertificateNoteView extends Component {
	constructor(props) {
		super(props);
		const { prevData, otherData } = this.props;
		const allowancePaper = getJsonData(otherData.allowancePaper);

		const initVal = prepareMultiInitialValues(
			{ obj: { date: getCurrentDate(true), shreeName: '', gharNo: '', gharJagga: '', dastur: '' }, reqFields: [] },
			{ obj: allowancePaper, reqFields: ['gharNo'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const udata = userData;
		const user_info = this.props.userData;
		const { initVal } = this.state;

		return (
			<>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={newSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							if (isBiratnagar) {
								await this.props.postAction(`${api.namsariTippani}${permitData.nameTransaferId}`, values);
							} else {
								await this.props.postAction(`${api.certificateNote}${permitData.nameTransaferId}`, values);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.history.push(
								//     getNextUrl(this.props.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
							console.log('error', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex needsProvince={true} userInfo={user_info} />
									<CertificateNoteSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
									/>
									<div style={{ textAlign: 'justify' }}>
										{cert.content.c1}
										<DashedLangInput
											name="shreeName"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.shreeName}
											error={errors.shreeName}
										/>
										{cert.content.c2}
										{permitData.nibedakName}
										{cert.content.c3}
										{permitData.oldMunicipal}
										{cert.content.c4}
										{udata.organization.name}
										{cert.content.c5}
										{permitData.newWardNo}
										{cert.content.c6}
										<DashedLangInput
											name="gharNo"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.gharNo}
											error={errors.gharNo}
										/>
										{cert.content.c7}
										{permitData.nibedakSadak}
										{cert.content.c8}
										{permitData.kittaNo}
										{cert.content.c9}
										{permitData.landArea} {permitData.landAreaType}
										{cert.content.c10}
										<DashedLangInput
											name="gharJagga"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.gharJagga}
											error={errors.gharJagga}
										/>
										{cert.content.c11}
										<DashedLangInput
											name="dastur"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.dastur}
											error={errors.dastur}
										/>
										{cert.content.c12}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const CertificateNoteSubHeading = ({ handleChange, setFieldValue, values, errors }) => {
	return isBirtamod ? (
		<div className="centered-underlined-heading">
			<h3 className="underline-large">{cert.title1}</h3>
			<FlexSingleRight>
				<DashedLangDateField
					name="date"
					handleChange={handleChange}
					setFieldValue={setFieldValue}
					value={values.date}
					error={errors.date}
					label={cert.miti}
					inline={true}
					compact={true}
				/>
			</FlexSingleRight>
			<br />
			<h3 className="underline-large">{cert.title2}</h3>
		</div>
	) : (
		<div className="centered-underlined-heading">
			<FlexSingleRight>
				<Form.Field>
					<DashedLangDateField
						name="date"
						handleChange={handleChange}
						setFieldValue={setFieldValue}
						value={values.date}
						error={errors.date}
						label={cert.miti}
						inline={true}
						compact={true}
					/>
				</Form.Field>
			</FlexSingleRight>
			<h3 className="underline-large">{cert.title1}</h3>
			<br />
			<h3 className="underline-large">{cert.title2}</h3>
		</div>
	);
};

const CertificateNote = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: isBiratnagar ? api.namsariTippani : api.certificateNote, objName: 'certificateNote', form: true },
			{ api: api.allowancePaper, objName: 'allowancePaper', form: false },
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			// param5: [
			//     'getElementsByClassName',
			//     'ui dropdown',
			//     'innerText',
			//     distanceOptions
			// ]
		}}
		render={(props) => <CertificateNoteView {...props} parentProps={parentProps} />}
	/>
);

export default CertificateNote;
