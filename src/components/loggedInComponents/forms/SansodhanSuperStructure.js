import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { SansodhanStructure } from '../../../utils/data/SansodhanSuperStructureData';
import { Formik } from 'formik';
//import { getPermitAndUserData } from "../../../store/actions/AllowancePaperAction";

import api from '../../../utils/api';
import * as Yup from 'yup';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getCurrentDate } from '../../../utils/dateUtils';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import { FooterSignature, FooterSignatureMultiline } from './formComponents/FooterSignature';
import { isBirtamod, Client, isKamalamai } from '../../../utils/clientUtils';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { getSansodhanIjajatFooter } from '../../../utils/clientConfigs/sansodhitSuperStructure';
import { GharDhaniWaresDropdown } from './formComponents/GharDhaniWaresDropdown';
import { gharDhaniWaresOption } from '../../../utils/optionUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { NepaliNumberToWord } from './../../../utils/nepaliAmount';
import { DashedNormalInput } from './../../shared/DashedFormInput';
import { getApproveByObject } from '../../../utils/formUtils';

const ss = SansodhanStructure.structureDesign;

const SansodhanSuperSchema = Yup.object().shape(
	Object.assign({
		date: validateNullableNepaliDate,
		samsoNakxaDate: validateNullableNepaliDate,
		mapdandaHakDate: validateNullableNepaliDate,
		superStructureBuildDate: validateNullableNepaliDate,
	})
);
class SansodhanSuperStructureComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, permitData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const naksaPassData = getApproveBy(0);
		const janchData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const json_data = getJsonData(prevData);
		const json_data2 = getJsonData(otherData.supStruCons);
		const superStructBuild = getJsonData(otherData.superStructBuild);
		const allowancePaper = getJsonData(otherData.allowancePaper);

		const initialValues = prepareMultiInitialValues(
			{
				// Default values
				obj: {
					sansodhanSuperIijajatDate: getCurrentDate(true),
					date: getCurrentDate(true),
					samsoNakxaDate: getCurrentDate(true),
					mapdandaHakDate: getCurrentDate(true),
					gharDaniWares: 'gd',
					shreeName: permitData.nibedakName,
					nayaMiti: json_data2.superStruSubmitDate,
				},
				reqFields: [],
			},
			{
				obj: allowancePaper,
				reqFields: ['gharNo'],
			},
			{
				obj: superStructBuild,
				reqFields: ['superStructureBuildDate'],
			},
			{ obj: json_data, reqFields: [] },
			{
				obj: {
					naksaPassSignature: naksaPassData.signature,
					janchSignature: janchData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		this.state = {
			initialValues,
		};
	}
	render() {
		const { initialValues } = this.state;
		const {
			permitData,
			errors: reduxErrors,
			userData,
			prevData,
			orgCode,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;

		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={SansodhanSuperSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.sansodhanSupStruIjjaat}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, setFieldValue, values, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex needsProvince={isBirtamod} userInfo={userData} />

									<FlexSingleRight>
										<DashedLangDateField
											name="date"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.date}
											error={errors.date}
											label={ss.miti}
										/>
									</FlexSingleRight>
									<SectionHeader>
										{isKamalamai && <h2 className="underline end-section">{ss.kamalamaiData.title}</h2>}
										<h3 className="underline">{ss.subject}</h3>
									</SectionHeader>
									<br />
									{isKamalamai ? (
										<div style={{ textAlign: 'justify' }} className="no-margin-field">
											{ss.kamalamaiData.data_1}
											{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
											{ss.kamalamaiData.data_2}
											{permitData.nibedakTol}
											{ss.kamalamaiData.data_3}
											{permitData.kittaNo}
											{ss.kamalamaiData.data_4}
											{permitData.landArea} {permitData.landAreaType}
											{ss.kamalamaiData.data_5}
											{permitData.nibedakName}
											{ss.kamalamaiData.data_6}
											{permitData.applicationNo}
											{ss.kamalamaiData.data_7}
											<DashedLangDateField
												inline={true}
												compact={true}
												name="superStructureBuildDate"
												setFieldValue={setFieldValue}
												value={values.superStructureBuildDate}
												error={errors.superStructureBuildDate}
											/>
											{ss.kamalamaiData.data_8}
											<DashedLangInput
												name="prabidikShree"
												setFieldValue={setFieldValue}
												value={values.prabidikShree}
												error={errors.prabidikShree}
											/>
											{ss.kamalamaiData.data_9}
											<DashedNormalInput
												name="dasturAmount"
												value={values.dasturAmount}
												handleChange={handleChange}
												error={errors.dasturAmount}
											/>
											{ss.kamalamaiData.data_10}
											{NepaliNumberToWord.getNepaliWord(values.dasturAmount)}
											{ss.kamalamaiData.data_11}
										</div>
									) : (
										<>
											<div style={{ textAlign: 'justify' }} className="no-margin-field">
												<div>
													{ss.shree}
													<DashedLangInput
														name="shreeName"
														setFieldValue={setFieldValue}
														value={values.shreeName}
														error={errors.shreeName}
													/>

													<br />
												</div>
												{userData.organization.name}
												{ss.content.content1}
												{permitData.nibedakTol}
												{ss.content.content2}
												<DashedLangInput
													name="gharNo"
													setFieldValue={setFieldValue}
													value={values.gharNo}
													error={errors.gharNo}
												/>
												{ss.content.content3}
												{permitData.nibedakSadak}
												<br />
												<br />
												{ss.content.content4}
												<DashedLangDateField
													inline={true}
													name="superStructureBuildDate"
													setFieldValue={setFieldValue}
													value={values.superStructureBuildDate}
													error={errors.superStructureBuildDate}
												/>
												{ss.content.content5}
												{permitData.oldMunicipal}
												{ss.content.content6}
												{permitData.oldWardNo}
												{ss.content.content7}
												{permitData.newWardNo}
												{ss.content.content8}
												{permitData.kittaNo}
												{ss.content.content9}
												{permitData.landArea} {permitData.landAreaType}
												{ss.content.content10}
												<DashedLangDateField
													inline={true}
													name="samsoNakxaDate"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.samsoNakxaDate}
													error={errors.samsoNakxaDate}
												/>
												{ss.content.content11}
												<br />
												{ss.content.content12}
												<DashedLangDateField
													inline={true}
													name="mapdandaHakDate"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.mapdandaHakDate}
													error={errors.mapdandaHakDate}
												/>
												{ss.content.content13}
											</div>
											{renderFooterSection(orgCode, values, useSignatureImage)}
										</>
									)}
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const renderFooterSection = (orgCode, values, useSignatureImage) => {
	if (orgCode === Client.SUNDARHARAICHA || orgCode === Client.INARUWA) {
		return (
			<>
				<br />
				<div className="margin-bottom">{renderSignature(orgCode, values, useSignatureImage)}</div>
				<br />
				<div className="margin-bottom">{ss.content.content14}</div>
				<div className="flex-item-space-between signature-div">
					<div style={{ textAlign: 'center' }}>
						<span className="ui input dashedForm-control" />
						<div>
							<GharDhaniWaresDropdown name="gharDaniWares" />
							{ss.content.content15}
						</div>
					</div>
					<div style={{ textAlign: 'center' }}>
						<span className="ui input signature-placeholder" />
						<div>{ss.footer.phantwalaSign}</div>
					</div>
				</div>
			</>
		);
	} else {
		return (
			<>
				<div className="margin-bottom">
					{ss.content.content14}
					{!isBirtamod && (
						<>
							<br />
							<br />
							<GharDhaniWaresDropdown name="gharDaniWares" />
							{ss.content.content15}:
							<span className="ui input dashedForm-control" />
						</>
					)}
				</div>
				{renderSignature(orgCode, values, useSignatureImage)}
			</>
		);
	}
};

const renderSignature = (orgCode, values, useSignatureImage) => {
	const { designations } = getSansodhanIjajatFooter(orgCode);
	if (orgCode === Client.SUNDARHARAICHA) {
		return (
			<FooterSignatureMultiline
				designations={designations}
				signatureImages={useSignatureImage && [values.naksaPassSignature, values.janchSignature, values.pramukhSignature]}
			/>
		);
	} else {
		return (
			<FooterSignature
				designations={designations}
				signatureImages={useSignatureImage && [values.naksaPassSignature, values.janchSignature, values.pramukhSignature]}
			/>
		);
	}
};

const SansodhanSuperStructure = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.sansodhanSupStruIjjaat,
				objName: 'sanSupStructIijajat',
				form: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: false,
			},
			{
				api: api.superStructureBuild,
				objName: 'superStructBuild',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowancePaper',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText', gharDhaniWaresOption],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		render={(props) => <SansodhanSuperStructureComponent {...props} parentProps={parentProps} />}
	/>
);

export default SansodhanSuperStructure;
