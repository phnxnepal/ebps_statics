import React from 'react';

import api from '../../../utils/api';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { GenericTippaniAdes } from './formComponents/GenericTippaniAdes';
import { genericTippaniAdes } from '../../../utils/data/genericFormData';

const SansodhanSuperTippaniAdesh = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.sansodhanSuperTippaniAdesh,
				objName: 'sansodhanSuperTippani',
				form: true,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'textarea', 'value'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => (
			<GenericTippaniAdes
				fieldName="nibedan"
				topic={genericTippaniAdes.sansodhanSuperStructureTippani.topic}
				title={genericTippaniAdes.title}
				api={api.sansodhanSuperTippaniAdesh}
				{...props}
				parentProps={parentProps}
			/>
		)}
	/>
);

export default SansodhanSuperTippaniAdesh;
