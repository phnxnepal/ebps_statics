import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import { getMessage } from '../../shared/getMessage';
import { WarersnamaData } from '../../../utils/data/waresnamaData';
import api from '../../../utils/api';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import LongFormDate from './formComponents/LongFormDate';
import { DashedLangInputWithSlash, CompactDashedLangInput } from '../../shared/DashedFormInput';
import {
	FingerprintSignature,
	FingerprintSignatureBirtamod,
	FingerprintSignatureBirtamodVertical,
	FingerprintSignatureVertical,
} from './formComponents/FingerprintSignature';
import { isBirtamod, isBiratnagar } from '../../../utils/clientUtils';
import { NamaFormCommonBody, WitnessSignature } from './formComponents/NamaFormComponents';
import { namaFormData } from '../../../utils/data/namaFormData';
import ErrorDisplay from '../../shared/ErrorDisplay';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';

const Wdata = WarersnamaData.structureDesign;
const reqData = WarersnamaData.sakchiBiratnagar;

const messageId = 'WarersnamaData.structureDesign';

const initialValues = {
	sakchiWadaNumber: '',
	sakchi2WadaNumber: '',
	tol: '',
	basneBarsa: '',
	koNateDar: '',
};

class WarersnamaView extends Component {
	constructor(props) {
		super(props);
		let initVal = prepareMultiInitialValues(
			{
				obj: initialValues,
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initVal,
		};
	}

	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		//console.log("all data===", userData, permitData);
		const { initVal } = this.state;

		return (
			<>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.waresnama, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, handleChange, handleSubmit, values, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div className="header-underlined-wrap">
									<h2>{getMessage(`${messageId}.heading`, Wdata.heading)}</h2>
								</div>
								<Grid>
									<Grid.Row>
										<Grid.Column width={4}>
											{isBirtamod ? <FingerprintSignatureBirtamodVertical /> : <FingerprintSignatureVertical />}
										</Grid.Column>
										<Grid.Column width={12} style={{ textAlign: 'justify' }}>
											<NamaFormCommonBody
												permitData={permitData}
												userData={userData}
												setFieldValue={setFieldValue}
												values={values}
												errors={errors}
											/>
											{getMessage(`${messageId}.main6`, namaFormData.commonData.main6)} {userData.organization.name}{' '}
											{getMessage(`${messageId}.main7`, namaFormData.commonData.main7)} {userData.organization.name}{' '}
											{getMessage(`${messageId}.ward`, namaFormData.commonData.ward)}
											<DashedLangInputWithSlash
												name="waresWard"
												setFieldValue={setFieldValue}
												value={values.waresWard}
												error={errors.waresWard}
											/>{' '}
											{getMessage(`${messageId}.main2`, namaFormData.commonData.main2)}
											<DashedLangInputWithSlash
												name="waresAge"
												setFieldValue={setFieldValue}
												value={values.waresAge}
												error={errors.waresAge}
											/>{' '}
											{getMessage(`${messageId}.main3`, namaFormData.commonData.main3)}
											<DashedLangInputWithSlash
												name="waresName"
												setFieldValue={setFieldValue}
												value={values.waresName}
												error={errors.waresName}
											/>{' '}
											{getMessage(`${messageId}.main8`, namaFormData.commonData.main8)} {userData.organization.name}{' '}
											{getMessage(`${messageId}.main9`, namaFormData.commonData.main9)}
											<DashedLangInputWithSlash
												name="nominee"
												setFieldValue={setFieldValue}
												value={values.nominee}
												error={errors.nominee}
											/>{' '}
											{getMessage(`${messageId}.main10`, namaFormData.commonData.main10)}
											<br />
											<br />
											<LongFormDate />
											<br />
											{isBirtamod ? <FingerprintSignatureBirtamod /> : <FingerprintSignature />}
										</Grid.Column>
									</Grid.Row>
									<br />
									<br />
									<br />

									{isBiratnagar ? (
										<div>
											<div>
												<h3>{reqData.saachi}</h3>
												<div>
													{reqData.one}
													{reqData.bimanapaWadaNumber}
													{permitData.newWardNo}
												</div>
												<div>
													{reqData.two}
													{reqData.bimanapaWadaNumber}
													<CompactDashedLangInput
														name="sakchiWadaNumber"
														value={values.sakchiWadaNumber}
														setFieldValue={setFieldValue}
													/>
													{reqData.yoWaresNaama}
													<CompactDashedLangInput
														name="sakchi2WadaNumber"
														value={values.sakchi2WadaNumber}
														setFieldValue={setFieldValue}
													/>
													{reqData.tol}
													<CompactDashedLangInput name="tol" value={values.tol} setFieldValue={setFieldValue} />
													{reqData.basneBarsa}
													<CompactDashedLangInput
														name="basneBarsa"
														value={values.basneBarsa}
														setFieldValue={setFieldValue}
													/>
													{reqData.ko}
													<CompactDashedLangInput name="koNatedar" value={values.koNatedar} setFieldValue={setFieldValue} />
												</div>
											</div>
										</div>
									) : (
										<WitnessSignature setFieldValue={setFieldValue} values={values} errors={errors} />
									)}
								</Grid>
							</div>
							<br />
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const WarersnamaMain = (parentProps) => (
	<FormContainerV2
		api={[{ api: api.waresnama, objName: 'waresnama', form: true }]}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <WarersnamaView {...props} parentProps={parentProps} />}
	/>
);
export default WarersnamaMain;
