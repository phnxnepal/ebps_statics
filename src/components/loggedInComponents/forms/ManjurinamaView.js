import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import { ManjurinamaData } from './../../../utils/data/ManjurinamaData';
import { getMessage } from '../../shared/getMessage';
import api from '../../../utils/api';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import { WitnessSignature, NamaFormCommonBody } from './formComponents/NamaFormComponents';
import { isBirtamod } from '../../../utils/clientUtils';
import {
	FingerprintSignatureBirtamod,
	FingerprintSignature,
	FingerprintSignatureBirtamodVertical,
	FingerprintSignatureVertical,
} from './formComponents/FingerprintSignature';
import LongFormDate from './formComponents/LongFormDate';
import { DashedLangInputWithSlash } from '../../shared/DashedFormInput';
import ErrorDisplay from '../../shared/ErrorDisplay';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';

const Mdata = ManjurinamaData.structureDesign;
const messageId = 'ManjurinamaData.structureDesign';

class ManjurinamaView extends Component {
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;

		let initVal = {};

		initVal = getJsonData(prevData);
		return (
			<>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.manjurinama, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, validateForm, handleSubmit, values, errors, setFieldValue }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div className="header-underlined-wrap">
									<h2>{getMessage(`${messageId}.heading`, Mdata.heading)}</h2>
								</div>
								<Grid>
									<Grid.Row>
										<Grid.Column width={4}>
											{isBirtamod ? <FingerprintSignatureBirtamodVertical /> : <FingerprintSignatureVertical />}
										</Grid.Column>
										<Grid.Column width={12} style={{ textAlign: 'justify' }}>
											<NamaFormCommonBody userData={userData} permitData={permitData} />
											{getMessage(`${messageId}.main6`, Mdata.main6)} {userData.organization.name}{' '}
											{getMessage(`${messageId}.main7`, Mdata.main7)} {userData.organization.name}{' '}
											{getMessage(`${messageId}.ward`, Mdata.ward)}
											<DashedLangInputWithSlash
												name="manjuriWard"
												setFieldValue={setFieldValue}
												value={values.manjuriWard}
												error={errors.manjuriWard}
											/>{' '}
											{getMessage(`${messageId}.main8`, Mdata.main8)}
											<DashedLangInputWithSlash
												name="manjuriAge"
												setFieldValue={setFieldValue}
												value={values.manjuriAge}
												error={errors.manjuriAge}
											/>{' '}
											{getMessage(`${messageId}.main9`, Mdata.main9)}
											<DashedLangInputWithSlash
												name="manjuriName"
												setFieldValue={setFieldValue}
												value={values.manjuriName}
												error={errors.manjuriName}
											/>{' '}
											{getMessage(`${messageId}.main10`, Mdata.main10)}
											<DashedLangInputWithSlash
												name="nominee"
												setFieldValue={setFieldValue}
												value={values.nominee}
												error={errors.nominee}
											/>{' '}
											{getMessage(`${messageId}.main11`, Mdata.main11)}
											<br />
											<br />
											<LongFormDate />
											<br />
											{isBirtamod ? <FingerprintSignatureBirtamod /> : <FingerprintSignature />}
										</Grid.Column>
									</Grid.Row>
									<WitnessSignature setFieldValue={setFieldValue} values={values} errors={errors} />
								</Grid>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const ManjurinamaMain = parentProps => (
	<FormContainerV2
		api={[{ api: api.manjurinama, objName: 'manjurinama', form: true }]}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <ManjurinamaView {...props} parentProps={parentProps} />}
	/>
);

export default ManjurinamaMain;
