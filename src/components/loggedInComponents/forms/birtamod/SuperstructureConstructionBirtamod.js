import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';

import { superstructureconstructionview } from '../../../../utils/data/mockLangFile';
import { getMessage } from '../../../shared/getMessage';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';

import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, FloorArray } from '../../../../utils/dataUtils';
import { SuperStructConstBirtamodSchema } from '../../formValidationSchemas/superStructureConstructionValidation';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { PatraSankhyaAndDate } from '../formComponents/PatraSankhyaAndDate';
import { SuperStructConsBody, SuperStructConstBodyLastSectionBirtamod } from '../ijajatPatraComponents/SuperStructureConsComponents';
import { FooterSignature } from '../formComponents/FooterSignature';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { SectionHeader } from '../../../uiComponents/Headers';

const sscv_data = superstructureconstructionview.superstructureconstructionview_data;

// const permitLang = buildingPermitApplicationForm.permitApplicationFormView;

// const mapTechLang = mapTechnical.mapTechnicalDescription;

// const buildPermitLang = buildingPermitApplicationForm.permitApplicationFormView;

const messageId = 'superstructureconstructionview.superstructureconstructionview_data';

// const areaUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];

// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];

// export const landAreaTypeOptions = [
// 	{ key: 1, value: 'रोपनी–आना–पैसा–दाम', text: 'रोपनी–आना–पैसा–दाम' },
// 	{ key: 2, value: 'बिघा–कठ्ठा–धुर', text: 'बिघा–कठ्ठा–धुर' },
// 	{ key: 3, value: 'वर्ग फिट', text: 'वर्ग फिट' },
// 	{ key: 4, value: 'वर्ग मिटर ', text: 'वर्ग मिटर' },
// ];

class SuperstructureConstructionViewComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData, otherData, enterByUser, userData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const rajaswo = getJsonData(otherData.rajaswo);
		// const suikritTalla = this.props.otherData.anusuchiGha.tallaThap;

		// const sadakAdhikar = () => _.max(otherJsonData.sadakAdhikarKshytra);

		// const setbackMaptech = () => {
		// 	if (otherJsonData.setBack) {
		// 		return Math.max.apply(
		// 			Math,
		// 			otherJsonData.setBack.map(value => value.distanceFromRoadCenter)
		// 		);
		// 	}
		// 	return null;
		// };

		const desApprovJsonData = otherData.designApproval;

		const anuSucKaJsonData = otherData.anukaMaster;
		const suikritTalla = otherData.anusuchiGha.thapTalla;
		// console.log('talla---', suikritTalla);

		const allowance = getJsonData(otherData.allowance);
		const noteorder = getJsonData(otherData.noteorder);
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);

		const floorArray = new FloorArray(permitData.floor);
		const floorMax = floorArray.getTopFloor().nepaliCount;

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					// plinthDetailsUnit: 'METRE',
					// buildCoverAreaUnit: 'METRE',
					// coverageDetailsUnit: 'METRE',
					buildingAreaUnit: DEFAULT_UNIT_AREA,
					coverageAreaUnit: DEFAULT_UNIT_AREA,
					houseLengthUnit: DEFAULT_UNIT_LENGTH,
					houseBreadthUnit: DEFAULT_UNIT_LENGTH,
					houseHeightUnit: DEFAULT_UNIT_LENGTH,
					constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					plinthDetailsUnit: DEFAULT_UNIT_AREA,
					buildCoverAreaUnit: DEFAULT_UNIT_AREA,
					coverageDetailsUnit: DEFAULT_UNIT_AREA,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					sabikPlinthArea: '',
					sabikCoverArea: '',
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['constructionType', 'purposeOfConstruction', 'landArea', 'landAreaType'] },
			{
				obj: mapTech,
				reqFields: ['approveDate', 'plinthDetails', 'plinthDetailsUnit', 'roof', 'coverageDetails', 'buildingHeight', 'allowableHeight'],
			},
			{ obj: rajaswo, reqFields: ['allowableHeight', 'constructionHeight', 'constructionHeightUnit'] },
			// {
			// 	obj: {
			// 		setbackMetre: setbackMaptech(),
			// 		sadakMetre: sadakAdhikar()

			// 	}, reqFields: []
			// }
			{
				obj: allowance,
				reqFields: ['allowanceDate', 'gharNo', 'publicPropertyDistance', 'publicPropertyUnit', 'sadakAdhikarUnit', 'requiredDistance'],
			},
			{
				obj: noteorder,
				reqFields: ['noteOrderDate'],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.supStrucDate)) {
			initialValues.supStrucDate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues.gharDhaniDate)) {
			initialValues.gharDhaniDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);
		this.state = {
			isSucess: false,
			initialValues,
			suikritTalla,
			buildingClass,
			floorMax,
		};
	}
	// state = {
	// 	isSucess: false,
	// };

	render() {
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;
		const { initialValues, suikritTalla, buildingClass, floorMax } = this.state;
		const user_info = this.props.userData;
		return (
			// <div className='superStruConsView'>

			<Formik
				initialValues={initialValues}
				validationSchema={SuperStructConstBirtamodSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						await this.props.postAction(`${api.superStructureConstruction}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error in Structure Construction Save', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							<div>
								<LetterHeadFlex userInfo={user_info} />
								<br />
								<SectionHeader>
									<h3 className="underline">{[getMessage(`${messageId}.title`, sscv_data.title)]}</h3>
								</SectionHeader>
								<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} dateFieldName="supStrucDate" />
								<br />

								<SuperStructConsBody
									floorMax={floorMax}
									suikritTalla={suikritTalla}
									permitData={permitData}
									buildingClass={buildingClass}
									userData={userData}
									values={values}
									setFieldValue={setFieldValue}
									errors={errors}
								/>
								<SuperStructConstBodyLastSectionBirtamod setFieldValue={setFieldValue} values={values} errors={errors} />
								<FooterSignature
									designations={[sscv_data.footer.sign_field, sscv_data.footer.sign_sipharis, sscv_data.footer.sign_swikrit]}
									signatureImages={useSignatureImage && [values.subSignature, values.sipharisSignature, values.swikritSignature]}
								/>
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
						{/* <SaveButton
							errors={errors}
							formUrl={this.props.parentProps.location.pathname}
							hasSavePermission={this.props.hasSavePermission}
							prevData={getJsonData(prevData)}
							handleSubmit={handleSubmit}
						/> */}
					</Form>
				)}
			</Formik>
		);
	}
}
const SuperstructureConstructionBirtamod = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.anusuchiGha,
				objName: 'anusuchiGha',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthTechApp',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteorder',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			//param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			//param9: ['getElementsByClassName', 'brackets', 'value'],
		}}
		render={(props) => <SuperstructureConstructionViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperstructureConstructionBirtamod;
