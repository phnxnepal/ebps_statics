import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { getCurrentDate } from './../../../../utils/dateUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { AllowancePaperBody, Subject, AllowanceFooterBirtamod, AllowancePaperSchema } from '../ijajatPatraComponents/AllowancePaperComponents';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import { PatraSankhyaAndDate } from '../formComponents/PatraSankhyaAndDate';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import { footerSignature } from '../../../../utils/data/genericFormData';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';

class AllowancePaperViewComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, userData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;

		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const prabidhikPratibedan = getJsonData(otherData.prabidhikPratibedan);
		const RajaswoData = getJsonData(otherData.RajaswoData);
		const desApprovJsonData = otherData.designApproval;

		const anuSucKaJsonData = otherData.anukaMaster;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);

		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);

		let initialValues = prepareMultiInitialValues(
			{
				obj: prabidhikPratibedan,
				reqFields: ['floor'],
			},
			{
				obj: RajaswoData,
				reqFields: ['requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: mapTech,
				reqFields: [
					'constructionType',
					'purposeOfConstruction',
					'purposeOfConstructionOther',
					'roof',
					'roofOther',
					'publicPropertyDistance',
					'publicPropertyUnit',
				],
			},
			{
				obj: {
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					floorareaUnit: DEFAULT_UNIT_LENGTH,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					floorwidthUnit: DEFAULT_UNIT_LENGTH,
					floorheightUnit: DEFAULT_UNIT_LENGTH,
					patraSankhya: '',
					chalaniNumber: '',
					gharNo: '',
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.allowanceDate)) {
			initialValues.allowanceDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		let anusuchikaOptions = [];

		otherData.anukaMaster.map((row) =>
			anusuchikaOptions.push({
				value: row.id,
				text: `${row.nameNepali} ${row.name}`,
			})
		);
		this.state = {
			initialValues,
			buildingClass,
		};
	}
	render() {
		const { userData, permitData, prevData, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, useSignatureImage } = this.props;
		const { initialValues, buildingClass } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={AllowancePaperSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.allowancePaper, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="build-gaps">
								<LetterHeadFlex userInfo={userData} />
								<Subject />
								<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} dateFieldName={'allowanceDate'} />
								<br />
								<AllowancePaperBody
									values={values}
									errors={errors}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									buildingClass={buildingClass}
									permitData={permitData}
									userData={userData}
								/>
								<AllowanceFooterBirtamod />
								<FooterSignatureMultiline
									designations={[[footerSignature.fieldNirikshan], [footerSignature.sipharis], [footerSignature.swikrit]]}
									signatureImages={useSignatureImage && [values.subSignature, values.sipharisSignature, values.swikritSignature]}
								/>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const AllowancePaperBirtamod = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.allowancePaper, objName: 'allowancePaper', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabidhikPratibedan',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'RajaswoData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <AllowancePaperViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default AllowancePaperBirtamod;
