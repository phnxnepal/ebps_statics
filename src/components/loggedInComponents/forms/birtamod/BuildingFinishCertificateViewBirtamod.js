import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, getOptionText } from '../../../../utils/dataUtils';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import api from '../../../../utils/api';
import { constructionTypeSelectOptions } from '../../../../utils/data/genericData';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { CertificateSubHeading } from '../certificateComponents/CertificateComponents';
import { certificateBuildingClassOptions } from '../../../../utils/optionUtils';
import { translateEngToNep } from '../../../../utils/langUtils';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import { FooterSignature } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FinishCertificateTabularFloorBlockInputs, HeightFloorNumberSection } from '../certificateComponents/CertificateFloorBlockInputs';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import EbpsTextareaField from '../../../shared/MyTextArea';

const Detail = BuildingFinishCertificateData.details;
// const formData = BuildingFinishCertificateData.formdata;
const naaya = BuildingFinishCertificateData.naya;
const formDataBirtamod = BuildingFinishCertificateData.birtamodFormData;
// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];
// const field_9 = [
// 	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
// 	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
// ];
// const squareUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];
class BuildingFinishCertificateView extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};
		const prabhidik = getJsonData(otherData.prabhidik);
		const dosrocharan = getJsonData(otherData.dosrocharan);
		const mapTech = getJsonData(otherData.mapTech);
		const allowance = getJsonData(otherData.allowance);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const designApprovalData = otherData.designApprovalData;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const topFloor = floorArray.getTopFloor();
		const bottomFloor = floorArray.getBottomFloor();
		// const floorArray = new FloorArray(permitData.floor);
		// const {topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		// const floorMax = topFloor.nepaliCount;
		// const highNepaliFloor = topFloor.nepaliCountName;
		// const bottomNepaliFloor = bottomFloor.nepaliCountName;

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['surrounding', 'photo'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', topFloor),
					...floorArray.getInitialValue('nepaliCountName', 'endFloor', topFloor),
					...floorArray.getInitialValue('nepaliCountName', 'startFloor', bottomFloor),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit()
				},
				reqFields: [],
			},
			{
				obj: designApprovalData,
				reqFields: ['buildingClass'],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'purposeOfConstruction', 'buildingHeight'],
			},
			{ obj: allowance, reqFields: ['gharNo'] },
			{
				obj: prabhidik,
				reqFields: [
					'constructionType',
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'requiredDistance',
					'sadakAdhikarUnit',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'elecVolt',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{
				obj: dosrocharan,
				reqFields: ['roomCount', 'windowCount', 'doorCount', 'shutterCount'],
			},
			// {
			// 	obj: { buildingArea: floorArray.getSumOfAreas(), floorNumber: floorMax, startFloor: bottomNepaliFloor, endFloor: highNepaliFloor },
			// 	reqFields: [],
			// },
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		initialValues1.constructionType = getOptionText(getConstructionTypeValue(initialValues1.constructionType), constructionTypeSelectOptions);

		const initVal = merge(initialValues, initialValues1);
		// console.log(initVal);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
			blocks: floorArray.getBlocks(),
			// topFloor,
			// bottomFloor
		};
	}
	render() {
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const user_info = this.props.userData;
		const { initVal, floorArray, formattedFloors, blocks } = this.state;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} needsOfficeName={false} />
									<CertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
									/>
									<div>
										{`${Detail.shree} ${permitData.applicantName}${Detail.nagarpalika} ${Detail.wda} ${permitData.newWardNo} ${Detail.nimnaBamojim}`}
									</div>
									<div>
										{formDataBirtamod.jaggaDhani}
										{permitData.applicantName}
									</div>
									<div>
										{formDataBirtamod.gharDhani}
										{permitData.nibedakName}
									</div>
									<div>
										{naaya.data3}
										{naaya.data4.d1}
										{permitData.oldMunicipal}
										{naaya.data4.d2}
										{permitData.oldWardNo}
										{naaya.data4.d3}
										<DashedLangInput name="gharNo" setFieldValue={setFieldValue} value={values.gharNo} />
										{naaya.data4.d4}
										{permitData.buildingJoinRoad}
										{naaya.data4.d5}
										{permitData.kittaNo}
										{naaya.data4.d6}
										{permitData.landArea} {permitData.landAreaType}
										{naaya.data4.d7}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											value={values.purposeOfConstruction}
										/>
									</div>
									<div className="flex-div">
										<div>{naaya.data5.surrounding}:</div>
										<div>
											{values.surrounding &&
												values.surrounding.map((index, i) => (
													<div key={i}>
														{surrounding.find(fl => fl.direction === index.side).value}
														<DashedLangInput
															name={`surrounding.${i}.kittaNo`}
															setFieldValue={setFieldValue}
															value={getIn(values, `surrounding.${i}.kittaNo`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.kittaNo`)}
														/>
													</div>
												))}
										</div>
									</div>
									<div>
										{naaya.data6.f1}
										{certificateBuildingClassOptions.map(option => (
											<div key={option.value} className="ui radio checkbox prabidhik">
												<input
													type="radio"
													name="buildingClass"
													defaultChecked={values.buildingClass === option.value}
													value={option.value}
												/>
												<label>{option.text}</label>
											</div>
										))}
										&emsp;
										{naaya.data7.g1}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
									</div>

									<div className="no-margin-field">
										{naaya.data7.g2}
										{': '}
										{translateEngToNep(permitData.applicationNo)} &emsp;
										{naaya.data7.g3}
										<DashedLangDateField
											name="patraDate"
											inline={true}
											setFieldValue={setFieldValue}
											error={errors.patraDate}
											value={values.patraDate}
										/>
									</div>

									<FinishCertificateTabularFloorBlockInputs
										floorArray={floorArray}
										showLetterNumer={true}
										showFloorRange={true}
										formattedFloors={formattedFloors}
										data={{ floorRangeSuffix: naaya.data8.talla }}
									/>

									<div>
										{naaya.data9.j2}
										<DashedLangInput name="coveragePercent" setFieldValue={setFieldValue} value={values.coveragePercent} />
										{naaya.data9.j3} {' (%): '}
										<DashedLangInput name="allowable" setFieldValue={setFieldValue} value={values.allowable} />
									</div>
									<HeightFloorNumberSection floorArray={floorArray} blocks={blocks} />
									<div>
										<DashedLengthInputWithRelatedUnits
											name="requiredDistance0"
											unitName="sadakAdhikarUnit"
											relatedFields={['sadakAdhikarKshytra0']}
											label={naaya.data9.j6}
										/>
										<DashedLengthInputWithRelatedUnits
											name="sadakAdhikarKshytra0"
											unitName="sadakAdhikarUnit"
											relatedFields={['requiredDistance0']}
											label={naaya.data9.j7}
										/>
									</div>

									<div>
										<DashedLengthInputWithRelatedUnits
											name="highTension.0.value"
											unitName="highTensionUnit"
											relatedFields={['highTension.1.value']}
											label={naaya.data9.j8}
										/>
										<DashedLengthInputWithRelatedUnits
											name="highTension.1.value"
											unitName="highTensionUnit"
											relatedFields={['highTension.0.value']}
											label={naaya.data9.j7}
										/>
									</div>
									<div>
										<DashedLengthInputWithRelatedUnits
											name="publicPropertyRequiredDistance"
											unitName="publicPropertyUnit"
											relatedFields={['publicPropertyDistance']}
											label={naaya.data9.j9}
										/>
										<DashedLengthInputWithRelatedUnits
											name="publicPropertyDistance"
											unitName="publicPropertyUnit"
											relatedFields={['publicPropertyRequiredDistance']}
											label={naaya.data9.j7}
										/>
									</div>
									<div>
										{naaya.data9.j10}
										<DashedLangInput
											name="dhalNikasArrangement"
											setFieldValue={setFieldValue}
											value={values.dhalNikasArrangement}
										/>
									</div>
									<div className="no-margin-field">
										{naaya.data9.j11}
										<DashedLangDateField
											name="nirmanDate"
											inline={true}
											setFieldValue={setFieldValue}
											error={errors.nirmanDate}
											value={values.nirmanDate}
											className="dashedForm-control"
										/>
										{naaya.data9.j12}
										<DashedLangDateField
											name="endDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.endDate}
											error={errors.endDate}
											className="dashedForm-control"
										/>
									</div>

									<div>
										{naaya.data9.j13}
										{Object.values(naaya.data9.j13_option).map(option => (
											<div className="ui radio checkbox prabidhik" key={option}>
												<input
													type="radio"
													name="palna"
													value={option}
													defaultChecked={values.palna === option}
													onChange={handleChange}
												/>
												<label>{option}</label>
											</div>
										))}

										{values.palna === naaya.data9.j13_option.option_2 && (
											<div>
												{naaya.data9.j14}
												<DashedLangInput
													name="palanaBibaran"
													placeholder="Additional Information..."
													setFieldValue={setFieldValue}
													value={values.palanaBibaran}
													error={errors.palanaBibaran}
												/>
											</div>
										)}
									</div>
									<EbpsTextareaField
											placeholder="अन्य विवरण"
											name="buildFinish"
											setFieldValue={setFieldValue}
											value={values.buildFinish}
											error={errors.buildFinish}
									/>
								</div>
								<FooterSignature designations={[naaya.signs.s1, naaya.signs.s2, naaya.signs.s3, naaya.signs.s4]} />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishViewBirtamod = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.DosrocharanPrabidhikView,
				objName: 'dosrocharan',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ["15dayspecial"]
		}}
		hasFile={true}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <BuildingFinishCertificateView {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishViewBirtamod;
