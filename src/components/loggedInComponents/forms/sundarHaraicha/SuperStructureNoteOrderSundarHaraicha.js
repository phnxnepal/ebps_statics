import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { superstructureSundarHaraicha } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';
import { DashedLangDateField } from './../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { PillarGaraTable } from '../formComponents/PlinthLevelComponents';
import { PatraSankhya } from '../formComponents/PatraSankhyaAndDate';

import { getMessage } from '../../../shared/getMessage';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import * as Yup from 'yup';
import { showToast } from '../../../../utils/functionUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { prepareMultiInitialValues } from './../../../../utils/dataUtils';
import { DashedAreaUnitInput } from '../../../shared/EbpsUnitLabelValue';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { PrintParams } from '../../../../utils/printUtils';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';

const superSunder = superstructureSundarHaraicha.content;
const messageId = 'superstructureSundarHaraicha.content';
const SuperStructureNoteSchema = Yup.object().shape(
	Object.assign({
		dates: validateNullableNepaliDate,
		peshGarneDate: validateNullableNepaliDate,
	})
);
class SuperStructureNoteOrderSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);
		const plinth = getJsonData(otherData.plinth);
		initVal = prepareMultiInitialValues(
			{
				obj: plinth,
				reqFields: [],
			},
			{ obj: json_data, reqFields: [] }
		);
		initVal.otherAreaUnit = 'SQUARE METRE';
		if (isStringEmpty(initVal.dates)) {
			initVal.dates = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.peshGarneDate)) {
			initVal.peshGarneDate = getCurrentDate(true);
		}

		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const pdata = this.props.permitData;
		const udata = userData;
		const user_info = this.props.userData;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={SuperStructureNoteSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.SuperStructureNoteOrder}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
									<SectionHeader>
										<h3 className="normal">{superSunder.superTitle.title_1}</h3>
										<h3 className="normal underline end-section">{superSunder.superTitle.title_2}</h3>
										<h3 className="underline end-section">{superSunder.superTitle.title_3}</h3>
									</SectionHeader>
									<FlexSingleRight>
										{`${superSunder.miti}`}
										<DashedLangDateField
											compact={true}
											name="dates"
											value={values.dates}
											error={errors.dates}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</FlexSingleRight>
									<div style={{ textAlign: 'justify ' }}>
										<p>
											<span style={{ marginLeft: '30px' }}>{udata.organization.name}</span>
											{getMessage(`${messageId}.data_1`, superSunder.data_1)}
											<span>{pdata.nibedakTol}</span>
											{getMessage(`${messageId}.data_2`, superSunder.data_2)}
											<span>{pdata.sadak}</span>
											{getMessage(`${messageId}.data_3`, superSunder.data_3)}
											<span>{pdata.nibedakName}</span>
											{getMessage(`${messageId}.data_4`, superSunder.data_4)}
											<span>{udata.organization.name}</span> <span>{pdata.newMunicipal}</span>
											{getMessage(`${messageId}.data_5`, superSunder.data_5)}
											<span>{pdata.oldMunicipal}</span>
											{getMessage(`${messageId}.data_6`, superSunder.data_6)}
											<span>{pdata.kittaNo}</span>
											{getMessage(`${messageId}.data_7`, superSunder.data_7)}
											<span>{pdata.landArea}</span> {pdata.landAreaType}
											{getMessage(`${messageId}.data_8`, superSunder.data_8)}
											<DashedLangDateField
												name="peshGarneDate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.peshGarneDate}
												error={errors.peshGarneDate}
												className="dashedForm-control"
											/>
											{getMessage(`${messageId}.data_9`, superSunder.data_9)}
											<DashedLangInput
												name="prabhidikShree"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.prabhidikShree}
												className="dashedForm-control"
											/>
											{getMessage(`${messageId}.data_10`, superSunder.data_10)}
										</p>
										<PillarGaraTable />
										<br />
										<div className="inline field">
											<DashedAreaUnitInput name="otherArea" unitName="otherAreaUnit" label={superSunder.footer} />
										</div>
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SuperStructureNoteOrderSundarHaraicha = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.SuperStructureNoteOrder,
				objName: 'superStructNoteOrder',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinth',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param7: ['getElementsByClassName', 'ui checkbox', 'label'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		render={(props) => <SuperStructureNoteOrderSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNoteOrderSundarHaraicha;
