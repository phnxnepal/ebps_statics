import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import * as Yup from 'yup';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { SuperStructureBuildData } from '../../../../utils/data/SuperStructureBuildData';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { showToast, getUserRole } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangDateField } from '../../../shared/DateField';
import api from '../../../../utils/api';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { UserType } from '../../../../utils/userTypeUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { ImageDisplayInline } from '../../../shared/file/FileView';

const Sdata = SuperStructureBuildData.structureDesign;
const sundarData = SuperStructureBuildData.sundarHaraicha;
const SuperStructureSchema = Yup.object().shape(
	Object.assign({
		tallaThapRequestDate: validateNullableNepaliDate,
		noteOrderDate: validateNullableNepaliDate,
	})
);

/**
 * @see SuperStructureBuildView
 */
class TallaThapIjajatRequestComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { userData, prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		// const user_info = this.props.userData;
		const noteOrder = getJsonData(otherData.noteOrder);
		let serInfo = {
			designerName: '',
			number: '',
			designerAddress: '',
			stamp: '',
		};
		if (getUserRole() === UserType.DESIGNER) {
			serInfo.designerName = userData.info.userName;
			serInfo.number = userData.info.mobile;
			serInfo.designerAddress = userData.info.address;
			serInfo.stamp = userData.info.stamp;
		}
		let erInfo = {
			erName: '',
			consultName: '',
			erStamp: '',
		};
		if (getUserRole() === UserType.ENGINEER) {
			erInfo.erName = userData.userName;
			erInfo.consultName = userData.info.consultancyName;
			erInfo.erStamp = userData.info.stamp;
		}
		// console.log('note order', noteOrder, otherData);
		initVal = prepareMultiInitialValues(
			{
				obj: noteOrder,
				reqFields: ['noteOrderDate'],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{
				obj: erInfo,
				reqFields: [],
			},
			{
				obj: { tallaThapRequestDate: getCurrentDate(true) },
				reqFields: [],
			},
			{ obj: json_data, reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		return (
			<Formik
				initialValues={initVal}
				validationSchema={SuperStructureSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;
					try {
						await this.props.postAction(`${api.tallaThapIjajatRequest}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(
							// 	getNextUrl(this.props.parentProps.location.pathname)
							// );
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleChange, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							<div className="flex-item-space-between">
								<LetterSalutation
									lines={[letterSalutation.shreemanChief, userData.organization.name, userData.organization.address]}
								/>
								<div className="self-top">
									<DashedLangDateField
										name="tallaThapRequestDate"
										handleChange={handleChange}
										value={values.tallaThapRequestDate}
										error={errors.tallaThapRequestDate}
										setFieldValue={setFieldValue}
										inline={true}
										label={Sdata.date}
									/>
								</div>
							</div>
							<br />
							<div className="section-header">
								<h3>
									{sundarData.subjectPrefix} <span className="underline">{sundarData.subject}</span>
								</h3>
							</div>
							<br />
							<div className="no-margin-field">
								<span style={{ textIndent: '2em' }}>
									{sundarData.body1} {userData.organization.name} {Sdata.wardNo} {permitData.newWardNo} {sundarData.body2}{' '}
								</span>
								<DashedLangDateField
									name="noteOrderDate"
									setFieldValue={setFieldValue}
									value={values.noteOrderDate}
									error={errors.noteOrderDate}
									inline={true}
									compact={true}
								/>
								{sundarData.body3} {userData.organization.name} {sundarData.body4}
							</div>

							<br />
							<div className="flex-item-flex-end no-margin-field">
								<div>
									<SectionHeader>
										<p className="underline left-align">{Sdata.writer}</p>
									</SectionHeader>
									<div>
										{sundarData.name} {permitData.nibedakName}
									</div>

									<div>
										{sundarData.wares} <DashedLangInput name="waresName" value={values.waresName} setFieldValue={setFieldValue} />
									</div>
									<div>
										{sundarData.address} {permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
									</div>
									<div>
										{sundarData.dastakhat}
										<span className="ui input dashedForm-control"></span>
									</div>
								</div>
							</div>
							<br />
							<div>
								<p style={{ textDecoration: 'underline' }}>{sundarData.footer.footerTitle} :-</p>
								<Grid columns={2} className="flex-item-flex-start">
									<Grid.Row>
										<Grid.Column>
											{sundarData.footer.name}
											<DashedLangInput
												name="designerName"
												setFieldValue={setFieldValue}
												value={values.designerName}
												error={errors.designerName}
											/>
										</Grid.Column>
										<Grid.Column>
											{sundarData.footer.consultancyName}
											<DashedLangInput
												name="consultName"
												setFieldValue={setFieldValue}
												value={values.consultName}
												error={errors.consultName}
											/>
										</Grid.Column>
									</Grid.Row>
									<Grid.Row>
										<Grid.Column>
											{sundarData.footer.phone}
											<DashedLangInput
												name="number"
												setFieldValue={setFieldValue}
												value={values.number}
												error={errors.number}
											/>
										</Grid.Column>
										<Grid.Column>
											{sundarData.footer.engineerName}
											<DashedLangInput
												name="erAnother"
												setFieldValue={setFieldValue}
												value={values.erAnother}
												error={errors.erAnother}
											/>
										</Grid.Column>
									</Grid.Row>
									<Grid.Row>
										<Grid.Column>
											{sundarData.address}
											<DashedLangInput
												name="designerAddress"
												setFieldValue={setFieldValue}
												value={values.designerAddress}
												error={errors.desgnerAddress}
											/>
										</Grid.Column>
										<Grid.Column>
											{sundarData.footer.engineerName}
											<DashedLangInput
												name="erName"
												setFieldValue={setFieldValue}
												value={values.erName}
												error={errors.erName}
											/>
										</Grid.Column>
									</Grid.Row>
									<Grid.Row>
										<Grid.Column>
											<ImageDisplayInline
												label={sundarData.footer.signatureStamp}
												src={values.stamp}
												alt="signature/stamp"
											// height={80}
											/>
										</Grid.Column>
										<Grid.Column>
											{sundarData.footer.signatureStamp}
											<DashedLangInput
												name="erStamp"
												setFieldValue={setFieldValue}
												value={values.erStamp}
												error={errors.erStamp}
											/>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							</div>
						</div>
						<br />
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}

const TallaThapIjajatRequestView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.tallaThapIjajatRequest,
				objName: 'tallaThapIjajatRequest',
				form: true,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteOrder',
				form: false,
			},
		]}
		prepareData={data => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param4: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			aram6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		render={props => <TallaThapIjajatRequestComponent {...props} parentProps={parentProps} />}
	/>
);

export default TallaThapIjajatRequestView;
