import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import * as Yup from 'yup';

import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { KaryaSampana } from '../../../../utils/data/certificateinstructiondata';
import { puranoGharSunderHaraicha } from '../../../../utils/data/certificateinstructiondata';
import { SectionHeader } from '../../../uiComponents/Headers';

import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { showToast } from '../../../../utils/functionUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { validateNepaliDate } from '../../../../utils/validationUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { PrintParams } from '../../../../utils/printUtils';
import api from '../../../../utils/api';
import { PatraSankhya } from '../formComponents/PatraSankhyaAndDate';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

const ksamp = KaryaSampana.KaryaSampana_data;
const haraichaData = puranoGharSunderHaraicha.data;
const prabhidikOpt = [
	{ key: 'a1', value: 'कन्सल्टेन्ट', text: 'कन्सल्टेन्ट' },
	{ key: 'a2', value: 'को ', text: 'को' },
];
const PuranoGharSchema = Yup.object().shape(
	Object.assign({
		certificateInstDate: validateNepaliDate,
	})
);
class PuranoGharNirmanTippaniSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);

		let initVal = {};
		const { prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		initVal = prepareMultiInitialValues(
			{
				obj: {
					consultant: 'कन्सल्टेन्ट',
				},
				reqFields: ['consultant'],
			},
			{
				obj: mapTech,
				reqFields: ['consultancyName', 'userName'],
			},
			// {
			// 	obj: serInfo,
			// 	reqFields: [],
			// },
			// { obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.certificateInstDate)) {
			initVal.certificateInstDate = getCurrentDate(true);
		}
		this.state = {
			initVal
		};

	}
	render() {
		const { initVal } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const pdata = permitData;
		const udata = userData;
		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }

		// let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);
		return (
			<div style={{ textAlign: 'justify' }}>
				<Formik
					initialValues={initVal}
					validationSchema={PuranoGharSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.puranoGharTippani}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//     getNextUrl(
								//         this.props.parentProps.location.pathname
								//     )
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={userData} />
									<PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
									<SectionHeader>
										<h3 className="normal underline">{haraichaData.title.title_1}</h3>
										<h3 className="normal underline end-section">{haraichaData.title.title_2}</h3>
									</SectionHeader>
									<FlexSingleRight>
										<DashedLangDateField
											name="certificateInstDate"
											setFieldValue={setFieldValue}
											error={errors.certificateInstDate}
											value={values.certificateInstDate}
											inline={true}
											compact={true}
											label={haraichaData.title.date}
										/>
									</FlexSingleRight>
									<div>
										<span>{udata.organization.name}</span>
										<span>{ksamp.data_1.data_1_2}</span>
										<span>{pdata.nibedakTol}</span>
										<span>{ksamp.data_1.data_1_3}</span>
										<span>{pdata.nibedakName}</span>
										<span>{ksamp.data_1.data_1_4}</span>
										<span>{udata.organization.name}</span>
										<span>{ksamp.data_1.data_1_2}</span>
										<span>{pdata.oldWardNo}</span>
										<span>{ksamp.data_1.data_1_7}</span>
										<span>{pdata.applicantName}</span>
										<span>{haraichaData.data_1}</span>
										<span>{udata.organization.name}</span>
										<span>{ksamp.data_1.data_1_2}</span>
										<span>{pdata.oldWardNo}</span>
										<span>{haraichaData.data_2}</span>
										<span>{pdata.kittaNo}</span>
										<span>{ksamp.data_1.data_1_13}</span>
										<span>{pdata.landArea}{pdata.landAreaType}</span>
										<span>{ksamp.data_1.data_1_14}</span>
										<DashedLangInput
											name="maa"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.maa}
											className="dashedForm-control"
										/>
										<span>{ksamp.data_1.data_1_15}</span>
										<DashedLangDateField
											name="certificateInstDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.certificateInstDate}
											error={errors.certificateInstDate}
											className="dashedForm-control"
										/>
										<span>{ksamp.data_1.data_1_16}</span>
										<DashedLangInput
											name="pramanPatra"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.pramanPatra}
											className="dashedForm-control"
										/>
										<span>{ksamp.data_1.data_1_17}</span>
										{values.consultant === 'कन्सल्टेन्ट' && values.consultancyName ? `${values.consultancyName}` : `${values.userName}`}{' '}
										<Select
											name="consultant"
											options={prabhidikOpt}
											value={values.consultant}
											onChange={(e, { value }) => setFieldValue('consultant', value)}
										/>
										<span>{haraichaData.data_3}</span>
									</div>
									<br />
									<br />
									<div className="signature-div flex-item-space-between">
										{haraichaData.designations.map((designation, index) => (
											<div key={index} >
												<p className="neighbours-para">{designation}</p>
												<br />
												<br />
												<p>
													{ksamp.data_2.data_2_4}
													<span className="ui input signature-placeholder" />
												</p>
											</div>
										))}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const PuranoGharNirmanTippaniSundarHaraicha = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.puranoGharTippani,
				objName: 'certificateInst',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			}
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			//param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param8: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
		}}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <PuranoGharNirmanTippaniSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default PuranoGharNirmanTippaniSundarHaraicha;
