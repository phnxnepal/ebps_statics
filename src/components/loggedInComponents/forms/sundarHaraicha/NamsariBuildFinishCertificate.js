import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import * as Yup from 'yup';

import { BuildingBuildCertificateData, mappingDirection } from '../../../../utils/data/BuildingBuildCertificateData';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNepaliDate } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadPhoto } from '../../../shared/LetterHead';
import { DashedLangInput, DashedNormalInput } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from '../../../shared/DateField';
import { translateDate } from '../../../../utils/langUtils';
import { DashedMultiUnitLengthInput, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import EbpsTextareaField from '../../../shared/MyTextArea';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { CertificateSubHeadingWithoutPhoto } from '../certificateComponents/CertificateComponents';
import api from '../../../../utils/api';
import { FooterSignature } from './../formComponents/FooterSignature';
import { PrintParams } from '../../../../utils/printUtils';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { FinishCertificateBlockFloors } from './../certificateComponents/CertificateFloorBlockInputs';

const Detail = BuildingBuildCertificateData.details;
const formData = BuildingBuildCertificateData.formdata;
const finishDetail = BuildingBuildCertificateData.finishDetail;

const field_9 = [
	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
];

const BuildingBuildSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNepaliDate,
	})
);

class NamsariBuildFinishCertificateComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData } = this.props;
		const mapTech = getJsonData(otherData.mapTech);
		const prabhidik = getJsonData(otherData.prabhidik);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const certificateNote = getJsonData(otherData.certificateNote);

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: [
					'floor',
					// 'surrounding',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'photo',
					'purposeOfConstruction',
					'purposeOfConstructionOther',
				],
			},
			{
				obj: {
					// ...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					// ...floorArray.getInitialValue('area', 'buildingArea', floorArray.getBottomFloor()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
				},
				reqFields: [],
			},
			{
				obj: {
					Bdate: getCurrentDate(true),
					// buildingArea: floorArray.getSumOfAreas(),
					namedUnit: 'METRE',
					floorUnit: 'METRE',
					highTensionLineUnit: 'METRE',
					publicPropertyUnit: 'METRE',
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'buildingHeight'],
			},
			{
				obj: { namsariName: certificateNote.shreeName || certificateNote.sasthaYaExtraName },
				reqFields: [],
			},
			{
				obj: prabhidik,
				reqFields: [
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'namedBorder',
					'namedUnit',
					'elecVolt',
					'publicProperty',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = {
			initialValues,
			floorArray,
			formattedFloors,
			blocks: floorArray.getBlocks(),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const user_info = this.props.userData;
		const { initialValues, floorArray, formattedFloors } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={BuildingBuildSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(`${api.buildingBuild}${this.props.permitData.nameTransaferId}`, values);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadPhoto userInfo={user_info} photo={values.photo} compact={true} />
									<CertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										titles={finishDetail.title}
										textSize={TextSize.MEDIUM}
									/>
									<br />
									<div>
										{userData.organization.name} {Detail.wda}
										{permitData.newWardNo} {Detail.data1}
										{values.namsariName}{Detail.data2}
										{translateDate(permitData.applicantDateBS)} {Detail.data3}
										{permitData.applicationNo}
										{finishDetail.data_1}
										{permitData.nibedakName}
										{finishDetail.data_2}
										{permitData.oldMunicipal}
										{finishDetail.data_3}
										{userData.organization.name}
										{finishDetail.wadaNo}
										{permitData.newWardNo}
										{finishDetail.data_4}
										{permitData.sadak}
										{finishDetail.data_5}
										{permitData.buildingJoinRoad}
										{finishDetail.data_6}
										{permitData.kittaNo}
										{finishDetail.data_7}
										{permitData.landArea} {permitData.landAreaType}
										{finishDetail.data_8}
										<DashedLangInput name="jagga" setFieldValue={setFieldValue} value={values.jagga} />
										{finishDetail.data_9}
										<DashedLangDateField
											name="Bdate"
											handleChange={handleChange}
											value={values.Bdate}
											error={errors.Bdate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
										{Detail.data11}
										<DashedLangInput name="maa" setFieldValue={setFieldValue} value={values.maa} />
										{Detail.data12}
										{values.namsariName}
										{Detail.data13}
									</div>
									<div>
										{formData.fdata1}
										{values.namsariName}, {formData.address}
										{permitData.newMunicipal} {permitData.nibedakTol} {permitData.nibedakSadak}
									</div>

									<div>
										{formData.fdata2}{' '}
										<div>
											{values.surrounding &&
												// values.surrounding &&
												values.surrounding.map((index, i) => (
													<div>
														{mappingDirection.find((fl) => fl.direction === index.side).value}{' '}
														<DashedNormalInput
															name={`surrounding.${i}.kittaNo`}
															value={getIn(values, `surrounding.${i}.kittaNo`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.kittaNo`)}
														/>
														{formData.sandhiyar}
														<DashedNormalInput
															name={`surrounding.${i}.sandhiyar`}
															value={getIn(values, `surrounding.${i}.sandhiyar`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.sandhiyar`)}
														/>
													</div>
												))}
										</div>
									</div>

									<div>
										{formData.fdata3}
										<DashedLangInput name="buildType" setFieldValue={setFieldValue} value={values.buildType} />
										{formData.fdata4}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
										{formData.fdata5}
										<DashedLangInput name="roofLen" setFieldValue={setFieldValue} value={values.roofLen} />
										{formData.fdata6}
										<DashedLangInput name="prayojan" setFieldValue={setFieldValue} value={values.prayojan} />
									</div>
									<div>
										{formData.fdata6_1}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											value={values.purposeOfConstruction}
										/>
										<br />
										{formData.fdata7}
										<DashedLangInput name="roomNo" setFieldValue={setFieldValue} value={values.roomNo} />
										{formData.fdata8}
										<DashedLangInput name="windowNo" setFieldValue={setFieldValue} value={values.windowNo} />
										{formData.fdata9}
										<DashedLangInput name="doorNo" setFieldValue={setFieldValue} value={values.doorNo} />
										{formData.fdata10}
										<DashedLangInput name="satarNo" setFieldValue={setFieldValue} value={values.satarNo} />
									</div>

									<DashedMultiUnitLengthInput
										name="namedMapdanda"
										unitName="namedUnit"
										relatedFields={['namedBorder', 'namedAdhikar']}
										label={formData.fdata13}
									/>

									<DashedMultiUnitLengthInput
										name="namedAdhikar"
										unitName="namedUnit"
										relatedFields={['namedBorder', 'namedMapdanda']}
										label={formData.fdata14}
									/>
									<DashedMultiUnitLengthInput
										name="namedBorder"
										unitName="namedUnit"
										relatedFields={['namedAdhikar', 'namedMapdanda']}
										label={formData.fdata15}
									/>

									<div>
										{formData.seven}
										<div className="div-indent">
											<FinishCertificateBlockFloors
												floorArray={floorArray}
												formattedFloors={formattedFloors}
												showAreaColumn={false}
											/>
										</div>
										{/* <CertificateFloorBorderlessTable floorArray={floorArray} /> */}
									</div>
									<div>
										{formData.fdata24}
										<DashedLangInput name="coverageDetails" setFieldValue={setFieldValue} value={values.coverageDetails} />
									</div>

									<DashedUnitInput name="isHighTensionLineDistance" label={formData.fdata25} unitName="highTensionLineUnit" />

									{formData.volt}
									<DashedLangInput name="elecVolt" setFieldValue={setFieldValue} value={values.elecVolt} />
									<br />

									<DashedUnitInput name="publicProperty.1.value" label={formData.fdata26} unitName="publicPropertyUnit" />

									{formData.fdata27}
									<DashedLangInput name="publicPropertyName" setFieldValue={setFieldValue} value={values.publicPropertyName} />

									<div>
										{formData.fdata28}
										<Select
											options={field_9}
											name="nikash"
											placeholder="छ (भएको)"
											onChange={(e, { value }) => setFieldValue('nikash', value)}
											value={values['nikash']}
											error={errors.nikash}
										/>
									</div>
									<div>
										{formData.fdata29}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>
									<br />
									<FooterSignature designations={finishDetail.designation} />
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const NamsariBuildFinishCertificate = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingBuild, objName: 'buildingBuild', form: true },
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
			{
				api: api.certificateNote,
				objName: 'certificateNote',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <NamsariBuildFinishCertificateComponent {...props} parentProps={parentProps} />}
	/>
);
export default NamsariBuildFinishCertificate;
