import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { superstructureconstructionview } from '../../../../utils/data/mockLangFile';
import { getJsonData, FloorArray, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { TallaThapIjajatSchema } from '../../formValidationSchemas/superStructureConstructionValidation';
import api from '../../../../utils/api';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { TallaThapIjajatBody } from '../ijajatPatraComponents/SuperStructureConsComponents';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { date } from '../../../../utils/data/formCommonData';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { tallaThapIjajat } from '../../../../utils/data/tallaThapData';
import { getUserTypeValueNepali } from '../../../../utils/functionUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { getApproveByObject } from '../../../../utils/formUtils';

const sscv_data = superstructureconstructionview.superstructureconstructionview_data;
const data = tallaThapIjajat;
const footerData = tallaThapIjajat.footer;

// const permitLang = buildingPermitApplicationForm.permitApplicationFormView;

// const mapTechLang = mapTechnical.mapTechnicalDescription;

// const buildPermitLang = buildingPermitApplicationForm.permitApplicationFormView;

// const areaUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];

// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];

// export const landAreaTypeOptions = [
// 	{ key: 1, value: 'रोपनी–आना–पैसा–दाम', text: 'रोपनी–आना–पैसा–दाम' },
// 	{ key: 2, value: 'बिघा–कठ्ठा–धुर', text: 'बिघा–कठ्ठा–धुर' },
// 	{ key: 3, value: 'वर्ग फिट', text: 'वर्ग फिट' },
// 	{ key: 4, value: 'वर्ग मिटर ', text: 'वर्ग मिटर' },
// ];

class TallaThapIjajatPatraComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData, otherData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		const jsonData = getJsonData(prevData);
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sengData = getApproveBy(0);
		const engData = getApproveBy(1);

		const mapTech = getJsonData(otherData.mapTech);

		const rajaswo = getJsonData(otherData.rajaswo);

		// const suikritTalla = this.props.otherData.anusuchiGha.tallaThap;

		// const sadakAdhikar = () => _.max(otherJsonData.sadakAdhikarKshytra);

		// const setbackMaptech = () => {
		// 	if (otherJsonData.setBack) {
		// 		return Math.max.apply(
		// 			Math,
		// 			otherJsonData.setBack.map(value => value.distanceFromRoadCenter)
		// 		);
		// 	}
		// 	return null;
		// };

		const desApprovJsonData = otherData.designApproval;

		const anuSucKaJsonData = otherData.anukaMaster;
		const suikritTalla = otherData.anusuchiGha.thapTalla;
		// console.log('talla---', suikritTalla);

		const allowance = getJsonData(otherData.allowance);
		const noteorder = getJsonData(otherData.noteorder);
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);

		const floorArray = new FloorArray(permitData.floor);
		const floorMax = floorArray.getTopFloor().nepaliCount;

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					// plinthDetailsUnit: 'METRE',
					// buildCoverAreaUnit: 'METRE',
					// coverageDetailsUnit: 'METRE',
					buildingAreaUnit: DEFAULT_UNIT_AREA,
					coverageAreaUnit: DEFAULT_UNIT_AREA,
					houseLengthUnit: DEFAULT_UNIT_LENGTH,
					houseBreadthUnit: DEFAULT_UNIT_LENGTH,
					houseHeightUnit: DEFAULT_UNIT_LENGTH,
					constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					plinthDetailsUnit: DEFAULT_UNIT_LENGTH,
					buildCoverAreaUnit: DEFAULT_UNIT_AREA,
					coverageDetailsUnit: DEFAULT_UNIT_AREA,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					tallaThapDate: getCurrentDate(true),
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['constructionType', 'purposeOfConstruction', 'landArea', 'landAreaType'] },
			{
				obj: mapTech,
				reqFields: ['approveDate', 'plinthDetails', 'plinthDetailsUnit', 'roof', 'coverageDetails', 'buildingHeight', 'allowableHeight'],
			},
			{ obj: rajaswo, reqFields: ['allowableHeight', 'constructionHeight', 'constructionHeightUnit'] },
			// {
			// 	obj: {
			// 		setbackMetre: setbackMaptech(),
			// 		sadakMetre: sadakAdhikar()

			// 	}, reqFields: []
			// }
			{
				obj: allowance,
				reqFields: ['allowanceDate', 'gharNo', 'publicPropertyDistance', 'publicPropertyUnit', 'sadakAdhikarUnit', 'requiredDistance'],
			},
			{
				obj: noteorder,
				reqFields: ['noteOrderDate'],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sengSignature: sengData.signature,
					engSignature: engData.signature,
				},
				reqFields: [],
			}
		);

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);
		this.state = {
			initialValues,
			floorMax,
			suikritTalla,
			buildingClass,
		};
	}

	render() {
		const { initialValues, buildingClass, suikritTalla, floorMax } = this.state;
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;

		const user_info = this.props.userData;
		return (
			// <div className='superStruConsView'>

			<Formik
				initialValues={initialValues}
				validationSchema={TallaThapIjajatSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						await this.props.postAction(`${api.superStructureConstruction}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error in Structure Construction Save', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							<div>
								<LetterHeadFlex userInfo={user_info} />
								<div className="flex-item-space-between">
									<LetterSalutation lines={[data.shree, `${userData.organization.name}, ${userData.organization.address}`]} />
									<div>
										{/* {Ndata.Sangyardate}: */}
										<DashedLangDateField
											name="tallaThapDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.tallaThapDate}
											label={date.date}
											error={errors.tallaThapDate}
											className="dashedForm-control"
										/>
									</div>
								</div>
								<br />
								<div className="section-header">
									<h3>
										<span>{data.subject} </span>
										<span className="underline">{data.subjectContent}</span>
									</h3>
								</div>

								<TallaThapIjajatBody
									floorMax={floorMax}
									suikritTalla={suikritTalla}
									permitData={permitData}
									buildingClass={buildingClass}
									userData={userData}
									values={values}
									setFieldValue={setFieldValue}
									errors={errors}
								/>

								<FooterSignatureMultiline
									designations={[
										[footerData.basti],
										[getUserTypeValueNepali(UserType.SUB_ENGINEER)],
										[`${footerData.pramukh}/${getUserTypeValueNepali(UserType.ENGINEER)}`, footerData.basti],
									]}
									signatureImages={(useSignatureImage, ['', values.sengSignature, values.engSignature])}
								/>
								<br />
								<p>{footerData.note}</p>
								<br />
								<div>
									{sscv_data.footer.footer_5}
									<span className="ui input dashedForm-control " />
								</div>
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}
const TallaThapIjajatPatra = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.tallaThapIjajatPatra,
				objName: 'tallaThapIjajatPatra',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.anusuchiGha,
				objName: 'anusuchiGha',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthTechApp',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteorder',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param8: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			//param9: ['getElementsByClassName', 'brackets', 'value'],
		}}
		render={(props) => <TallaThapIjajatPatraComponent {...props} parentProps={parentProps} />}
	/>
);

export default TallaThapIjajatPatra;
