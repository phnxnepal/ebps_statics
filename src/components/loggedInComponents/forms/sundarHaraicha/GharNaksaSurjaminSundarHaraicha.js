import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import * as Yup from 'yup';

import { GharSurjaminData } from '../../../../utils/data/GharSurjaminData';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues, surroundingMappingFlat } from '../../../../utils/dataUtils';
import api from '../../../../utils/api';
import { showToast } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedMultiUnitLengthInput } from '../../../shared/EbpsUnitLabelValue';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { GenericApprovalFileView } from '../../../shared/file/GenericApprovalFileView';
import { FormUrlFull } from '../../../../utils/enums/url';
import { WARD_MASTER } from '../../../../utils/constants';
import { FooterSignature } from '../formComponents/FooterSignature';
import { footerSignature, footerInputSignature, longFormDate } from '../../../../utils/data/genericFormData';
import { GenericLongDate } from '../formComponents/LongFormDate';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { DashedSelect } from '../../../shared/Select';
import { LocalAPI } from '../../../../utils/urlUtils';

// const options = [
// 	{ key: 'a', text: 'फिट', value: 'FEET' },
// 	{ key: 'b', text: 'मिटर', value: 'METRE' },
// ];

const data = GharSurjaminData.sundarHaraicha;
// const GSData = GharSurjaminData.structureDesign;
const schema = Yup.object().shape({
	// uploadFile: validateFile,
	// gharnakshaSurjaminMuchulka: Yup.string().required('Required')
});

class GharNakshaSurjaminSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);

		const { permitData, prevData, otherData, localDataObject, DEFAULT_UNIT_LENGTH } = this.props;
		const json_data = getJsonData(prevData);
		const surjaminMuchulka = getJsonData(otherData.surjaminMuchulka);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const wardOption = localDataObject.wardMaster.map(row => ({ key: row.id, value: row.name, text: row.name }));

		// initVal.munWard = permitData.newWardNo;
		// initVal.marga = permitData.buildingJoinRoad;
		// initVal.eastwestMeter = 'METRE';
		// initVal.eastwestMeter1 = 'METRE';
		// initVal.southNorthMeter = 'METRE';
		// initVal.southNorthMeter1 = 'METRE';
		// initVal.height1 = 'METRE';

		// const initVal1 = { ...initVal, ...json_data };
		// console.log('initval', initVal);

		const initialValues = prepareMultiInitialValues(
			{ obj: permitData, reqFields: ['newWardNo', 'buildingJoinRoad'] },
			{ obj: surjaminMuchulka, reqFields: ['aminName', 'prashashanName'] },
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: { buildingUnit: DEFAULT_UNIT_LENGTH }, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);

		this.state = { initialValues, wardOption };
	}
	render() {
		// let initVal = {};
		// if (permitData && userData && prevData) {
		const { initialValues, wardOption } = this.state;
		const { permitData, prevData, userData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, errors: reduxErrors } = this.props;
		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						try {
							await this.props.postAction(`${api.gharnakshaSurjaminMuchulka}`, values, true);

							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div className="section-header">
									<h3>{data.title}</h3>
								</div>

								<div style={{ textAlign: 'justify' }}>
									{data.line1} <span>{userData.organization.name}</span>
									{data.line2}
									<DashedLangInput
										name="aminName"
										setFieldValue={setFieldValue}
										value={values.aminName}
										error={errors.aminName}
									/>{' '}
									{data.shree}
									<DashedLangInput
										name="prashashanName"
										setFieldValue={setFieldValue}
										value={values.prashashanName}
										error={errors.prashashanName}
									/>
									{data.shree}
									<DashedLangInput
										name="thirdName"
										setFieldValue={setFieldValue}
										value={values.thirdName}
										error={errors.thirdName}
									/>
									{data.line4} <span>{userData.organization.name}</span> {data.wardNo}
									{/* <DashedLangInput
										name="newWardNo"
										setFieldValue={setFieldValue}
										value={values.newWardNo}
										error={errors.newWardNo}
									/> */}
									<DashedSelect name="newWardNo" options={wardOption} />
									{' '}
									{data.sadakSlash}{' '}
									<DashedLangInput
										name="buildingJoinRoad"
										setFieldValue={setFieldValue}
										value={values.buildingJoinRoad}
										error={errors.buildingJoinRoad}
									/>
									{data.line5} <span>{userData.organization.name}</span>,{data.wardNo} <span>{permitData.nibedakTol}</span>{' '}
									{data.sadak} {permitData.nibedakSadak} {data.line6} <span>{permitData.nibedakName}</span> {data.line6_1}{' '}
									{data.line7}
								</div>
								<div>
									{data.line8} {userData.organization.name} {data.line8_1}
								</div>
								<br />
								<div>
									<div className="section-header">
										<p className="left-align">{data.jaggaBibaran}</p>
									</div>
									<div>
										{userData.organization.name} {data.wardNo} {permitData.newWardNo} {data.sabik}: {permitData.oldMunicipal}{' '}
										{data.sadakNam} {permitData.sadak}
									</div>
									<div>
										{data.jaggakoKitta} {permitData.kittaNo} {data.landArea} {permitData.landArea} {permitData.landAreaType}
									</div>
									<div className="section-header">
										<p className="left-align">{data.charKilla}</p>
									</div>
									<div>
										{surroundingMappingFlat.map(surr => {
											let dataSurr = {};
											let index = 0;
											if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
												dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
												index = values.surrounding.indexOf(dataSurr);
											}
											return (
												<>
													{surr.value}
													{':'}
													<DashedLangInput
														name={`surrounding.${index}.sandhiyar`}
														setFieldValue={setFieldValue}
														value={getIn(values, `surrounding.${index}.sandhiyar`)}
														error={getIn(errors, `surrounding.${index}.sandhiyar`)}
													/>{' '}
												</>
											);
										})}
									</div>
								</div>
								<div>
									<div className="section-header">
										<p className="left-align">{data.nirmanBibaran}</p>
									</div>
									<div>{data.bhawanCompound}</div>
									<div>
										{data.naap}{' '}
										<DashedMultiUnitLengthInput
											name="buildingLength"
											unitName="buildingUnit"
											relatedFields={['buildingWidth', 'buildingHeight']}
										/>{' '}
										{data.width}{' '}
										<DashedMultiUnitLengthInput
											name="buildingWidth"
											unitName="buildingUnit"
											relatedFields={['buildingLength', 'buildingHeight']}
										/>{' '}
										{data.height}{' '}
										<DashedMultiUnitLengthInput
											name="buildingHeight"
											unitName="buildingUnit"
											relatedFields={['buildingLength', 'buildingWidth']}
										/>
									</div>
								</div>

								<EbpsTextArea name="uttar" onChange={handleChange} setFieldValue={setFieldValue} value={values.uttar} />

								{/* Section 2 ----  */}
								<br />
								<div style={{ pageBreakBefore: 'always' }} className="section-header">
									<p className="left-align underline">{data.charKillaAge}</p>
								</div>
								<div>
									{surroundingMappingFlat.map((surr, id) => {
										let dataSurr = {};
										let index = 0;
										if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
											dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
											index = values.surrounding.indexOf(dataSurr);
										}
										return (
											<div key={id}>
												{surr.value} {data.age}{' '}
												<DashedLangInput
													compact={true}
													name={`surrounding.${index}.age`}
													setFieldValue={setFieldValue}
													value={getIn(values, `surrounding.${index}.age`)}
													error={getIn(errors, `surrounding.${index}.age`)}
												/>{' '}
												{data.koShree}{' '}
												<DashedLangInput
													name={`surrounding.${index}.sandhiyar`}
													setFieldValue={setFieldValue}
													value={getIn(values, `surrounding.${index}.sandhiyar`)}
													error={getIn(errors, `surrounding.${index}.sandhiyar`)}
												/>{' '}
											</div>
										);
									})}
								</div>
								<br />
								<div className="section-header">
									<p className="left-align underline">{data.chimeki}</p>
								</div>
								<div>
									{new Array(5).fill('').map((_, index) => (
										<div key={index}>
											{userData.organization.name} {data.wardNo}{' '}
											<DashedLangInput
												compact={true}
												name={`neighbour.${index}.ward`}
												setFieldValue={setFieldValue}
												value={getIn(values, `neighbour.${index}.ward`)}
												error={getIn(errors, `neighbour.${index}.ward`)}
											/>{' '}
											{data.basneBarsa}{' '}
											<DashedLangInput
												compact={true}
												name={`neighbour.${index}.age`}
												setFieldValue={setFieldValue}
												value={getIn(values, `neighbour.${index}.age`)}
												error={getIn(errors, `neighbour.${index}.age`)}
											/>{' '}
											{data.koShree}{' '}
											<DashedLangInput
												name={`neighbour.${index}.name`}
												setFieldValue={setFieldValue}
												value={getIn(values, `neighbour.${index}.name`)}
												error={getIn(errors, `neighbour.${index}.name`)}
											/>{' '}
										</div>
									))}
								</div>
								<div>{data.rohabar}</div>
								<div>
									{data.jaggaDhaniWares}
									<DashedLangInput
										name="jaggaDhaniName"
										setFieldValue={setFieldValue}
										value={getIn(values, 'jaggaDhaniName')}
										error={getIn(errors, 'jaggaDhaniName')}
									/>{' '}
								</div>
								<div>{data.subFooter}</div>
								<br />
								<FooterSignature designations={[footerSignature.wadaAdhaksya, footerSignature.wadaSachib]} />
								<br />

								<div className="section-header">
									<p className="left-align">{footerInputSignature.talmelGarne}</p>
								</div>
								<div>
									{footerInputSignature.name}{' '}
									<DashedLangInput
										name="approveName"
										setFieldValue={setFieldValue}
										value={getIn(values, 'approveName')}
										error={getIn(errors, 'approveName')}
									/>{' '}
								</div>
								<div>
									{footerInputSignature.pad}{' '}
									<DashedLangInput
										name="approveDesignation"
										setFieldValue={setFieldValue}
										value={getIn(values, 'approveDesignation')}
										error={getIn(errors, 'approveDesignation')}
									/>{' '}
								</div>
								<div>
									{footerInputSignature.dashtakhat}: <span className="ui input dashedForm-control" />
								</div>
								<br />

								<GenericLongDate
									datas={[longFormDate.talmelMiti, longFormDate.year, longFormDate.month, longFormDate.date, longFormDate.roj]}
									last={longFormDate.suvam}
								/>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const GharNaksaApproveView = props => {
	return (
		<div ref={props.setRef}>
			<GenericApprovalFileView
				fileCategories={props.fileCategories}
				files={props.files}
				url={FormUrlFull.GHAR_NAKSA_SURJAMIN}
				prevData={checkError(props.prevData)}
			/>
		</div>
	);
};

const GharNakshaSurjaminSundarHaraichaView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: true,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'surjaminMuchulka',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		localData={[new LocalAPI(WARD_MASTER, 'wardMaster')]}
		prepareData={data => data}
		onBeforeGetContent={{
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={props =>
			props.hasSavePermission ? (
				<GharNakshaSurjaminSundarHaraichaComponent {...props} parentProps={parentProps} />
			) : (
					<GharNaksaApproveView {...props} parentProps={parentProps} />
				)
		}
	/>
);

export default GharNakshaSurjaminSundarHaraichaView;
