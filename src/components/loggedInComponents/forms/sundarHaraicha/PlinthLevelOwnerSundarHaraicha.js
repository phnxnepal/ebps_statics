import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import * as Yup from 'yup';
import { getJsonData, checkError, prepareMultiInitialValues, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserRole, isEmpty, showToast } from '../../../../utils/functionUtils';
import { PostActionParams } from '../../../../utils/formUtils';
import api from '../../../../utils/api';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { getMessage } from '../../../shared/getMessage';
import { validateNullableNumber } from '../../../../utils/validationUtils';
import { ImageInlineNoLabel } from '../../../shared/file/FileView';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import { UserOldInfo, HaalSection } from '../formComponents/PermitInfo';
import { shreeOptions } from '../../../../utils/optionUtils';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { plinthLevelOwnerData } from '../../../../utils/data/plinthLevelFormsData';
import { plinthOwnerData } from '../../../../utils/data/plinthLevelOwnerData';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { SansodhanCheckbox } from '../formComponents/SansodhanCheckbox';

const plor_data = plinthOwnerData.sundarData;
const userInfo = plinthLevelOwnerData.userInfo;
const signatureSection = plinthLevelOwnerData.signatureSection;

const messageId = 'PlinthLvlOwnerRepresentation.PlinthLvlOwnerRepresentation_data';

const plinthSchema = Yup.object().shape(
	Object.assign({
		nirmanVyabasayiMobile: validateNullableNumber,
		...yesNoSectionSchema,
	})
);

class PlinthLevelOwnerSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { permitData, prevData, userData, otherData, hasDesignerChanged } = this.props;
		const json_data = getJsonData(prevData);
		const anuGhaData = checkError(otherData.anuGha);
		initVal = json_data;
		// if (isStringEmpty(initVal.certificateInstDate)) {
		// 	initVal.certificateInstDate = getCurrentDate(true);
		// }

		// initVal.prabidhikName = userData.designer ? userData.designer.userName : '';
		// initVal.consultingNameStamp = userData.designer ? userData.designer.stamp : '';
		// initVal.pramadPatraNo = userData.designer ? userData.designer.municipalRegNo : '';

		let mapSideObj = {};
		if (getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR || hasDesignerChanged) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					userName: userData.info.userName,
					educationQualification: userData.info.educationQualification,
					consultancyName: userData.info.consultancyName,
					licenseNo: userData.info.licenseNo,
					municipalRegNo: userData.info.municipalRegNo,
					stamp: userData.info.stamp,
					enterDate: getCurrentDate(),
				};
			} else {
				mapSideObj = {};
			}
		} else {
			if (!isEmpty(anuGhaData)) {
				if (!isEmpty(anuGhaData.dName && anuGhaData.dDesignation && anuGhaData.dDate)) {
					mapSideObj = {
						userName: anuGhaData.dName,
						educationQualification: anuGhaData.dDesignation,
						enterDate: anuGhaData.dDate,
					};
				} else
					mapSideObj = {
						userName: anuGhaData.enterBy.userName,
						educationQualification: anuGhaData.enterBy.educationQualification,
						licenseNo: anuGhaData.enterBy.licenseNo,
						municipalRegNo: anuGhaData.enterBy.municipalRegNo,
						consultancyName: anuGhaData.enterBy.consultancyName,
						stamp: anuGhaData.enterBy.stamp,
						enterDate: anuGhaData.enterDate,
					};
			}
		}

		initVal = prepareMultiInitialValues(
			{
				obj: {
					certificateInstDate: getCurrentDate(true),
					prabidhikName: userData.designer ? userData.designer.userName : '',
					consultingNameStamp: userData.designer ? userData.designer.stamp : '',
					pramadPatraNo: userData.designer ? userData.designer.municipalRegNo : '',
					shree: shreeOptions[0].value,
					mapdandaPalanaBhayeko: 'Y',
					setBackPlanBhayeko: 'Y',
					changesMadeYes: 'Y',
					groundCoverageBhayeko: 'N',
					buildingConstFollowBhayeko: 'Y',
					pillar: 'Y',
					pillarGrid: 'Y',
					baseBeam: 'Y',
					strapBeam: 'Y',
					jointValid: 'Y',
					pillarGara: 'Y',
					garaTwoFeet: 'Y',
					cornerRod: 'Y',
					windowGaraSpace: 'Y',
					sanso: plor_data.check[0],
					engParisadDartNo: userData.designer ? userData.designer.licenseNo : '',
				},
				reqFields: [],
			},
			{
				obj: anuGhaData,
				reqFields: ['thekdarName', 'tAddress', 'tDarta', 'mistiriName', 'mDarta', 'mAddress'],
			},
			{ obj: mapSideObj, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, prevData, userData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		// if (isStringEmpty(initVal.certificateInstDate)) {
		// 	initVal.certificateInstDate = getCurrentDate(true);
		// }

		// initVal.prabidhikName = userData.designer ? userData.designer.userName : '';
		// initVal.consultingNameStamp = userData.designer ? userData.designer.stamp : '';
		// initVal.pramadPatraNo = userData.designer ? userData.designer.municipalRegNo : '';

		// const plinthData = ['thekdarName', 'tAddress', 'mistiriName', 'mAddress'];
		// const plinthNepaliNum = ['mDarta', 'tDarta']
		// const formDataSchema = plinthData.map(row => {
		//     return {
		//         [row]: validateString
		//     };
		// });

		// const numericalNepaliSchema = plinthNepaliNum.map(row => {
		//     return {
		//         [row]: validateNullableOfficialReqNumbers
		//     };
		// });

		return (
			<Formik
				initialValues={{
					...initVal,
				}}
				validationSchema={plinthSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						let url = '';
						if (values.sanso === plor_data.check[1]) {
							url = `${api.plinthLevelOwnerRepresentation}${permitData.applicantNo}/Y`;
						} else {
							url = `${api.plinthLevelOwnerRepresentation}${permitData.applicantNo}/N`;
						}

						const params = new PostActionParams({ api: url, data: values, concatAppId: false, fetchMenu: true });
						await this.props.postAction(...params.getParams());

						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						console.log('errror', err);
						showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							<div className="section-header">
								<h3 className="underline end-section">{getMessage(`${messageId}.plor_heading2`, plor_data.plor_heading2)}</h3>
							</div>
							<LetterSalutation
								lines={[letterSalutation.shreemanEngineer, `${userData.organization.name} ${letterSalutation.karyalaya}`]}
							/>
							<br />
							<div className="margin-bottom">
								<UserOldInfo permitData={permitData} userData={userData} />
								{userInfo.le} <HaalSection permitData={permitData} /> {userInfo.end}
							</div>
							<YesNoSection
								setFieldValue={setFieldValue}
								errors={errors}
								values={values}
								sansodhan={
									<SansodhanCheckbox
										label={plor_data.checklabel}
										values={values}
										handleChange={handleChange}
										options={plor_data.check}
										constructionType={permitData.constructionType}
									/>
								}
							/>
							{signatureSection.paragraph}
							<div className="flex-item-space-between">
								<div>
									<div>
										{signatureSection.sign}: <span className="ui input dashedForm-control" />
									</div>
									<div>
										{signatureSection.superibechyak}: {values.userName}
									</div>
									<div>
										{signatureSection.nepalId}: {values.licenseNo}
									</div>
									<div>
										{signatureSection.organizationId}: {values.municipalRegNo}
									</div>
									<div>
										{signatureSection.firmName}: {values.consultancyName}
									</div>
								</div>
								<div>
									<ImageInlineNoLabel src={values.stamp} alt="stamp" />
									<p style={{ textAlign: 'center' }}>{signatureSection.firmStamp}</p>
								</div>
								<div></div>
							</div>
							<br />
							<p>{signatureSection.paragraph2}</p>
							<div className="no-margin-field">
								<div>
									{signatureSection.nirmanVyabasayi}:
									<DashedLangInput
										name="nirmanVyabasayiName"
										setFieldValue={setFieldValue}
										value={values.nirmanVyabasayiName}
										error={errors.nirmanVyabasayiName}
									/>{' '}
									{signatureSection.sign}:
									<span className="ui input dashedForm-control" />
								</div>
								<div>
									{signatureSection.address}:
									<DashedLangInput
										name="nirmanVyabasayiAddress"
										setFieldValue={setFieldValue}
										value={values.nirmanVyabasayiAddress}
										error={errors.nirmanVyabasayiAddress}
									/>
									{signatureSection.mobile}:
									<DashedLangInput
										name="nirmanVyabasayiMobile"
										setFieldValue={setFieldValue}
										value={values.nirmanVyabasayiMobile}
										error={errors.nirmanVyabasayiMobile}
									/>
								</div>
							</div>
						</div>
						<br />
						<br />
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}

const PlinthLevelOwnerSundarHaraicha = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: true,
			},
			{
				api: api.anusuchiGha,
				objName: 'anuGha',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.REMOVE_ON_PRINT,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		render={(props) => <PlinthLevelOwnerSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default PlinthLevelOwnerSundarHaraicha;
