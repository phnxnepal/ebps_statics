import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import * as Yup from 'yup';

import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput, DashedReadOnlyField } from '../../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, getOptionText } from '../../../../utils/dataUtils';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import api from '../../../../utils/api';
import { constructionTypeSelectOptions } from '../../../../utils/data/genericData';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { CertificateSubHeading } from '../certificateComponents/CertificateComponents';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { PurposeOfConstructionRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import { BuildingTypeRadio } from '../mapPermitComponents/BuildingTypeRadio';
import { SundarHaraichaCertificateFloorTable } from '../certificateComponents/CertificateFloorBlockInputs';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { BlockComponents } from '../../../shared/formComponents/BlockFloorComponents';

const Detail = BuildingFinishCertificateData.details;
const formData = BuildingFinishCertificateData.formdata;
const sundarData = BuildingFinishCertificateData.sundarHaraicha;
const schema = Yup.object().shape(
	Object.assign({
		nirmanDate: validateNullableNepaliDate,
		endDate: validateNullableNepaliDate,
	})
);

class BuildingFinishCertificateViewSundarHaraicha extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};

		const prabhidik = getJsonData(otherData.prabhidik);
		const dosrocharan = getJsonData(otherData.dosrocharan);
		const mapTech = getJsonData(otherData.mapTech);
		const allowance = getJsonData(otherData.allowance);
		// const organization = otherData.organization;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		// const plinthFloor = floorArray.getPlinthFloor();
		const topFloor = floorArray.getTopFloor();
		const bottomFloor = floorArray.getBottomFloor();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['photo', 'purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', topFloor),
					...floorArray.getInitialValue('length', 'buildingLength', bottomFloor),
					...floorArray.getInitialValue('width', 'buildingWidth', bottomFloor),
					// buildingLength: plinthFloor.plinthLength,
					// buildingWidth: plinthFloor.plinthWidth,
					// floors: floorArray.getTopFloor().englishCount,
					floor: floorArray.getFloorWiseInitialValues('area', 'area', formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: allowance,
				reqFields: ['gharNo'],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'purposeOfConstruction', 'buildingHeight', 'highTension', 'highTensionUnit'],
			},
			{
				obj: prabhidik,
				reqFields: [
					'constructionType',
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'requiredDistance',
					'sadakAdhikarUnit',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'elecVolt',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{
				obj: dosrocharan,
				reqFields: ['roomCount', 'windowCount', 'doorCount', 'shutterCount'],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		initialValues1.constructionType = getOptionText(getConstructionTypeValue(initialValues1.constructionType), constructionTypeSelectOptions);

		const initVal = merge(initialValues, initialValues1);
		// console.log(initVal);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
			blocks: floorArray.getBlocks(),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const user_info = this.props.userData;
		const { initVal, floorArray, formattedFloors, blocks } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={schema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div className="build-gap">
									<LetterHeadFlex userInfo={user_info} />
									<CertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
									/>

									<p>
										{sundarData.shree} {permitData.applicantName}
									</p>
									<div>
										{userData.organization.name} {Detail.wda}
										{permitData.newWardNo} {sundarData.sadakKoNam} {permitData.sadak} {sundarData.gharNo}
										<DashedLangInput name="gharNo" setFieldValue={setFieldValue} value={values.gharNo} />
									</div>

									<div className="no-margin-date">
										<span style={{ textIndent: '2em', display: 'inline-block' }}>{sundarData.secondPara} </span>{' '}
										<DashedLangDateField
											name="nirmanDate"
											compact={true}
											inline={true}
											setFieldValue={setFieldValue}
											error={errors.nirmanDate}
											value={values.nirmanDate}
										/>
										{sundarData.secondPara2}{' '}
										<DashedLangDateField
											name="endDate"
											compact={true}
											inline={true}
											setFieldValue={setFieldValue}
											error={errors.endDate}
											value={values.endDate}
										/>
										{sundarData.secondPara3}
									</div>

									<div className="section-header">
										<p className="underline">{sundarData.thirdParaTitle}</p>
									</div>

									<p>
										{Detail.data4}
										{userData.organization.name}
										{Detail.wda}
										{permitData.newWardNo} {Detail.sabikWada} {permitData.oldWardNo} {Detail.kittaNo} {permitData.kittaNo}{' '}
										{Detail.chhetraPhal} {permitData.landArea} {permitData.landAreaType}
									</p>
									<div>
										<PurposeOfConstructionRadio
											values={values}
											setFieldValue={setFieldValue}
											errors={errors}
											space={true}
											fieldLabel={`${Detail.purposeOfConstruction}:`}
										/>
									</div>
									<div className="no-margin-date">
										<BlockComponents floorArray={floorArray} blocks={blocks}>
											{(block) => {
												const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
												const buildingLength = floorArray.getReducedFieldName('buildingLength', block);
												const buildingWidth = floorArray.getReducedFieldName('buildingWidth', block);
												const floorNumber = floorArray.getReducedFieldName('floorNumber', block);
												return (
													<>
														{sundarData.buildingDetails}: {sundarData.length}
														<DashedLengthInputWithRelatedUnits
															name={buildingLength}
															unitName="floorUnit"
															relatedFields={[
																...floorArray.getAllFields(),
																...floorArray.getAllReducedBlockFields(
																	['buildingArea', 'buildingHeight', 'buildingWidth', 'buildingLength'],
																	buildingLength
																),
															]}
														/>
														{sundarData.width}
														<DashedLengthInputWithRelatedUnits
															name={buildingWidth}
															unitName="floorUnit"
															relatedFields={[
																...floorArray.getAllFields(),
																...floorArray.getAllReducedBlockFields(
																	['buildingArea', 'buildingHeight', 'buildingWidth', 'buildingLength'],
																	buildingWidth
																),
															]}
														/>
														{sundarData.height}
														<DashedLengthInputWithRelatedUnits
															name={buildingHeight}
															unitName="floorUnit"
															relatedFields={[
																...floorArray.getAllFields(),
																...floorArray.getAllReducedBlockFields(
																	['buildingArea', 'buildingHeight', 'buildingWidth', 'buildingLength'],
																	buildingHeight
																),
															]}
														/>
														{sundarData.talla} <DashedReadOnlyField name={floorNumber} />
													</>
												);
											}}
										</BlockComponents>
										{/* {sundarData.buildingDetails}: {sundarData.length}
										<DashedLengthInputWithRelatedUnits
											name="buildingLength"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight', 'buildingWidth']}
										/>{' '}
										{sundarData.width}
										<DashedLengthInputWithRelatedUnits
											name="buildingWidth"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingLength', 'buildingHeight']}
										/>
										{sundarData.height}
										<DashedLengthInputWithRelatedUnits
											name="buildingHeight"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingLength', 'buildingWidth']}
										/>
										{sundarData.talla} <DashedLangInput setFieldValue={setFieldValue} name="floors" value={values.floors} /> */}
										<BuildingTypeRadio
											values={values}
											setFieldValue={setFieldValue}
											errors={errors}
											space={true}
											fieldLabel={`${sundarData.buildingType}:`}
										/>{' '}
										{sundarData.buildingTypeSuffix}
									</div>
									<div className="section-header">
										<p>{sundarData.floorTitle}</p>
									</div>
									<SundarHaraichaCertificateFloorTable
										floorArray={floorArray}
										formattedFloors={formattedFloors}
										showLetterNumer={false}
										blocks={blocks}
									/>
									<div className="no-margin-date">
										<div>
											<DashedLengthInputWithRelatedUnits
												name="requiredDistance"
												unitName="sadakAdhikarUnit"
												relatedFields={['sadakAdhikarKshytra']}
												label={`${sundarData.sadakAdhikar} ${sundarData.chodiyeko}`}
											/>
											<DashedLengthInputWithRelatedUnits
												name="sadakAdhikarKshytra"
												unitName="sadakAdhikarUnit"
												relatedFields={['requiredDistance']}
												label={`${sundarData.requiredDistance} ${sundarData.chodnuParne}`}
											/>
										</div>
										<div>
											<DashedLengthInputWithRelatedUnits
												name="highTension.0.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.1.value']}
												label={`${sundarData.highTension} ${sundarData.chodiyeko}`}
											/>
											<DashedLengthInputWithRelatedUnits
												name="highTension.1.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.0.value']}
												label={`${sundarData.chodnuParne}`}
											/>
										</div>
										<div>
											<DashedLengthInputWithRelatedUnits
												name="publicPropertyRequiredDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyDistance']}
												label={`${sundarData.nadi} ${sundarData.chodiyeko}`}
											/>
											<DashedLengthInputWithRelatedUnits
												name="publicPropertyDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyRequiredDistance']}
												label={`${sundarData.chodnuParne}`}
											/>
										</div>
									</div>

									<div>
										{formData.fdata29}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>
									<FooterSignatureMultiline designations={sundarData.footerSignature} />
									<div className="signature-div flex-item-space-between">
										<div style={{ textAlign: 'center' }}>
											<p>
												<span className="ui input signature-placeholder" />
											</p>
											<p>{sundarData.gharDhani}</p>
										</div>
									</div>
									<p>{sundarData.footerEnd}</p>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishViewSundarHaraicha = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.DosrocharanPrabidhikView,
				objName: 'dosrocharan',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param0: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
			// param7: ["15dayspecial"]
		}}
		hasFile={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateViewSundarHaraicha {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishViewSundarHaraicha;
