import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import * as Yup from 'yup';

import {
	getJsonData,
	getApprovalData,
	prepareMultiInitialValues,
	handleSuccess,
	checkError,
	FloorArray,
	squareUnitOptions,
} from '../../../../utils/dataUtils';
import { getUserRole, getUserTypeValueNepali, showToast } from '../../../../utils/functionUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { data } from '../../../../utils/data/SuperStructureTippaniData';
import { PatraSankhyaAndDate } from '../formComponents/PatraSankhyaAndDate';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

const commonData = data.common;
const sundarData = data.sundarHaraicha;
// const messageId = 'superstructurenodeorderview.superstructurenodeorderview_data';
const SuperStructureNoteSchema = Yup.object().shape(
	Object.assign({
		superNoteOrderDate: validateNullableNepaliDate,
		tallaThapDate: validateNullableNepaliDate,
		certificateDate: validateNullableNepaliDate,
		erDate: validateNullableNepaliDate,
		chiefDate: validateNullableNepaliDate,
	})
);
class TallaThapIjajatTippaniAdesComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};

		const { permitData, userData, prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		// const AllowancePaperData = getJsonData(otherData.AllowancePaper);
		// const plintLevelTechApplicationData = getJsonData(otherData.PlintLevelTechApplication);
		const mapTech = getJsonData(otherData.mapTech);
		const prabhidikPratibedhan = getJsonData(otherData.prabhidikPratibedhan);

		const floorArray = new FloorArray(permitData.floor);
		const plinthFloor = floorArray.getPlinthFloor();

		let serInfo = {
			subName: '',
			subDesignation: '',
		};

		if (getUserRole() === UserType.SUB_ENGINEER) {
			serInfo.subName = userData.userName;
			serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		}

		let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			// {
			// 	obj: AllowancePaperData,
			// 	reqFields: ['allowanceDate'],
			// },
			// {
			// 	obj: plintLevelTechApplicationData,
			// 	reqFields: ['techPersonName'],
			// },
			// {
			// 	obj: this.state.formData,
			// 	reqFields: ['dinaMilney', 'mapdanda'],
			// },
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{
				obj: prabhidikPratibedhan,
				reqFields: ['namedAdhikar', 'sadakAdhikarUnit'],
			},
			// { obj: permitData, reqFields: ['surrounding'] },
			{ obj: mapTech, reqFields: ['buildingHeight'] },
			{
				obj: {
					floorUnit: floorArray.getFloorUnit(),
					buildingArea: floorArray.getSumOfAreas(),
					buildingLength: plinthFloor.plinthLength,
					buildingWidth: plinthFloor.plinthWidth,
					floors: floorArray.getTopFloor().englishCount,
					tallaThapDate: getCurrentDate(true),
					superNoteOrderDate: getCurrentDate(true),
					certificateDate: getCurrentDate(true),
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{ obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = {
			initVal,
			floorArray,
		};
	}
	render() {
		const { initVal, floorArray } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={SuperStructureNoteSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.tallaThapIjajatTippaniades}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />
								<div className="section-header">
									<p className="medium">{sundarData.title}</p>
								</div>
								<PatraSankhyaAndDate
									values={values}
									setFieldValue={setFieldValue}
									errors={errors}
									dateFieldName="superNoteOrderDate"
								/>
								<div className="section-header">
									<p className="large">{commonData.title_1}</p>
								</div>
								<br />
								<div className="section-header">
									<p className="medium underline">{sundarData.subject}</p>
								</div>
								<br />

								<div style={{ textAlign: 'justify ' }} className="no-margin-field">
									<p>
										{sundarData.body.shreeman} {permitData.nibedakName}
									</p>
									<div>
										{sundarData.body.jilla} {userData.organization.address} {userData.organization.name}{' '}
										{permitData.applicantAddress} {commonData.wada} {permitData.nibedakTol} {sundarData.body.basneShree}{' '}
										{permitData.nibedakName} {sundarData.body.le} {permitData.applicantName} {sundarData.body.nauma}{' '}
										{permitData.oldMunicipal} {sundarData.body.gabisaa} {permitData.oldWardNo} {sundarData.body.hal}{' '}
										{permitData.newMunicipal} {commonData.wada} {permitData.newWardNo} {sundarData.body.kittaNo}{' '}
										{permitData.kittaNo} {sundarData.body.area} {permitData.landArea} {permitData.landAreaType}{' '}
										{sundarData.body.koSadak} {permitData.nibedakSadak} {sundarData.body.koBich}{' '}
										<DashedUnitInput name="namedAdhikar" unitName="sadakAdhikarUnit" /> {sundarData.body.sandhiyar}{' '}
										{values.surrounding &&
											values.surrounding.map((surr, i) => (
												<>
													{surrounding.find(fl => fl.direction === surr.side).value}{' '}
													<DashedLangInput
														name={`surrounding.${i}.sandhiyar`}
														setFieldValue={setFieldValue}
														value={getIn(values, `surrounding.${i}.sandhiyar`)}
														handleChange={handleChange}
														error={getIn(errors, `surrounding.${i}.sandhiyar`)}
													/>{' '}
												</>
											))}
										{sundarData.body.jagga}
										<DashedLengthInputWithRelatedUnits
											name="buildingLength"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight', 'buildingWidth']}
										/>{' '}
										{sundarData.body.chaudai}
										<DashedLengthInputWithRelatedUnits
											name="buildingWidth"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingLength', 'buildingHeight']}
										/>
										{sundarData.body.uchai}
										<DashedLengthInputWithRelatedUnits
											name="buildingHeight"
											unitName="floorUnit"
											relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingLength', 'buildingWidth']}
										/>
										<DashedLangInput setFieldValue={setFieldValue} name="floors" value={values.floors} /> {sundarData.body.talle}
										<DashedAreaInputWithRelatedUnits
											name="buildingArea"
											unitName="floorUnit"
											squareOptions={squareUnitOptions}
											relatedFields={[...floorArray.getAllFields(), 'buildingHeight', 'buildingLength', 'buildingWidth']}
										/>
										{sundarData.body.vayeko}
										<DashedLangInput name="constructionFinishDate" setFieldValue={setFieldValue} />
										{sundarData.body.constructionYear}{' '}
										<DashedLangDateField
											compact={true}
											inline={true}
											name={'certificateDate'}
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={getIn(values, 'certificateDate')}
											error={getIn(errors, 'certificateDate')}
										/>
										{sundarData.body.noteOrderDate}{' '}
										<DashedLangDateField
											compact={true}
											inline={true}
											name={'tallaThapDate'}
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={getIn(values, 'tallaThapDate')}
											error={getIn(errors, 'tallaThapDate')}
										/>
										{sundarData.body.tallaThapDate}
									</div>
								</div>
							</div>

							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const TallaThapTippaniAdesh = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.tallaThapIjajatTippaniades,
				objName: 'tallaThapIjajatTippani',
				form: true,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
			{
				api: api.superStructureBuild,
				objName: 'superBuild',
				form: false,
			},
			// {
			// 	api: api.allowancePaper,
			// 	objName: 'AllowancePaper',
			// 	form: false,
			// },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: false,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value'],
			param1: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		render={props => <TallaThapIjajatTippaniAdesComponent {...props} parentProps={parentProps} />}
	/>
);

export default TallaThapTippaniAdesh;
