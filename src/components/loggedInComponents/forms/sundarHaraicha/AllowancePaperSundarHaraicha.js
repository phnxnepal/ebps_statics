import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';

import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import {
	AllowancePaperBodySundarHariacha,
	AllowanceFooterSundarHaraicha,
	AllowancePaperSundarHaraichaSchema,
} from '../ijajatPatraComponents/AllowancePaperComponents';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { AllowancePaperData } from '../../../../utils/data/AllowancePaperData';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';

const sundarData = AllowancePaperData.sundarData;

class AllowancePaperSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, userData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const prabidhikPratibedan = getJsonData(otherData.prabidhikPratibedan);
		const RajaswoData = getJsonData(otherData.RajaswoData);
		const desApprovJsonData = otherData.designApproval;
		const anuSucKaJsonData = otherData.anukaMaster;
		// const floorArray = new FloorArray(permitData.floor);
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		const floorArray = new FloorBlockArray(permitData.floor);
		const bottomFloor = floorArray.getBottomFloor();

		let initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			{
				obj: {
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', floorArray.getTopFloor()),
					...floorArray.getInitialValue('length', 'buildingLength', bottomFloor),
					...floorArray.getInitialValue('width', 'buildingWidth', bottomFloor),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: prabidhikPratibedan,
				reqFields: ['floor'],
			},
			{
				obj: RajaswoData,
				reqFields: ['requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: mapTech,
				reqFields: [
					'constructionType',
					'coverageArea',
					'coverageDetails',
					'coverageUnit',
					'buildingHeight',
					'roof',
					'roofOther',
					'publicPropertyDistance',
					'publicPropertyUnit',
				],
			},
			{
				obj: {
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					floorareaUnit: DEFAULT_UNIT_LENGTH,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					floorwidthUnit: DEFAULT_UNIT_LENGTH,
					floorheightUnit: DEFAULT_UNIT_LENGTH,
					applicantDateBS: permitData.applicantDateBS,
					// floors: floorArray.getNepaliHighestFloorValue(),
					bhupayog: mapTech.purposeOfConstruction || '',
					bhupayogOther: mapTech.purposeOfConstructionOther || '',
				},
				reqFields: [],
			},

			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.allowanceDate)) {
			initialValues.allowanceDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		let anusuchikaOptions = [];

		otherData.anukaMaster.map((row) =>
			anusuchikaOptions.push({
				value: row.id,
				text: `${row.nameNepali} ${row.name}`,
			})
		);
		this.state = {
			initialValues,
			buildingClass,
			floorArray,
			blocks: floorArray.getBlocks(),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, useSignatureImage } = this.props;
		const { initialValues, buildingClass, floorArray, blocks } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={AllowancePaperSundarHaraichaSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.allowancePaper, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} compact={true} />
								<div className="section-header">
									<h3 className="normal end-section-minimal">{sundarData.preSubject}</h3>
									<h3 className="end-section-minimal">
										{sundarData.subjectPrefix} <span className="underline">{sundarData.subject}</span>
									</h3>
								</div>

								<AllowancePaperBodySundarHariacha
									values={values}
									errors={errors}
									handleChange={handleChange}
									setFieldValue={setFieldValue}
									buildingClass={buildingClass}
									permitData={permitData}
									userData={userData}
									floorArray={floorArray}
									blocks={blocks}
								/>
								<FooterSignatureMultiline
									designations={sundarData.firstSignature}
									signatureImages={
										useSignatureImage && [
											values.subSignature,
											values.sipharisSignature,
											values.swikritSignature,
											values.pramukhSignature,
										]
									}
								/>
								<AllowanceFooterSundarHaraicha />
							</div>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const AllowancePaperSundarHaraicha = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.allowancePaper, objName: 'allowancePaper', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabidhikPratibedan',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'RajaswoData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <AllowancePaperSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default AllowancePaperSundarHaraicha;
