import React, { Component } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { NirmanKarya } from '../../../../utils/data/mockLangFile';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import api from '../../../../utils/api';
import { showToast } from '../../../../utils/functionUtils';
import { DashedLangInputWithSlash, DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from '../../../shared/DateField';
import { Select, Label, Grid, Form } from 'semantic-ui-react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { HaalSection } from '../formComponents/PermitInfo';
import { anushuchi_ga } from '../../../../utils/data/mockLangFile';
import { PrintParams } from '../../../../utils/printUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';

const nmlang = NirmanKarya.NirmanKarya_data;
const data = anushuchi_ga.anushuchiGaView;
const opt = [
	{ key: 'A', value: 'सुपरस्ट्रक्चर', text: 'सुपरस्ट्रक्चर' },
	{ key: 'B', value: 'कम्पाउण्डवाल', text: 'कम्पाउण्डवाल' },
];

const opt1 = [
	{ key: 'ma', value: 'गरेको', text: 'गरेको' },
	{ key: 'hami', value: 'गरेका', text: 'गरेका' },
];

const getMs = value => {
	const ret = opt1.filter(row => row.value === value);
	return ret[0] ? ret[0].key : 'ma';
};

const DosrocharanSchema = Yup.object().shape(
	Object.assign({
		dosrocharanDate: validateNullableNepaliDate,
		supStrucDate: validateNullableNepaliDate,
		wareshDate: validateNullableNepaliDate,
	})
);
class DosrocharanAbedanSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		initVal = prepareMultiInitialValues(
			{
				// Default values.
				obj: {
					abedanMs: nmlang.data_2.data_2_ms.maile,
					supercomp: opt[0].value,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(otherData.superStructCons),
				reqFields: ['supStrucDate', 'chalaniNumber'],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.dosrocharanDate)) {
			initVal.dosrocharanDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.wareshDate)) {
			initVal.wareshDate = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}

	render() {
		const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		const udata = userData;
		return (
			<div>
				<Formik
					initialValues={{ applicantMs: opt1[0].value, ...initVal }}
					validationSchema={DosrocharanSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.DosrocharanAbedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//     getNextUrl(
								//         this.props.parentProps.location.pathname
								//     )
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, handleChange, isSubmitting, values, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef} className="build-gaps">
								<div className="NJ-Main superStruConsView">
									<SectionHeader>
										<h2>{nmlang.heading.heading_1}</h2>
									</SectionHeader>
									<div style={{ textAlign: 'right' }}>
										{nmlang.data_2.data_2_2}
										{': '}
										<DashedLangDateField
											name="dosrocharanDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.dosrocharanDate}
											// label={nmlang.data_2.data_2_2}
											className="dashedForm-control"
											error={errors.dosrocharanDate}
										/>
									</div>

									<SectionHeader>
										<h3 className="underline">{nmlang.heading.SubjectSundar}</h3>
									</SectionHeader>

									<div style={{ textAlign: 'justify' }}>
										<span>{nmlang.data_1.data_1_1}</span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{nmlang.data_2.data_2_1.data_2_1_1}
											{', '}
											{udata.organization.name}{' '}
										</span>
										<DashedLangDateField
											inline={true}
											label={nmlang.data_2.data_2_2}
											name="supStrucDate"
											setFieldValue={setFieldValue}
											value={values.supStrucDate}
											error={errors.supStrucDate}
										/>
										<span>{nmlang.sundarData.data2}</span>
										<DashedLangInputWithSlash
											name="chalaniNumber"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.chalaniNumber}
											error={errors.chalaniNumber}
											inline={true}
										/>
										<span>{nmlang.sundarData.data3}</span>
										<Select
											placeholder=" सुपरस्ट्रक्चर "
											options={opt}
											name="supercomp"
											value={values.supercomp}
											onChange={(e, { value }) => setFieldValue('supercomp', value)}
										/>
										{errors.supercomp && (
											<Label pointing prompt size="large">
												{errors.supercomp}
											</Label>
										)}
										<span>{nmlang.sundarData.data1}</span>
										<HaalSection permitData={permitData} /> <span>{nmlang.sundarData.data4}</span>
										{' 	'}
										<Select
											options={opt1}
											name="applicantMs"
											onChange={(e, { value }) => {
												setFieldValue('applicantMs', value);
											}}
											value={values.applicantMs}
										/>{' '}
										{data[getMs(values['applicantMs'])].third}
									</div>

									<div style={{ paddingTop: '20px', paddingLeft: '30px' }}>
										<div>
											{nmlang.sundarData.data5}
											<DashedLangInput name="pasa" setFieldValue={setFieldValue} value={values.pasa} error={errors.pasa} />
										</div>
										<p>
											{nmlang.sundarData.data6}
											{permitData.applicationNo}
										</p>
										<p>
											{nmlang.sundarData.data7}
											{permitData.applicantDateBS}
										</p>
									</div>

									<div className="NJ-right">
										<Grid>
											<Grid.Row>
												<Grid.Column>
													<p style={{ textAlign: 'center' }}>{nmlang.sundarData.data8}</p>
													<span>{`${nmlang.footer_1.footer_1_1} `}</span>
													<strong>{permitData.nibedakName}</strong>
													<br />
													<br />
													<span>{nmlang.footer_1.footer_1_3}</span>
													<span className="ui input dashedForm-control"></span>
													<br />
													<br />
												</Grid.Column>
											</Grid.Row>
										</Grid>
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const DosrocharanAbedanSundarHaraicha = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.DosrocharanAbedan,
				objName: 'dosrocharanAbedan',
				form: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'superStructCons',
				form: false,
			},
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			...PrintParams.INLINE_FIELD,
		}}
		useInnerRef={true}
		prepareData={data => data}
		parentProps={parentProps}
		render={props => <DosrocharanAbedanSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanAbedanSundarHaraicha;
