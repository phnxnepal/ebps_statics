import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import * as Yup from 'yup';
import { PlinthLevelTechLang, sundarData } from '../../../../utils/data/PilenthTechAppLang';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput, CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import { UserOldInfo, HaalSection } from '../formComponents/PermitInfo';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { plinthLevelOwnerData } from '../../../../utils/data/plinthLevelFormsData';
import { validateNullableNepaliDate, validateNullableNumber } from '../../../../utils/validationUtils';
import { olddosrocharanPrabidhikview } from '../../../../utils/data/mockLangFile';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

const PlinthTechLang = PlinthLevelTechLang;
const dpv = olddosrocharanPrabidhikview;
const userInfo = plinthLevelOwnerData.userInfo;

const plinthLevelAppSchema = {
	plinthApprovalDate: validateNullableNepaliDate,
	date: validateNullableNepaliDate,
	naksaDastur: validateNullableNumber,
	thapGharDastur: validateNullableNumber,
};

const schema = Yup.object().shape(Object.assign({ ...yesNoSectionSchema, ...plinthLevelAppSchema }));

class SuperStructureNirmanKaryaSundarHaraichaComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, enterByUser } = this.props;

		const json_data = getJsonData(prevData);
		const plinthOwner = getJsonData(otherData.plinthOwner);
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					letterSubmitDate: getCurrentDate(true),
					date: getCurrentDate(true),
					naksaDastur: '',
					thapGharDastur: '',
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{ obj: plinthOwner, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = { initialValues };
	}

	render() {
		const { errors: reduxErrors, permitData, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialValues } = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// console.log('log');
						try {
							await this.props.postAction(`${api.supernirmaanKarya}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleSubmit, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h3 className="underline normal end-section">({PlinthTechLang.heading})</h3>
									<h3 className="underline end-section">{dpv.sundarData.heading}</h3>
								</SectionHeader>

								<LetterSalutation
									lines={[letterSalutation.shreemanEngineer, `${userData.organization.name} ${letterSalutation.karyalaya}`]}
								/>
								<br />
								<div className="margin-bottom">
									<UserOldInfo permitData={permitData} userData={userData} />
									{userInfo.le} <HaalSection permitData={permitData} /> {userInfo.end}
								</div>
								<YesNoSection setFieldValue={setFieldValue} errors={errors} values={values} />
								<br />
								<div className="no-margin-field margin-bottom">
									{sundarData.plinthDate}
									<CompactDashedLangDate
										name="plinthApprovalDate"
										setFieldValue={setFieldValue}
										value={values.plinthApprovalDate}
										error={errors.plinthApprovalDate}
									/>{' '}
									{sundarData.naksaDastur}
									<CompactDashedLangInput
										name="naksaDastur"
										setFieldValue={setFieldValue}
										value={values.naksaDastur}
										error={errors.naksaDastur}
									/>
									{sundarData.thapGharDastur}{' '}
									<CompactDashedLangInput
										name="thapGharDastur"
										setFieldValue={setFieldValue}
										value={values.thapGharDastur}
										error={errors.thapGharDastur}
									/>{' '}
									{sundarData.plinthTechAppDate}{' '}
									<CompactDashedLangDate
										name="letterSubmitDate"
										setFieldValue={setFieldValue}
										value={values.letterSubmitDate}
										error={errors.letterSubmitDate}
									/>{' '}
									{sundarData.end}
								</div>
								<div className="margin-bottom">
									<div>
										{PlinthTechLang.sign}: <span className="ui input dashedForm-control" />
									</div>
									<div>
										{sundarData.signatureSection.techName}{' '}
										<DashedLangInput name="subName" setFieldValue={setFieldValue} value={values.subName} error={errors.subName} />
									</div>
									<div>
										{PlinthTechLang.position}:{' '}
										<DashedLangInput
											name="subDesignation"
											setFieldValue={setFieldValue}
											value={values.subDesignation}
											error={errors.subDesignation}
										/>
									</div>
									<div>
										{sundarData.signatureSection.date}:{' '}
										<DashedLangInput name="date" setFieldValue={setFieldValue} value={values.date} error={errors.date} />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SuperStructureNirmanKaryaSundarHaraicha = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.supernirmaanKarya,
				objName: 'superNirmanKarya',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui checkbox', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByTagName', 'input', 'value'],
		}}
		render={props => <SuperStructureNirmanKaryaSundarHaraichaComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNirmanKaryaSundarHaraicha;
