import React, { Component } from 'react';
import { Form, Table, TableCell, Dropdown, Select } from 'semantic-ui-react';
import { mapTechnical } from '../../../utils/data/mockLangFile';
import { Formik, getIn } from 'formik';

import { isEmpty, getUserRole } from '../../../utils/functionUtils';
import { surroundingMappingFlat } from '../../../utils/dataUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import FormikInput from '../../shared/FormikInput';
import FormikCheckbox from '../../shared/FormikCheckbox';
import api from '../../../utils/api';
import { checkError, getJsonData, prepareMultiInitialValues, handleSuccess, getRelatedFields } from '../../../utils/dataUtils';
import UnitDropdown from '../../shared/UnitDropdown';
import { EbpsFormInline } from '../../shared/EbpsForm';
import { translateDate, translateNepToEng } from '../../../utils/langUtils';
import MapTechValidateSchema from '../formValidationSchemas/mapTechDesViewValidation';
import { DashedLangInput, DashedNormalInput, DashedNormalInputIm } from '../../shared/DashedFormInput';
import SaveButtonValidation from './../../shared/SaveButtonValidation';
import {
	DashedAreaUnitInput,
	DashedMultiUnitAreaInput,
	DashedMultiUnitLengthInput,
	UnitDropdownWithRelatedFields,
} from '../../shared/EbpsUnitLabelValue';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../utils/enums/constructionType';
import { UserType } from '../../../utils/userTypeUtils';
import { getCurrentDate, getNepaliDate } from '../../../utils/dateUtils';
import { landAreaTypeOptions } from '../../../utils/optionUtils';
import { SetbackSelect } from '../../shared/Select';
import { TableInputField } from '../../shared/TableInput';
import { validateNullableNumber, validateNumber, validateNullableNUmberField } from '../../../utils/validationUtils';
import { ConstructionTypeRadio } from './mapPermitComponents/ConstructionTypeRadio';
import { lengthconvertor } from '../../../utils/mathUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { PurposeOfConstructionRadio } from './mapPermitComponents/PurposeOfConstructionRadio';
import { SectionHeader } from '../../uiComponents/Headers';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { FloorDetailsTable, HeightTable } from './mapTechFormComponents/FloorDetailsTable';
import { floorData } from '../../../utils/data/genericData';
import { RadioInput } from '../../shared/formComponents/RadioInput';
import { NirmanTypeRadio } from './mapTechFormComponents/NirmanTypeRadio';
import { SadakAdhikarSection } from './mapTechFormComponents/SadakAdhikarSection';
import { isKamalamai, isBiratnagar, isKageyshowri } from '../../../utils/clientUtils';
import { FormikCheckboxIm } from '../../shared/FormikCheckbox';
import { PrintIdentifiers } from '../../../utils/printUtils';
import { SignatureImage } from './formComponents/SignatureImage';

const mapTech_data = mapTechnical.mapTechnicalDescription;
const RoadSetBack = mapTech_data.mapTech_24_table;
const outerLayout = mapTech_data.mapTech_25_table;
const lengthtoLeave = mapTech_data.mapTech_26_table;
const cantiliver = mapTech_data.mapTech_27_table;
const hitensionline = mapTech_data.mapTech_28_table;
// const constructionTypeOptions = buildingPermitApplicationForm.permitApplicationFormView.form_step5.checkBox_option2;
const mixtureOptions = [
	{ key: 'भित्री', value: 'भित्री गाह्राे', text: 'भित्री गाह्राे' },
	{ key: 'बाहिरी', value: 'बाहिरी गाह्राे', text: 'बाहिरी गाह्राे' },
];
const inchOptions = [
	{ key: '5', value: '5 inch', text: '5 inch' },
	{ key: '10', value: '10 inch', text: '10 inch' },
];
const ratioOptions = [
	{ key: '1', value: '1:4', text: '1:4' },
	{ key: '2', value: '1:6', text: '1:6' },
];
const ratioFirst = [{ key: '1', value: '1:4', text: '1:4' }];

const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

class MapTechnicalDescriptionViewComponent extends Component {
	constructor(props) {
		super(props);

		const { permitData, otherData, prevData, userData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const topFloor = floorArray.getTopFloor();
		const bottomFloor = floorArray.getBottomFloor();
		
		const formattedFloors = floorArray.getFormattedFloors();
		let initVal = {};
		
		Object.values(mapTech_data.mapTech_17.checkbox_options).map((e, i) => {
			if(i < topFloor.floor){
				initVal[e] = "checked"
			}
		})

		const constructionType = permitData.constructionType;

		initVal.publicPropertyUnit = DEFAULT_UNIT_LENGTH;
		initVal.cantileverUnit = DEFAULT_UNIT_LENGTH;
		initVal.setBackUnit = DEFAULT_UNIT_LENGTH;
		initVal.highTensionUnit = DEFAULT_UNIT_LENGTH;
		initVal.landLengthUnit = DEFAULT_UNIT_LENGTH;
		// initVal.landLengthUnit = 'METRE';
		initVal.fieldDetailsUnit = DEFAULT_UNIT_AREA;
		initVal.fieldLengthUnit = DEFAULT_UNIT_LENGTH;
		// initVal.fieldWidthUnit = 'METRE';
		initVal.plinthDetailsUnit = DEFAULT_UNIT_AREA;
		initVal.plinthLengthUnit = DEFAULT_UNIT_LENGTH;
		// initVal.plinthLengthUnit = 'METRE';
		initVal.otherAreaUnit = DEFAULT_UNIT_AREA;
		initVal.coverageUnit = DEFAULT_UNIT_AREA;
		initVal.coverageHeightUnit = DEFAULT_UNIT_LENGTH;
		// initVal.coverageWidthUnit = 'METRE';
		initVal.descriptionFloorUnit = DEFAULT_UNIT_LENGTH;
		initVal.buildingHeightUnit = DEFAULT_UNIT_LENGTH;
		initVal.floorHeightUnit = DEFAULT_UNIT_LENGTH;
		initVal.availableAreaUnit = DEFAULT_UNIT_AREA;
		initVal.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initVal.adhikarRoadExists0 = 'N';
		initVal.adhikarRoadExists1 = 'N';
		initVal.adhikarRoadExists2 = 'N';
		initVal.adhikarRoadExists3 = 'N';
		initVal.sadakAdhikarKshytra0 = 0;
		initVal.sadakAdhikarKshytra1 = 0;
		initVal.sadakAdhikarKshytra2 = 0;
		initVal.sadakAdhikarKshytra3 = 0;
		initVal.ratioOptions = ratioFirst;
		initVal.ratio = '1:4';
		initVal.ratio = '5 inch';

		initVal.plinthLength = bottomFloor.length;
		initVal.plinthDetails = bottomFloor.area;
		initVal.plinthWidth = bottomFloor.width;

		initVal.changeApproval = mapTech_data.mapTech_6.checkbox_options.option_1;
		if (permitData.floor[0] && permitData.floor[0].floorUnit) {
			initVal.floorUnit = `SQUARE ${permitData.floor[0].floorUnit}`;
			initVal.heightUnit = permitData.floor[0].floorUnit;
			initVal.plinthLengthUnit = permitData.floor[0].floorUnit;
		} else {
			initVal.floorUnit = DEFAULT_UNIT_AREA;
			initVal.plinthLengthUnit = DEFAULT_UNIT_LENGTH;
		}
		const isPuranoGhar = getConstructionTypeValue(constructionType) === ConstructionTypeValue.PURANO_GHAR;
		if (constructionType) {
			if (isEmpty(initVal.floorDetails)) {
				initVal.floorDetails = [];
			}
			if (isPuranoGhar) {
				initVal.floorDetails = floorArray.getInitialValues('area', 'oldConstructionArea', formattedFloors);
			} else {
				initVal.floorDetails = floorArray.getInitialValues('area', 'proposedConstructionArea', formattedFloors);
			}
		}

		const floorDetailsSchema = floorArray.getFloorSchema(
			['proposedConstructionArea', 'oldConstructionArea', 'totalConstructionArea'],
			validateNullableNumber
		);

		if (permitData.surrounding) {
			permitData.surrounding.forEach((side) => {
				const index = parseInt(side.side) - 1;
				if (isEmpty(initVal.wallToBorder)) {
					initVal.wallToBorder = [];
				}
				if (!initVal.wallToBorder[index]) {
					initVal.wallToBorder[index] = {};
				}
				initVal.wallToBorder[index].existsingSpacing = side.feet;
				initVal.wallToBorder[index].roadExists = outerLayout.options.option2;
				initVal.wallToBorder[index].windowDoorExists = outerLayout.options.option2;
			});
		}

		Object.keys(outerLayout.mapTech_25_table_subheading).forEach((key, index) => {
			if (initVal.wallToBorder && !initVal.wallToBorder[index]) {
				initVal.wallToBorder[index] = {
					roadExists: outerLayout.options.option2,
					windowDoorExists: outerLayout.options.option2,
				};
			}
		});

		const jsonData = getJsonData(prevData);

		const anuGha = checkError(otherData.anuGha);
		const permitEdit = otherData.permitEdit;
		// (surroundingMappingFlat.filter((e) => {console.log( e.value.includes(permitEdit.mohada) )}))
		// const dName = checkError(otherData.anuGha.dName);

		if(jsonData.fieldDetails == undefined || isEmpty(jsonData.fieldDetails)){
			jsonData.fieldDetails = permitData.landArea
		}

		let mapSideObj = {};
		if (isPuranoGhar) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					userName: userData.info.userName,
					educationQualification: userData.info.educationQualification,
					consultancyName: userData.info.consultancyName,
					municipalRegNo: userData.info.municipalRegNo,
					stamp: userData.info.stamp,
					enterDate: getCurrentDate(true),
				};
			} else {
				mapSideObj = {};
			}
		} else {
			if (!isEmpty(anuGha)) {
				if (!isEmpty(anuGha.dName && anuGha.dDesignation && anuGha.dDate)) {
					mapSideObj = {
						userName: anuGha.dName,
						educationQualification: anuGha.dDesignation,
						consultancyName: anuGha.dConsultancyName,
						municipalRegNo: anuGha.dMunicipalRegNo,
						enterDate: anuGha.dDate,
					};
				} else
					mapSideObj = {
						userName: anuGha.enterBy.userName,
						educationQualification: anuGha.enterBy.educationQualification,
						consultancyName: anuGha.enterBy.consultancyName,
						municipalRegNo: anuGha.enterBy.municipalRegNo,
						stamp: anuGha.enterBy.stamp,
						enterDate: getNepaliDate(anuGha.enterDate),
					};
			}
		}
		const floorHeight = topFloor.floor;

		// Other Set back options
		const otherSetbackMaster = this.props.otherData.otherSetBack;
		let otherSetBackOption = [{ value: mapTech_data.mapTech_23.defaultNadiOption, text: mapTech_data.mapTech_23.defaultNadiOption }];
		otherSetbackMaster.forEach((row) => row.status === 'Y' && otherSetBackOption.push({ value: row.placeName, text: row.placeName }));

		// Road set back
		const roadSetbackMaster = this.props.otherData.roadSetBack ? this.props.otherData.roadSetBack.filter((row) => row.status === 'Y') : [];
		let roadSetBackOption = [
			{
				value: mapTech_data.mapTech_23.defaultRoadOption,
				text: mapTech_data.mapTech_23.defaultRoadOption,
			},
		];
		roadSetbackMaster.forEach((row) => row.status === 'Y' && roadSetBackOption.push({ value: row.roadName, text: row.roadName }));

		const initVal1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['constructionType', 'purposeOfConstruction', 'purposeOfConstructionOther', 'landArea', 'landAreaType'],
			},
			// { obj: plinthLevel, reqFields: [] },
			{
				obj: {
					...floorArray.getInitialValue('nepaliCountName', 'buildingDetailfloor', topFloor),
					floor: { ...floorArray.getInitialValues('height', 'height', formattedFloors) },
				},
				reqFields: [],
			},
			{ obj: { buildingFloorHeight: floorHeight }, reqFields: [] },
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					roof: mapTech_data.mapTech_22.mapTech_22_radioBox.mapTech_22_Opt_1,
				},
				reqFields: [],
			},
			{ obj: mapSideObj, reqFields: [] },
			{
				obj: {
					roadName0: roadSetBackOption[0].value,
					roadName1: roadSetBackOption[0].value,
					roadName2: roadSetBackOption[0].value,
					roadName3: roadSetBackOption[0].value,
				},
				reqFields: [],
			},

			{ obj: jsonData, reqFields: [] }
		);
		initVal1.constructionType = getConstructionTypeValue(initVal1.constructionType);
		this.state = {
			Swikriti: mapTech_data.mapTech_6.mapTech_6_Opt_1,
			construction_type: mapTech_data.mapTech_20.mapTech_20_Opt_1,
			initVal1,
			initVal,
			floorArray,
			formattedFloors,
			topFloor,
			hasBlocks: floorArray.hasBlocks,
			blocks: floorArray.blocks || [],
			roadSetbackMaster,
			roadSetBackOption,
			otherSetBackOption,
			otherSetbackMaster,
			floorSchema: floorArray.getFloorSchema(['height'], validateNullableNumber),
			buildingHeightSchema: floorArray.getFloorSchema(['buildingHeight'], validateNullableNumber),
			floorDetailsSchema,
			hasOtherSetbackMaster: otherSetbackMaster && Array.isArray(otherSetbackMaster) && otherSetbackMaster.length > 0,
			hasRoadSetbackMaster: roadSetbackMaster && Array.isArray(roadSetbackMaster) && roadSetbackMaster.length > 0,
			isPuranoGhar,
		};
	}

	render() {
		const {
			floorArray,
			formattedFloors,
			initVal,
			initVal1,
			hasBlocks,
			blocks,
			topFloor,
			roadSetbackMaster,
			roadSetBackOption,
			otherSetBackOption,
			otherSetbackMaster,
			floorDetailsSchema,
			floorSchema,
			buildingHeightSchema,
			isPuranoGhar,
		} = this.state;
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, staticFiles } = this.props;
		// For deep merging objects
		return (
			<div className="mapTech-Form">
				<Formik
					enableReinitialize
					initialValues={{ ...initVal, ...initVal1 }}
					validationSchema={MapTechValidateSchema(floorDetailsSchema, floorSchema, buildingHeightSchema, isPuranoGhar)}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);

						values.unit = 'मितर्';
						values.applicationNo = permitData.applicantNo;

						if (values.ceilingHeight && values.ceilingHeight.length > 0) {
							values.totalHeight = values.ceilingHeight.reduce((total, next) => {
								if (next) {
									return total + next['height'] ? parseInt(next['height']) : 0;
								} else {
									return 0;
								}
							}, 0);
						}
						// delete values.sadakAdhikarKshytra;
						try {
							await this.props.postAction(api.mapTechnicalDescription, values, true);

							window.scrollTo(0, 0);
							actions.setSubmitting(false);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Errors', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={(props) => {
						return (
							<Form loading={props.isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="print-small-font">
									<FormHeading />
									<div className="inline field">
										<span>{mapTech_data.mapTech_1} </span>
										<span className="ui input dashedForm-control">{permitData.applicantName}</span>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_2} </span>
										<span className="ui input dashedForm-control">
											{isBiratnagar ? permitData.naksawalaName : permitData.nibedakName}
										</span>
									</div>
									<div className="inline field">
										<PurposeOfConstructionRadio
											space={true}
											fieldLabel={mapTech_data.mapTech_3}
											values={props.values}
											setFieldValue={props.setFieldValue}
											errors={props.errors}
										/>
									</div>
									{/* Construction Use */}
									<div className="inline field">
										<ConstructionTypeRadio space={true} fieldLabel={mapTech_data.mapTech_4} />
									</div>
									{/* Change Details */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_5.mapTech_5_head}</span>
										<div style={{ marginLeft: '20px' }} className="inline field">
											<span>{mapTech_data.mapTech_5.mapTech_5_1}</span>
											<div className="ui input">
												<DashedLangInput
													name="changeDetails[0]"
													value={getIn(props.values, 'changeDetails.0')}
													setFieldValue={props.setFieldValue}
												/>
											</div>
										</div>
										<div style={{ marginLeft: '20px' }} className="inline field">
											<span>{mapTech_data.mapTech_5.mapTech_5_2}</span>
											<div className="ui input">
												<DashedLangInput
													name="changeDetails[1]"
													value={getIn(props.values, 'changeDetails.1')}
													setFieldValue={props.setFieldValue}
												/>
											</div>
										</div>
									</div>
									{/* Change Approval */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_6.mapTech_6_head}</span>
										<br />
										{Object.values(mapTech_data.mapTech_6.checkbox_options).map((option) => (
											<div key={option} className="ui radio checkbox" style={{ marginRight: 10 }}>
												<input
													type="radio"
													name="changeApproval"
													value={option}
													id={option}
													defaultChecked={props.values.changeApproval === option}
													onChange={props.handleChange}
												/>
												<label>{option}</label>
											</div>
										))}
									</div>
									<div className="inline field">
										{mapTech_data.mapTech_7} <span className="ui input dashedForm-control">{permitData.kittaNo}</span>
									</div>
									{/* Land Details */}
									<div className="inline field">
										<UnitDropdown
											label={mapTech_data.mapTech_8}
											name="landAreaType"
											setFieldValue={props.setFieldValue}
											value={props.values.landAreaType}
											options={landAreaTypeOptions}
										/>
										<div className="ui input">
											<DashedNormalInput
												name="landArea"
												value={translateNepToEng(props.values.landArea)}
												setFieldValue={props.setFieldValue}
												handleChange={props.handleChange}
											/>
										</div>
										<DashedMultiUnitLengthInput
											name="landLength"
											unitName="landLengthUnit"
											relatedFields={['landWidth']}
											label={mapTech_data.mapTech_8_1}
										/>
										<DashedMultiUnitLengthInput
											name="landWidth"
											unitName="landLengthUnit"
											relatedFields={['landLength']}
											label={mapTech_data.mapTech_8_2}
										/>
									</div>
									{/* Field Details */}
									<div className="inline field">
										<DashedMultiUnitAreaInput
											name="fieldDetails"
											unitName="fieldLengthUnit"
											label={mapTech_data.mapTech_9}
											squareOptions={squareUnitOptions}
											relatedFields={['fieldLength', 'fieldWidth']}
										/>
										<DashedMultiUnitLengthInput
											name="fieldLength"
											unitName="fieldLengthUnit"
											relatedFields={['fieldWidth']}
											areaField={'fieldDetails'}
											label={mapTech_data.mapTech_9_1}
										/>
										<DashedMultiUnitLengthInput
											name="fieldWidth"
											unitName="fieldLengthUnit"
											relatedFields={['fieldLength']}
											areaField={'fieldDetails'}
											label={mapTech_data.mapTech_9_2}
										/>
									</div>
									{/* Plint details */}
									<div className="inline field">
										<DashedMultiUnitAreaInput
											name="plinthDetails"
											unitName="plinthLengthUnit"
											label={mapTech_data.mapTech_10}
											squareOptions={squareUnitOptions}
											relatedFields={['plinthLength', 'plinthWidth']}
										/>
										<DashedMultiUnitLengthInput
											name="plinthLength"
											unitName="plinthLengthUnit"
											relatedFields={['plinthWidth']}
											areaField={'plinthDetails'}
											label={mapTech_data.mapTech_9_1}
										/>
										<DashedMultiUnitLengthInput
											name="plinthWidth"
											unitName="plinthLengthUnit"
											relatedFields={['plinthLength']}
											areaField={'plinthDetails'}
											label={mapTech_data.mapTech_9_2}
										/>
									</div>
									{/* Coverage details */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_11}</span>
										<DashedNormalInputIm name="coverageDetails" />{' '}
										<DashedMultiUnitLengthInput
											name="coverageHeight"
											unitName="coverageHeightUnit"
											relatedFields={['coverageWidth']}
											label={mapTech_data.mapTech_11_1}
										/>
										<DashedMultiUnitLengthInput
											name="coverageWidth"
											unitName="coverageHeightUnit"
											relatedFields={['coverageHeight']}
											label={mapTech_data.mapTech_11_2}
										/>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_12}</span>
										<div className="ui input">
											<DashedLangInput
												name="buildingFloorHeight"
												value={props.values.buildingFloorHeight}
												setFieldValue={props.setFieldValue}
												handleChange={props.handleChange}
												error={props.errors.buildingFloorHeight}
											/>
										</div>
										{/* <DashedUnitInput name="descriptionFloorWidth" unitName="descriptionFloorUnit" label={mapTech_data.mapTech_11_2} /> */}
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<FloorDetailsTable
											values={props.values}
											setFieldValue={props.setFieldValue}
											handleChange={props.handleChange}
											errors={props.errors}
											formattedFloors={formattedFloors}
											floorArray={floorArray}
										/>
									</div>
									{/* Other area */}
									<div className="inline field">
										<DashedAreaUnitInput name="otherArea" unitName="otherAreaUnit" label={mapTech_data.mapTech_13} />
									</div>
									{/* Available area */}
									<div className="inline field">
										<DashedAreaUnitInput name="availableArea" unitName="availableAreaUnit" label={mapTech_data.mapTech_14} />
									</div>
									{/* Ground Coverage */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_15.mapTech_15_head}</span>
										<div className="inline field" style={{ marginLeft: '20px' }}>
											<DashedAreaUnitInput
												name="coverageArea"
												unitName="coverageUnit"
												label={mapTech_data.mapTech_15.mapTech_15_1}
											/>
										</div>
										<div className="inline field" style={{ marginLeft: '20px' }}>
											<span>{mapTech_data.mapTech_15.mapTech_15_2}</span>
											<DashedNormalInputIm name="coveragePercent" />
										</div>
									</div>
									{/* Allowablt */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_16}</span>
										<DashedNormalInputIm name="allowable" />
									</div>
									<div className="inline field">

										<span>{mapTech_data.mapTech_17.mapTech_17_head}</span>
										{hasBlocks ? (
											blocks.map((block) => (
												<div className="div-indent" key={block}>
													{floorData.block} {block}
													{Object.values(mapTech_data.mapTech_17.checkbox_options).map((option) => (
														<React.Fragment key={option}>
															{isKamalamai ? (
																<FormikCheckboxIm name={`${block}_buildingDetailfloor`} label={option} space={true} />
															) : (
																<RadioInput name={`${block}_buildingDetailfloor`} space={true} option={option} />
															)}
														</React.Fragment>
													))}
												</div>
											))
										) : (
											<>
												{Object.values(mapTech_data.mapTech_17.checkbox_options).map((option) => (
													<React.Fragment key={option}>
														{isKamalamai  || isKageyshowri ? (
															<FormikCheckboxIm name="buildingDetailfloor" label={option} space={true} />
														) : (
															<RadioInput name="buildingDetailfloor" space={true} option={option} />
														)}
													</React.Fragment>
												))}
											</>
										)}
									</div>
									<HeightTable floorArray={floorArray} formattedFloors={formattedFloors} topFloor={topFloor} />
									<NirmanTypeRadio space={true} fieldLabel={mapTech_data.mapTech_20.mapTech_20_head} />
									<div className="inline field">
										<span>{mapTech_data.mapTech_21}</span>
										<div className="ui input">
											{/* <DashedLangInput
												name="mixture"
												value={props.values.mixture}
												setFieldValue={props.setFieldValue}
												error={props.errors.mixture}
											/> */}
											<Select
												placeholder="भित्री गाह्राे"
												name="mixture"
												onChange={(e, { value }) => props.setFieldValue('mixture', value)}
												value={props.values.mixture}
												options={mixtureOptions}
											/>
											<Select
												placeholder="5 inch"
												name="inch"
												onChange={(e, { value }) => {
													props.setFieldValue('inch', value);
													if (value === '10 inch') {
														props.setFieldValue('ratioOptions', ratioOptions);
													} else {
														props.setFieldValue('ratioOptions', ratioFirst);
													}
												}}
												value={props.values.inch}
												options={inchOptions}
											/>
											<Select
												placeholder="1:4"
												name="ratio"
												onChange={(e, { value }) => props.setFieldValue('ratio', value)}
												value={props.values.ratio}
												options={props.values.ratioOptions}
											/>
											{
												// [getMs(props.values['mixture'])]
											}
										</div>
									</div>
									{/* <EbpsFormInline
						span={mapTech_data.mapTech_21}
						name='mixture'
						setFieldValue={props.setFieldValue}
						value={props.values.mixture}
						error={props.errors.mixture}
					/> */}
									<div className="inline field">
										<span>{mapTech_data.mapTech_22.mapTech_22_head}</span>
										{Object.values(mapTech_data.mapTech_22.mapTech_22_radioBox).map((option) => (
											<div className="ui radio checkbox prabidhik" key={option}>
												<input
													type="radio"
													name="roof"
													value={option}
													defaultChecked={props.values.roof === option}
													onChange={props.handleChange}
												/>
												<label>{option}</label>
											</div>
										))}

										{props.values.roof === mapTech_data.mapTech_22.mapTech_22_radioBox.mapTech_22_Opt_6 && (
											<DashedLangInput
												// inline={true}
												name="roofOther"
												placeholder="Additional Information..."
												setFieldValue={props.setFieldValue}
												value={props.values.roofOther}
												error={props.errors.roofOther}
											/>
										)}
									</div>

									<SadakAdhikarSection
										hasRoadSetbackMaster={this.state.hasRoadSetbackMaster}
										values={props.values}
										errors={props.errors}
										setFieldValue={props.setFieldValue}
										roadSetBackOption={roadSetBackOption}
										roadSetbackMaster={roadSetbackMaster}
									/>
									<div className="inline field">
										<span>{mapTech_data.mapTech_24}</span>
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<Table celled compact collapsing className="road-table">
											<Table.Header>
												<Table.Row>
													{Object.keys(RoadSetBack.mapTech_24_table_heading).map((key, index) => (
														<Table.HeaderCell key={index}>
															{RoadSetBack.mapTech_24_table_heading[key]}
															{index !== 0 && (
																<>
																	{' ('}
																	<Dropdown
																		name="setBackUnit"
																		onChange={(e, { value }) => props.setFieldValue('setBackUnit', value)}
																		value={props.values.setBackUnit}
																		options={distanceOptions}
																	/>
																	{')'}
																</>
															)}
														</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{Object.keys(RoadSetBack.mapTech_24_table_subheading).map((key, RoadSetBackValue) => {
													// RoadSetBackValue++;
													return (
														<Table.Row key={RoadSetBackValue}>
															<TableCell>
																<label htmlFor={`setBack[${RoadSetBackValue}]`}>
																	{RoadSetBack.mapTech_24_table_subheading[key]}
																</label>
																{/*<input id={`floor[${floorIndex - 1}].floor`} name={`floor[${floorIndex - 1}].floor`} hidden value={`floor[${floorIndex - 1}].floor`}/>*/}
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadCenter`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`setBack.${RoadSetBackValue}.distanceFromRoadCenter`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadCenter`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(
																				props.errors,
																				`setBack.${RoadSetBackValue}.distanceFromRoadCenter`
																			)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadBoundary`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`setBack.${RoadSetBackValue}.distanceFromRoadBoundary`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadBoundary`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(
																				props.errors,
																				`setBack.${RoadSetBackValue}.distanceFromRoadBoundary`
																			)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(
																				props.errors,
																				`setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`
																			)}
																		</div>
																	)}
																</div>
															</TableCell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_25}</span>
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<Table celled compact collapsing>
											<Table.Header>
												<Table.Row>
													{Object.keys(outerLayout.mapTech_25_table_heading).map((key) => (
														<Table.HeaderCell key={key}>{outerLayout.mapTech_25_table_heading[key]}</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{Object.keys(outerLayout.mapTech_25_table_subheading).map((key, outerLayoutValue) => {
													// outerLayoutValue++;
													return (
														<Table.Row key={outerLayoutValue}>
															<TableCell>
																<label htmlFor={`wallToBorder[${outerLayoutValue}]`}>
																	{outerLayout.mapTech_25_table_subheading[key]}
																</label>
															</TableCell>
															<TableCell>
																{Object.values(outerLayout.options).map((option) => (
																	<div className="ui radio checkbox prabidhik" key={option}>
																		<input
																			type="radio"
																			name={`wallToBorder.${outerLayoutValue}.roadExists`}
																			value={option}
																			defaultChecked={
																				getIn(props.values, `wallToBorder.${outerLayoutValue}.roadExists`) ===
																				option
																			}
																			onChange={props.handleChange}
																		/>
																		<label>{`${option}`}</label>
																	</div>
																))}
															</TableCell>
															<TableCell>
																{Object.values(outerLayout.options).map((option) => (
																	<div className="ui radio checkbox prabidhik" key={option}>
																		<input
																			type="radio"
																			name={`wallToBorder.${outerLayoutValue}.windowDoorExists`}
																			value={option}
																			defaultChecked={
																				getIn(
																					props.values,
																					`wallToBorder.${outerLayoutValue}.windowDoorExists`
																				) === option
																			}
																			onChange={props.handleChange}
																		/>
																		<label>{`${option}`}</label>
																	</div>
																))}
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `wallToBorder.${outerLayoutValue}.minimumSpacing`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`wallToBorder.${outerLayoutValue}.minimumSpacing`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `wallToBorder.${outerLayoutValue}.minimumSpacing`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `wallToBorder.${outerLayoutValue}.minimumSpacing`)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `wallToBorder.${outerLayoutValue}.existsingSpacing`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`wallToBorder.${outerLayoutValue}.existsingSpacing`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `wallToBorder.${outerLayoutValue}.existsingSpacing`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `wallToBorder.${outerLayoutValue}.existsingSpacing`)}
																		</div>
																	)}
																</div>
															</TableCell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_26.mapTech_26_title}</span>
										<div className="inline field" style={{ marginLeft: '25px' }}>
											{`${mapTech_data.mapTech_26.mapTech_26_subtitle} : `}
											<DashedLangInput 
												name="publicPropertyName"
												value="publicPropertyName"
												setFieldValue={props.setFieldValue}
												handleChange={props.handleChange}
												error={props.errors.publicPropertyName}
												value={props.values.publicPropertyName}
											/>
											<SetbackSelect
												options={otherSetBackOption}
												name="publicPropertyName"
												unitField="publicPropertyUnit"
												setbackField="publicPropertyRequiredDistance"
												setbackMaster={otherSetbackMaster}
												error={!!props.errors.publicPropertyName}
											/>
										</div>
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<Table celled compact collapsing>
											<Table.Body>
												{Object.keys(lengthtoLeave.mapTech_26_table_subheading).map((key, index) => {
													// lengthtoLeaveValue++;
													const suffix = index === 0 ? 'RequiredDistance' : 'Distance';
													return (
														<Table.Row key={index}>
															<TableCell>
																<label htmlFor={`publicProperty${suffix}`}>
																	{lengthtoLeave.mapTech_26_table_subheading[key]}
																	{` ( `}
																	<Dropdown
																		name="publicPropertyUnit"
																		onChange={(e, { value }) => {
																			// surroundingMappingFlat.forEach((side, index) => {
																			const reqOther = otherSetbackMaster.find(
																				(setother) =>
																					setother.placeName === getIn(props.values, `publicPropertyName`)
																			);
																			const prevUnit = getIn(props.values, 'publicPropertyUnit');
																			if (reqOther) {
																				if (value === 'METRE') {
																					props.setFieldValue(
																						`publicPropertyRequiredDistance`,
																						reqOther.setBackMeter
																					);
																				} else {
																					props.setFieldValue(
																						`publicPropertyRequiredDistance`,
																						reqOther.setBackFoot
																					);
																				}
																			}
																			// });

																			props.setFieldValue('publicPropertyUnit', value);
																			props.setFieldValue(
																				'publicPropertyDistance',
																				lengthconvertor(
																					getIn(props.values, 'publicPropertyDistance'),
																					value,
																					prevUnit
																				)
																			);
																		}}
																		value={getIn(props.values, 'publicPropertyUnit')}
																		options={distanceOptions}
																	/>
																	{' ) '}
																</label>
															</TableCell>
															<TableCell>
																<TableInputField
																	name={`publicProperty${suffix}`}
																	onChange={
																		index === 1 ||
																		!this.state.hasOtherSetbackMaster ||
																		props.values.publicPropertyName === mapTech_data.mapTech_23.defaultNadiOption
																			? props.handleChange
																			: undefined
																	}
																	readOnly={
																		index === 1 ||
																		!this.state.hasOtherSetbackMaster ||
																		props.values.publicPropertyName === mapTech_data.mapTech_23.defaultNadiOption
																			? false
																			: true
																	}
																	value={getIn(props.values, `publicProperty${suffix}`)}
																	error={getIn(props.errors, `publicProperty${suffix}`)}
																	validate={validateNullableNUmberField}
																/>
																{/* <TableInput
																		name={`publicProperty${suffix}`}
																		value={getIn(props.values, `publicProperty${suffix}`)}
																		onChange={index === 1 ? props.handleChange : undefined}
																		error={getIn(props.errors, `publicProperty${suffix}`)}
																	/> */}
															</TableCell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_27}</span>
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<Table celled compact collapsing>
											<Table.Header>
												<Table.Row>
													{Object.keys(cantiliver.mapTech_27_table_heading).map((key, index) => (
														<Table.HeaderCell key={key}>
															{`${cantiliver.mapTech_27_table_heading[key]} `}
															{index !== 0 && index !== 5 && (
																<UnitDropdownWithRelatedFields
																	unitName="cantileverUnit"
																	options={distanceOptions}
																	relatedFields={getRelatedFields(
																		'',
																		Object.keys(cantiliver.mapTech_27_table_heading),
																		'cantilever',
																		['frontSide', 'backSide', 'rightSide', 'leftSide']
																	)}
																/>
															)}
														</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{Object.keys(cantiliver.mapTech_27_table_subheading).map((key, cantiliverValue) => {
													// cantiliverValue++;
													return (
														<Table.Row key={cantiliverValue}>
															<TableCell>
																<label htmlFor={`cantilever[${cantiliverValue}]`}>
																	{cantiliver.mapTech_27_table_subheading[key]}
																</label>
																{/*<input id={`floor[${floorIndex - 1}].floor`} name={`floor[${floorIndex - 1}].floor`} hidden value={`floor[${floorIndex - 1}].floor`}/>*/}
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `cantilever.${cantiliverValue}.frontSide`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`cantilever.${cantiliverValue}.frontSide`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `cantilever.${cantiliverValue}.frontSide`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `cantilever.${cantiliverValue}.frontSide`)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `cantilever.${cantiliverValue}.backSide`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`cantilever.${cantiliverValue}.backSide`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `cantilever.${cantiliverValue}.backSide`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `cantilever.${cantiliverValue}.backSide`)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `cantilever.${cantiliverValue}.rightSide`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`cantilever.${cantiliverValue}.rightSide`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `cantilever.${cantiliverValue}.rightSide`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `cantilever.${cantiliverValue}.rightSide`)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<div
																	className={
																		getIn(props.errors, `cantilever.${cantiliverValue}.leftSide`)
																			? 'error field'
																			: 'field'
																	}
																>
																	<FormikInput
																		name={`cantilever.${cantiliverValue}.leftSide`}
																		handleChange={props.handleChange}
																	/>
																	{getIn(props.errors, `cantilever.${cantiliverValue}.leftSide`) && (
																		<div class="ui pointing above prompt label">
																			{getIn(props.errors, `cantilever.${cantiliverValue}.leftSide`)}
																		</div>
																	)}
																</div>
															</TableCell>
															<TableCell>
																<EbpsFormInline
																	name={`cantilever.${cantiliverValue}.remarks`}
																	setFieldValue={props.setFieldValue}
																	value={getIn(props.values, `cantilever.${cantiliverValue}.remarks`)}
																	error={props.errors[`cantilever.${cantiliverValue}.remarks`]}
																/>
															</TableCell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</div>
									<div className="inline field">
										<span>{mapTech_data.mapTech_28}</span>
									</div>
									<div
										style={{
											marginBottom: '10px',
										}}
									>
										<Table celled compact collapsing>
											<Table.Body>
												{Object.keys(hitensionline.mapTech_28_table_subheading).map((key, hitensionlineValue) => {
													// hitensionlineValue++;
													return (
														<Table.Row key={hitensionlineValue}>
															<TableCell>
																<label htmlFor={`highTension[${hitensionlineValue}]`}>
																	{hitensionline.mapTech_28_table_subheading[key]}{' '}
																	<UnitDropdownWithRelatedFields
																		unitName="highTensionUnit"
																		options={distanceOptions}
																		relatedFields={getRelatedFields(
																			'',
																			Object.keys(hitensionline.mapTech_28_table_subheading),
																			'highTension',
																			['value']
																		)}
																	/>{' '}
																</label>
															</TableCell>
															<TableCell>
																<TableInputField
																	name={`highTension.${hitensionlineValue}.value`}
																	onChange={props.handleChange}
																	value={getIn(props.values, `highTension.${hitensionlineValue}.value`)}
																	error={getIn(props.errors, `highTension.${hitensionlineValue}.value`)}
																	// validate={validateNumberField}
																/>
																{/* <div className={getIn(props.errors, `highTension.${hitensionlineValue}.value`) ? 'error field' : 'field'}>
																		<FormikInput name={`highTension.${hitensionlineValue}.value`} handleChange={props.handleChange} />
																		{getIn(props.errors, `highTension.${hitensionlineValue}.value`) && (
																			<div class="ui pointing above prompt label">
																				{getIn(props.errors, `highTension.${hitensionlineValue}.value`)}
																			</div>
																		)}
																	</div> */}
															</TableCell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>
									</div>
									<div
										style={{
											marginBottom: '10px',
											pageBreakInside: 'avoid',
										}}
									>
										<Table celled>
											{/* <Table celled style={{pageBreakBefore : 'always'}}> */}
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>
														{mapTech_data.maptech_person_table.maptech_person_table_header_1}
													</Table.HeaderCell>
													<Table.HeaderCell>
														{mapTech_data.maptech_person_table.maptech_person_table_header_2}
													</Table.HeaderCell>
												</Table.Row>
											</Table.Header>

											<Table.Body>
												<Table.Row>
													<Table.Cell verticalAlign="top">
														<span>{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_1}</span>
														<span className="ui input dashedForm-control">{permitData.nibedakName}</span>
														<span>{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_2}</span>
														<br />
														<span>{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_3}</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_4}
															{initVal1.userName}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_5}
															{initVal1.educationQualification}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_6}
															{initVal1.consultancyName}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_7}
															{initVal1.municipalRegNo}
														</span>
														<br />
														<SignatureImage
															label={mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_8}
															showSignature={true}
															value={initVal1.stamp}
														/>
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_1.cell_1_data_9}
															{initVal1.enterDate}
														</span>
													</Table.Cell>
													<Table.Cell verticalAlign="top">
														<div>{mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_1}</div>
														<SignatureImage
															showSignature={true}
															label={mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_2}
															value={staticFiles.ghardhaniSignature}
														/>
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_3}
															{permitData.nibedakName}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_4}
															{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_5}
															{translateDate(permitData.applicantDateBS)}
														</span>
														<br />
														<span>
															{mapTech_data.maptech_person_table.maptech_person_table_cell_2.cell_2_data_6}
															{permitData.applicantMobileNo}
														</span>
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
									</div>
									{/* </div> */}
								</div>
								{/* </FormWrapper> */}
								<br />
								<SaveButtonValidation
									errors={props.errors}
									validateForm={props.validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={props.handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const FormHeading = () => (
	<>
		<SectionHeader>
			<h3 className="top-margin">{mapTech_data.mapTech_subject}</h3>
		</SectionHeader>
	</>
);

const 	MapTechnicalDescriptionView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: true,
			},
			{
				api: api.anusuchiGha,
				objName: 'anuGha',
				form: false,
			},
			{
				api: api.otherSetBack,
				objName: 'otherSetBack',
				form: false,
				utility: true,
			},
			{
				api: api.roadSetBack,
				objName: 'roadSetBack',
				form: false,
				utility: true,
			},
			{ api: api.buildPermit, objName: 'permitEdit', form: false },
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value'],
			param0: ['getElementsByTagName', 'span', 'innerText'],
			param1: [PrintIdentifiers.CHECKBOX_LABEL],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		parentProps={parentProps}
		checkPreviousFiles={true}
		useInnerRef={true}
		render={(props) => <MapTechnicalDescriptionViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default MapTechnicalDescriptionView;
