import React, { Component } from 'react';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../utils/paramUtil';
import api from '../../../utils/api';
import { Formik } from 'formik';
import { Form, Table, TextArea } from 'semantic-ui-react';
import { SectionHeader } from '../../uiComponents/Headers';
import { earthQuakeSafetyNoObjectionSheet } from '../../../../src/utils/data/EarthquakeSafetyNoObjectionSheetData';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { DashedDateField } from '../../shared/DateField';
import { TableInputField } from '../../shared/TableInput';
import { FooterSignatureMultiline } from './formComponents/FooterSignature';
import { prepareMultiInitialValues, handleSuccess, checkError, getJsonData } from '../../../utils/dataUtils';
import { validateNullableNormalNumber, validateNullableOfficialNumbers } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { DashedNormalInputIm } from '../../shared/DashedFormInput';
import { getCurrentDate } from '../../../utils/dateUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import SaveButtonValidation from '../../shared/SaveButtonValidation';

const requiredData = earthQuakeSafetyNoObjectionSheet;
const initialValues = {
	applicantDateBS: getCurrentDate(false),
	firstRowSn: '',
	firstRowComments: '',
	firstRowRemarks: '',
	secondRowSn: '',
	SecondRowComments: '',
	SecondRowRemarks: '',
	thirdRowSn: '',
	thirdRowComments: '',
	thirdRowRemarks: '',
	fourthRowSn: '',
	fourthRowComments: '',
	fourthRowRemarks: '',
	fifthRowSn: '',
	fifthRowComments: '',
	fifthRowRemarks: '',
	sixthRowSn: '',
	sixthRowComments: '',
	sixthRowRemarks: '',
	seventhRowSn: '',
	seventhRowComments: '',
	seventhRowRemarks: '',
	secondTable1strow1stcolumn: '',
	secondTable1strow2ndcolumn: '',
	secondTable1strow3rdcolumn: '',
	secondTable2ndrow1stcolumn: '',
	secondTable2ndrow2ndcolumn: '',
	secondTable2ndrow3rdcolumn: '',
	secondTable3rdrow1stcolumn: '',
	secondTable3rdrow2ndcolumn: '',
	secondTable3rdrow3rdcolumn: '',
	secondTable4throw1stcolumn: '',
	secondTable4throw2ndcolumn: '',
	secondTable4throw3rdcolumn: '',
	secondTable5throw1stcolumn: '',
	secondTable5throw2ndcolumn: '',
	secondTable5throw3rdcolumn: '',
	secondTabl6throw1stcolumn: '',
	secondTable6throw2ndcolumn: '',
	secondTable6throw3rdcolumn: '',
	recommendations: '',
	regNo: '',
};
const validation = Yup.object().shape({
	firstRowSn: validateNullableNormalNumber,
	secon: validateNullableNormalNumber,
	thirdRowSn: validateNullableNormalNumber,
	fourthRowSn: validateNullableNormalNumber,
	fifthRowSn: validateNullableNormalNumber,
	sixthRowSn: validateNullableNormalNumber,
	seventhRowSn: validateNullableNormalNumber,
	regNo: validateNullableOfficialNumbers,
});
class EarthquakeSafetyNoObjectionSheetComponent extends Component {
	constructor(props) {
		super(props);

		const initVal = prepareMultiInitialValues(
			{
				obj: initialValues,
				reqFields: [],
			},
			{ obj: getJsonData(this.props.prevData), reqFields: [] }
		);
		this.state = {
			initVal,
		};
	}

	render() {
		const { errors, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, prevData } = this.props;
		const { initVal } = this.state;
		return (
			<div>
				{errors && <ErrorDisplay message={errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={validation}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.EarthquakeSafetyNoObjectionSheet, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleSubmit, handleChange, values, errors, setFieldValue, validateForm }) => (
						<div>
							<Form>
								<div ref={this.props.setRef}>
									<SectionHeader>
										<h1>{requiredData.biratnagarMetroPolitan}</h1>
										<h2>{requiredData.earthquakeSafetySection}</h2>
									</SectionHeader>
									<FlexSingleRight>
										{requiredData.regNo}
										<DashedNormalInputIm name="regNo" value={values.regNo} error={errors.regNo} handleChange={handleChange} />
									</FlexSingleRight>
									<SectionHeader>
										<h2 className="underline">{requiredData.noObjectionSheet}</h2>
										<h3>{requiredData.forOfficialUse}</h3>
									</SectionHeader>
									{requiredData.commentsOnStructuralDrawings}
									<Table border="1px solid black">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width="2">{requiredData.sNo}</Table.HeaderCell>
												<Table.HeaderCell>{requiredData.commentsSuggestions}</Table.HeaderCell>
												<Table.HeaderCell>{requiredData.remarks}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{/* row1 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField
														name="firstRowSn"
														value={values.firstRowSn}
														error={errors.firstRowSn}
														// setFieldValue={setFieldValue}
													/>
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="firstRowComments" value={values.firstRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="firstRowRemarks" value={values.firstRowRemarks} />
												</Table.Cell>
											</Table.Row>
											{/* row2 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="seconRowSn" value={values.seconRowSn} error={errors.seconRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="SecondRowComments" value={values.SecondRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="SecondRowRemarks" value={values.SecondRowRemarks} />
												</Table.Cell>
											</Table.Row>
											{/* row3 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="thirdRowSn" value={values.thirdRowSn} error={errors.thirdRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="thirdRowComments" value={values.thirdRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField
														name="thirdRowRemarks"
														value={values.thirdRowRemarks}
														error={errors.thirdRowComments}
													/>
												</Table.Cell>
											</Table.Row>
											{/* row4 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="fourthRowSn" value={values.fourthRowSn} error={errors.fourthRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="fourthRowComments" value={values.fourthRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="fourthRowRemarks" value={values.fourthRowRemarks} />
												</Table.Cell>
											</Table.Row>
											{/* row5 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="fifthRowSn" value={values.fifthRowSn} error={errors.fifthRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="fifthRowComments" value={values.fifthRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="fifthRowRemarks" value={values.fifthRowRemarks} />
												</Table.Cell>
											</Table.Row>
											{/* row6 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="sixthRowSn" value={values.sixthRowSn} error={errors.sixthRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="sixthRowComments" value={values.sixthRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="sixthRowRemarks" value={values.sixthRowComments} />
												</Table.Cell>
											</Table.Row>
											{/* row7 */}
											<Table.Row>
												<Table.Cell>
													<TableInputField name="seventhRowSn" value={values.seventhRowSn} error={errors.seventhRowSn} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="seventhRowComments" value={values.seventhRowComments} />
												</Table.Cell>
												<Table.Cell>
													<TableInputField name="seventhRowRemarks" value={values.seventhRowRemarks} />
												</Table.Cell>
											</Table.Row>
										</Table.Body>
									</Table>
									<FlexSingleRight>
										<FooterSignatureMultiline designations={[[requiredData.jrEngineer, requiredData.submittedBy]]} />
									</FlexSingleRight>
									<br />
									<br />
									<br />

									<FlexSingleRight>
										{requiredData.date}
										<DashedDateField name="applicantDateBS" inline={true} compact={true} />
									</FlexSingleRight>
									<Table border="1px solid black">
										{/* row 1 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTable1strow1stcolumn" value={values.secondTable1strow1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable1strow2ndcolumn" value={values.secondTable1strow2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable1strow3rdcolumn" value={values.secondTable1strow3rdcolumn} />
											</Table.Cell>
										</Table.Row>
										{/* row 2 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTable2ndrow1stcolumn" value={values.secondTable2ndrow1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable2ndrow2ndcolumn" value={values.secondTable2ndrow2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable2ndrow3rdcolumn" value={values.secondTable2ndrow3rdcolumn} />
											</Table.Cell>
										</Table.Row>
										{/* row 3 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTable3rdrow1stcolumn" value={values.secondTable3rdrow1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable3rdrow2ndcolumn" value={values.secondTable3rdrow2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable3rdrow3rdcolumn" value={values.secondTable3rdrow3rdcolumn} />
											</Table.Cell>
										</Table.Row>
										{/* row 4 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTable4throw1stcolumn" value={values.secondTable4throw1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable4throw2ndcolumn" value={values.secondTable4throw2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable4throw3rdcolumn" value={values.secondTable4throw3rdcolumn} />
											</Table.Cell>
										</Table.Row>
										{/* row 5 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTable5throw1stcolumn" value={values.secondTable5throw1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable5throw2ndcolumn" value={values.secondTable5throw2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable5throw3rdcolumn" value={values.secondTable5throw3rdcolumn} />
											</Table.Cell>
										</Table.Row>
										{/* row 6 */}
										<Table.Row>
											<Table.Cell>
												<TableInputField name="secondTabl6throw1stcolumn" value={values.secondTabl6throw1stcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable6throw2ndcolumn" value={values.secondTable6throw2ndcolumn} />
											</Table.Cell>
											<Table.Cell>
												<TableInputField name="secondTable6throw3rdcolumn" value={values.secondTable6throw3rdcolumn} />
											</Table.Cell>
										</Table.Row>
									</Table>
									{requiredData.recommendation}
									<TextArea name="recommendations" value={values.recommendations} onChange={handleChange} />
									<FlexSingleRight>
										<FooterSignatureMultiline designations={[[requiredData.engineer, requiredData.recommendedBy]]} />
										<br />
										<br />
										{requiredData.date}
										<DashedDateField name="applicantDateBS" inline={true} compact={true} />
									</FlexSingleRight>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						</div>
					)}
				</Formik>
			</div>
		);
	}
}
const EarthquakeSafetyNoObjectionSheet = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.EarthquakeSafetyNoObjectionSheet).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <EarthquakeSafetyNoObjectionSheetComponent {...props} parentProps={parentProps} />}
	/>
);
export default EarthquakeSafetyNoObjectionSheet;
