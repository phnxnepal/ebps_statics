import React, { Component } from 'react';
import { Tab, List, Form } from 'semantic-ui-react';
import { DashedLangInput } from '../../shared/DashedFormInput';

import { GharSurjaminData } from '../../../utils/data/GharSurjaminData';
import { Formik, getIn } from 'formik';
import api from '../../../utils/api';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import * as Yup from 'yup';
import { EbpsTextArea } from '../../shared/EbpsForm';
import GharNakshaSurjaminMuchulkaPrintView from './GharNakshaSurjaminprintView';
import { DashedMultiUnitLengthInput } from '../../shared/EbpsUnitLabelValue';
import { DashedMultiUnitAreaInput } from './../../shared/EbpsUnitLabelValue';
import { GenericApprovalFileView } from '../../shared/file/GenericApprovalFileView';
import { FormUrlFull } from '../../../utils/enums/url';
import { isBirtamod } from '../../../utils/clientUtils';
import {
	NeighbourWitnessSection,
	SurroundingSection,
	AbsentSurroundingSection,
	SurroundingSectionParagraph,
} from './gharNaksaComponents/GharNaksaComponents';
import { footerInputSignature } from '../../../utils/data/genericFormData';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';

const options = [
	{ key: 'a', text: 'फिट', value: 'FEET' },
	{ key: 'b', text: 'मिटर', value: 'METRE' },
];

const GSData = GharSurjaminData.structureDesign;

const schema = Yup.object().shape({
	// uploadFile: validateFile,
	// gharnakshaSurjaminMuchulka: Yup.string().required('Required')
});
class GharNakshaSurjaminMuchulkaViewComponent extends Component {
	constructor(props) {
		super(props);

		const { permitData, prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		const surjaminMuchulka = getJsonData(otherData.surjaminMuchulka);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					munWard: permitData.newWardNo,
					marga: permitData.buildingJoinRoad,
					eastwestMeter: 'METRE',
					eastwestMeter1: 'METRE',
					southNorthMeter: 'METRE',
					southNorthMeter1: 'METRE',
					height1: 'METRE',
					aminName: surjaminMuchulka ? surjaminMuchulka.aminName : '',
					prashashanName: surjaminMuchulka ? surjaminMuchulka.prashashanName : '',
				},
				reqFields: [],
			},
			// { obj: permitData, reqFields: ['surrounding'] },
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = { initialValues };
	}

	render() {
		const { initialValues } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;

		const panes = props => {
			return [
				{
					menuItem: `${GSData.tab_1}`,
					render: () => (
						<Tab.Pane attached={false}>
							<Tab1 permitData={permitData} userData={userData} {...props} />
						</Tab.Pane>
					),
				},
				{
					menuItem: `${GSData.tab_2}`,
					render: () => (
						<Tab.Pane attached={false}>
							<Tab2 permitData={permitData} userData={userData} {...props} />
						</Tab.Pane>
					),
				},
			];
		};
		// console.log(permitData);

		// console.log(userData);
		return (
			<>
				<div style={{ display: 'none' }}>
					<div ref={this.props.setRef}>
						<GharNakshaSurjaminMuchulkaPrintView />
					</div>
				</div>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						try {
							await this.props.postAction(`${api.gharnakshaSurjaminMuchulka}`, values, true);

							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//     getNextUrl(
								//         this.props.parentProps.location.pathname
								//     )
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div>
								<div className="NJ-header Tabs">
									<Tab
										menu={{
											borderless: true,
											attached: false,
											tabular: false,
										}}
										panes={panes({
											handleChange,
											values,
											errors,
											setFieldValue,
											handleSubmit,
										})}
									/>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const Tab1 = props => {
	const permitData = props.permitData;
	const userData = props.userData;
	// console.log(props);
	return (
		<div className="NJ-Main">
			<h2 className="NJ-title">{GSData.tab1.title}</h2>
			<p style={{ textAlign: 'justify' }}>
				{GSData.tab1.line1} <span>{userData.organization.name}</span>
				{','} {GSData.tab1.line2}
				<DashedLangInput
					name="aminName"
					setFieldValue={props.setFieldValue}
					value={props.values.aminName}
					error={props.errors.aminName}
				/>{' '}
				{GSData.tab1.line3}
				<DashedLangInput
					name="prashashanName"
					setFieldValue={props.setFieldValue}
					value={props.values.prashashanName}
					error={props.errors.prashashanName}
				/>{' '}
				{GSData.tab1.line4} <span>{userData.organization.name}</span> {GSData.tab1.wardNo}
				<DashedLangInput name="munWard" setFieldValue={props.setFieldValue} value={props.values.munWard} error={props.errors.munWard} />
				{GSData.tab1.line5} <span>{userData.organization.name}</span>,{GSData.tab1.wardNo} <span>{permitData.nibedakTol}</span>{' '}
				{GSData.tab1.line6} <span>{permitData.nibedakName}</span> {GSData.tab1.line6_1} <span>{permitData.applicantName}</span>
				{GSData.tab1.line7} <span>{permitData.oldMunicipal}</span> {GSData.tab1.wardNo} <span>{permitData.oldWardNo}</span>
				{GSData.tab1.line8} <span>{userData.organization.name}</span> {GSData.tab1.wardNo} <span>{permitData.newWardNo}</span>{' '}
				{GSData.tab1.line9} {permitData.sadak} {GSData.tab1.toll}
				{/* <Input type="text" Placeholder="marga"></Input> */}
				<DashedLangInput name="marga" setFieldValue={props.setFieldValue} value={props.values.marga} error={props.errors.marga} />
				{GSData.tab1.line10} <SurroundingSectionParagraph setFieldValue={props.setFieldValue} values={props.values} errors={props.errors} />
				{GSData.tab1.line11} <span>{permitData.kittaNo}</span> {GSData.tab1.line12}
				<span>
					{permitData.landArea} {permitData.landAreaType}
				</span>
				{GSData.tab1.line12_1}
				<DashedMultiUnitAreaInput
					name="eastwestMeter_input"
					unitName="eastwestMeter"
					label={GSData.tab1.line12_2}
					squareOptions={options}
					relatedFields={['southNorthMeter_input', 'eastwestMeter1_input', 'southNorthMeter1_input', 'height1_input']}
				/>
				<DashedMultiUnitLengthInput
					name="southNorthMeter_input"
					unitName="eastwestMeter"
					relatedFields={['eastwestMeter1_input', 'southNorthMeter1_input', 'height1_input']}
					areaField={'eastwestMeter_input'}
					label={GSData.tab1.line13}
				/>
				<span>{GSData.tab1.line14}</span>{' '}
				<DashedMultiUnitLengthInput
					name="eastwestMeter1_input"
					unitName="eastwestMeter"
					relatedFields={['southNorthMeter_input', 'southNorthMeter1_input', 'height1_input']}
					areaField={'eastwestMeter_input'}
					label={GSData.tab1.line14_1}
				/>
				<DashedMultiUnitLengthInput
					name="southNorthMeter1_input"
					unitName="eastwestMeter"
					relatedFields={['southNorthMeter_input', 'eastwestMeter1_input', 'height1_input']}
					areaField={'eastwestMeter_input'}
					label={GSData.tab1.line15}
				/>
				<DashedMultiUnitLengthInput
					name="height1_input"
					unitName="eastwestMeter"
					relatedFields={['southNorthMeter_input', 'eastwestMeter1_input', 'southNorthMeter1_input']}
					areaField={'eastwestMeter_input'}
					label={GSData.tab1.line16}
				/>
				{GSData.tab1.line17}
				<span>{userData.organization.name}</span> {/* <Input type="text" name=""></Input> */}
				<DashedLangInput name="karalaya" setFieldValue={props.setFieldValue} value={props.values.karalaya} error={props.errors.karalaya} />
				{GSData.tab1.line18}
			</p>
			<h2 className="NJ-title">
				<u>{GSData.tab1.tapasil}</u>
			</h2>
			<p>{GSData.tab1.line19}</p>
			<EbpsTextArea name="uttar" onChange={props.handleChange} setFieldValue={props.setFieldValue} value={props.values.uttar} />
		</div>
	);
};

const Tab2 = props => {
	// const permitData = props.permitData;
	const userData = props.userData;
	return (
		<div className="NJ-Main">
			<SurroundingSection setFieldValue={props.setFieldValue} values={props.values} errors={props.errors} />
			<br />
			<NeighbourWitnessSection userData={userData} setFieldValue={props.setFieldValue} values={props.values} errors={props.errors} />
			<br />
			{!isBirtamod && (
				<>
					<AbsentSurroundingSection setFieldValue={props.setFieldValue} values={props.values} errors={props.errors} />

					<h3 className="NJ-title" style={{ textAlign: 'left' }}>
						{GSData.tab2.subheading.subheading4}
					</h3>
					<br />
				</>
			)}
			<h2 className="NJ-title" style={{ textAlign: 'center' }}>
				{GSData.tab2.subheading.subheading6}
			</h2>
			<List>
				<List.Item>
					<span>{GSData.tab2.content.signature}</span>
					<span type="text" name="signs1" className=" ui input dashedForm-control" />
					<span>
						{GSData.tab2.content.content5}
						<DashedLangInput
							name="sWadaNo"
							setFieldValue={props.setFieldValue}
							value={props.values.sWadaNo}
							error={props.errors.sWadaNo}
						/>
						{GSData.tab2.content.content7}
					</span>
					<DashedLangInput
						name="sWadaPerson"
						setFieldValue={props.setFieldValue}
						value={props.values.sWadaPerson}
						error={props.errors.sWadaPerson}
					/>
					<br />
				</List.Item>

				<List.Item>
					<span>{GSData.tab2.content.signature}</span>
					<span type="text" name="signs2" className=" ui input dashedForm-control" />
					<span>{GSData.tab2.content.content8}</span>
					<DashedLangInput
						name="sWadaKaryalaya"
						setFieldValue={props.setFieldValue}
						value={props.values.sWadaKaryalaya}
						error={props.errors.sWadaKaryalaya}
					/>
					<span>{GSData.tab2.content.content8_1}</span>
					<DashedLangInput
						name="sWadaSachib"
						setFieldValue={props.setFieldValue}
						value={props.values.sWadaSachib}
						error={props.errors.sWadaSachib}
					/>
					<br />
				</List.Item>
			</List>
			<br />

			<h2 className="NJ-title" style={{ textAlign: 'left', textDecoration: 'underline' }}>
				{GSData.tab2.subheading.subheading5}
			</h2>

			<List>
				<List.Item>
					<span>{GSData.tab2.content.signature}</span>
					<span type="text" name="signs3" className=" ui input dashedForm-control" />
					<span>{GSData.tab2.content.content9}</span>
					<DashedLangInput
						name="prabidhikKarma"
						setFieldValue={props.setFieldValue}
						value={props.values.prabidhikKarma}
						error={props.errors.prabidhikKarma}
					/>
					<span>{GSData.tab2.content.content9_1}</span>
					<DashedLangInput
						name="prabidhikPad"
						setFieldValue={props.setFieldValue}
						value={props.values.prabidhikPad}
						error={props.errors.prabidhikPad}
					/>
					<br />
				</List.Item>

				<List.Item>
					<span>{GSData.tab2.content.signature}</span>
					<span type="text" name="signs4" className=" ui input dashedForm-control" />
					<span>{GSData.tab2.content.content10}</span>
					<DashedLangInput
						name="prashasanikKarma"
						setFieldValue={props.setFieldValue}
						value={props.values.prashasanikKarma}
						error={props.errors.prashasanikKarma}
					/>
					<span>{GSData.tab2.content.content9_1}</span>
					<DashedLangInput
						name="prashasanikPad"
						setFieldValue={props.setFieldValue}
						value={props.values.prashasanikPad}
						error={props.errors.prashasanikPad}
					/>
					<br />
				</List.Item>
			</List>
			<br />
			{isBirtamod && (
				<>
					{' '}
					<div className="section-header">
						<p className="left-align underline">{footerInputSignature.talmelGarneKoDastakhat}: </p>
					</div>
					<div>
						{footerInputSignature.name}{' '}
						<DashedLangInput
							name="approveName"
							setFieldValue={props.setFieldValue}
							value={getIn(props.values, 'approveName')}
							error={getIn(props.errors, 'approveName')}
						/>{' '}
						{footerInputSignature.pad}{' '}
						<DashedLangInput
							name="approveDesignation"
							setFieldValue={props.setFieldValue}
							value={getIn(props.values, 'approveDesignation')}
							error={getIn(props.errors, 'approveDesignation')}
						/>{' '}
					</div>{' '}
					<div>
						{footerInputSignature.name}{' '}
						<DashedLangInput
							name="approveName2"
							setFieldValue={props.setFieldValue}
							value={getIn(props.values, 'approveName2')}
							error={getIn(props.errors, 'approveName2')}
						/>{' '}
						{footerInputSignature.pad}{' '}
						<DashedLangInput
							name="approveDesignation2"
							setFieldValue={props.setFieldValue}
							value={getIn(props.values, 'approveDesignation2')}
							error={getIn(props.errors, 'approveDesignation2')}
						/>{' '}
					</div>
					<br />
				</>
			)}
			<div style={{ textAlign: 'justify' }}>
				<span>
					{GSData.tab2.subcontent.sub1}
					<DashedLangInput
						name="muchulkaSaal"
						setFieldValue={props.setFieldValue}
						value={props.values.muchulkaSaal}
						error={props.errors.muchulkaSaal}
					/>
					{GSData.tab2.subcontent.sub2}
					<DashedLangInput
						name="muchulkaMahina"
						setFieldValue={props.setFieldValue}
						value={props.values.muchulkaMahina}
						error={props.errors.muchulkaMahina}
					/>
					{GSData.tab2.subcontent.sub3}
					<DashedLangInput
						name="muchulkaGate"
						setFieldValue={props.setFieldValue}
						value={props.values.muchulkaGate}
						error={props.errors.muchulkaGate}
					/>
					{GSData.tab2.subcontent.sub4}
					<DashedLangInput
						name="muchulkaRoz"
						setFieldValue={props.setFieldValue}
						value={props.values.muchulkaRoz}
						error={props.errors.muchulkaRoz}
					/>
					{GSData.tab2.subcontent.sub5}
					<DashedLangInput
						name="muchulkaSubham"
						setFieldValue={props.setFieldValue}
						value={props.values.muchulkaSubham}
						error={props.errors.muchulkaSubham}
					/>
					{GSData.tab2.subcontent.sub6}{' '}
				</span>
				<br />
			</div>
		</div>
	);
};

const GharNaksaApproveView = props => {
	return (
		<div ref={props.setRef}>
			<GenericApprovalFileView
				fileCategories={props.fileCategories}
				files={props.files}
				url={FormUrlFull.GHAR_NAKSA_SURJAMIN}
				prevData={checkError(props.prevData)}
			/>
		</div>
	);
};

const GharNakshaSurjaminMuchulkaView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: true,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'surjaminMuchulka',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		hasFileView={true}
		render={props =>
			props.hasSavePermission ? (
				<GharNakshaSurjaminMuchulkaViewComponent {...props} parentProps={parentProps} />
			) : (
					<GharNaksaApproveView {...props} parentProps={parentProps} />
				)
		}
	/>
);

export default GharNakshaSurjaminMuchulkaView;
