import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid, Select } from 'semantic-ui-react';
import { CompactDashedLangDate } from '../../shared/DateField';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { getSalutation } from '../../../utils/clientConfigs/sansodhitSuperStructure';
import { SectionHeader } from '../../uiComponents/Headers';
import { FlexSingleLeft } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { MyadRequest } from '../../../utils/data/MyadAddData';
import { ImageDisplayInline } from '../../shared/file/FileView';
import { showToast } from '../../../utils/functionUtils';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';

const myad = MyadRequest.Myad_data;
const opt = [
    { key: 'mero', value: 'मेरो', text: 'मेरो' },
    { key: 'hamro', value: 'हाम्रो', text: 'हाम्रो' },
];
const optSecond = [
    { key: 'first', value: 'प्रथम', text: 'प्रथम' },
    { key: 'second', value: 'दोस्रो', text: 'दोस्रो' },
];

const getMs = value => {
    const ret = opt.filter(row => row.value === value);
    return ret[0] ? ret[0].key : 'mero';
};
const MyadRequestSchema = Yup.object().shape(
    Object.assign({
        supStrucDate: validateNullableNepaliDate,
        allowanceDate: validateNullableNepaliDate,
    })
);
class MyadAdd extends Component {
    constructor(props) {
        super(props);
        const { prevData, orgCode, userData, otherData } = props;
        const json_data = getJsonData(prevData);
        const initialValues = prepareMultiInitialValues(
            //prev values
            { obj: getJsonData(otherData.superStructure), reqFields: ['supStrucDate'] },
            { obj: getJsonData(otherData.allowancePaper), reqFields: ['allowanceDate'] },
            { obj: json_data, reqFields: [] }
        );

        const { salutations } = getSalutation(orgCode, userData);

        this.state = { initialValues: { mero: opt[0].value, first: optSecond[0].value, ...initialValues }, salutations }
    }
    render() {
        const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
        const { initialValues, salutations } = this.state;
        return (
            <Formik
                initialValues={initialValues}
                validationSchema={MyadRequestSchema}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    values.applicationNo = permitData.applicationNo;
                    values.error && delete values.error;
                    try {
                        await this.props.postAction(`${api.MyadThapRequest}${permitData.applicationNo}`, values);
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        if (this.props.success && this.props.success.success) {
                            handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                        }
                    } catch (err) {
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        showToast('Something went wrong !!');
                    }
                }}
            >
                {({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors }) => (
                    <Form loading={isSubmitting} className="ui form">
                        {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

                        <div ref={this.props.setRef}>
                            <div className="flex-item-space-between margin-bottom">
                                <LetterSalutation lines={salutations} />
                            </div>

                            <SectionHeader>
                                <h3 className="underline end-section">{myad.subject}</h3>
                            </SectionHeader>

                            <div style={{ textAlign: 'justify ' }}>
                                <span className="indent-span">
                                    {myad.content.content_1}
                                </span>
                                <Select
                                    name="mero"
                                    options={opt}
                                    onChange={(e, { value }) => {
                                        setFieldValue('mero', value);
                                    }}
                                    value={values.mero}
                                />
                                {myad.content.content_1_1}
                                {userData.organization.name}
                                {myad.content.wadaNo}{permitData.newWardNo}
                                {myad.content.content_2}{permitData.kittaNo}
                                {myad.content.content_3}{permitData.landArea}{permitData.landAreaType}
                                {myad.content.content_4}
                                {values.first === 'प्रथम' ?
                                    <CompactDashedLangDate
                                        name="allowanceDate"
                                        setFieldValue={setFieldValue}
                                        value={values.allowanceDate}
                                        error={errors.allowanceDate}
                                    /> :
                                    <CompactDashedLangDate
                                        name="supStrucDate"
                                        setFieldValue={setFieldValue}
                                        value={values.supStrucDate}
                                        error={errors.supStrucDate}
                                    />

                                }
                                {myad.content.content_5}
                                <Select
                                    name="first"
                                    options={optSecond}
                                    onChange={(e, { value }) => {
                                        setFieldValue('first', value);
                                    }}
                                    value={values.first}
                                />
                                {myad.content.content_5_1}
                                {myad[getMs(values['mero'])].first}
                            </div>
                            <br />
                            <FlexSingleLeft>
                                <div>{myad.content.content_6}{permitData.applicationNo}</div>
                            </FlexSingleLeft>
                            <br />
                            <div>
                                <Grid columns={2} className="flex-item-flex-start">
                                    <Grid.Row>
                                        <Grid.Column><span style={{ textDecoration: "underline" }}>{myad.footer.footer_1}</span></Grid.Column>
                                        <Grid.Column><span style={{ textDecoration: "underline" }}>{myad.footer.footer_2}</span></Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            {myad.footer.footer_3}
                                            <DashedLangInput
                                                name="naam"
                                                setFieldValue={setFieldValue}
                                                value={values.naam}
                                                error={errors.naam}
                                            />
                                        </Grid.Column>
                                        <Grid.Column>
                                            {myad.footer.footer_4}
                                            <DashedLangInput
                                                name="naamThar"
                                                setFieldValue={setFieldValue}
                                                value={values.naamThar}
                                                error={errors.naamThar}
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            {/* {myad.footer.footer_5}{userData.info.stamp} */}
                                            <span>
                                                <ImageDisplayInline
                                                    label={myad.footer.footer_5}
                                                    alt="stamp"
                                                    src={userData.info.stamp}
                                                />
                                            </span>
                                        </Grid.Column>
                                        <Grid.Column>
                                            {myad.footer.footer_6}
                                            <span className="ui input dashedForm-control" />
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </div>

                        </div>

                        <SaveButtonValidation
                            errors={errors}
                            validateForm={validateForm}
                            formUrl={formUrl}
                            hasSavePermission={hasSavePermission}
                            hasDeletePermission={hasDeletePermission}
                            isSaveDisabled={isSaveDisabled}
                            prevData={checkError(prevData)}
                            handleSubmit={handleSubmit}
                        />
                    </Form>
                )}
            </Formik>
        );
    }
}

const MyadAddRequest = parentProps => (
    <FormContainerV2
        api={[
            {
                api: api.MyadThapRequest,
                objName: 'myadThapnus',
                form: true,
            },
            {
                api: api.superStructureConstruction,
                objName: 'superStructure',
                form: false,
            },
            { api: api.allowancePaper, objName: 'allowancePaper', form: false },

        ]}
        onBeforeGetContent={{
            ...PrintParams.INLINE_FIELD,
            ...PrintParams.TEXT_AREA,
            param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
            param7: ['getElementsByClassName', 'ui checkbox', 'label'],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
        }}
        prepareData={data => data}
        parentProps={parentProps}
        useInnerRef={true}
        render={props => <MyadAdd {...props} parentProps={parentProps} />}
    />
);
export default MyadAddRequest;
