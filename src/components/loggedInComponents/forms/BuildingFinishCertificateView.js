import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { DashedLangInput, DashedNormalInput } from '../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from './../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { isStringEmpty } from './../../../utils/stringUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import { mappingDirection } from '../../../utils/data/BuildingBuildCertificateData';
import EbpsTextareaField from '../../shared/MyTextArea';
import { DashedMultiUnitLengthInput, DashedUnitInput } from '../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { FinishCertificateBlockFloors } from './certificateComponents/CertificateFloorInputs';
import { getConstructionTypeText } from '../../../utils/enums/constructionType';
import { CertificateSubHeading } from './certificateComponents/CertificateComponents';
import { FooterSignature } from './formComponents/FooterSignature';
import { chaBhayekoOptions } from '../../../utils/optionUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { getParagraph } from '../../../utils/clientConfigs/certificate';
import { RenderLayout } from '../../shared/layout/GenericLayout';
import { PrintParams } from '../../../utils/printUtils';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { isKamalamai, isMechi } from '../../../utils/clientUtils';
// import { AllowancePaperData } from '../../../utils/data/AllowancePaperData';
import { KamalamaiCertificateSignature } from './kamalamai/KamalamaiCertificateSignature';

const formData = BuildingFinishCertificateData.formdata;
// const kamalamaiData = AllowancePaperData.kamalamaiData;

class BuildingFinishCertificateView extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, orgCode, userData, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};

		const prabhidik = getJsonData(otherData.prabhidik);
		const dosrocharan = getJsonData(otherData.dosrocharan);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		// const organization = otherData.organization;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['floor', 'photo'],
			},
			{
				obj: {
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
				},
				reqFields: [],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfAreas()),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'purposeOfConstruction', 'buildingHeight'],
			},
			{
				obj: prabhidik,
				reqFields: [
					'constructionType',
					'roofLen',
					'roof',
					'namedMapdanda',
					'namedAdhikar',
					'requiredDistance',
					'sadakAdhikarUnit',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'elecVolt',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'publicPropertyName',
				],
			},
			{
				obj: dosrocharan,
				reqFields: ['roomCount', 'windowCount', 'doorCount', 'shutterCount'],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		initialValues1.constructionType = getConstructionTypeText(initialValues1.constructionType || permitData.constructionType);

		const initVal = merge(initialValues, initialValues1);
		// console.log(initVal);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
			firstParagraph: getParagraph(orgCode, userData, permitData),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray, formattedFloors, firstParagraph } = this.state;
		const user_info = this.props.userData;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} hideLogo={isMechi} />
									<CertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
									/>

									<RenderLayout layout={firstParagraph} values={values} setFieldValue={setFieldValue} />
									<div>
										{formData.fdata1}
										{permitData.nibedakName}
										{formData.address}
										{userData.organization.name} {permitData.nibedakTol} {formData.sadak} {permitData.nibedakSadak}
									</div>
									<div>
										{formData.fdata2}
										<div style={{ marginLeft: '20px' }}>
											{values.surrounding &&
												values.surrounding.map((index, i) => (
													<div key={i}>
														{mappingDirection.find((fl) => fl.direction === index.side).value}{' '}
														<DashedNormalInput
															name={`surrounding.${i}.kittaNo`}
															value={getIn(values, `surrounding.${i}.kittaNo`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.kittaNo`)}
														/>
														{formData.sandhiyar}
														<DashedNormalInput
															name={`surrounding.${i}.sandhiyar`}
															value={getIn(values, `surrounding.${i}.sandhiyar`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.sandhiyar`)}
														/>
													</div>
												))}
										</div>
									</div>
									<div>
										{formData.fdata3}
										<DashedLangInput name="constructionType" setFieldValue={setFieldValue} value={values.constructionType} />
										{formData.fdata4}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
										{formData.fdata5}
										<DashedLangInput name="roofLen" setFieldValue={setFieldValue} value={values.roofLen} />
										{formData.fdata6}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											value={values.purposeOfConstruction}
										/>
									</div>
									<div>
										{formData.fdata7}
										<DashedLangInput name="roomCount" setFieldValue={setFieldValue} value={values.roomCount} />
										{formData.fdata8}
										<DashedLangInput name="windowCount" setFieldValue={setFieldValue} value={values.windowCount} />
										{formData.fdata9}
										<DashedLangInput name="doorCount" setFieldValue={setFieldValue} value={values.doorCount} />
										{formData.fdata10}
										<DashedLangInput name="shutterCount" setFieldValue={setFieldValue} value={values.shutterCount} />
									</div>
									<div>
										{formData.fdata11}
										<DashedLangInput name="karyaDisc" setFieldValue={setFieldValue} value={values.karyaDisc} />
										{formData.fdata12}
										<DashedLangInput name="haalKaryaDics" setFieldValue={setFieldValue} value={values.haalKaryaDics} />
										{formData.fdata12_1}
									</div>

									<DashedMultiUnitLengthInput
										name="namedMapdanda"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedAdhikar']}
										label={formData.fdata13}
									/>

									<DashedMultiUnitLengthInput
										name="namedAdhikar"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedMapdanda']}
										label={formData.fdata14}
									/>
									<DashedMultiUnitLengthInput
										name="requiredDistance"
										unitName="sadakAdhikarUnit"
										relatedFields={['namedAdhikar', 'namedMapdanda']}
										label={formData.fdata15}
									/>
									<br />
									<br />
									<span className="neighbours-para-note" style={{ display: 'flex' }}>
										{formData.seven}
										<div className="div-indent">
											<FinishCertificateBlockFloors floorArray={floorArray} formattedFloors={formattedFloors} />
										</div>
									</span>

									<div>
										{formData.fdata24}
										<DashedLangInput name="coverageDetails" setFieldValue={setFieldValue} value={values.coverageDetails} />
									</div>
									<span className="neighbours-para-note">
										{formData.fdata25}
										<DashedUnitInput
											name="isHighTensionLineDistance"
											// label={formData.fdata25}
											unitName="highTensionLineUnit"
											inline={true}
										/>

										{formData.volt}
										<DashedNormalInput
											name="elecVolt"
											value={values.elecVolt}
											setFieldValue={setFieldValue}
											handleChange={handleChange}
										/>
										{/* <DashedLangInput
											name="elecVolt"
											setFieldValue={setFieldValue}
											inline={true}
											value={values.elecVolt}
											className="dashedForm-control"
										/> */}
									</span>
									<br />
									<span className="neighbours-para-note">
										{formData.fdata26}
										<DashedLangInput name="publicPropertyName" setFieldValue={setFieldValue} value={values.publicPropertyName} />
										<DashedUnitInput name="publicPropertyDistance" label={formData.fdata27} unitName="publicPropertyUnit" />
									</span>

									<div>
										{formData.fdata28}
										<Select
											options={chaBhayekoOptions}
											name="nikash"
											placeholder="छ (भएको)"
											onChange={(e, { value }) => setFieldValue('nikash', value)}
											value={values['nikash']}
											error={errors.nikash}
											// defaultValue={props.values['nikash']}
										/>
									</div>
									<div>
										{formData.fdata29}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
									</div>
									{isKamalamai ? (
										<KamalamaiCertificateSignature images={[]} />
									) : (
										<FooterSignature designations={[formData.fdata30, formData.fdata31, formData.fdata32]} />
									)}
									<br />
									<p className="neighbours-para-note">
										{formData.fdata33}
										<span className="ui input dashedForm-control " />
									</p>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishView = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.DosrocharanPrabidhikView,
				objName: 'dosrocharan',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
			// param7: ["15dayspecial"]
		}}
		hasFile={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateView {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishView;
