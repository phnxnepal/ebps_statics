import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Message, Select } from 'semantic-ui-react';
import { anushuchi_ga } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';
import { getMessage } from '../../../shared/getMessage';
import api from '../../../../utils/api';
import { checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { translateDate } from '../../../../utils/langUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { UserType } from '../../../../utils/userTypeUtils';
import { ImageDisplayInline } from '../../../shared/file/FileView';

const data = anushuchi_ga.anushuchiGaView;
const messageId = 'anusuchi_ga.anushuchiGaView';
const opt = [
	{ key: 'ma', value: 'मैले', text: 'मैले' },
	{ key: 'hami', value: 'हामीले', text: 'हामीले' },
];

const getMs = value => {
	const ret = opt.filter(row => row.value === value);
	return ret[0] ? ret[0].key : 'ma';
};
class AnusuchiGaComponent extends Component {
	constructor(props) {
		super(props);
		let initialValues = {};

		const { userData, prevData } = this.props;

		if (!isEmpty(this.props.prevData)) {
			initialValues = checkError(this.props.prevData);
		}

		let designer_data = {};

		if (!isEmpty(checkError(this.props.prevData))) {
			designer_data = { ...prevData.enterBy, enterDateBs: prevData.enterDateBs };
		} else if (!isEmpty(userData) && userData.userType === UserType.DESIGNER) {
			designer_data = {
				userName: userData.userName,
				educationQualification: userData.info.educationQualification,
				licenseNo: userData.info.licenseNo,
				address: userData.info.address,
				mobile: userData.info.mobile,
				municipalRegNo: userData.info.municipalRegNo,
				signature: userData.info.signature,
				stamp: userData.info.stamp,
				enterDateBs: getCurrentDate(),
			};
		}

		this.state = { initialValues: { applicantMs: opt[0].value, ...initialValues }, designer_data };
	}

	render() {
		const detailed_Info = this.props.permitData;
		const user_info = this.props.userData;
		const building_class = this.props.otherData.designApproval;

		const { initialValues, designer_data } = this.state;
		const { hasDeletePermission, isSaveDisabled, hasSavePermission, formUrl, prevData } = this.props;

		return (
			<div className="anusuchiga-Form">
				{/* <Printthis content={() => this.anushuchiGaPrint.current} /> */}
				<Formik
					initialValues={initialValues}
					onSubmit={async (values, actions) => {
						// console.log('Form values', values);
						actions.setSubmitting(true);

						const datatosend = {};
						datatosend.applicationNo = detailed_Info.applicantNo;

						try {
							await this.props.postAction(`${api.anusuchiGa}${detailed_Info.applicantNo}`, datatosend);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								//   this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								//   this.props.parentProps.history.push(
								//     getNextUrl(this.props.parentProps.location.pathname)
								//   );
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={props => {
						return (
							<Form loading={props.isSubmitting}>
								{!isEmpty(this.props.errors) && (
									<Message negative>
										<Message.Header>Error</Message.Header>
										<p>{this.props.errors.message}</p>
									</Message>
								)}
								<div ref={this.props.setRef}>
									<FormHeading userinfo={user_info} />
									<div>
										{/* <div className="select-forForm"> */}
										<Select
											options={opt}
											name="applicantMs"
											onChange={(e, { value }) => {
												props.setFieldValue('applicantMs', value);
											}}
											value={props.values.applicantMs}
										/>{' '}
										{/* </div> */}
										{getMessage(`${messageId}.ga_data_1`, data.ga_data_1)}
										{data[getMs(props.values['applicantMs'])].first}
										{getMessage(`${messageId}.ga_data_1_1`, data.ga_data_1_1)}
										<span>{user_info.organization.name}</span>
										{getMessage(`${messageId}.ga_data_2`, data.ga_data_2)}
										<span>{detailed_Info.newWardNo}</span>
										{getMessage(`${messageId}.ga_data_3`, data.ga_data_3)}
										<span>{detailed_Info.buildingJoinRoad}</span>
										{getMessage(`${messageId}.ga_data_4`, data.ga_data_4)}
										<span>{detailed_Info.kittaNo}</span>
										{getMessage(`${messageId}.ga_data_5`, data.ga_data_5)}
										<span>
											{detailed_Info.landArea} {detailed_Info.landAreaType}
										</span>
										{getMessage(`${messageId}.ga_data_6`, data.ga_data_6)}
										<span>{detailed_Info.nibedakName}</span>
										{getMessage(`${messageId}.ga_data_7`, data.ga_data_7)}
										<span>{building_class.buildingClass}</span>
										<span>
											{getMessage(`${messageId}.ga_maindata1`, data.ga_maindata1)}

											{data[getMs(props.values['applicantMs'])].fourth}
											{getMessage(`${messageId}.ga_maindata1_1`, data.ga_maindata1_1)}
											{data[getMs(props.values['applicantMs'])].second}
											{getMessage(`${messageId}.ga_maindata2`, data.ga_maindata2)}
											{data[getMs(props.values['applicantMs'])].third}
										</span>
									</div>
									<FormDesignerDetails designerinfo={designer_data} />
								</div>

								<br />
								<SaveButtonValidation
									errors={props.errors}
									validateForm={props.validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={props.handleSubmit}
								/>
								{/* {this.props.hasSavePermission && (
                                    <Button
                                        primary
                                        disabled={
                                            initialValues.erStatus === 'A'
                                        }
                                        onClick={props.handleSubmit}
                                    >
                                        Save
                                    </Button>
                                )} */}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const FormHeading = props => {
	const UserInfoData = props.userinfo;
	return (
		<>
			<h2 style={{ textAlign: 'center' }}>{getMessage(`${messageId}.ga_heading`, data.ga_heading)}</h2>

			<span style={{ textAlign: 'left' }}>
				<p>
					{getMessage(`${messageId}.ga_head`, data.ga_head)}
					<br />
					<span>{UserInfoData.organization.name}</span>
					<br />
					<span>
						{UserInfoData.organization.address}
						{', '}
						{UserInfoData.organization.province}
					</span>
				</p>
			</span>

			<SectionHeader>
				<h3 className="end-section">{getMessage(`${messageId}.ga_title`, data.ga_title)} </h3>
				<h4 className="end-section">{getMessage(`${messageId}.ga_subtitle`, data.ga_subtitle)}</h4>
			</SectionHeader>
		</>
	);
};

const FormDesignerDetails = props => {
	const {
		userName: userName = '',
		educationQualification: educationQualification = '',
		licenseNo: licenseNo = '',
		address: address = '',
		mobile: mobile = '',
		municipalRegNo: municipalRegNo = '',
		signature: signature = '',
		stamp: stamp = '',
	} = { ...props.designerinfo };

	const { enterDateBs = '' } = props.designerinfo && props.designerinfo ? props.designerinfo : {};
	return (
		<>
			<span>
				{getMessage(`${messageId}.ga_designerName`, data.ga_designerName)}
				<span>{userName}</span>
				<br />
				{getMessage(`${messageId}.ga_designerDesignation`, data.ga_designerDesignation)}
				<span>{educationQualification}</span>
				<br />
				{getMessage(`${messageId}.ga_engineeringCouncil`, data.ga_engineeringCouncil)}
				<span>{licenseNo}</span>
				<br />
				{getMessage(`${messageId}.ga_designerAdd`, data.ga_designerAdd)}
				<span>{address}</span>
				<br />
				{getMessage(`${messageId}.ga_designerMobile`, data.ga_designerMobile)}
				<span>{mobile}</span>
				<br />
				{getMessage(`${messageId}.ga_municipalRegNo`, data.ga_municipalRegNo)}
				<span>{municipalRegNo}</span>
				<br />
				<ImageDisplayInline
					label={getMessage(`${messageId}.ga_designerSignature`, data.ga_designerSignature)}
					src={signature}
					alt="signature"
					height={40}
				/>
				{getMessage(`${messageId}.ga_date`, data.ga_date)}
				<span>{translateDate(enterDateBs)}</span>
				<br />
				<br />
				<div>
					<span>
						<ImageDisplayInline
							label={getMessage(`${messageId}.ga_consultancyStamp`, data.ga_consultancyStamp)}
							alt="stamp"
							src={stamp}
						/>
					</span>
				</div>
			</span>
		</>
	);
};

const Anushuchiga = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.anusuchiGa, objName: 'anusuchiGa', form: true },
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={data => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param7: ['getElementsByClassName', 'ui selection dropdown', 'innerText', opt],
		}}
		render={props => <AnusuchiGaComponent {...props} parentProps={parentProps} />}
	/>
);

export default Anushuchiga;
