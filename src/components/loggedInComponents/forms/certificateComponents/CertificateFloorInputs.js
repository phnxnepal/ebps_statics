import React from 'react';
import { mappingLetter, BuildingBuildCertificateData } from '../../../../utils/data/BuildingBuildCertificateData';
import { floorMappingFlat, squareUnitOptions } from '../../../../utils/dataUtils';
import {
	DashedLengthInputWithRelatedUnits,
	DashedAreaInputWithRelatedUnits,
	UnitDropdownWithRelatedFields,
} from '../../../shared/EbpsUnitLabelValue';
import { getUnitValue, getAreaUnitValue } from '../../../../utils/functionUtils';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import { DashedNormalInputIm, DashedLangInput } from '../../../shared/DashedFormInput';
import { Table, Grid } from 'semantic-ui-react';
import { BlockFloorComponents, BlockFloorComponentsV2 } from '../../../shared/formComponents/BlockFloorComponents';
import { buildingFinishCertidicateData } from '../../../../utils/data/BuildingFinishCertificateViewBiratnagarData';
import { floorData } from '../../../../utils/data/genericData';
import { getIn } from 'formik';
import { DashedLangDateField } from '../../../shared/DateField';

const formData = BuildingBuildCertificateData.formdata;
const naaya = BuildingFinishCertificateData.naya;
const inaruwaData = BuildingFinishCertificateData.inaruwa;

export const CertificateFloorInputs = ({ floorArray, floorUnit, buildingArea, buildingHeight, compact }) => {
	return (
		<>
			{floorArray.getLength() > 0 &&
				floorArray.getFloors().map((floor, index) => (
					<div>
						{mappingLetter.find((fl) => fl.letter === index).value} {floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.length`}
							unitName="floorUnit"
							compact={compact}
							relatedFields={[...floorArray.getAllFields(`floor.${index}.length`), 'buildingArea', 'buildingHeight']}
							label={formData.length}
						/>
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.width`}
							unitName="floorUnit"
							compact={compact}
							relatedFields={[...floorArray.getAllFields(`floor.${index}.width`), 'buildingArea', 'buildingHeight']}
							label={formData.breadth}
						/>
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.height`}
							unitName="floorUnit"
							compact={compact}
							relatedFields={[...floorArray.getAllFields(`floor.${index}.height`), 'buildingArea', 'buildingHeight']}
							label={formData.height}
						/>
						<DashedAreaInputWithRelatedUnits
							name={`floor.${index}.area`}
							unitName="floorUnit"
							compact={compact}
							label={formData.area}
							squareOptions={squareUnitOptions}
							relatedFields={[...floorArray.getAllFields(`floor.${index}.area`), 'buildingArea', 'buildingHeight']}
						/>
					</div>
				))}
			{floorArray.getLength() > 0 && (
				<>
					{mappingLetter.find((fl) => fl.letter === floorArray.getLength()).value}
					{formData.fdata22}
					{buildingArea} {`${getAreaUnitValue(floorUnit)}`}
					{formData.fdata23}
					{buildingHeight}
					{` ${getUnitValue(floorUnit)}`}
				</>
			)}
		</>
	);
};

export const FinishCertificateBlockFloors = ({ floorArray, formattedFloors, showAreaColumn = true }) => {
	return (
		<>
			<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
				{(floorObj, block) => {
					const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
					const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
					return (
						<div className="margin-bottom">
							{floorObj.map((floor, index) => (
								<div key={index}>
									{floor.label.sn(index)} {floor.label.floorName}{' '}
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'length')}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.length`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.length}
									/>
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'width')}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.width`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.breadth}
									/>
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'height')}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.height`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.height}
									/>
									<DashedAreaInputWithRelatedUnits
										name={floor.fieldName('floor', 'area')}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.area`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.area}
										squareOptions={squareUnitOptions}
									/>
								</div>
							))}
							{showAreaColumn && floorArray.getLength() > 0 && (
								<>
									{formData.fdata22}
									<DashedAreaInputWithRelatedUnits
										name={buildingArea}
										unitName="floorUnit"
										squareOptions={squareUnitOptions}
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingArea),
										]}
									/>

									{formData.fdata23}
									<DashedLengthInputWithRelatedUnits
										name={buildingHeight}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingHeight),
										]}
									/>
								</>
							)}
							<br />
						</div>
					);
				}}
			</BlockFloorComponents>
		</>
	);
};

export const FinishCertificateBiratnagarBlockFloors = ({ floorArray, formattedFloors, values, errors, setFieldValue }) => {
	const data = BuildingFinishCertificateData.sundarHaraicha;
	const reqData = buildingFinishCertidicateData;

	return (
		<Table className="certificate-borderless-table">
			<Table.Header style={{ verticalAlign: 'top' }}>
				<Table.Row>
					<Table.HeaderCell>{data.floors.gharTalla}</Table.HeaderCell>
					<Table.HeaderCell width="4">
						{reqData.swikritJaggaAnusar}
						{' ('}
						<UnitDropdownWithRelatedFields
							brackets={false}
							unitName={`floorUnit`}
							relatedFields={[...floorArray.getAllFields(), ...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'])]}
							options={squareUnitOptions}
						/>
						{' ) '}
					</Table.HeaderCell>
					<Table.HeaderCell>{reqData.nirmanBhayekoStiti}</Table.HeaderCell>
					<Table.HeaderCell>{reqData.nirmanBhayekoMiti}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				<BlockFloorComponentsV2 floorArray={floorArray} formattedFloors={formattedFloors}>
					{(floorObj, block) => (
						<>
							{floorObj.map((currentRow, idx) => (
								<Table.Row key={`${currentRow.floor}_${block}`}>
									<Table.Cell>{`${currentRow.label.sn(idx)} ${currentRow.label.floorBlockName}`}</Table.Cell>
									<Table.Cell>
										<DashedNormalInputIm name={currentRow.fieldName('floor', 'area')} />
									</Table.Cell>
									<Table.Cell>
										<DashedLangInput
											name={currentRow.fieldName('floor', 'status')}
											value={getIn(values, currentRow.fieldName('floor', 'status'))}
											error={getIn(errors, currentRow.fieldName('floor', 'status'))}
											setFieldValue={setFieldValue}
										/>
									</Table.Cell>
									<Table.Cell>
										<DashedLangDateField
											name={currentRow.fieldName('floor', 'date')}
											value={getIn(values, currentRow.fieldName('floor', 'date'))}
											error={getIn(errors, currentRow.fieldName('floor', 'date'))}
											setFieldValue={setFieldValue}
										/>
									</Table.Cell>
								</Table.Row>
							))}
							<Table.Row>
								<Table.Cell textAlign="left">
									{data.floors.total} {data.area} {block ? `${floorData.block} ${block}` : ''}
								</Table.Cell>
								<Table.Cell>
									<DashedNormalInputIm name={floorArray.getReducedFieldName('buildingArea', block)} />
								</Table.Cell>
								<Table.Cell></Table.Cell>
								<Table.Cell></Table.Cell>
							</Table.Row>
						</>
					)}
				</BlockFloorComponentsV2>
			</Table.Body>
		</Table>
	);
};

export const AllowanceKamalamaiBlockFloors = ({ floorArray, formattedFloors, showAreaColumn = true }) => {
	return (
		<>
			<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
				{(floorObj, block) => {
					const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
					// const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
					return (
						<Grid className="no-padding-grid">
							<Grid.Row columns={2} className="margin-bottom">
								{floorObj.map((floor, index) => (
									<Grid.Column key={index}>
										{floor.label.sn(index)} {floor.label.floorName}{' '}
										<DashedAreaInputWithRelatedUnits
											name={floor.fieldName('floor', 'area')}
											unitName="floorUnit"
											relatedFields={[
												...floorArray.getAllFields(`floor.${index}.area`),
												...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
											]}
											label={formData.area}
											squareOptions={squareUnitOptions}
										/>
									</Grid.Column>
								))}
								<Grid.Column>
									{showAreaColumn && floorArray.getLength() > 0 && (
										<>
											{formData.fdata22}
											<DashedAreaInputWithRelatedUnits
												name={buildingArea}
												unitName="floorUnit"
												squareOptions={squareUnitOptions}
												relatedFields={[
													...floorArray.getAllFields(),
													...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingArea),
												]}
											/>
										</>
									)}
								</Grid.Column>
							</Grid.Row>
						</Grid>
					);
				}}
			</BlockFloorComponents>
		</>
	);
};

export const FinishCertificateFloorInputs = ({ floorArray, showAreaColumn = true }) => {
	return (
		<>
			{floorArray.getLength() > 0 &&
				floorArray.getFloors().map((floor, index) => (
					<div key={index}>
						{mappingLetter.find((fl) => fl.letter === index).value} {floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.length`}
							unitName="floorUnit"
							relatedFields={[...floorArray.getAllFields(`floor.${index}.length`), 'buildingArea', 'buildingHeight']}
							label={formData.length}
						/>
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.width`}
							unitName="floorUnit"
							relatedFields={[...floorArray.getAllFields(`floor.${index}.width`), 'buildingArea', 'buildingHeight']}
							label={formData.breadth}
						/>
						<DashedLengthInputWithRelatedUnits
							name={`floor.${index}.height`}
							unitName="floorUnit"
							relatedFields={[...floorArray.getAllFields(`floor.${index}.height`), 'buildingArea', 'buildingHeight']}
							label={formData.height}
						/>
						<DashedAreaInputWithRelatedUnits
							name={`floor.${index}.area`}
							unitName="floorUnit"
							label={formData.area}
							squareOptions={squareUnitOptions}
							relatedFields={[...floorArray.getAllFields(`floor.${index}.area`), 'buildingArea', 'buildingHeight']}
						/>
					</div>
				))}
			{showAreaColumn && floorArray.getLength() > 0 && (
				<>
					{formData.fdata22}
					<DashedAreaInputWithRelatedUnits
						name="buildingArea"
						unitName="floorUnit"
						squareOptions={squareUnitOptions}
						relatedFields={[...floorArray.getAllFields('buildingArea'), 'buildingHeight']}
					/>

					{formData.fdata23}
					<DashedLengthInputWithRelatedUnits
						name="buildingHeight"
						unitName="floorUnit"
						relatedFields={[...floorArray.getAllFields('buildingHeight'), 'buildingArea']}
					/>
				</>
			)}
		</>
	);
};

export const FinishCertificateTabularFloorInputs = ({ floorArray, showAreaColumn = true, showLetterNumer = true }) => {
	const naaya = BuildingFinishCertificateData.naya;
	return (
		<table className="certificate-table">
			<tr>
				<th>{naaya.data8.g1}</th>
				<th>
					{naaya.data8.g2}
					{' ( '}
					<UnitDropdownWithRelatedFields
						brackets={false}
						unitName={`floorUnit`}
						relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight']}
					/>
					{' ) '}
				</th>
				<th>
					{naaya.data8.g3}
					{' ( '}
					<UnitDropdownWithRelatedFields
						brackets={false}
						unitName={`floorUnit`}
						relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight']}
					/>{' '}
					{' ) '}
				</th>

				<th>
					{naaya.data8.g4}
					{' ( '}
					<UnitDropdownWithRelatedFields
						brackets={false}
						unitName={`floorUnit`}
						relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight']}
					/>{' '}
					{' ) '}
				</th>
				<th>
					{naaya.data8.g5}
					{' ( '}
					<UnitDropdownWithRelatedFields
						options={squareUnitOptions}
						brackets={false}
						unitName={`floorUnit`}
						relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight']}
					/>{' '}
					{' ) '}
				</th>
			</tr>

			{floorArray.getLength() > 0 &&
				floorArray.getFloors().map((floor, index) => (
					<tr>
						<td>
							{showLetterNumer && mappingLetter.find((fl) => fl.letter === index).value}{' '}
							{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
						</td>
						<td style={{ paddingLeft: '20px' }}>
							<DashedNormalInputIm name={`floor.${index}.length`} />
						</td>
						{/* </div> */}
						<td>
							<DashedNormalInputIm name={`floor.${index}.width`} />
						</td>
						<td>
							<DashedNormalInputIm name={`floor.${index}.height`} />
						</td>
						<td>
							<DashedNormalInputIm name={`floor.${index}.area`} />
						</td>
					</tr>
				))}
		</table>
	);
};

export const SundarHaraichaCertificateFloorTable = ({ floorArray }) => {
	const data = BuildingFinishCertificateData.sundarHaraicha;
	return (
		<Table className="certificate-ui-table">
			<Table.Header>
				<Table.Row>
					<Table.HeaderCell width="1" rowSpan="2">
						{data.floors.snLabel}
					</Table.HeaderCell>
					<Table.HeaderCell width="2" rowSpan="2">
						{data.floors.gharTalla}
					</Table.HeaderCell>

					<Table.HeaderCell width="3" colSpan="3">
						{data.floors.swikritGharko}
						{' ('}
						<UnitDropdownWithRelatedFields
							brackets={false}
							unitName={`floorUnit`}
							relatedFields={[...floorArray.getAllFields(), 'buildingArea', 'buildingHeight']}
							options={squareUnitOptions}
						/>
						{' ) '}
					</Table.HeaderCell>
					<Table.HeaderCell width="3" colSpan="3">
						{data.floors.buildYear}
					</Table.HeaderCell>
					<Table.HeaderCell width="1" rowSpan="2">
						{data.floors.nirman}
					</Table.HeaderCell>
					<Table.HeaderCell width="1" rowSpan="2"></Table.HeaderCell>
				</Table.Row>
				<Table.Row>
					<Table.HeaderCell width="2">{`${data.floors.block} ${data.floors.letters[0]}`}</Table.HeaderCell>
					<Table.HeaderCell width="1">{`${data.floors.block} ${data.floors.letters[1]}`}</Table.HeaderCell>
					<Table.HeaderCell width="1">{`${data.floors.block} ${data.floors.letters[2]}`}</Table.HeaderCell>
					<Table.HeaderCell width="1">{`${data.floors.block} ${data.floors.letters[0]}`}</Table.HeaderCell>
					<Table.HeaderCell width="1">{`${data.floors.block} ${data.floors.letters[1]}`}</Table.HeaderCell>
					<Table.HeaderCell width="1">{`${data.floors.block} ${data.floors.letters[2]}`}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>

			{floorArray.getLength() > 0 &&
				floorArray.getFloors().map((floor, index) => (
					<Table.Row>
						<Table.Cell>{data.floors.sn[index]}.</Table.Cell>
						<Table.Cell textAlign="left">{floorMappingFlat.find((fl) => fl.floor === floor.floor).value} </Table.Cell>
						<Table.Cell>
							<DashedNormalInputIm name={`floor.${index}.area`} />
						</Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
					</Table.Row>
				))}

			{floorArray.getLength() > 0 && (
				<Table.Row>
					<Table.Cell></Table.Cell>
					<Table.Cell textAlign="left">{data.floors.total}</Table.Cell>
					<Table.Cell>
						<DashedNormalInputIm name="buildingArea" />
					</Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
					<Table.Cell></Table.Cell>
				</Table.Row>
			)}
		</Table>
	);
};

export const CertificateFloorBorderlessTable = ({ floorArray }) => {
	return (
		<div>
			<Table className="certificate-borderless-table">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{naaya.data8.g1}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g2}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g3}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g4}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g5}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				{floorArray.getLength() > 0 &&
					floorArray.getFloors().map((floor, index) => (
						<Table.Body key={index}>
							<Table.Row>
								<Table.Cell>
									{mappingLetter.find((fl) => fl.letter === index).value}{' '}
									{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
								</Table.Cell>
								<Table.Cell style={{ paddingLeft: '20px' }}>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.length`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.length`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								{/* </div> */}
								<Table.Cell>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.width`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.width`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								<Table.Cell>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.height`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.height`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								<Table.Cell>
									<DashedAreaInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.area`}
										unitName={`floorUnit`}
										// label={formData.area}
										squareOptions={squareUnitOptions}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.area`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
							</Table.Row>

							{/* {` ${getUnitValue(floor.floorUnit)}`} */}
						</Table.Body>
					))}
			</Table>
		</div>
	);
};
export const FloorBorderlessTableInaruwa = ({ floorArray }) => {
	return (
		<div>
			<Table className="certificate-borderless-table">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell>{inaruwaData.content.content_9}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g2}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g3}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g4}</Table.HeaderCell>
						<Table.HeaderCell>{naaya.data8.g5}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>

				{floorArray.getLength() > 0 &&
					floorArray.getFloors().map((floor, index) => (
						<Table.Body key={index}>
							<Table.Row>
								<Table.Cell>
									{mappingLetter.find((fl) => fl.letter === index).value}{' '}
									{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
								</Table.Cell>
								<Table.Cell style={{ paddingLeft: '20px' }}>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.length`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.length`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								{/* </div> */}
								<Table.Cell>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.width`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.width`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								<Table.Cell>
									<DashedLengthInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.height`}
										unitName={`floorUnit`}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.height`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
								<Table.Cell>
									<DashedAreaInputWithRelatedUnits
										compact={true}
										name={`floor.${index}.area`}
										unitName={`floorUnit`}
										// label={formData.area}
										squareOptions={squareUnitOptions}
										relatedFields={[...floorArray.getAllFields(`floor.${index}.area`), 'buildingArea', 'buildingHeight']}
									/>
								</Table.Cell>
							</Table.Row>

							{/* {` ${getUnitValue(floor.floorUnit)}`} */}
						</Table.Body>
					))}
			</Table>
		</div>
	);
};
