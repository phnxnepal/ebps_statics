import React from 'react';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { getDocUrl } from '../../../../utils/config';
import { getClientCode } from '../../../../utils/clientUtils';
import { getSubHeading } from '../../../../utils/clientConfigs/certificate';
import { getIn } from 'formik';
import { DatePosition, TextSize } from '../../../../utils/constants/formComponentConstants';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import { PatraSankhya } from '../formComponents/PatraSankhyaAndDate';
import { certificateSubHeading } from '../../../../utils/data/genericFormData';

export const FinishCertificateSubHeading = ({ handleChange, setFieldValue, values, errors, Detail }) => {
	const fields = getSubHeading(getClientCode());
	return (
		<div className="flex-container">
			<div className="flex-item-space-between">
				<div className="flex-item-left-margin">
					{fields.serialNumberFields.map((field) => (
						<div>
							{field.label} :
							<DashedLangInputWithSlash
								compact={true}
								name={field.fieldName}
								className="dashedForm-control"
								handleChange={handleChange}
								setFieldValue={setFieldValue}
								value={getIn(values, field.fieldName)}
								error={getIn(errors, field.fieldName)}
							/>
						</div>
					))}
				</div>
				<div className="flex-item-grow-middle">
					<div>
						<span className="certificate-title-normal">
							<h2>{Detail.titleSplit1}</h2>
						</span>
						<span className="certificate-title-bordered">
							<h2>{Detail.titleSplit2}</h2>
						</span>
					</div>
				</div>
				<div className="flex-item-right-margin">
					<div className="certificate-image">{values.photo && <img src={`${getDocUrl()}${values.photo}`} alt="imageFile" />}</div>
				</div>
			</div>
			<div className="flex-item-right margin-bottom margin-top">
				{`${Detail.Bdate}: `}
				<DashedLangDateField
					compact={true}
					name="Bdate"
					value={values.Bdate}
					error={errors.Bdate}
					setFieldValue={setFieldValue}
					inline={true}
				/>
			</div>
		</div>
	);
};

export const CertificateSubHeading = ({ handleChange, setFieldValue, values, errors, title: titleOverride, noImage = false }) => {
	const {
		serialNumberFields,
		datePosition = DatePosition.BOTTOM,
		titles = [{ title: BuildingFinishCertificateData.details.title, className: `${TextSize.SMALL} underline` }],
	} = getSubHeading(getClientCode());
	return (
		<div className="flex-container">
			<div className="flex-item-space-between">
				<div className={`${datePosition === DatePosition.TOP ? 'flex-item-left-margin-self-top' : 'flex-item-left-margin'}`}>
					{serialNumberFields.map((field) => (
						<div key={field.fieldName}>
							{field.label} :
							<DashedLangInputWithSlash
								compact={true}
								name={field.fieldName}
								handleChange={handleChange}
								setFieldValue={setFieldValue}
								value={getIn(values, field.fieldName)}
								error={getIn(errors, field.fieldName)}
							/>
						</div>
					))}
				</div>
				<div className="flex-item-grow-middle">
					<div className="certificate-sub-heading">
						{titleOverride ? (
							<p className={`${TextSize.SMALL} underline`}>{titleOverride}</p>
						) : (
							<>
								{titles.map((title, idx) => (
									<p key={idx} className={title.className}>
										{title.title}
									</p>
								))}
							</>
						)}
					</div>
				</div>
				<div className="flex-item-right-margin">
					{datePosition === DatePosition.TOP && (
						<div>
							{`${BuildingFinishCertificateData.details.Bdate}: `}
							<DashedLangDateField
								compact={true}
								name="Bdate"
								value={values.Bdate}
								error={errors.Bdate}
								setFieldValue={setFieldValue}
								inline={true}
							/>
						</div>
					)}
					{!noImage? (
						<div className="certificate-image">{values.photo && <img src={`${getDocUrl()}${values.photo}`} alt="imageFile" />}</div>
						) : <div><div className="certificate-image"></div> <br/></div>
					}
					{datePosition === DatePosition.BOTTOM && (
						<div>
							{`${BuildingFinishCertificateData.details.Bdate}: `}
							<DashedLangDateField
								compact={true}
								name="Bdate"
								value={values.Bdate}
								error={errors.Bdate}
								setFieldValue={setFieldValue}
								inline={true}
							/>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};

export const CertificateSubHeadingWithoutPhoto = ({ setFieldValue, values, errors, titles, textSize = TextSize.BIG }) => {
	return (
		<div className="flex-container">
			<div className="flex-item-space-between">
				<div className="flex-item-left-margin">
					<PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
				</div>
				<div className="flex-item-grow-middle">
					<div>
						{titles.map((title) => (
							<span className="certificate-title">{textSize === TextSize.BIG ? <h2>{title}</h2> : <h3>{title}</h3>}</span>
						))}
					</div>
				</div>
				<div className="flex-item-right-margin">
					<div className="flex-item-right">
						{`${certificateSubHeading.miti}: `}
						<DashedLangDateField
							compact={true}
							name="Bdate"
							value={values.Bdate}
							error={errors.Bdate}
							setFieldValue={setFieldValue}
							inline={true}
						/>
					</div>
				</div>
			</div>
		</div>
	);
};
