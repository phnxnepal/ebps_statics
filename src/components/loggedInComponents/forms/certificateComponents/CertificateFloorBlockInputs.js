import React from 'react';
import { BuildingBuildCertificateData } from '../../../../utils/data/BuildingBuildCertificateData';
import { floorMappingFlat, squareUnitOptions } from '../../../../utils/dataUtils';
import {
	DashedLengthInputWithRelatedUnits,
	DashedAreaInputWithRelatedUnits,
	UnitDropdownWithRelatedFields,
} from '../../../shared/EbpsUnitLabelValue';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import { DashedNormalInputIm, DashedReadOnlyField } from '../../../shared/DashedFormInput';
import { Table } from 'semantic-ui-react';
import { BlockFloorComponents, BlockComponents } from '../../../shared/formComponents/BlockFloorComponents';

const formData = BuildingBuildCertificateData.formdata;
const naaya = BuildingFinishCertificateData.naya;

export const FinishCertificateBlockFloors = ({ floorArray, formattedFloors, showAreaColumn = true, compact = false }) => {
	return (
		<>
			<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
				{(floorObj, block) => {
					const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
					const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
					return (
						<div className="margin-bottom">
							{floorObj.map((floor, index) => (
								<div key={index}>
									{floor.label.sn(index)} {floor.label.floorName}{' '}
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'length')}
										unitName="floorUnit"
										compact={compact}
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.length`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.length}
									/>
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'width')}
										unitName="floorUnit"
										compact={compact}
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.width`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.breadth}
									/>
									<DashedLengthInputWithRelatedUnits
										name={floor.fieldName('floor', 'height')}
										unitName="floorUnit"
										compact={compact}
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.height`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.height}
									/>
									<DashedAreaInputWithRelatedUnits
										name={floor.fieldName('floor', 'area')}
										unitName="floorUnit"
										compact={compact}
										relatedFields={[
											...floorArray.getAllFields(`floor.${index}.area`),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
										]}
										label={formData.area}
										squareOptions={squareUnitOptions}
									/>
								</div>
							))}
							{showAreaColumn && floorArray.getLength() > 0 && (
								<>
									{formData.fdata22}
									<DashedAreaInputWithRelatedUnits
										name={buildingArea}
										unitName="floorUnit"
										squareOptions={squareUnitOptions}
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingArea),
										]}
									/>

									{formData.fdata23}
									<DashedLengthInputWithRelatedUnits
										name={buildingHeight}
										unitName="floorUnit"
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingHeight),
										]}
									/>
								</>
							)}
							<br />
						</div>
					);
				}}
			</BlockFloorComponents>
		</>
	);
};

export const FinishCertificateTabularFloorBlockInputs = ({
	floorArray,
	formattedFloors,
	showAreaColumn = true,
	showFloorRange = false,
	showLetterNumer = true,
	data = { floorRangeSuffix: null },
}) => {
	const naaya = BuildingFinishCertificateData.naya;
	return (
		<div className="flex-div">
			<div>{naaya.data8.g1_0}</div>
			<div>
				<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
					{(floorObj, block) => {
						const allOtherFields = floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']);
						const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
						return (
							<table className="small-font-print-table margin-bottom">
								<thead>
									<tr>
										<th>{naaya.data8.g1_1}</th>
										<th>
											{naaya.data8.g2}
											{' ( '}
											<UnitDropdownWithRelatedFields
												brackets={false}
												unitName={`floorUnit`}
												relatedFields={[...floorArray.getAllFields(), ...allOtherFields]}
											/>
											{' ) '}
										</th>
										<th>
											{naaya.data8.g3}
											{' ( '}
											<UnitDropdownWithRelatedFields
												brackets={false}
												unitName={`floorUnit`}
												relatedFields={[...floorArray.getAllFields(), ...allOtherFields]}
											/>{' '}
											{' ) '}
										</th>

										<th>
											{naaya.data8.g4}
											{' ( '}
											<UnitDropdownWithRelatedFields
												brackets={false}
												unitName={`floorUnit`}
												relatedFields={[...floorArray.getAllFields(), ...allOtherFields]}
											/>{' '}
											{' ) '}
										</th>
										<th>
											{naaya.data8.g5}
											{' ( '}
											<UnitDropdownWithRelatedFields
												options={squareUnitOptions}
												brackets={false}
												unitName={`floorUnit`}
												relatedFields={[...floorArray.getAllFields(), ...allOtherFields]}
											/>{' '}
											{' ) '}
										</th>
									</tr>
								</thead>

								<tbody>
									{floorObj.map((floor, index) => (
										<tr key={index}>
											<td>
												{showLetterNumer && floor.label.sn(index)} {floor.label.floorName}{' '}
											</td>
											<td>
												<DashedNormalInputIm name={floor.fieldName('floor', 'length')} />
											</td>
											<td>
												<DashedNormalInputIm name={floor.fieldName('floor', 'width')} />
											</td>
											<td>
												<DashedNormalInputIm name={floor.fieldName('floor', 'height')} />
											</td>
											<td>
												<DashedNormalInputIm name={floor.fieldName('floor', 'area')} />
											</td>
										</tr>
									))}
									{showFloorRange && (
										<Table.Row>
											<Table.Cell colSpan="3">
												<DashedNormalInputIm name={floorArray.getReducedFieldName('startFloor', block)} readOnly={true} />
												{data.floorRangeSuffix || ''}
												{naaya.data8.g6}
												<DashedNormalInputIm name={floorArray.getReducedFieldName('endFloor', block)} readOnly={true} />
												{data.floorRangeSuffix || ''}
											</Table.Cell>
										</Table.Row>
									)}
									{showAreaColumn && (
										<Table.Row>
											<Table.Cell colSpan="3">
												<span style={{ fontWeight: 'bold' }}>{naaya.data9.j1}</span>
												<DashedAreaInputWithRelatedUnits
													name={buildingArea}
													unitName="floorUnit"
													squareOptions={squareUnitOptions}
													relatedFields={[
														...floorArray.getAllFields(),
														...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingArea),
													]}
												/>
											</Table.Cell>
										</Table.Row>
									)}
								</tbody>
							</table>
						);
					}}
				</BlockFloorComponents>
			</div>
		</div>
	);
};

export const SundarHaraichaCertificateFloorTable = ({ floorArray, formattedFloors, blocks }) => {
	const data = BuildingFinishCertificateData.sundarHaraicha;
	const itr = floorArray.getFloorWiseInitialValues('area', 'area', formattedFloors);
	const hasBlocks = floorArray.getHasBlocks();
	const rowSpans = hasBlocks ? 2 : 1;
	const colSpans = hasBlocks ? blocks.length : 1;
	let idx = 0;

	return (
		<Table fixed className="certificate-ui-table">
			<Table.Header>
				<Table.Row>
					<Table.HeaderCell width="1" rowSpan={rowSpans}>
						{data.floors.snLabel}
					</Table.HeaderCell>
					<Table.HeaderCell width="2" rowSpan={rowSpans}>
						{data.floors.gharTalla}
					</Table.HeaderCell>

					<Table.HeaderCell colSpan={colSpans}>
						{data.floors.swikritGharko}
						{' ('}
						<UnitDropdownWithRelatedFields
							brackets={false}
							unitName={`floorUnit`}
							relatedFields={[
								...floorArray.getAllFields(),
								...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight', 'buildingWidth', 'buildingLength']),
							]}
							options={squareUnitOptions}
						/>
						{' ) '}
					</Table.HeaderCell>
					<Table.HeaderCell colSpan={colSpans}>{data.floors.buildYear}</Table.HeaderCell>
					<Table.HeaderCell width="1" rowSpan={rowSpans}>
						{data.floors.nirman}
					</Table.HeaderCell>
					{/* <Table.HeaderCell width="1" rowSpan="2"></Table.HeaderCell> */}
				</Table.Row>
				{hasBlocks && (
					<Table.Row>
						{blocks.map((blk) => (
							<Table.HeaderCell key={`${blk}-area`} width="2">{`${data.floors.block} ${blk}`}</Table.HeaderCell>
						))}
						{blocks.map((blk) => (
							<Table.HeaderCell key={`${blk}-area`} width="2">{`${data.floors.block} ${blk}`}</Table.HeaderCell>
						))}
					</Table.Row>
				)}
			</Table.Header>
			<Table.Body>
				{itr.map((floor, index) => {
					idx++;
					return (
						<Table.Row key={index}>
							<Table.Cell>{idx}.</Table.Cell>
							<Table.Cell textAlign="left">{floorMappingFlat.find((fl) => fl.floor === index).value}</Table.Cell>
							{hasBlocks ? (
								<>
									{blocks.map((blk) => (
										<Table.Cell key={`${blk}-area`}>
											<DashedNormalInputIm name={floorArray.getFieldName('floor', index, 'area', blk)} />
										</Table.Cell>
									))}
									{blocks.map((blk) => (
										<Table.Cell key={`${blk}-year`}>
											<DashedNormalInputIm name={floorArray.getFieldName('floor', index, 'year', blk)} />
										</Table.Cell>
									))}
								</>
							) : (
								<>
									<Table.Cell>
										<DashedNormalInputIm name={floorArray.getFieldName('floor', index, 'area', null)} />
									</Table.Cell>
									<Table.Cell>
										<DashedNormalInputIm name={floorArray.getFieldName('floor', index, 'year', null)} />
									</Table.Cell>
								</>
							)}
							<Table.Cell></Table.Cell>
							{/* <Table.Cell></Table.Cell> */}
						</Table.Row>
					);
				})}
				{floorArray.getLength() > 0 && (
					<Table.Row>
						<Table.Cell></Table.Cell>
						<Table.Cell textAlign="left">{data.floors.total}</Table.Cell>
						{hasBlocks ? (
							blocks.map((blk) => (
								<Table.Cell key={`${blk}-area`}>
									<DashedNormalInputIm name={floorArray.getReducedFieldName('buildingArea', blk)} />
								</Table.Cell>
							))
						) : (
							<Table.Cell>
								<DashedNormalInputIm name={floorArray.getReducedFieldName('buildingArea', null)} />
							</Table.Cell>
						)}
						<Table.Cell colSpan={blocks ? parseInt(blocks.length) + 1 : 1}></Table.Cell>
					</Table.Row>
				)}
			</Table.Body>
		</Table>
	);
};

export const CertificateFloorBlockBorderlessTable = ({
	floorArray,
	formattedFloors,
	compact = false,
	showFloorRange = false,
	showAreaColumn = false,
	data = { listHeader: BuildingFinishCertificateData.naya.data8.g1_0 },
}) => {
	const naaya = BuildingFinishCertificateData.naya;
	return (
		<div style={{ display: 'flex' }}>
			<div>{data.listHeader}</div>
			<div>
				<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
					{(floorObj, block) => {
						const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
						return (
							<Table className="certificate-borderless-table small-font-print-table">
								<Table.Header>
									<Table.Row>
										<Table.HeaderCell>{naaya.data8.g1_1}</Table.HeaderCell>
										<Table.HeaderCell>{naaya.data8.g2}</Table.HeaderCell>
										<Table.HeaderCell>{naaya.data8.g3}</Table.HeaderCell>
										<Table.HeaderCell>{naaya.data8.g4}</Table.HeaderCell>
										<Table.HeaderCell>{naaya.data8.g5}</Table.HeaderCell>
									</Table.Row>
								</Table.Header>
								<Table.Body>
									{floorObj.map((floor, index) => {
										const length = floor.fieldName('floor', 'length');
										const width = floor.fieldName('floor', 'width');
										const height = floor.fieldName('floor', 'height');
										const area = floor.fieldName('floor', 'area');
										return (
											<Table.Row key={index}>
												<Table.Cell>
													{floor.label.sn(index)} {floor.label.floorName}{' '}
												</Table.Cell>
												<Table.Cell>
													<DashedLengthInputWithRelatedUnits
														name={length}
														compact={compact}
														unitName="floorUnit"
														relatedFields={[
															...floorArray.getAllFields(length),
															...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
														]}
													/>
												</Table.Cell>
												{/* </div> */}
												<Table.Cell>
													<DashedLengthInputWithRelatedUnits
														name={width}
														compact={compact}
														unitName="floorUnit"
														relatedFields={[
															...floorArray.getAllFields(width),
															...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
														]}
													/>
												</Table.Cell>
												<Table.Cell>
													<DashedLengthInputWithRelatedUnits
														name={height}
														compact={compact}
														unitName="floorUnit"
														relatedFields={[
															...floorArray.getAllFields(height),
															...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
														]}
													/>
												</Table.Cell>
												<Table.Cell>
													<DashedLengthInputWithRelatedUnits
														name={area}
														compact={compact}
														unitName="floorUnit"
														relatedFields={[
															...floorArray.getAllFields(area),
															...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight']),
														]}
														squareOptions={squareUnitOptions}
													/>
												</Table.Cell>
											</Table.Row>
										);
									})}
									{showFloorRange && (
										<Table.Row>
											<Table.Cell colSpan="3">
												<DashedNormalInputIm name={floorArray.getReducedFieldName('startFloor', block)} readOnly={true} />
												{naaya.data8.g6}
												<DashedNormalInputIm name={floorArray.getReducedFieldName('endFloor', block)} readOnly={true} />
											</Table.Cell>
										</Table.Row>
									)}
									{showAreaColumn && (
										<Table.Row>
											<Table.Cell colSpan="3">
												<span style={{ fontWeight: 'bold' }}>{naaya.data9.j1}</span>
												<DashedAreaInputWithRelatedUnits
													name={buildingArea}
													unitName="floorUnit"
													squareOptions={squareUnitOptions}
													relatedFields={[
														...floorArray.getAllFields(),
														...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingArea),
													]}
												/>
											</Table.Cell>
										</Table.Row>
									)}
								</Table.Body>
							</Table>
						);
					}}
				</BlockFloorComponents>
			</div>
		</div>
	);
};

export const HeightFloorNumberSection = ({ floorArray, blocks, data = { number: null, heightLabel: null, floorNumberLabel: null } }) => {
	const labels = {
		number: data.number || naaya.data9.j4_0,
		heightLabel: data.heightLabel || naaya.data9.j4_1,
		floorNumberLabel: data.floorNumber || naaya.data9.j5,
	};
	return (
		<div className="flex-div">
			<div>{labels.number}</div>
			<div>
				<BlockComponents floorArray={floorArray} blocks={blocks}>
					{(block) => {
						const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
						const floorNumber = floorArray.getReducedFieldName('floorNumber', block);
						return (
							<>
								{' '}
								{labels.heightLabel}
								<DashedLengthInputWithRelatedUnits
									name={buildingHeight}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllFields(),
										...floorArray.getAllReducedBlockFields(['buildingArea', 'buildingHeight'], buildingHeight),
									]}
								/>
								{labels.floorNumberLabel} <DashedReadOnlyField name={floorNumber} />
							</>
						);
					}}
				</BlockComponents>
			</div>
		</div>
	);
};
