import React from 'react';
import { DashedLangDateField } from '../../../shared/DateField';
import { getDocUrl } from '../../../../utils/config';
import { DatePosition, TextSize } from '../../../../utils/constants/formComponentConstants';
import { PatraSankhya } from '../formComponents/PatraSankhyaAndDate';

export const PuranoGharCertificateSubHeading = ({
	setFieldValue,
	values,
	errors,
	Detail,
	datePosition = DatePosition.TOP,
	textSize = TextSize.BIG,
}) => {
	return (
		<div className="flex-container">
			{datePosition === DatePosition.TOP && (
				<div className="flex-item-right margin-bottom">
					{`${Detail.Bdate}: `}
					<DashedLangDateField compact={true} name="Bdate" value={values.Bdate} error={errors.Bdate} setFieldValue={setFieldValue} inline={true} />
				</div>
			)}
			<div className="flex-item-space-between">
				<div className="flex-item-left-margin">
					<PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
				</div>
				<div className="flex-item-grow-middle">
					<div>
						<span className="certificate-title">{textSize === TextSize.BIG ? <h2>{Detail.title2}</h2> : <h3>{Detail.title2}</h3>}</span>
						<span className="certificate-title">{textSize === TextSize.BIG ? <h2>{Detail.title3}</h2> : <h3>{Detail.title3}</h3>}</span>
					</div>
				</div>
				<div className="flex-item-right">
					<div className="certificate-image">{values.photo && <img src={`${getDocUrl()}${values.photo}`} alt="imageFile" height={100} width={100} />}</div>
				</div>
			</div>
			{datePosition === DatePosition.BOTTOM && (
				<div className="flex-item-right margin-bottom">
					{`${Detail.Bdate}: `}
					<DashedLangDateField compact={true} name="Bdate" value={values.Bdate} error={errors.Bdate} setFieldValue={setFieldValue} inline={true} />
				</div>
			)}
		</div>
	);
};

export const PuranoGharCertificateSubHeadingWithoutPhoto = ({ setFieldValue, values, errors, Detail, textSize = TextSize.BIG }) => {
	return (
		<div className="flex-container">
			<div className="flex-item-space-between">
				<div className="flex-item-left-margin">
					<PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
				</div>
				<div className="flex-item-grow-middle">
					<div>
						<span className="certificate-title">{textSize === TextSize.BIG ? <h2>{Detail.title2}</h2> : <h3>{Detail.title2}</h3>}</span>
						<span className="certificate-title">{textSize === TextSize.BIG ? <h2>{Detail.title3}</h2> : <h3>{Detail.title3}</h3>}</span>
					</div>
				</div>
				<div className="flex-item-right-margin">
					<div className="flex-item-right">
						{`${Detail.Bdate}: `}
						<DashedLangDateField compact={true} name="Bdate" value={values.Bdate} error={errors.Bdate} setFieldValue={setFieldValue} inline={true} />
					</div>
				</div>
			</div>
		</div>
	);
};
