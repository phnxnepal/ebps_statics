import React from 'react';
import { LayoutIjajat } from '../../../../utils/data/LayoutPratibedanIjajatData';
import { DashedLengthInputWithRelatedUnits } from './../../../shared/EbpsUnitLabelValue';

export const Surroundings = () => {
    return (
        <div>
            <span style={{ flexDirection: 'row' }}>
                <DashedLengthInputWithRelatedUnits name="purwa" label={LayoutIjajat.data.surr_purwa} unitName="surrUnit" relatedFields={['paschim', 'uttar', 'dakshin']} />
                <DashedLengthInputWithRelatedUnits name="paschim" label={LayoutIjajat.data.surr_paschim} unitName="surrUnit" relatedFields={['purwa', 'uttar', 'dakshin']} />
            </span>
            <br />
            <span style={{ flexDirection: 'row' }}>
                <DashedLengthInputWithRelatedUnits name="uttar" label={LayoutIjajat.data.surr_uttar} unitName="surrUnit" relatedFields={['purwa', 'paschim', 'dakshin']} />
                <DashedLengthInputWithRelatedUnits name="dakshin" label={LayoutIjajat.data.surr_dakshin} unitName="surrUnit" relatedFields={['purwa', 'paschim', 'uttar']} />
            </span>
        </div>
    );
}

