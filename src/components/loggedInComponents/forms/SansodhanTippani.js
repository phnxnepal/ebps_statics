import React from 'react';

import api from '../../../utils/api';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { genericTippaniAdes } from '../../../utils/data/genericFormData';
import { GenericTippaniAdes } from './formComponents/GenericTippaniAdes';

const SansodhanTippani = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.sansodhanTippani,
				objName: 'sansodhanTippani',
				form: true,
			},
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'textarea', 'value'],
		}}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => (
			<GenericTippaniAdes
				fieldName="nibedan"
				topic={genericTippaniAdes.sansodhanTippani.topic}
				title={genericTippaniAdes.title}
				api={api.sansodhanTippani}
				{...props}
				parentProps={parentProps}
			/>
		)}
	/>
);

export default SansodhanTippani;
