import React, { Component } from 'react';
import { Formik } from 'formik';
import Viewer from 'react-viewer';
import { Form, Divider, List, Modal, Icon, Dropdown, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { getUploadedFilesMaster } from '../../../store/actions/fileActions';
import { getPermitAndUserData } from '../../../store/actions/formActions';
import FallbackComponent from '../../shared/FallbackComponent';
import { getDocUrl } from '../../../utils/config';
import { getSplitUrl } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import DeleteModal from './../../shared/DeleteModal';
import { deleteFile } from './../../../store/actions/fileActions';
import { getDeleteStatus } from '../../../utils/urlUtils';
import { ImageDisplayPreview } from '../../shared/file/FileView';
import { CompactInfoMessage } from '../../shared/formComponents/CompactInfoMessage';
import { fileData } from '../../../utils/data/genericData';
import { SectionHeader } from '../../uiComponents/Headers';
import { SearchableDropdown } from '../../shared/formComponents/SearchableDropdown';
// import Helmet from 'react-helmet';
// import { IMAGE } from '../../../utils/constants/fileConstants';

// let urlArr = [];
export class ListFiles extends Component {
	state = {
		open: false,
		modalUrl: '',
		currImg: 0,
		id: '',
		isDeleteModalOpen: false,
		formOptions: [],
		selectedForm: '',
		fileCategories: [],
	};

	getFormWiseCategory = (form) => {
		return this.props.fileCategories.filter((cat) => String(cat.formId) === String(form));
	};

	handleFormSelection = (form) => {
		this.setState({ selectedForm: form, fileCategories: this.getFormWiseCategory(form) });
	};

	show = (size, modalUrl) => () => {
		this.setState({ size, open: true, modalUrl: modalUrl });
	};

	close = () => this.setState({ open: false });

	componentDidMount = () => {
		this.props.getPermitAndUserData();

		if (this.props.files) {
			this.props.getUploadedFilesMaster(true);
		} else {
			this.props.getUploadedFilesMaster();
		}
	};

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success && this.props.success) {
			this.props.getUploadedFiles();
		}

		if (prevProps.formMaster !== this.props.formMaster) {
			const requiredForms = this.props.fileCategories ? this.props.fileCategories.map((cat) => String(cat.formId)) : [];
			const formMaster = this.props.formMaster.filter((form) => requiredForms.includes(String(form.id)));
			const formOptions =
				formMaster && formMaster.length > 0
					? formMaster.map((row) => ({
							value: row.id,
							text: row.name,
					  }))
					: [];
			const selectedForm = this.props.url ? this.props.url : (formOptions && formOptions.length > 0 ? formOptions[0].value : '');
			const fileCategories = this.getFormWiseCategory(selectedForm);
			this.setState({ formOptions, selectedForm, fileCategories });
		}
	}

	handleDeleteModalOpen = (id) => {
		this.setState({
			isDeleteModalOpen: true,
			id: id,
		});
	};

	handleDeleteModalClose = () => {
		this.setState({
			isDeleteModalOpen: false,
		});
	};

	render() {
		const { formOptions, selectedForm, fileCategories } = this.state;
		if (this.props.permitData && this.props.userData && this.props.fileCategories && this.props.formMaster) {
			const { open, size, modalUrl } = this.state;
			const { files, prevData, userData, Durl } = this.props;

			let isDelete = false;
			if (prevData) {
				try {
					isDelete = getDeleteStatus(Durl, prevData, userData);
				} catch (err) {
					isDelete = false;
				}
			}
			return (
				<>
					<SectionHeader>
						<h2 className="bottom-margin">{fileData.fileList}</h2>
					</SectionHeader>

					<Formik
						initialValues={{ formName: this.props.url || 1 }}
						// onSubmit={async (values, actions) => {
						// 	actions.setSubmitting(true);
						// }}
						render={(props) => {
							return (
								<div>
									<Form loading={props.isSubmitting}>
										{formOptions.length < 1 ? (
											<CompactInfoMessage content={fileData.noFormWithFile} />
										) : (
											<SearchableDropdown
												label={fileData.selectForm}
												options={formOptions}
												name="selectedForm"
												value={selectedForm}
												handleChange={this.handleFormSelection}
												bold
											/>
										)}
									</Form>

									<br />
									{fileCategories.length > 0 ? (
										<div>
											{/* <Helmet>
												<title>File List</title>
											</Helmet> */}
											<Grid columns={2} divided doubling padded>
												{fileCategories.map((category, index) => (
													<Grid.Column key={index}>
														{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
														<Divider horizontal>{category.name}</Divider>{' '}
														<List relaxed>
															<List.Header>Existing Files</List.Header>
															<div className="previewFileUpload">
																{files &&
																files.length > 0 &&
																files.some((file) => file.fileType.id === category.id) ? (
																	files
																		.filter((file) => file.fileType.id === category.id)
																		.map((el) => {
																			const fileTitleName = getSplitUrl(el.fileUrl, '/');
																			return (
																				<div
																					key={el.fileUrl}
																					className={'eachPreviewFile viewFileSuccess'}
																					title={fileTitleName}
																				>
																					<>
																						<ImageDisplayPreview
																							src={el.fileUrl}
																							alt={fileTitleName}
																							onClick={() => {
																								this.setState({
																									imageOpen: true,
																									currImg: { src: `${getDocUrl()}${el.fileUrl}` },
																								});
																							}}
																						/>
																						{isDelete && (
																							<Icon
																								name="close"
																								className="closeImg"
																								onClick={() => this.handleDeleteModalOpen(el.id)}
																							/>
																						)}
																					</>
																				</div>
																			);
																		})
																) : (
																	<List.Item>No files uploaded.</List.Item>
																)}
															</div>
														</List>
														<Viewer
															visible={this.state.imageOpen}
															onClose={() => this.setState({ imageOpen: false })}
															images={[this.state.currImg]}
															zIndex={10000}
														/>
													</Grid.Column>
												))}
											</Grid>
										</div>
									) : (
										<CompactInfoMessage content={fileData.noFileCategoryForm} />
									)}
								</div>
							);
						}}
					/>
					<Modal size={size} open={open} onClose={this.close}>
						<Modal.Content>
							<img src={modalUrl} alt="" />
						</Modal.Content>
					</Modal>
					<DeleteModal
						open={this.state.isDeleteModalOpen}
						onClose={this.handleDeleteModalClose}
						history={this.props.parentHistory}
						loadingModal={this.props.loadingModal}
						id={this.state.id}
						deleteFile={() => this.props.deleteFile(this.state.id)}
					/>
				</>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getUploadedFilesMaster,
	deleteFile,
};

const mapStateToProps = (state) => ({
	permitData: state.root.formData.permitData,
	userData: state.root.formData.userData,
	files: state.root.formData.files,
	fileCategories: state.root.formData.fileCategories,
	formMaster: state.root.formData.formMaster,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

export default connect(mapStateToProps, mapDispatchToProps)(ListFiles);

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(ListFiles);
