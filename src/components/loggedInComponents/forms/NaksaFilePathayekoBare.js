import React, { Component } from 'react';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { Form, Grid } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import ErrorDisplay from '../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import { isEmpty, showToast } from '../../../utils/functionUtils';
import api from '../../../utils/api';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { SectionHeader } from '../../uiComponents/Headers';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { DashedLangDateField } from './../../shared/DateField';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import { ImageDisplayInline } from '../../shared/file/FileView';

import { naksaFile } from '../../../utils/data/naksaFilePathayeko';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { RadioInput } from './../../shared/formComponents/RadioInput';
const NaksaFileSchema = Yup.object().shape(
	Object.assign({
		date: validateNullableNepaliDate,
	})
);
const naksaFileData = naksaFile.data;
class NaksaFilePathayekoBareComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		initVal = prepareMultiInitialValues(
			{
				obj: mapTech,
				reqFields: ['consultancyName'],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.date)) {
			initVal.date = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const user_info = this.props.userData;
		const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const salutationData = [naksaFileData.data_salutation, `${userData.organization.name}, ${userData.organization.address}`];
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={NaksaFileSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.NaksaFilePathayekoBare}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<SectionHeader>
										<h3 className="normal underline end-section">{naksaFileData.title}</h3>
									</SectionHeader>
									<LetterSalutation lines={salutationData} />
									<br />
									<div style={{ textAlign: 'justify ' }}>
										<p>
											{naksaFileData.data_1}
											{userData.organization.name}
											{naksaFileData.data_2}
											{permitData.nibedakTol}
											{naksaFileData.data_3}
											{permitData.sadak}
											{naksaFileData.data_4}
											{permitData.kittaNo}
											{naksaFileData.data_5}
											{permitData.landArea}
											{permitData.landAreaType}
											{naksaFileData.data_6}
											{permitData.nibedakName}
											{naksaFileData.data_7}
										</p>
										<br />
									</div>
									<FlexSingleRight>
										<ImageDisplayInline label={naksaFileData.footer_1} src={userData.info.stamp} alt="organization stamp" />
										{naksaFileData.footer_2}
										<DashedLangInput
											name="consultancyName"
											setFieldValue={setFieldValue}
											value={values.consultancyName}
											className="dashedForm-control"
										/>
										<div className="no-margin-field">
											{naksaFileData.footer_3}
											<DashedLangDateField
												compact={true}
												name="date"
												value={values.date}
												error={errors.date}
												setFieldValue={setFieldValue}
												inline={true}
											/>
										</div>
										{naksaFileData.footer_4}
										<span className="ui input signature-placeholder" />
									</FlexSingleRight>
									<br />
									<br />
									<div>
										<SectionHeader>
											<p className="left-align underline">{naksaFileData.checkBoxTitle}</p>
										</SectionHeader>
										{naksaFileData.checkBoxItems.map((checkItem, index) => (
											<Grid columns={2} key={index}>
												<Grid.Row key={index}>
													<Grid.Column>{checkItem}</Grid.Column>
													<Grid.Column>
														{naksaFileData.radioOption.map((name, id) => (
															<RadioInput key={id} option={name} name={`kagajat.${index}`} space={true} />
														))}
													</Grid.Column>
												</Grid.Row>
											</Grid>
										))}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const NaksaFilePathayekoBare = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.NaksaFilePathayekoBare,
				objName: 'naksaFilePathayekoBare',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['15dayspecial'],
			param4: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		render={props => <NaksaFilePathayekoBareComponent {...props} parentProps={parentProps} />}
	/>
);

export default NaksaFilePathayekoBare;
