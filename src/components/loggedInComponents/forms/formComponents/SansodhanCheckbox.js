import React from 'react';
import { Grid } from 'semantic-ui-react';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { Field } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';

export const SansodhanCheckbox = ({ constructionType, label, options, handleChange, values }) => {
	return (
		<div className="remove-on-print">
			<Grid stackable columns={4}>
				<Grid.Column mobile={16} tablet={8} computer={6}>
					{/* <Segment> */}
					<h3>
						{/* {getConstructionTypeValue(constructionType) === ConstructionTypeValue.NAYA_NIRMAN && ( */}
							<div>
								<fieldset>
									<SectionHeader>
										<p className="underline left-align">{label}</p>
									</SectionHeader>{' '}
									{options.map((row, index) => (
										<div key={index}>
											<Field
												type="radio"
												name="sanso"
												id={index}
												value={row}
												onClick={handleChange}
												defaultChecked={values.sanso === row}
												style={{ cursor: 'pointer', margin: '.4rem' }}
											/>
											<label>{row}</label>
										</div>
									))}
								</fieldset>
							</div>
						{/* )} */}
					</h3>
					{/* </Segment> */}
				</Grid.Column>
			</Grid>
		</div>
	);
};
