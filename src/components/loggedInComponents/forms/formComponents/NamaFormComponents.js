import React from 'react';
import { getMessage } from '../../../shared/getMessage';
import { namaFormData } from '../../../../utils/data/namaFormData';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { Grid } from 'semantic-ui-react';
import { getIn } from 'formik';

const messageId = 'namaFormData.commonData';

export const NamaFormCommonBody = ({ userData, permitData, setFieldValue, values, errors }) => {
	return (
		<span>
			{getMessage(`${messageId}.main1`, namaFormData.commonData.main1)}
			{userData.organization.address} {getMessage(`${messageId}.district`, namaFormData.commonData.district)} {userData.organization.name}{' '}
			{getMessage(`${messageId}.ward`, namaFormData.commonData.ward)}
			{permitData.nibedakTol} {namaFormData.commonData.ma} {getMessage(`${messageId}.main2`, namaFormData.commonData.main2)} {permitData.tol}{' '}
			{getMessage(`${messageId}.main3`, namaFormData.commonData.main3)} {permitData.nibedakName}{' '}
			{getMessage(`${messageId}.main4`, namaFormData.commonData.main4)} {permitData.oldMunicipal}{' '}
			{getMessage(`${messageId}.haal`, namaFormData.commonData.haal)} {userData.organization.name}{' '}
			{getMessage(`${messageId}.ward`, namaFormData.commonData.ward)} {permitData.newWardNo}{' '}
			{getMessage(`${messageId}.main5`, namaFormData.commonData.main5)} {permitData.kittaNo}{' '}
			{getMessage(`${messageId}.area`, namaFormData.commonData.area)} {permitData.landArea} {permitData.landAreaType}{' '}
		</span>
	);
};

export const RohawarSignature = ({ setFieldValue, values, errors }) => {
	return (
		<Grid.Row>
			<Grid.Column width="2" />
			<Grid.Column width="14">
				<Grid.Row>
					<h3 className="no-margin-heading">{getMessage(`${messageId}.rohawar`, namaFormData.commonData.rohawar)}</h3>
				</Grid.Row>
				{namaFormData.commonData.rohawarList.map((row, index) => (
					<Grid.Row key={index} style={{ padding: '5px 0 5px 0' }}>
						<div>
							{` ${row} `}
							<DashedLangInput
								name={`witness.${index}.name`}
								setFieldValue={setFieldValue}
								value={getIn(values, `witness.${index}.name`)}
								error={getIn(errors, `witness.${index}.name`)}
							/>{' '}
							{getMessage(`${messageId}.address`, namaFormData.commonData.address)}{' '}
							<DashedLangInput
								name={`witness.${index}.address`}
								setFieldValue={setFieldValue}
								value={getIn(values, `witness.${index}.address`)}
								error={getIn(errors, `witness.${index}.address`)}
							/>
						</div>
					</Grid.Row>
				))}
			</Grid.Column>
			<Grid.Column width="2" />
		</Grid.Row>
	);
};

export const WitnessSignature = ({ setFieldValue, values, errors }) => {
	return (
		<Grid.Row>
			<Grid.Column width="2" />
			<Grid.Column width="14">
				<Grid.Row>
					{/* <Grid.Column> */}
					<h3 className="no-margin-heading">{getMessage(`${messageId}.main12`, namaFormData.commonData.main12)}</h3>
					{/* </Grid.Column> */}
				</Grid.Row>
				{namaFormData.commonData.witness.map((row, index) => (
					<Grid.Row key={index} style={{ padding: '5px 0 5px 0' }}>
						{/* <Grid.Column> */}
						<div>
							{row}
							<DashedLangInput
								name={`witness.${index}.name`}
								setFieldValue={setFieldValue}
								value={getIn(values, `witness.${index}.name`)}
								error={getIn(errors, `witness.${index}.name`)}
							/>
							{/* </Grid.Column>
												<Grid.Column> */}
							{getMessage(`${messageId}.signature`, namaFormData.commonData.signature)}
							<span className="ui input dashedForm-control" />
							{/* </Grid.Column>
												<Grid.Column> */}
							{getMessage(`${messageId}.address`, namaFormData.commonData.address)}
							<DashedLangInput
								name={`witness.${index}.address`}
								setFieldValue={setFieldValue}
								value={getIn(values, `witness.${index}.address`)}
								error={getIn(errors, `witness.${index}.address`)}
							/>
							{/* </Grid.Column> */}
						</div>
					</Grid.Row>
				))}
			</Grid.Column>
			<Grid.Column width="2" />
		</Grid.Row>
	);
};
