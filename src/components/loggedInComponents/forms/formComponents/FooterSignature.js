import React from 'react';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from '../../../shared/DateField';
import { footerInputSignature } from '../../../../utils/data/genericFormData';
import { isBirtamod, isInaruwa } from '../../../../utils/clientUtils';
import { ImageDisplayInline } from '../../../shared/file/FileView';

export const FooterSignature = ({ designations = [], signatureImages = [] }) => {
	return (
		<div className="signature-div flex-item-space-between extra-top-margin" style={{ alignItems: 'baseline' }}>
			{designations.map((designation, index) => (
				<div key={index} style={{ textAlign: 'center', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
					<div>{signatureImages[index] && <ImageDisplayInline height={35} alt="user signature" src={signatureImages[index]} />}</div>
					<div>
						<span className="ui input signature-placeholder" />
					</div>
					<div>{designation}</div>
				</div>
			))}
		</div>
	);
};

export const FooterSignatureMultiline = ({ designations = [], signatureImages = [] }) => {
	return (
		<div className="signature-div flex-item-space-between extra-top-margin" style={{ alignItems: 'baseline' }}>
			{designations.map((designation, index) => (
				<div key={index} style={{ textAlign: 'center', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
					<div>{signatureImages[index] && <ImageDisplayInline height={35} alt="user signature" src={signatureImages[index]} />}</div>
					<div>
						<span className="ui input signature-placeholder" />
					</div>
					{designation.map((line, idx) => (
						<div key={idx}>{line}</div>
					))}
				</div>
			))}
		</div>
	);
};

export const FooterSignatureInput = ({ values, errors, setFieldValue, enterDateField }) => {
	const designations = getDesignations(enterDateField);
	return (
		<div className="signature-div flex-item-space-between">
			{designations.map((designation, index) => (
				<div key={index} style={{ textAlign: 'left' }}>
					<p>{designation.label}</p>
					<br />
					<p>
						{footerInputSignature.sahi}
						<span className="ui input dashedForm-control" />
					</p>
					<div>
						{footerInputSignature.name}
						<DashedLangInput
							name={designation.nameField}
							setFieldValue={designation.editable ? setFieldValue : undefined}
							readOnly={!designation.editable ? true : undefined}
							value={values[designation.nameField]}
							error={errors[designation.nameField]}
						/>
					</div>
					<div>
						{footerInputSignature.pad}
						<DashedLangInput
							name={designation.designationField}
							setFieldValue={designation.editable ? setFieldValue : undefined}
							readOnly={!designation.editable ? true : undefined}
							value={values[designation.designationField]}
							error={errors[designation.designationField]}
						/>
					</div>
					<div>
						{footerInputSignature.date}
						<DashedLangDateField
							name={designation.dateField}
							// handleChange={undefined}
							// setFieldValue={designation.editable ? setFieldValue : undefined}
							setFieldValue={setFieldValue}
							// readOnly={!designation.editable ? true : undefined}
							inline={true}
							compact={true}
							value={values[designation.dateField]}
							error={errors[designation.dateField]}
						/>
					</div>
				</div>
			))}
		</div>
	);
};

export const getDesignations = (enterDateField) => {
	return isBirtamod
		? [
				{
					label: footerInputSignature.tayar,
					nameField: 'subName',
					designationField: 'subDesignation',
					signatureField: 'subSignature',
					dateField: enterDateField,
					editable: true,
				},
				{
					label: footerInputSignature.pes,
					nameField: 'erName',
					designationField: 'erDesignation',
					signatureField: 'erSignature',
					dateField: 'erDate',
					editable: true,
				},
				{
					label: footerInputSignature.sadar,
					nameField: 'chiefName',
					designationField: 'chiefDesignation',
					signatureField: 'chiefSignature',
					dateField: 'chiefDate',
					editable: true,
				},
		  ]
		: isInaruwa
		? [
				{
					label: footerInputSignature.pes,
					nameField: 'subName',
					designationField: 'subDesignation',
					signatureField: 'subSignature',
					dateField: enterDateField,
					editable: true,
				},
				{
					label: footerInputSignature.ruju,
					nameField: 'erName',
					designationField: 'erDesignation',
					signatureField: 'erSignature',
					dateField: 'erDate',
					editable: true,
				},
				{
					label: footerInputSignature.swikrit,
					nameField: 'chiefName',
					designationField: 'chiefDesignation',
					signatureField: 'chiefSignature',
					dateField: 'chiefDate',
					editable: true,
				},
		  ]
		: [
				{
					label: footerInputSignature.pes,
					nameField: 'subName',
					designationField: 'subDesignation',
					signatureField: 'subSignature',
					dateField: enterDateField,
					editable: false,
				},
				{
					label: footerInputSignature.sipharis,
					nameField: 'erName',
					designationField: 'erDesignation',
					signatureField: 'erSignature',
					dateField: 'erDate',
					editable: true,
				},
				{
					label: footerInputSignature.swikrit,
					nameField: 'chiefName',
					designationField: 'chiefDesignation',
					signatureField: 'chiefSignature',
					dateField: 'chiefDate',
					editable: true,
				},
		  ];
};

export const DetailedSignature = ({ values, errors, setFieldValue, enterDateField, designation, children }) => {
	return React.Children.map(children, (child) => {
		return React.cloneElement(child, {
			setFieldValue,
			errors,
			values,
			enterDateField,
			designation,
		});
	});
};

DetailedSignature.Name = ({ name, setFieldValue, errors, values, label, designation, className }) => {
	const fieldName = name || designation.nameField;
	return (
		<div className={className}>
			{label ? label : footerInputSignature.name}
			<DashedLangInput
				name={fieldName}
				setFieldValue={designation ? (designation.editable ? setFieldValue : undefined) : setFieldValue}
				readOnly={designation ? (!designation.editable ? true : undefined) : undefined}
				value={values[fieldName]}
				error={errors[fieldName]}
			/>
		</div>
	);
};

DetailedSignature.Designation = ({ name, setFieldValue, errors, values, label, designation, className }) => {
	const fieldName = name || designation.designationField;
	return (
		<div className={className}>
			{label ? label : footerInputSignature.pad}
			<DashedLangInput
				name={fieldName}
				setFieldValue={designation ? (designation.editable ? setFieldValue : undefined) : setFieldValue}
				readOnly={designation ? (!designation.editable ? true : undefined) : undefined}
				value={values[fieldName]}
				error={errors[fieldName]}
			/>
		</div>
	);
};

DetailedSignature.Date = ({ name, setFieldValue, errors, values, label, designation, className }) => {
	const fieldName = name || designation.dateField;
	return (
		<div className={`${className} signature-date`}>
			<DashedLangDateField
				label={label ? label : footerInputSignature.date}
				name={fieldName}
				setFieldValue={designation ? (designation.editable ? setFieldValue : undefined) : setFieldValue}
				readOnly={designation ? (!designation.editable ? true : undefined) : undefined}
				value={values[fieldName]}
				error={errors[fieldName]}
				inline={true}
				compact={true}
			/>
		</div>
	);
};

DetailedSignature.Signature = ({ name, showSignature, values, label, className, designation }) => {
	const fieldName = name || designation.signatureField;
	return (
		<div className={className}>
			{showSignature && values[fieldName] ? (
				<ImageDisplayInline
					label={label ? label : footerInputSignature.dashtakhat}
					height={35}
					alt="user signature"
					src={values[fieldName]}
				/>
			) : (
				<div>
					{label ? label : footerInputSignature.dashtakhat} <span className="ui input dashedForm-control" />
				</div>
			)}
		</div>
	);
};
