import React from 'react';
import { Field, getIn } from 'formik';
import { Dropdown } from 'semantic-ui-react';
import { shreeOptions } from '../../../../utils/optionUtils';

export const ShreeDropdown = ({ label, name, options = shreeOptions }) => {
	return (
		<Field>
			{({ form, field }) => (
				<>
					{label && `${label}`}{' '}
					<div style={{ display: 'inline-flex' }}>
						<Dropdown
							name={name}
							onChange={(e, { value }) => form.setFieldValue(name, value)}
							value={getIn(form.values, name)}
							options={options}
						/>
					</div>
				</>
			)}
		</Field>
	);
};
