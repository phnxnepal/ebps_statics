import React from 'react';
import PropTypes from 'prop-types';

export const LetterSalutation = ({ lines, boldLines = [] }) => {
	return (
		<div>
			{lines.map((line, index) => (
				<div key={index}>{boldLines.includes(index) ? <b>{line}</b> : line}</div>
			))}
		</div>
	);
};

LetterSalutation.propTypes = {
	lines: PropTypes.array.isRequired,
};
