import React from 'react';
import { permitInfo } from '../../../../utils/data/genericFormData';
import { ShreeDropdown } from './ShreeDropdown';

export const HaalSection = ({ permitData }) => {
	return (
		<>
			{permitInfo.haal} {permitData.newMunicipal} {permitInfo.wardNo} {permitData.newWardNo} {permitInfo.sabik} {permitData.oldMunicipal}{' '}
			{permitInfo.kitaa} {permitData.kittaNo} {permitInfo.area} {permitData.landArea} {permitData.landAreaType}
		</>
	);
};

export const UserOldInfo = ({ permitData, userData }) => {
	return (
		<>
			<span className="indent-span">{permitInfo.uparoktaSambandha}</span> {userData.organization.name} {permitInfo.wardNo} {permitData.oldWardNo}{' '}
			{permitInfo.sadak} {permitData.nibedakSadak} {permitInfo.basne} <ShreeDropdown name="shree" /> {permitData.applicantName}
		</>
	);
};
