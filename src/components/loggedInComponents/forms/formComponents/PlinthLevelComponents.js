import React from 'react';
import { Table } from 'semantic-ui-react';
import * as Yup from 'yup';

import { YesNoOnlyRadio, YesNoRadio } from './YesNoRadio';
import { pillarGaraTable, yesNoSection } from '../../../../utils/data/plinthLevelFormsData';
import { SectionHeader } from '../../../uiComponents/Headers';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { commonMessages } from '../../../../utils/data/validationData';

const bodyData = yesNoSection;
export const PillarGaraTable = () => {
	return (
		<Table className="left-ui-table">
			<Table.Header>
				<Table.Row>
					<Table.HeaderCell>{pillarGaraTable.table.pillar.title}</Table.HeaderCell>
					<Table.HeaderCell>{pillarGaraTable.table.gara.title}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				{pillarGaraTable.table.indices.map(idx => {
					const pillarObj = pillarGaraTable.table.pillar.lines[idx];
					const garaObj = pillarGaraTable.table.gara.lines[idx];

					return (
						<Table.Row key={idx}>
							<Table.Cell>
								<YesNoOnlyRadio space={true} fieldLabel={pillarObj.line} name={pillarObj.name} options={pillarObj.options} />
							</Table.Cell>
							<Table.Cell>{garaObj && <YesNoOnlyRadio space={true} fieldLabel={garaObj.line} name={garaObj.name} />}</Table.Cell>
						</Table.Row>
					);
				})}
			</Table.Body>
		</Table>
	);
};

export const YesNoSection = ({ setFieldValue, values, errors, sansodhan = null }) => {
	return (
		<div>
			<SectionHeader>
				<p className="underline left-align">{bodyData.title}</p>
			</SectionHeader>
			<ol className="ol-div">
				{bodyData.yesNoLines.map((obj, index) => (
					<li key={index}>
						<YesNoRadio
							name={obj.name}
							otherName={obj.otherName}
							fieldLabel={obj.line}
							setFieldValue={setFieldValue}
							values={values}
							errors={errors}
							space={true}
							options={obj.options}
							placeholder={obj.placeholder}
						/>{' '}
						{obj.suffix && obj.suffix}
					</li>
				))}
				<PillarGaraTable />
				{sansodhan && sansodhan}
				<li>
					{bodyData.anya}:
					<EbpsTextareaField name="anyaBibaran" setFieldValue={setFieldValue} value={values.anyaBibaran} error={errors.anyaBibaran} />
				</li>
			</ol>
		</div>
	);
};

export const yesNoSectionSchema = {
	mapdandaPalana: Yup.string().when('mapdandaPalanaBhayeko', {
		is: val => val === 'N',
		then: Yup.string().required(commonMessages.required),
	}),
	setBackPlan: Yup.string().when('setBackPlanBhayeko', {
		is: val => val === 'N',
		then: Yup.string().required(commonMessages.required),
	}),
	groundCoverage: Yup.string().when('groundCoverageBhayeko', {
		is: val => val === 'Y',
		then: Yup.string().required(commonMessages.required),
	}),
	changesMade: Yup.string().when('changesMadeYes', {
		is: val => val === 'N',
		then: Yup.string().required(commonMessages.required),
	}),
	buildingConstFollow: Yup.string().when('buildingConstFollowBhayeko', {
		is: val => val === 'N',
		then: Yup.string().required(commonMessages.required),
	}),
};
