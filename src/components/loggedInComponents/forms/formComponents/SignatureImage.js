import React from 'react';
import { footerInputSignature } from '../../../../utils/data/genericFormData';
import { ImageDisplayInline } from '../../../shared/file/FileView';

export const SignatureImage = ({ className, value, showSignature, label }) => {
	return (
		<div className={className}>
			{showSignature && value ? (
				<ImageDisplayInline label={label ? label : footerInputSignature.dashtakhat} height={35} alt="user signature" src={value} />
			) : (
				<div>
					{label ? label : footerInputSignature.dashtakhat} <span className="ui input dashedForm-control" />
				</div>
			)}
		</div>
	);
};

export const SignatureImageVertical = ({ className, value, showSignature, label }) => {
	return (
		<div className={className} style={{ textAlign: 'center', display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
			<div>{showSignature && value && <ImageDisplayInline height={35} alt="user signature" src={value} />}</div>
			<div>
				<span className="ui input signature-placeholder" />
			</div>
			<div>{label ? label : ''}</div>
		</div>
	);
};
