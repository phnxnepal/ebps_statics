import React from 'react';

export const Subject = ({ subjects = [] }) => {
	return (
		<div>
			<h3 className="underlined-centered-subject">
				{subjects.map((subject, index) => (
					<p key={index} className="neighbours-para">
						<span>{subject}</span>
					</p>
				))}
			</h3>
		</div>
	);
};
