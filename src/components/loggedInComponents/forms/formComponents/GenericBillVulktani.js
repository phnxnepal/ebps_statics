import React from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';

import { billVuktaniData } from '../../../../utils/data/PahiloCharanBillVuktaniData';
import { useRajasowBillForm } from '../../../../hooks/useBillForm';
import { getJsonData, checkError } from '../../../../utils/dataUtils';
import { useFormSuccess } from '../../../../hooks/useFormSuccess';
import { billSchema } from '../../formValidationSchemas/billSchemas';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import EbpsForm, { EbpsNormalForm, EbpsSlashForm } from '../../../shared/EbpsForm';
import { DashedLangDateField } from '../../../shared/DateField';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

const data = billVuktaniData;
export const GenericBillVulktani = (props) => {
	const { initialValues } = useRajasowBillForm(JSON.stringify(getJsonData(props.prevData)), props.userData.userName, props.rajasowData);

	const { api, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, titleKey } = props;
	const jsonData = checkError(props.prevData);
	const statuses = jsonData
		? (({ erStatus, serStatus, rwStatus, aminiStatus, chiefStatus, eStatus, fStatus, gStatus }) => ({
				erStatus,
				serStatus,
				rwStatus,
				aminiStatus,
				chiefStatus,
				eStatus,
				fStatus,
				gStatus,
		  }))(jsonData)
		: {};

	const [setFormSubmitted] = useFormSuccess(formUrl, JSON.stringify(props.success), JSON.stringify(statuses));

	return (
		<Formik
			initialValues={initialValues}
			enableReinitialize
			validationSchema={billSchema}
			onSubmit={async (values, { setSubmitting }) => {
				setSubmitting(true);
				values.applicationNo = props.permitData.applicantNo;
				try {
					await props.postAction(api, values, true);
					window.scroll(0, 0);
					setFormSubmitted(true);
					setSubmitting(false);
				} catch (err) {
					setSubmitting(false);
					console.log('Error', err);
				}
			}}
			render={({ handleChange, values, isSubmitting, handleSubmit, setFieldValue, errors, validateForm }) => (
				<Form loading={isSubmitting}>
					{props.errors && <ErrorDisplay message={props.errors.message} />}
					<div ref={props.setRef}>
						<br />
						<div>
							<h2 style={{ textAlign: 'center' }}>{data[titleKey]}</h2>
							<EbpsNormalForm
								label={data.vuktaniRakam}
								name="vuktaniRakam"
								onChange={handleChange}
								value={values.vuktaniRakam}
								error={errors.vuktaniRakam}
							/>
							<EbpsSlashForm
								label={data.rashidNumber}
								name="rashidNumber"
								value={values.rashidNumber}
								error={errors.rashidNumber}
								setFieldValue={setFieldValue}
							/>
							<EbpsForm
								name="receiveSign"
								label={data.receiveSign}
								value={values.receiveSign}
								error={errors.receiveSign}
								setFieldValue={setFieldValue}
							/>
							<DashedLangDateField
								label={data.miti}
								name="miti"
								setFieldValue={setFieldValue}
								value={values.miti}
								error={errors.miti}
							/>
						</div>
					</div>
					<br />
					<SaveButtonValidation
						errors={errors}
						validateForm={validateForm}
						formUrl={formUrl}
						hasSavePermission={hasSavePermission}
						hasDeletePermission={hasDeletePermission}
						isSaveDisabled={isSaveDisabled}
						prevData={checkError(prevData)}
						handleSubmit={handleSubmit}
					/>
				</Form>
			)}
		/>
	);
};

GenericBillVulktani.propType = {
	titleKey: PropTypes.string.isRequired,
	api: PropTypes.string.isRequired,
};
