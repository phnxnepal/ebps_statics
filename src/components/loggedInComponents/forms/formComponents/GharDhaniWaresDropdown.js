import React from 'react';
import { Field, getIn } from 'formik';
import { Dropdown } from 'semantic-ui-react';
import { gharDhaniWaresOption } from '../../../../utils/optionUtils';

export const GharDhaniWaresDropdown = ({ label, name }) => {
	return (
		<Field>
			{({ form, field }) => (
				<>
					{label && `${label}`}{' '}
					<div style={{ display: 'inline-flex' }}>
						<Dropdown
							name={name}
							onChange={(e, { value }) => form.setFieldValue(name, value)}
							value={getIn(form.values, name)}
							options={gharDhaniWaresOption}
						/>
					</div>
				</>
			)}
		</Field>
	);
};
