import React from 'react';
import PropTypes from 'prop-types';
import { getPatraSankhya } from '../../../../utils/clientConfigs/certificate';
import { DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from '../../../shared/DateField';
import { date } from '../../../../utils/data/formCommonData';
import { getClientCode } from '../../../../utils/clientUtils';
import { getIn } from 'formik';

export const PatraSankhyaAndDate = ({ setFieldValue, values, errors, dateFieldName = 'allowanceDate' }) => {
	const { serialNumberFields } = getPatraSankhya(getClientCode());
	return (
		<div className="flex-item-space-between">
			<div>
				{serialNumberFields.map(field => (
					<div key={field.fieldName}>
						{field.label} :
						<DashedLangInputWithSlash
							compact={true}
							name={field.fieldName}
							setFieldValue={setFieldValue}
							value={getIn(values, field.fieldName)}
							error={getIn(errors, field.fieldName)}
						/>
					</div>
				))}
			</div>
			<div>
				{/* {Ndata.Sangyardate}: */}
				<DashedLangDateField
					name={dateFieldName}
					inline={true}
					setFieldValue={setFieldValue}
					value={values[dateFieldName]}
					label={date.date}
					error={errors[dateFieldName]}
				/>
			</div>
		</div>
	);
};

PatraSankhyaAndDate.propTypes = {
	setFieldValue: PropTypes.func.isRequired,
	values: PropTypes.object.isRequired,
	errors: PropTypes.object.isRequired,
};

export const PatraSankhya = ({ setFieldValue, values, errors }) => {
	const { serialNumberFields } = getPatraSankhya(getClientCode());
	return (
		<div>
			{serialNumberFields.map((field, idx) => (
				<div key={idx}>
					{field.label} :
					<DashedLangInputWithSlash
						compact={true}
						name={field.fieldName}
						setFieldValue={setFieldValue}
						value={getIn(values, field.fieldName)}
						error={getIn(errors, field.fieldName)}
					/>
				</div>
			))}
		</div>
	);
};
