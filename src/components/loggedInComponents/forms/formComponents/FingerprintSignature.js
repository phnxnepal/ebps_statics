import React from 'react';
import { Table } from 'semantic-ui-react';
import { getMessage } from '../../../shared/getMessage';
import { fingerPrintData } from '../../../../utils/data/genericFormData';
import { isBiratnagar } from '../../../../utils/clientUtils';

const messageId = 'fingerPrintData';

export const FingerprintSignature = () => {
	return (
		// <Grid>
		// 	<Grid.Row>
		// 		{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
		// 		<span className="ui input signature-placeholder" />
		// 	</Grid.Row>
		// 	<Grid.Row>
		// 		<Grid.Column width={6}>
		// 			<Table celled>
		// 				<Table.Header>
		// 					<Table.Row textAlign="center">
		// 						<Table.HeaderCell>{getMessage(`${messageId}.right`, fingerPrintData.right)}</Table.HeaderCell>
		// 						<Table.HeaderCell>{getMessage(`${messageId}.left`, fingerPrintData.left)}</Table.HeaderCell>
		// 					</Table.Row>
		// 				</Table.Header>
		// 				<Table.Body>
		// 					<Table.Row height="175px">
		// 						<Table.Cell></Table.Cell>
		// 						<Table.Cell></Table.Cell>
		// 					</Table.Row>
		// 				</Table.Body>
		// 			</Table>
		// 		</Grid.Column>
		// 	</Grid.Row>
		// </Grid>
		<div style={{ display: 'flex', justifyContent: 'flex-start' }}>
			<div className="finger-print-div">
				<div>
					{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
					<span className="ui input dashedForm-control" />
				</div>
				<br />
				<FingerPrint />
			</div>
		</div>
	);
};

export const FingerPrint = ({ vertical = false, height = 150 }) => (
	<div className={`${vertical ? 'finger-print-table-vertical' : 'finger-print-table'}`}>
		<Table celled compact>
			<Table.Header>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.right`, fingerPrintData.right)}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				<Table.Row height={height}>
					<Table.Cell></Table.Cell>
				</Table.Row>
			</Table.Body>
		</Table>
		<Table celled compact className="right-table">
			<Table.Header>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.left`, fingerPrintData.left)}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				<Table.Row height={height}>
					<Table.Cell></Table.Cell>
				</Table.Row>
			</Table.Body>
		</Table>
	</div>
);
export const FingerPrintBiratnager = ({ height = 60 }) => {
	return (
		<div className="finger-print-table-biratnagar">
			<Table celled compact>
				<Table.Header>
					<Table.Row textAlign="top" height={height}>
						<Table.HeaderCell>{getMessage(`${messageId}.right`)}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
			</Table>
			<Table celled compact className="right-table">
				<Table.Header>
					<Table.Row textAlign="top" height={height}>
						<Table.HeaderCell>{getMessage(`${messageId}.left`, fingerPrintData.left)}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
			</Table>
		</div>
	);
};

export const FingerprintSignatureBirtamod = () => {
	return (
		<div style={{ display: 'flex', justifyContent: 'flex-end' }}>
			<div className="finger-print-div">
				<div>
					{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
					<span className="ui input dashedForm-control" />
				</div>
				<br />
				<FingerPrintBirtamod />
			</div>
		</div>
	);
};

export const FingerprintSignatureBirtamodVertical = () => {
	return (
		<div className="vertical_grid">
			<div className="finger-print-div-vertical">
				<FingerPrintBirtamod height={100} vertical={true} />
				<div>
					{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
					<span className="ui input dashedForm-control" />
				</div>
			</div>
		</div>
	);
};

export const FingerprintSignatureVertical = () => {
	return (
		<div className="vertical_grid">
			<div className="finger-print-div-vertical">
				<FingerPrint height={100} vertical={true} />
				{isBiratnagar ? null : (
					<div>
						{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
						<span className="ui input dashedForm-control" />
					</div>
				)}
			</div>
			{/* <Grid>
				<Grid.Row>
					<Grid.Column>
						<Table celled>
							<Table.Header>
								<Table.Row textAlign="center">
									<Table.HeaderCell>{getMessage(`${messageId}.right`, fingerPrintData.right)}</Table.HeaderCell>
									<Table.HeaderCell>{getMessage(`${messageId}.left`, fingerPrintData.left)}</Table.HeaderCell>
								</Table.Row>
							</Table.Header>
							<Table.Body>
								<Table.Row height="140px">
									<Table.Cell></Table.Cell>
									<Table.Cell></Table.Cell>
								</Table.Row>
							</Table.Body>
						</Table>
					</Grid.Column>
				</Grid.Row>
			</Grid>
			<div>
				{getMessage(`${messageId}.signature`, fingerPrintData.signature)}
				<span className="ui input dashedForm-control" />
			</div> */}
		</div>
	);
};

export const FingerPrintBirtamod = ({ height = 150, vertical = false }) => (
	<div className={`${vertical ? 'finger-print-table-vertical' : 'finger-print-table'}`}>
		<Table celled compact>
			<Table.Header>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.right`, fingerPrintData.right)}</Table.HeaderCell>
				</Table.Row>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.lyapche`, fingerPrintData.lyapche)}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				<Table.Row height={height}>
					<Table.Cell></Table.Cell>
				</Table.Row>
			</Table.Body>
		</Table>
		<Table celled compact className="right-table">
			<Table.Header>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.left`, fingerPrintData.left)}</Table.HeaderCell>
				</Table.Row>
				<Table.Row textAlign="center">
					<Table.HeaderCell>{getMessage(`${messageId}.lyapche`, fingerPrintData.lyapche)}</Table.HeaderCell>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				<Table.Row height={height}>
					<Table.Cell></Table.Cell>
				</Table.Row>
			</Table.Body>
		</Table>
	</div>
);
