import React from 'react';
import { getMessage } from '../../../shared/getMessage';
import { longFormDate } from '../../../../utils/data/genericFormData';
import { isBirtamod } from '../../../../utils/clientUtils';

const messageId = 'longFormDate';
const data = longFormDate;

const LongFormDate = () => {
	return (
		<div>
			{getMessage(`${messageId}.ad`, data.ad)}
			<span className="ui input dashedForm-control" />
			{getMessage(`${messageId}.year`, data.year)}
			<span className="ui input dashedForm-control" />
			{getMessage(`${messageId}.month`, data.month)}
			<span className="ui input dashedForm-control" />
			{getMessage(`${messageId}.end`, data.end)}
		</div>
	);
};
export const LongFormDateNotice = () => {
	return (
		<div>
			{isBirtamod ? getMessage(`${messageId}.date`, data.date) : getMessage(`${messageId}.ad`, data.ad)}
			<span className="small-dashed" />
			{getMessage(`${messageId}.year`, data.year)}
			<span className="small-dashed" />
			{getMessage(`${messageId}.monthOnly`, data.monthOnly)}
			<span className="small-dashed" />
			{getMessage(`${messageId}.gate`, data.gate)}
			{!isBirtamod && (
				<>
					<span className="ui input dashedForm-control" />
					{getMessage(`${messageId}.endRojSuvam`, data.endRojSuvam)}
				</>
			)}
		</div>
	);
};

export const GenericLongDate = ({ datas, last }) => {
	return (
		<div>
			{datas.map((data, index) => (
				<React.Fragment key={index}>
					{data} <span className="ui input dashedForm-control" />
				</React.Fragment>
			))}
			{last}
		</div>
	);
};
export default LongFormDate;
