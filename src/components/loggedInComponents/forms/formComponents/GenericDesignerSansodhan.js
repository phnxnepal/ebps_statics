import { prepareMultiInitialValues, getJsonData, handleSuccess, checkError } from '../../../../utils/dataUtils';
import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { Formik } from 'formik';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

export class GenericDesignerSansodhan extends Component {
	constructor(props) {
		super(props);

		const { fieldName, prevData } = this.props;
		const initialValues = prepareMultiInitialValues(
			{ obj: { [fieldName]: '' }, reqFields: [] },
			{
				obj: getJsonData(prevData),
				reqFields: [],
			}
		);

		this.state = {
			initialValues,
		};
	}

	render() {
		const { topic, fieldName, prevData, permitData, api, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialValues } = this.state;
		return (
			<>
				<Formik
					initialValues={initialValues}
					onSubmit={async (values, { setSubmitting }) => {
						values.applicationNo = permitData.applicantNo;
						try {
							await this.props.postAction(api, values, true);

							window.scroll(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(prevData), this.props.parentProps, this.props.success);
								setSubmitting(false);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, isSubmitting, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<div className="section-header">
									<h2 className="end-section">{topic}</h2>
								</div>

								<EbpsTextArea placeholder="निवेदन" name={fieldName} setFieldValue={setFieldValue} value={values[fieldName]} />
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				/>
			</>
		);
	}
}
