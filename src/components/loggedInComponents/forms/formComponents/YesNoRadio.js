import React from 'react';
import { bhayekoOptions } from '../../../../utils/optionUtils';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { yesNoData } from '../../../../utils/data/genericFormData';
import { getIn } from 'formik';

export const YesNoRadio = ({
	name,
	otherName,
	space = false,
	fieldLabel,
	values,
	setFieldValue,
	errors,
	options = bhayekoOptions,
	placeholder = yesNoData.nabhayeko,
}) => {
	return (
		<>
			{fieldLabel}{' '}
			{options.map(option => (
				<React.Fragment key={option.key}>
					<RadioInput space={space} name={name} option={option.value} label={option.text} />
				</React.Fragment>
			))}
			{values[name] && values[name] === options[1].value && (
				<>
					<EbpsTextareaField
						placeholder={placeholder}
						name={otherName}
						setFieldValue={setFieldValue}
						value={getIn(values, otherName)}
						error={getIn(errors, otherName)}
					/>
				</>
			)}
		</>
	);
};

export const YesNoOnlyRadio = ({ name, space = false, fieldLabel, options = bhayekoOptions }) => {
	return (
		<>
			{fieldLabel}{' '}
			{options.map(option => (
				<React.Fragment key={option.key}>
					<RadioInput space={space} name={name} option={option.value} label={option.text} />
				</React.Fragment>
			))}
		</>
	);
};
