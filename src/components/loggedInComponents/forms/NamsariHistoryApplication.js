import React, { useState, useEffect } from 'react';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../utils/paramUtil';
import api from '../../../utils/api';
import { Table } from 'semantic-ui-react';
import { namsariApplicationTableData, namsariApplicationData } from '../../../utils/data/ebps-setup/namsariSetupData';
import { SectionHeader } from '../../uiComponents/Headers';
import { CompactInfoMessage } from '../../shared/formComponents/CompactInfoMessage';
import { getJsonData } from '../../../utils/dataUtils';

const tableData = namsariApplicationTableData;

const NamsariHistoryAppilcationComponent = ({ otherData, permitData, setRef, prevData }) => {
	const [historyList, setHistoryList] = useState([]);
	const [certificateNote, setCertificateNote] = useState({});

	useEffect(() => {
		const list = otherData.historyList ? otherData.historyList.nameTransferHistory : [];
		setHistoryList(
			list
				.filter((row) => row.applicationNo === permitData.applicantNo)
				.map((row) => ({
					...row,
					sasthaYaExtraName: certificateNote.id === row.id ? certificateNote.sasthaYaExtraName : '',
				}))
		);
	}, [otherData.historyList, permitData, certificateNote]);

	useEffect(() => {
		const certificateNote = prevData ? getJsonData(prevData) : {};
		setCertificateNote({ ...certificateNote, id: prevData.id });
	}, [prevData]);

	return (
		<div ref={setRef}>
			<SectionHeader>
				<h3>{namsariApplicationData.namsariTitle}</h3>
			</SectionHeader>
			{historyList.length < 1 ? (
				<CompactInfoMessage content={namsariApplicationData.notNamsaried} />
			) : (
				<Table>
					<Table.Header>
						<Table.Row>
							{tableData.map((row) => (
								<Table.HeaderCell key={row.dataField}>{row.text}</Table.HeaderCell>
							))}
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{historyList.map((el) => (
							<Table.Row key={el.applicationNo}>
								{tableData.map((row) => (
									<Table.Cell key={row.dataField}>{el[row.dataField]}</Table.Cell>
								))}
							</Table.Row>
						))}
					</Table.Body>
				</Table>
			)}
		</div>
	);
};

const NamsariHistoryAppilcation = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.certificateNote).setForm().getParams(), new ApiParam(api.namsariHistory, 'historyList').setUtility().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		viewOnly
		render={(props) => <NamsariHistoryAppilcationComponent {...props} parentProps={parentProps} />}
	/>
);
export default NamsariHistoryAppilcation;
