import React, { Component } from 'react';
import { aminKoPratibedan } from '../../../utils/data/AminKoSthalgatData';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import api from '../../../utils/api';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import { EbpsTextArea } from '../../shared/EbpsForm';
import { LetterHeadFlex } from '../../shared/LetterHead';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
const data = aminKoPratibedan;

class AminKoPratibedanComponent extends Component {
	constructor(props) {
		super(props);

		let initVal = { aminKoPratibedan: '' };
		const { prevData } = this.props;

		const json_data = getJsonData(prevData);

		initVal = { ...initVal, ...json_data };

		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { permitData, userData, hasSavePermission, formUrl, prevData, isSaveDisabled, hasDeletePermission } = this.props;
		return (
			<>
				<Formik
					initialValues={initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						try {
							await this.props.postAction(api.aminkoPratibedan, values, true);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />
								<div className="section-header">
									<h2 className="underline">{data.tippani}</h2>
									<br />
									<h3 className="underline">{data.topic}</h3>
								</div>
								<br />
								<EbpsTextArea
									placeholder="निवेदन"
									name="aminKoPratibedan"
									setFieldValue={setFieldValue}
									onChange={handleChange}
									value={values.aminKoPratibedan}
								/>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const AminKoPratibedan = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.aminkoPratibedan,
				objName: 'aminPratibedan',
				form: true,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			param4: ['getElementsByTagName', 'textarea', 'value'],
		}}
		useInnerRef={true}
		render={props => <AminKoPratibedanComponent {...props} parentProps={parentProps} />}
	/>
);

export default AminKoPratibedan;
