import React, { Component } from 'react';
import { Formik } from 'formik';
import { KaryaSampana } from '../../../utils/data/certificateinstructiondata';
import { Form } from 'semantic-ui-react';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { Select } from 'semantic-ui-react';
import api from '../../../utils/api';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import { prepareMultiInitialValues } from './../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import * as Yup from 'yup';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import { getDesignations, DetailedSignature } from './formComponents/FooterSignature';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { getApproveByObject, getSaveByUserDetails } from '../../../utils/formUtils';
import { footerInputSignature } from '../../../utils/data/genericFormData';
import { PrintParams } from '../../../utils/printUtils';
import { isKamalamai } from '../../../utils/clientUtils';

const ksamp = KaryaSampana.KaryaSampana_data;
const kamalamaiData = KaryaSampana.kamalamai;
const Options = [
	{ key: 'a1', value: 'सेटव्याक', text: ' सेटव्याक' },
	{ key: 'a2', value: 'सडक सेटव्याक', text: 'सडक सेटव्याक' },
];
const meroOpt = [
	{ key: 'a1', value: 'मेरो', text: 'मेरो' },
	{ key: 'a2', value: 'हाम्रो', text: 'हाम्रो' },
];
const prabhidikOpt = [
	{ key: 'a1', value: 'कन्सल्टेन्ट', text: 'कन्सल्टेन्ट' },
	{ key: 'a2', value: 'को ', text: 'को' },
];
const CertificateInstSchema = Yup.object().shape(
	Object.assign({
		certificateInstDate: validateNullableNepaliDate,
		certificatePeshDate: validateNullableNepaliDate,
		date: validateNullableNepaliDate,
		erDate: validateNullableNepaliDate,
		chiefDate: validateNullableNepaliDate,
	})
);
class CertificateInstructionViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData, enterByUser, userData, hasDesignerChanged, newDesignerName } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const rujuData = getApproveBy(1);
		// const swikritData = getApproveBy(1);

		const json_data = getJsonData(prevData);
		const SupSturData = getJsonData(otherData.superBuild);
		const sansodhanSuper = getJsonData(otherData.sansodhanSuperStructure);
		const mapTech = getJsonData(otherData.mapTech);
		// console.log(SupSturData);
		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// serInfo.subName = enterByUser.name;
		// serInfo.subDesignation = enterByUser.designation;

		// let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			{
				obj: SupSturData,
				reqFields: ['municipalDate'],
			},
			{
				obj: sansodhanSuper,
				reqFields: ['date'],
			},
			{
				obj: {
					consultant: 'कन्सल्टेन्ट',
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['consultancyName'],
			},
			{
				obj: { userName: hasDesignerChanged ? newDesignerName || mapTech.userName : mapTech.userName },
				reqFields: [],
			},
			// {
			// 	obj: serInfo,
			// 	reqFields: [],
			// },
			// { obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{
				obj: {
					erName: rujuData.name,
					erDesignation: rujuData.designation,
					erSignature: rujuData.signature,
					erDate: rujuData.date,
					// chiefName: swikritData.name,
					// chiefDesignation: swikritData.designation,
					// chiefSignature: swikritData.signature,
					// chiefDate: swikritData.date,
				},
				reqFields: [],
			},
			/**
			 * @TODO confirm this. will this override the above object?
			 */
			{ obj: json_data, reqFields: [] }
		);

		if (isStringEmpty(initVal.certificateInstDate)) {
			initVal.certificateInstDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.certificatePeshDate)) {
			initVal.certificatePeshDate = getCurrentDate(true);
		}
		this.state = {
			initVal,
			designations: getDesignations('certificatePeshDate'),
		};
	}
	render() {
		const { initVal, designations } = this.state;
		const {
			permitData,
			prevData,
			errors: reduxErrors,
			userData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;
		const pdata = permitData;
		const udata = userData;
		return (
			<div style={{ textAlign: 'justify' }}>
				<Formik
					initialValues={initVal}
					validationSchema={CertificateInstSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.certificateInstruction}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								// 	getNextUrl(
								// 		this.props.parentProps.location.pathname
								// 	)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, validateForm, errors, setFieldValue }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef} style={{ textAlign: 'justify' }} className="build-gap">
								<LetterHeadFlex userInfo={userData} />

								<SectionHeader>
									<h3 className="underline">{ksamp.heading.heading_4}</h3>
								</SectionHeader>
								<FlexSingleRight>
									<DashedLangDateField
										name="certificateInstDate"
										setFieldValue={setFieldValue}
										value={values.certificateInstDate}
										label={ksamp.date.date_1}
										inline={true}
										compact={true}
										error={errors.certificateInstDate}
									/>
								</FlexSingleRight>
								<br />
								{isKamalamai ? (
									<>
										<SectionHeader>
											<h3 className="underline end-section">{kamalamaiData.subject.subject_1}</h3>
										</SectionHeader>
										{console.log(userData.organization.name)}
										<div>
											<span>{kamalamaiData.data.data_1_1}</span>
											{userData.organization.name}
											<span>{kamalamaiData.data.data_1_2}</span>
											
											<span>{pdata.newWardNo}</span>

											<span>{kamalamaiData.data.data_2}</span>
											<span>{udata.userName}</span>

											<span>{kamalamaiData.data.data_3}</span>
											<span>{pdata.newWardNo}</span>

											<span>{kamalamaiData.data.data_4}</span>
											<DashedLangInput setFieldValue={setFieldValue} />

											<span>{kamalamaiData.data.data_5}</span>
											<span>{pdata.newWardNo}</span>

											<span>{kamalamaiData.data.data_6}</span>
											<span>{pdata.kittaNo}</span>

											<span>{kamalamaiData.data.data_7}</span>
											<span>{pdata.oldMunicipal}</span>

											<span>{kamalamaiData.data.data_8}</span>
											<DashedLangInput
												name="data8"
												value={values.data8}
												error={errors.data8}
												setFieldValue={setFieldValue}
											/>

											<span>{kamalamaiData.data.data_9}</span>
											<DashedLangDateField
												name="date1"
												handleChange={handleChange}
												value={values.date1}
												error={errors.date1}
												setFieldValue={setFieldValue}
												inline={true}
											/>

											<span>{kamalamaiData.data.data_10}</span>
											<DashedLangInput 
												name="data10"
												value={values.data10}
												error={errors.data10} 
												setFieldValue={setFieldValue} 
											/>

											<span>{kamalamaiData.data.data_11}</span>
											<DashedLangInput 
												name="data11"
												value={values.data11}
												error={errors.data11} 
												setFieldValue={setFieldValue} 
											/>

											<span>{kamalamaiData.data.data_12}</span>


										</div>
										
									</>
								) : (
									<>
										<SectionHeader>
											<h3 className="underline end-section">{ksamp.subject.subject_1}</h3>
										</SectionHeader>
										<div className="no-margin-field">
											<span>{udata.organization.name}</span>
											<span>{ksamp.data_1.data_1_2}</span>
											<span>{pdata.nibedakTol}</span>
											<span>{ksamp.data_1.data_1_3}</span>
											<span>{pdata.nibedakName}</span> <span>{ksamp.data_1.data_1_4}</span>{' '}
											<Select
												placeholder="मेरो"
												name="mero"
												options={meroOpt}
												defaultValue="मेरो"
												value={values.mero}
												onChange={(e, { value }) => setFieldValue('mero', value)}
											/>{' '}
											<span>{ksamp.data_1.data_1_8}</span>
											<span>{pdata.oldMunicipal}</span>
											<span>{ksamp.data_1.data_1_6}</span>
											<span>{pdata.newWardNo}</span>
											<span>{ksamp.data_1.data_1_7}</span> <span>{pdata.buildingJoinRoad}</span>
											<span>{ksamp.data_1.data_1_12}</span>
											<span>{pdata.kittaNo}</span>
											<span>{ksamp.data_1.data_1_13}</span> <span>{pdata.landArea}</span> <span>{pdata.landAreaType}</span>
											<span>{ksamp.data_1.data_1_14}</span>{' '}
											<DashedLangInput
												name="ChetrafalMa"
												setFieldValue={setFieldValue}
												value={values.ChetrafalMa}
												error={errors.ChetrafalMa}
											/>
											<span>{ksamp.data_1.data_1_15}</span>{' '}
											<DashedLangDateField
												label={udata.enterDate}
												name="date"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.date}
												error={errors.date}
											/>{' '}
											<span>{ksamp.data_1.data_1_16}</span>
											<DashedLangInput
												name="Patralei"
												setFieldValue={setFieldValue}
												value={values.Patralei}
												error={errors.Patralei}
											/>{' '}
											<span>{ksamp.data_1.data_1_17}</span>{' '}
											{values.consultant === 'कन्सल्टेन्ट' ? `${values.consultancyName}` : `${values.userName}`}{' '} {/* showing undefine, not my code*/}
											<Select
												name="consultant"
												options={prabhidikOpt}
												value={values.consultant}
												onChange={(e, { value }) => setFieldValue('consultant', value)}
											/>
											<span>{ksamp.data_1.data_1_18}</span>
											<Select
												placeholder="सेटव्याक"
												name="setBack"
												options={Options}
												defaultValue="सेटव्याक"
												value={values.setBack}
												onChange={(e, { value }) => setFieldValue('setBack', value)}
											/>
											<span>{ksamp.data_1.data_1_20}</span>
										</div>
									</>
								)}
								<br />
								<br />
								{isKamalamai ? null : (
									<div className="signature-div flex-item-space-between">
										{designations.map((designation, index) => (
											<div key={index} style={{ textAlign: 'left' }}>
												<p>{designation.label}</p>
												<DetailedSignature
													setFieldValue={setFieldValue}
													values={values}
													errors={errors}
													designation={designation}
												>
													<DetailedSignature.Signature
														label={footerInputSignature.sahi}
														showSignature={useSignatureImage}
													/>
													<DetailedSignature.Name />
													<DetailedSignature.Designation />
													<DetailedSignature.Date />
												</DetailedSignature>
											</div>
										))}
									</div>
								)}
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const CertificateInstructionView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.certificateInstruction,
				objName: 'certificateInst',
				form: true,
			},
			{
				api: api.superStructureBuild,
				objName: 'superBuild',
				form: false,
			},
			{
				api: api.sansodhanSupStruIjjaat,
				objName: 'sansodhanSuperStructure',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			// param1: ['getElementsByTagName', 'input', 'value'],
			// param4: ['getElementsByTagName', 'textarea', 'value'],
			// param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={isKamalamai}
		render={(props) => <CertificateInstructionViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default CertificateInstructionView;
