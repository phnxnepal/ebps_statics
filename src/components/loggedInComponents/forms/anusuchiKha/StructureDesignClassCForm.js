import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Table, Input, Message, Header, Label } from 'semantic-ui-react';
import { anushuchiKha } from '../../../../utils/data/mockLangFile';
import { isEmpty, getUserTypeValueNepali } from '../../../../utils/functionUtils';
// import { SemanticToastContainer } from 'react-semantic-toasts';
import 'react-semantic-toasts/styles/react-semantic-alert.css';

// Redux
import api from '../../../../utils/api';
import { getJsonData, prepareComplexInitialValues, getNameFormat, getFloorEnglish, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
// import StructureDesignClassCPrintView from './StructureDesignClassCPrintView';
import { checkError } from './../../../../utils/dataUtils';
import { getClassWiseNextUrl } from '../../../../utils/urlUtils';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { StructureDesignCPurano, StructureDesigneCBirtamodSchema } from '../../formValidationSchemas/structureDesignClassCValidation';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { EnglishField } from '../../../shared/fields/EnglishField';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import FormikCheckbox from '../../../shared/FormikCheckbox';

const data = anushuchiKha.structureDesign;
const table1 = data.table1;
const table2 = data.table2;
const table3 = data.table3;
const table4 = data.table4;
// const valueForInputHeader = ['Wallheight', 'Wallthickness', 'MaximumLength'];

class StructureDesignClassCFormComponent extends Component {
	constructor(props) {
		super(props);

		let initialValues = {};
		const permitData = this.props.permitData;
		const userData = this.props.userData;

		const otherData = {
			Nameofclient: permitData.nibedakName,
			Address: `${permitData.newMunicipal} - ${permitData.nibedakTol}`,
			ContactNo: permitData.applicantMobileNo,
		};

		let designerData = {};
		if (userData.userType === 'D')
			designerData = {
				userName: userData.userName,
				Designation: getUserTypeValueNepali(userData.userType),
				orgName: userData.organization.name,
				orgAddress: userData.organization.address,
				affDate: getCurrentDate(),
			};
		else {
			designerData = {
				userName: '',
				Designation: '',
				orgName: '',
				orgAddress: '',
				affDate: '',
			};
		}
		// const floorArray = new FloorArray(permitData.floor);
		// const topFloor = floorArray.getTopFloor();

		const floorArray = new FloorBlockArray(permitData.floor);
		const topFloor = floorArray.getTopFloor();
		const floorCount = floorArray.getFloorsWithLabels();
		const hasBlocks = floorArray.hasBlocks;

		let numberOfStorey = {};
		let sumOfHeights = {};

		if (floorArray.hasBlocks) {
			Object.entries(topFloor).forEach(([block, row], index) => (numberOfStorey[index] = row.englishCount));
			Object.entries(floorArray.getSumOfHeights()).forEach(([block, row], index) => (sumOfHeights[index] = row));
		} else {
			numberOfStorey = topFloor.englishCount;
			sumOfHeights = floorArray.getSumOfHeights();
		}

		// if (isStringEmpty(designerData.affDate)) {
		//   designerData.affDate = getCurrentDate(true);
		// }
		const jsonData = getJsonData(this.props.prevData);
		initialValues = prepareComplexInitialValues(
			{
				obj: otherData,
				userName: userData.userName,
				reqFields: ['Nameofclient', 'Address', 'ContactNo'],
			},
			{
				obj: designerData,
				reqFields: ['userName', 'Designation', 'orgName', 'orgAddress', 'affDate'],
			},
			{
				obj: {
					'1_A_2_noofstorey_qty': numberOfStorey,
					'1_A_2_totalheightofbuilding_qty': numberOfStorey,
				},
				reqFields: [],
			},
			{
				obj: jsonData,
				reqFields: [],
			}
		);
		this.state = {
			initialValues,
			floorCount,
			floorArray,
			hasBlocks,
			topFloor,
			'2_5_row3': 'Yes',
			'2_5_row4': 'Yes',
			'2_5_row5': 'Yes',
			'2_5_row6': 'Yes',
			'2_6_row1': 'Plint level',
		};
	}

	// componentDidMount() {
	//   this.props.getFormDataWithPermit(api.structureDesignC, data => data);
	// }

	handleRadioButtonClicked = e => {
		if (e.target.id !== null) {
			const id = e.target.id;
			const value = e.target.value;
			this.setState(() => ({
				[id]: value,
			}));
		}
	};

	render() {
		const { initialValues, floorCount, floorArray, hasBlocks, topFloor } = this.state;
		const { hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl, prevData, permitData } = this.props;

		return (
			<>
				{/* <div style={{ display: 'none' }}>
					<div ref={this.props.setRef}>
						<StructureDesignClassCPrintView />
					</div>
				</div> */}
				<Formik
					validationSchema={
						getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR
							? StructureDesignCPurano
							: // : isBirtamod
							  // ? StructureDesigneCBirtamodSchema
							  StructureDesigneCBirtamodSchema(hasBlocks)
						// : StructureDesignCSchema
					}
					initialValues={initialValues}
					onSubmit={async (values, actions) => {
						values = { ...values, ...this.state };
						// console.log('data to send', values);
						Object.keys(values).forEach(value => {
							if (value.includes('qty')) {
								// console.log('qty field ', value);
								const reccField = `${value.split('qty')[0]}recommendation`;
								if (!values[reccField]) {
									// console.log('', Variable);
									values[reccField] = '';
								}
							}
						});
						actions.setSubmitting(true);
						try {
							await this.props.postAction(`${api.structureDesignC}${this.props.permitData.applicantNo}`, values);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success) {
								let nextUrl = getClassWiseNextUrl(this.props.parentProps.location.pathname);
								// '/user/forms/anusuchiga-view'
								// // console.log('success', this.props.success);
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//   // getNextUrl(this.props.parentProps.location.pathname)
								//   '/user/forms/anusuchiga-view'
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success, nextUrl);
							}
						} catch (err) {
							// console.log('err struct form ', err);
							window.scrollTo(0, 0);
							actions.setSubmitting(false);
						}
					}}
					render={props => {
						// console.log('Props in render --', props);
						return (
							<Form
								// onSubmit={props.handleSubmit}
								loading={props.isSubmitting}
							>
								<div ref={this.props.setRef}>
									{/* <SemanticToastContainer /> */}
									<FormHeading />
									{!isEmpty(this.props.errors) && (
										<Message negative>
											<Message.Header>Error</Message.Header>
											<p>{this.props.errors.message}</p>
										</Message>
									)}
									{/* <FormWrapper erStatus={erStatus}> */}
									<Table celled collapsing compact className="english-div">
										{Object.keys(table1).map(row => (
											<Table.Row>
												<Table.Cell>
													<Header as="h7">{table1[row]}</Header>
												</Table.Cell>
												<Table.Cell>
													<Input
														name={table1[row].replace(/\s/g, '')}
														onChange={props.handleChange}
														value={props.values[table1[row].replace(/\s/g, '')]}
													/>
												</Table.Cell>
											</Table.Row>
										))}
									</Table>

									<h5 className="formFeild-mainLabel english-div">{table2.heading}</h5>
									<Table celled collapsing compact className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width={1}>S.N.</Table.HeaderCell>
												<Table.HeaderCell width={8}>Provision</Table.HeaderCell>
												<Table.HeaderCell width={3}>Area</Table.HeaderCell>
												<Table.HeaderCell width={4}>Recommendations</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											<Table.Row>
												<Table.HeaderCell colSpan={3}>{table2.sectionA.heading}</Table.HeaderCell>
											</Table.Row>
											{Object.keys(table2.sectionA.rows).map(rowKey => {
												const row = table2.sectionA.rows[rowKey];
												return (
													<Table.Row>
														<Table.Cell>{row.sn}</Table.Cell>
														<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
														<Table.Cell>
															{Object.keys(row.inputs).map(input => {
																const currentInput = row.inputs[input];
																const fieldName = currentInput.prefix
																	? `1_A_${row.sn}_${getNameFormat(currentInput.prefix)}_qty`
																	: `1_A_${row.sn}_qty`;
																if (currentInput.floorRelated) {
																	if (hasBlocks) {
																		return Object.entries(topFloor).map(([block, obj], index) => (
																			<LabeledTableInput
																				key={`${block}-${index}`}
																				name={`${fieldName}.${index}`}
																				prefix={`${currentInput.prefix} Block ${block}`}
																				suffix={currentInput.suffix}
																				value={getIn(props.values, `${fieldName}.${index}`)}
																				handleChange={props.handleChange}
																				error={getIn(props.errors, `${fieldName}.${index}`)}
																			/>
																		));
																	} else {
																		return (
																			<table key={currentInput.prefix}>
																				<LabeledTableInput
																					key={currentInput.prefix}
																					name={fieldName}
																					prefix={currentInput.prefix}
																					suffix={currentInput.suffix}
																					value={props.values[fieldName]}
																					handleChange={props.handleChange}
																					error={props.errors[fieldName]}
																				/>
																			</table>
																		);
																	}
																} else {
																	return (
																		<table key={currentInput.prefix}>
																			<LabeledTableInput
																				key={currentInput.prefix}
																				name={fieldName}
																				prefix={currentInput.prefix}
																				suffix={currentInput.suffix}
																				value={props.values[fieldName]}
																				handleChange={props.handleChange}
																				error={props.errors[fieldName]}
																			/>
																		</table>
																	);
																}
															})}
														</Table.Cell>
														<Table.Cell>
															<EnglishField fluid placeholder="Recommendations" name={`1_A_${row.sn}_recommendation`} />
														</Table.Cell>
													</Table.Row>
												);
											})}
											<Table.Row>
												<Table.HeaderCell colSpan={3}>{table2.sectionB.heading}</Table.HeaderCell>
											</Table.Row>
											{/* Section B */}
											{Object.keys(table2.sectionB.rows).map(rowKey => {
												const row = table2.sectionB.rows[rowKey];
												// console.log('row sectionB', row);
												return (
													<Table.Row>
														<Table.Cell>{row.sn}</Table.Cell>
														<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
														<Table.Cell>
															{row.sn === '26' ? (
																<table>
																	<Table.Row>
																		<Table.Cell>
																			<Input labelPosition="right">
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'Sill band and Lintel band'}
																				</Label>
																				<table>
																					<LabeledTableInput
																						style={{ width: '80px' }}
																						name={'1_C_26_sill-band-and-lintel-band'}
																						value={props.values['1_C_26_sill-band-and-lintel-band']}
																						handleChange={props.handleChange}
																						error={props.errors['1_C_26_sill-band-and-lintel-band']}
																					/>
																				</table>
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'no. of'}
																				</Label>
																			</Input>

																			<Input labelPosition="right">
																				<table>
																					<LabeledTableInput
																						style={{ width: '80px' }}
																						name={'1_C_26_no-of'}
																						value={props.values['1_C_26_no-of']}
																						handleChange={props.handleChange}
																						error={props.errors['1_C_26_no-of']}
																					/>
																				</table>
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'mm'}
																				</Label>
																			</Input>
																			<Input labelPosition="right">
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'bars with'}
																				</Label>
																				<table>
																					<LabeledTableInput
																						style={{ width: '80px' }}
																						name={'1_C_26_bars-with'}
																						value={props.values['1_C_26_bars-with']}
																						handleChange={props.handleChange}
																						error={props.errors['1_C_26_bars-with']}
																					/>
																				</table>
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'mm C-hook@'}
																				</Label>
																			</Input>

																			<Input labelPosition="right">
																				<table>
																					<LabeledTableInput
																						style={{ width: '80px' }}
																						name={'1_C_26_c-hook'}
																						value={props.values['1_C_26_c-hook']}
																						handleChange={props.handleChange}
																						error={props.errors['1_C_26_c-hook']}
																					/>
																				</table>
																				<Label
																					basic
																					style={{
																						width: '80px',
																					}}
																				>
																					{'mm c-c'}
																				</Label>
																			</Input>
																		</Table.Cell>
																	</Table.Row>{' '}
																</table>
															) : (
																Object.keys(row.inputs).map(input => {
																	const currentInput = row.inputs[input];
																	const fieldName = currentInput.prefix
																		? `1_B_${row.sn}_${getNameFormat(currentInput.prefix)}_qty`
																		: `1_B_${row.sn}_qty`;
																	return (
																		<table>
																			<LabeledTableInput
																				name={fieldName}
																				prefix={currentInput.prefix}
																				suffix={currentInput.suffix}
																				handleChange={props.handleChange}
																				value={props.values[fieldName]}
																				error={props.errors[fieldName]}
																			/>
																		</table>
																	);
																	// return (
																	//   <Table.Row>
																	//     <Table.Cell>
																	//       <Input labelPosition="right">
																	//         {currentInput.prefix && (
																	//           <Label basic>
																	//             {currentInput.prefix}
																	//           </Label>
																	//         )}
																	//         <input />
																	//         {currentInput.suffix && (
																	//           <Label basic>
																	//             {currentInput.suffix}
																	//           </Label>
																	//         )}
																	//       </Input>
																	//     </Table.Cell>
																	//   </Table.Row>
																	// );
																})
															)}
														</Table.Cell>
														<Table.Cell>
															<EnglishField fluid placeholder="Recommendations" name={`1_B_${row.sn}_recommendation`} />
														</Table.Cell>
													</Table.Row>
												);
											})}
											<Table.Row>
												<Table.HeaderCell colSpan={3}>{table2.sectionC.heading}</Table.HeaderCell>
											</Table.Row>
											{Object.keys(table2.sectionC.rows).map(rowKey => {
												const row = table2.sectionC.rows[rowKey];
												return (
													<Table.Row>
														<Table.Cell>{row.sn}</Table.Cell>
														<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
														<Table.Cell>
															{row.inputs.length > 1 ? (
																Object.keys(row.inputs).map(input => {
																	const currentInput = row.inputs[input];
																	return (
																		<table>
																			<LabeledTableInput
																				name={`1_C_${row.sn}_qty`}
																				prefix={currentInput.prefix}
																				suffix={currentInput.suffix}
																				handleChange={props.handleChange}
																				value={props.values[`1_C_${row.sn}_qty`]}
																			/>
																		</table>
																	);
																})
															) : (
																<table>
																	<LabeledTableInput
																		name={`1_C_${row.sn}_qty`}
																		handleChange={props.handleChange}
																		value={props.values[`1_C_${row.sn}_qty`]}
																	/>
																</table>
															)}
														</Table.Cell>
														<Table.Cell>
															<EnglishField fluid placeholder="Recommendations" name={`1_C_${row.sn}_recommendation`} />
														</Table.Cell>
													</Table.Row>
												);
											})}
										</Table.Body>
									</Table>

									<h5 className="formFeild-mainLabel english-div">{table3.heading}</h5>
									<Table celled structured compact className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell>S.N.</Table.HeaderCell>
												<Table.HeaderCell width={6}>Provision</Table.HeaderCell>
												<Table.HeaderCell colSpan="3">Area</Table.HeaderCell>
												<Table.HeaderCell>Recommendations</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{/* Section 1 */}
											<Table.Row key={'section-1'}>
												<Table.Cell>{table3.sections.section1.sn}</Table.Cell>
												<Table.Cell>{table3.sections.section1.rows.row1.name}</Table.Cell>
												<Table.Cell colSpan="3">
													<EnglishField name={`2_${table3.sections.section1.sn}_row1_qty`} />
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section1.sn}_row1_recommendation`} />
												</Table.Cell>
											</Table.Row>
											{/* Section 2 */}
											<Table.Row key={'section-2'}>
												<Table.Cell>{table3.sections.section2.sn}</Table.Cell>
												<Table.Cell>{table3.sections.section2.rows.row1.name}</Table.Cell>
												<Table.Cell colSpan="3">
													<EnglishField name={`2_${table3.sections.section2.sn}_row1_qty`} />
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section2.sn}_row1_recommendation`} />
												</Table.Cell>
											</Table.Row>
											{/* Section 3 */}
											<Table.Row key="section-3">
												<Table.Cell>{table3.sections.section3.sn}</Table.Cell>
												<Table.Cell>{table3.sections.section3.rows.row1.name}</Table.Cell>
												<Table.Cell colSpan="3">
													<EnglishField name={`2_${table3.sections.section3.sn}_row1_qty`} />
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section3.sn}_row1_recommendation`} />
												</Table.Cell>
											</Table.Row>

											{/* Section 4 */}

											<Table.Row key="section-4">
												<Table.Cell rowSpan={floorCount.length + 1}>{table3.sections.section4.sn}</Table.Cell>

												<Table.Cell>
													<u>{table3.sections.section4.rows.row1.name}</u>
												</Table.Cell>

												<Table.Cell textAlign="center">
													<DisabledInput value={table3.sections.section4.rows.row1.inputs[0]} />
												</Table.Cell>

												<Table.Cell textAlign="center">
													<DisabledInput value={table3.sections.section4.rows.row1.inputs[1]} />
												</Table.Cell>

												<Table.Cell textAlign="center">
													<DisabledInput value={table3.sections.section4.rows.row1.inputs[2]} />
												</Table.Cell>

												<Table.Cell />
											</Table.Row>
											{floorArray.getFloorsWithLabels().map(currentRow => {
												return (
													<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
														<Table.Cell>
															{`${getFloorEnglish(currentRow.floor)} ${hasBlocks && 'Block ' + currentRow.block} `}
														</Table.Cell>

														<Table.Cell>
															<EnglishField name={`2_4_floor${currentRow.floor}_${currentRow.block}_wallheight`} />
														</Table.Cell>
														<Table.Cell>
															<EnglishField name={`2_4_floor${currentRow.floor}_${currentRow.block}_wallthickness`} />
														</Table.Cell>
														<Table.Cell>
															<EnglishField name={`2_4_floor${currentRow.floor}_${currentRow.block}_maximumlength`} />
														</Table.Cell>

														<Table.Cell>
															<EnglishField name={`2_4_floor${currentRow.floor}_${currentRow.block}_recommendation`} />
														</Table.Cell>
													</Table.Row>
												);
											})}

											{/* Section 5 */}

											<Table.Row key="section-5">
												<Table.Cell rowSpan="6">{table3.sections.section5.sn}</Table.Cell>
												<Table.Cell>
													<u>{table3.sections.section5.rows.row1.name}</u>
												</Table.Cell>
												<Table.Cell colSpan="3" />
												<Table.Cell />
											</Table.Row>

											<Table.Row>
												<Table.Cell>{table3.sections.section5.rows.row2.name}</Table.Cell>
												<Table.Cell colSpan="3">
													<Input
														name={`2_${table3.sections.section5.sn}_row2_qty`}
														onChange={props.handleChange}
														value={props.values[`2_${table3.sections.section5.sn}_row2_qty`]}
													/>
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section5.sn}_row2_recommendation`} />
												</Table.Cell>
											</Table.Row>

											<Table.Row>
												<Table.Cell>{table3.sections.section5.rows.row3.name}</Table.Cell>
												<Table.Cell colSpan="3">
													{table3.sections.section5.rows.row3.inputs.map(input => (
														<RadioInput
															space={true}
															name={`2_${table3.sections.section5.sn}_row3_qty`}
															key={input}
															option={input}
														/>
													))}
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section5.sn}_row3_recommendation`} />
												</Table.Cell>
											</Table.Row>

											<Table.Row>
												<Table.Cell>{table3.sections.section5.rows.row4.name}</Table.Cell>
												<Table.Cell colSpan="3">
													{table3.sections.section5.rows.row4.inputs.map(input => (
														<RadioInput
															space={true}
															name={`2_${table3.sections.section5.sn}_row4_qty`}
															key={input}
															option={input}
														/>
													))}
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section5.sn}_row4_recommendation`} />
												</Table.Cell>
											</Table.Row>

											<Table.Row>
												<Table.Cell>{table3.sections.section5.rows.row5.name}</Table.Cell>
												<Table.Cell colSpan="3">
													{table3.sections.section5.rows.row4.inputs.map(input => (
														<RadioInput
															space={true}
															name={`2_${table3.sections.section5.sn}_row5_qty`}
															key={input}
															option={input}
														/>
													))}
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section5.sn}_row5_recommendation`} />
												</Table.Cell>
											</Table.Row>

											<Table.Row>
												<Table.Cell>{table3.sections.section5.rows.row6.name}</Table.Cell>
												<Table.Cell colSpan="3">
													{table3.sections.section5.rows.row4.inputs.map(input => (
														<RadioInput
															space={true}
															name={`2_${table3.sections.section5.sn}_row6_qty`}
															key={input}
															option={input}
														/>
													))}
												</Table.Cell>
												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section5.sn}_row6_recommendation`} />
												</Table.Cell>
											</Table.Row>

											{/* Section 6 */}
											<Table.Row>
												<Table.Cell>{table3.sections.section6.sn}</Table.Cell>
												<Table.Cell>{table3.sections.section6.rows.row1.name}</Table.Cell>
												<Table.Cell colspan="3">
													{table3.sections.section6.rows.row1.inputs.map(input => (
														<FormikCheckbox
															name={`2_${table3.sections.section6.sn}_row1_qty`}
															space={true}
															value={input}
														/>
														// <RadioInput
														// 	space={true}
														// 	name={`2_${table3.sections.section6.sn}_row1_qty`}
														// 	key={input}
														// 	option={input}
														// />
													))}
												</Table.Cell>

												<Table.Cell>
													<EnglishField name={`2_${table3.sections.section6.sn}_row1_recommendation`} />
												</Table.Cell>
											</Table.Row>

											{/* Section 7 */}

											<Table.Row>
												<Table.Cell rowSpan={floorCount.length + 1}>{table3.sections.section7.sn}</Table.Cell>
												<Table.Cell>
													<u>{table3.sections.section7.rows.row1.name}</u>
												</Table.Cell>
												<Table.Cell colspan="3" />
												<Table.Cell />
											</Table.Row>
											{floorArray.getFloorsWithLabels().map(currentRow => {
												return (
													<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
														<Table.Cell>{`${getFloorEnglish(currentRow.floor)} ${hasBlocks &&
															'Block ' + currentRow.block}`}</Table.Cell>
														<Table.Cell colspan="3">
															<EnglishField name={`2_7_floor${currentRow.floor}_${currentRow.block}_qty`} />
														</Table.Cell>
														<Table.Cell>
															<EnglishField name={`2_7_floor${currentRow.floor}_${currentRow.block}_recommendation`} />
														</Table.Cell>
													</Table.Row>
												);
											})}
										</Table.Body>
									</Table>

									<h4 align="center" className="formFeild-mainLabel english-div">
										{table4.heading}
									</h4>

									<h5 className="formFeild-mainLabel english-div">{table4.subheading}</h5>
									<Table celled collapsing compact className="english-div">
										<Table.Body>
											{table4.form.map(row => (
												<Table.Row>
													<Table.Cell>
														<Header as="h5">{row.display}</Header>
													</Table.Cell>
													<Table.Cell>
														{/* <Input className="structureC"
                              name={table4.form[row]}
                              onChange={props.handleChange}
                              value={props.values[table4.form[row]]}
                            /> */}
														<Input
															className="structureC"
															name={row.name}
															onChange={props.handleChange}
															value={props.values[row.name]}
														/>
													</Table.Cell>
												</Table.Row>
											))}
										</Table.Body>
									</Table>
								</div>
								<br />

								<SaveButtonValidation
									errors={props.errors}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={props.handleSubmit}
									validateForm={props.validateForm}
								/>
							</Form>
						);
					}}
				/>
			</>
		);
	}
}

export const FormHeading = () => (
	<div className="centered-underlined-heading">
		<h5>{data.heading1}</h5>
		<h5>
			{data.heading2} <br />
			{data.heading3}
		</h5>

		<div className="english-div">
			<h3 className="underline english-div">{data.subheading1}</h3>
			<h5>{data.subheading2}</h5>
			<h5>{data.subheading3}</h5>
		</div>

		<div className="english-div">
			<span>{data.note1} </span>
			<h3>{data.note2} </h3>
		</div>
	</div>
);

const LabeledTableInput = props => {
	// console.log('props in label', props);
	return (
		<Table.Row key={props.key}>
			<Table.Cell>
				<Form.Field>
					<Form.Input error={props.error}>
						<Input labelPosition={props.suffix ? 'right' : undefined}>
							{props.prefix && (
								<Label
									basic
									style={{
										'white-space': 'pre-wrap',
										'word-wrap': 'break-word',
										width: '80px',
									}}
								>
									{props.prefix}
								</Label>
							)}
							<input
								style={{ width: '80px' }}
								className="english-div-field"
								name={props.name}
								onChange={props.handleChange}
								value={props.value}
							/>
							{props.suffix && (
								<Label
									basic
									style={{
										'white-space': 'pre-wrap',
										'word-wrap': 'break-word',
										width: '80px',
									}}
								>
									{props.suffix}
								</Label>
							)}
						</Input>
					</Form.Input>
				</Form.Field>
			</Table.Cell>
		</Table.Row>
	);
};

const DisabledInput = props => <Input style={{ opacity: '1' }} defaultValue={props.value} disabled />;

// const RadioLabel = props => (
//   <label style={{ marginRight: '15px' }}>{props.value}</label>
// );

const StructureDesignClassCForm = parentProps => (
	<FormContainerV2
		api={[{ api: api.structureDesignC, objName: 'structC', form: true }]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui radio checkbox prabidhik', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <StructureDesignClassCFormComponent {...props} parentProps={parentProps} />}
	/>
);

export default StructureDesignClassCForm;
