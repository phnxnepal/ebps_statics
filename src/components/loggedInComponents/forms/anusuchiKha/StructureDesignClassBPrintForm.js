import React, { Component } from 'react';
import { Table, Form, Message } from 'semantic-ui-react';
import { structureDesignClassB } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';

// Redux
import { connect } from 'react-redux';
import FallbackComponent from '../../../shared/FallbackComponent';
import { Field, Formik, getIn } from 'formik';
import FormikCheckbox from '../../../shared/FormikCheckbox';
import api from '../../../../utils/api';
import { setPrint } from '../../../../store/actions/general';
import { getJsonData, getFloorEnglish, FloorArray } from '../../../../utils/dataUtils';
import StructureDesignBSchema from '../../formValidationSchemas/structureDesignClassBValidation';
import { getPermitAndUserData } from './../../../../store/actions/AllowancePaperAction';
import { getFormDataWithPermitAndUser } from './../../../../store/actions/formActions';
import { FormHeading } from './StructureDesignClassBForm';

const sdcb = structureDesignClassB.structureDesignClassB_data;
const table1 = sdcb.table.table1;
const table2 = sdcb.table.table2;
const table3 = sdcb.table.table3;
const table4 = sdcb.table.table4;
const table5 = sdcb.table.table5;

class StructureDesignClassBPrintFormComponent extends Component {
	componentDidMount() {
		this.props.getFormDataWithPermitAndUser(api.structureDesignB, data => data);
	}

	handleLoadCombinations = e => {
		const id = e.target.id;

		this.setState({
			designPhilosophy: parseInt(id),
		});
	};

	handleProvisonExt = e => {
		const id = e.target.id;
		this.setState({ provFutExt: parseInt(id) });
	};

	handleStrDesFutExt = e => {
		const id = e.target.id;
		this.setState({ strDesFutExt: parseInt(id) });
	};

	handleAlumuStruct = e => {
		const id = e.target.id;

		this.setState({
			aluminiumStructure: parseInt(id),
		});
	};

	render() {
		if (!isEmpty(this.props.permitData) && !isEmpty(this.props.userData)) {
			const permitData = this.props.permitData;
			// const userData = this.props.userData;
			const prevData = this.props.prevData;

			const floorArray = new FloorArray(permitData.floor);
			const json_data = getJsonData(prevData);

			// let initVal = {};

			const initVal1 = { ...json_data };
			// console.log(initVal1);

			return (
				<React.Fragment>
					<div className="view-Form sdcbFormContentWrap">
						<Formik
							key={'print-key'}
							initialValues={initVal1}
							enableReinitialize
							validationSchema={StructureDesignBSchema}
							onSubmit={async (values, actions) => {}}
							render={props => {
								// console.log(props.values);

								return (
									<Form loading={props.isSubmitting}>
										{!isEmpty(this.props.errors) && (
											<Message negative>
												<Message.Header>Error</Message.Header>
												<p>{this.props.errors.message}</p>
											</Message>
										)}
										<div>
											<FormHeading />
											<Table celled structured className="sdcbFirstTable">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell width={1}>{sdcb.table.heading.heading_1}</Table.HeaderCell>
														<Table.HeaderCell width={8}>{sdcb.table.heading.heading_2}</Table.HeaderCell>
														<Table.HeaderCell width={4}>{sdcb.table.heading.heading_3}</Table.HeaderCell>
														<Table.HeaderCell width={3}>{sdcb.table.heading.heading_4}</Table.HeaderCell>
													</Table.Row>
												</Table.Header>
												<Table.Body>
													<Table.Row>
														<Table.Cell colSpan={4}>
															<strong>{table1.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section1.description}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_1_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section2.description}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section3.description}</Table.Cell>
														<Table.Cell>
															{props.values['1_3_qty'] && props.values['1_3_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section4.description}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section5.description_1}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_5_qty')}</span>
															{/* {table5.section5.radiobox.map((name, index) => (
                                                            <div key={index} className="ui radio checkbox">
                                                                <Field
                                                                    type="radio"
                                                                    name="1_5_qty"
                                                                    id={index}
                                                                    value={name}
                                                                    onClick={this.handleProvisonExt}
                                                                />
                                                                <label>{name}</label>
                                                            </div>
                                                        ))} */}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													{props.values['1_5_qty'] === 0 && (
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table1.section.section5.description_2}</Table.Cell>
															<Table.Cell>
																<span>{getIn(props.values, '1_6_qty')} Floors</span>
															</Table.Cell>
															<Table.Cell>
																<span>{getIn(props.values, '1_6_remark')}</span>
															</Table.Cell>
														</Table.Row>
													)}
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section5.description_3}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '1_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell colSpan="4">
															<strong>{table1.heading_2}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table1.sub_section.cell_1.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_1.description}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_1_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table1.sub_section.cell_2.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row className="materialSpecsRow">
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_2.description}</Table.Cell>
														<Table.Cell>
															{props.values['2_2_1_qty'] && props.values['2_2_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<p>{getIn(props.values, '2_2_1_cem_remark')}</p>
															<p>{getIn(props.values, '2_2_1_faggSand_remark')}</p>
															<p>{getIn(props.values, '2_2_1_natBuiSt_remark')}</p>
															<p>{getIn(props.values, '2_2_1_til_remark')}</p>
															<p>{getIn(props.values, '2_2_1_metFram_remark')}</p>
															<p>{getIn(props.values, '2_2_1_coaAgg_remark')}</p>
															<p>{getIn(props.values, '2_2_1_buiLim_remark')}</p>
															<p>{getIn(props.values, '2_2_1_bri_remark')}</p>
															<p>{getIn(props.values, '2_2_1_tim_remark')}</p>
															<p>{getIn(props.values, '2_2_1_strStel_remark')}</p>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_2.description_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_2_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_2_2_remark')}</span>
														</Table.Cell>
													</Table.Row>

													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table1.sub_section.cell_3.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_3.description}</Table.Cell>
														<Table.Cell>
															{props.values['2_3_1_qty'] && props.values['2_3_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_3_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table1.sub_section.cell_3.description_1}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_3.description_2}</Table.Cell>
														<Table.Cell>
															{getIn(props.values, '2_3_2_1_qty') ? (
																<span>{getIn(props.values, '2_3_2_1_qty')} weight</span>
															) : (
																''
															)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_3_2_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_3.description_3}</Table.Cell>
														<Table.Cell>
															{getIn(props.values, '2_3_2_2_qty') ? (
																<span>{getIn(props.values, '2_3_2_2_qty')} weight</span>
															) : (
																''
															)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_3_2_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_3.description_4}</Table.Cell>
														<Table.Cell>
															{getIn(props.values, '2_3_2_3_qty') ? (
																<span>{getIn(props.values, '2_3_2_3_qty')} weight</span>
															) : (
																''
															)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_3_2_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.sub_section.cell_3.description_5}</Table.Cell>
														<Table.Cell>
															{getIn(props.values, '2_3_2_4_qty') ? (
																<span>{getIn(props.values, '2_3_2_4_qty')} weight</span>
															) : (
																''
															)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_3_2_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">{table1.sub_section.note}</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table2.section1.heading}</strong>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbSecondTable">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell width={1} rowSpan="2"></Table.HeaderCell>
														<Table.HeaderCell width={6}>
															<strong>{table2.section1.tableHead1}</strong>
														</Table.HeaderCell>
														<Table.HeaderCell width={5} colSpan="2">
															<strong>{table2.section1.tableHead2}</strong>
														</Table.HeaderCell>
														<Table.HeaderCell width={3}>
															<strong>Remarks</strong>
														</Table.HeaderCell>
													</Table.Row>
													<Table.Row>
														<Table.HeaderCell></Table.HeaderCell>
														<Table.HeaderCell>
															<strong>{table2.section1.tableHead3}</strong>
														</Table.HeaderCell>
														<Table.HeaderCell>
															<strong>{table2.section1.tableHead4}</strong>
														</Table.HeaderCell>
														<Table.HeaderCell></Table.HeaderCell>
													</Table.Row>
												</Table.Header>
												<Table.Body>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="4">
															<span>{getIn(props.values, 'propOccpuType')}</span>
														</Table.Cell>
													</Table.Row>
													{props.values.propOccpuType === 'For Residental Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_1.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_1_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_1.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_2_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_1.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_1_3_remark')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'For Hotels, Hostels, Dormitories' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_2.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_1_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_2.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_2_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_2.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_3_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_2.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_4_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_2.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_2_5_remark')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}
													{props.values.propOccpuType === 'For Educational Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_3.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_1_remark')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_3.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_3.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_3.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_3.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_3_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'For Institutional Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_4.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_4.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_4.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_4.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_4.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_4_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'For Assembly Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_5.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_5.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_5.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_5.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_5.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_5_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'For Business and Office Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_6.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_6.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_6.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_6.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_6.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_6_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'Mercantile Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_7.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_7.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_7.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_7.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_7.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_7_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'Industrial Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_5}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_5_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_5_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_5_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_8.td_6}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_6_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_6_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_8_6_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}

													{props.values.propOccpuType === 'Storage Buildings' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_9.td_1}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_1_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_1_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_1_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_9.td_2}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_2_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_2_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_2_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_9.td_3}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_3_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_3_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_3_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell>{table2.section1.tbody.tRow_9.td_4}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_4_UD')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_4_CL')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_4_9_4_rmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbThirdTable">
												<Table.Body>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table3.section1.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell width={1}></Table.Cell>
														<Table.Cell width={6}>{table3.section1.description_1}</Table.Cell>
														<Table.Cell width={5}>
															<span>{getIn(props.values, '2_5_1_WZ')}</span>
														</Table.Cell>
														<Table.Cell width={3}>
															<span>{getIn(props.values, '2_5_1_WNrMRk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section1.description_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_5_2_WZ')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_5_2_WNrMRk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table3.section2.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_1}</Table.Cell>
														<Table.Cell>
															{props.values['2_6_1_qty'] && props.values['2_6_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_3}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_4}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_5}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_6}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_6_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_6_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section2.description_7}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_6_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table3.section3.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section3.description_1}</Table.Cell>
														<Table.Cell>
															{props.values['2_7_1_qty'] && props.values['2_7_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section3.description_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section3.description_3}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section3.description_4}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_7_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table3.section4.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section4.description_1}</Table.Cell>
														<Table.Cell>
															{props.values['2_8_1_qty'] && props.values['2_8_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="2_8_1_remark" />
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">
															<strong>{table3.section5.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_1}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_1_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_3}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_4}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_5}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_6}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_6_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_6_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table3.section5.description_7}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="3">{table3.note}</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbFourthTable">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell width={7}>{table4.section.thead1}</Table.HeaderCell>
														<Table.HeaderCell width={5}>{table4.section.thead2}</Table.HeaderCell>
														<Table.HeaderCell width={3}>{table4.section.thead3}</Table.HeaderCell>
													</Table.Row>
												</Table.Header>
												<Table.Body>
													<Table.Row>
														<Table.Cell>{table4.section.tbody.td_1}</Table.Cell>
														<Table.Cell>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_east_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_1}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_east_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_2}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_2}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox checkboxWithInputField">
																	<FormikCheckbox
																		name="2_9_8_east_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_3.line_1}
																	/>
																	<span>{getIn(props.values, '2_9_8_east_qty_building_structure')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_2}</strong>
																	<span>{getIn(props.values, '2_9_8_east_qty_building_floor')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_3}</strong>
																</div>
															</div>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_9_8_east_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell>{table4.section.tbody.td_2}</Table.Cell>
														<Table.Cell>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_west1_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_1}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_west1_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_2}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_2}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox checkboxWithInputField">
																	<FormikCheckbox
																		name="2_9_8_west1_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_3.line_1}
																	/>
																	<span>{getIn(props.values, '2_9_8_west1_qty_building_structure')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_2}</strong>
																	<span>{getIn(props.values, '2_9_8_west_qty_building_floor')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_3}</strong>
																</div>
															</div>
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="2_9_8_west_remark" />
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell>{table4.section.tbody.td_3}</Table.Cell>
														<Table.Cell>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_north_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_1}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_north_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_2}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_2}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox checkboxWithInputField">
																	<FormikCheckbox
																		name="2_9_8_north_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_3.line_1}
																	/>
																	<span>{getIn(props.values, '2_9_8_north_qty_building_structure')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_2}</strong>
																	<span>{getIn(props.values, '2_9_8_north_qty_building_floor')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_3}</strong>
																</div>
															</div>
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="2_9_8_north_remark" />
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell>{table4.section.tbody.td_4}</Table.Cell>
														<Table.Cell>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_south_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_1}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox">
																	<FormikCheckbox
																		name="2_9_8_south_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_2}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_2}
																	/>
																</div>
															</div>
															<div className="field">
																<div className="ui checkbox checkboxWithInputField">
																	<FormikCheckbox
																		name="2_9_8_south_qty"
																		labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																		onChange={props.handleChange}
																		value={table4.section.tbody.checkbox.checkbox_3.line_1}
																	/>
																	<span>{getIn(props.values, '2_9_8_south_qty_building_structure')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_2}</strong>
																	<span>{getIn(props.values, '2_9_8_south_qty_building_floor')}</span>
																	<strong>{table4.section.tbody.checkbox.checkbox_3.line_3}</strong>
																</div>
															</div>
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="2_9_8_south_remark" />
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbFifthTable">
												<Table.Body>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section1.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell width={1}></Table.Cell>
														<Table.Cell width={6}>{table5.section1.description_1}</Table.Cell>
														<Table.Cell width={5} colSpan="3">
															<span>{getIn(props.values, '2_10_1_qty')}</span>
														</Table.Cell>
														<Table.Cell width={3}>
															<span>{getIn(props.values, '2_10_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_3}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>
															<strong>{table5.section1.sub_section.thead_1}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section1.sub_section.thead_2}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section1.sub_section.thead_3}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section1.sub_section.thead_4}</strong>
														</Table.Cell>
														<Table.Cell></Table.Cell>
													</Table.Row>
													{floorArray.getHeightRelevantFloors().map((currentRow, idx) => {
														return (
															<Table.Row key={idx}>
																<Table.Cell></Table.Cell>
																<Table.Cell>{getFloorEnglish(currentRow.floor)}</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, `2_10_4_floor${currentRow.floor}_WallHeight`)}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, `2_10_4_floor${currentRow.floor}__WallThick`)}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, `2_10_4_floor${currentRow.floor}_MaxLeng`)}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, `2_10_4_floor${currentRow.floor}_remark`)}</span>
																</Table.Cell>
															</Table.Row>
														);
													})}
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_4}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_5}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_6_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_6_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_6}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_7}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_8_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_8_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_8}</Table.Cell>
														<Table.Cell colSpan="3">
															{props.values['2_10_9_qty'] && props.values['2_10_9_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_9_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section1.sub_section2.heading}</strong>
														</Table.Cell>
													</Table.Row>
													{floorArray.getHeightRelevantFloors().map(currentRow => {
														return (
															<>
																<Table.Row>
																	<Table.Cell></Table.Cell>
																	<Table.Cell>{getFloorEnglish(currentRow.floor)}</Table.Cell>
																	<Table.Cell colSpan="3">
																		<span>{getIn(props.values, `2_10_floor${currentRow.floor}_qty`)}</span>
																	</Table.Cell>
																	<Table.Cell>
																		<span>
																			{getIn(props.values, `2_10_floor${currentRow.floor}_recommendation`)}
																		</span>
																	</Table.Cell>
																</Table.Row>
															</>
														);
													})}
													{/* <Table.Row>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell textAlign="right">
                                                        {table5.section1.sub_section2.td_cell_1}
                                                    </Table.Cell>
                                                    <Table.Cell colSpan="3">
                                                        <span>{getIn(props.values, '2_10_10_g_f_qty')}</span>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <span>{getIn(props.values, '2_10_10_g_f_remark')}</span>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell textAlign="right">
                                                        {table5.section1.sub_section2.td_cell_2}
                                                    </Table.Cell>
                                                    <Table.Cell colSpan="3">
                                                        <span>{getIn(props.values, '2_10_10_f_f_qty')}</span>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <span>{getIn(props.values, '2_10_10_f_f_remark')}</span>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell textAlign="right">
                                                        {table5.section1.sub_section2.td_cell_3}
                                                    </Table.Cell>
                                                    <Table.Cell colSpan="3">
                                                        <span>{getIn(props.values, '2_10_10_s_f_qty')}</span>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <span>{getIn(props.values, '2_10_10_s_f_remark')}</span>
                                                    </Table.Cell>
                                                </Table.Row> */}
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section1.description_9}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_10_10_c_c_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_10_10_c_c_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section2.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_1}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_1_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_3}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_4}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_5}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_6}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_6_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_6_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_7}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_8}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_8_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_8_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_9}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_9_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_9_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_10}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_10_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_10_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.description_11}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_11_11_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_11_remark')}</span>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbSixthTable">
												<Table.Body>
													<Table.Row>
														<Table.Cell width={1}></Table.Cell>
														<Table.Cell width={6}>
															<strong>{table5.section2.sub_section.thead_1}</strong>
														</Table.Cell>
														<Table.Cell width={4} colSpan="4">
															<strong>{table5.section2.sub_section.thead_2}</strong>
														</Table.Cell>
														<Table.Cell width={3}></Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell></Table.Cell>
														<Table.Cell>
															<strong>{table5.section2.sub_section.thead_3}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section2.sub_section.thead_4}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section2.sub_section.thead_5}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section2.sub_section.thead_6}</strong>
														</Table.Cell>
														<Table.Cell></Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_1}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_mSDrCanLev')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_mSDrSimSup')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_mSDrOnSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_mSDrBoSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_mSDrRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_2}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_spCorBeCanLev')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_spCorBeSimSup')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_spCorBeOnSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_spCorBeBoSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_spCorBeRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_3}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_depCorTeCanLev')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_depCorTeSimSup')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_depCorTeOnSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_depCorTeBoSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_depCorTeRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_4}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_widCorTeCanLev')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_widCorTeSimSup')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_widCorTeOnSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_widCorTeBoSiCon')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_12_widCorTeRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.descriptio_12}</Table.Cell>
														<Table.Cell colSpan="4">
															<span>{getIn(props.values, '2_11_13_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_13_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.descriptio_13}</Table.Cell>
														<Table.Cell colSpan="4">
															<span>{getIn(props.values, '2_11_14_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_14_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section2.sub_section2.heading}</strong>
														</Table.Cell>
													</Table.Row>
													{props.values['2_11_14_qty'] === 'Limit state method' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">{table5.section2.radiobox[0]} 1</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_1_limStaMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_1_limStaMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">2</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_2_limStaMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_2_limStaMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">3</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_3_limStaMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_3_limStaMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">4</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_4_limStaMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_4_limStaMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}
													{props.values['2_11_14_qty'] === 'Working stress method' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">{table5.section2.radiobox[1]} 1</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_1_workStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_1_workStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">2</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_2_workStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_2_workStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">3</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_3_workStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_3_workStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">4</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_4_workStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_4_workStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}
													{props.values['2_11_14_qty'] === 'Unlimited strength method' && (
														<>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">{table5.section2.radiobox[2]} 1</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_1_unLimStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_1_unLimStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">2</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_2_unLimStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_2_unLimStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">3</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_3_unLimStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_3_unLimStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
															<Table.Row>
																<Table.Cell></Table.Cell>
																<Table.Cell textAlign="right">4</Table.Cell>
																<Table.Cell colSpan="4">
																	<span>{getIn(props.values, '2_11_14_4_unLimStrMeth')}</span>
																</Table.Cell>
																<Table.Cell>
																	<span>{getIn(props.values, '2_11_14_4_unLimStrMethRmrk')}</span>
																</Table.Cell>
															</Table.Row>
														</>
													)}
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.descriptio_14}</Table.Cell>
														<Table.Cell colSpan="4">
															<span>{getIn(props.values, '2_11_15_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_15_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section2.descriptio_15}</Table.Cell>
														<Table.Cell colSpan="4">
															<span>{getIn(props.values, '2_11_16_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_11_16_remark')}</span>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
											<Table celled structured className="sdcbSeventhTable">
												<Table.Body>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section3.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell width={1}></Table.Cell>
														<Table.Cell width={6}>{table5.section3.description_1}</Table.Cell>
														<Table.Cell width={5} colSpan="3">
															{props.values['2_12_1_qty'] && props.values['2_12_1_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell width={3}>
															<span>{getIn(props.values, '2_12_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_3}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_3_remark')}</span>
														</Table.Cell>
													</Table.Row>

													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>
															<strong>{table5.section3.sub_section.thead_1}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section3.sub_section.thead_2}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section3.sub_section.thead_3}</strong>
														</Table.Cell>
														<Table.Cell>
															<strong>{table5.section3.sub_section.thead_4}</strong>
														</Table.Cell>
														<Table.Cell></Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_4}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_expSecPip')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_expSecWbStnd')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_expSecCompSec')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_expSecRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_5}</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_ntexpSecPip')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_ntexpSecWbStnd')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_ntexpSecCompSec')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_4_ntexpSecRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_6}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_7}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_6_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_6_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_8}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_7_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_7_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section3.description_9}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_12_8_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_12_8_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section4.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section4.description_1}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_13_1_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_13_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section4.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_13_2_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_13_2_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section4.description_3}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_13_3_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_13_3_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section4.description_4}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_13_4_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_13_4_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section4.description_5}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_13_5_qty')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_13_5_remark')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section5.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section5.description_1}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, 'alumStruMem')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_14_1_remark')}</span>
														</Table.Cell>
													</Table.Row>
													{props.values['alumStruMem'] === 'Yes' && (
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table5.section5.description_2}</Table.Cell>
															<Table.Cell colSpan="3">
																<span>{getIn(props.values, '2_14_2_menDesName')}</span>
															</Table.Cell>
															<Table.Cell>
																<span>{getIn(props.values, '2_14_2_menDesNameRmrk')}</span>
															</Table.Cell>
														</Table.Row>
													)}
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell colSpan="6">
															<strong>{table5.section6.heading}</strong>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section6.description_1}</Table.Cell>
														<Table.Cell colSpan="3">
															<span>{getIn(props.values, '2_15_1_safetyMeasUse')}</span>
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_15_1_safetyMeasUseRmrk')}</span>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section6.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															{props.values['2_15_2_qty'] && props.values['2_15_2_qty'].map(row => <p>{row}</p>)}
														</Table.Cell>
														<Table.Cell>
															<span>{getIn(props.values, '2_15_2_safeWareUseRmrk')}</span>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
										</div>
										<p>{table5.note}</p>
									</Form>
								);
							}}
						/>
					</div>
				</React.Fragment>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getFormDataWithPermitAndUser,
	setPrint: res => setPrint(res),
};
const mapStateToProps = state => {
	return {
		formData: state.root.formData,
		permitData: state.root.formData.permitData,
		userData: state.root.formData.userData,
		prevData: state.root.formData.prevData,
		errors: state.root.ui.errors,
		loading: state.root.ui.loading,
		printcontent: state.root.general.printcontent,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StructureDesignClassBPrintFormComponent);
