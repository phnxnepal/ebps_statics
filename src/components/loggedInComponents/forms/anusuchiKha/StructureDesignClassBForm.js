import React, { Component } from 'react';
import { Table, Form, Input, Select, Label } from 'semantic-ui-react';
import { structureDesignClassB } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';

// Redux
import { Formik, getIn } from 'formik';
import { EnglishField as Field } from '../../../shared/fields/EnglishField';
import FormikCheckbox from '../../../shared/FormikCheckbox';
import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, getFloorEnglish, handleSuccess } from '../../../../utils/dataUtils';
import StructureDesignBSchema from '../../formValidationSchemas/structureDesignClassBValidation';
import { checkError } from './../../../../utils/dataUtils';
import { getClassWiseNextUrl } from '../../../../utils/urlUtils';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { TableInputIm } from '../../../shared/TableInput';
import { SectionHeader } from '../../../uiComponents/Headers';

const sdcb = structureDesignClassB.structureDesignClassB_data;
const table1 = sdcb.table.table1;
const table2 = sdcb.table.table2;
const table3 = sdcb.table.table3;
const table4 = sdcb.table.table4;
const table5 = sdcb.table.table5;

const propOccpuName = Object.values(table2.section1.radioBox).map((name, key) => ({ key: key, value: name, text: name }));

class StructureDesignClassBFormComponent extends Component {
	state = {
		isSucess: false,
		designPhilosophy: 0,
		aluminiumStructure: 0,
		provFutExt: 0,
		strDesFutExt: 0,
	};

	constructor(props) {
		super(props);
		this.StructuralClassBFormPrint = React.createRef();

		const { otherData, prevData, permitData } = this.props;

		const jsonData = getJsonData(prevData);

		const desApprovJsonData = otherData.designApproval;

		let buildingClass = table1.sub_section.cell_1.checkbox.find(value => value.code === desApprovJsonData.buildingClass);

		const floorArray = new FloorBlockArray(permitData.floor);
		const topFloor = floorArray.getTopFloor();

		let numberOfStorey = {};
		let sumOfHeights = {};

		if (floorArray.hasBlocks) {
			Object.entries(topFloor).forEach(([block, row], index) => (numberOfStorey[index] = row.englishCount));
			Object.entries(floorArray.getSumOfHeights()).forEach(([block, row], index) => (sumOfHeights[index] = row));
		} else {
			numberOfStorey = topFloor.englishCount;
			sumOfHeights = floorArray.getSumOfHeights();
		}

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					'1_1_qty': numberOfStorey,
					'2_1_qty': buildingClass && buildingClass.value,
					'1_2_qty': sumOfHeights,
					propOccpuType: propOccpuName[0].value,
				},
				reqFields: [],
			},
			{ obj: jsonData, reqFields: [] }
		);

		this.state = {
			initialValues,
			floorArray,
			hasBlocks: floorArray.hasBlocks,
			topFloor: topFloor,
		};
	}

	handleLoadCombinations = e => {
		const id = e.target.id;

		this.setState({
			designPhilosophy: parseInt(id),
		});
	};

	handleProvisonExt = e => {
		const id = e.target.id;
		this.setState({ provFutExt: parseInt(id) });
	};

	handleStrDesFutExt = e => {
		const id = e.target.id;
		this.setState({ strDesFutExt: parseInt(id) });
	};

	handleAlumuStruct = e => {
		const id = e.target.id;

		this.setState({
			aluminiumStructure: parseInt(id),
		});
	};

	// handlePropOccpuType = e => {
	//   console.log('handlePropOccpuType', e.target.value);
	// };

	render() {
		const { initialValues, floorArray, hasBlocks, topFloor } = this.state;
		const { permitData, formUrl, hasSavePermission, prevData, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<React.Fragment>
				{/* <div style={{ display: 'none' }}>
					<div ref={this.props.setRef}>
						<StructureDesignClassBPrintForm />
					</div>
				</div> */}
				<div className="view-Form sdcbFormContentWrap" ref={this.props.setRef}>
					<Formik
						initialValues={initialValues}
						validationSchema={
							getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR
								? undefined
								// : isBirtamod
								// ? StructureDesignBBirtamodSchema
								: StructureDesignBSchema(hasBlocks)
						}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);
							try {
								await this.props.postAction(`${api.structureDesignB}${this.props.permitData.applicantNo}`, values);

								actions.setSubmitting(false);

								window.scrollTo(0, 0);
								if (this.props.success && this.props.success.success) {
									let nextUrl = getClassWiseNextUrl(this.props.parentProps.location.pathname);
									// showToast('Your data has been successfully');
									// this.props.parentProps.history.push(
									// 	getClassWiseNextUrl(
									// 		this.props.parentProps.location.pathname
									// 	)
									// 	// getNextUrl(this.props.parentProps.location.pathname)
									// 	// '/user/forms/anusuchiga-view'
									// 	// '/user/forms/electrical-design'
									// );
									handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success, nextUrl);
								}
							} catch (err) {
								actions.setSubmitting(false);
								window.scrollTo(0, 0);
							}
						}}
						render={props => {
							return (
								<Form loading={props.isSubmitting}>
									{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
									<div>
										<FormHeading />

										<Table celled structured className="sdcbFirstTable english-div" id="english-div">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={1}>{sdcb.table.heading.heading_1}</Table.HeaderCell>
													<Table.HeaderCell width={8}>{sdcb.table.heading.heading_2}</Table.HeaderCell>
													<Table.HeaderCell width={4}>{sdcb.table.heading.heading_3}</Table.HeaderCell>
													<Table.HeaderCell width={3}>{sdcb.table.heading.heading_4}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell colSpan={4}>{table1.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section1.description}</Table.Cell>
													<Table.Cell>
														{hasBlocks ? (
															Object.entries(topFloor).map(([block, obj], index) => (
																<TableInputIm name={`1_1_qty.${index}`} label={`Block ${block}`} />
															))
														) : (
															<TableInputIm name="1_1_qty" />
														)}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_1_remark" />
													</Table.Cell>	
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section2.description}</Table.Cell>
													<Table.Cell>
														{hasBlocks ? (
															Object.entries(topFloor).map(([block, obj], index) => (
																<TableInputIm name={`1_2_qty.${index}`} label={`Block ${block}`} />
															))
														) : (
															<TableInputIm name="1_2_qty" />
														)}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section3.description}</Table.Cell>
													<Table.Cell>
														{table1.section.section3.input.checkbox.map(name => {
															return (
																<div
																	key={name}
																	// className={
																	// 	props.errors['1_3_qty']
																	// 		? 'error field'
																	// 		: 'field'
																	// }
																>
																	<div key={name} className="ui checkbox">
																		<FormikCheckbox
																			name="1_3_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																	{/* {props.errors['1_3_qty'] && (
																			<div className="ui left pointing prompt">
																				{props.errors['1_3_qty']}
																			</div>
																		)} */}
																</div>
															);
														})}
														{props.errors['1_3_qty'] && (
															<Label pointing={'top'} prompt basic size="small" color="red">
																{props.errors['1_3_qty']}
															</Label>
														)}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section4.description}</Table.Cell>
													<Table.Cell>
														<div className={props.errors['1_4_qty'] ? 'error field' : 'field'}>
															<div className="ui fluid input">
																<Field type="text" name="1_4_qty" />
															</div>
															{props.errors['1_4_qty'] && (
																<div className="ui pointing above prompt label">{props.errors['1_4_qty']}</div>
															)}
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_4_remark" />
													</Table.Cell>
												</Table.Row>

												{/*newly added */}

												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section6.description}</Table.Cell>
													<Table.Cell>
														<div className={props.errors['1_4_qty'] ? 'error field' : 'field'}>
															<div className="ui fluid input">
																<Field type="text" name="1_6_qty" />
															</div>
															{props.errors['1_6_qty'] && (
																<div className="ui pointing above prompt label">{props.errors['1_6_qty']}</div>
															)}
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_6_remark" />
													</Table.Cell>
												</Table.Row>



												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section7.description}</Table.Cell>
													<Table.Cell>
														<div className={props.errors['1_7_qty'] ? 'error field' : 'field'}>
															<div className="ui fluid input">
																<Field type="text" name="1_7_qty" />
															</div>
															{props.errors['1_7_qty'] && (
																<div className="ui pointing above prompt label">{props.errors['1_7_qty']}</div>
															)}
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_7_remark" />
													</Table.Cell>
												</Table.Row>

												{/* newly added end */}




												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section5.description_1}</Table.Cell>
													<Table.Cell>
														{table5.section5.radiobox.map(input => (
															<RadioInput name="1_5_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_5_remark" />
													</Table.Cell>
												</Table.Row>
												{getIn(props.values, `1_5_qty`) === 'Yes' && (
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table1.section.section5.description_2}</Table.Cell>
														<Table.Cell>
															<div className={props.errors['1_6_qty'] ? 'error field' : 'field'}>
																<div className="ui right labeled input">
																	<Field type="text" name="1_6_qty" style={{ width: '200px' }} />
																	<div className="ui label">Floors</div>
																</div>
																{props.errors['1_6_qty'] && (
																	<div className="ui pointing above prompt label">{props.errors['1_6_qty']}</div>
																)}
															</div>
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="1_6_remark" />
														</Table.Cell>
													</Table.Row>
												)}
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.section.section5.description_3}</Table.Cell>
													<Table.Cell>
														{table1.section.section5.radio.map(input => (
															<RadioInput name="1_7_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="1_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell colSpan="4">{table1.heading_2}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table1.sub_section.cell_1.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_1.description}</Table.Cell>
													<Table.Cell>
														{table1.sub_section.cell_1.checkbox.map((name, index) => (
															<div key={index} className="ui radio checkbox">
																<input
																	type="radio"
																	name="2_1_qty"
																	defaultChecked={props.values['2_1_qty'] === name.value}
																	defaultValue={name.value}

																/>
																<label>{name.value}</label>
															</div>
														))}
														{props.errors['2_1_qty'] && (
															<div className="ui pointing above basic label red">{props.errors['2_1_qty']}</div>
														)}
														{/* {table1.sub_section.cell_1.checkbox.map(name => {
                                return (
                                  <div key={name} className={props.errors['2_1_qty'] ? 'error field' :  'field'}>
                                    <div className="ui checkbox">
                                      <FormikCheckbox
                                        name="2_1_qty"
                                        labelname={name}
                                        onChange={props.handleChange}
                                        value={name}
                                      />
                                    </div>
                                    {props.errors['2_1_qty'] && <div className="ui pointing above  prompt label">{props.errors['2_1_qty']}</div>}
                                  </div>
                                );
                              })} */}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table1.sub_section.cell_2.heading}</Table.Cell>
												</Table.Row>
												<Table.Row className="materialSpecsRow">
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_2.description}</Table.Cell>
													<Table.Cell>
														{table1.sub_section.cell_2.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_2_1_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_2_1_cem_remark" />
														<Field type="text" name="2_2_1_faggSand_remark" />
														<Field type="text" name="2_2_1_natBuiSt_remark" />
														<Field type="text" name="2_2_1_til_remark" />
														<Field type="text" name="2_2_1_metFram_remark" />
														<Field type="text" name="2_2_1_coaAgg_remark" />
														<Field type="text" name="2_2_1_buiLim_remark" />
														<Field type="text" name="2_2_1_bri_remark" />
														<Field type="text" name="2_2_1_tim_remark" />
														<Field type="text" name="2_2_1_strStel_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_2.description_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_2_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_2_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table1.sub_section.cell_3.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_3.description}</Table.Cell>
													<Table.Cell>
														{table1.sub_section.cell_3.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_3_1_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_3_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table1.sub_section.cell_3.description_1}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_3.description_2}</Table.Cell>
													<Table.Cell>
														<div className="ui right labeled input">
															<Field type="text" name="2_3_2_1_qty" style={{ width: '200px' }} />
															<div className="ui label">weight</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_3_2_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_3.description_3}</Table.Cell>
													<Table.Cell>
														<div className="ui right labeled input">
															<Field type="text" name="2_3_2_2_qty" style={{ width: '200px' }} />
															<div className="ui label">weight</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_3_2_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_3.description_4}</Table.Cell>
													<Table.Cell>
														<div className="ui right labeled input">
															<Field type="text" name="2_3_2_3_qty" style={{ width: '200px' }} />
															<div className="ui label">weight</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_3_2_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table1.sub_section.cell_3.description_5}</Table.Cell>
													<Table.Cell>
														<div className="ui right labeled input">
															<Field type="text" name="2_3_2_4_qty" style={{ width: '200px' }} />
															<div className="ui label">weight</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_3_2_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table1.sub_section.note}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table2.section1.heading}</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbSecondTable">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={1} rowSpan="2"></Table.HeaderCell>
													<Table.HeaderCell width={6}>{table2.section1.tableHead1}</Table.HeaderCell>
													<Table.HeaderCell width={5} colSpan="2">
														{table2.section1.tableHead2}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>Remarks</Table.HeaderCell>
												</Table.Row>
												<Table.Row>
													<Table.HeaderCell></Table.HeaderCell>
													<Table.HeaderCell>{table2.section1.tableHead3}</Table.HeaderCell>
													<Table.HeaderCell>{table2.section1.tableHead4}</Table.HeaderCell>
													<Table.HeaderCell></Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="4">
														<Form.Field width="4" error={!!props.errors.propOccpuType}>
															<Select
																options={propOccpuName}
																name="propOccpuType"
																id="propOccpu"
																className={props.errors.propOccpuType ? 'error' : ''}
																value={props.values.propOccpuType}
																placeholder="Select proposed occupancy type"
																onChange={(e, { value }) => props.setFieldValue('propOccpuType', value)}
															/>
															{props.errors.propOccpuType && (
																<Label pointing prompt size="small">
																	{props.errors.propOccpuType}
																</Label>
															)}
														</Form.Field>
													</Table.Cell>
												</Table.Row>
												{props.values.propOccpuType === 'For Residental Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_1.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_1_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_1.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_2_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_1.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_1_3_remark" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'For Hotels, Hostels, Dormitories' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_2.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_1_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_2.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_2_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_2.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_3_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_2.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_4_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_2.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_2_5_remark" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'For Educational Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_3.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_1_remark" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_3.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_3.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_3.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_3.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_3_5_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'For Institutional Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_4.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_4.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_4.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_4.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_4.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_4_5_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'For Assembly Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_5.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_5.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_5.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_5.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_5.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_5_5_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'For Business and Office Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_6.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_6.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_6.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_6.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_6.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_6_5_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'Mercantile Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_7.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_7.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_7.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_7.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_7.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_7_5_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'Industrial Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_4_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_5}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_5_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_5_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_5_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_8.td_6}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_6_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_6_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_8_6_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}

												{props.values.propOccpuType === 'Storage Buildings' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_9.td_1}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_1_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_1_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_1_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_9.td_2}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_2_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_2_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_2_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_9.td_3}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_3_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_3_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_3_rmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell>{table2.section1.tbody.tRow_9.td_4}</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_4_UD" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_4_CL" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_4_9_4_rmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbThirdTable english-div">
											<Table.Body>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.section1.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell width={1}></Table.Cell>
													<Table.Cell width={6}>{table3.section1.description_1}</Table.Cell>
													<Table.Cell width={5}>
														<Field type="text" name="2_5_1_WZ" />
													</Table.Cell>
													<Table.Cell width={3}>
														<Field type="text" name="2_5_1_WNrMRk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section1.description_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_5_2_WZ" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_5_2_WNrMRk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.section2.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_1}</Table.Cell>
													<Table.Cell>
														{table3.section2.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_6_1_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_3}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_4}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_4_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_5}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_5_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_6}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_6_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_6_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section2.description_7}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_7_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_6_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.section3.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section3.description_1}</Table.Cell>
													<Table.Cell>
														{table3.section3.checkbox.map(name => {
															return (
																<div key={name} className="ui checkbox">
																	<FormikCheckbox
																		name="2_7_1_qty"
																		labelname={name}
																		onChange={props.handleChange}
																		value={name}
																	/>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section3.description_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section3.description_3}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section3.description_4}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_4_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_7_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.section4.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section4.description_1}</Table.Cell>
													<Table.Cell>
														{table3.section4.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_8_1_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_8_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.section5.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_1}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_1_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_3}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_4}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_4_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_5}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_5_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_6}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_6_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_6_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table3.section5.description_7}</Table.Cell>
													<Table.Cell>
														{table3.section5.radiobox.map(input => (
															<RadioInput name="2_9_7_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="3">{table3.note}</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbFourthTable english-div">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={7}>{table4.section.thead1}</Table.HeaderCell>
													<Table.HeaderCell width={5}>{table4.section.thead2}</Table.HeaderCell>
													<Table.HeaderCell width={3}>{table4.section.thead3}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell>{table4.section.tbody.td_1}</Table.Cell>
													<Table.Cell>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_east_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_1}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_east_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_2}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_2}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox checkboxWithInputField">
																<FormikCheckbox
																	name="2_9_8_east_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_3.line_1}
																/>
																<Field
																	type="text"
																	name="2_9_8_east_qty_building_structure"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_2}
																<Field
																	type="text"
																	name="2_9_8_east_qty_building_floor"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_3}
															</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_8_east_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{table4.section.tbody.td_2}</Table.Cell>
													<Table.Cell>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_west1_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_1}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_west1_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_2}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_2}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox checkboxWithInputField">
																<FormikCheckbox
																	name="2_9_8_west1_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_3.line_1}
																/>
																<Field
																	type="text"
																	name="2_9_8_west1_qty_building_structure"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_2}
																<Field
																	type="text"
																	name="2_9_8_west_qty_building_floor"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_3}
															</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_8_west_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{table4.section.tbody.td_3}</Table.Cell>
													<Table.Cell>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_north_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_1}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_north_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_2}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_2}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox checkboxWithInputField">
																<FormikCheckbox
																	name="2_9_8_north_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_3.line_1}
																/>
																<Field
																	type="text"
																	name="2_9_8_north_qty_building_structure"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_2}
																<Field
																	type="text"
																	name="2_9_8_north_qty_building_floor"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_3}
															</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_8_north_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{table4.section.tbody.td_4}</Table.Cell>
													<Table.Cell>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_south_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_1}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox">
																<FormikCheckbox
																	name="2_9_8_south_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_2}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_2}
																/>
															</div>
														</div>
														<div className="field">
															<div className="ui checkbox checkboxWithInputField">
																<FormikCheckbox
																	name="2_9_8_south_qty"
																	labelname={table4.section.tbody.checkbox.checkbox_3.line_1}
																	onChange={props.handleChange}
																	value={table4.section.tbody.checkbox.checkbox_3.line_1}
																/>
																<Field
																	type="text"
																	name="2_9_8_south_qty_building_structure"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_2}
																<Field
																	type="text"
																	name="2_9_8_south_qty_building_floor"
																	className="dashedForm-control"
																/>
																{table4.section.tbody.checkbox.checkbox_3.line_3}
															</div>
														</div>
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_9_8_south_remark" />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbFifthTable english-div">
											<Table.Body>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section1.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell width={1}></Table.Cell>
													<Table.Cell width={6}>{table5.section1.description_1}</Table.Cell>
													<Table.Cell width={5} colSpan="3">
														<Field type="text" name="2_10_1_qty" />
													</Table.Cell>
													<Table.Cell width={3}>
														<Field type="text" name="2_10_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_2}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_10_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_3}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_10_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.sub_section.thead_1}</Table.Cell>
													<Table.Cell>{table5.section1.sub_section.thead_2}</Table.Cell>
													<Table.Cell>{table5.section1.sub_section.thead_3}</Table.Cell>
													<Table.Cell>{table5.section1.sub_section.thead_4}</Table.Cell>
													<Table.Cell></Table.Cell>
												</Table.Row>
												{floorArray.getFloorsWithLabels().map(currentRow => {
													return (
														<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
															<Table.Cell></Table.Cell>
															<Table.Cell>{`${getFloorEnglish(currentRow.floor)} ${hasBlocks &&
																'Block ' + currentRow.block}`}</Table.Cell>
															<Table.Cell>
																<Field
																	type="text"
																	name={`2_10_4_floor${currentRow.floor}_${currentRow.block}_WallHeight`}
																/>
															</Table.Cell>
															<Table.Cell>
																<Field
																	type="text"
																	name={`2_10_4_floor${currentRow.floor}_${currentRow.block}_WallThick`}
																/>
															</Table.Cell>
															<Table.Cell>
																<Field
																	type="text"
																	name={`2_10_4_floor${currentRow.floor}_${currentRow.block}_MaxLeng`}
																/>
															</Table.Cell>
															<Table.Cell>
																<Field
																	type="text"
																	name={`2_10_4_floor${currentRow.floor}_${currentRow.block}_remark`}
																/>
															</Table.Cell>
														</Table.Row>
													);
												})}
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_4}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section1.checkbox.checkbox_1.map(input => (
															<RadioInput name="2_10_5_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_5}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section1.checkbox.checkbox_1.map(input => (
															<RadioInput name="2_10_6_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_6_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_6}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section1.checkbox.checkbox_1.map(input => (
															<RadioInput name="2_10_7_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_7}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section1.checkbox.checkbox_1.map(input => (
															<RadioInput name="2_10_8_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_8_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_8}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section1.checkbox.checkbox_2.map(name => {
															return (
																<div key={name} className="ui checkbox">
																	<FormikCheckbox
																		name="2_10_9_qty"
																		labelname={name}
																		onChange={props.handleChange}
																		value={name}
																	/>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_9_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section1.sub_section2.heading}</Table.Cell>
												</Table.Row>
												{floorArray.getFloorsWithLabels().map(currentRow => {
													// if (currentRow.floor < 11) {
													return (
														<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
															<Table.Cell></Table.Cell>
															<Table.Cell>{`${getFloorEnglish(currentRow.floor)} ${hasBlocks &&
																'Block ' + currentRow.block}`}</Table.Cell>
															<Table.Cell colSpan="3">
																<Input
																	name={`2_10_floor${currentRow.floor}_${currentRow.block}_qty`}
																	onChange={props.handleChange}
																	value={props.values[`2_10_floor${currentRow.floor}_${currentRow.block}_qty`]}
																/>
															</Table.Cell>
															<Table.Cell>
																<Input
																	name={`2_10_floor${currentRow.floor}_${currentRow.block}_recommendation`}
																	onChange={props.handleChange}
																	value={
																		props.values[
																			`2_10_floor${currentRow.floor}_${currentRow.block}_recommendation`
																		]
																	}
																/>
															</Table.Cell>
														</Table.Row>
													);
													// } else return null;
												})}
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section1.description_9}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_10_10_c_c_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_10_10_c_c_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section2.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_1}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_1_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_2}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_3}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_4}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_4_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_5}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_5_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_6}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_6_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_6_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_7}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_7_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_8}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_8_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_8_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_9}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_9_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_9_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_10}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_10_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_10_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.description_11}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_11_11_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_11_remark" />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbSixthTable english-div">
											<Table.Body>
												<Table.Row>
													<Table.Cell width={1}></Table.Cell>
													<Table.Cell width={6}>{table5.section2.sub_section.thead_1}</Table.Cell>
													<Table.Cell width={4} colSpan="4">
														{table5.section2.sub_section.thead_2}
													</Table.Cell>
													<Table.Cell width={3}></Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.sub_section.thead_3}</Table.Cell>
													<Table.Cell>{table5.section2.sub_section.thead_4}</Table.Cell>
													<Table.Cell>{table5.section2.sub_section.thead_5}</Table.Cell>
													<Table.Cell>{table5.section2.sub_section.thead_6}</Table.Cell>
													<Table.Cell></Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_1}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_mSDrCanLev" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_mSDrSimSup" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_mSDrOnSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_mSDrBoSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_mSDrRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_2}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_spCorBeCanLev" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_spCorBeSimSup" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_spCorBeOnSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_spCorBeBoSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_spCorBeRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_3}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_depCorTeCanLev" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_depCorTeSimSup" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_depCorTeOnSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_depCorTeBoSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_depCorTeRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell textAlign="right">{table5.section2.sub_section.td_cell_4}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_widCorTeCanLev" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_widCorTeSimSup" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_widCorTeOnSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_widCorTeBoSiCon" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_12_widCorTeRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.descriptio_12}</Table.Cell>
													<Table.Cell colSpan="4">
														<Field type="text" name="2_11_13_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_13_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.descriptio_13}</Table.Cell>
													<Table.Cell colSpan="4">
														{table5.section2.radiobox.map(input => (
															<RadioInput name="2_11_14_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_14_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section2.sub_section2.heading}</Table.Cell>
												</Table.Row>
												{getIn(props.values, `2_11_14_qty`) === 'Limit state method' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">{table5.section2.radiobox[0]} 1</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_1_limStaMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_1_limStaMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">2</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_2_limStaMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_2_limStaMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">3</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_3_limStaMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_3_limStaMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">4</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_4_limStaMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_4_limStaMethRmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}
												{getIn(props.values, `2_11_14_qty`) === 'Working stress method' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">{table5.section2.radiobox[1]} 1</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_1_workStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_1_workStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">2</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_2_workStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_2_workStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">3</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_3_workStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_3_workStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">4</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_4_workStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_4_workStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}
												{getIn(props.values, `2_11_14_qty`) === 'Unlimited strength method' && (
													<>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">{table5.section2.radiobox[2]} 1</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_1_unLimStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_1_unLimStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">2</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_2_unLimStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_2_unLimStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">3</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_3_unLimStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_3_unLimStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell></Table.Cell>
															<Table.Cell textAlign="right">4</Table.Cell>
															<Table.Cell colSpan="4">
																<Field type="text" name="2_11_14_4_unLimStrMeth" />
															</Table.Cell>
															<Table.Cell>
																<Field type="text" name="2_11_14_4_unLimStrMethRmrk" />
															</Table.Cell>
														</Table.Row>
													</>
												)}
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.descriptio_14}</Table.Cell>
													<Table.Cell colSpan="4">
														<Field type="text" name="2_11_15_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_15_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section2.descriptio_15}</Table.Cell>
													<Table.Cell colSpan="4">
														<Field type="text" name="2_11_16_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_11_16_remark" />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<Table celled structured className="sdcbSeventhTable english-div">
											<Table.Body>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section3.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell width={1}></Table.Cell>
													<Table.Cell width={6}>{table5.section3.description_1}</Table.Cell>
													<Table.Cell width={5} colSpan="3">
														{table5.section3.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_12_1_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell width={3}>
														<Field type="text" name="2_12_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_2}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_12_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_3}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_12_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.sub_section.thead_1}</Table.Cell>
													<Table.Cell>{table5.section3.sub_section.thead_2}</Table.Cell>
													<Table.Cell>{table5.section3.sub_section.thead_3}</Table.Cell>
													<Table.Cell>{table5.section3.sub_section.thead_4}</Table.Cell>
													<Table.Cell></Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_4}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_expSecPip" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_expSecWbStnd" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_expSecCompSec" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_expSecRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_5}</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_ntexpSecPip" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_ntexpSecWbStnd" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_ntexpSecCompSec" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_4_ntexpSecRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_6}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section3.radiobox.map(input => (
															<RadioInput name="2_12_5_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_7}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_12_6_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_6_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_8}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section3.radiobox.map(input => (
															<RadioInput name="2_12_7_qty" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_7_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section3.description_9}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_12_8_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_12_8_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section4.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section4.description_1}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_13_1_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_13_1_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section4.description_2}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_13_2_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_13_2_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section4.description_3}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_13_3_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_13_3_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section4.description_4}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_13_4_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_13_4_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section4.description_5}</Table.Cell>
													<Table.Cell colSpan="3">
														<Field type="text" name="2_13_5_qty" />
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_13_5_remark" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section5.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section5.description_1}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section5.radiobox.map(input => (
															<RadioInput name="alumStruMem" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_14_1_remark" />
													</Table.Cell>
												</Table.Row>
												{getIn(props.values, `alumStruMem`) === 'Yes' && (
													<Table.Row>
														<Table.Cell></Table.Cell>
														<Table.Cell>{table5.section5.description_2}</Table.Cell>
														<Table.Cell colSpan="3">
															<Field type="text" name="2_14_2_menDesName" />
														</Table.Cell>
														<Table.Cell>
															<Field type="text" name="2_14_2_menDesNameRmrk" />
														</Table.Cell>
													</Table.Row>
												)}
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell colSpan="6">{table5.section6.heading}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section6.description_1}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section6.radiobox.map(input => (
															<RadioInput name="2_15_1_safetyMeasUse" key={input} option={input} />
														))}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_15_1_safetyMeasUseRmrk" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell></Table.Cell>
													<Table.Cell>{table5.section6.description_2}</Table.Cell>
													<Table.Cell colSpan="3">
														{table5.section6.checkbox.map(name => {
															return (
																<div key={name} className="field">
																	<div className="ui checkbox">
																		<FormikCheckbox
																			name="2_15_2_qty"
																			labelname={name}
																			onChange={props.handleChange}
																			value={name}
																		/>
																	</div>
																</div>
															);
														})}
													</Table.Cell>
													<Table.Cell>
														<Field type="text" name="2_15_2_safeWareUseRmrk" />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
									</div>
									<p>{table5.note}</p>
									<SaveButtonValidation
										errors={props.errors}
										formUrl={formUrl}
										hasSavePermission={hasSavePermission}
										hasDeletePermission={hasDeletePermission}
										isSaveDisabled={isSaveDisabled}
										prevData={checkError(prevData)}
										handleSubmit={props.handleSubmit}
										validateForm={props.validateForm}
									/>
								</Form>
							);
						}}
					/>
				</div>
			</React.Fragment>
		);
	}
}

export const FormHeading = React.memo(() => (
	<SectionHeader>
		<h3>{sdcb.heading.heading_1}</h3>
		<h3 className="end-section">{sdcb.heading.heading_2}</h3>
		<h4 className="english-div underline">{sdcb.heading.heading_3}</h4>
		<h4 className="english-div end-section">{sdcb.heading.heading_4}</h4>
	</SectionHeader>
));

const StructureDesignClassBForm = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.structureDesignB, objName: 'structB', form: true },
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui selection dropdown', 'innerText', propOccpuName],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		render={props => <StructureDesignClassBFormComponent {...props} parentProps={parentProps} />}
	/>
);

export default StructureDesignClassBForm;
