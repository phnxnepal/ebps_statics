import React, { Component } from 'react';
import { Form, Table } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import { anushuchiKha } from '../../../../utils/data/mockLangFile';
import { getMessage } from '../../../shared/getMessage';
import * as Yup from 'yup';
import { LabeledInputWithVariableUnit } from '../../../shared/LabeledInput';
import { replaceDot, parseKey } from '../../../../utils/functionUtils';

import api from '../../../../utils/api';
import { validateString } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { prepareArchiData, handleSuccess } from '../../../../utils/dataUtils';
import { checkError } from './../../../../utils/dataUtils';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { EnglishField } from '../../../shared/fields/EnglishField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { PurposeOfConstructionEnglishDropdown } from '../mapPermitComponents/PurposeOfConstructionRadio';

let tabledata = anushuchiKha.architectureDesignRequirements;
const data = anushuchiKha.architectureDesignRequirements;
const messageId = 'anusuchiKha.architectureDesignRequirements';

const ArchiClassBSchema = Yup.object().shape(
	Object.assign(
		{
			buildingtype: validateString,
		}
		// ...formDataSchema
	)
);

class ArchitecturalDesignRequirementsClassBFormComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData, listData, groups } = prepareArchiData(props.prevData);

		const initialValues = {
			...prevData,
			buildingtype: getIn(props, 'prevData.buildingtype') || this.props.permitData.purposeOfConstruction || '',
			buildingTypeOther: getIn(props, 'prevData.buildingTypeOther') || this.props.permitData.purposeOfConstructionOther || '',
			// buildingtype: this.props.permitData.purposeOfConstruction,
			// buildingTypeOther: this.props.permitData.purposeOfConstructionOther,
		};

		let list =
			this.props.prevData && !isStringEmpty(this.props.prevData.enterBy) && !isStringEmpty(this.props.prevData.enterDate)
				? this.props.prevData
				: {};

		this.state = {
			initialValues,
			list,
			listData,
			groups,
		};
	}

	render() {
		const { initialValues, list, listData, groups } = this.state;
		const { permitData, hasDeletePermission, isSaveDisabled, hasSavePermission, formUrl } = this.props;

		return (
			<>
				<Formik
					enableReinitialize
					initialValues={initialValues}
					validationSchema={
						getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR ? undefined : ArchiClassBSchema
					}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						const dataToSend = {
							applicationNo: permitData.applicantNo,
							buildingtype: values.buildingtype,
							buildingType: values.buildingtype,
							buildingTypeOther: values.buildingTypeOther,
							obj: [],
						};

						Object.keys(values).forEach((key) => {
							const { groupId, classId, field } = parseKey(key);
							listData
								.filter((doc) => {
									return doc.groupId === groupId && doc.classId === classId;
								})
								.forEach((updatedRow) => {
									const index = dataToSend.obj.findIndex((x) => {
										return x.classId === updatedRow.classId;
									});
									if (index > -1) {
										dataToSend.obj[index] = Object.assign(dataToSend.obj[index], { [field]: values[key] });
									} else {
										dataToSend.obj.push({
											classId: updatedRow.classId,
											[field]: values[key],
										});
									}
								});
						});

						// console.log('"Data to send"', dataToSend);
						try {
							await this.props.postAction(api.architecturalDesignB, dataToSend, true);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// const url = getNextUrl(
								// 	this.props.parentProps.location.pathname
								// );
								// setTimeout(() => {
								// 	this.props.parentProps.history.push(url);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Architecture form class b submission failed. ', err);
						}
					}}
					render={(props) => {
						// console.log('form values --   ', props.values);
						return (
							<Form
								// onSubmit={props.handleSubmit}
								loading={props.isSubmitting}
							>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="english-div">
									<FormHeading />
									<Form.Field inline>
										{/* <div style={{ padding: '30px 0 30px 0' }}> */}
										{/* <Form.Input error={props.errors.buildingtype}> */}
										<PurposeOfConstructionEnglishDropdown
											values={props.values}
											errors={props.errors}
											setFieldValue={props.setFieldValue}
											fieldLabel="Type of Building: "
										/>
										{/* <Label basic>Type of Building</Label>
												<span style={{ marginLeft: '10px' }}>
													<Input
														name="buildingtype"
														className="dashedForm-control"
														onChange={props.handleChange}
														// label="Type of Building"
														value={props.values.buildingtype}
													/>
												</span> */}
										{/* </Form.Input> */}
										{/* </div> */}
									</Form.Field>
									<Table unstackable celled collapsing compact>
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width={8}>Building Elements</Table.HeaderCell>
												<Table.HeaderCell width={4}>As per Submitted Design</Table.HeaderCell>
												<Table.HeaderCell width={4}>Remarks</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										{Object.keys(groups).map((groupId) => {
											// console.log('props formData', props);
											return (
												<Table.Body key={groupId}>
													<Table.Row>
														<Table.Cell>
															<em className="english-div">{`${groupId}. ${groups[groupId]}`}</em>
														</Table.Cell>
														<Table.Cell></Table.Cell>
														<Table.Cell></Table.Cell>
													</Table.Row>
													{listData
														.filter((row) => row.groupId === groupId)
														.map((row) => {
															const qtyField = `${replaceDot(row.classId.toString())}_qty`;
															const remarkField = `${replaceDot(row.classId.toString())}_remarks`;
															const unitField = `${replaceDot(row.classId.toString())}_usedUnit`;
															return (
																<Table.Row key={row.classId}>
																	<Table.Cell key={'id'}>
																		<span
																			style={{
																				marginLeft: '20px',
																			}}
																		>{`${row.classId} `}</span>
																		<span
																			style={{
																				marginLeft: '20px',
																			}}
																		>
																			{row.buildingElements}
																		</span>
																	</Table.Cell>
																	{row.classId === '1.3' ? (
																		[<Table.Cell key="sub-id"></Table.Cell>]
																	) : row.classId === '1.302' ? (
																		[<Table.Cell key="sub-id-2"></Table.Cell>]
																	) : row.classId === '2.4' ? (
																		[
																			<Table.Cell>
																				{tabledata.radiobutton_1.map((input, idx) => (
																					// <Table.Cell>
																					<div key={idx} className="ui radio checkbox prabidhik">
																						<input
																							type="radio"
																							name={`${qtyField}`}
																							value={input}
																							checked={getIn(props.values, `${qtyField}`) === input}
																							onChange={props.handleChange}
																						/>
																						<label htmlFor={`${qtyField}`}>{input}</label>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>,
																		]
																	) : row.classId === '4.2' ? (
																		[
																			<Table.Cell>
																				{tabledata.radiobutton_2.map((input, idx) => (
																					// <Table.Cell>
																					<div key={idx} className="ui radio checkbox prabidhik">
																						<input
																							type="radio"
																							name={`${qtyField}`}
																							value={input}
																							checked={getIn(props.values, `${qtyField}`) === input}
																							onChange={props.handleChange}
																						/>
																						<label htmlFor={`${qtyField}`}>{input}</label>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>,
																		]
																	) : row.classId === '5.1' ? (
																		[
																			<Table.Cell>
																				{tabledata.radiobutton_3.map((input, idx) => (
																					// <Table.Cell>
																					<div key={idx} className="ui radio checkbox prabidhik">
																						<input
																							type="radio"
																							name={`${qtyField}`}
																							value={input}
																							checked={getIn(props.values, `${qtyField}`) === input}
																							onChange={props.handleChange}
																						/>
																						<label htmlFor={`${qtyField}`}>{input}</label>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>,
																		]
																	) : (
																		<Table.Cell>
																			<Form.Field>
																				<Form.Input error={props.errors[qtyField]}>
																					<LabeledInputWithVariableUnit
																						suffix={row.unit}
																						name={qtyField}
																						unitField={unitField}
																						values={props.values}
																						handleChange={props.handleChange}
																						setFieldValue={props.setFieldValue}
																						value={props.values[qtyField]}
																					/>
																				</Form.Input>
																			</Form.Field>
																		</Table.Cell>
																	)}
																	{row.classId === '1.3' ? (
																		[<Table.Cell></Table.Cell>]
																	) : row.classId === '1.302' ? (
																		[<Table.Cell></Table.Cell>]
																	) : (
																		<Table.Cell>
																			<EnglishField placeholder="Remarks" name={remarkField} />
																		</Table.Cell>
																	)}
																</Table.Row>
															);
														})}
												</Table.Body>
											);
										})}
									</Table>
								</div>

								<br />
								<SaveButtonValidation
									errors={props.errors}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={list}
									handleSubmit={props.handleSubmit}
									validateForm={props.validateForm}
								/>
							</Form>
						);
					}}
				/>
			</>
		);
	}
}

const FormHeading = React.memo(() => (
	<>
		<h3 style={{ textAlign: 'center' }}>{getMessage(`${messageId}.heading`, data.heading)}</h3>
		<span style={{ textAlign: 'center' }}>
			<p>
				{getMessage(`${messageId}.subheading1`, data.subheading1)} <br />
				{getMessage(`${messageId}.subheading2`, data.subheading2)}
				<br />
				{getMessage(`${messageId}.subheading3`, data.subheading3)}
			</p>
		</span>

		<span style={{ textAlign: 'center' }}>
			<p>
				{getMessage(`${messageId}.subheading4`, data.subheading4)} <br />
				{getMessage(`${messageId}.subheading5`, data.subheading5)}
			</p>
		</span>

		<span style={{ textAlign: 'center' }}>
			<h5>{getMessage(`${messageId}.note`, data.note)}</h5>
		</span>
	</>
));

const ArchitecturalDesignRequirementsClassBForm = (parentProps) => (
	<FormContainerV2
		api={[{ api: api.architecturalDesignB, objName: 'archiB', form: true }]}
		useInnerRef={true}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui basic label', 'innerText'],
		}}
		parentProps={parentProps}
		render={(props) => <ArchitecturalDesignRequirementsClassBFormComponent {...props} parentProps={parentProps} />}
	/>
);

export default ArchitecturalDesignRequirementsClassBForm;
