import React, { Component } from 'react';
import { Formik, getIn } from 'formik';

import { Form, Table, TableCell } from 'semantic-ui-react';
import { anushuchiKha } from '../../../../utils/data/mockLangFile';
import { replaceDot, parseKey } from '../../../../utils/functionUtils';

import { getMessage } from '../../../shared/getMessage';
import * as Yup from 'yup';
import { LabeledInputWithVariableUnit } from '../../../shared/LabeledInput';

import api from '../../../../utils/api';
import { validateString } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { prepareArchiData, handleSuccess } from '../../../../utils/dataUtils';
import { checkError } from './../../../../utils/dataUtils';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { EnglishField } from '../../../shared/fields/EnglishField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { PurposeOfConstructionEnglishDropdown } from '../mapPermitComponents/PurposeOfConstructionRadio';

let tabledata = anushuchiKha.architectureDesign;
const data = anushuchiKha.architectureDesign;
const messageId = 'anusuchiKha.architectureDesign';

class ArchitectureDesignClassCFormComponent extends Component {
	render() {
		const formattedData = [];

		const { prevData, listData, groups } = prepareArchiData(this.props.prevData);

		const { permitData, hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl } = this.props;

		formattedData.push(this.props.prevData);

		const ArchiClassCSchema = Yup.object().shape(
			Object.assign(
				{
					buildingtype: validateString,
				}
				// ...formDataSchema
			)
		);
		const initialValues = {
			...prevData,
			// buildingtype: getIn(this.props, 'prevData.buildingtype', ''),
			buildingtype: getIn(this.props, 'prevData.buildingtype') || this.props.permitData.purposeOfConstruction || '',
			buildingTypeOther: getIn(this.props, 'prevData.buildingTypeOther') || this.props.permitData.purposeOfConstructionOther || '',
		};

		let list =
			this.props.prevData && !isStringEmpty(this.props.prevData.enterBy) && !isStringEmpty(this.props.prevData.enterDate)
				? this.props.prevData
				: {};
		// console.log('schema', ArchiClassCSchema);
		return (
			<>
				<Formik
					enableReinitialize
					initialValues={initialValues}
					validationSchema={
						getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR ? undefined : ArchiClassCSchema
					}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						const dataToSend = {
							applicationNo: this.props.permitData.applicationId,
							buildingtype: values.buildingtype,
							buildingType: values.buildingtype,
							buildingTypeOther: values.buildingTypeOther,
							obj: [],
						};

						Object.keys(values).forEach((key) => {
							const { groupId, classId, field } = parseKey(key);
							listData
								.filter((doc) => {
									return doc.groupId === groupId && doc.classId === classId;
								})
								.forEach((updatedRow) => {
									const index = dataToSend.obj.findIndex((x) => {
										return x.classId === updatedRow.classId;
									});
									if (index > -1) {
										dataToSend.obj[index] = Object.assign(dataToSend.obj[index], { [field]: values[key] });
									} else {
										dataToSend.obj.push({
											classId: updatedRow.classId,
											[field]: values[key],
										});
									}
								});
						});

						try {
							await this.props.postAction(api.architecturalDesignC, dataToSend, true);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// const url = getNextUrl(
								// 	this.props.parentProps.location.pathname
								// );
								// setTimeout(() => {
								// 	this.props.parentProps.history.push(url);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Architecture form class c submission failed. ', err);
						}
					}}
					render={(props) => {
						// console.log('form values --   ', props.values);
						return (
							<Form loading={props.isSubmitting}>
								<div ref={this.props.setRef}>
									<FormHeading />

									{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
									<Form.Field inline>
										{/* <Form.Input error={props.errors.buildingtype}> */}
										<PurposeOfConstructionEnglishDropdown
											values={props.values}
											errors={props.errors}
											setFieldValue={props.setFieldValue}
											fieldLabel="Type of Building: "
										/>
										{/* <Label basic>Type of Building</Label>
											<span style={{ marginLeft: '10px' }}>
												<Input
													name="buildingtype"
													className="dashedForm-control"
													onChange={props.handleChange}
													// label="Type of Building"
													value={props.values.buildingtype}
												/>
											</span>
										</Form.Input> */}
									</Form.Field>
									<Table celled collapsing compact className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width={6}>Building Elements</Table.HeaderCell>
												<Table.HeaderCell width={4}>As per Submitted Design</Table.HeaderCell>
												<Table.HeaderCell width={6}>Remarks</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										{Object.keys(groups).map((groupId) => {
											// console.log('props formData', props);
											return (
												<Table.Body key={groupId}>
													<Table.Row>
														<TableCell>
															<em>{`${groupId}. ${groups[groupId]}`}</em>
														</TableCell>
													</Table.Row>
													{listData
														.filter((row) => row.groupId === groupId)
														.map((row) => {
															const qtyField = `${replaceDot(row.classId.toString())}_qty`;
															const remarkField = `${replaceDot(row.classId.toString())}_remarks`;
															const unitField = `${replaceDot(row.classId.toString())}_usedUnit`;
															return (
																<Table.Row key={row.classId}>
																	<Table.Cell key="name">
																		<span
																			style={{
																				marginLeft: '20px',
																			}}
																		>{`${row.classId} `}</span>
																		<span
																			style={{
																				marginLeft: '20px',
																			}}
																		>
																			{row.buildingElements}
																		</span>
																	</Table.Cell>
																	{row.classId === '4.1' ? (
																		<Table.Cell key="radio-input">
																			{tabledata.radiobutton_1.map((input, idx) => (
																				// <Table.Cell>
																				<div key={idx} className="ui radio checkbox prabidhik">
																					<input
																						type="radio"
																						name={`${qtyField}`}
																						value={input}
																						checked={getIn(props.values, `${qtyField}`) === input}
																						onChange={props.handleChange}
																					/>
																					<label htmlFor={`${qtyField}`}>{input}</label>
																				</div>
																				// </Table.Cell>
																			))}
																		</Table.Cell>
																	) : (
																		<Table.Cell key="input-input">
																			<Form.Field>
																				<Form.Input error={props.errors[qtyField]}>
																					<LabeledInputWithVariableUnit
																						suffix={row.unit}
																						name={qtyField}
																						unitField={unitField}
																						values={props.values}
																						handleChange={props.handleChange}
																						setFieldValue={props.setFieldValue}
																						value={props.values[qtyField]}
																					/>
																				</Form.Input>
																			</Form.Field>
																		</Table.Cell>
																	)}
																	<Table.Cell key="remark-input">
																		<EnglishField type="text" placeholder="Remarks" name={remarkField} />
																	</Table.Cell>
																</Table.Row>
															);
														})}
												</Table.Body>
											);
										})}
									</Table>
								</div>
								<br />
								<SaveButtonValidation
									errors={props.errors}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={list}
									handleSubmit={props.handleSubmit}
									validateForm={props.validateForm}
								/>
							</Form>
						);
					}}
				/>
			</>
		);
	}
}

const FormHeading = React.memo(() => (
	<>
		<h3 style={{ textAlign: 'center' }}>{getMessage(`${messageId}.heading`, data.heading)}</h3>
		<span style={{ textAlign: 'center' }}>
			<p>
				{getMessage(`${messageId}.subheading1`, data.subheading1)} <br />
				{getMessage(`${messageId}.subheading2`, data.subheading2)}
				<br />
				{getMessage(`${messageId}.subheading3`, data.subheading3)}
			</p>
		</span>

		<span style={{ textAlign: 'center' }} className="english-div">
			<p>
				{getMessage(`${messageId}.subheading4`, data.subheading4)} <br />
				{getMessage(`${messageId}.subheading5`, data.subheading5)}
			</p>
		</span>

		<span style={{ textAlign: 'center' }} className="english-div">
			<h5>{getMessage(`${messageId}.note`, data.note)}</h5>
		</span>
	</>
));

const ArchitectureDesignClassCForm = (parentProps) => (
	<FormContainerV2
		api={[{ api: api.architecturalDesignC, objName: 'archiC', form: true }]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui basic label', 'innerText'],
		}}
		// onBeforeGetContent={{
		// 	param1: ['getElementsByTagName', 'input', 'value'],
		// 	param2: ['getElementsByTagName', 'span', 'innerText'],
		// 	param3: ['getElementsByClassName', 'ui label', 'innerText']
		// }}
		render={(props) => <ArchitectureDesignClassCFormComponent {...props} parentProps={parentProps} />}
	/>
);

export default ArchitectureDesignClassCForm;
