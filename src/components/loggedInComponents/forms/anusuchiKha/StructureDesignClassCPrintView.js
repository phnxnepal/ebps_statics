import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Table, Input, Header } from 'semantic-ui-react';
import { anushuchiKha } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';
// import { SemanticToastContainer } from 'react-semantic-toasts';
import 'react-semantic-toasts/styles/react-semantic-alert.css';

// Redux
import { connect } from 'react-redux';
import { getFormDataWithPermit, postFormDataByUrl } from '../../../../store/actions/formActions';
import FallbackComponent from '../../../shared/FallbackComponent';
import api from '../../../../utils/api';
import { getJsonData, getNameFormat, getFloorEnglish } from '../../../../utils/dataUtils';

import { setPrint } from '../../../../store/actions/general';
import { FormHeading } from './StructureDesignClassCForm';
import { FloorBlockArray } from '../../../../utils/floorUtils';

const data = anushuchiKha.structureDesign;
const table1 = data.table1;
const table2 = data.table2;
const table3 = data.table3;
const table4 = data.table4;
// const valueForInputHeader = ['Wallheight', 'Wallthickness', 'MaximumLength'];

class StructureDesignClassCPrintView extends Component {
	constructor(props) {
		super(props);
		this.state = {
			'2_5_row3': 'Yes',
			'2_5_row4': 'Yes',
			'2_5_row5': 'Yes',
			'2_5_row6': 'Yes',
			'2_6_row1': 'Plint level',
		};
	}

	componentDidMount() {
		this.props.getFormDataWithPermit(api.structureDesignC, (data) => data);
	}

	// handleRadioButtonClicked = e => {
	//   if (e.target.id !== null) {
	//     const id = e.target.id;
	//     const value = e.target.value;
	//     this.setState(() => ({
	//       [id]: value
	//     }));
	//   }
	// };

	render() {
		// let initialValues = {};

		// console.log('prev data', this.props);

		if (!isEmpty(this.props.permitData) && !isEmpty(this.props.prevData)) {
			// const strData = [
			//   '1_A_1_qty',
			//   '1_A_2_totalheightofbuilding_qty',
			//   '1_A_2_noofstorey_qty',
			//   '1_A_3_qty',
			//   '1_A_4_qty',
			//   '1_A_5_Length_qty',
			//   '1_A_5_Breadth_qty',
			//   '1_A_6_MinNoofBays_qty',
			//   '1_A_7_qty',
			//   '1_A_8_qty',
			//   '1_A_9_qty',
			//   '1_A_10_qty',
			//   '1_A_11_qty',
			//   '1_A_12_qty',

			//   '1_B_14_qty',
			//   '1_B_18_qty',

			//   '1_C_26_sill-band-and-lintel-band',
			//   '1_C_26_no-of',
			//   '1_C_26_bars-with',
			//   '1_C_26_c-hook'
			// ];
			// const formDataSchema = strData.map(row => {
			//   return {
			//     [row]: validateString
			//   };
			// });
			// const classCSchema = Yup.object().shape(Object.assign(...formDataSchema));
			let initialValues = {};
			const permitData = this.props.permitData;
			const floorArray = new FloorBlockArray(permitData.floor);
			// const userData = this.props.userData;
			// const {
			//   erStatus: erStatus = 'P',
			//   serStatus: serStatus = 'P'
			// } = this.props.prevData;

			// const otherData = {
			//   Nameofclient: permitData.nibedakName,
			//   Address: `${permitData.newMunicipal} - ${permitData.nibedakTol}`,
			//   ContactNo: permitData.applicantMobileNo
			// };

			// let designerData = {};
			// if (userData.userType === 'D')
			//   designerData = {
			//     userName: userData.userName,
			//     Designation: getUserTypeValue(userData.userType),
			//     orgName: userData.organization.name,
			//     orgAddress: userData.organization.address,
			//     affDate: getCurrentDate()
			//   };
			// else {
			//   designerData = {
			//     userName: '',
			//     Designation: '',
			//     orgName: '',
			//     orgAddress: '',
			//     affDate: ''
			//   };
			// }
			// console.log('d', designerData);
			// if (isStringEmpty(designerData.affDate)) {
			//   designerData.affDate = getCurrentDate(true);
			// }
			const jsonData = getJsonData(this.props.prevData);

			// const floorData = permitData.floor;

			const floorCount = floorArray.getHeightRelevantFloors();

			initialValues = jsonData;

			// const FormHeading = () => (
			//   <>
			//     <h3 style={{ textAlign: 'center' }}>{data.heading1}</h3>
			//     <span style={{ textAlign: 'center' }}>
			//       <p>
			//         {data.heading2} <br />
			//         {data.heading3}
			//       </p>
			//     </span>

			//     <span style={{ textAlign: 'center' }}>
			//       <p>
			//         {data.subheading1} <br />
			//         {data.subheading2} <br />
			//         {data.subheading3} <br />
			//       </p>
			//     </span>

			//     <span style={{ textAlign: 'center' }}>
			//       <h5>{data.note1} </h5>
			//       <h5>{data.note2} </h5>
			//     </span>
			//   </>
			// );

			// const DisabledInput = props => (
			//   <Input style={{ opacity: '1' }} defaultValue={props.value} disabled />
			// );

			const RadioLabel = (props) => <label style={{ marginRight: '15px' }}>{props.value}</label>;

			return (
				<>
					<Formik
						initialValues={initialValues}
						render={(props) => {
							// console.log('Props in render --', props);
							return (
								<Form
									// onSubmit={props.handleSubmit}
									loading={props.isSubmitting}
								>
									<div>
										{/* <SemanticToastContainer /> */}
										<FormHeading />
										{/* <FormWrapper erStatus={erStatus}> */}
										<Table celled collapsing compact>
											{Object.keys(table1).map((row) => (
												<Table.Row>
													<Table.Cell>
														<Header as="h7">{table1[row]}</Header>
													</Table.Cell>
													<Table.Cell>
														<span> {props.values[table1[row].replace(/\s/g, '')]}</span>
													</Table.Cell>
												</Table.Row>
											))}
										</Table>

										<h5 className="formFeild-mainLabel english-div">{table2.heading}</h5>
										<Table celled collapsing compact className="english-div">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={1}>S.N.</Table.HeaderCell>
													<Table.HeaderCell width={8}>Provision</Table.HeaderCell>
													<Table.HeaderCell width={3}>Area</Table.HeaderCell>
													<Table.HeaderCell width={4}>Recommendations</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.HeaderCell colSpan={3}>{table2.sectionA.heading}</Table.HeaderCell>
												</Table.Row>
												{Object.keys(table2.sectionA.rows).map((rowKey) => {
													const row = table2.sectionA.rows[rowKey];
													return (
														<Table.Row>
															<Table.Cell>{row.sn}</Table.Cell>
															<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
															<Table.Cell>
																{Object.keys(row.inputs).map((input) => {
																	const currentInput = row.inputs[input];
																	const fieldName = currentInput.prefix
																		? `1_A_${row.sn}_${getNameFormat(currentInput.prefix)}_qty`
																		: `1_A_${row.sn}_qty`;
																	if (floorArray.getHasBlocks()) {
																		return <div></div>;
																	}
																	return (
																		<table>
																			<span>
																				{currentInput.prefix} {props.values[fieldName]} {currentInput.suffix}
																			</span>
																		</table>
																	);
																})}
															</Table.Cell>
															<Table.Cell>
																<span>{props.values[`1_A_${row.sn}_recommendation`]}</span>
															</Table.Cell>
														</Table.Row>
													);
												})}
												<Table.Row>
													<Table.HeaderCell colSpan={3}>{table2.sectionB.heading}</Table.HeaderCell>
												</Table.Row>
												{/* Section B */}
												{Object.keys(table2.sectionB.rows).map((rowKey) => {
													const row = table2.sectionB.rows[rowKey];
													// console.log('row sectionB', row);
													return (
														<Table.Row>
															<Table.Cell>{row.sn}</Table.Cell>
															<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
															<Table.Cell>
																{row.sn === '26' ? (
																	<table>
																		<Table.Row>
																			<Table.Cell>
																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{'Sill band and Lintel band'}{' '}
																				</span>
																				<Input labelPosition="right">
																					<span>{props.values['1_C_26_sill-band-and-lintel-band']}</span>
																				</Input>

																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{' '}
																					{'no. of'}{' '}
																				</span>
																				<Input labelPosition="right">
																					<span>{props.values['1_C_26_no-of']}</span>
																				</Input>

																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{' '}
																					{'mm'}{' '}
																				</span>
																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{'bars with'}{' '}
																				</span>
																				<Input labelPosition="right">
																					<span>{props.values['1_C_26_bars-with']}</span>
																				</Input>

																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{' '}
																					{'mm C-hook@'}{' '}
																				</span>
																				<Input labelPosition="right">
																					<span>{props.values['1_C_26_c-hook']}</span>
																				</Input>

																				<span
																					style={{
																						width: '150px',
																					}}
																				>
																					{'mm c-c'}
																				</span>
																			</Table.Cell>
																		</Table.Row>
																	</table>
																) : (
																	Object.keys(row.inputs).map((input) => {
																		const currentInput = row.inputs[input];
																		const fieldName = currentInput.prefix
																			? `1_B_${row.sn}_${getNameFormat(currentInput.prefix)}_qty`
																			: `1_B_${row.sn}_qty`;
																		return (
																			<table>
																				<span>
																					{currentInput.prefix} {props.values[fieldName]}{' '}
																					{currentInput.suffix}
																				</span>
																			</table>
																		);
																		// return (
																		//   <Table.Row>
																		//     <Table.Cell>
																		//       <Input labelPosition="right">
																		//         {currentInput.prefix && (
																		//           <Label basic>
																		//             {currentInput.prefix}
																		//           </Label>
																		//         )}
																		//         <input />
																		//         {currentInput.suffix && (
																		//           <Label basic>
																		//             {currentInput.suffix}
																		//           </Label>
																		//         )}
																		//       </Input>
																		//     </Table.Cell>
																		//   </Table.Row>
																		// );
																	})
																)}
															</Table.Cell>
															<Table.Cell>
																<span> {props.values[`1_B_${row.sn}_recommendation`]}</span>
															</Table.Cell>
														</Table.Row>
													);
												})}
												<Table.Row>
													<Table.HeaderCell colSpan={3}>{table2.sectionC.heading}</Table.HeaderCell>
												</Table.Row>
												{Object.keys(table2.sectionC.rows).map((rowKey) => {
													const row = table2.sectionC.rows[rowKey];
													return (
														<Table.Row>
															<Table.Cell>{row.sn}</Table.Cell>
															<Table.Cell>{`${row.name}: ${row.description}`}</Table.Cell>
															<Table.Cell>
																{row.inputs.length > 1 ? (
																	Object.keys(row.inputs).map((input) => {
																		const currentInput = row.inputs[input];
																		return (
																			<table>
																				<span>
																					{currentInput.prefix}
																					{props.values[`1_C_${row.sn}_qty`]}
																					{currentInput.suffix}
																				</span>
																			</table>
																		);
																	})
																) : (
																	<table>
																		<span>{props.values[`1_C_${row.sn}_qty`]}</span>
																	</table>
																)}
															</Table.Cell>
															<Table.Cell>
																<span>{props.values[`1_C_${row.sn}_recommendation`]}</span>
															</Table.Cell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>

										<h5 className="formFeild-mainLabel english-div">{table3.heading}</h5>
										<Table celled structured compact className="english-div">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>S.N.</Table.HeaderCell>
													<Table.HeaderCell width={6}>Provision</Table.HeaderCell>
													<Table.HeaderCell colspan="3">Area</Table.HeaderCell>
													<Table.HeaderCell>Recommendations</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{/* Section 1 */}
												<Table.Row>
													<Table.Cell>{table3.sections.section1.sn}</Table.Cell>
													<Table.Cell>{table3.sections.section1.rows.row1.name}</Table.Cell>
													<Table.Cell colSpan="3">
														<span>{props.values[`2_${table3.sections.section1.sn}_row1_qty`]}</span>
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section1.sn}_row1_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>
												{/* Section 2 */}
												<Table.Row>
													<Table.Cell>{table3.sections.section2.sn}</Table.Cell>
													<Table.Cell>{table3.sections.section2.rows.row1.name}</Table.Cell>
													<Table.Cell colSpan="3">
														<span>{props.values[`2_${table3.sections.section2.sn}_row1_qty`]}</span>
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section2.sn}_row1_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>
												{/* Section 3 */}
												<Table.Row>
													<Table.Cell>{table3.sections.section3.sn}</Table.Cell>
													<Table.Cell>{table3.sections.section3.rows.row1.name}</Table.Cell>
													<Table.Cell colSpan="3">
														<span>{props.values[`2_${table3.sections.section3.sn}_row1_qty`]}</span>
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section3.sn}_row1_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												{/* Section 4 */}

												<Table.Row>
													<Table.Cell rowSpan={floorCount.length + 1}>{table3.sections.section4.sn}</Table.Cell>

													<Table.Cell>
														<u>{table3.sections.section4.rows.row1.name}</u>
													</Table.Cell>

													<Table.Cell textAlign="center">
														<span>{table3.sections.section4.rows.row1.inputs[0]}</span>
													</Table.Cell>

													<Table.Cell textAlign="center">
														<span>{table3.sections.section4.rows.row1.inputs[1]}</span>
													</Table.Cell>

													<Table.Cell textAlign="center">
														<span>{table3.sections.section4.rows.row1.inputs[2]}</span>
													</Table.Cell>

													<Table.Cell />
												</Table.Row>
												{floorArray.getHeightRelevantFloors().map((currentRow) => {
													return (
														<Table.Row key={currentRow.floor}>
															<Table.Cell>{getFloorEnglish(currentRow.floor)}</Table.Cell>

															<Table.Cell>
																<span>{props.values[`2_4_floor${currentRow.floor}_wallheight`]}</span>
															</Table.Cell>
															<Table.Cell>
																<span>{props.values[`2_4_floor${currentRow.floor}_wallthickness`]}</span>
															</Table.Cell>
															<Table.Cell>
																<span>{props.values[`2_4_floor${currentRow.floor}_maximumlength`]}</span>
															</Table.Cell>

															<Table.Cell>
																<span>{props.values[`2_4_floor${currentRow.floor}_recommendation`]}</span>
															</Table.Cell>
														</Table.Row>
													);
												})}

												{/* left for print */}

												<Table.Row>
													<Table.Cell rowSpan="6">{table3.sections.section5.sn}</Table.Cell>
													<Table.Cell>
														<u>{table3.sections.section5.rows.row1.name}</u>
													</Table.Cell>
													<Table.Cell colSpan="3" />
													<Table.Cell />
												</Table.Row>

												<Table.Row>
													<Table.Cell>{table3.sections.section5.rows.row2.name}</Table.Cell>
													<Table.Cell colspan="3">
														<span>{props.values[`2_${table3.sections.section5.sn}_row2_qty`]}</span>
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section5.sn}_row2_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												<Table.Row>
													<Table.Cell>{table3.sections.section5.rows.row3.name}</Table.Cell>
													<Table.Cell colspan="3">
														{table3.sections.section5.rows.row3.inputs.map((input) => (
															// <Table.Cell>
															<div className="ui radio checkbox">
																{/* <span>{props.values[`2_${table3.sections.section5.sn}_row3_qty`]}
                                   </span> */}
																<span>{props.values[`2_${table3.sections.section5.sn}_row3_qty`]}</span>
																{/* <RadioLabel value={input} /> */}
															</div>
															// </Table.Cell>
														))}
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section5.sn}_row3_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												<Table.Row>
													<Table.Cell>{table3.sections.section5.rows.row4.name}</Table.Cell>
													<Table.Cell colspan="3">
														{table3.sections.section5.rows.row4.inputs.map((input) => (
															// <Table.Cell>
															<div className="ui radio checkbox">
																<span>{props.values[`2_${table3.sections.section5.sn}_row4_qty`]}</span>
																<RadioLabel value={input} />
															</div>
															// </Table.Cell>
														))}
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section5.sn}_row4_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												<Table.Row>
													<Table.Cell>{table3.sections.section5.rows.row5.name}</Table.Cell>
													<Table.Cell colspan="3">
														{table3.sections.section5.rows.row5.inputs.map((input) => (
															// <Table.Cell>
															<div className="ui radio checkbox">
																<span>{props.values[`2_${table3.sections.section5.sn}_row5_qty`]}</span>
																<RadioLabel value={input} />
															</div>
															// </Table.Cell>
														))}
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section5.sn}_row5_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												<Table.Row>
													<Table.Cell>{table3.sections.section5.rows.row6.name}</Table.Cell>
													<Table.Cell colspan="3">
														{table3.sections.section5.rows.row6.inputs.map((input) => (
															// <Table.Cell>
															<div className="ui radio checkbox">
																<span>{props.values[`2_${table3.sections.section5.sn}_row6_qty`]}</span>
																<RadioLabel value={input} />
															</div>
															// </Table.Cell>
														))}
													</Table.Cell>
													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section5.sn}_row6_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												{/* Section 6 */}
												<Table.Row>
													<Table.Cell>{table3.sections.section6.sn}</Table.Cell>
													<Table.Cell>{table3.sections.section6.rows.row1.name}</Table.Cell>
													<Table.Cell colspan="3">
														{/* {table3.sections.section6.rows.row1.inputs.map(
                            input => (
                              // <Table.Cell>
                              <div className='ui radio checkbox'> */}
														{/* <input
                                  type='radio'
                                  name={`2_${table3.sections.section6.sn}_row1_qty`}
                                  value={input}
                                  id={`2_${table3.sections.section6.sn}_row1_qty`}
                                  checked={
                                    this.state[
                                      `2_${table3.sections.section6.sn}_row1_qty`
                                    ] === input
                                  }
                                  onChange={this.handleRadioButtonClicked}
                                /> */}
														<span>{props.values[`2_${table3.sections.section6.sn}_row1_qty`]}</span>
														{/*                                 
                                  <RadioLabel value={input} />
                              </div>
                              // </Table.Cell>
                            )
                          )} */}
													</Table.Cell>

													<Table.Cell>
														<span>{props.values[`2_${table3.sections.section6.sn}_row1_recommendation`]}</span>
													</Table.Cell>
												</Table.Row>

												{/* Section 7 */}
												<Table.Row>
													<Table.Cell rowSpan={floorCount.length + 1}>{table3.sections.section7.sn}</Table.Cell>
													<Table.Cell>
														<u>{table3.sections.section7.rows.row1.name}</u>
													</Table.Cell>
													<Table.Cell colspan="3" />
													<Table.Cell />
												</Table.Row>
												{floorArray.getHeightRelevantFloors().map((currentRow) => {
													return (
														<Table.Row key={currentRow.floor}>
															<Table.Cell>{getFloorEnglish(currentRow.floor)}</Table.Cell>
															<Table.Cell colspan="3">
																<span>{props.values[`2_7_floor${currentRow.floor}_qty`]}</span>
															</Table.Cell>
															<Table.Cell>
																<span>{props.values[`2_7_floor${currentRow.floor}_recommendation`]}</span>
															</Table.Cell>
														</Table.Row>
													);
												})}
											</Table.Body>
										</Table>

										<h4 align="center" className="formFeild-mainLabel english-div">
											{table4.heading}
										</h4>

										<h5 className="formFeild-mainLabel english-div">{table4.subheading}</h5>
										<Table celled collapsing compact className="english-div">
											<Table.Body>
												{table4.form.map((row) => (
													<Table.Row>
														<Table.Cell>
															<Header as="h5">{row.display}</Header>
														</Table.Cell>
														<Table.Cell>
															{/* <Input className="structureC"
                              name={table4.form[row]}
                              onChange={props.handleChange}
                              value={props.values[table4.form[row]]}
                            /> */}
															<Input
																className="structureC"
																name={row.name}
																onChange={props.handleChange}
																value={props.values[row.name]}
															/>
														</Table.Cell>
													</Table.Row>
												))}
											</Table.Body>
										</Table>
										{/* </FormWrapper> */}
									</div>
									<br />
									{/* <SaveButton
                  errors={props.errors}
                  formUrl={this.props.parentProps.location.pathname}
                  hasSavePermission={this.props.hasSavePermission}
                  prevData={getJsonData(this.props.prevData)}
                  handleSubmit={props.handleSubmit}
                /> */}
									{/* {this.props.hasSavePermission && (
                    <Button
                      primary
                      disabled={erStatus === 'A'}
                      onClick={props.handleSubmit}
                    >
                      Save
                    </Button>
                  )
                    // : (
                    //   <ApprovalGroup
                    //     history={this.props.history}
                    //     url={`${api.structureDesignC}${this.props.permitData.applicantNo}`}
                    //   />
                    // )
                  } */}
									{/* <ApprovalStatusDisplay
                    erStatus={erStatus}
                    serStatus={serStatus}
                    comments={this.props.comment}
                  /> */}
								</Form>
							);
						}}
					/>
					{/* <HistorySidebar history={this.props.formHistory} /> */}
					{/* <h2>Structure Design Class C Form</h2> */}
					{/* </HistorySidebar> */}
				</>
			);
			// } else {
			//   return (
			//     <FallbackComponent
			//       loading={this.props.loading}
			//       errors={this.props.errors}
			//     />
			//   );
			// }
		} else {
			return <FallbackComponent loading={this.props.loading} errors={this.props.errors} />;
		}
	}
}

const mapDispatchToProps = { getFormDataWithPermit, postFormDataByUrl, setPrint: (res) => setPrint(res) };
const mapStateToProps = (state) => {
	return {
		prevData: state.root.formData.prevData,
		permitData: state.root.formData.permitData,
		comment: state.root.formData.comment,
		formHistory: state.root.formData.history,
		success: state.root.formData.success,
		errors: state.root.ui.errors,
		loading: state.root.ui.loading,
	};
};

// const StructureDesignClassCPrintView = parentProps => (
//   <JSONDataMultipleGetFormContainer
//     api={[{ api: api.structureDesignC, objName: 'structC', form: true }]}
//     prepareData={data => data}
//     onBeforeGetContent={{
//       param1: ['getElementsByTagName', 'input', 'value'],
//       param2: ['getElementsByClassName', 'ui label', 'innerText']
//     }}
//     parentProps={parentProps}
//     useInnerRef={true}
//     render={props => (
//       <StructureDesignClassCPrintView
//         {...props}
//         parentProps={parentProps}
//       />
//     )}
//   />
// );

// export default StructureDesignClassCPrintView;

export default connect(mapStateToProps, mapDispatchToProps)(StructureDesignClassCPrintView);
