import React from 'react';
import { DashedLangInput, DashedNormalInputIm, DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { ImageDisplayInline } from '../../../shared/file/FileView';
import { DashedLangDateField } from '../../../shared/DateField';

export const ManjuriPatraForm = ({ setFieldValue, values, errors, designerData }) => {
	return (
		<div>
			<div>
				{designerData.name}: <DashedLangInput name="dName" setFieldValue={setFieldValue} value={values.dName} error={errors.dName} />
			</div>
			<div>
				{designerData.qualification}:{' '}
				<DashedLangInput name="dDesignation" setFieldValue={setFieldValue} value={values.dDesignation} error={errors.dDesignation} />
			</div>
			<div>
				{designerData.consultancy}: <DashedNormalInputIm name="dConsultancyName" />{' '}
				<ImageDisplayInline src={values.dStamp} height={60} alt="user signature" />
			</div>
			<div>
				{designerData.nagarpalikaRegistration}:{' '}
				<DashedLangInputWithSlash
					name="dMunicipalRegNo"
					setFieldValue={setFieldValue}
					value={values['dMunicipalRegNo']}
					error={errors.dMunicipalRegNo}
				/>
			</div>
			<div>
				{designerData.engineeringCouncilNo}: <DashedNormalInputIm name="dEngineeringCouncil" />
			</div>
			<div>
				{designerData.address}: <DashedLangInput name="dAdd" setFieldValue={setFieldValue} value={values.dAdd} error={errors.dAdd} />
			</div>{' '}
			<div>
				{designerData.phone}: <DashedLangInput name="dMobile" setFieldValue={setFieldValue} value={values.dMobile} error={errors.dMobile} />
			</div>
			<div>
				<ImageDisplayInline label={`${designerData.signature}: `} src={values.dSignature} height={60} alt="user signature" />
			</div>
			<div>
				<DashedLangDateField
					name="dDate"
					setFieldValue={setFieldValue}
					value={values.dDate}
					error={errors.dDate}
					label={`${designerData.date}: `}
				/>
			</div>
		</div>
	);
};
