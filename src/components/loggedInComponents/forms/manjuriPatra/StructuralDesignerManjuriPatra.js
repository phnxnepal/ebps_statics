import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { isEmpty } from '../../../../utils/functionUtils';
import { checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SectionHeader } from '../../../uiComponents/Headers';
import { structuralManjuriData } from '../../../../utils/data/manjuriPatra';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { ManjuriPatraForm } from './ManjuriPatraFrom';
import { ManjuriPatraSchema } from '../../formValidationSchemas/manjuriPatraSchema';

const bodyData = structuralManjuriData.body;
const designerData = structuralManjuriData.designer;

class StructuralDesignerManjuriPatraComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData, staticFiles } = this.props;
		let initialValues = {};

		if (!isEmpty(checkError(this.props.prevData))) {
			// designerData = { ...prevData.enterBy, enterDateBs: prevData.enterDateBs };
			initialValues = { ...prevData };
		} else {
			initialValues = {
				/**
				 * @info this is supposed to be archtecture's information so it is empty.
				 */
				dName: '',
				dDesignation: '',
				dConsultancyName: '',
				dEngineeringCouncil: '',
				dMunicipalRegNo: '',
				dMobile: '',
				dAdd: '',
				dDate: getCurrentDate(true),
			};
		}

		initialValues.dSignature = staticFiles.engineerSignature || '';
		initialValues.dStamp = staticFiles.engineerStamp || '';
		this.state = {
			initialValues,
		};
	}

	render() {
		const { initialValues } = this.state;
		const { permitData, userData } = this.props;
		return (
			<div>
				<Formik
					initialValues={initialValues}
					validationSchema={ManjuriPatraSchema}
					onSubmit={async (values, actions) => {
						// console.log('Form values', values);
						actions.setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await this.props.postAction(`${api.structuralDesignerManjuriPatra}${permitData.applicantNo}`, values);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ setFieldValue, isSubmitting, values, errors, validateForm, handleSubmit }) => {
						return (
							<Form loading={isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef}>
									<SectionHeader>
										<h3 className="bottom-margin">{structuralManjuriData.title}</h3>
									</SectionHeader>
									<br />
									<LetterSalutation
										lines={[
											`${structuralManjuriData.shree} ${userData.organization.name}`,
											`${userData.organization.officeName}`,
											`${userData.organization.address}`,
										]}
									/>
									<br />
									<SectionHeader>
										<h4 className="underline bottom-margin">{structuralManjuriData.subject}</h4>
									</SectionHeader>
									<div>
										{bodyData.address} {permitData.applicantAddress} {bodyData.ward} {permitData.newWardNo} {bodyData.kittaNo}{' '}
										{permitData.kittaNo} {bodyData.area} {permitData.landArea} {permitData.landAreaType} {bodyData.gharDhani}{' '}
										{permitData.applicantName} {bodyData.end}
									</div>
									<br />
									<ManjuriPatraForm setFieldValue={setFieldValue} values={values} errors={errors} designerData={designerData} />
								</div>
								<br />
								<SaveButtonValidation
									errors={errors}
									formUrl={this.props.formUrl}
									isSaveDisabled={this.props.isSaveDisabled}
									hasDeletePermission={this.props.hasDeletePermission}
									hasSavePermission={this.props.hasSavePermission}
									prevData={checkError(this.props.prevData)}
									handleSubmit={handleSubmit}
									validateForm={validateForm}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const StructuralDesignerManjuriPatra = (parentProps) => (
	<FormContainerV2
		api={[{ api: api.structuralDesignerManjuriPatra, objName: 'manjuriPatra', form: true }]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
		}}
		/**
		 * @info using this to fetch files as this has no files
		 */
		fetchFiles={true}
		render={(props) => <StructuralDesignerManjuriPatraComponent {...props} parentProps={parentProps} />}
	/>
);

export default StructuralDesignerManjuriPatra;
