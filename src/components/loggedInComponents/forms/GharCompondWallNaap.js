import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import { GharCompondWall } from '../../../utils/data/GharCompondWallNaapData';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
const ghar = GharCompondWall.GharCompondWall_data;

class GharCompondWallNaapComponent extends Component {
	render() {
		const udata = this.props.userData;

		let initVal = {};

		initVal = getJsonData(this.props.prevData);

		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div>
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.gharCompoundWallNaap, values, true);

							window.scroll(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ handleSubmit, isSubmitting, handleChange, values, errors, setFieldValue, validateForm }) => {
						return (
							<Form loading={isSubmitting} className="NJ-right">
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef}>
									<div style={{ textAlign: 'justify' }} ref={this.printRef} className="build-gaps">
										<div>
											<h2>
												<span>{ghar.heading_1.heading_1_1}</span>
											</h2>
											<Grid>
												{Object.values(ghar.floors).map(row => (
													<Grid.Row columns={3}>
														<Grid.Column>{`${row}`}</Grid.Column>
														<Grid.Column>{`${ghar.data_1.data_1_2}`}</Grid.Column>
														<Grid.Column>{`${ghar.data_1.data_1_3}`}</Grid.Column>
													</Grid.Row>
												))}
											</Grid>
										</div>
										<br />
										<div>
											<h2 style={{ textAlign: 'center' }}>
												<span>{ghar.heading_2.heading_2_1}</span>
											</h2>
											<span>{ghar.data_2.data_2_1}</span>
											<DashedLangInput
												name="kittaNo"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.kittoNo}
												error={errors.kittoNo}
											/>
											<span>{ghar.data_2.data_2_2}</span>
											<DashedLangInput
												name="JiWi"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.JiWi}
												error={errors.JiWi}
											/>
											<span>{ghar.data_2.data_2_3}</span>
											<DashedLangInput
												name="Pakki"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Pakki}
												error={errors.Pakki}
											/>
											<span>{ghar.data_2.data_2_4}</span> <span>{udata.organization.name}</span>
											<span>{ghar.data_2.data_2_4_1}</span>
											<DashedLangInput
												name="wardNo1"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.wardNo1}
												error={errors.wardNo1}
											/>
											<span>{ghar.data_2.data_2_5}</span>
											<DashedLangInput
												name="Basne"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Basne}
												error={errors.Basne}
											/>
											<span>{ghar.data_2.data_2_6}</span>
											<DashedLangInput
												name="Shree"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Shree}
												error={errors.Shree}
											/>
											<span>{ghar.data_2.data_2_7}</span>
										</div>
										<br />
										<div>
											{[0, 1, 2, 3, 4, 5].map(() => (
												<div>
													<span style={{ marginLeft: '50px' }}>{udata.organization.name}</span>
													<span>{ghar.data_3.data_3_2}</span>
													<DashedLangInput
														name="wardNo"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.wardNo}
														error={errors.wardNo}
													/>
													<span>{ghar.data_3.data_3_3}</span>
													<DashedLangInput
														name="Barsa1"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.Barsa1}
														error={errors.Barsa1}
													/>
													<span>{ghar.data_3.data_3_4}</span>
													<DashedLangInput
														name="KaShree1"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.KaShree1}
														error={errors.KaShree1}
													/>
													<br />
												</div>
											))}
										</div>
										<br />
										<div>
											<h3 style={{ textAlign: 'center' }}>
												<span>{ghar.heading_3.heading_3_1}</span>
											</h3>
											<span>{udata.organization.name}</span>
											<span>{ghar.data_3.data_3_2}</span>
											<DashedLangInput
												name="wardNo"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.wardNo}
												error={errors.wardNo}
											/>
											<span>{ghar.data_3.data_3_3}</span>
											<DashedLangInput
												name="BasneBarsa"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.BasneBarsa}
												error={errors.BasneBarsa}
											/>
											<span>{ghar.data_3.data_3_4}</span>
											<DashedLangInput
												name="AweydakShree"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.AweydakShree}
												error={errors.AweydakShree}
											/>
										</div>
										<br />
										<div>
											<h3 style={{ textAlign: 'center' }}>
												<span>{ghar.heading_4.heading_4_1}</span>
											</h3>
											<span>{ghar.data_4.data_4_1}</span>
											<span>{udata.organization.name}</span>
											<span>{ghar.data_4.data_4_3}</span>
											<DashedLangInput
												name="RohawarWada"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.RohawarWada}
												error={errors.RohawarWada}
											/>
											<span>{ghar.data_4.data_4_4}</span>
											<DashedLangInput
												name="RohawarBarsa"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.RohawarBarsa}
												error={errors.RohawarBarsa}
											/>
											<span>{ghar.data_4.data_4_5}</span>
											<DashedLangInput
												name="RohawarShree"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.RohawarShree}
												error={errors.RohawarShree}
											/>
										</div>
										<br />
										<div>
											<h3 style={{ textAlign: 'center' }}>
												<span>{ghar.heading_5.heading_5_1}</span>
											</h3>
											<span>{udata.organization.name}</span>
											<span>{ghar.data_5.data_5_2}</span>
											<br />
											<span>{ghar.data_5.data_5_3}</span>
											<DashedLangInput
												name="Sambat"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Sambat}
												error={errors.Sambat}
											/>
											<span>{ghar.data_5.data_5_4}</span>
											<DashedLangInput
												name="Saal"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Saal}
												error={errors.Saal}
											/>
											<span>{ghar.data_5.data_5_5}</span>
											<DashedLangInput
												name="Mahina"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Mahina}
												error={errors.Mahina}
											/>
											<span>{ghar.data_5.data_5_6}</span>
											<DashedLangInput
												name="Gatey"
												className="dashedForm-control"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.Gatey}
												error={errors.Gatey}
											/>
											<span>{ghar.data_5.data_5_7}</span>
										</div>
									</div>
									<br />
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const GharCompoundWallNaap = parentProps => (
	<FormContainerV2
		api={[{ api: api.gharCompoundWallNaap, objName: 'gharcompound', form: true }]}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		render={props => <GharCompondWallNaapComponent {...props} parentProps={parentProps} />}
	/>
);

export default GharCompoundWallNaap;
