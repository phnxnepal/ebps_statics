import React from 'react';
import { Step, Button, Grid, Loader, Dimmer, Select, Label, Icon, Modal, Message } from 'semantic-ui-react';
import { stepData } from '../../../utils/data/forwardToNextData';
import { connect } from 'react-redux';
import { postForwardToNext, getFormDataWithPermit } from '../../../store/actions/forwardToNextAction';
import { getLocalDataByKey } from '../../../store/actions/formActions';
import { getForwardData, clearSuccessState } from '../../../store/actions/AdminAction';
import { CompactError } from '../../shared/ErrorDisplay';
import { showToast, currentUrlObj, getUserRole, getUserTypeValueNepali, getBuildPermitObj, isEmpty } from '../../../utils/functionUtils';
import { getGroupName, getJsonData } from '../../../utils/dataUtils';
import FallbackComponent from '../../shared/FallbackComponent';
import { getLocalStorage } from '../../../utils/secureLS';
import { TASK_IDS, APPLICATION_FORWARD_DATA, REJECTED_STATUS } from '../../../utils/constants';
import { taskList } from '../../../utils/data/genericData';
import { getCurrentUrlObj, LocalAPIArray, LocalAPI } from '../../../utils/urlUtils';
import Helmet from 'react-helmet';
import { CompactInfoMessage } from '../../shared/formComponents/CompactInfoMessage';
import api from '../../../utils/api';

const data = stepData;

function getSkipFormAmountAPI(skipForm) {
	if (String(skipForm) === '48') {
		return api.sansodhanSuperTippaniAdesh;
	} else if (String(skipForm) === '47') {
		return api.sansodhankoTippaniAdesh;
	} else if (String(skipForm) === '46') {
		return api.sansodhanTippani;
	} else return null;
}

const getOpts = (forwardTo, userType, groupId) => {
	let option = [];
	try {
		forwardTo
			.filter((opt) => opt.forwardBy === userType && opt.formGroup === parseInt(groupId))
			.forEach((o) =>
				option.push({
					key: o.id,
					value: o.forwardTo,
					skipParameter: o.skipParameter,
					text: getUserTypeValueNepali(o.forwardTo),
				})
			);
	} catch {
		option = [];
	}
	return option;
};

export class ForwardToNextForm extends React.Component {
	constructor(props) {
		super(props);
		let groupName;
		let groupId;
		try {
			const groupObj = getGroupName();
			const url = this.props.location.pathname;
			const urlSplit = url.split('/');
			groupId = props.modal ? currentUrlObj(url).groupId : urlSplit[urlSplit.length - 1];
			if (props.modal) {
				groupName = groupObj.find((group) => parseInt(group.id) === parseInt(currentUrlObj(url).groupId));
			} else {
				groupName = groupObj.find((group) => parseInt(group.id) === parseInt(urlSplit[urlSplit.length - 1]));
			}
		} catch {
			groupName = data.step1.title;
			groupId = 0;
		}

		const taskIds = getLocalStorage(TASK_IDS);
		const permitData = getBuildPermitObj();
		let isVisible = taskIds && JSON.parse(taskIds).some((each) => each === permitData.applicantNo);

		const userType = getUserRole();
		// const options = getOptions(userType);
		// console.log('group name', groupName);
		this.state = {
			groupName: groupName ? groupName.name : data.step1.title,
			groupId,
			userType: userType,
			// options: getOptions(userType),
			// sendTo: options.length > 0 ? options[0].value : '',
			options: '',
			sendTo: '',
			isVisible: isVisible,
			hasBeenRejected: false,
			isConfirmationOpen: false,
			needsForcefullForward: false,
			autoSaveForm: null,
		};
	}

	componentDidMount() {
		window.scroll(0, 0);
		this.props.clearSuccessState();

		this.props.getLocalDataByKey(
			new LocalAPIArray([new LocalAPI(APPLICATION_FORWARD_DATA, 'forwardMaster'), new LocalAPI(REJECTED_STATUS, 'rejectedStatus')])
		);
		const url = this.props.location.pathname;
		const urlSplit = url.split('/');
		const groupId = this.props.modal ? currentUrlObj(url).groupId : urlSplit[urlSplit.length - 1];
		this.setState({ groupId });
	}

	componentDidUpdate(prevProps) {
		if (prevProps.location.pathname !== this.props.location.pathname) {
			this.props.getLocalDataByKey(
				new LocalAPIArray([new LocalAPI(APPLICATION_FORWARD_DATA, 'forwardMaster'), new LocalAPI(REJECTED_STATUS, 'rejectedStatus')])
			);
			const url = this.props.location.pathname;
			const urlSplit = url.split('/');
			const groupId = this.props.modal ? currentUrlObj(url).groupId : urlSplit[urlSplit.length - 1];
			this.setState({ groupId });
		}

		if (prevProps.forwardTo !== this.props.forwardTo) {
			const options = getOpts(this.props.forwardTo, this.state.userType, this.state.groupId);
			const rowWithSkip = options.find((row) => row.skipParameter);
			if (rowWithSkip) {
				const amountApi = getSkipFormAmountAPI(rowWithSkip.skipParameter);
				if (amountApi) {
					this.props.getFormDataWithPermit(amountApi, (data) => getJsonData(data));
				}
			}
			this.setState({
				options: options,
				sendTo: options && options.length > 0 && options[0].value,
			});
		}

		if (prevProps.amountCheckForm !== this.props.amountCheckForm) {
			let amount = 0;
			if (!isEmpty(this.props.amountCheckForm)) {
				amount = this.props.amountCheckForm.totalAmt || 0;
			}
			if (amount !== null && amount !== undefined && +amount <= 0) {
				this.setState((prevState) => ({
					options: prevState.options.filter((row) => !row.skipParameter),
					autoSaveForm: prevState.options.find((row) => row.skipParameter).skipParameter,
				}));
			}
		}

		if (
			this.state.options !== '' &&
			this.props.rejectedStatus !== prevProps.rejectedStatus &&
			this.props.rejectedStatus &&
			this.props.rejectedStatus.rejectedBy
		) {
			this.setState({
				options: [
					{
						key: this.props.rejectedStatus.rejectedBy,
						value: this.props.rejectedStatus.rejectedBy,
						text: getUserTypeValueNepali(this.props.rejectedStatus.rejectedBy),
					},
				],
				sendTo: this.props.rejectedStatus.rejectedBy,
				hasBeenRejected: true,
			});
		}

		if (prevProps.errors !== this.props.errors) {
			const needsForcefullForward = this.props.errors && this.props.errors.message === 'This group is already forwarded';
			this.setState({ needsForcefullForward });
		}
	}

	handleModalOpen = () => {
		this.setState({ isConfirmationOpen: true });
	};

	handleModalClose = () => {
		this.setState({ isConfirmationOpen: false });
	};

	handlePost = async () => {
		const url = this.props.location.pathname;
		const urlSplit = url.split('/');

		let urlObj;

		if (this.props.modal) {
			urlObj = getCurrentUrlObj(url);
		} else {
			// urlObj = currentUrlObj(urlSplit.slice(0, urlSplit.length - 1).join('/'));
			urlObj = { groupId: urlSplit[urlSplit.length - 1] };
		}

		let groupId = '',
			sendTo = '';
		try {
			groupId = urlObj.groupId;
			sendTo = this.state.sendTo;
		} catch (err) {
			groupId = '';
			sendTo = '';
		}
		// console.log('gthis.', urlSplit[urlSplit.length - 1], groupId);
		try {
			await this.props.postForwardToNext(groupId, sendTo, this.state.needsForcefullForward, this.state.autoSaveForm);

			if (this.props.success && this.props.success.success) {
				showToast(this.props.success.data.message || 'Form successfully forwarded.');

				// const url = this.props.location.pathname;
				// const urlSplit = url.split('/');

				this.props.history.push(
					'/user/task-list'
					// getNextGroupUrl(
					// 	urlSplit.slice(0, urlSplit.length - 1).join('/'),
					// 	urlSplit[urlSplit.length - 1]
					// )
				);
			}
		} catch (err) {
			console.log('Error in forward to next form', err);
		}
	};

	handleSendToSet = (value) => {
		this.setState({ sendTo: value });
	};

	render() {
		const { userType, needsForcefullForward, hasBeenRejected } = this.state;
		const { forwardTo } = this.props;
		if (!Array.isArray(forwardTo)) {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		} else {
			// const options = getOpts(forwardTo, userType, groupId);
			return (
				<>
					{!this.props.modal && (
						<Helmet>
							<title>Forward To</title>
						</Helmet>
					)}
					<Dimmer active={this.props.loading} inverted page>
						<Loader active={this.props.loading} inline="centered" />
					</Dimmer>
					{this.state.options.length < 1 ? (
						<Message floating negative className="renew" icon="warning circle" header={data.error.title} content={data.error.content} />
					) : (
						<Grid centered rows={3} padded>
							<Grid.Row centered>
								<Step.Group ordered>
									<Step completed>
										<Step.Content>
											<Step.Title>{this.state.groupName}</Step.Title>
											{/* <Step.Description>{data.step1.description}</Step.Description> */}
										</Step.Content>
									</Step>

									<Step active>
										<Step.Content>
											<Step.Title>{data.step2.title}</Step.Title>
										</Step.Content>
									</Step>
								</Step.Group>
							</Grid.Row>
							{hasBeenRejected && (
								<Grid.Row>
									<CompactInfoMessage content={data.rejected} />
								</Grid.Row>
							)}
							<Grid.Row centered>
								{userType && (
									<>
										<Label style={{ lineHeight: 2 }} basic color="blue" size="large">
											{data.sendToLabel}
										</Label>
										<Select
											options={this.state.options}
											value={this.state.sendTo}
											name="sendTo"
											onChange={(e, { value }) => this.handleSendToSet(value)}
										/>
									</>
								)}
								<Button primary onClick={this.handleModalOpen} disabled={!this.state.isVisible}>
									Forward to Next
								</Button>
							</Grid.Row>
							<Grid.Row centered>
								{!this.state.isVisible && (
									<Label basic color="blue" size="large">
										<Icon name="info" />
										{taskList.taskListError}
									</Label>
								)}
							</Grid.Row>
							{this.props.errors && (
								<Grid.Row centered>
									<CompactError message={this.props.errors.message} />
								</Grid.Row>
							)}
							{needsForcefullForward && (
								<Grid.Row centered>
									<Button negative onClick={this.handleModalOpen}>
										Forward Forcefully
									</Button>
								</Grid.Row>
							)}
						</Grid>
					)}
					<Modal key="forward-confirmation" closeIcon open={this.state.isConfirmationOpen} onClose={this.handleModalClose}>
						<Modal.Header>{data.confirmation.title}</Modal.Header>
						<Modal.Content>
							<h3>
								{needsForcefullForward ? data.confirmation.contentForcefully1 : data.confirmation.content1}
								{this.state.sendTo === '' ? getUserTypeValueNepali('B') : getUserTypeValueNepali(this.state.sendTo)}
								{needsForcefullForward ? data.confirmation.contentForcefully2 : data.confirmation.content2}
							</h3>
						</Modal.Content>
						<Modal.Actions>
							<Button negative onClick={this.handleModalClose}>
								{data.confirmation.close}
							</Button>
							<Button
								positive
								onClick={() => {
									this.handleModalClose();
									this.handlePost();
								}}
							>
								{data.confirmation.save}
							</Button>
						</Modal.Actions>
					</Modal>
				</>
			);
		}
	}
}

const mapStateToProps = (state) => ({
	forwardTo: state.root.formData.forwardMaster,
	rejectedStatus: state.root.formData.rejectedStatus,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
	amountCheckForm: state.root.formData.amountData,
});

const mapDispatchToProps = { postForwardToNext, getLocalDataByKey, getForwardData, getFormDataWithPermit, clearSuccessState };

export default connect(mapStateToProps, mapDispatchToProps)(ForwardToNextForm);
