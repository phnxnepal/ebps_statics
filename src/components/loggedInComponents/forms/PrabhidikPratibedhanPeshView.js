import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select, Dropdown } from 'semantic-ui-react';
import merge from 'lodash/merge';
// import max from 'lodash/max';
import { prabidhikpratibedhan, mapTechnical } from '../../../utils/data/mockLangFile';
import { isEmpty } from '../../../utils/functionUtils';
import api from '../../../utils/api';
import * as Yup from 'yup';
import { getJsonData, prepareMultiInitialValues, surroundingMappingFlat, handleSuccess } from '../../../utils/dataUtils';
import { getMessage } from '../../shared/getMessage';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { DashedLangDateField } from './../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty, isNumberEmpty } from '../../../utils/stringUtils';
import { DashedLangInput, DashedNormalInput } from '../../shared/DashedFormInput';
import {
	validateString,
	validateMinDistanceField,
	validateNullableNepaliDate,
	validateNumber,
	validateNullableNormalNumber,
	validateNullableZeroNumber,
} from '../../../utils/validationUtils';
import { checkError } from './../../../utils/dataUtils';
import { DashedMultiUnitLengthInput, DashedUnitInput, DashedAreaUnitInput } from '../../shared/EbpsUnitLabelValue';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../utils/enums/constructionType';
import { ConstructionTypeRadio } from './mapPermitComponents/ConstructionTypeRadio';
import { RadioInput } from '../../shared/formComponents/RadioInput';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { PrabidhikFloorDetails } from './prabidhikComponents/PrabidhikFloorDetails';
import { getSaveByUserDetails } from '../../../utils/formUtils';
import { DetailedSignature } from './formComponents/FooterSignature';
import { SansodhanCheckbox } from './formComponents/SansodhanCheckbox';
import { PrintParams, PrintIdentifiers } from '../../../utils/printUtils';

const pppv_data = prabidhikpratibedhan.prabidhikpratibedhan_data;
const messageId = 'prabidhikpratibedhan.prabidhikpratibedhan_data';
// const permitLang = buildingPermitApplicationForm.permitApplicationFormView;
const mapTechLang = mapTechnical.mapTechnicalDescription;
// const areaUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];
const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

const field_9 = [
	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
];
const field_10 = [
	{ key: 1, value: 'मिल्ने', text: 'मिल्ने' },
	{ key: 2, value: 'नमिल्ने', text: 'नमिल्ने' },
];
// let nadiOptions = [];

const strData = ['fieldNine', 'fieldTen', 'roofLen', 'elecVolt'];

const nadiData = ['publicPropertyDistance'];

const numData = ['namedMapdanda', 'namedAdhikar', 'requiredDistance', 'isHighTensionLineDistance'];

const formDataSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});

const numberDataSchema = numData.map((row) => {
	return {
		[row]: validateNumber,
	};
});

class PrabhidikPratibedhanPeshViewComponent extends Component {
	constructor(props) {
		super(props);

		const { permitData, otherData, prevData, enterByUser, userData } = this.props;

		const floorArray = new FloorBlockArray(permitData.floor);

		const topFloor = floorArray.getTopFloor();
		const groundFloor = floorArray.getFloorByFloor(1);
		const blocks = floorArray.getBlocks();

		// debugger;

		// Other_SEt_BacK
		const otherSetBackMaster = this.props.otherData.otherSetBack;

		let nadiOptions = [{ value: mapTechLang.mapTech_23.defaultNadiOption, text: mapTechLang.mapTech_23.defaultNadiOption }];
		otherSetBackMaster.map(
			(row) =>
				row.status === 'Y' &&
				nadiOptions.push({
					value: row.placeName,
					text: row.placeName,
				})
		);
		let initialValues = {};
		// initialValues.topFloorLen = []
		// initialValues.topFloorLen = permitData.floor && permitData.floor.length > 0 ? calculateHighestFloor(permitData.floor, 'length').length : 0;
		// initialValues.topFloorBreadth = permitData.floor && permitData.floor.length > 0 ? calculateHighestFloor(permitData.floor, 'width').width : 0;
		// initialValues.topFloorHeight = permitData.floor && permitData.floor.length > 0 ? calculateHighestFloor(permitData.floor, 'height').height : 0;
		// initialValues.topFloorArea = permitData.floor && permitData.floor.length > 0 ? calculateHighestFloor(permitData.floor, 'area').area : 0;
		initialValues.sadakMapUnit = 'METRE';
		initialValues.sadakAreaUnit = 'METRE';
		initialValues.floorUnit = 'METRE';
		initialValues.sadakAdhikarUnit = 'METRE';
		initialValues.highTensionLineUnit = 'METRE';
		initialValues.publicPropertyUnit = 'METRE';
		initialValues.plinthDetailsUnit = 'SQUARE METRE';
		initialValues.sanso = pppv_data.sansodhan_check[0];
		const mapTech = getJsonData(otherData.mapTech);
		const RajaswoData = getJsonData(otherData.RajaswoData);

		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };
		// serInfo.subName = enterByUser.name;
		// serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: mapTech,
				reqFields: [
					'plinthDetails',
					'plinthDetailsUnit',
					'roof',
					'roofOther',
					'publicProperty',
					'publicPropertyUnit',
					'publicPropertyName',
					'publicPropertyDistance',
					'publicPropertyRequiredDistance',
					// 'sadakAdhikarUnit',
					'floorUnit',
				],
			},
			{
				obj: RajaswoData,
				reqFields: ['namedMapdanda', 'namedAdhikar', 'requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: getSaveByUserDetails(enterByUser, userData),
				reqFields: [],
			},
			{
				obj: {
					constructionType: permitData.constructionType,
					isHighTensionLineDistance: permitData.isHighTensionLineDistance,
					highTensionLineUnit: permitData.highTensionLineUnit,
					topFloor: floorArray.getInitialValue(['length', 'width', 'height', 'area'], '', topFloor),
					floor: floorArray.getInitialValue(['length', 'width', 'height', 'area'], '', groundFloor),
				},
				reqFields: [],
			},
			{ obj: getJsonData(otherData.SurjaminDate), reqFields: ['surjaminDate'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		initialValues1.floorUnit = initialValues1.floorUnit && initialValues1.floorUnit.replace('SQUARE', '').trim();
		initialValues1.constructionType = getConstructionTypeValue(initialValues1.constructionType);
		// console.log('trimed floorunit', initialValues1.floorUnit);
		const initVal = merge(initialValues, initialValues1);

		// let prabhidik_data = {};
		let prabhidik_option_data = {};
		// let initVal = {};

		// if (this.props.prevData) {
		// 	prabhidik_data.constructionType = getConstructionTypeValue(this.props.prevData.constructionType);
		// }

		if (this.props.otherData.mapTech) {
			prabhidik_option_data.roof = this.props.otherData.mapTech.roof;
			prabhidik_option_data.plinthDetails = this.props.otherData.mapTech.plinthDetails;
		}

		if (isStringEmpty(initVal.prabidhikPratibedanDate)) {
			initVal.prabidhikPratibedanDate = getCurrentDate(true);
		}

		const isPuranoGhar = initialValues1.constructionType === ConstructionTypeValue.PURANO_GHAR;
		// Scehma
		const nadiSchema = nadiData.map((row) => {
			return {
				[row]: isPuranoGhar ? validateNullableZeroNumber : validateMinDistanceField('publicPropertyRequiredDistance').nullable(),
			};
		});

		const prabidhikSchema = Yup.object().shape(
			Object.assign(
				{
					prabidhikPratibedanDate: validateNullableNepaliDate,
					surjaminDate: validateNullableNepaliDate,
					topFloor: Yup.lazy((value) => {
						if (value) {
							const schema = {};
							Object.keys(value).forEach((key) => (schema[key] = validateNullableNormalNumber));
							return Yup.object().shape(schema);
						} else return Yup.object().nullable();
					}),
					floor: Yup.lazy((value) => {
						if (value) {
							const schema = {};
							Object.keys(value).forEach((key) => (schema[key] = validateNullableNormalNumber));
							return Yup.object().shape(schema);
						} else return Yup.object().nullable();
					}),
				},
				...formDataSchema,
				...numberDataSchema,
				...nadiSchema
			)
		);

		this.state = {
			initVal,
			nadiOptions,
			floorArray,
			blocks,
			otherSetBackMaster,
			prabidhikSchema,
		};
	}

	render() {
		const { initVal, floorArray, blocks, nadiOptions, otherSetBackMaster, prabidhikSchema } = this.state;
		const {
			permitData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			userData: user_info,
			useSignatureImage,
		} = this.props;

		const detailed_Info = permitData;

		return (
			<div className="view-Form">
				<Formik
					initialValues={initVal}
					validationSchema={prabidhikSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						// const datatosend = {};

						try {
							let url = '';
							if (values.sanso === pppv_data.sansodhan_check[1]) {
								url = `${api.prabhidikPratibedhanPesh}${permitData.applicantNo}/Y`;
							} else {
								url = `${api.prabhidikPratibedhanPesh}${permitData.applicantNo}/N`;
							}

							// refetch the menu as the menu will be updated
							await this.props.postAction(url, values, false, false, true);

							// await this.props.postAction(
							// 	api.prabhidikPratibedhanPesh,
							// 	values,
							// 	true
							// );
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							window.scroll(0, 0);
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={(props) => {
						// console.log('irritating value===', props.values);
						// console.log(props.values.subName)
						return (
							<Form loading={props.isSubmitting}>
								{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="print-small-font">
									<div style={{ display: 'flex', float: 'right' }} className="no-margin-field">
										<DashedLangDateField
											name="prabidhikPratibedanDate"
											inline={true}
											compact={true}
											value={props.values.prabidhikPratibedanDate}
											error={props.errors.prabidhikPratibedanDate}
											setFieldValue={props.setFieldValue}
											label={getMessage(`${messageId}.date`, pppv_data.date)}
										/>
									</div>
									<FormHeading userinfo={this.props.userData} />
									<div className="no-margin-field">
										{getMessage(`${messageId}.ppbv_data_1`, pppv_data.ppbv_data_1)}
										<span>{user_info.organization.name}</span>
										{getMessage(`${messageId}.ppbv_data_2`, pppv_data.ppbv_data_2)}
										<span>{detailed_Info.nibedakTol}</span>
										{getMessage(`${messageId}.ppbv_data_3`, pppv_data.ppbv_data_3)}
										<span>{detailed_Info.nibedakName}</span>
										{getMessage(`${messageId}.ppbv_data_4`, pppv_data.ppbv_data_4)}
										<span>{detailed_Info.applicantName}</span>
										{getMessage(`${messageId}.ppbv_data_5`, pppv_data.ppbv_data_5)}
										<span>{detailed_Info.oldMunicipal}</span>
										{getMessage(`${messageId}.ppbv_data_6`, pppv_data.ppbv_data_6)}
										<span>{user_info.organization.name}</span>
										{getMessage(`${messageId}.ppbv_data_7`, pppv_data.ppbv_data_7)}
										<span>{detailed_Info.newWardNo}</span>
										{getMessage(`${messageId}.ppbv_data_8`, pppv_data.ppbv_data_8)}
										<span>{detailed_Info.sadak}</span>
										{getMessage(`${messageId}.ppbv_data_8_1`, pppv_data.ppbv_data_8_1)}
										<span>{detailed_Info.buildingJoinRoad}</span>
										{getMessage(`${messageId}.ppbv_data_8_2`, pppv_data.ppbv_data_8_2)}
										{getMessage(`${messageId}.ppbv_data_8_3`, pppv_data.ppbv_data_8_3)}
										<span>{detailed_Info.kittaNo}</span>
										{getMessage(`${messageId}.ppbv_data_9`, pppv_data.ppbv_data_9)}
										<span>{detailed_Info.landArea}</span> <span>{detailed_Info.landAreaType}</span>
										{getMessage(`${messageId}.ppbv_data_10`, pppv_data.ppbv_data_10)}
										<DashedLangDateField
											name="surjaminDate"
											handleChange={props.handleChange}
											value={props.values.surjaminDate}
											error={props.errors.surjaminDate}
											setFieldValue={props.setFieldValue}
											inline={true}
											compact={true}
										/>
										{/* <Input name='' className='dashedForm-control' /> */}
										{getMessage(`${messageId}.ppbv_data_11`, pppv_data.ppbv_data_11)}
									</div>
									{/* <br /> */}
									{/* <FormppbvDetails /> */}
									{/* <p> */}
									{/* <h6> */}
									{getMessage(`${messageId}.ppbv_content_1`, pppv_data.ppbv_content_1)}
									<DetailedSignature setFieldValue={props.setFieldValue} values={props.values} errors={props.errors}>
										<DetailedSignature.Name className="div-indent" name="subName" label={pppv_data.ppbv_content_2} />
										<DetailedSignature.Designation
											className="div-indent"
											name="subDesignation"
											label={pppv_data.ppbv_content_3}
										/>
										<DetailedSignature.Signature className="div-indent" name="subSignature" showSignature={useSignatureImage} />
									</DetailedSignature>
									<br />
									{/* <h3> */}
									<span style={{ display: 'block', textAlign: 'center' }}>
										{getMessage(`${messageId}.ppbv_footer_header`, pppv_data.ppbv_footer_header)}
									</span>
									{/* </h3> */}
									<br />
									{/* <p> */}
									{/* {getMessage(`${messageId}.ppbv_footer_data_1`, pppv_data.ppbv_footer_data_1)} */}
									{/* {permitLang.form_step5.checkBox_option2.map((name, index) => (
										<div key={index} className="ui radio checkbox prabidhik">
											<Field type="radio" name="constructionType" id={index} defaultChecked={props.values.constructionType === name} value={name} />
											<label>{name}</label>
										</div>
									))} */}
									{/* {isBiratnagar ? (
										<ConstructionTypeRadioBiratnagar
											space={true}
											fieldLabel={getMessage(`${messageId}.ppbv_footer_data_1`, pppv_data.ppbv_footer_data_1)}
											errors={props.errors}
											values={props.values}
											setFieldValue={props.setFieldValue}
										/>
									) : ( */}
									<ConstructionTypeRadio
										space={true}
										fieldLabel={getMessage(`${messageId}.ppbv_footer_data_1`, pppv_data.ppbv_footer_data_1)}
									/>
									{/* )} */}
									<br />
									{getMessage(`${messageId}.ppbv_footer_data_2`, pppv_data.ppbv_footer_data_2)}
									<DashedAreaUnitInput name="plinthDetails" unitName="plinthDetailsUnit" />
									<br />
									{/* <div className="inline field"> */}
									<DashedMultiUnitLengthInput
										name="namedMapdanda"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedAdhikar']}
										label={pppv_data.ppbv_footer_data_3}
										error={props.errors.namedMapdanda}
									/>
									<DashedMultiUnitLengthInput
										name="namedAdhikar"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedMapdanda']}
										label={pppv_data.ppbv_footer_data_3_1}
										error={props.errors.namedAdhikar}
									/>
									<DashedMultiUnitLengthInput
										name="requiredDistance"
										unitName="sadakAdhikarUnit"
										relatedFields={['namedAdhikar', 'namedMapdanda']}
										label={pppv_data.ppbv_footer_data_3_2}
										error={props.errors.requiredDistance}
									/>
									<br />
									{getMessage(`${messageId}.ppbv_footer_data_4`, pppv_data.ppbv_footer_data_4)}
									{Object.values(mapTechLang.mapTech_22.mapTech_22_radioBox).map((name, index) => (
										<RadioInput option={name} name="roof" key={index} />
									))}
									{props.values.roof === mapTechLang.mapTech_22.mapTech_22_radioBox.mapTech_22_Opt_6 && (
										<DashedLangInput
											// inline={true}
											name="roofOther"
											placeholder="Additional Information..."
											setFieldValue={props.setFieldValue}
											value={props.values.roofOther}
											error={props.errors.roofOther}
										/>
									)}
									<br />
									<span style={{ marginLeft: '20px' }}>
										{getMessage(`${messageId}.ppbv_footer_data_4_1`, pppv_data.ppbv_footer_data_4_1)}
										<DashedLangInput
											name="roofLen"
											value={props.values.roofLen}
											setFieldValue={props.setFieldValue}
											error={props.errors.roofLen}
										/>
									</span>
									<PrabidhikFloorDetails floorArray={floorArray} blocks={blocks} />
									{/* </div> */}
									{/* <div className="inline field"> */}
									<DashedUnitInput
										name="isHighTensionLineDistance"
										label={pppv_data.ppbv_footer_data_7}
										unitName="highTensionLineUnit"
										error={props.errors.isHighTensionLineDistance}
									/>
									{/* </div> */}
									{getMessage(`${messageId}.ppbv_footer_data_7_1`, pppv_data.ppbv_footer_data_7_1)}
									<DashedNormalInput
										name="elecVolt"
										value={props.values.elecVolt}
										setFieldValue={props.setFieldValue}
										handleChange={props.handleChange}
										error={props.errors.elecVolt}
									/>
									{/* <Field
										type="text"
										className="dashedForm-control"
										name="elecVolt"
										value={props.values.elecVolt}
										error={props.errors.elecVolt}
									/> */}
									<br />
									{/* <div className="inline field"> */}
									{getMessage(`${messageId}.ppbv_footer_data_8`, pppv_data.ppbv_footer_data_8)}
									<Select
										options={nadiOptions}
										name={`publicPropertyName`}
										// placeholder="Select an option"
										onChange={(e, { value }) => {
											props.setFieldValue(`publicPropertyName`, value);
											const reqNadi = otherSetBackMaster.find((nadi) => nadi.placeName === value);
											if (reqNadi) {
												if (props.values.publicPropertyUnit === 'METRE') {
													props.setFieldValue(`publicPropertyDistance`, reqNadi.setBackMeter);

													props.setFieldValue(`publicPropertyRequiredDistance`, reqNadi.setBackMeter);
												} else {
													props.setFieldValue(`publicPropertyDistance`, reqNadi.setBackFoot);

													props.setFieldValue(`publicPropertyRequiredDistance`, reqNadi.setBackFoot);
												}
											} else {
												props.setFieldValue(`publicPropertyDistance`, '');

												props.setFieldValue(`publicPropertyRequiredDistance`, '');
											}
											// console.log(listData.find(nadi => nadi.placeName === value));
											// console.log(nadiOptions);
										}}
										value={getIn(props.values, `publicPropertyName`)}
										error={!!props.errors.publicPropertyName}
									/>{' '}
									{!isNumberEmpty(getIn(props.values, `publicPropertyRequiredDistance`)) &&
										`${pppv_data.ppbv_footer_data_8_1_1} ${getIn(props.values, `publicPropertyRequiredDistance`)} `}
									{pppv_data.ppbv_footer_data_8_1}
									<DashedNormalInput
										// key={index}
										name={`publicPropertyDistance`}
										value={getIn(props.values, `publicPropertyDistance`)}
										error={getIn(props.errors, `publicPropertyDistance`)}
										handleChange={props.handleChange}
									/>{' '}
									<div className="brackets" style={{ display: 'inline-flex' }}>
										{`( `}
										<Dropdown
											name="publicPropertyUnit"
											onChange={(e, { value }) => {
												surroundingMappingFlat.forEach((side, index) => {
													const reqNadi = otherSetBackMaster.find(
														(nadi) => nadi.placeName === getIn(props.values, `publicPropertyName`)
													);
													if (reqNadi) {
														if (value === 'METRE') {
															props.setFieldValue(`publicPropertyRequiredDistance`, reqNadi.setBackMeter);
														} else {
															props.setFieldValue(`publicPropertyRequiredDistance`, reqNadi.setBackFoot);
														}
													}
												});

												props.setFieldValue('publicPropertyUnit', value);
											}}
											value={getIn(props.values, 'publicPropertyUnit')}
											options={distanceOptions}
										/>
										{' ) '}
									</div>
									<br />
									{getMessage(`${messageId}.ppbv_footer_data_9`, pppv_data.ppbv_footer_data_9)}
									<Select
										options={field_9}
										name="fieldNine"
										// placeholder="Select an option"
										onChange={(e, { value }) => props.setFieldValue('fieldNine', value)}
										value={props.values['fieldNine']}
										error={!!props.errors.fieldNine}
										// defaultValue={props.values['fieldNine']}
									/>
									<br />
									{getMessage(`${messageId}.ppbv_footer_data_10`, pppv_data.ppbv_footer_data_10)}
									<Select
										options={field_10}
										name="fieldTen"
										// placeholder="Select an option"
										onChange={(e, { value }) => props.setFieldValue('fieldTen', value)}
										value={props.values['fieldTen']}
										error={!!props.errors.fieldTen}
										// defaultValue={props.values['fieldNine']}
									/>
									<br />
									<SansodhanCheckbox
										label={pppv_data.sansodhan_checklabel}
										values={props.values}
										handleChange={props.handleChange}
										options={pppv_data.sansodhan_check}
										constructionType={permitData.constructionType}
									/>
									{getMessage(`${messageId}.ppbv_footer_data_11`, pppv_data.ppbv_footer_data_11)}
									<br />
									<div className="div-indent">
										{getMessage(`${messageId}.ppbv_footer_data_11_1`, pppv_data.ppbv_footer_data_11_1)}:
										<DashedLangInput
											// labelName={permitFormLang.petitioner_additional}
											name="buildingAdditional"
											setFieldValue={props.setFieldValue}
											value={props.values['buildingAdditional']}
											line="full-line"
										/>
									</div>
									<div className="div-indent">
										{getMessage(`${messageId}.ppbv_footer_data_11_2`, pppv_data.ppbv_footer_data_11_2)}:
										<DashedLangInput
											// labelName={permitFormLang.petitioner_additional}
											name="sabikAdditional"
											setFieldValue={props.setFieldValue}
											value={props.values['sabikAdditional']}
											line="full-line"
										/>
									</div>
								</div>
								<SaveButtonValidation
									errors={props.errors}
									validateForm={props.validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={props.handleSubmit}
								/>
								{/* <Form.Button primary content='Save' /> */}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const FormHeading = (props) => {
	const UserInfoData = props.userinfo;
	return (
		<>
			<span style={{ textAlign: 'left' }}>
				<p>
					{getMessage(`${messageId}.ppbv_heading`, pppv_data.ppbv_heading)}
					<br />
					<span>{UserInfoData.organization.name}</span>
					<br />
					<span>{UserInfoData.organization.officeName}</span>
					<br />
					<span>
						{UserInfoData.organization.address}
						{', '}
						{UserInfoData.organization.province}
					</span>
				</p>
			</span>

			<span style={{ textAlign: 'center' }}>
				<h3>{getMessage(`${messageId}.ppbv_subject`, pppv_data.ppbv_subject)} </h3>
			</span>
		</>
	);
};

const PrabhidikPratibedhanPeshView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			// {
			// 	api: api.anusuchiKaMaster,
			// 	objName: 'buildingClass',
			// 	form: false,
			// 	utility: true,
			// },
			{
				api: api.surjaminMuchulka,
				objName: 'SurjaminDate',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'RajaswoData',
				form: false,
			},
			// {
			// 	api: api.organizationUserInfo,
			// 	objName: 'serInfo',
			// 	form: false,
			// 	utility: true,
			// },
			{
				api: api.otherSetBack,
				objName: 'otherSetBack',
				form: false,
				utility: true,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.REMOVE_ON_PRINT,
			paramCheckbox: [PrintIdentifiers.CHECKBOX_LABEL],
			param8: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			//param1: ["getElementsByTagName", "input", "value"],
			// param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <PrabhidikPratibedhanPeshViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default PrabhidikPratibedhanPeshView;
