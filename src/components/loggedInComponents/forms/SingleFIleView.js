import React, { Component } from 'react';
import { Divider, List } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { postFile, getUploadedFiles } from '../../../store/actions/fileActions';
import { getPermitAndUserData } from '../../../store/actions/formActions';

import ErrorDisplay from '../../shared/ErrorDisplay';
// import IconPdf from '../../../assets/images/pdf.svg';
import Viewer from 'react-viewer';

import { getDocUrl } from '../../../utils/config';
import { getSplitUrl } from '../../../utils/functionUtils';
// import DeleteModal from './../../shared/DeleteModal';
import { ImageDisplayPreview } from '../../shared/file/FileView';
// let urlArr = [];
export class SingleFileView extends Component {
	state = {
		currImg: 0,
		isDeleteModalOpen: false,
	};
	componentDidMount() {
		// this.props.getPermitAndUserData();
		// this.props.getUploadedFiles();
		// if (this.props.files && this.props.files.length > 0) {
		// 	urlArr = this.props.files
		// 		.map(el => {
		// 			if (getSplitUrl(el.fileUrl, '.') != 'pdf') {
		// 				return { src: `${getDocUrl()}${el.fileUrl}` };
		// 			}
		// 		})
		// 		.filter(src => src);
		// }
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success) {
			this.props.getUploadedFiles();
		}
	}

	// handleDeleteModalOpen = id => {
	// 	this.setState({
	// 		isDeleteModalOpen: true,
	// 		id: id,
	// 	});
	// };

	// handleDeleteModalClose = () => {
	// 	this.setState({
	// 		isDeleteModalOpen: false,
	// 	});
	// };

	render() {
		const { files } = this.props;
		return (
			<>
				{this.props.fileCategories
					.filter(category => category && category.viewUrl.trim() === this.props.url)
					.map(category => (
						<div key={category.id}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<Divider horizontal>{category.name}</Divider>{' '}
							<List relaxed>
								<List.Header>Existing Files</List.Header>
								<div className="previewFileUpload">
									{files && files.length > 0 && files.some(file => file.fileType.id === category.id) ? (
										files
											.filter(file => file.fileType.id === category.id)
											.map((el, index) => {
												const fileTitleName = getSplitUrl(el.fileUrl, '/');
												return (
													<div key={el.fileUrl} className={'eachPreviewFile viewFileSuccess'} title={fileTitleName}>
														<>
															<ImageDisplayPreview
																src={el.fileUrl}
																alt={fileTitleName}
																onClick={() => {
																	this.setState({
																		imageOpen: true,
																		currImg: { src: `${getDocUrl()}${el.fileUrl}` },
																	});
																}}
															/>
															{/* {isDelete && <Icon name="close" className="closeImg" onClick={() => this.handleDeleteModalOpen(el.id)} />} */}
														</>
													</div>
												);
												// const pdfValidate = getSplitUrl(el.fileUrl, ".");
												// const fileTitleName = getSplitUrl(el.fileUrl, "/");
												// return (
												//   <div
												//     key={el.fileUrl}
												//     className={
												//       pdfValidate === "pdf"
												//         ? "eachPreviewFile  viewFileSuccess pdfFilePreview"
												//         : "eachPreviewFile viewFileSuccess"
												//     }
												//     title={fileTitleName}
												//   >
												//     {pdfValidate === "pdf" ? (
												//       <a
												//         href={`${getDocUrl()}${el.fileUrl}`}
												//         target="_blank"
												//         rel="noopener noreferrer"
												//       >
												//         <img src={IconPdf} alt="pdfIconFile" />
												//       </a>
												//     ) : (
												//       <>
												//         <img
												//           src={`${getDocUrl()}${el.fileUrl}`}
												//           alt="imageFile"
												//           onClick={() => {
												//             this.setState({
												//               imageOpen: true,
												//               currImg: urlArr.findIndex(
												//                 each =>
												//                   each.src ==
												//                   `${getDocUrl()}${el.fileUrl}`
												//               )
												//             });
												//           }}
												//         />
												//         {userData && userData.userType && category.formName.enterBy ==
												//           userData.userType && (
												//           <Icon
												//             name="close"
												//             className="closeImg"
												//             onClick={() =>
												//               this.handleDeleteModalOpen(el.id)
												//             }
												//           />
												//         )}
												//       </>
												//     )}
												//   </div>
												// );
											})
									) : (
										<List.Item>No files uploaded.</List.Item>
									)}
								</div>
							</List>
							<Viewer
								visible={this.state.imageOpen}
								onClose={() => this.setState({ imageOpen: false })}
								images={[this.state.currImg]}
								zIndex={10000}
							/>
							{/* <DeleteModal
								open={this.state.isDeleteModalOpen}
								onClose={this.handleDeleteModalClose}
								history={this.props.parentHistory}
								loadingModal={this.props.loadingModal}
								id={this.state.id}
								deleteFile={() => this.props.deleteFile(this.state.id)}
							/> */}
						</div>
					))}
			</>
		);
	}
}

const mapDispatchToProps = {
	postFile,
	getPermitAndUserData,
	getUploadedFiles,
};

const mapStateToProps = state => ({
	files: state.root.formData.files,
	fileCategories: state.root.formData.fileCategories,
	success: state.root.formData.success,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleFileView);
