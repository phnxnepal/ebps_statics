import { Formik, getIn, Field } from 'formik';
import React, { Component } from 'react';

import { Form, Table, TableCell, Message } from 'semantic-ui-react';
import FormikCheckbox from '../../shared/FormikCheckbox';
import { SelectInput } from '../../shared/Select';
import EbpsForm from '../../shared/EbpsForm';
import EbpsTextareaField from '../../shared/MyTextArea';
import { buildingPermitApplicationForm } from '../../../utils/data/mockLangFile';
import MapPermitApplicationView from './MapPermintApplicationView';
import { isEmpty, setBuildPermit, getUserInfoObj } from '../../../utils/functionUtils';

import { LangDateField } from '../../shared/DateField';
import { LabelValue } from '../../shared/EbpsUnitLabelValue';
import { floorMappingFlat, surroundingMappingFlat, handleSuccess } from '../../../utils/dataUtils';

import cloneDeep from 'lodash/cloneDeep';
import { uniq } from 'lodash';
import api from '../../../utils/api';
import JSONDataMultipleGetFormContainer from '../../../containers/base/JSONDataMultipleGetFormContainer';
import { translateDate } from '../../../utils/langUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { checkError } from './../../../utils/dataUtils';
import { getDeleteDisabledStatus } from './../../../utils/urlUtils';
import { chaChainaNepaliOptions, distanceOptions } from '../../../utils/optionUtils';
import { FloorTable, FloorTableHeader } from './mapPermitComponents/FloorTable';
import { DEFAULT_UNIT_LENGTH as DefaultUnitLength } from '../../../utils/constants';
import { BuildingPermitKamalamaiSchema, BuildingPermitSchema } from '../formValidationSchemas/buildPermitValidation';
import { getConstructionTypeValue } from '../../../utils/enums/constructionType';
import { WardAddressSection } from './mapPermitComponents/WardAddressSection';
import MyMapComponent from '../OpenStreamMap';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { CommercialTable } from './mapPermitComponents/CommercialTable';
import { selectFloorValues } from '../../../store/selectors/form/buildingPermit';
import { BuildPermitHelper } from './mapPermitComponents/BuildPermitHelpers';
import { PermitSubjectParagraph } from './mapPermitComponents/PermitSubjectParagraph';
import { isKamalamai, isBirtamod, isBiratnagar } from '../../../utils/clientUtils';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { mapData } from '../../../utils/data/mapPermit';
import { SignatureImage } from './formComponents/SignatureImage';
import { UNITS } from '../../../utils/enums/unit';
import { ApplicantDetails } from './mapPermitComponents/ApplicantDetails';
import { LocationDetails } from './mapPermitComponents/LocationDetails';
import { PermitSectionHeader } from './mapPermitComponents/PermitSectionHeader';
import { ConstructionTypeSection } from './mapPermitComponents/ConstructionTypeSection';

const landPropsName = [
	{ key: 1, value: 'निजी', text: 'निजी' },
	{ key: 2, value: 'गुठी', text: 'गुठी' },
	{ key: 3, value: 'साझा', text: 'साझा' },
	{ key: 4, value: 'सरकारी', text: 'सरकारी' },
	{ key: 5, value: 'संयुक्त', text: 'संयुक्त' },
];

const DEFAULT_UNIT_LENGTH = isBirtamod ? UNITS.METRE.LENGTH : DefaultUnitLength;

const permitFormLang = buildingPermitApplicationForm.permitApplicationFormView;

const initialValues = {
	buildingJoinRoadType: [permitFormLang.form_step2.checkBox_option.option_1],
	earthquake: 'N',
	buildingJoinRoadTypeOther: '',
	purposeOfConstructionOther: '',
	constructionTypeOther: '',
	municipalName: getUserInfoObj() && getUserInfoObj().organization.name,
	mohada: [permitFormLang.form_step5.checkBox_option3.option_1],
	constructionFinishingOther: '',
	foharArrangementOther: '',
	floor: [{ floorUnit: DEFAULT_UNIT_LENGTH }],
	dhalUnit: DEFAULT_UNIT_LENGTH,
	highTensionLineUnit: DEFAULT_UNIT_LENGTH,
	pipelineUnit: DEFAULT_UNIT_LENGTH,
	kittaNo: [''],
	landArea: [''],
	// surrounding: [
	//   { side: '1', sideUnit: 'METRE' },
	//   { side: '2', sideUnit: 'METRE' },
	//   { side: '3', sideUnit: 'METRE' },
	//   { side: '4', sideUnit: 'METRE' }
	// ]
	// purposeOfConstruction: [permitFormLang.form_step5.checkBox_option.option_1],
	// constructionType: [permitFormLang.form_step5.checkBox_option2.option_1],
	// mohada: [permitFormLang.form_step5.checkBox_option3.option_1],
	// constructionFinishing: [permitFormLang.form_step5.checkBox_option4.option_1],
	// dhalNikasArrangement: [permitFormLang.form_step6.checkBox_option.option_1],
	// foharArrangement: [permitFormLang.form_step7.checkBox_option.option_1],
};

class BuildingPermitComponent extends Component {
	constructor(props) {
		super(props);
		this.printMapPermitApplication = React.createRef();

		const { prevData } = this.props;

		let lat = 27.600769;
		let lng = 85.30014;

		if (prevData) {
			if (prevData.lat) lat = Number(prevData.lat);
			if (prevData.lng) lng = Number(prevData.lng);
		}

		const wardOptions = this.props.otherData.wardMaster.map((ward) => {
			return { key: ward.id, value: ward.name, text: ward.name };
		});

		const formUrl = this.props.parentProps.location.pathname;
		const { hasDeletePermission, isSaveDisabled } = getDeleteDisabledStatus(formUrl, this.props.prevData, this.props.userData);

		let initVal = {};

		initVal = this.props.prevData;

		/**
		 * @todo refactor this code block
		 */
		if (!Array.isArray(this.props.prevData.buildingJoinRoadType)) {
			if (this.props.prevData.buildingJoinRoadType && this.props.prevData.buildingJoinRoadType.includes(',')) {
				initVal.buildingJoinRoadType = this.props.prevData.buildingJoinRoadType.split(',');
			} else if (initVal.buildingJoinRoadType) {
				initVal.buildingJoinRoadType = [this.props.prevData.buildingJoinRoadType];
			}
		}

		if (!Array.isArray(this.props.prevData.mohada)) {
			if (this.props.prevData.mohada && this.props.prevData.mohada.includes(',')) {
				initVal.mohada = this.props.prevData.mohada.split(',');
			} else if (initVal.mohada) {
				initVal.mohada = [this.props.prevData.mohada];
			}
		}

		if (!Array.isArray(this.props.prevData.kittaNo)) {
			if (this.props.prevData.kittaNo && this.props.prevData.kittaNo.includes(',')) {
				initVal.kittaNo = this.props.prevData.kittaNo.split(', ');
				initVal.landDetails = Array.apply(null, { length: initVal.kittaNo.length }).map(() => '');
			} else if (initVal.kittaNo) {
				initVal.kittaNo = [this.props.prevData.kittaNo];
				initVal.landDetails = [''];
			}
		}

		if (!Array.isArray(this.props.prevData.landArea)) {
			if (this.props.prevData.landArea && this.props.prevData.landArea.includes(',')) {
				initVal.landArea = this.props.prevData.landArea.split(', ');
			} else if (initVal.landArea) {
				initVal.landArea = [this.props.prevData.landArea];
			}
		}

		/**
		 * @todo refactor later.
		 */
		Object.keys(initVal).forEach((key) => {
			if (initVal[key] === null || initVal[key] === undefined) {
				initVal[key] = '';
			}
		});

		const floorClone = cloneDeep(selectFloorValues(initVal.floor)) || [];
		let firstFloorIndex = 0,
			floorUnit = DEFAULT_UNIT_LENGTH;

		const blocks = uniq(floorClone.map((fl) => fl && fl.block)).filter((fl) => fl);
		if (!(blocks && blocks.length > 0)) {
			floorMappingFlat.forEach((fl, index) => {
				if (initVal.floor && floorClone) {
					initVal.floor[index] = floorClone.find((fll) => {
						if (fll) {
							return parseInt(fll.floor) === index;
						} else {
							return false;
						}
					});
				}
			});

			for (const [index, floor] of floorClone.entries()) {
				if (!isStringEmpty(floor)) {
					firstFloorIndex = index;
					floorUnit = floor.floorUnit || DEFAULT_UNIT_LENGTH;
					break;
				}
			}
		} else {
			floorUnit = (initVal.floor[firstFloorIndex] && initVal.floor[firstFloorIndex].floorUnit) || DEFAULT_UNIT_LENGTH;
		}

		const surroundingClone = cloneDeep(initVal.surrounding);

		// console.log('suur clone', surroundingClone);
		surroundingMappingFlat.forEach((sr, index) => {
			if (initVal.surrounding) {
				initVal.surrounding[index] = surroundingClone.find((srr) => {
					if (srr) {
						return srr.side === index + 1;
					} else {
						return false;
					}
				});
			}
		});

		if (isStringEmpty(initVal.applicantDate)) {
			initVal.applicantDate = getCurrentDate(true);
		}
		// console.log('surr now', initVal.surrounding);

		if (isStringEmpty(initialValues.municipalName)) {
			initialValues.municipalName = getUserInfoObj() && getUserInfoObj().organization.name;
		}

		initVal.constructionType = getConstructionTypeValue(initVal.constructionType);

		this.state = {
			view: false,
			hasDeletePermission,
			isSaveDisabled,
			initVal: { ...initialValues, ...initVal, floorUnit },
			blocks,
			mapPosition: [lat, lng],
			marker: { lat, lng },
			wardOptions,
			hasBlocks: blocks && blocks.length > 0 ? chaChainaNepaliOptions[0].value : chaChainaNepaliOptions[1].value,
		};
	}

	handleMarkerClick = (e) => {
		this.setState({ marker: e.latlng });
	};

	handleFindOnMap = (lat, lng) => {
		if (!lat || !lng) {
			return;
		}
		this.setState({ marker: { lat, lng }, mapPosition: [lat, lng] });
	};

	render() {
		const { initVal, blocks } = this.state;
		const { staticFiles } = this.props;
		return (
			<React.Fragment>
				<div style={{ display: 'none' }}>
					<div ref={this.props.setRef}>
						<MapPermitApplicationView ghardhaniSignature={staticFiles.ghardhaniSignature} />
					</div>
				</div>

				<div className="buildingPermit-applicationForm">
					<Formik
						initialValues={initVal}
						validationSchema={isKamalamai ? BuildingPermitKamalamaiSchema : BuildingPermitSchema}
						onSubmit={(values, actions) => {
							actions.setSubmitting(true);

							const { filteredData, dataToSend } = BuildPermitHelper.formatEditPostValues(values, this.state.marker);

							this.props
								.postAction(api.buildPermit, filteredData, false, true, true)
								.then((res) => {
									window.scroll(0, 0);
									// // console.log('post props', res);
									// if (!isEmpty(this.props.errors)) {
									//   actions.setErrors(this.props.errors.message);
									// } else {
									//   showToast('Data successfully saved.');

									//   setTimeout(() => {
									//     this.props.parentProps.history.push(
									//       getNextUrl(this.props.parentProps.location.pathname)
									//     );
									//   }, 1000);
									// }
									// actions.setSubmitting(false);
									// // console.log('Data updated', res);
									if (this.props.success && this.props.success.success) {
										setBuildPermit(dataToSend);
										handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
									}
									actions.setSubmitting(false);
								})
								.catch((err) => {
									//   showToast(err)
									actions.setSubmitting(false);
								});
						}}
						render={(props) => {
							// const floorDetails = permitFormLang.form_step9.floor_details;
							const memberDetails = permitFormLang.form_step10.member_details;
							// let memberIndex = 0;
							// let floorIndex = 0;

							// console.log('values', props.values, props.errors, this.state);
							return (
								<Form loading={props.isSubmitting}>
									{!isEmpty(this.props.errors) && (
										<Message negative>
											<Message.Header>Error</Message.Header>
											<p>{this.props.errors.message}</p>
										</Message>
									)}
									<div>
										<PermitSubjectParagraph applicantMs={props.values.applicantMs} />
										<PermitSectionHeader content={permitFormLang.form_tapasil} />
										<ApplicantDetails setFieldValue={props.setFieldValue} errors={props.errors} values={props.values} />
										<WardAddressSection
											data={isBiratnagar ? permitFormLang.form_step2_biratnagar : permitFormLang.form_step2}
											newMunicipal={initVal.newMunicipal}
											setFieldValue={props.setFieldValue}
											errors={props.errors}
											values={props.values}
											wardOptions={this.state.wardOptions}
										/>
										{/* Section 3 -- */}
										<LocationDetails
											setFieldValue={props.setFieldValue}
											errors={props.errors}
											values={props.values}
											landPropsName={landPropsName}
											handleChange={props.handleChange}
										/>

										<PermitSectionHeader content={permitFormLang.form_step5.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_1}</span>
											{Object.values(permitFormLang.form_step5.checkBox_option).map((option) => (
												<div className="ui radio checkbox" key={option}>
													<input
														type="radio"
														name="purposeOfConstruction"
														value={option}
														defaultChecked={props.values.purposeOfConstruction === option}
														onChange={(e) => {
															const oldVal = props.values.purposeOfConstruction;
															if (
																((e.target.value === permitFormLang.form_step5.checkBox_option.option_1 ||
																	e.target.value === permitFormLang.form_step5.checkBox_option.option_3) &&
																	oldVal === permitFormLang.form_step5.checkBox_option.option_2) ||
																((oldVal === permitFormLang.form_step5.checkBox_option.option_1 ||
																	oldVal === permitFormLang.form_step5.checkBox_option.option_3) &&
																	e.target.value === permitFormLang.form_step5.checkBox_option.option_2)
															) {
																props.setFieldValue('floor', []);
															}
															props.handleChange(e);
														}}
													/>
													<label>{option}</label>
												</div>
											))}

											{props.values.purposeOfConstruction === permitFormLang.form_step5.checkBox_option.option_3 && (
												<EbpsForm
													name="purposeOfConstructionOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													value={props.values.purposeOfConstructionOther}
													error={props.errors.purposeOfConstructionOther}
												/>
											)}
										</div>
										<ConstructionTypeSection props={props} />
										<div className="frmCheckbox-wrap">
											<LangDateField
												label={permitFormLang.form_step5.fieldName_3}
												name="oldMapDate"
												setFieldValue={props.setFieldValue}
												value={props.values.oldMapDate}
												error={props.errors.oldMapDate}
											/>
										</div>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_4}</span>
											<FormikCheckbox
												name="mohada"
												// labelName={
												//   permitFormLang.form_step5.checkBox_option3.option_1
												// }
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_1}
											/>
											<FormikCheckbox
												name="mohada"
												// labelName={
												//   permitFormLang.form_step5.checkBox_option3.option_2
												// }
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_2}
											/>
											<FormikCheckbox
												name="mohada"
												// labelName={
												//   permitFormLang.form_step5.checkBox_option3.option_3
												// }
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_3}
											/>
											<FormikCheckbox
												name="mohada"
												// labelName={
												//   permitFormLang.form_step5.checkBox_option3.option_4
												// }
												onChange={props.handleChange}
												value={permitFormLang.form_step5.checkBox_option3.option_4}
											/>
											{/* {Object.values(
                      permitFormLang.form_step5.checkBox_option3
                    ).map(option => (
                      <div className='ui radio checkbox'>
                        <input
                          type='radio'
                          name='mohada'
                          value={option}
                          id={option}
                          defaultChecked={props.values.mohada === option}
                          onChange={props.handleChange}
                        />
                        <label>{option}</label>
                      </div>
                    ))} */}
										</div>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step5.fieldName_5}</span>

											{Object.values(permitFormLang.form_step5.checkBox_option4).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="constructionFinishing"
														value={option}
														defaultChecked={props.values.constructionFinishing === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.constructionFinishing === permitFormLang.form_step5.checkBox_option4.option_6 && (
												<EbpsForm
													name="constructionFinishingOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													value={props.values.constructionFinishingOther}
													error={props.errors.constructionFinishingOther}
												/>
											)}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step6.heading} />
										<div className="frmCheckbox-wrap">
											{Object.values(permitFormLang.form_step6.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="dhalNikasArrangement"
														value={option}
														defaultChecked={props.values.dhalNikasArrangement === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.dhalNikasArrangement === permitFormLang.form_step6.checkBox_option.option_3 && (
												<div className="sewageMgmt">
													<span>{permitFormLang.form_step6.fieldName_1}</span>
													<LabelValue
														name="dhalNikasArrangementOther"
														onChange={props.handleChange}
														value={props.values.dhalNikasArrangementOther}
														setFieldValue={props.setFieldValue}
														options={distanceOptions}
														nameUnit="dhalUnit"
														unitvalue={props.values.dhalUnit}
														error={props.errors.dhalNikasArrangementOther}
													/>
												</div>
											)}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step7.heading} />
										<div className="frmCheckbox-wrap">
											{Object.values(permitFormLang.form_step7.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="foharArrangement"
														value={option}
														defaultChecked={props.values.foharArrangement === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
											{props.values.foharArrangement === permitFormLang.form_step7.checkBox_option.option_3 && (
												<EbpsForm
													name="foharArrangementOther"
													placeholder="Additional Information..."
													setFieldValue={props.setFieldValue}
													value={props.values.foharArrangementOther}
													error={props.errors.foharArrangementOther}
												/>
											)}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step8.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step8.fieldName_1}</span>

											{Object.values(permitFormLang.form_step8.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="pipeline"
														value={option}
														defaultChecked={props.values.pipeline === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
										</div>
										{props.values.pipeline === permitFormLang.form_step8.checkBox_option.option_1 && (
											<div className="pipelineMgmt">
												<span>{permitFormLang.form_step8.fieldName_2}</span>
												<LabelValue
													name="pipelineDistance"
													onChange={props.handleChange}
													value={props.values.pipelineDistance}
													setFieldValue={props.setFieldValue}
													options={distanceOptions}
													nameUnit="pipelineUnit"
													unitvalue={props.values.pipelineUnit}
													error={props.errors.pipelineDistance}
												/>
											</div>
										)}
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step8.fieldName_3}</span>

											{Object.values(permitFormLang.form_step8.checkBox_option2).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="doPipelineConnection"
														value={option}
														defaultChecked={props.values.doPipelineConnection === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
										</div>
										<PermitSectionHeader content={permitFormLang.form_step9.heading} />
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step9.fieldName_1}</span>

											{Object.values(permitFormLang.form_step9.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="isHighTensionLine"
														value={option}
														defaultChecked={props.values.isHighTensionLine === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
										</div>
										<div className="frmCheckbox-wrap">
											<span>{permitFormLang.form_step9.fieldName_2}</span>

											{Object.values(permitFormLang.form_step9.checkBox_option).map((option) => (
												<div key={option} className="ui radio checkbox">
													<input
														type="radio"
														name="isLowTensionLine"
														value={option}
														defaultChecked={props.values.isLowTensionLine === option}
														onChange={props.handleChange}
													/>
													<label>{option}</label>
												</div>
											))}
										</div>
										{/* {props.values.isHighTensionLine === */}
										{/* permitFormLang.form_step9.checkBox_option.option_1 && ( */}
										<div className="elecMgmt">
											<span>{permitFormLang.form_step9.fieldName_3}</span>
											<LabelValue
												name="isHighTensionLineDistance"
												onChange={props.handleChange}
												value={props.values.isHighTensionLineDistance}
												setFieldValue={props.setFieldValue}
												options={distanceOptions}
												nameUnit="highTensionLineUnit"
												unitvalue={props.values.highTensionLineUnit}
												error={props.errors.isHighTensionLineDistance}
											/>
										</div>
										{/* )} */}
										<br />
										{/* <div style={{ marginLeft: '30px' }}>
											<Table celled compact collapsing>
												<Table.Header>
													<Table.Row textAlign="center">
														{Object.keys(floorDetails.table_heading).map((key, index) => (
															<Table.HeaderCell key={index}>
																{floorDetails.table_heading[key]}
																<br />
																{index === 0 && (
																	<Dropdown
																		value={getIn(props.values, `floor.${firstFloorIndex}.floorUnit`)}
																		name={`floor.${firstFloorIndex}.floorUnit`}
																		onChange={(e, { value }) => {
																			const prevunit = getIn(props.values, `floor.${firstFloorIndex}.floorUnit`);

																			props.values.floor &&
																				props.values.floor.forEach((fl, index) => {
																					if (fl) {
																						props.setFieldValue(`floor.${index}.length`, lengthconvertor(fl.length, value, prevunit));
																						props.setFieldValue(`floor.${index}.width`, lengthconvertor(fl.width, value, prevunit));
																						props.setFieldValue(`floor.${index}.height`, lengthconvertor(fl.height, value, prevunit));
																						props.setFieldValue(
																							`floor.${index}.area`,
																							areaconvertor(fl.area, `SQUARE ${value}`, `SQUARE ${prevunit}`)
																						);
																					}
																				});
																			props.setFieldValue(`floor.${firstFloorIndex}.floorUnit`, value);
																		}}
																		options={distanceOptions}
																	/>
																)}
															</Table.HeaderCell>
														))}
													</Table.Row>
												</Table.Header>
												<FloorTable {...props} />
											</Table>
										</div> */}
										<div>
											<b>{permitFormLang.form_tallaBibarab}</b>
										</div>
										<div style={{ marginLeft: '30px' }}>
											<div>
												<b>
													{permitFormLang.form_step9.floor_details.block}
													{chaChainaNepaliOptions.map((option) => (
														<React.Fragment key={option.key}>
															<div className={`ui radio checkbox ${'prabidhik'}`}>
																<input
																	type="radio"
																	value={option.value}
																	checked={this.state.hasBlocks === option.value}
																	onChange={(e) => {
																		props.setFieldValue('floor', []);
																		this.setState({ hasBlocks: option.value });
																	}}
																/>
																<label>{option.value}</label>
															</div>
														</React.Fragment>
													))}
												</b>
											</div>
											{/* {props.values.purposeOfConstruction === permitFormLang.form_step5.checkBox_option.option_2 ? ( */}
											{this.state.hasBlocks === chaChainaNepaliOptions[0].value ? (
												<CommercialTable
													setFieldValue={props.setFieldValue}
													handleChange={props.handleChange}
													values={props.values}
													errors={props.errors}
													initialBlocks={blocks}
												/>
											) : (
												<Table celled compact collapsing>
													<FloorTableHeader
														errors={props.errors}
														values={props.values}
														setFieldValue={props.setFieldValue}
													/>
													<FloorTable {...props} isEdit={true} />
												</Table>
											)}
										</div>
										<div>
											<PermitSectionHeader content={permitFormLang.form_step10.heading} />
											{props.errors.member && <span className="tableError">{props.errors.member}</span>}

											<div style={{ marginLeft: '30px' }} className={`field ${props.errors.member ? 'error' : ''}`}>
												<Table celled compact collapsing>
													<Table.Header>
														<Table.Row textAlign="center">
															{Object.keys(memberDetails.table_heading).map((key) => (
																<Table.HeaderCell key={key}>{memberDetails.table_heading[key]}</Table.HeaderCell>
															))}
														</Table.Row>
													</Table.Header>
													<Table.Body>
														{Object.keys(memberDetails.table_subheading)
															// .splice(0, initVal.member.length)
															.map((key, memberIndex) => {
																return (
																	<Table.Row key={memberIndex}>
																		<TableCell>{memberDetails.table_subheading[key]}</TableCell>
																		<TableCell>
																			<EbpsForm
																				style={{
																					width: '180px',
																				}}
																				name={`member[${memberIndex}].memberName`}
																				setFieldValue={props.setFieldValue}
																				// className="tablewidth"
																				value={getIn(props.values, `member[${memberIndex}].memberName`)}
																				error={getIn(props.errors, `member[${memberIndex}].memberName`)}
																			/>
																		</TableCell>
																		<TableCell>
																			<EbpsForm
																				style={{
																					width: '180px',
																				}}
																				name={`member[${memberIndex}].relation`}
																				setFieldValue={props.setFieldValue}
																				// className="tablewidth"
																				value={getIn(props.values, `member[${memberIndex}].relation`)}
																				error={getIn(props.errors, `member[${memberIndex}].relation`)}
																			/>
																		</TableCell>
																	</Table.Row>
																);
															})}
													</Table.Body>
												</Table>
											</div>
										</div>
										<br />
										<div className="petitionerDetails">
											<PermitSectionHeader content={permitFormLang.petitioner_nibedak} />
											<SignatureImage
												value={staticFiles.ghardhaniSignature}
												showSignature={true}
												label={permitFormLang.petitioner_signature}
											/>
											{/* <div className="petitionerSignature">{permitFormLang.petitioner_signature}</div> */}
											<div>
												<EbpsForm
													label={permitFormLang.petitioner_name}
													name="nibedakName"
													setFieldValue={props.setFieldValue}
													value={props.values['nibedakName']}
													error={props.errors.nibedakName}
												/>
											</div>
											<div>
												<EbpsForm
													label={permitFormLang.petitioner_age}
													name="tol"
													setFieldValue={props.setFieldValue}
													value={props.values['tol']}
													error={props.errors.tol}
												/>
											</div>
											<div>
												<EbpsForm
													label={permitFormLang.petitioner_municipal}
													name="newMunicipal"
													setFieldValue={props.setFieldValue}
													value={props.values['newMunicipal']}
													// error={props.errors.newMunicipal}
													// disabled
												/>
											</div>
											<div>
												<SelectInput
													label={permitFormLang.petitioner_vadaNo}
													name="nibedakTol"
													compact={true}
													options={this.state.wardOptions}
												/>
											</div>
											<div>
												<EbpsForm
													label={permitFormLang.petitioner_road}
													name="nibedakSadak"
													setFieldValue={props.setFieldValue}
													error={props.errors.nibedakSadak}
													value={props.values['nibedakSadak']}
												/>
											</div>
											<div>
												<EbpsForm
													label={permitFormLang.petitioner_phone}
													name="applicantMobileNo"
													setFieldValue={props.setFieldValue}
													value={props.values['applicantMobileNo']}
													error={props.errors.applicantMobileNo}
												/>
											</div>
											<div>
												<LangDateField
													label={permitFormLang.petitioner_date}
													//   name='applicantDate'
													//   setFieldValue={props.setFieldValue}
													value={translateDate(props.values.applicantDateBS)}
													//   error={props.errors.applicantDate}
													disabled={true}
												/>
											</div>
										</div>
										<br />
										<EbpsTextareaField
											labelName={permitFormLang.petitioner_additional}
											name="nibedakAdditional"
											setFieldValue={props.setFieldValue}
											value={props.values['nibedakAdditional']}
										/>
									</div>
									<br />
									<FlexSingleRight>
										<Form.Group inline style={{ paddingBottom: 10, paddingTop: 10 }}>
											<Form.Field>
												<label>
													<b>Latitude</b>
												</label>
												<Field name="lat" type="number" />
											</Form.Field>
											<Form.Field>
												<label>
													<b>Longitude</b>
												</label>
												<Field name="lng" type="number" />
											</Form.Field>
											<Form.Button
												primary
												icon="search"
												content={mapData.find}
												onClick={this.handleFindOnMap.bind(this, props.values.lat, props.values.lng)}
											/>
										</Form.Group>
									</FlexSingleRight>
									<MyMapComponent
										handleMarkerClick={(e) => {
											if (e.latlng) {
												props.setFieldValue('lat', e.latlng.lat);
												props.setFieldValue('lng', e.latlng.lng);
												this.handleMarkerClick(e);
											}
										}}
										marker={this.state.marker}
										position={this.state.mapPosition}
									/>
									<br />
									<SaveButtonValidation
										errors={props.errors}
										hasDeletePermission={this.state.hasDeletePermission}
										isSaveDisabled={this.state.isSaveDisabled}
										formUrl={this.props.parentProps.location.pathname}
										hasSavePermission={this.props.hasSavePermission}
										prevData={checkError(this.props.prevData)}
										handleSubmit={props.handleSubmit}
										validateForm={props.validateForm}
									/>
								</Form>
							);
						}}
					/>
				</div>
			</React.Fragment>
		);
	}
}

const BuildingPermit = (parentProps) => (
	<JSONDataMultipleGetFormContainer
		api={[
			{ api: api.buildPermit, objName: 'permitEdit', form: true },
			{ api: api.wardSetup, objName: 'wardMaster', form: false, utility: true },
		]}
		useInnerRef={true}
		hasFileView={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <BuildingPermitComponent {...props} parentProps={parentProps} />}
	/>
);

export default BuildingPermit;
