import React, { Component } from 'react';
import { Form, Table, Dropdown, Select } from 'semantic-ui-react';
import { rajaswoFormLang } from '../../../utils/data/rajaswoDetailLang';
import { Formik, getIn } from 'formik';

//Redux
import { isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { calculateTotal, round, getFormattedNumber } from '../../../utils/mathUtils';
import api from '../../../utils/api';
import { checkError, floorMappingFlat, prepareMultiInitialValues, getJsonData, handleSuccess } from '../../../utils/dataUtils';
import EbpsForm, { EbpsNumberForm } from '../../shared/EbpsForm';
import UnitDropdown from '../../shared/UnitDropdown';
import TableInput from '../../shared/TableInput';
import RajasowTableInput, {
	RajasowTable2Input,
	RajasowRateInput,
	RajasowDiscountPercent,
	RajasowUnitDropdownWithRelatedFields,
	getBatabaranDastur,
	getSarsaphaiDastur,
	needToCalculateAnyaAmt,
} from '../../shared/RajasowTableInput';
import {
	DashedLengthInputWithRelatedUnits,
	DashedAreaInputWithRelatedUnits,
	DashedLengthInputWithRelatedUnitsAndCalc,
} from '../../shared/EbpsUnitLabelValue';
import { isKankai, isKamalamai, isBirtamod, isMechi, isInaruwa, isKageyshowri } from '../../../utils/clientUtils';
import { mapTechnical } from '../../../utils/data/mockLangFile';
import { DEFAULT_UNIT_LENGTH } from '../../../utils/constants';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { SectionHeader } from '../../uiComponents/Headers';
import { ApiParam } from '../../../utils/paramUtil';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { FloorBlockArray, converToFeet } from '../../../utils/floorUtils';
import { BlockFloorComponents, BlockComponents } from '../../shared/formComponents/BlockFloorComponents';
import { floorData } from '../../../utils/data/genericData';
import { getRajasowSchema } from '../formValidationSchemas/rajasowDetailSchema';
import { NepaliNumberToWord } from '../../../utils/nepaliAmount';
import { hasDharauti, additionalFloorOptions, getFloorOptionsDy, jTypeOptions, Opt, initDetails, getInitDetails } from '../formUtils/rajasowUtils';
import { squareUnitOptions } from '../../../utils/optionUtils';
import { ConstructionTypeValue } from '../../../utils/enums/constructionType';
// import { commonMessages } from '../../../utils/data/validationData';

const areaUnitOptions = [
	{ key: 1, value: 'SQUARE METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'SQUARE FEET', text: 'वर्ग फिट' },
];

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

const mapTech_data = mapTechnical.mapTechnicalDescription;
const rajaswoForm = rajaswoFormLang;

class RajaswoDetailViewComponent extends Component {
	constructor(props) {
		super(props);

		let initVal = {};
		const { permitData, prevData, otherData } = this.props;
		const rajasowMaster = otherData.rajasowMaster;
		const prevDataParsed = getJsonData(prevData);
		const floorArray = new FloorBlockArray(
			needToCalculateAnyaAmt && permitData.floor[0].floorUnit !== 'FEET' ? converToFeet(permitData.floor) : permitData.floor
		);
		const formattedFloors = floorArray.getFormattedFloors();

		initVal = getJsonData(prevData);
		if (isEmpty(initVal)) {
			initVal = {};

			initVal.purposeOfConstruction = 'आवासिय क्षेत्र';
			initVal.details = [];
			floorArray
				.getFloorsWithUnit()
				.forEach((row, index) => (initVal.details[index] = getInitDetails(row, rajasowMaster, permitData.constructionType)));
			try {
				const detailsTotal = initVal.details.reduce((acc, row) => (acc += row.drAmount), 0);
				
				const batabaranDastur = getBatabaranDastur(detailsTotal);
				const sarasaphaiDastur = getSarsaphaiDastur(detailsTotal);
			
				initVal.sarasaphaiDastur = sarasaphaiDastur

				// initVal.dharautiAmt = round(detailsTotal / 2);
				initVal.dharautiAmt = 0;
				initVal.totalAmtBeforeDharauti = detailsTotal;
				initVal.batabaranDasturAmt = getBatabaranDastur(detailsTotal);
				initVal.sarsaphaiDasturAmt = getSarsaphaiDastur(detailsTotal);
				initVal.detailsTotal = detailsTotal;
				initVal.jaribanaDasturAmt = 0;
				if (needToCalculateAnyaAmt) {
					initVal.anyeAmt = initVal.details && round(initVal.details.reduce((acc, rw) => acc + getFormattedNumber(rw.dArea), 0) * 0.11);
				}
				if (isMechi) {
					initVal.applicationAmt = 750;
					initVal.formAmt = 10;
					initVal.aminAmt = 300;
				}
				initVal.totalAmt = isKamalamai
			? round(detailsTotal + 
					getFormattedNumber(initVal.anyeAmt) +
					getFormattedNumber(initVal.jaribanaDasturAmt) +
					getFormattedNumber(initVal.applicationAmt) +
					getFormattedNumber(initVal.formAmt) +
					getFormattedNumber(initVal.aminAmt)
				)
			: isMechi || isBirtamod || isInaruwa || isKankai
			? round(
				detailsTotal +
						getFormattedNumber(initVal.anyeAmt) +
						getFormattedNumber(initVal.applicationAmt) +
						getFormattedNumber(initVal.formAmt) +
						getFormattedNumber(initVal.aminAmt)
			)
			: round(detailsTotal + detailsTotal / 2);
			
			
			initVal.sarsaphaiDasturAmt = detailsTotal / 10;
			initVal.batabaranDasturAmt = (getFormattedNumber(initVal.totalAmt) + getFormattedNumber(initVal.sarsaphaiDasturAmt)) / 100;
			if(!isKamalamai){
				initVal.sarsaphaiDasturAmt = 0;
				initVal.batabaranDasturAmt = 0;
			}
			
			// initVal.dharautiAmt = round(getFormattedNumber(initVal.totalAmt) / 2);
			initVal.dharautiAmt = 0;
			initVal.totalAmt = round(getFormattedNumber(initVal.totalAmt) + getFormattedNumber(initVal.sarsaphaiDasturAmt) + getFormattedNumber(initVal.batabaranDasturAmt));

					
			} catch {
				initVal.totalAmt = 0;
				initVal.dharautiAmt = 0;
				initVal.totalAmtBeforeDharauti = 0;
				initVal.batabaranDasturAmt = 0;
				initVal.sarsaphaiDasturAmt = 0;
				initVal.sarasaphaiDastur = 0;
				initVal.detailsTotal = 0;
				initVal.jaribanaDasturAmt = 0;
			}
		}else{
			const detailsTotal = initVal.details.reduce((acc, row) => (acc += row.drAmount), 0);
			const sarasaphaiDastur = getSarsaphaiDastur(detailsTotal);
			if (needToCalculateAnyaAmt) {
				initVal.anyeAmt = initVal.details && round(initVal.details.reduce((acc, rw) => acc + getFormattedNumber(rw.dArea), 0) * 0.11);
			}
			initVal.totalAmt = isKamalamai
			? round(detailsTotal + 
					getFormattedNumber(initVal.anyeAmt) +
					getFormattedNumber(initVal.jaribanaDasturAmt) +
					getFormattedNumber(initVal.applicationAmt) +
					getFormattedNumber(initVal.formAmt) +
					getFormattedNumber(initVal.aminAmt)
				)
			: isMechi || isBirtamod || isInaruwa || isKankai
			? round(
				detailsTotal +
						getFormattedNumber(initVal.anyeAmt) +
						getFormattedNumber(initVal.applicationAmt) +
						getFormattedNumber(initVal.formAmt) +
						getFormattedNumber(initVal.aminAmt)
			)
			: round(detailsTotal + detailsTotal / 2);
			
			
			initVal.sarsaphaiDasturAmt = detailsTotal / 10;
			initVal.batabaranDasturAmt = (getFormattedNumber(initVal.totalAmt) + getFormattedNumber(initVal.sarsaphaiDasturAmt)) / 100;
			if(!isKamalamai){
				initVal.sarsaphaiDasturAmt = 0;
				initVal.batabaranDasturAmt = 0;
			}

			initVal.dharautiAmt = round(initVal.totalAmt / 2);
			initVal.totalAmt = round(getFormattedNumber(initVal.totalAmt) + getFormattedNumber(initVal.sarsaphaiDasturAmt) + getFormattedNumber(initVal.batabaranDasturAmt));


		}

		initVal.details &&
			initVal.details.forEach((row, index) => {
				if (isEmpty(initVal.hasJasta)) {
					initVal.hasJasta = [];
				}

				initVal.hasJasta[index] = !!(row.jrAmount || row.jArea);
			});

		initVal.details = initVal.details.sort((a, b) =>
			a.block !== undefined && b.block !== undefined ? (a.block < b.block ? -1 : 1) : a.floor < b.floor ? -1 : 1
		);
		// Road set back
		const roadSetbackMaster = this.props.otherData.roadSetBack;
		let roadSetBackOption = [
			{
				value: mapTech_data.mapTech_23.defaultRoadOption,
				text: mapTech_data.mapTech_23.defaultRoadOption,
			},
		];
		roadSetbackMaster.forEach((row) => row.status === 'Y' && roadSetBackOption.push({ value: row.roadName, text: row.roadName }));

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					unnamedUnit: DEFAULT_UNIT_LENGTH,
					jAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					dAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					// allowableHeightUnit: DEFAULT_UNIT_LENGTH,
					// constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					// floorlengthUnit: DEFAULT_UNIT_LENGTH,
					// floorWidthUnit: DEFAULT_UNIT_LENGTH,
					// floorareaUnit: DEFAULT_UNIT_LENGTH,
					floorUnit: floorArray.getFloorUnit(),
					discountPercent: isKankai ? 5 : 0,
					roadName: roadSetBackOption[0].value,
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['floor', 'buildingJoinRoad'] },
			{
				obj: {
					...floorArray.getInitialValue(null, 'constructionHeight', floorArray.getSumOfHeights()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width'], formattedFloors),
				},
				reqFields: [],
			},
			{ obj: initVal, reqFields: [] }
		);

		const isPuranoGhar = permitData.constructionType === ConstructionTypeValue.PURANO_GHAR;
		// /**
		//  * @todo remove later
		//  */
		// if (isEmpty(initialValues.jAreaUnit)) initialValues.jAreaUnit = DEFAULT_UNIT_AREA;
		// if (isEmpty(initialValues.dAreaUnit)) initialValues.dAreaUnit = DEFAULT_UNIT_AREA;

		this.state = {
			floorOptions: isEmpty(prevDataParsed) ? additionalFloorOptions : getFloorOptionsDy(prevDataParsed.details),
			lastSelectedFloor: '',
			selectedFlorr: [],
			floorArray,
			blocks: floorArray.getBlocks(),
			formattedFloors,
			initialValues,
			roadSetbackMaster,
			roadSetBackOption,
			rajasowMaster,
			constructionType: permitData.constructionType,
			schema: getRajasowSchema(isKankai, floorArray.getFloorSchema, isPuranoGhar),
		};
	}

	getJastaRate = (floor, type) => {
		const typeValue = type === jTypeOptions[0].value ? 'J' : 'O';
		const currentJastaRow = this.state.rajasowMaster.find(
			(mRow) => mRow.floor === floor && mRow.floorType === typeValue && mRow.constructionType === String(this.state.constructionType)
		);
		if (currentJastaRow) return currentJastaRow.dasturRate;
		else return 0;
	};

	render() {
		const { floorArray, blocks, formattedFloors, initialValues, roadSetbackMaster, schema } = this.state;
		const { permitData, prevData, hasDeletePermission, isSaveDisabled, hasSavePermission, formUrl } = this.props;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);

						values.applicationNo = permitData.applicantNo;
						values.details = values.details.filter((row) => !isEmpty(row));
						values.details &&
							values.details.forEach((row, index) => {
								if (row) {
									initDetails(row);
									row.totalAmount = calculateTotal(row.drAmount, row.depositAmount, row.jrAmount);
									if (row.floorText) delete row.floorText;
								} else {
									// values.details[index] = {};
									// values.details[index].dArea = 0;
									// values.details[index].drAmount = 0;
									// values.details[index].drRate = 0;
								}
							});

						values.error && delete values.error;

						try {
							await this.props.postAction(api.rajaswaEntry, values, true);

							// console.log('response -- after post', response);
							window.scrollTo(0, 0);
							actions.setSubmitting(false);
							// console.log(this.props);

							if (this.props.success && this.props.success.success) {
								// showToast("Your data has been successfully");
								// this.props.parentProps.history.push(
								//   getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, isSubmitting, handleChange, values, setFieldValue, errors, validateForm }) => {
						const hasDhrt = hasDharauti(values.details);
						const dharautiClass = hasDhrt ? undefined : 'remove-on-print';
						return (
							<Form loading={isSubmitting}>
								<div ref={this.props.setRef} className="print-small-font">
									<SectionHeader>
										<h2 className="top-margin">{rajaswoForm.heading}</h2>
									</SectionHeader>
									<div className="rajaswoForm-wrap">
										<br />
										{rajaswoForm.rajaswaFirstPara.content_line_1}
										<Select
											placeholder="आवासिय क्षेत्र"
											options={Opt}
											name="purposeOfConstruction"
											value={values.purposeOfConstruction}
											onChange={(e, { value }) => {
												setFieldValue('purposeOfConstruction', value);
											}}
										/>
										<BlockComponents floorArray={floorArray} blocks={blocks}>
											{(block) => {
												const allowableHeightField = floorArray.getReducedFieldName('allowableHeight', block);
												const constructionHeightField = floorArray.getReducedFieldName('constructionHeight', block);
												return (
													<>
														:
														<DashedLengthInputWithRelatedUnits
															name={allowableHeightField}
															unitName="floorUnit"
															relatedFields={[
																...floorArray.getAllReducedBlockFields(
																	['constructionHeight', 'allowableHeight'],
																	allowableHeightField
																),
																...floorArray.getAllFields(),
															]}
															label={rajaswoForm.rajaswaFirstPara.content_line_2}
														/>
														<DashedLengthInputWithRelatedUnits
															name={constructionHeightField}
															unitName="floorUnit"
															relatedFields={[
																...floorArray.getAllReducedBlockFields(
																	['constructionHeight', 'allowableHeight'],
																	constructionHeightField
																),
																...floorArray.getAllFields(),
															]}
															label={rajaswoForm.rajaswaFirstPara.content_line_3}
														/>
													</>
												);
											}}
										</BlockComponents>
										<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
											{(floorObj) => (
												<>
													{floorObj.map((floor, index) => (
														<div key={index}>
															{rajaswoForm.rajaswaFirstPara.prefix} {floor.nepaliName}{' '}
															<DashedLengthInputWithRelatedUnitsAndCalc
																name={floor.fieldName('floor', 'length')}
																unitName="floorUnit"
																relatedFields={[
																	...floorArray.getAllFields(`floor.${index}.length`),
																	...floorArray.getAllReducedBlockFields(['constructionHeight', 'allowableHeight']),
																]}
																otherField={floor.fieldName('floor', 'width')}
																areaField={floor.fieldName('floor', 'area')}
																label={rajaswoForm.rajaswaFirstPara.length}
															/>
															<DashedLengthInputWithRelatedUnitsAndCalc
																name={floor.fieldName('floor', 'width')}
																unitName="floorUnit"
																relatedFields={[
																	...floorArray.getAllFields(`floor.${index}.width`),
																	...floorArray.getAllReducedBlockFields(['constructionHeight', 'allowableHeight']),
																]}
																otherField={floor.fieldName('floor', 'length')}
																areaField={floor.fieldName('floor', 'area')}
																label={rajaswoForm.rajaswaFirstPara.width}
															/>
															<DashedAreaInputWithRelatedUnits
																name={floor.fieldName('floor', 'area')}
																unitName="floorUnit"
																relatedFields={[
																	...floorArray.getAllFields(`floor.${index}.area`),
																	...floorArray.getAllReducedBlockFields(['constructionHeight', 'allowableHeight']),
																]}
																label={rajaswoForm.rajaswaFirstPara.area}
																squareOptions={squareUnitOptions}
															/>
														</div>
													))}
													<br />
												</>
											)}
										</BlockFloorComponents>
										{/* <br /> */}
										<SectionHeader>
											<h4 className="underline">{rajaswoForm.rajaswo_table_1.header}</h4>
										</SectionHeader>
										<Table celled size="small" compact="very" className="no-page-break-inside">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width="2">{rajaswoForm.rajaswo_table_1.table_head_1}</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_2}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_3}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_4}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_5} {'( '}
														<Dropdown
															name="sadakAdhikarUnit"
															onChange={(e, { value }) => {
																// surroundingMappingFlat.forEach((side, index) => {
																const reqRoad = roadSetbackMaster.find(
																	(road) => road.roadName === getIn(values, `roadName`)
																);
																if (reqRoad) {
																	if (value === 'METRE') {
																		setFieldValue(`requiredDistance`, reqRoad.setBackMeter);
																	} else {
																		setFieldValue(`requiredDistance`, reqRoad.setBackFoot);
																	}
																}
																// });

																setFieldValue('sadakAdhikarUnit', value);
															}}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
														{' ) '}
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_6}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>{rajaswoForm.rajaswo_table_1.table_head_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell>
														{/* <Select
															options={roadSetBackOption}
															name={`roadName`}
															placeholder="Select an option"
															onChange={(e, { value }) => {
																setFieldValue(`roadName`, value);
																console.log(value);
																const reqRoad = roadSetbackMaster.find((road) => road.roadName === value);
																if (reqRoad) {
																	if (values.sadakAdhikarUnit === 'METRE') {
																		setFieldValue(`sadakAdhikarKshytra`, reqRoad.setBackMeter);

																		setFieldValue(`requiredDistance`, reqRoad.setBackMeter);
																	} else {
																		setFieldValue(`sadakAdhikarKshytra`, reqRoad.setBackFoot);

																		setFieldValue(`requiredDistance`, reqRoad.setBackFoot);
																	}
																} else {
																	setFieldValue(`sadakAdhikarKshytra`, '');

																	setFieldValue(`requiredDistance`, '');
																}
															}}
															value={getIn(values, `roadName`)}
															error={!!errors.roadName}
														/> */}
														<EbpsForm name="roadName" value={values.roadName} setFieldValue={setFieldValue} />
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedMapdanda"
															value={values.namedMapdanda}
															onChange={handleChange}
															error={errors.namedMapdanda}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedMeasurement"
															value={values.namedMeasurement}
															onChange={handleChange}
															error={errors.namedMeasurement}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedAdhikar"
															value={values.namedAdhikar}
															onChange={handleChange}
															error={errors.namedAdhikar}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														{/* {getIn(values, 'roadName') === mapTech_data.mapTech_23.defaultRoadOption ? ( */}
															<EbpsNumberForm
																name="requiredDistance"
																value={values.requiredDistance}
																onChange={handleChange}
																error={errors.requiredDistance}
																isTable={true}
															/>
														{/* ) : (
															getIn(values, `requiredDistance`) && `${getIn(values, `requiredDistance`)} `
														)} */}
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name={`sadakAdhikarKshytra`}
															value={getIn(values, `sadakAdhikarKshytra`)}
															onChange={handleChange}
															error={getIn(errors, `sadakAdhikarKshytra`)}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsForm name="namedRemarks" value={values.namedRemarks} setFieldValue={setFieldValue} />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>{' '}
										<SectionHeader>
											<h4 className="underline">{rajaswoForm.rajaswo_table_2.header}</h4>
										</SectionHeader>
										<Table celled size="small" compact="very" className="no-page-break-inside">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width="2">{rajaswoForm.rajaswo_table_2.table_head_1}</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_2.table_head_2}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_3}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_4}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_2.table_head_3}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_6}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>{rajaswoForm.rajaswo_table_1.table_head_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell className="top-margin-print-cell">
														<EbpsForm name="unnamedSadak" value={values.unnamedSadak} setFieldValue={setFieldValue} />
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedMapdanda"
															value={values.unnamedMapdanda}
															onChange={handleChange}
															error={errors.unnamedMapdanda}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedMeasurement"
															value={values.unnamedMeasurement}
															onChange={handleChange}
															error={errors.unnamedMeasurement}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedAdhikar"
															value={values.unnamedAdhikar}
															onChange={handleChange}
															error={errors.unnamedAdhikar}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedBorder"
															value={values.unnamedBorder}
															onChange={handleChange}
															error={errors.unnamedBorder}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedDistance"
															value={values.unnamedDistance}
															onChange={handleChange}
															error={errors.unnamedDistance}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsForm name="unnamedRemarks" value={values.unnamedRemarks} setFieldValue={setFieldValue} />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<div>
											<SectionHeader>
												<h4 className="underline">{rajaswoForm.table_heading1}</h4>
											</SectionHeader>
											<Table celled structured size="small" compact="very">
												<Table.Header>
													<HeaderFirstRow dharautiClass={dharautiClass} />
													<Table.Row>
														{/* Dhalan */}
														<Table.HeaderCell rowSpan="2" width={4}>
															{rajaswoForm.table_head.thead_2}
															{/* {' ( '} */}
															{/* <Dropdown
																// defaultValue="वर्ग मिटर"
																options={areaUnitOptions}
																name="dAreaUnit"
																onChange={(e, { value }) => setFieldValue('dAreaUnit', value)}
																value={values['dAreaUnit']}
															/> */}
															{/* {' )'} */}
															<RajasowUnitDropdownWithRelatedFields
																options={areaUnitOptions}
																unitName="dAreaUnit"
																areaFields={[
																	'drAmount',
																	'jrAmount',
																	'depositAmount',
																	'jDepositAmount',
																	'detailsTotal',
																]}
																relatedFields={[
																	'detailsTotal',
																	...floorArray.getAllCustomsFields('', 'details', [
																		'dArea',
																		'jArea',
																		'drAmount',
																		'jrAmount',
																		'depositAmount',
																		'jDepositAmount',
																	]),
																]}
															/>
														</Table.HeaderCell>
														<Table.HeaderCell className={dharautiClass} colSpan="2">
															{rajaswoForm.table_head.thead_3}
														</Table.HeaderCell>
														{/* Other */}
														{/* <Table.HeaderCell rowSpan="2" width={3}>
														{rajaswoForm.table_head.thead_4} {' ( '}
														<Dropdown
															// defaultValue="वर्ग मिटर"
															options={areaOptions}
															name="jAreaUnit"
															onChange={(e, { value }) =>
																setFieldValue('jAreaUnit', value)
															}
															value={values['jAreaUnit']}
														/>
														{' )'}
													</Table.HeaderCell> */}
														{/* <Table.HeaderCell colSpan="2">
														{rajaswoForm.table_head.thead_5}
													</Table.HeaderCell> */}
														{/* Dhazrauti */}
													</Table.Row>
													<HeaderThirdRow dharautiClass={dharautiClass} />
												</Table.Header>
												<Table.Body>
													{initialValues.details
														// .filter(rw => rw.floor <= 2)
														.map((row, index) => {
															const isHeightRelevantFloor = row.floor < 11;
															return [
																<Table.Row key={index}>
																	<Table.Cell>
																		<div>
																			{floorMappingFlat.find((fl) => row.floor === fl.floor).value}{' '}
																			{row.block && `${floorData.block} ${row.block}`}
																		</div>
																		{row.floor > 0 && (
																			<div
																				className="ui checkbox"
																				style={{
																					marginRight: 10,
																				}}
																			>
																				<input
																					type="checkbox"
																					name={`hasJasta.${index}`}
																					value={getIn(values, `hasJasta.${index}`)}
																					defaultChecked={getIn(values, `hasJasta.${index}`)}
																					onChange={(e) => {
																						setFieldValue(
																							`details.${index}.jType`,
																							jTypeOptions[0].value
																						);
																						handleChange(e);
																					}}
																				/>
																				<label>{rajaswoForm.table_jasta}</label>
																			</div>
																		)}
																	</Table.Cell>
																	{/* Dhalan */}
																	<Table.Cell>
																		<RajasowTableInput
																			name={`details.${index}.dArea`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.drRate`}
																			amountFieldName={`details.${index}.drAmount`}
																			error={getIn(errors, `details.${index}.dArea`)}
																			floorFieldName={`details.${index}.floor`}
																			setFieldValue={setFieldValue}
																			floor={row.floor}
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<RajasowRateInput
																			name={`details.${index}.drRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.dArea`}
																			amountFieldName={`details.${index}.drAmount`}
																			error={getIn(errors, `details.${index}.drRate`)}
																			setFieldValue={setFieldValue}
																			isHeightRelevantFloor={isHeightRelevantFloor}
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInput
																			readOnly={true}
																			placeholder="0.0"
																			name={`details.${index}.drAmount`}
																			value={getIn(values, `details.${index}.drAmount`)}
																			error={getIn(errors, `details.${index}.drAmount`)}
																		/>
																	</Table.Cell>

																	{/* Dharauti */}
																	{/* <Table.Cell className={dharautiClass}>
																		<RajasowTableInput
																			name={`details.${index}.depositRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.dArea`}
																			amountFieldName={`details.${index}.depositAmount`}
																			error={getIn(errors, `details.${index}.depositRate`)}
																			setFieldValue={setFieldValue}
																		/>
																	</Table.Cell>
																	<Table.Cell className={dharautiClass}>
																		<TableInput
																			readOnly={true}
																			placeholder="0.00"
																			name={`details.${index}.depositAmount`}
																			value={getIn(values, `details.${index}.depositAmount`)}
																			error={getIn(errors, `details.${index}.depositAmount`)}
																			// onChange={handleChange}
																		/>
																	</Table.Cell> */}
																</Table.Row>,
																getIn(values, `hasJasta.${index}`) && (
																	<Table.Row key={`jasta-${index}`}>
																		<Table.Cell>
																			<Form.Field inline>
																				<span>
																					{floorMappingFlat.find((fl) => row.floor === fl.floor).value}{' '}
																					{row.block && `${floorData.block} ${row.block}`}
																				</span>{' '}
																				{/* <Label basic> */}
																				<Select
																					className="fit-content-select"
																					// defaultValue={jTypeOptions[0].text}
																					options={jTypeOptions}
																					name={`details.${index}.jType`}
																					onChange={(e, { value }) => {
																						const jastaRate = this.getJastaRate(row.floor, value);
																						const jArea = getIn(values, `details.${index}.jArea`) || 0;
																						setFieldValue(`details.${index}.jType`, value);
																						setFieldValue(`details.${index}.jrRate`, jastaRate);
																						setFieldValue(
																							`details.${index}.jrAmount`,
																							round(jArea * jastaRate)
																						);
																					}}
																					value={getIn(values, `details.${index}.jType`)}
																				/>
																				{/* </Label> */}
																			</Form.Field>
																		</Table.Cell>
																		{/* Other */}
																		<Table.Cell>
																			<Form.Field>
																				{/* <Input labelPosition="right"> */}
																				{/* <TableInput
																				name={`details.${index}.jArea`}
																				value={getIn(values, `details.${index}.jArea`)}
																				onChange={e => {
																					setFieldValue(
																						`details.${index}.jArea`,
																						e.target.value
																					);
																					if (
																						!values[`details.${index}.floor`]
																					) {
																						setFieldValue(
																							`details.${index}.floor`,
																							row.floor
																						);
																					}
																					setFieldValue(
																						`details.${index}.jrAmount`,
																						calculateAmount(
																							'jrRate',
																							e.target.value,
																							index,
																							values,
																							'details'
																						),
																						false
																					);
																				}}
																				error={getIn(errors, `details.${index}.jArea`)}
																			/> */}
																				<RajasowTableInput
																					name={`details.${index}.jArea`}
																					index={index}
																					values={values}
																					mulFieldName={`details.${index}.jrRate`}
																					amountFieldName={`details.${index}.jrAmount`}
																					error={getIn(errors, `details.${index}.jArea`)}
																					floorFieldName={`details.${index}.floor`}
																					setFieldValue={setFieldValue}
																					floor={row.floor}
																				/>
																				{/* </Input> */}
																			</Form.Field>
																		</Table.Cell>
																		<Table.Cell>
																			{/* <TableInput
																		placeholder="0.00"
																		name={`details.${index}.jrRate`}
																		value={getIn(values, `details.${index}.jrRate`)}
																		onChange={e => {
																			setFieldValue(
																				`details.${index}.jrRate`,
																				e.target.value
																			);
																			setFieldValue(
																				`details.${index}.jrAmount`,
																				calculateAmount(
																					'jArea',
																					e.target.value,
																					index,
																					values,
																					'details'
																				),
																				false
																			);
																		}}
																		error={getIn(errors, `details.${index}.jrRate`)}

																	/> */}
																			<RajasowRateInput
																				name={`details.${index}.jrRate`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jArea`}
																				amountFieldName={`details.${index}.jrAmount`}
																				error={getIn(errors, `details.${index}.jrRate`)}
																				setFieldValue={setFieldValue}
																				isHeightRelevantFloor={isHeightRelevantFloor}
																			/>
																		</Table.Cell>
																		<Table.Cell>
																			<TableInput
																				readOnly={true}
																				placeholder="0.00"
																				name={`details.${index}.jrAmount`}
																				error={getIn(errors, `details.${index}.jrAmount`)}
																				value={getIn(values, `details.${index}.jrAmount`)}
																				// onChange={handleChange}
																			/>
																		</Table.Cell>

																		{/* Dharauti */}
																		{/* <Table.Cell className={dharautiClass}>
																			<RajasowTableInput
																				name={`details.${index}.jDepositRate`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jArea`}
																				amountFieldName={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositRate`)}
																				setFieldValue={setFieldValue}
																			/>
																		</Table.Cell>
																		<Table.Cell className={dharautiClass}>
																			<TableInput
																				placeholder="0.00"
																				name={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositAmount`)}
																				value={getIn(values, `details.${index}.jDepositAmount`)}
																				onChange={handleChange}
																			/>
																		</Table.Cell> */}
																	</Table.Row>
																),
															];
														})}
													{/* Dynamic additional rows */}
													{/* ------------------------------------ */}

													<Table.Row>
														<Table.Cell className="print-col-span" data-colspan={hasDhrt ? '3' : '3'} colSpan={'3'}>
															{rajaswoForm.table_total}
														</Table.Cell>
														<Table.Cell>
															<TableInput
																placeholder="0.00"
																name="detailsTotal"
																onBlur={() => {
																	if (values.details) {
																		const amt = values.details.reduce((amount, next) => {
																			if (next) {
																				return (
																					parseFloat(amount) +
																					calculateTotal(
																						next.drAmount,
																						next.jrAmount,
																						next.depositAmount,
																						next.jDepositAmount
																					)
																				);
																			} else {
																				return 0.0;
																			}
																		}, 0.0);
																		setFieldValue('detailsTotal', round(amt));
																	}
																}}
																value={values.detailsTotal}
																error={errors.detailsTotal}
																onChange={handleChange}
															/>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
										</div>
										<Table celled structured size="small" compact="very">
											<Table.Body>
												<Table.Row>
													<Table.Cell width={4}>{rajaswoForm.table_foot.Child_1}</Table.Cell>
													<Table.Cell width={6}>
														<RajasowTable2Input
															name="formAmt"
															values={values}
															setFieldValue={setFieldValue}
															error={errors.formAmt}
														/>
													</Table.Cell>
													<Table.Cell rowSpan={isKankai ? '9' : '8'}>{rajaswoForm.table_foot.Child_6}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_2}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input
															name="applicationAmt"
															values={values}
															setFieldValue={setFieldValue}
															error={errors.applicationAmt}
														/>
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_3}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input
															name="aminAmt"
															values={values}
															setFieldValue={setFieldValue}
															error={errors.aminAmt}
														/>
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{isKageyshowri ? rajaswoForm.table_foot.child_wildcard : rajaswoForm.table_foot.Child_4}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input
															name="anyeAmt"
															values={values}
															setFieldValue={setFieldValue}
															error={errors.anyeAmt}
														/>
													</Table.Cell>
												</Table.Row>
												{!isBirtamod && !isMechi && !isInaruwa && !isKankai && (
													<Table.Row>
														<Table.Cell>{rajaswoForm.table_foot.jaribanaDastur}</Table.Cell>
														<Table.Cell>
															<RajasowTable2Input
																name="jaribanaDasturAmt"
																values={values}
																setFieldValue={setFieldValue}
																error={errors.jaribanaDasturAmt}
															/>
														</Table.Cell>
													</Table.Row>
												)}
												{isKamalamai && (
													<>
														<Table.Row>
															<Table.Cell>{rajaswoForm.table_foot.sarsaphaiDastur}</Table.Cell>
															<Table.Cell>
																<RajasowTable2Input
																	name="sarsaphaiDasturAmt"
																	values={values}
																	setFieldValue={setFieldValue}
																	error={errors.applicationAmt}
																/>
															</Table.Cell>
														</Table.Row>
														<Table.Row>
															<Table.Cell>{rajaswoForm.table_foot.batabaranDastur}</Table.Cell>
															<Table.Cell>
																<RajasowTable2Input
																	name="batabaranDasturAmt"
																	values={values}
																	setFieldValue={setFieldValue}
																	error={errors.applicationAmt}
																/>
															</Table.Cell>
														</Table.Row>
													</>
												)}
												{isKankai && (
													<Table.Row>
														<Table.Cell>{rajaswoForm.table_foot.discount_row}</Table.Cell>
														<Table.Cell>
															<Table basic="very" compact="very">
																<Table.Header>
																	<Table.Row>
																		<Table.HeaderCell verticalAlign="bottom">
																			{rajaswoForm.table_foot.discount_percent}
																		</Table.HeaderCell>
																		<Table.HeaderCell verticalAlign="bottom">
																			{rajaswoForm.table_foot.discount_amount}
																		</Table.HeaderCell>
																	</Table.Row>
																</Table.Header>
																<Table.Body>
																	<Table.Row>
																		<Table.Cell>
																			<RajasowDiscountPercent
																				name="discountPercent"
																				values={values}
																				setFieldValue={setFieldValue}
																				error={errors.discountPercent}
																			/>
																		</Table.Cell>
																		<Table.Cell>
																			{' '}
																			<TableInput
																				placeholder="0.00"
																				name="discountAmount"
																				value={values.discountAmount}
																			/>
																		</Table.Cell>
																	</Table.Row>
																</Table.Body>
															</Table>
														</Table.Cell>
													</Table.Row>
												)}
												{/* These clients dont need dharauti */}
												{!isKamalamai && !isBirtamod && !isMechi && !isInaruwa && !isKankai && (
													<>
														<Table.Row>
															<Table.Cell>{rajaswoForm.table_foot.totalAmtWithDharauti}</Table.Cell>
															<Table.Cell>
																<TableInput
																	readOnly={true}
																	placeholder="0.00"
																	name="totalAmtBeforeDharauti"
																	value={values.totalAmtBeforeDharauti}
																	error={errors.totalAmtBeforeDharauti}
																/>
															</Table.Cell>
														</Table.Row>
														{isKageyshowri ? null : (

															<Table.Row>
																<Table.Cell>{rajaswoForm.table_foot.dharautiAmt}</Table.Cell>
																<Table.Cell>
																	<TableInput
																		readOnly={true}
																		placeholder="0.00"
																		name="dharautiAmt"
																		value={values.dharautiAmt}
																		error={errors.dharautiAmt}
																	/>
																</Table.Cell>
															</Table.Row>
														)}
													</>
												)}
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.totalAmt}</Table.Cell>
													<Table.Cell>
														<TableInput
															readOnly={true}
															placeholder="0.00"
															name="totalAmt"
															value={values.totalAmt}
															error={errors.totalAmt}
														/>
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										{values.totalAmt && values.totalAmt > 0 ? (
											<div>
												{rajaswoForm.formSecond.amountInWordsTotal}: {NepaliNumberToWord.getNepaliWord(values.totalAmt)}
											</div>
										) : null}
									</div>
								</div>
								<br />
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}
const RajaswoDetailView = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.rajaswaEntry, 'rajaswoEntry').setForm().getParams(),
			new ApiParam(api.roadSetBack, 'roadSetBack').setUtility().getParams(),
			new ApiParam(api.rajasowSetup, 'rajasowMaster')
				.setUtility()
				.setConcatToApi({ prefix: '?wardNo=', fieldName: 'newWardNo', needsTranslation: true })
				.getParams(),
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		needsRajaswo={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param4: ['15dayspecial'],
			param5: ['removeOnPrint'],
		}}
		render={(props) => <RajaswoDetailViewComponent {...props} parentProps={parentProps} />}
	/>
);

const HeaderFirstRow = React.memo(({ dharautiClass }) => (
	<Table.Row>
		{/* sn */}
		<Table.HeaderCell rowSpan="3">{rajaswoForm.table_head.thead_1}</Table.HeaderCell>
		<Table.HeaderCell colSpan="3">{rajaswoForm.table_head.thead_type.dhalan}</Table.HeaderCell>
		{/* <Table.HeaderCell colSpan="3" width={3}>
			{rajaswoForm.table_head.thead_type.other}
		</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass} rowSpan="2" colSpan="2">
			{rajaswoForm.table_head.thead_6}
		</Table.HeaderCell> */}
	</Table.Row>
));

const HeaderThirdRow = React.memo(({ dharautiClass }) => (
	<Table.Row>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_1}</Table.HeaderCell>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_2}</Table.HeaderCell>
		{/* <Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_1}
		</Table.HeaderCell>
		<Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_2}
		</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_1}</Table.HeaderCell> */}
		{/* <Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_2}</Table.HeaderCell> */}
	</Table.Row>
));

export default RajaswoDetailView;
