import React, { Component } from 'react';
import { aminSthalgatPratibedan } from '../../../utils/data/AminKoSthalgatData';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import api from '../../../utils/api';
import { showToast, getUserRole } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import * as Yup from 'yup';
import { validateNotReqdPhone, validateNullableNepaliDate } from '../../../utils/validationUtils';
import { EbpsTextArea } from '../../shared/EbpsForm';
import { LetterHeadFlex } from '../../shared/LetterHead';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { UserType } from '../../../utils/userTypeUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { getSaveByUserDetails } from '../../../utils/formUtils';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { DetailedSignature } from './formComponents/FooterSignature';
const data = aminSthalgatPratibedan;
const designation = aminSthalgatPratibedan.signature;

const schema = Yup.object().shape({
	aminPhone: validateNotReqdPhone,
	aminDate: validateNullableNepaliDate,
});
class AminSthalgatPratibedanComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, userData, enterByUser } = this.props;

		let aminInfo = {
			aminName: '',
			aminDesignation: '',
		};

		if (getUserRole() === UserType.AMIN) {
			const { subName, subDesignation, subSignature } = getSaveByUserDetails(enterByUser, userData);
			aminInfo.aminName = subName;
			aminInfo.aminPhone = userData.info.mobile;
			aminInfo.aminDesignation = subDesignation;
			aminInfo.aminSignature = subSignature;
		}

		const json_data = getJsonData(prevData);

		const initialValues = prepareMultiInitialValues(
			{ obj: { aminKoPratibedan: '', aminDate: getCurrentDate(true), sitePlanLocation: '', aminPhone: '' }, reqFields: [] },
			{ obj: aminInfo, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = { initialValues };
	}
	render() {
		const { initialValues } = this.state;
		const {
			permitData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			userData: user_info,
			useSignatureImage,
		} = this.props;

		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						try {
							await this.props.postAction(api.aminKoSthalgatPratibedan, values, true);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={user_info} />
								<div className="section-header">
									<h2 className="underline">{data.tippani}</h2>
									<br />
									<h3 className="underline">{data.topic}</h3>
								</div>
								<br />
								<EbpsTextArea
									placeholder="निवेदन"
									name="aminKoPratibedan"
									setFieldValue={setFieldValue}
									onChange={handleChange}
									value={values.aminKoPratibedan}
								/>
								<br />
								<div className="flex-item-space-between">
									<div>
										{data.sitePlanLocation}:
										<DashedLangInput setFieldValue={setFieldValue} name="sitePlanLocation" value={values.sitePlanLocation} />
									</div>
									<FlexSingleRight>
										<div className="no-margin-field">
											<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors}>
												<DetailedSignature.Name name="aminName" label={designation.name} />
												<DetailedSignature.Designation name="aminDesignation" label={designation.designation} />
												<DetailedSignature.Name name="aminPhone" label={designation.phone} />
												<DetailedSignature.Date name="aminDate" />
												<DetailedSignature.Signature
													name="aminSignature"
													showSignature={useSignatureImage}
													label={designation.signature}
												/>
											</DetailedSignature>
										</div>
									</FlexSingleRight>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const AminSthalgatPratibedan = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.aminKoSthalgatPratibedan,
				objName: 'aminPratibedan',
				form: true,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
		}}
		useInnerRef={true}
		render={(props) => <AminSthalgatPratibedanComponent {...props} parentProps={parentProps} />}
	/>
);

export default AminSthalgatPratibedan;
