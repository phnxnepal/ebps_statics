import React, { Component } from 'react';
import { Table, TableCell } from 'semantic-ui-react';
import { floorDetailsData } from '../../../utils/data/mockLangFile';
import { isEmpty, getUnitValue } from '../../../utils/functionUtils';

// Redux
import { connect } from 'react-redux';
import { getPermitData } from '../../../store/actions/formActions';
import FallbackComponent from '../../shared/FallbackComponent';

const floor_details_data = floorDetailsData.floorDetailsDataTable;

class FloorDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSucess: false
        };
    }
    componentDidMount() {
        this.props.getPermitData();
    }

    render() {
        if (
            //   !isEmpty(this.props.formData) &&
            !isEmpty(this.props.permitData)
        ) {
            const permitData = this.props.permitData;
            return (
                <div className='floorDetails-Form'>
                    <Table celled compact collapsing>
                        <Table.Header>
                            <Table.Row>
                                {permitData.floor.length > 0 &&
                                    Object.keys(
                                        floor_details_data.floorDetailsDataTable_heading
                                    ).map((key, floorDetailIndex) =>
                                        floorDetailIndex === 0 ? (
                                            <Table.HeaderCell>
                                                {
                                                    floor_details_data
                                                        .floorDetailsDataTable_heading[
                                                    key
                                                    ]
                                                }
                                            </Table.HeaderCell>
                                        ) : (
                                                <Table.HeaderCell>
                                                    {
                                                        floor_details_data
                                                            .floorDetailsDataTable_heading[
                                                        key
                                                        ]
                                                    }
                                                    (
                                                {getUnitValue(
                                                        permitData.floor[0]
                                                            .floorUnit
                                                    )}
                                                    )
                                            </Table.HeaderCell>
                                            )
                                    )}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {permitData.floor.length > 0 &&
                                Object.keys(
                                    floor_details_data.floorDetailsDataTable_subheading
                                )
                                    .splice(0, permitData.floor.length)
                                    .map((key, floorDetailIndex) => {
                                        // buildingIndex++;
                                        return (
                                            <Table.Row>
                                                <TableCell>
                                                    {
                                                        floor_details_data
                                                            .floorDetailsDataTable_subheading[
                                                        key
                                                        ]
                                                    }
                                                </TableCell>
                                                <TableCell>
                                                    {/* <Input */}
                                                    {
                                                        permitData.floor[
                                                            floorDetailIndex
                                                        ].length
                                                    }
                                                    {/* onChange={props.handleChange} */}
                                                    {/* /> */}
                                                </TableCell>
                                                <TableCell>
                                                    {/* <Input */}
                                                    {
                                                        permitData.floor[
                                                            floorDetailIndex
                                                        ].width
                                                    }
                                                    {/* onChange={props.handleChange} */}
                                                    {/* /> */}
                                                </TableCell>
                                                <TableCell>
                                                    {/* <Input */}
                                                    {
                                                        permitData.floor[
                                                            floorDetailIndex
                                                        ].height
                                                    }
                                                    {/* onChange={props.handleChange} */}
                                                    {/* /> */}
                                                </TableCell>
                                                <TableCell>
                                                    {/* <Input */}
                                                    {
                                                        permitData.floor[
                                                            floorDetailIndex
                                                        ].area
                                                    }
                                                    {/* onChange={props.handleChange} */}
                                                    {/* /> */}
                                                </TableCell>
                                            </Table.Row>
                                        );
                                    })}
                        </Table.Body>
                    </Table>
                </div>
            );
        } else {
            return (
                <FallbackComponent
                    loading={this.props.loading}
                    errors={this.props.errors}
                />
            );
        }
    }
}

const mapDispatchToProps = {
    getPermitData
};
const mapStateToProps = state => {
    return {
        permitData: state.root.formData.permitData,
        errors: state.root.ui.errors,
        loading: state.root.ui.loading
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FloorDetails);
