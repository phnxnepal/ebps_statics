import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { CompactDashedLangDate } from '../../shared/DateField';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { SectionHeader } from '../../uiComponents/Headers';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { MyadTippaniData } from '../../../utils/data/MyadThapTippaniData';
import { LetterHead } from './../../shared/LetterHead';
import { PatraSankhya } from './formComponents/PatraSankhyaAndDate';
import { DashedLangDateField } from './../../shared/DateField';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { FlexSingleLeft } from './../../uiComponents/FlexDivs';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { showToast } from '../../shared/viewToast';

const myad = MyadTippaniData.myad_data;
const opt = [
    { key: 'ghar', value: 'घर', text: 'घर' },
    { key: 'parkhal', value: 'पर्खाल', text: 'पर्खाल' },
    { key: 'pala', value: 'पला', text: 'पला' },
];
const MyadTippaniSchema = Yup.object().shape(
    Object.assign({
        dates: validateNullableNepaliDate,
        // myadDate: validateNullableNepaliDate,
    })
);
class MyadTippani extends Component {
    constructor(props) {
        super(props);
        let initialValues;
        const { prevData } = props;
        const json_data = getJsonData(prevData);
        initialValues = prepareMultiInitialValues(
            //prev values
            { obj: json_data, reqFields: [] }
        );
        if (isStringEmpty(initialValues.dates)) {
            initialValues.dates = getCurrentDate(true);
        }
        this.state = { initialValues: { ghar: opt[0].value, passGhar: opt[0].value, ...initialValues } }
    }
    render() {
        const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
        const { initialValues } = this.state;
        const user_info = this.props.userData;
        return (
            <Formik
                initialValues={initialValues}
                validationSchema={MyadTippaniSchema}
                onSubmit={async (values, actions) => {
                    actions.setSubmitting(true);
                    values.applicationNo = permitData.applicationNo;
                    values.error && delete values.error;
                    try {
                        await this.props.postAction(`${api.MyadThapTippani}${permitData.applicationNo}`, values);
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        if (this.props.success && this.props.success.success) {
                            handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                        }
                    } catch (err) {
                        actions.setSubmitting(false);
                        window.scrollTo(0, 0);
                        showToast('Something went wrong !!');
                    }
                }}
            >
                {({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors }) => (
                    <Form loading={isSubmitting} className="ui form">
                        {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

                        <div ref={this.props.setRef}>
                            <LetterHead userInfo={user_info} />
                            <PatraSankhya setFieldValue={setFieldValue} errors={errors} values={values} />
                            <FlexSingleRight>
                                {`${myad.date}`}
                                <DashedLangDateField
                                    compact={true}
                                    name="dates"
                                    value={values.dates}
                                    error={errors.dates}
                                    setFieldValue={setFieldValue}
                                    inline={true}
                                />
                            </FlexSingleRight>
                            <SectionHeader>
                                <h3 className="normal underline">{myad.title}</h3>
                                <h3 className="normal underline end-section">{myad.subject}</h3>
                            </SectionHeader>
                            <FlexSingleLeft>
                                {`${myad.salutation}`}
                                <span className="ui input dashedForm-control" />
                            </FlexSingleLeft>
                            <br />
                            <div style={{ textAlign: 'justify ' }}>
                                <span className="indent-span">
                                    {myad.content.content_1}
                                </span>
                                {userData.organization.name}
                                {myad.content.content_2}
                                {permitData.newWardNo}
                                {myad.content.content_3}
                                {permitData.nibedakName}
                                {myad.content.content_4}
                                {userData.organization.name}
                                {myad.content.content_2}
                                {permitData.oldWardNo}
                                {myad.content.content_5}
                                {permitData.kittaNo}
                                {myad.content.content_6}
                                {permitData.landArea}{permitData.landAreaType}
                                {myad.date}
                                {permitData.applicantDateBS}{' '}
                                {/* <CompactDashedLangDate
                                    name="myadDate"
                                    setFieldValue={setFieldValue}
                                    value={values.myadDate}
                                    error={errors.myadDate}
                                /> */}
                                {myad.content.content_7}
                                <Select
                                    name="ghar"
                                    options={opt}
                                    onChange={(e, { value }) => {
                                        setFieldValue('ghar', value);
                                    }}
                                    value={values.ghar}
                                />
                                {myad.content.content_8}
                                <Select
                                    name="passGhar"
                                    options={opt}
                                    onChange={(e, { value }) => {
                                        setFieldValue('passGhar', value);
                                    }}
                                    value={values.passGhar}
                                />
                                {myad.content.content_9}
                                <DashedLangInput
                                    name="rakamRu"
                                    setFieldValue={setFieldValue}
                                    value={values.rakamRu}
                                    error={errors.rakamRu}
                                />
                                {myad.content.content_10}
                                <DashedLangInput
                                    name="ko"
                                    setFieldValue={setFieldValue}
                                    value={values.ko}
                                    error={errors.ko}
                                />
                                {myad.content.content_11}
                                <DashedLangInput
                                    name="dasturRu"
                                    setFieldValue={setFieldValue}
                                    value={values.dasturRu}
                                    error={errors.dasturRu}
                                />
                                {myad.content.content_12}
                            </div>
                        </div>
                        <SaveButtonValidation
                            errors={errors}
                            validateForm={validateForm}
                            formUrl={formUrl}
                            hasSavePermission={hasSavePermission}
                            hasDeletePermission={hasDeletePermission}
                            isSaveDisabled={isSaveDisabled}
                            prevData={checkError(prevData)}
                            handleSubmit={handleSubmit}
                        />
                    </Form>
                )}
            </Formik>
        );
    }
}

const MyadThapTippani = parentProps => (
    <FormContainerV2
        api={[
            {
                api: api.MyadThapTippani,
                objName: 'myadTippani',
                form: true,
            },
        ]}
        onBeforeGetContent={{
            ...PrintParams.INLINE_FIELD,
            ...PrintParams.TEXT_AREA,
            param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
            param7: ['getElementsByClassName', 'ui checkbox', 'label'],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
        }}
        prepareData={data => data}
        parentProps={parentProps}
        useInnerRef={true}
        render={props => <MyadTippani {...props} parentProps={parentProps} />}
    />
);
export default MyadThapTippani;
