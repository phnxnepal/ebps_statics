import React, { Component } from 'react';
import { Formik } from 'formik';
import { GharNaksha } from '../../../utils/data/mockLangFile';
import { DashedLangDateField } from '../../shared/DateField';
import { Form, Grid } from 'semantic-ui-react';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { showToast } from '../../../utils/functionUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import * as Yup from 'yup';
import { validateNepaliDate } from '../../../utils/validationUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';

const gnak = GharNaksha.GharNaksha_data;

const GharnaksakoSchema = Yup.object().shape(
	Object.assign({
		date: validateNepaliDate,
	})
);
class GharNaksakoKabuliyatnamaComponent extends Component {
	constructor(props) {
		super(props);

		const { otherData, prevData } = this.props;
		const jsonData = getJsonData(prevData);
		const gharNaksaSurjamin = getJsonData(otherData.gharNaksaSurjamin);

		let initialValues = prepareMultiInitialValues(
			{
				obj: gharNaksaSurjamin,
				reqFields: ['purbaSandhiyar', 'paschimSandhiyar', 'uttarSandhiyar', 'dakshinSandhiyar'],
			},
			{
				obj: jsonData,
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.date)) {
			initialValues.date = getCurrentDate(true);
		}
		this.state = { initialValues };
	}

	render() {
		const { initialValues } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div className="gharNakshaKabuliyetname-wrap">
				<Formik
					initialValues={initialValues}
					validationSchema={GharnaksakoSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						try {
							await this.props.postAction(`${api.gharNaksakoKabuliyatnama}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleChange, handleSubmit, setFieldValue, values, errors, validateForm }) => (
						// <FormWrapper erStatus={erStatus}>
						<Form loading={isSubmitting} className="NJ-right">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

							<div style={{ textAlign: 'justify' }} ref={this.props.setRef}>
								<div style={{ textAlign: 'center' }}>
									<h2>
										<span>{gnak.firstheading.heading_1}</span>
									</h2>
								</div>
								<br />
								<div style={{ textAlign: 'right' }}>
									<Form.Field>
										<div style={{ textAlign: 'right' }}>
											{/* <DateField name="date" handleChange={handleChange} value={values.date} label={gnak.firstheading.date} /> */}
											<DashedLangDateField
												name="date"
												handleChange={handleChange}
												value={values.date}
												error={errors.date}
												setFieldValue={setFieldValue}
												label={gnak.firstheading.date}
											/>
										</div>
									</Form.Field>
								</div>
								<br />
								<br />
								<div>
									{gnak.data_1.data_1_1}
									<DashedLangInput name="grandParents" setFieldValue={setFieldValue} value={values.grandParents} />
									{gnak.data_1.data_1_2}
									{gnak.data_1.data_1_4}
									{gnak.data_1.data_1_3}
									<DashedLangInput name="parents" setFieldValue={setFieldValue} value={values.parents} />
									{gnak.data_1.data_1_5}
									{gnak.data_1.data_1_6}
									{gnak.data_1.data_1_7}
									<DashedLangInput name="jilla" setFieldValue={setFieldValue} value={values.jilla} />
									{gnak.data_1.data_1_8}
									{userData.organization.name}
									{gnak.data_1.data_1_10}
									{permitData.newWardNo}
									{gnak.data_1.data_1_11}
									{permitData.tol}
									{gnak.data_1.data_1_12}
									{permitData.nibedakName}
									{gnak.data_1.data_1_13}
									<DashedLangInput name="buildingTalla" setFieldValue={setFieldValue} value={values.buildingTalla} />
									{gnak.data_1.data_1_14}
									{permitData.landPassDate}
									{gnak.data_1.data_1_15}
									{userData.organization.name}
									{gnak.data_1.data_1_17}
									<DashedLangInput name="talla" setFieldValue={setFieldValue} value={values.talla} />
									{gnak.data_1.data_1_18}
									{gnak.data_1.data_1_18_1}
								</div>
								<br />
								<br />
								<div style={{ textAlign: 'center' }}>
									<h2>
										<span>{gnak.secondheading.heading_1}</span>
									</h2>
									<h5 style={{ textDecoration: 'underline' }}>
										<span>{gnak.secondheading.heading_2}</span>
									</h5>
								</div>
								<Grid>
									<Grid.Row floated="left">
										<Grid.Column width={8}>
											<span>{gnak.data_2.data_2_1}</span>
											{/* <DashedLangInput
												name="purba"
												setFieldValue={setFieldValue}
												value={values.purbaSandhiyar}
											/> */}
											<span className="ui input dashedForm-control">{values.purbaSandhiyar}</span>
										</Grid.Column>
										<Grid.Column width={8}>
											<span>{gnak.data_2.data_2_2}</span>
											{/* <DashedLangInput
												name="paschim"
												setFieldValue={setFieldValue}
												value={values.paschimSandhiyar}
											/> */}
											<span className="ui input dashedForm-control">{values.paschimSandhiyar}</span>
										</Grid.Column>
									</Grid.Row>
									<Grid.Row floated="right">
										<Grid.Column width={8}>
											<span>{gnak.data_2.data_2_3}</span>
											{/* <DashedLangInput
												name="uttar"
												setFieldValue={setFieldValue}
												value={values.uttarSandhiyar}
											/> */}
											<span className="ui input dashedForm-control">{values.uttarSandhiyar}</span>
										</Grid.Column>
										<Grid.Column width={8}>
											<span>{gnak.data_2.data_2_4}</span>
											<span className="ui input dashedForm-control">{values.dakshinSandhiyar}</span>
										</Grid.Column>
									</Grid.Row>
								</Grid>
								<br />
								<br />
								<div>
									<span>{gnak.data_3.data_3_1}</span>
									<DashedLangInput name="compondNaap" setFieldValue={setFieldValue} value={values.compondNaap} />
									<br />
									<Grid>
										<Grid.Row columns={3}>
											<Grid.Column>
												<span>{gnak.data_3.data_3_2}</span>
												<DashedLangInput name="lambai" setFieldValue={setFieldValue} value={values.lambai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_3}</span>
												<DashedLangInput name="chaudai" setFieldValue={setFieldValue} value={values.chaudai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_4}</span>
												<DashedLangInput name="uchai" setFieldValue={setFieldValue} value={values.uchai} />
											</Grid.Column>
										</Grid.Row>
										<Grid.Row columns={3}>
											<Grid.Column>
												<span>{gnak.data_3.data_3_5}</span>
												<DashedLangInput
													name="sauchalayaLambai"
													setFieldValue={setFieldValue}
													value={values.sauchalayaLambai}
												/>
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_6}</span>
												<DashedLangInput
													name="sauchalayaChaudai"
													setFieldValue={setFieldValue}
													value={values.sauchalayaChaudai}
												/>
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_7}</span>
												<DashedLangInput
													name="sauchalayaUchai"
													setFieldValue={setFieldValue}
													value={values.sauchalayaUchai}
												/>
											</Grid.Column>
										</Grid.Row>
										<Grid.Row columns={3}>
											<Grid.Column>
												<span>{gnak.data_3.data_3_8}</span>
												<DashedLangInput name="septingLambai" setFieldValue={setFieldValue} value={values.septingLambai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_9}</span>
												<DashedLangInput name="septingChaudai" setFieldValue={setFieldValue} value={values.septingChaudai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_10}</span>
												<DashedLangInput name="septingUchai" setFieldValue={setFieldValue} value={values.septingUchai} />
											</Grid.Column>
										</Grid.Row>
										<Grid.Row columns={3}>
											<Grid.Column>
												<span>{gnak.data_3.data_3_11}</span>
												<DashedLangInput name="compondLambai" setFieldValue={setFieldValue} value={values.compondLambai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_12}</span>
												<DashedLangInput name="compondChaudai" setFieldValue={setFieldValue} value={values.compondChaudai} />
											</Grid.Column>
											<Grid.Column>
												<span>{gnak.data_3.data_3_13}</span>
												<DashedLangInput name="compondUchai" setFieldValue={setFieldValue} value={values.compondUchai} />
											</Grid.Column>
										</Grid.Row>
									</Grid>
									<div>
										<br />
										<br />
										<br />
										<span>{gnak.footer.footer_1}</span>
										<DashedLangInput name="sambat" setFieldValue={setFieldValue} value={values.sambat} />
										<span>{gnak.footer.footer_2}</span>
										<DashedLangInput name="saal" setFieldValue={setFieldValue} value={values.saal} />
										<span>{gnak.footer.footer_3}</span>
										<DashedLangInput name="gatey" setFieldValue={setFieldValue} value={values.gatey} />
										<span>{gnak.footer.footer_4}</span>
										<DashedLangInput name="roj" setFieldValue={setFieldValue} value={values.roj} />
										<span>{gnak.footer.footer_5}</span>
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const GharNaksakoKabuliyatnama = parentProps => (
	<FormContainerV2
		api={[
			{ api: api.gharNaksakoKabuliyatnama, objName: 'gharNaksaKabuliyat', form: true },
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: false,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <GharNaksakoKabuliyatnamaComponent {...props} parentProps={parentProps} />}
	/>
);

export default GharNaksakoKabuliyatnama;
