import React from 'react';
import { DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import { BlockComponents } from '../../../shared/formComponents/BlockFloorComponents';
import { squareUnitOptions } from '../../../../utils/dataUtils';
import { prabidhikpratibedhan } from '../../../../utils/data/mockLangFile';

const pppv_data = prabidhikpratibedhan.prabidhikpratibedhan_data;

export const PrabidhikFloorDetails = ({ floorArray, blocks }) => {
	const allFloorFields = floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], '', 'floor');
	const allTopFloorFields = floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], '', 'topFloor');

	return (
		<div>
			{pppv_data.ppbv_footer_data_5}
			<div className="no-margin-field div-indent">
				<BlockComponents floorArray={floorArray} blocks={blocks}>
					{block => {
						const length = floorArray.getReducedFieldName('length', block, 'floor');
						const width = floorArray.getReducedFieldName('width', block, 'floor');
						const height = floorArray.getReducedFieldName('height', block, 'floor');
						const area = floorArray.getReducedFieldName('area', block, 'floor');
						return (
							<>
								<DashedLengthInputWithRelatedUnits
									name={length}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], length, 'floor'),
										...allTopFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_5_0}
								/>
								<DashedLengthInputWithRelatedUnits
									name={width}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], width, 'floor'),
										...allTopFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_5_1}
								/>
								<DashedLengthInputWithRelatedUnits
									name={height}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], height, 'floor'),
										...allTopFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_5_2}
								/>
								<DashedAreaInputWithRelatedUnits
									name={area}
									unitName="floorUnit"
									label={pppv_data.ppbv_footer_data_5_3}
									squareOptions={squareUnitOptions}
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], area, 'floor'),
										...allTopFloorFields,
									]}
								/>
							</>
						);
					}}
				</BlockComponents>
			</div>
			{pppv_data.ppbv_footer_data_6}
			<div className="no-margin-field div-indent">
				<BlockComponents floorArray={floorArray} blocks={blocks}>
					{block => {
						const length = floorArray.getReducedFieldName('length', block, 'topFloor');
						const width = floorArray.getReducedFieldName('width', block, 'topFloor');
						const height = floorArray.getReducedFieldName('height', block, 'topFloor');
						const area = floorArray.getReducedFieldName('area', block, 'topFloor');
						return (
							<>
								<DashedLengthInputWithRelatedUnits
									name={length}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], length, 'topFloor'),
										...allFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_6_0}
								/>
								<DashedLengthInputWithRelatedUnits
									name={width}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], width, 'topFloor'),
										...allFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_6_1}
								/>
								<DashedLengthInputWithRelatedUnits
									name={height}
									unitName="floorUnit"
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], height, 'topFloor'),
										...allFloorFields,
									]}
									label={pppv_data.ppbv_footer_data_6_2}
								/>
								<DashedAreaInputWithRelatedUnits
									name={area}
									unitName="floorUnit"
									label={pppv_data.ppbv_footer_data_6_3}
									squareOptions={squareUnitOptions}
									relatedFields={[
										...floorArray.getAllReducedBlockFields(['length', 'width', 'area', 'height'], area, 'topFloor'),
										...allFloorFields,
									]}
								/>
							</>
						);
					}}
				</BlockComponents>
			</div>
		</div>
	);
};
