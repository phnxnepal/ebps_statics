import React from 'react';
import api from '../../../utils/api';

import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { genericTippaniAdes } from '../../../utils/data/genericFormData';
import { PrintParams } from '../../../utils/printUtils';
import { GenericDesignerSansodhan } from './formComponents/GenericDesignerSansodhan';

const SansodhanGariNibedan = (parentProps) => (
	// <FormContainerV2
	// 	api={[
	// 		{
	// 			api: api.sansodhanGariNibedan,
	// 			objName: 'sansodhanNibedan',
	// 			form: true,
	// 		},
	// 	]}
	// 	prepareData={data => data}
	// 	parentProps={parentProps}
	// 	onBeforeGetContent={{
	// 		param1: ['getElementsByTagName', 'textarea', 'value'],
	// 	}}
	// 	render={props => (
	// 		<GenericTippaniAdes
	// 			fieldName="nibedan"
	// 			topic={genericTippaniAdes.sansodhanGariNibedan.topic}
	// 			title={genericTippaniAdes.title}
	// 			api={api.sansodhanGariNibedan}
	// 			{...props}
	// 			parentProps={parentProps}
	// 		/>
	// 	)}
	// />
	<FormContainerV2
		api={[
			{
				api: api.sansodhanGariNibedan,
				objName: 'sansodhanGariNibedan',
				form: true,
			},
		]}
		onBeforeGetContent={PrintParams.TEXT_AREA}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFile={true}
		render={(props) => (
			<GenericDesignerSansodhan
				fieldName="nibedan"
				topic={genericTippaniAdes.sansodhanGariNibedan.topic}
				api={api.sansodhanGariNibedan}
				{...props}
				parentProps={parentProps}
			/>
		)}
	/>
);

export default SansodhanGariNibedan;
