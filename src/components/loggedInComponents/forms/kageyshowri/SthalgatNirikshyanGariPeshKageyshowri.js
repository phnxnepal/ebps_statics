import React, { Component } from 'react';
import { Form, Table } from 'semantic-ui-react';

import { Formik, Field, getIn } from 'formik';

import * as Yup from 'yup';

// import * as Yup from 'yup';
import { naksaData, sthalgatNirikshyanGariPratibedanReqData, noObjectionSthalgatData } from '../../../../utils/data/bhawanData';
import { CompactDashedLangDate, DashedDateField } from '../../../shared/DateField';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { prepareMultiInitialValues, getJsonData, floorMappingFlat, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import FormikCheckbox, { FormikCheckboxIm } from '../../../shared/FormikCheckbox';
import TableInput from '../../../shared/TableInput';
import { checkError } from '../../../../utils/dataUtils';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { validateNullableOfficialReqNumbers, validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { getSaveByUserDetails } from '../../../../utils/formUtils';
import { isKamalamai } from '../../../../utils/clientUtils';
import { DashedAreaUnitInput, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight, FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { ApiParam } from '../../../../utils/paramUtil';

const stringify = (value) => {
	if (value) {
		if (typeof value === 'string') return value;
		else return JSON.stringify(value);
	} else return '';
};

let data = naksaData.structureDesign;
let TableData = naksaData.structureDesign.table.rows;
let additionalData = naksaData.structureDesign.table.additionalData;
const customInputs = naksaData.structureDesign.customInputs;
// const footerData = naksaData.structureDesign.footer;
const reqData = sthalgatNirikshyanGariPratibedanReqData;
const arrayValueIds = [21, 30, 7, 5, 1, 2];
const objectValueIds = [29, 4, 20, 16, 25];
const toArray = (field) => {
	if (field.includes('[')) {
		const returnVal = field.replace(/[[\]]+/g, '');
		const retArr = returnVal.split(',');
		return retArr.map((val) => val.trim());
	} else return [field];
};
let initialVal = {
	chana: '',
	dwaraNakshaDesignMiti: '',
	date: '',
	kaufiyat: '',
	peshGarne: '',
	juEKoNaam: '',
	peshGarneMiti: '',
	engineerKoRaya: '',
	engineerKoSahiMiti: '',
};

const MapCheckSchema = {}
class SthalgatNirikshyanGariPeshKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, userData, prevData, otherData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;
		const mapTech = getJsonData(otherData.mapTechnicalDescription);
		const mapCheckReport = getJsonData(otherData.mapCheckReport);

		const pulledData = {
			details: [
				{
					description: customInputs.buildingClass,
					designData: otherData.designApprovalData.buildingClass,
				},
				{
					description: customInputs.purposeOfConstruction,
					// designData: permitData.purposeOfConstruction,
					designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther },
				},
				{
					description: customInputs.constructionType,
					designData: getConstructionTypeValue(permitData.constructionType),
				},
				{
					description: customInputs.structuralType,
					designData: TableData.row8.input_1[0],
				},
				// {
				// 	description: purposeOfConstructionOther,
				// 	designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther }
				// },
				{
					description: customInputs.floorNumber,
					designData: mapTech.buildingDetailfloor || '',
				},
				{
					description: customInputs.plinthArea,
					designData: [mapTech.plinthDetails || 0, mapTech.plinthDetailsUnit || DEFAULT_UNIT_LENGTH],
				},
				{
					description: customInputs.buildingHeight,
					designData: [
						'',
						mapTech.buildingHeight || 0,
						mapTech.buildingHeightUnit || DEFAULT_UNIT_LENGTH,
						mapTech.buildingHeightUnit || DEFAULT_UNIT_LENGTH,
					],
				},
				{
					description: customInputs.landLength,
					designData: [mapTech.fieldLength || 0, mapTech.fieldLengthUnit || DEFAULT_UNIT_LENGTH],
				},
				{
					description: customInputs.landWidth,
					designData: [mapTech.fieldWidth || 0, mapTech.fieldLengthUnit || DEFAULT_UNIT_LENGTH],
				},
			],
		};

		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// } else if (!isStringEmpty(checkError(prevData).enterBy)) {
		// 	serInfo.subName = prevData.enterBy;
		// 	serInfo.subDesignation = getUserTypeValueNepali(UserType.SUB_ENGINEER);
		// }

		const initialDetails = prepareMultiInitialValues(
			{
				obj: getSaveByUserDetails(enterByUser, userData),
				reqFields: [],
			},
			{
				obj: mapCheckReport,
				reqFields: [],
			},
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true), dwaraNakshaDesignMiti: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: { mapCheckReportDate: getCurrentDate(true) },
				reqFields: [],
			},
			{ obj: prepareInitialVaules(prevData, pulledData), reqFields: [] }
		);

		initialDetails.details.forEach((row) => {
			if (arrayValueIds.includes(row.descriptionId)) {
				if (Array.isArray(row.designData)) return;
				row.designData = toArray(row.designData);
			} else if (objectValueIds.includes(row.descriptionId)) {
				try {
					if (typeof row.designData === 'string') {
						row.designData = JSON.parse(row.designData);
					}
					if (typeof row.designData !== 'object') {
						row.designData = {};
					}
				} catch (err) {
					// console.log('err', err, ' in ', row.designData, row.descriptionId, row);
					return {};
				}
			}
		});

		// to maintain index integrity
		prevData.details && prevData.details.map((row, inputIndex) => (row.inputIndex = inputIndex));

		let prevDatas = prevData.enterBy == null && prevData.enterDate == null ? {} : prevData;

		this.state = {
			toggle: {
				index: 0,
				value: TableData.row8.input_1[0],
			},
			initialDetails,
			prevDatas,
		};
	}

	handleFilterInputChangeRoofStyle = (e) => {
		const updateRoofStyle = e.target.value;
		if ('अन्य' === updateRoofStyle) {
			this.setState({
				inputRoofStyleFilter: true,
			});
		} else {
			this.setState({ inputRoofStyleFilter: false });
		}
		this.setState({ RoofStyle: updateRoofStyle });
	};

	render() {
		const { permitData, prevData, hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl, userData } = this.props;

		const { initialDetails, prevDatas } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialDetails}
					validationSchema={MapCheckSchema}
					onSubmit={async (values, actions) => {
						// values.details.filter(row => row.descriptionId === 9).map(row => (row.designData = this.state.toggle.value));
						actions.setSubmitting(true);
						values.details[28].designData = stringify(values.details[28].designData);
						values.details[3].designData = stringify(values.details[3].designData);
						values.details[19].designData = stringify(values.details[19].designData);
						values.details[24].designData = stringify(values.details[24].designData);
						values.details[15].designData = stringify(values.details[15].designData);

						const dataToSend = { details: values.details, jsonData: JSON.stringify({ ...values, details: '' }) };

						try {
							await this.props.postAction(api.SthalgatNiirikshyanGariPesh, dataToSend, true);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, values, handleChange, setFieldValue, isSubmitting, errors, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								<div ref={this.props.setRef}>
									<br />
									<br />

									<SectionHeader>
										<h3>{data.table.heading}</h3>
									</SectionHeader>
									<div className="NayaJhanchPachi">
										<Table celled>
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>{TableData.row1.sn}</Table.HeaderCell>
													<Table.HeaderCell>{TableData.row1.name}</Table.HeaderCell>
													<Table.HeaderCell colSpan="3">{TableData.row1.input_1}</Table.HeaderCell>
													<Table.HeaderCell>{TableData.row1.input_2}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{prevData.details &&
													prevData.details
														.filter((row) => row.type === '' || row.type === 'P : G')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																<Table.Cell>{row.description}</Table.Cell>
																{parseInt(row.sn) === 9 ? (
																	<Table.Cell colSpan="3">
																		{TableData.row8.input_1.map((input, index) => (
																			<div key={index} className="ui radio checkbox">
																				<input
																					type="radio"
																					value={TableData.row8.input_1[index]}
																					name={`details.${row.inputIndex}.designData`}
																					checked={
																						getIn(values, `details.${row.inputIndex}.designData`) ===
																						input
																					}
																					onChange={(e) => {
																						setFieldValue(`details.${row.inputIndex}.designData`, input);
																					}}
																				/>
																				<label for={`details.${row.inputIndex}.designData`}>{input}</label>
																			</div>
																		))}
																	</Table.Cell>
																) : !Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell colSpan="3">
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																	[
																		(row.description === customInputs.landLength ||
																			row.description === customInputs.landWidth) && (
																			<Table.Cell colSpan="3">
																				<DashedUnitInput
																					name={`details.${row.inputIndex}.designData[0]`}
																					unitName={`details.${row.inputIndex}.designData[1]`}
																				/>
																			</Table.Cell>
																		),
																		row.description === customInputs.buildingClass && (
																			<Table.Cell colSpan="3">
																				{TableData.row2.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index} className="ui radio checkbox">
																						<input
																							type="radio"
																							name={`details.${row.inputIndex}.designData`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						<label for={`details.${row.inputIndex}.designData`}>
																							{input}
																						</label>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.plinthArea && (
																			<Table.Cell colSpan="3">
																				<DashedAreaUnitInput
																					name={`details.${row.inputIndex}.designData[0]`}
																					unitName={`details.${row.inputIndex}.designData[1]`}
																				/>
																			</Table.Cell>
																		),
																		row.description === customInputs.floorNumber && (
																			<Table.Cell colSpan="3">
																				{TableData.row5.input_1.map((input, index) => (
																					// <Table.Cell>
																					<>
																						{isKamalamai ? (
																							<FormikCheckboxIm
																								name={`details.${row.inputIndex}.designData`}
																								label={input}
																							/>
																						) : (
																							<div key={index} className="ui radio checkbox">
																								<input
																									type="radio"
																									name={`details.${row.inputIndex}.designData`}
																									value={input}
																									checked={
																										getIn(
																											values,
																											`details.${row.inputIndex}.designData`
																										) === input
																									}
																									onChange={handleChange}
																								/>
																								<label for={`details.${row.inputIndex}.designData`}>
																									{input}
																								</label>
																							</div>
																						)}
																					</>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.buildingHeight && (
																			<Table.Cell colSpan="3">
																				{TableData.row6.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index}>
																						{/* <label for={`details.${row.inputIndex}.designData`}>
																							{input}
																						</label> */}
																						<DashedUnitInput
																							label={input}
																							name={`details.${row.inputIndex}.designData[${index}]`}
																							unitName={`details.${row.inputIndex}.designData[${
																								index + 2
																							}]`}
																						/>
																						{/* <input
																							type="text"
																							className="dashedForm-control"
																							name={`details.${row.inputIndex}.designData.${index}`}
																							// id={`details.${row.inputIndex}.designData.${index}`}
																							value={getIn(
																								values,
																								`details.${row.inputIndex}.designData.${index}`
																							)}
																							onChange={handleChange}
																						/> */}
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.purposeOfConstruction && (
																			<Table.Cell colSpan="3">
																				{TableData.row3.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index} className="ui radio checkbox">
																						<input
																							type="radio"
																							name={`details.${row.inputIndex}.designData.option`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						<label for={`details.${row.inputIndex}.designData.option`}>
																							{input}
																						</label>
																					</div>
																					// </Table.Cell>
																				))}
																				{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																					'अन्य' && (
																					<input
																						type="text"
																						className="dashedForm-control"
																						name={`details.${row.inputIndex}.designData.other`}
																						// id={`details.${row.inputIndex}.designData.${index}`}
																						value={getIn(
																							values,
																							`details.${row.inputIndex}.designData.other`
																						)}
																						onChange={handleChange}
																					/>
																					// <DashedLangInput
																					// 	// inline={true}
																					// 	name={`details.${row.inputIndex}.designData.other`}
																					// 	placeholder="Additional Information..."
																					// 	setFieldValue={setFieldValue}
																					// 	value={values.purposeOfConstructionOther}
																					// 	error={errors.purposeOfConstructionOther}
																					// />
																				)}
																			</Table.Cell>
																		),
																		row.description === customInputs.constructionType && (
																			<Table.Cell colSpan="3">
																				<ConstructionTypeRadio
																					name={`details.${row.inputIndex}.designData`}
																					space={true}
																				/>
																			</Table.Cell>
																		),
																	]
																)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}
												{getIn(values, `details.8.designData`) === TableData.row8.input_1[0] &&
													prevData.details &&
													prevData.details
														.filter((row) => row.type === 'P')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																<Table.Cell>{row.description}</Table.Cell>
																{!Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell colSpan="3">
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																	[
																		row.description === customInputs.pillarDetails && (
																			<Table.Cell colSpan="3">
																				{TableData.row17.input_1.map((input, optionIndex) => (
																					// <Table.Cell>
																					<div key={optionIndex} className="ui checkbox">
																						<FormikCheckbox
																							// type="checkbox"
																							name={`details.${row.inputIndex}.designData.option`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						{getIn(
																							values,
																							`details.${row.inputIndex}.designData.option`
																						) &&
																							getIn(
																								values,
																								`details.${row.inputIndex}.designData.option`
																							).includes(input) && (
																								<Table.Row>
																									<Table.Cell colSpan="3">
																										<TableInput
																											name={`details.${row.inputIndex}.designData.value.${optionIndex}`}
																											value={getIn(
																												values,
																												`details.${row.inputIndex}.designData.value.${optionIndex}`
																											)}
																											onChange={handleChange}
																										/>
																									</Table.Cell>
																								</Table.Row>
																							)}
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.concreteGrade && (
																			<Table.Cell colSpan="3">
																				{TableData.row20.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index} className="ui radio checkbox">
																						<input
																							type="radio"
																							name={`details.${row.inputIndex}.designData.option`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						<label for={`details.${row.inputIndex}.designData.option`}>
																							{input}
																						</label>
																					</div>
																					// </Table.Cell>
																				))}
																				{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																					'अन्य' && (
																					<input
																						type="text"
																						className="dashedForm-control"
																						name={`details.${row.inputIndex}.designData.other`}
																						// id={`details.${row.inputIndex}.designData.${index}`}
																						value={getIn(
																							values,
																							`details.${row.inputIndex}.designData.other`
																						)}
																						onChange={handleChange}
																					/>
																				)}
																			</Table.Cell>
																		),
																		row.description === customInputs.concreteBand && (
																			<Table.Cell colSpan="3">
																				{TableData.row31.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index} className="ui checkbox">
																						<FormikCheckbox
																							// type="checkbox"
																							name={`details.${row.inputIndex}.designData`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																	]
																)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}
												{getIn(values, `details.8.designData`) === TableData.row8.input_1[1] &&
													prevData.details
														.filter((row) => row.type === 'G')
														.map((row, index) => (
															<Table.Row key={index}>
																<Table.Cell>{row.sn}</Table.Cell>
																{/* <Table.Cell>{row.description}</Table.Cell> */}
																{row.description === customInputs.garoBibaran ? (
																	<Table.Cell>
																		<Table.Row>{row.description}</Table.Row>
																		{permitData.floor.map((floor) => (
																			<Table.Row key={floor.floor}>
																				<Table.Cell>
																					{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}
																				</Table.Cell>
																			</Table.Row>
																		))}
																	</Table.Cell>
																) : (
																	<Table.Cell>{row.description}</Table.Cell>
																)}
																{!Object.values(customInputs).includes(row.description) ? (
																	<Table.Cell colSpan="3">
																		<Form.Field>
																			<Field name={`details.${row.inputIndex}.designData`} />
																		</Form.Field>
																	</Table.Cell>
																) : (
																	[
																		row.description === customInputs.concreteGradeGaro && (
																			<Table.Cell colSpan="3">
																				{TableData.row20.input_1.map((input) => (
																					// // <Table.Cell>
																					<RadioInput
																						name={`details.${row.inputIndex}.designData.option`}
																						option={input}
																					/>
																					// </Table.Cell>
																				))}
																				{getIn(values, `details.${row.inputIndex}.designData.option`) ===
																					'अन्य' && (
																					<input
																						type="text"
																						className="dashedForm-control"
																						name={`details.${row.inputIndex}.designData.other`}
																						// id={`details.${row.inputIndex}.designData.${index}`}
																						value={getIn(
																							values,
																							`details.${row.inputIndex}.designData.other`
																						)}
																						onChange={handleChange}
																					/>
																				)}
																			</Table.Cell>
																		),
																		row.description === customInputs.concreteBandGaro && (
																			<Table.Cell >
																				{TableData.row31.input_1.map((input) => (
																					// <Table.Cell>
																					<div key={input} className="ui checkbox">
																						<FormikCheckbox
																							// type="checkbox"
																							name={`details.${row.inputIndex}.designData`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.garoBibaran && (
																			<Table.Cell>
																				{TableData.row30.input_1.map((input, optionIndex) => (
																					// <Table.Cell>
																					<React.Fragment key={optionIndex}>
																						<FormikCheckbox
																							// type="checkbox"
																							name={`details.${row.inputIndex}.designData.option`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData.option`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						{getIn(
																							values,
																							`details.${row.inputIndex}.designData.option`
																						) &&
																							getIn(
																								values,
																								`details.${row.inputIndex}.designData.option`
																							).includes(input) &&
																							permitData.floor.map((floor) => {
																								// const index = optionIndex + floor.floor;
																								// console.table({'option': optionIndex, 'floor': floor.floor, 'index': index});
																								return (
																									<Table.Row key={floor.floor}>
																										{/* {
																							floorMappingFlat.find(
																								fl => fl.floor === floor.floor
																							).value
																						} */}
																										<Table.Cell>
																											<TableInput
																												name={`details.${row.inputIndex}.designData.value.${floor.floor}.${optionIndex}`}
																												value={getIn(
																													values,
																													`details.${row.inputIndex}.designData.value.${floor.floor}.${optionIndex}`
																												)}
																												onChange={handleChange}
																											/>
																										</Table.Cell>
																									</Table.Row>
																								);
																							})}
																					</React.Fragment>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																		row.description === customInputs.cornerStiching && (
																			<Table.Cell>
																				{TableData.row33.input_1.map((input, index) => (
																					// <Table.Cell>
																					<div key={index} className="ui radio checkbox">
																						<input
																							type="radio"
																							name={`details.${row.inputIndex}.designData`}
																							value={input}
																							checked={
																								getIn(
																									values,
																									`details.${row.inputIndex}.designData`
																								) === input
																							}
																							onChange={handleChange}
																						/>
																						<label for={`details.${row.inputIndex}.designData`}>
																							{input}
																						</label>
																					</div>
																					// </Table.Cell>
																				))}
																			</Table.Cell>
																		),
																	]
																)}
																<Table.Cell>
																	<Form.Field>
																		<Field name={`details.${row.inputIndex}.remark`} />
																	</Form.Field>
																</Table.Cell>
															</Table.Row>
														))}

													

												<Table.Row>
													<Table.Cell rowSpan="2">{additionalData.row1.num}</Table.Cell>
													<Table.Cell>{additionalData.row1.input_1[0]}</Table.Cell>
													<Table.Cell>{additionalData.row1.input_2[0]}</Table.Cell>
													<Table.Cell>{additionalData.row1.input_2[1]}</Table.Cell>
													<Table.Cell>{additionalData.row1.input_2[2]}</Table.Cell>
													<Table.Cell rowSpan="2">

														<TableInput
															name="name1"
															value={values.name1}
															error={errors.name1}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>

													</Table.Cell>
												</Table.Row>
												<Table.Row>
												<Table.Cell>{additionalData.row1.input_1[1]}</Table.Cell>

													<Table.Cell>
														<TableInput 
															name="name3"
															value={values.name3}
															error={errors.name3}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													<Table.Cell>
														<TableInput 
															name="name4"
															value={values.name4}
															error={errors.name4}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													<Table.Cell>
														<TableInput 
															name="name5"
															value={values.name5}
															error={errors.name5}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													
												</Table.Row>

												<Table.Row>
													<Table.Cell>{additionalData.row2.num}</Table.Cell>
													<Table.Cell>{additionalData.row2.input_1}</Table.Cell>
													<Table.Cell colSpan='3'>
														<TableInput
															name="name7"
															value={values.name7}
															error={errors.name7}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													<Table.Cell>
														<TableInput
															name="name8"
															value={values.name8}
															error={errors.name8}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
												</Table.Row>


												<Table.Row>
													<Table.Cell>{additionalData.row3.num}</Table.Cell>
													<Table.Cell>{additionalData.row3.input_1}</Table.Cell>
													<Table.Cell colSpan='3'>
														<TableInput
															name="name9"
															value={values.name9}
															error={errors.name9}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													<Table.Cell>
														<TableInput
															name="name10"
															value={values.name10}
															error={errors.name10}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
												</Table.Row>


												<Table.Row>
													<Table.Cell>{additionalData.row4.num}</Table.Cell>
													<Table.Cell>{additionalData.row4.input_1}</Table.Cell>
													<Table.Cell colSpan='3'>
														<TableInput
															name="name11"
															value={values.name11}
															error={errors.name11}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
													<Table.Cell>
														<TableInput
															name="name12"
															value={values.name12}
															error={errors.name12}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</Table.Cell>
												</Table.Row>


												<Table.Row>
													<Table.Cell>{additionalData.row5.num}</Table.Cell>
													<Table.Cell colSpan="5">
														{additionalData.row5.input_1}
													</Table.Cell>
												</Table.Row>






											</Table.Body>
										</Table>
										<br />



										<div style={{display: 'grid', gridTemplateColumns: "1fr 1fr", gridGap: '20px'}}>
											<div>
												<div>
													<span>{noObjectionSthalgatData.footer.gharDhani} : {permitData.applicantName}</span> <br />
													<span>{noObjectionSthalgatData.footer.address}</span> <br />
													<span>{noObjectionSthalgatData.footer.sahi}</span> <br />
													<span>{noObjectionSthalgatData.footer.date}</span> <br />
													<span>{noObjectionSthalgatData.footer.samparka} : {permitData.applicantMobileNo}</span> <br />
												</div>
												
												<br />
												<br />

												<div>
													<span>{noObjectionSthalgatData.footer.sartha.list1}</span><br /><br />
													<span>{noObjectionSthalgatData.footer.sartha.list2}</span><br /><br />
												</div>
											</div>
											<div>
												<div style={{border: "1px solid black"}}>{noObjectionSthalgatData.footer.note}</div>
												<br />
												<br />
												<div>
													<b><u>{noObjectionSthalgatData.footer.paramDartha.heading}</u></b>
													<br />

													<span>{noObjectionSthalgatData.footer.paramDartha.en} </span>
													<DashedLangInput
														name={"enName"}
														setFieldValue={setFieldValue}
														value={values.enName}
														error={errors.enName}
													/>
													<br />
													<br />

													<span>{noObjectionSthalgatData.footer.paramDartha.signature}</span>

													<br />
													<br />

													<span>{noObjectionSthalgatData.footer.samparka}</span>
													<DashedLangInput
														name={"samparka"}
														setFieldValue={setFieldValue}
														value={values.samparka}
														error={errors.samparka}
													/>
													<br />
												</div>
											</div>
											</div>

									
									</div>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevDatas)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const prepareInitialVaules = (prevData, pulledData) => {
	// const filteredPrevData = Object.entries(prevData).reduce((acc, [key, value]) => {
	// 	if (value && ['subDate', 'subDesignation', 'subName', 'subSignature'].includes(key)) {
	// 		return { ...acc, [key]: value };
	// 	} else return acc;
	// }, {});
	let jsonData = {};
	try {
		jsonData = prevData && prevData.jsonData && JSON.parse(prevData.jsonData);
	} catch (error) {}
	const ret = { ...jsonData, details: [] };

	if (prevData.details && prevData.details.length > 0) {
		prevData.details.forEach((row, index) => {
			const pulledVal = pulledData.details.find((el) => el.description === row.description);
			if (row.descriptionId === 8) {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? getConstructionTypeValue(row.designData) : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			} else {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? row.designData : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			}
		});
	}

	return ret;
};

const SthalgatNirikshyanGariPeshKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.SthalgatNiirikshyanGariPesh).setForm().getParams(),
			// { api: api.allowancePaper, objName: 'allowancePaper', form: true },
			// {
			// 	api: api.mapCheckReport,
			// 	objName: 'mapCheckReport',
			// 	form: true,
			// },
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.mapCheckReport,
				objName: 'mapCheckReport',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTechnicalDescription',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'buildingClass',
				form: false,
				utility: true,
			},
		]}
		onBeforeGetContent={{
			param: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <SthalgatNirikshyanGariPeshKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default SthalgatNirikshyanGariPeshKageyshowri;
