import React, { Component } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { Form } from 'semantic-ui-react';
import { superstructurenodeorderview } from '../../../../utils/data/mockLangFile';
import { FooterSignature } from '../formComponents/FooterSignature';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { getJsonData, getApprovalData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserRole, getUserTypeValueNepali, isEmpty, showToast } from '../../../../utils/functionUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { DashedUnitInput } from './../../../shared/EbpsUnitLabelValue';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import api from '../../../../utils/api';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getMessage } from '../../../shared/getMessage';
import { DashedLangDateField, CompactDashedLangDate } from '../../../shared/DateField';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';

const ssnov_data = superstructurenodeorderview.superstructurenodeorderview_data;
const kageyshowriData = superstructurenodeorderview.kageyshowriData;
const messageId = 'superstructurenodeorderview.superstructurenodeorderview_data';

/**
 * @TODO remove unused variables
 */

class SuperStructureNoteOrderViewKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { userData, prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		const surjamin = getJsonData(otherData.surjamin);
		const noteorderPilengthLevel = getJsonData(otherData.noteorderPilengthLevel);

		let serInfo = {
			subName: '',
			subDesignation: '',
		};

		if (getUserRole() === UserType.SUB_ENGINEER) {
			serInfo.subName = userData.userName;
			serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		}

		let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			{
				obj: surjamin,
				reqFields: [],
			},
			{
				obj: json_data,
				reqFields: [],
			},
			{
				obj: noteorderPilengthLevel,
				reqFields: [],
			},
		);

		initVal.superStructureUnit = 'METRE';
		initVal.otherAreaUnit = 'SQUARE METRE';

		if (isStringEmpty(initVal.dates)) {
			initVal.dates = getCurrentDate(true);
		}

		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		const user_info = this.props.userData;

		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.SuperStructureNoteOrder}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={user_info} />
								<FlexSingleRight>
									<DashedLangDateField
										label={ssnov_data.date}
										name="superStructureNoteOrderDate"
										setFieldValue={setFieldValue}
										// inline={true}
										compact={true}
										value={values.superStructureNoteOrderDate}
										error={errors.superStructureNoteOrderDate}
									/>
								</FlexSingleRight>
								<SectionHeader>
									<h2 className="underline end-section">
										<span>{getMessage(`${messageId}.title_1`, ssnov_data.title_1)}</span>
									</h2>
								</SectionHeader>
								<SectionHeader>
									<h3 className="end-section">
										<span>{kageyshowriData.bisaye}</span>
									</h3>
								</SectionHeader>
								<div className="no-margin-field margin-bottom">

									<span>{kageyshowriData.sriman}</span>
									<br />
								{' '}{userData.organization.address}{' '}

									<span>{kageyshowriData.jilla}</span>
									{userData.organization.name}


									<span>{kageyshowriData.palika}</span>
									{' '}{permitData.newWardNo}{' '}

									<span>{kageyshowriData.basne}</span>
									
									<span>{kageyshowriData.shree}</span>
									{' '}{permitData.applicationName}{' '}

									<span>{kageyshowriData.le}</span>
									

									<span>{kageyshowriData.content1}</span>
									{' '}{permitData.newWardNo}{' '}

									<span>{kageyshowriData.kittaNo}</span>
									{' '}{permitData.kittaNo}{' '}

									<span>{kageyshowriData.cheetrafal}</span>
									{' '}{permitData.landArea}{' '}

									<span>{kageyshowriData.bamiko}</span>
									{' '}{permitData.buildingJoinRoad}{' '}

									<span>{kageyshowriData.content2}</span>
									<DashedUnitInput
										name="kendra"
										value={values.kendra}
										error={errors.kendra}
										setFieldValue={setFieldValue}
									/>{' '}

									<span>{kageyshowriData.content3} {kageyshowriData.direction1}</span>

								<DashedLangInput
									name={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : "dir0"}
									setFieldValue={setFieldValue}
									value={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : ""}
									error={errors.landArea}
								/>
								
								<span>{kageyshowriData.direction2}</span>

								<DashedLangInput
									name={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : "dir1"}
									setFieldValue={setFieldValue}
									value={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : ""}
									error={errors.landArea}
								/>
								<span>{kageyshowriData.direction3}</span>

								<DashedLangInput
									name={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : "dir2"}
									setFieldValue={setFieldValue}
									value={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : ""}
									error={errors.landArea}
								/>
								
								<span>{kageyshowriData.direction4}</span>

								<DashedLangInput
									name={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : "dir3"}
									setFieldValue={setFieldValue}
									value={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : ""}
									error={errors.landArea}
								/>


									<span>{kageyshowriData.content4} {kageyshowriData.lambae}</span>

									<DashedLengthInputWithRelatedUnits
										name="lambai"
										unitName="floorUnit"
										relatedFields={["chaudahi", "uchahi"]}
									/>{' '}

									<span>{kageyshowriData.chaudae}</span>

									<DashedLengthInputWithRelatedUnits
										name="chaudahi"
										unitName="floorUnit"
										relatedFields={["uchahi", "lambai"]}
									/>

									<span>{kageyshowriData.uchae}</span>

									<DashedLengthInputWithRelatedUnits
										name="uchahi"
										unitName="floorUnit"
										relatedFields={["lambai", "chaudahi"]}
									/>



									<span>{kageyshowriData.talle}</span>
									<DashedLangInput
										name="talle"
										value={values.talle}
										error={errors.talle}
										setFieldValue={setFieldValue}
									/>

									<span>{kageyshowriData.bargafeet}</span>
									<DashedLangInput
										name="bargaFeet"
										value={values.bargaFeet}
										error={errors.bargaFeet}
										setFieldValue={setFieldValue}
									/>

									<span>{kageyshowriData.bhayeko}</span>
									<DashedLangInput
										name="niramad"
										value={values.niramad}
										error={errors.niramad}
										setFieldValue={setFieldValue}
									/>

									<span>{kageyshowriData.content5}</span>
									<DashedLangDateField
										name="date1"
										inline={true}
										value={values.date1}
										error={errors.date1}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>

									<span>{kageyshowriData.content6}</span>
									<DashedLangDateField
										name="date2"
										inline={true}
										value={values.date2}
										error={errors.date2}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>
									<span>{kageyshowriData.content7}</span>
									{' '}{permitData.applicationName}{' '}

									<span>{kageyshowriData.le} {kageyshowriData.date}</span>
									<DashedLangDateField
										name="date3"
										inline={true}
										value={values.date3}
										error={errors.date3}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>

									<span>{kageyshowriData.content8}</span>

								</div>
								<FooterSignature
									designations={[
										kageyshowriData.subEng,
										kageyshowriData.eng,
										kageyshowriData.strEng,
										kageyshowriData.pramukh,
									]}
								/>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SuperStructureNoteOrderViewKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.SuperStructureNoteOrder,
				objName: 'surjamin',
				form: true,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteorderPilengthLevel',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param1: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		render={(props) => <SuperStructureNoteOrderViewKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNoteOrderViewKageyshowri;
