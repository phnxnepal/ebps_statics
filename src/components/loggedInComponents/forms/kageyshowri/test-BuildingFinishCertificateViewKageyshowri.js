import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { NeighboursNameApplicationData } from '../../../../utils/data/NeighboursNameApplicationData';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { FlexSpaceBetween } from '../../../uiComponents/FlexDivs';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { getJsonData, prepareMultiInitialValues, surroundingMappingFlat, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';

import { FloorBlockArray } from '../../../../utils/floorUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import {
	validateNullableOfficialNumbers,
	validateNullableNepaliDate,
} from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { PrintParams } from '../../../../utils/printUtils';
const { object } = require('yup');

const reqData = NeighboursNameApplicationData.structureDesign.Kageyshowri;

const shriShrimanShrimatiOptions = [
	{
		key: reqData.shir,
		text: reqData.shir,
		value: reqData.shir,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shushri,
		text: reqData.shushri,
		value: reqData.shushri,
	},
];
const intialVal = {
	pasa: '',
	chana: '',
	miti: '',
	shriShrimanShrimati: shriShrimanShrimatiOptions[0].value,
	nimnaBamojim: '',
	gharNo: '',
	buildingClass: '',
	surrounding: '',
	nirmanStructuralSystem: '',
	chalaniNumber: '',
	chalaniNumberMiti: '',
	sixNakshaPassNagariBanayekoMiti: '',
};
const validate = object({
	pasa: validateNullableOfficialNumbers,
	chana: validateNullableOfficialNumbers,
	miti: validateNullableNepaliDate,
	allowanceDate: validateNullableNepaliDate,
	noNaksaPassDate: validateNullableNepaliDate,
});

class BuildingFinishCertificateViewKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } = this.props;
		const allPaper = getJsonData(otherData.allowancePaper);
		const prabidhik = getJsonData(otherData.prabhidik);
		const floorArray = new FloorBlockArray(this.props.permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const designAppr = otherData.designApproval;
		const mapTech = getJsonData(otherData.mapTech);
		let initialValue = prepareMultiInitialValues(
			{
				obj: intialVal,
				reqFields: [],
			},
			{
				obj: {
					miti: getCurrentDate(true),
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					coverageUnit: DEFAULT_UNIT_AREA,
					highTensionUnit: DEFAULT_UNIT_LENGTH,
				},
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['surrounding', 'dhalNikasArrangement'],
			},
			{
				obj: mapTech,
				reqFields: [
					'publicPropertyRequiredDistance',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'highTension',
					'highTensionUnit',
					'coverageArea',
					'coverageDetails',
					'coverageUnit',
				],
			},
			{
				obj: prabidhik,
				reqFields: ['namedMapdanda', 'namedAdhikar', 'requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('nepaliCountName', 'noOfStorey', floorArray.getTopFloor()),
					floor: floorArray.getMultipleInitialValues(['area'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: allPaper,
				reqFields: ['gharNo', 'chalaniNumber', 'allowanceDate'],
			},
			{
				obj: designAppr,
				reqFields: ['buildingClass'],
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValue,
			allPaper,
			floorArray,
			formattedFloors,
		};
	}
	render() {
		const { initialValue, floorArray, formattedFloors } = this.state;
		const { prevData, permitData, userData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div>
				<Formik
					initialValues={initialValue}
					validationSchema={validate}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, errors, setFieldValue, handleSubmit, validateForm, handleChange }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="print-small-font">
								{/* <LetterHeadPhoto userInfo={userData} photo={permitData.photo} /> */}
								<LetterHeadFlex userInfo={userData} />

								<FlexSpaceBetween>
									<div>
									</div>
									<div>
										{reqData.miti}
										<CompactDashedLangDate name="miti" value={values.miti} error={errors.miti} setFieldValue={setFieldValue} />
									</div>
								</FlexSpaceBetween>
								<SectionHeader>
									<h2>{reqData.bisaye}</h2>
								</SectionHeader>
								<br />
								<div>
									<span>{reqData.shir}</span>
									<DashedLangInput/>
									<br />
									
									<span>{reqData.nagarWada}</span>
									<DashedLangInput/>

									<span>{reqData.kittaNo}</span>
									<DashedLangInput/>

									<span>{reqData.content1}</span>
									<DashedLangInput/>

									<span>{reqData.palika}</span>
									<DashedLangInput/>


									<span>{reqData.wadanum}</span>
									<DashedLangInput/>
									
									<span>{reqData.basne}</span>
									<DashedLangInput/>
									
									<span>{reqData.content2}</span>
									<br />
									<br />
									<br />

									<table border="1px" width="100%">
										<caption>{reqData.table1.heading}</caption>
										<thead>
											<tr>
												<th>{reqData.table1.headData.list1}</th>
												<th>{reqData.table1.headData.list2}</th>
												<th>{reqData.table1.headData.list3}</th>
												<th>{reqData.table1.headData.list4}</th>
												<th>{reqData.table1.headData.list5}</th>
												</tr>
										</thead>
										<tbody>
											{reqData.table1.bodyData.map((e) => {
												return <tr>
													<td>{e}</td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
												</tr>
											})}
										</tbody>
									</table>
									<br />
									<br />
									<br />


									<table border="1px" width="100%">
										<caption>{reqData.table2.heading}</caption>
										<thead>
											<tr>
												<th>{reqData.table2.headData.list1}</th>
												<th>{reqData.table2.headData.list2}</th>
												<th>{reqData.table2.headData.list3}</th>
												<th>{reqData.table2.headData.list4}</th>
												<th>{reqData.table2.headData.list5}</th>
												<th>{reqData.table2.headData.list6}</th>
												</tr>
										</thead>
										<tbody>
											<tr>
												<td>&nbsp;</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
									<br />
									<br />


									<span><b>{reqData.note}</b> {reqData.content3}</span>
									<br />
									
									<DashedLangInput/>
									<span>{reqData.wadaKaryalaya}</span>
									
									<DashedLangInput/>
									<span>{reqData.content4}</span>
								</div>
								<FooterSignatureMultiline
									designations={[
										[reqData.gharDhaniKoSahi],
										[reqData.sthalgatNirikxak, reqData.subEngineer],
										[reqData.jachGarneEngineer, reqData.gharNakshaPramukhSakha],
										[reqData.pramanPatraPradanGarne, reqData.pramukhPrasaasaliyaAdhikrit],
										[reqData.phatWalaKoSahi],
									]}
								/>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishCertificateViewKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.buildingFinish).setForm().getParams(),
			new ApiParam(api.allowancePaper, 'allowancePaper').getParams(),
			new ApiParam(api.designApproval, 'designApproval').getParams(),
			new ApiParam(api.prabhidikPratibedhanPesh, 'prabidhik').getParams(),
			new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams(),
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateViewKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishCertificateViewKageyshowri;
