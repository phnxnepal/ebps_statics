import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import EbpsForm, { EbpsNormalForm } from '../../../shared/EbpsForm';
import { getUnitValue } from './../../../../utils/functionUtils';
import { NeighboursNameApplicationData } from '../../../../utils/data/NeighboursNameApplicationData';
import { Formik, getIn } from 'formik';
import { Form, Table, Select } from 'semantic-ui-react';
import TableInput from '../../../shared/TableInput';
import { checkClient, Client } from '../../../../utils/clientUtils';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { validateNumberField } from '../../../../utils/validationUtils';
import { TableInputField } from '../../../shared/TableInput';
import { PuranoGharCertificateSubHeadingWithoutPhoto } from '../certificateComponents/PuranoGharComponents';
import { LetterHeadFlex, LetterHeadPhoto } from '../../../shared/LetterHead';
import { FlexSpaceBetween } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangInput, DashedNormalInput, DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedSelect } from '../../../shared/Select';
import { getJsonData, prepareMultiInitialValues, surroundingMappingFlat, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';

import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { FinishCertificateBiratnagarBlockFloors } from '../certificateComponents/CertificateFloorInputs';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import {
	validateNullableOfficialNumbers,
	validateNullableNepaliDate,
} from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SurroundingArray } from '../../../../utils/surroundingUtils';

import { DashedMultiUnitLengthInput, DashedLengthInputWithRelatedUnits, DashedAreaUnitInput } from '../../../shared/EbpsUnitLabelValue';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { PrintParams } from '../../../../utils/printUtils';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { BlockComponentsV2 } from '../../../shared/formComponents/BlockFloorComponents';
const { object } = require('yup');

const reqData = NeighboursNameApplicationData.structureDesign.Kageyshowri;

const shriShrimanShrimatiOptions = [
	{
		key: reqData.shir,
		text: reqData.shir,
		value: reqData.shir,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shushri,
		text: reqData.shushri,
		value: reqData.shushri,
	},
];
const intialVal = {
	pasa: '',
	chana: '',
	miti: '',
	shriShrimanShrimati: shriShrimanShrimatiOptions[0].value,
	nimnaBamojim: '',
	gharNo: '',
	buildingClass: '',
	surrounding: '',
	nirmanStructuralSystem: '',
	chalaniNumber: '',
	chalaniNumberMiti: '',
	sixNakshaPassNagariBanayekoMiti: '',
};
const validate = object({
	pasa: validateNullableOfficialNumbers,
	chana: validateNullableOfficialNumbers,
	miti: validateNullableNepaliDate,
	allowanceDate: validateNullableNepaliDate,
	noNaksaPassDate: validateNullableNepaliDate,
});

class NeighboursNameApplicationKageyshowriComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, userData, permitData, orgCode, otherData, localDataObject, enterByUser } = props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const serData = getApproveBy(0);
		const erData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const surrArray = new SurroundingArray(permitData.surrounding);
		const floorArray = new FloorBlockArray(permitData.floor);
		const completeSurroundingObject = { surrounding: surrArray.getAllSurrounding() };
		const jsonData = getJsonData(prevData);
		const noticeData = getJsonData(otherData.noticeData);

		const wardOption = localDataObject.wardMaster && localDataObject.wardMaster.map((row) => ({ key: row.id, value: row.name, text: row.name }));
		let prevDataFormatted;
		try {
			if (jsonData.surrounding) {
				const prevSurrounding = new SurroundingArray(getJsonData(prevData).surrounding);
				prevDataFormatted = { ...getJsonData(prevData), surrounding: prevSurrounding.getAllSurrounding() };
			} else {
				prevDataFormatted = getJsonData(prevData);
			}
		} catch {
			prevDataFormatted = {};
		}

		const initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['newWardNo'],
			},
			{
				obj: completeSurroundingObject,
				reqFields: [],
			},
			{
				obj: {
					// default values
					banaune: 'बनाउने',
					parcha: 'पर्छ ?',
					Bdate: getCurrentDate(true),
					floor: floorArray.getMultipleInitialValues(['length', 'width', 'height', 'block', 'floor'], floorArray.getFloors(), false),
					floorUnit: floorArray.getFloorUnit(),
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			// { obj: { ...erInfo, ...serInfo }, reqFields: [] },
			{
				obj: noticeData,
				reqFields: [],
			},
			{
				obj: {
					serName: serData.name,
					serDesignation: serData.designation,
					serSignature: serData.signature,
					erName: erData.name,
					erSignature: erData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		this.state = {
			initialValues,
			floorArray,
			wardOption,
			isKankai: checkClient(orgCode, Client.KANKAI),
			isSundarHaraicha: checkClient(orgCode, Client.SUNDARHARAICHA),
			isBirtamod: checkClient(orgCode, Client.BIRTAMOD),
			isKamalamai: checkClient(orgCode, Client.KAMALAMAI),
			isIllam: checkClient(orgCode, Client.ILLAM),
		};
	}
	render() {
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, userData, useSignatureImage } = this.props;
		const { initialValues, floorArray } = this.state;
		
		return (
			<div>
				<Formik
					initialValues={initialValues}
					validationSchema={validate}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.noticePeriodFor15Days, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, errors, setFieldValue, existingFloors, handleSubmit, validateForm, handleChange }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="print-small-font">
								{/* <LetterHeadPhoto userInfo={userData} photo={permitData.photo} /> */}
								{/* <LetterHeadFlex userInfo={userData} /> */}





								<div>
									<LetterHeadFlex userInfo={userData} />
									<PuranoGharCertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={reqData.buildingTitle}
										textSize={TextSize.MEDIUM}
									/>
									<br />
								</div>


								<br />
								<div>
								
									<span>{reqData.shir}</span>
									<DashedLangInput
										name="shree"
										setFieldValue={setFieldValue}
										value={values.shree}
										error={errors.shree}
									/>
									
									<br />

									{userData.organization.name}
									<span>{reqData.nagarWada}</span>
									{' '}{permitData.newWardNo}{' '}
									

									<span>{reqData.kittaNo}</span>
									{' '}{permitData.kittaNo}{' '}

									<span>{reqData.chhetrafal}</span>
									{' '}{permitData.landArea}{' '}


									<span>{reqData.content1}</span>
									{' '}{permitData.applicantAddress}{' '}


									<span>{reqData.palika}</span>
									{' '}{userData.organization.name}{' '}


									<span>{reqData.wadanum}</span>
									{' '}{permitData.newWardNo}{' '}
									{console.log(permitData)}
									
									<span>{reqData.basne}</span>
									{' '}{permitData.applicantName}{' '}
									
									<span>{reqData.content2}</span>
									<br />
									<br />
									<br />

									<table border="1px" width="100%">
										<caption>{reqData.table1.heading}</caption>
										<thead>
											<tr>
												<th>{reqData.table1.headData.list1}</th>
												<th>{reqData.table1.headData.list2}</th>
												<th>{reqData.table1.headData.list3}</th>
												<th>{reqData.table1.headData.list4}</th>
												<th>{reqData.table1.headData.list5}</th>
												</tr>
										</thead>
										<tbody>
											{reqData.table1.bodyData.map((e, i) => {
												
												return <tr>
													<td>{e}</td>
													<td>
													<TableInput
														setFieldValue={setFieldValue}
															name={`row1${i}`}
															value={values[`row1${i}`]}
															error={errors[`row1${i}`]}
													/>
													</td>
													<td>
														<TableInput
															setFieldValue={setFieldValue}
															name={`row2${i}`}
															value={values[`row2${i}`] ? "" : permitData.surrounding[i] ? permitData.surrounding[i].kittaNo : ""}
															error={errors[`row2${i}`]}
														/>
													</td>
													<td>
														
														<TableInput
															setFieldValue={setFieldValue}
															name={`row3${i}`}
															value={values[`row3${i}`]}
															value={values[`row3${i}`] ? "" : permitData.surrounding[i] ? permitData.surrounding[i].sandhiyar : ""}
														/>
													</td>
													<td>
														<TableInput
															setFieldValue={setFieldValue}
															name={`row4${i}`}
															value={values[`row4${i}`]}
															error={errors[`row4${i}`]}
														/>
													</td>
												</tr>
											})}
										</tbody>
									</table>
									<br />
									<br />
									<br />
									
									<center><b>{reqData.Table_2.heading}</b></center>

									<Table celled className="compact-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={2}>{reqData.Table_2.rows.row1.input_1}</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${reqData.Table_2.rows.row1.input_2} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${reqData.Table_2.rows.row1.input_3} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${reqData.Table_2.rows.row1.input_4} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>{reqData.Table_2.rows.row1.input_5}</Table.HeaderCell>
													<Table.HeaderCell width={2}>{reqData.Table_2.rows.row1.input_6}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{floorArray.getFloorsWithLabels().map((row, input) => (
													<Table.Row key={input}>
														<Table.Cell>
															<EbpsForm
																name={`tapasil_${input}_description`}
																setFieldValue={setFieldValue}
																value={getIn(values, `tapasil_${input}_description`)}
																error={getIn(errors, `tapasil_${input}_description`)}
															/>
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.length`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.length`)}
																error={getIn(errors, `floor.${input}.length`)}
															/>

															{/* <Field
                                                                    name={`tapasil_${input}_length`}
                                                                    value={
                                                                        row.length
                                                                    }
                                                                /> */}
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.width`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.width`)}
																error={getIn(errors, `floor.${input}.width`)}
															/>
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.height`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.height`)}
																error={getIn(errors, `floor.${input}.height`)}
															/>
														</Table.Cell>
														<Table.Cell>
															{/* <Field
																	name={`tapasil_${input}_floorNo`}
																	value={floorMappingFlat.find(fl => fl.floor === row.floor).value}
																/> */}
															{row.label.floorBlockName}
														</Table.Cell>
														<Table.Cell>
															<EbpsForm
																name={`tapasil_${input}_kaifiyat`}
																setFieldValue={setFieldValue}
																value={getIn(values, `tapasil_${input}_kaifiyat`)}
																error={getIn(errors, `tapasil_${input}_kaifiyat`)}
															/>
														</Table.Cell>
													</Table.Row>
												))}
											</Table.Body>
										</Table>




									<br />
									<br />


									<span><b>{reqData.note}</b> {reqData.content3}</span>
									<br />
									<span><b>{reqData.bodhartha}</b></span>
									
									{' '}{permitData.newWardNo}{' '}

									<span>{reqData.wadaKaryalaya}</span>
									
									<DashedLangInput
										name="karyalaya"
										setFieldValue={setFieldValue}
										value={values.karyalaya}
										error={errors.karyalaya}
									/>
									<span>{reqData.content4}</span>
								</div>
							</div>
							<SaveButtonValidation
									errors={errors}
									formUrl={formUrl}
									isSaveDisabled={isSaveDisabled}
									hasDeletePermission={hasDeletePermission}
									hasSavePermission={hasSavePermission}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
									validateForm={validateForm}
								/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const NeighboursNameApplicationKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.noticePeriodFor15Days, 'noticeData').setForm().getParams(),
			new ApiParam(api.allowancePaper, 'allowancePaper').getParams(),
			new ApiParam(api.designApproval, 'designApproval').getParams(),
			new ApiParam(api.prabhidikPratibedhanPesh, 'prabidhik').getParams(),
			new ApiParam(api.buildingFinish, 'buildingFinish').getParams(),
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		prepareData={(data) => data}
		hasFileView={true}
		fetchFiles={true}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <NeighboursNameApplicationKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);
export default NeighboursNameApplicationKageyshowri;
