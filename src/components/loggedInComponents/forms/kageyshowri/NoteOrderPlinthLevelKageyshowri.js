import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { noteOrderPilenthLang } from '../../../../utils/data/noteOrderPilenthLang';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { Formik, getIn, yupToFormErrors } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangInput, DashedNormalInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import { Form } from 'semantic-ui-react';
import { getJsonData, prepareMultiInitialValues, handleSuccess } from '../../../../utils/dataUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { FooterSignature } from '../formComponents/FooterSignature';
import { mappingDirection } from '../../../../utils/data/BuildingBuildCertificateData';
import { validateNormalNumber, validateNullableNormalNumber } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { checkError } from '../../../../utils/dataUtils';
import { DashedMultiUnitAreaInput, DashedMultiUnitLengthInput } from '../../../shared/EbpsUnitLabelValue';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { userdata } from '../../../../utils/data/ebps-setup/userformdata';
import {
	validateNullableNepaliDate,
} from '../../../../utils/validationUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';

const { object } = require('yup');
const validate = object({
	date1: validateNullableNepaliDate,
	date2: validateNullableNepaliDate,
	date3: validateNullableNepaliDate,
	date4: validateNullableNepaliDate,
});
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];
const initialVal = {
	amin: '',
	direction: '',
	road: '',
	roadArea: '',
	areaFromMiddle: '',
	chodnuparneSetBack: '',
	minFromMiddle: '',
	chodekoSetback: '',
	totalChodeko: '',
	buildingHeight: '',
	pratibedan: '',
	juniorEngineer: '',
};

const requiredData = noteOrderPilenthLang.kageyshowri;
const contentData = noteOrderPilenthLang.content;

class NoteOrderPlinthLevelKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		
		const { otherData, prevData, permitData, DEFAULT_UNIT_LENGTH } = this.props;

		const mapTech = getJsonData(otherData.mapTech);
		const prabhidikPratibedhan = getJsonData(otherData.prabhidikPratibedhan);
		const noteOrder = getJsonData(otherData.noteOrder);
		

		const initialValues = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: noteOrder,
				reqFields: [],
			},
			{
				obj: {
					areaFromMiddleUnit: DEFAULT_UNIT_LENGTH,
					minFromMiddleUnit: DEFAULT_UNIT_LENGTH,
					chodekoSetbackUnit: DEFAULT_UNIT_LENGTH,
					totalChodekoUnit: DEFAULT_UNIT_LENGTH,
					buildingHeightUnit: DEFAULT_UNIT_LENGTH,
					chodnuparneSetBackUnit: DEFAULT_UNIT_LENGTH,
					plinthLengthUnit: DEFAULT_UNIT_LENGTH,
				},
				reqFields: [],
			},
			{
				obj: { 
					date: getCurrentDate(true),
					date1: getCurrentDate(true),
					date2: getCurrentDate(true),
					date3: getCurrentDate(true),
					date4: getCurrentDate(true),
				 },
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['surrounding'], // {buildingHeight: 30}
			},
			{
				obj: mapTech,
				reqFields: [
					// 'buildingHeight',
					// 'buildingDetailfloor',
					// 'plinthLength',
					// 'plinthDetails',
					// 'plinthLengthUnit',
					// 'purposeOfConstruction',
					// 'cType',
					// 'buildingDetailfloor',
					// 'coverageDetails',
				], // {buildingHeight: 30}
			},
			{
				obj: prabhidikPratibedhan,
				reqFields: ['surrounding'], // {buildingHeight: 30}
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);

		this.state = { initialValues };
	}

	render() {
		const { userData, permitData, formUrl, hasDeletePermission, isSaveDisabled, prevData, postAction, hasSavePermission, errors: reduxErrors } = this.props;
		const { initialValues } = this.state;

		return (
			<>
				<div>{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}</div>

				<Formik
					initialValues={initialValues}
					validationSchema={validate}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.noteorderPilengthLevel, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleSubmit, values, handleChange, setFieldValue, isSubmitting, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />

								<FlexSingleRight>
									<label htmlFor="date">{requiredData.date}</label>
									<DashedLangDateField
										inline={true}
										name="date"
										setFieldValue={setFieldValue}
										value={values.date}
										error={errors.date}
									/>
								</FlexSingleRight>
								<SectionHeader>
									<h2>{requiredData.subject}</h2>
									<br />
									<h3>{requiredData.heading}</h3>
								</SectionHeader>
								<br />
								<br />
								<div style={{ textAlign: 'justify' }} className="no-margin-field margin-bottom">
									<span >{requiredData.shreeman}</span>
									<br />

									{' '}{requiredData.kathmandu}{' '}
										
									<span >{contentData.content2}</span>
									{userData.organization.name}

									<span >{contentData.gaupalika} / {contentData.nagarpalika} {contentData.content3} </span>
									{' '}{permitData.newWardNo}{' '}
									
									<span >{requiredData.nibedakKoNaam}</span>
									
									{contentData.shree}
									{permitData.applicantName}
									
									<span >{contentData.content5} </span>

									{permitData.applicantName}

									<span >{requiredData.content1}</span>
									{userData.organization.address}

									<span >{requiredData.content1_1}</span>
									{permitData.newWardNo}
									
									<span >{requiredData.kittaNo}</span>
									{permitData.kittaNo}
									
									<span >{requiredData.cheetrafal}</span>
									{permitData.landArea}
									
									<span >{requiredData.bimiko}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="bimiko"
										value={values.bimiko}
										error={errors.bimiko}
									/>
									
									<span >{requiredData.content2}</span>
			
									<DashedLengthInputWithRelatedUnits
										name="kendra"
										unitName="floorUnit"
										relatedFields={[]}
									/>



									<span > {requiredData.content3} {contentData.direction1}</span>

									<DashedLangInput
										name={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : "dir0"}
										setFieldValue={setFieldValue}
										value={permitData.surrounding[0] != undefined ? permitData.surrounding[0].sandhiyar : ""}
										error={errors.landArea}
									/>

									<span >{contentData.direction2}</span>
									<DashedLangInput
										name={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : "dir1"}
										setFieldValue={setFieldValue}
										value={permitData.surrounding[1] != undefined ? permitData.surrounding[1].sandhiyar : ""}
										error={errors.landArea}
									/>

									<span >{contentData.direction3}</span>
									<DashedLangInput
										name={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : "dir2"}
										setFieldValue={setFieldValue}
										value={permitData.surrounding[2] != undefined ? permitData.surrounding[2].sandhiyar : ""}
										error={errors.landArea}
									/>

									<span >{contentData.direction4}</span>
									<DashedLangInput
										name={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : "dir3"}
										setFieldValue={setFieldValue}
										value={permitData.surrounding[3] != undefined ? permitData.surrounding[3].sandhiyar : ""}
										error={errors.landArea}
									/>


									<span >{requiredData.content4}</span>

									<DashedLengthInputWithRelatedUnits
										name="landLength"
										unitName="floorUnit"
										relatedFields={[]}
									/>{' '}

									<span >{contentData.length2}</span>

									<DashedLengthInputWithRelatedUnits
										name="landWidth"
										unitName="floorUnit"
										relatedFields={[]}
									/>

									<span >{contentData.length3}</span>

									<DashedLengthInputWithRelatedUnits
										name="ucahi"
										unitName="floorUnit"
										relatedFields={[]}
									/>


									{/* sad */}
									
									<DashedNormalInput
										setFieldValue={setFieldValue}
										name="buildingFloorHeight"
										value={values.buildingFloorHeight}
										error={errors.buildingFloorHeight}
									/>
									
									<span >{contentData.length4}</span>
									<span >{contentData.content15} {requiredData.yearPrefix1}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="saalBanne"
										value={values.saalBanne}
										error={errors.saalBanne}
									/>
									
									<span >{contentData.content16} {requiredData.banekoBanne}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="banne"
										value={values.banne}
										error={errors.banne}
									/>
									
									<DashedLangDateField
										inline={true}
										label={requiredData.content5}
										name="date1"
										setFieldValue={setFieldValue}
										value={values.date1}
										error={errors.date1}
									/>
									
									<span >{requiredData.content6}</span>
									<br />
									<span >{requiredData.content7}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="chaNo"
										value={values.chaNo}
										error={errors.chaNo}
									/>
									{' '}
									<DashedLangDateField
										inline={true}
										label={contentData.miti}
										name="date2"
										setFieldValue={setFieldValue}
										value={values.date2}
										error={errors.date2}
									/>
									
									<span >{requiredData.content8}</span>{' '}
									<DashedLangDateField
										inline={true}
										label={contentData.miti}
										name="date3"
										setFieldValue={setFieldValue}
										value={values.date3}
										error={errors.date3}
									/>
									
									<span >{requiredData.content9}</span>{' '}
									<DashedLangDateField
										inline={true}
										label={contentData.miti}
										name="date4"
										setFieldValue={setFieldValue}
										value={values.date4}
										error={errors.date4}
									/>
									
									<span >{requiredData.content10}</span>
									{` ${permitData.applicantName} `}
									
									<span >{requiredData.content11}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="personName1"
										value={values.personName1}
										error={errors.personName1}
									/>
									
									<span >{requiredData.content12}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="personName2"
										value={values.personName2}
										error={errors.personName2}
									/>
									
									<span >{requiredData.bata} {contentData.miti} {requiredData.yearPrefix2}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="miti2"
										value={values.miti2}
										error={errors.miti2}
									/>
									
									<span >{requiredData.content13}</span>
									<DashedLangInput
										setFieldValue={setFieldValue}
										name="bamojak"
										value={values.bamojak}
										error={errors.bamojak}
									/>
									
									<span >{requiredData.content14}</span>
									{requiredData.nakshaPass}
									<FooterSignature
										designations={[
											requiredData.subEng,
											requiredData.engineer,
											requiredData.strEng,
											requiredData.pramukh,
										]}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={false}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const NoteOrderPlinthLevelKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.noteorderPilengthLevel, 'noteOrder').setForm().getParams(),
			new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams(),
			new ApiParam(api.prabhidikPratibedhanPesh, 'prabhidikPratibedhan').getParams(),
	]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
			...PrintParams.INLINE_FIELD,
		}}
		prepareData={(data) => data}
		fetchFiles={true}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={(props) => <NoteOrderPlinthLevelKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);
export default NoteOrderPlinthLevelKageyshowri;
