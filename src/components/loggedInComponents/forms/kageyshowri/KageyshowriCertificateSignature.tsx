import React from 'react';
import { footerSignature } from '../../../../utils/data/genericFormData';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';

export const KageyshowriCertificateSignature = ({ images, subName }: { images: [], subName : {
	prabesh: "",
	sifaris: "",
	jach: "",
	suekrit: ""
} }) => {
	
	return (
		<FooterSignatureMultiline
			designations={[
				[footerSignature.ser, `(${subName.prabesh})`],
				[footerSignature.er, `(${subName.sifaris})`],
				[footerSignature.structureEr, `(${subName.jach})`],
				[footerSignature.chief, `(${subName.suekrit})`],
			]}
			signatureImages={images}
		/>
	);
};
