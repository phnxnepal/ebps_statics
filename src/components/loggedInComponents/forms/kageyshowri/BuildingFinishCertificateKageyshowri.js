import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { CompactDashedLangDate, DashedLangDateField } from '../../../shared/DateField';
import * as Yup from 'yup';
import { DashedLangInput, CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { Table } from 'semantic-ui-react';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { getCurrentDate } from './../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { FloorTable, FloorTableHeader } from './FloorTable';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { translateEngToNep } from '../../../../utils/langUtils';
import { LetterHeadFlex, LetterHeadPhoto } from '../../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { PuranoGharCertificateSubHeadingWithoutPhoto } from '../certificateComponents/PuranoGharComponents';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { PrintParams } from '../../../../utils/printUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { ShreeDropdown } from './../formComponents/ShreeDropdown';
import { shreeOptions } from '../../../../utils/optionUtils';
import EbpsTextareaField from './../../../shared/MyTextArea';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { CertificateFloorBlockBorderlessTable, HeightFloorNumberSection } from '../certificateComponents/CertificateFloorBlockInputs';

const naaya = BuildingFinishCertificateData.naya;
const kageyshowriData = BuildingFinishCertificateData.kageyshowri;

const convertbib = (inputIndex) => {
	if (inputIndex === 'A') {
		return '(क)';
	} else if (inputIndex === 'B') {
		return '(ख)';
	} else if (inputIndex === 'C') {
		return '(ग)';
	} else {
		return '(घ)';
	}
};
const puranoGharNirmanSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNullableNepaliDate,
		patraDate: validateNullableNepaliDate,
		paasDate: validateNullableNepaliDate,
		talaSwikriti: validateNullableNepaliDate,
	})
);
class BuildingKageyshowri extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};
		const prabhidik = getJsonData(otherData.prabhidik);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const anuSucKaJsonData = otherData.anukaMaster;
		const desApprovJsonData = otherData.designApprovalData;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let inputIndex = buildingClass ? buildingClass.id : '';

		initialValues.sadakUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.buildingAreaUnit = DEFAULT_UNIT_AREA;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionUnit = DEFAULT_UNIT_LENGTH;
		initialValues.nirmanDate = translateEngToNep(permitData.applicantDateBS);

		// const floorArray = new FloorArray(permitData.floor);
		// const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		// const floorMax = topFloor.nepaliCount;
		// const highNepaliFloor = topFloor.nepaliName;
		// const bottomNepaliFloor = bottomFloor.nepaliName;
		const floorArray = new FloorBlockArray(permitData.floor);
		const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['photo', 'dhalNikasArrangement'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', topFloor),
					...floorArray.getInitialValue('nepaliName', 'startFloor', bottomFloor),
					...floorArray.getInitialValue('nepaliName', 'endFloor', topFloor),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: [
					'allowable',
					'coveragePercent',
					'purposeOfConstruction',
					'roof',
					'buildingHeight',
					'sadakAdhikarKshytra0',
					'requiredDistance0',
					'sadakAdhikarUnit',
					'highTensionUnit',
					'highTension',
				],
			},
			{
				obj: prabhidik,
				reqFields: ['publicPropertyRequiredDistance', 'publicPropertyUnit', 'publicPropertyDistance'],
			},
			// { obj: { startFloor: bottomNepaliFloor }, reqFields: [] },
			// { obj: { endFloor: highNepaliFloor }, reqFields: [] },
			// { obj: { floorNumber: floorMax }, reqFields: [] },
			// { obj: { buildingArea: floorArray.getSumOfAreas() }, reqFields: [] },
			{
				obj: {
					buildclass: inputIndex,
					shree: shreeOptions[0].value,
				},
				reqFields: [],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues1.date1)) {
			initialValues1.date1 = getCurrentDate(true);
		}

		const initVal = merge(initialValues, initialValues1);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
			blocks: floorArray.getBlocks(),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray, formattedFloors, blocks } = this.state;
		const getSpecificFields = (e, i) => {
			if(e == "FEET" || e =="METRE"){

				return <DashedLengthInputWithRelatedUnits
					compact={true}
					name={`data${i}`}
					unitName="floorUnit"
					relatedFields={[]}
				/>
			}else if(e == "%"){
				return <DashedLangInput name="" name={`data1${i}`} compact={true} />
			}else{
				return <DashedLangInput name="" name={`data2${i}`} compact={true} />
			}
		}
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={puranoGharNirmanSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadPhoto photo='photo' userInfo={userData} />
									<PuranoGharCertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={kageyshowriData.buildingTitle}
										textSize={TextSize.MEDIUM}
									/>
									<br />
									
									<div>

										<span><ShreeDropdown name="shree" /></span>
										{' '}{permitData.applicantName}{' '}


										<br />
										{' '}{userData.organization.name}{' '}
										<span>{kageyshowriData.content.content_1}</span>
										{' '}{permitData.newWardNo}{' '}

										<br />

										<span>{kageyshowriData.content.content_2}</span>
										<DashedLangDateField
											inline={true}
											name="date1"
											value={values.date1}
											error={errors.date1}
											setFieldValue={setFieldValue}
										/>

										<span>{kageyshowriData.content.content_3}</span>

										<br />
										<br />



										<table border="1px" width="100%" style={{textAlign: 'center'}}>
											<caption>{kageyshowriData.content.table1.caption}</caption>
											<thead>
												<tr>
													<th colspan="2">{kageyshowriData.content.table1.headings.head1}</th>
													<th>{kageyshowriData.content.table1.headings.head2}</th>
													<th>{kageyshowriData.content.table1.headings.head3}</th>
													<th>{kageyshowriData.content.table1.headings.head4}</th>
													<th>{kageyshowriData.content.table1.headings.head5}</th>
													<th>{kageyshowriData.content.table1.headings.head6}</th>
													<th>{kageyshowriData.content.table1.headings.head7}</th>
												</tr>
											</thead>

											<tbody>
												<tr>
													<td>{kageyshowriData.content.table1.data.data_1}</td>
													<td>{permitData.newWardNo}</td>
													<td>b</td>
													<td>	
														<DashedLangInput
															compact={true}
															name="data99"
														/>
														<span>{kageyshowriData.content.table1.data.bami}</span>
													</td>
													<td rowspan="2">
														<DashedLangInput />
													</td>
													<td rowspan="2">
														{kageyshowriData.content.table1.data.chetra.map(e => {
															return <>
																{e.map(f => {
																	return <label>
																		<input type="checkbox" />
																		{`  ${f}   `}
																	</label>
																})}
																<br />
															</>
														})}
													</td>
													<td rowspan="2">
														{kageyshowriData.content.table1.data.prayojana.map(e => {
															return <>
																{e.map(f => {
																	return <label>
																		<input type="checkbox" />
																		{`  ${f}   `}
																	</label>
																})}
																<br />
															</>
														})}
													</td>
													<td rowspan="2">
														{kageyshowriData.content.table1.data.batoList.map((e) => {
															return <>
																<label>
																	<input type="checkbox" />
																	{`  ${e}`}
																</label>
																<br />
															</>
														})}
													</td>
												</tr>
												<tr>
													<td>{kageyshowriData.content.table1.data.data_2}</td>
													<td>{permitData.oldWardNo}</td>
													<td>&nbsp;</td>
													<td>
														<DashedLangInput />
														<span>{kageyshowriData.content.table1.data.bami}</span>
													</td>
												</tr>
												<tr>
													<td colspan="3">{kageyshowriData.content.table1.data.bottomFields.list1}</td>
													{kageyshowriData.content.table1.data.bottomFields.other.map(e => {
														return <td>
																<label>
																	<input type="checkbox" />
																	{`  ${e}`}
																</label>
															</ td>
													})}
												</tr>
											</tbody>
										</table>
										
										<br />
										<br />
										<br />

											<table border="1px" width="100%" style={{textAlign: 'center'}}>
											<caption>{kageyshowriData.content.table3.caption}</caption>
												<thead>
													
													<tr>
														<th rowspan="2">{kageyshowriData.content.table3.headings.head1}</th>
														<th rowspan="2">{kageyshowriData.content.table3.headings.head2}</th>
														<th rowspan="2">{kageyshowriData.content.table3.headings.head3}</th>
														<th rowspan="2">{kageyshowriData.content.table3.headings.head4}</th>
														<th colspan="2">{kageyshowriData.content.table3.headings.head5}</th>
														<th>{kageyshowriData.content.table3.headings.head6}</th>
													</tr>
													<tr>
														<th>{kageyshowriData.content.table3.headings.head7}</th>
														<th>{kageyshowriData.content.table3.headings.head8}</th>
														<th>&nbsp;</th>
													</tr>
												</thead>

												<tbody>	
													{kageyshowriData.content.table3.data.allDatas.map((e, i) => <tr>
															<td>{e.karNo}</td>
															<td>{e.bibarad}</td>
															<td>{getSpecificFields(e.maapdhanda, i)}</td>
															<td>{getSpecificFields(e.shuekriti, i)}</td>
															<td>{getSpecificFields(NaN, i)}</td>
															<td>{getSpecificFields(NaN, i)}</td>
															<td>{getSpecificFields(NaN, i)}</td>
														</tr>
													)}
												</tbody>
											</table>


										<br />
										<br />


										<Table celled compact collapsing>
												<FloorTableHeader
													errors={errors}
													values={values}
													setFieldValue={setFieldValue}
												/>
												<FloorTable 
													errors={errors}
													values={values}
													setFieldValue={setFieldValue}
													handleChange={handleChange}
												 	isEdit={true} 
												 />
										</Table>





									</div>

									<div>

										
										<FooterSignatureMultiline designations={kageyshowriData.content.content_16} />
										<br />
										
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishCertificateKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},

			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingKageyshowri {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishCertificateKageyshowri;
