import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { FlexSingleLeft, FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { formStructure } from '../../../../utils/data/kageyshowri/newForm';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { Formik } from 'formik';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { Form } from 'semantic-ui-react';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { PuranoGharCertificateSubHeadingWithoutPhoto } from '../certificateComponents/PuranoGharComponents';
import { getSaveByUserDetails, getApproveByObject } from '../../../../utils/formUtils';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { SectionHeader } from '../../../uiComponents/Headers';
import {
	validateNullableOfficialNumbers,
	validateNullableNepaliDate,
} from '../../../../utils/validationUtils';

const { object } = require('yup');

const validate = object({
	date1: validateNullableNepaliDate,
});

const kageyshowriData = formStructure.certificatePratibedan;

export class CertificatePratibedanSambandhamaaKageshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, userData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;

		const jsonData = getJsonData(prevData);


		// const prabidhikPratibedan = getJsonData(otherData.prabidhikPratibedan);
		let initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			
			// {
			// 	obj: prabidhikPratibedan,
			// 	reqFields: ['floor'],
			// },
			
			{
				obj: jsonData,
				reqFields: [],
			},
		);

		if (isStringEmpty(initialValues.Bdate)) {
			initialValues.Bdate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		this.state = {
			initialValues,
		};
	}
	render() {
		const {
			userData,
			permitData,
			prevData,
			formUrl,
			hasSavePermission,
			hasDeletePermission,
			isSaveDisabled,
		} = this.props;
		const { initialValues } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={validate}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.PratibeydanSambandhama, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="view-Form print-small-font">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<PuranoGharCertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={{Bdate: 'मिति '}}
										textSize={TextSize.MEDIUM}
									/>
									
			


										<SectionHeader>
											<h3 className="underline">{kageyshowriData.heading}</h3>
										</SectionHeader>
									<div>
										<br />
										<br />
										{kageyshowriData.top1}
										<DashedLangInput 
											name="top1"
											setFieldValue={setFieldValue}
											value={values.top1}
											error={errors.top1}
										/>
										<br />
										{kageyshowriData.wardNo}{' '}{permitData.newWardNo}{' '}{kageyshowriData.koKaryalaya},
										<br />
										{userData.organization.name}
										<br />
										{userData.organization.address}{' '}, {userData.organization.province}, {kageyshowriData.nepal}
										<br /><br /><br />
									
										{kageyshowriData.content1}
										<DashedLangDateField
											name="date1"
											inline={true}
											value={values.date1}
											error={errors.date1}
											setFieldValue={setFieldValue}
											handleChange={handleChange}
										/>
										{' '}{kageyshowriData.maShree}{' '}
										{' '}{permitData.applicantName}{' '}
										{' '}{kageyshowriData.content2}{' '}
										{' '}{permitData.newWardNo}{' '}
										{' '}{kageyshowriData.koKitta}{' '}
										{' '}{permitData.kittaNo}{' '}
										{' '}{kageyshowriData.chhetraFal}{' '}
										{' '}{permitData.landArea}{' '}
										{' '}{kageyshowriData.content3}{' '}


									</div>
									<br /><br />
									<FlexSingleRight>
										<DashedLangInput
											name="input1"
											value={values.input1}
											error={errors.input1}
											setFieldValue={setFieldValue}
											handleChange={handleChange}
										/>
									</FlexSingleRight>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const CertificatePratibedanSambandhamaaKageshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.PratibeydanSambandhama, objName: 'pratibeydan', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		fetchFiles={true}
		hasFileView={true}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <CertificatePratibedanSambandhamaaKageshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default CertificatePratibedanSambandhamaaKageshowri;
