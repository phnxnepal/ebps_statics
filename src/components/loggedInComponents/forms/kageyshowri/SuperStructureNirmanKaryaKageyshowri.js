import React, { Component, useState } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import * as Yup from 'yup';
import { PlinthLevelTechLangKageyshowri, kageyshowriData } from '../../../../utils/data/PilenthTechAppLang';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput, CompactDashedLangInput, DashedNormalInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate, DashedLangDateField } from '../../../shared/DateField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import { UserOldInfo, HaalSection } from '../formComponents/PermitInfo';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { plinthLevelOwnerData } from '../../../../utils/data/plinthLevelFormsData';
import { validateNullableNepaliDate, validateNullableNumber } from '../../../../utils/validationUtils';
import { olddosrocharanPrabidhikview } from '../../../../utils/data/mockLangFile';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

const PlinthTechLang = PlinthLevelTechLangKageyshowri;
const dpv = olddosrocharanPrabidhikview;
const userInfo = plinthLevelOwnerData.userInfo;

const plinthLevelAppSchema = {
	plinthApprovalDate: validateNullableNepaliDate,
	date: validateNullableNepaliDate,
	naksaDastur: validateNullableNumber,
	thapGharDastur: validateNullableNumber,
};

const schema = Yup.object().shape(Object.assign({ ...yesNoSectionSchema, ...plinthLevelAppSchema }));

class SuperStructureNirmanKaryaKageyshowriComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, enterByUser } = this.props;
		const json_data = getJsonData(prevData);
		const plinthOwner = getJsonData(otherData.plinthOwner);
		const superNirmanKarya = getJsonData(otherData.superNirmanKarya);
		const dosrocharanAbedan = getJsonData(otherData.dosrocharanAbedan);
		const prabhidikPratibedhan = getJsonData(otherData.prabhidikPratibedhan);
		
		
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		
		const initialValues = prepareMultiInitialValues(
			{
				
				obj: {
					date1 : dosrocharanAbedan.date1 ? dosrocharanAbedan.date1 : "",
					lambae : prabhidikPratibedhan.lambae ? prabhidikPratibedhan.lambae : "",
					chaudahi : prabhidikPratibedhan.chaudahi ? prabhidikPratibedhan.chaudahi : "",
					uchahi : prabhidikPratibedhan.uchahi ? prabhidikPratibedhan.uchahi : "",
					headDate: getCurrentDate(true),
					date2: getCurrentDate(true),
					date: getCurrentDate(true),
					naksaDastur: '',
					thapGharDastur: '',
				},
				reqFields: [],
			},
			{
				obj: serInfo,
				reqFields: [],
			},
			{ obj: plinthOwner, reqFields: [] },
			{ obj: json_data, reqFields: [] },
			{ obj: superNirmanKarya, reqFields: [] },

		);
		this.state = { ...initialValues, answers: {"data1" : false, "data2" : false, "data3" : false, "data4" : false, "data5" : false} };
	}

	render() {

		const { errors: reduxErrors, permitData, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const initialValues  = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// console.log('log');
						try {
							await this.props.postAction(`${api.supernirmaanKarya}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleSubmit, handleChange, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>



								<SectionHeader>
									<h3 className="underline end-section">{dpv.kageyshowriData.heading}</h3>
								</SectionHeader>



								<LetterSalutation
									lines={[letterSalutation.prashasakhiya, `${userData.organization.name} ${letterSalutation.karyalaya}`, `${userData.organization.address}, ${userData.organization.province}, ${dpv.kageyshowriData.nepal}`]}
								/>
								<div className="flex-item-right">
										{kageyshowriData.date}
										<DashedLangDateField
											compact={true}
											name="headDate"
											value={values.headDate}
											error={errors.headDate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</div>
									
								<span>{PlinthTechLang.shree}</span>
								{' '}{permitData.applicantName}{' '}

								<span>{PlinthTechLang.content1}</span>
								{' '}{permitData.oldMunicipal}{' '}

								<span>{PlinthTechLang.gabisha}</span>
								{' '}{permitData.newWardNo}{' '}

								<span>{PlinthTechLang.content2}</span>
								{' '}{userData.organization.name}{' '}

								<span>{PlinthTechLang.content2_1}</span>
								{' '}{permitData.newWardNo}{' '}

								<span>{PlinthTechLang.toll} {permitData.sadak} </span>


								<span>{PlinthTechLang.chhetrafal}</span>
								{' '}{permitData.landArea}{' '}

								<span>{PlinthTechLang.content3}</span>
								<DashedLangDateField 
									inline={true}
									name="date1"
									value={values.date1}
									error={errors.date1}
									setFieldValue={setFieldValue}
								/>


								<span>{PlinthTechLang.content4}</span>

								<br/>
								<br/>

								{PlinthTechLang.questions.map((e, j) => {
									return <div className="frmCheckbox-wrap">
										{e} {' '}
										{PlinthTechLang.answers.map((f, i) => {
											return <>
												<div key={f} className="ui radio checkbox">
													<input
														type="radio"
														name={`radioData_${j}`}
														value={f}
														defaultChecked={values[`radioData_${j}`] === f}
														onChange={handleChange}
														onClick={() => {
															if(PlinthTechLang.answers[1] == f){
																const dataName = this.state.answers
																dataName[`data${j}`] = true
																this.setState({...this.state, answers : dataName})
															}
														}}
													/>
													<label>{f}</label>
												</div>
												{PlinthTechLang.answers[1] == f && this.state.answers[`data${j}`] && (
													<>
													<br />
														{PlinthTechLang.chhainaBhaneBibarad}
														<DashedNormalInput 
															name={`textData_${j}`}
															value={values[`textData_${j}`]}
															error={errors[`textData_${j}`]}
															onChange={handleChange}
															setFieldValue={setFieldValue}
														/>
													</>
												)}
												{' '}
											</>
										})}
										{' '}
										
									</div>	
								})}

								<br />
								<br />

								<span>{PlinthTechLang.karyalaya}</span>
								<DashedLangInput/>

								<span>{PlinthTechLang.date} </span>
								<DashedLangDateField 
									inline={true}
									name="date2"
									value={values.date2}
									error={errors.date2}
									setFieldValue={setFieldValue}
								/>
								
								<span>{PlinthTechLang.content5}</span>
								{' '}{permitData.newWardNo}{' '}

								<span>{PlinthTechLang.basneShree}</span>
								{' '}{permitData.applicantName}{' '}

								<span>{PlinthTechLang.ko} {PlinthTechLang.kittaNo}</span>
								{' '}{permitData.kittaNo}{' '}

								<span>{PlinthTechLang.chhetrafal}</span>
								{' '}{permitData.landArea}{' '}


								<span>{PlinthTechLang.bimaLambae} {PlinthTechLang.lambae}</span>

								<DashedLengthInputWithRelatedUnits
									name="lambae"
									unitName="floorUnit"
									relatedFields={["chaudahi", "uchahi"]}
								/>{' '}

								<span>{PlinthTechLang.chaudahi}</span>

								<DashedLengthInputWithRelatedUnits
									name="chaudahi"
									unitName="floorUnit"
									relatedFields={["uchahi", "lambai"]}
								/>

								<span>{PlinthTechLang.uchahi}</span>

								<DashedLengthInputWithRelatedUnits
									name="uchahi"
									unitName="floorUnit"
									relatedFields={["lambai", "chaudahi"]}
								/>

								<span>{PlinthTechLang.content6}</span>

								<br/>

								<span>{PlinthTechLang.aanya}</span>
								
								<br/>
								<br/>
								

								<div className="margin-bottom">
									<div>
										{console.log(userData)}
										{kageyshowriData.signatureSection.techName} {' '}{userData.info.userName} {' '} 
										
									</div>
									<br/>

									<div>
										{kageyshowriData.signatureSection.signature} {' '}
										<DashedLangInput name="signature" />
									</div>
									<br/>
									<div>
										{kageyshowriData.signatureSection.level} {' '}{userData.info.educationQualification} {' '}
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const SuperStructureNirmanKaryaKageyshowri = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.supernirmaanKarya,
				objName: 'superNirmanKarya',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
			{
				api: api.DosrocharanAbedan,
				objName: 'dosrocharanAbedan',
				form: true,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: true,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui checkbox', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByTagName', 'input', 'value'],
		}}
		render={props => <SuperStructureNirmanKaryaKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureNirmanKaryaKageyshowri;
