import React, { Component } from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { getJsonData, getApprovalData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { Formik, getIn } from 'formik';
import { SurjaminMuchulkaViewKageyshowriData } from '../../../../utils/data/SurjaminMuchulkaViewKageyshowriData';
import { SectionHeader } from '../../../uiComponents/Headers';
import { getCurrentDate } from '../../../../utils/dateUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { ApiParam } from '../../../../utils/paramUtil';
import { DashedLangDateField } from '../../../shared/DateField';
import api from '../../../../utils/api';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getUnitValue } from '../../../../utils/functionUtils';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { DashedSelect } from '../../../shared/Select';
import { PuranoGharCertificateSubHeadingWithoutPhoto } from '../certificateComponents/PuranoGharComponents';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { PrintParams } from '../../../../utils/printUtils';
import { DashedDateField } from '../../../shared/DateField';
import { initialValue } from '../biratnagar/structureDesignBComponents/initivalValues';

const reqData = SurjaminMuchulkaViewKageyshowriData;

class SurjaminMuchulkaViewKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData } = this.props;
		
		const mapTech = getJsonData(otherData.mapTech);
		const surjamin = getJsonData(otherData.surjamin);
		let initVal = prepareMultiInitialValues(
			{
				obj: mapTech,
				reqFields: [],
			},
			{
				obj: surjamin,
				reqFields: [],
			},
		);
		if (isStringEmpty(initVal.Bdate)) {
			initVal.Bdate = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, userData, postAction, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, prevData } = this.props;
		const { initVal } = this.state;
		
		return (
			<Formik
				initialValues={initVal}
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);

					values.applicationNo = permitData.applicantNo;

					try {
						await postAction(api.surjaminMuchulka, values, true);

						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, handleSubmit, errors, handleChange, validateForm, values, setFieldValue }) => (
					<div>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}

						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />
								<PuranoGharCertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={reqData.toSet}
										textSize={TextSize.MEDIUM}
									/>

								<SectionHeader>
									<h2 className="underline">{reqData.subject}</h2>
								</SectionHeader>
								<br />
								<br />
								{reqData.sachib}
								<br />
								{reqData.wardNo}{' '}{permitData.newWardNo}{' '}{reqData.koKaryalaya}
								<br />
								{userData.organization.name}
								<br />
								{`${userData.organization.address}, ${userData.organization.province}, ${reqData.nepal}`}
								{/* {reqData.topList.map((e) => <><span>{e}</span><br /></>)} */}
								<br />
								<br />
								<span>{reqData.content1}</span>
								{' '}{userData.organization.address}{' '}

								<span>{reqData.palika}</span>
								{' '}{userData.organization.name}{' '}

								<span>{reqData.wada}</span>
								{' '}{permitData.newWardNo}{' '}


								<span>{reqData.basne}</span>
								{' '}{permitData.applicantName}{' '}


								<span>{reqData.lesabik}</span>
								{' '}{permitData.oldMunicipal}{' '}

								<span>{reqData.gabisa}</span>
								{' '}{permitData.oldWardNo}{' '}
								

								<span>{reqData.content2}</span>
								{' '}{permitData.newWardNo}{' '}


								<span>{reqData.ko}</span>
								{' '}{permitData.sadak}{' '}


								<span>{reqData.tolko}</span>
								{' '}{permitData.kittaNo}{' '}

								<span>{reqData.chettrafal}</span>
								{' '}{permitData.landArea}{' '}

								<span>{reqData.bhayekoJagga}</span>

								<DashedLangInput 
									name="bhayeko"
									setFieldValue={setFieldValue}
									value={values.bhayeko ? values.bhayeko : permitData.nibedakSadak ? permitData.nibedakSadak : permitData.sadak ? permitData.sadak : ""}
									error={errors.bhayeko}
								/>
								{' '}{reqData.content3}{' '}
								<DashedLangDateField
									inline={true}
									compact={true}
									name="date2"
									setFieldValue={setFieldValue}
									value={values.date2}
									error={errors.date2}
								/>

								<span>{reqData.content4}</span>
								<DashedLangInput 
									name="banaune"
									setFieldValue={setFieldValue}
									value={values.banaune}
									error={errors.banaune}
								/>
								<span>{reqData.content4_1}</span>

								<br />

								<h3>{reqData.bodhartha}</h3>
								<span>{reqData.content5}</span>

								<br/>
								<span>{reqData.wada}</span>

								{' '}{permitData.newWardNo}{' '}
								<span>{reqData.content6}</span>



							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					</div>
				)}
			</Formik>
		);
	}
}
const SurjaminMuchulkaViewKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.surjaminMuchulka,
				objName: 'surjamin',
				form: true,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'surjamin',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		onBeforeGetContent={{ ...PrintParams.TEXT_AREA, ...PrintParams.DASHED_INPUT }}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SurjaminMuchulkaViewKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);
export default SurjaminMuchulkaViewKageyshowri;
