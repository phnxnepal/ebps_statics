import React from 'react';
import { Table } from 'semantic-ui-react';
import { floorMappingFlat, distanceOptions } from '../../../../utils/dataUtils';
import { getIn } from 'formik';
import { TableInputField } from '../../../shared/TableInput';
import { calculateArea } from '../../../../utils/mathUtils';
import { validateNumberField } from '../../../../utils/validationUtils';
import { UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';

const floorDetails = BuildingFinishCertificateData.kageyshowri.floor_details;

export const FloorTable = ({ values, setFieldValue, errors, handleChange, existingFloors }) => {
	return (
		<Table.Body>
			{floorMappingFlat.map((floor, floorDetailIndex) => {
				const isDisabled = existingFloors ? getIn(values, `floor[${floorDetailIndex}].floor`) !== undefined : false;
				const isGroundFloor = floor.floor === 1 || false;
				const length = getIn(values, `floor[${floor.floor}].length`)
				const width = getIn(values, `floor[${floor.floor}].width`)
				const height = getIn(values, `floor[${floor.floor}].height`)
				const area = getIn(values, `floor[${floor.floor}].area`)
				if(!length && !width && !height && !area){
					return (<></>)
				}
				return (
					<Table.Row key={floorDetailIndex}>
						<Table.Cell>{floor.value}</Table.Cell>
						<Table.Cell>
							<TableInputField
								disabled={isDisabled}
								name={`floor[${floor.floor}].length`}
								onChange={
									isDisabled
										? undefined
										: (e) => {
												setFieldValue(`floor[${floor.floor}].length`, e.target.value);
												setFieldValue(
													`floor[${floor.floor}].area`,
													calculateArea(e.target.value, getIn(values, `floor[${floor.floor}].width`))
												);
										  }
								}
								value={getIn(values, `floor[${floor.floor}].length`)}
								error={getIn(errors, `floor[${floor.floor}].length`)}
								validate={isGroundFloor ? validateNumberField : undefined}
							/>
							
						</Table.Cell>
						<Table.Cell>
							<TableInputField
								disabled={isDisabled}
								name={`floor[${floor.floor}].width`}
								onChange={
									isDisabled
										? undefined
										: (e) => {
												setFieldValue(`floor[${floor.floor}].width`, e.target.value);
												setFieldValue(
													`floor[${floor.floor}].area`,
													calculateArea(e.target.value, getIn(values, `floor[${floor.floor}].length`))
												);
										  }
								}
								value={getIn(values, `floor[${floor.floor}].width`)}
								error={getIn(errors, `floor[${floor.floor}].width`)}
								validate={isGroundFloor ? validateNumberField : undefined}
							/>
						</Table.Cell>
						<Table.Cell>
							<TableInputField
								disabled={isDisabled}
								name={`floor[${floor.floor}].height`}
								value={getIn(values, `floor[${floor.floor}].height`)}
								onChange={isDisabled ? undefined : handleChange}
								error={getIn(errors, `floor[${floor.floor}].height`)}
								validate={isGroundFloor ? validateNumberField : undefined}
							/>
						</Table.Cell>
						<Table.Cell>
							<TableInputField
								disabled={isDisabled}
								name={`floor[${floor.floor}].area`}
								value={getIn(values, `floor[${floor.floor}].area`)}
								onChange={isDisabled ? undefined : handleChange}
								error={getIn(errors, `floor[${floor.floor}].area`)}
								validate={isGroundFloor ? validateNumberField : undefined}
							/>
						</Table.Cell>
					</Table.Row>
				);
			})}
		</Table.Body>
	);
};

export const FloorTableHeader = ({ values, errors, isCommercial }) => {
	const relatedFields =
		values.floor &&
		values.floor.map((row, index) => [`floor.${index}.length`, `floor.${index}.height`, `floor.${index}.width`, `floor.${index}.area`]).flat();

	const floorError = (errors && errors.floor && typeof errors.floor === 'string') || values.floor.length < 1;
	return (
		<Table.Header>
			<Table.Row>
				<Table.HeaderCell></Table.HeaderCell>
				<Table.HeaderCell colSpan="4">{floorDetails.heading0}</Table.HeaderCell>
			</Table.Row>
			{floorError && (
				<Table.Row>
					<Table.Cell colSpan={isCommercial ? '6' : '5'} error textAlign="center">
						<b>{errors.floor}</b>
					</Table.Cell>
				</Table.Row>
			)}
			<Table.Row textAlign="center">
				{Object.keys(floorDetails.table_heading).map((key, index) => (
					<Table.HeaderCell error={floorError} key={index} width={index === 0 ? '3' : '2'}>
						{floorDetails.table_heading[key]}
						{index === 0 && (
							<>
								<UnitDropdownWithRelatedFields unitName="floorUnit" options={distanceOptions} relatedFields={relatedFields} />
								{errors.floorUnit ? <span className="tableError">{errors.floorUnit}</span> : null}
							</>
						)}
					</Table.HeaderCell>
				))}
				{isCommercial && <Table.HeaderCell width="2">{floorDetails.commercialActions}</Table.HeaderCell>}
			</Table.Row>
		</Table.Header>
	);
};
