import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import TableInput from '../../../shared/TableInput';
import { Formik } from 'formik';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { AllowancePaperKageyshowriBody, KageyshowriSubject } from '../ijajatPatraComponents/AllowancePaperComponents';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { Form } from 'semantic-ui-react';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { AllowancePaperData } from '../../../../utils/data/AllowancePaperData';
import { FooterSignature } from '../formComponents/FooterSignature';
import { getSaveByUserDetails, getApproveByObject } from '../../../../utils/formUtils';
import { KageyshowriCertificateSignature } from './KageyshowriCertificateSignature';
import { FlexSingleLeft, FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { superstructureConstructionKageyshowri } from '../../../../utils/data/mockLangFile';
import { SectionHeader } from '../../../uiComponents/Headers';

const kageyshowriData = superstructureConstructionKageyshowri.kageyshowriData;

export class SuperStructureConstructionKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, userData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;

		const jsonData = getJsonData(prevData);

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const floorMax = floorArray.getTopFloor().nepaliCount;
		
		const mapTech = getJsonData(otherData.mapTech);
		// const prabidhikPratibedan = getJsonData(otherData.prabidhikPratibedan);
		const allowancePaperData = getJsonData(otherData.allowancePaperData);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const superStructure = getJsonData(otherData.allowancePaper);
		const desApprovJsonData = otherData.designApproval;
		const anuSucKaJsonData = otherData.anukaMaster;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['purposeOfConstruction', 'purposeOfConstructionOther'],
			},
			{
				obj: superStructure,
				reqFields: [],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
					floorMax,
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			// {
			// 	obj: prabidhikPratibedan,
			// 	reqFields: ['floor'],
			// },
			{
				obj: allowancePaperData,
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: [
					'constructionType',
					'roof',
					'roofOther',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'plinthDetails',
					'plinthLength',
					'plinthWidth',
					'plinthLengthUnit',
					'buildingHeight',
					'heightUnit',
				],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{
				obj: {
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					floorareaUnit: DEFAULT_UNIT_LENGTH,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					floorwidthUnit: DEFAULT_UNIT_LENGTH,
					floorheightUnit: DEFAULT_UNIT_LENGTH,
					batoUnit: DEFAULT_UNIT_LENGTH,
					bidhutUnit: DEFAULT_UNIT_LENGTH,
					nadiUnit: DEFAULT_UNIT_LENGTH,
					patraSankhya: '',
					chalaniNumber: '',
					gharNo: '',
				},
				reqFields: [],
			},

			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.allowanceDate)) {
			initialValues.allowanceDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);

		let anusuchikaOptions = [];

		otherData.anukaMaster.map((row) =>
			anusuchikaOptions.push({
				value: row.id,
				text: `${row.nameNepali} ${row.name}`,
			})
		);

		this.state = {
			initialValues,
			buildingClass,
			floorArray,
			formattedFloors,
			floorMax,
		};
	}
	render() {
		const {
			userData,
			permitData,
			prevData,
			formUrl,
			hasSavePermission,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues, buildingClass, floorArray, formattedFloors, floorMax } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					// validationSchema={AllowancePaperSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.superStructureConstruction, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="view-Form print-small-font">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<div className="flex-item-right">
										{kageyshowriData.date}
										<DashedLangDateField
											compact={true}
											name="allowanceDate"
											value={values.allowanceDate}
											error={errors.allowanceDate}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</div>
			


										<SectionHeader>
											<h3 className="underline">{kageyshowriData.kamalamaiTitle}</h3>
										</SectionHeader>
										
									<div>
										





										<div>
											<span >{kageyshowriData.shree}</span>
											<DashedLangInput
												name="shree"
												value={values.shree}
												error={errors.shree}
												setFieldValue={setFieldValue}
											/>
											<br />


											{userData.organization.name}{' '}


											<span >{kageyshowriData.content1}</span>
											{' '}{permitData.newWardNo}{' '}

											<span >{kageyshowriData.alphabiram}</span>
											<br />

											<span >{kageyshowriData.shabik}</span>
											{' '}{permitData.oldMunicipal}{' '}

											<span >{kageyshowriData.hal}</span>
											{' '}{userData.organization.name}{' '}

											<span >{kageyshowriData.wadaNo}</span>
											{' '}{permitData.newWardNo}{' '}
											
											{console.log(permitData)}

											<span >{kageyshowriData.toll}</span>
											{' '}{permitData.sadak}{' '}

											<span >{kageyshowriData.kittaNo}</span>
											{' '}{permitData.kittaNo}{' '}

											<span >{kageyshowriData.chhetrafal}</span>
											{' '}{permitData.landArea}{' '}


											<span >{kageyshowriData.bamiBhayeko}</span>
											<DashedLangInput
												name="bami"
												value={values.bami}
												error={errors.bami}
												setFieldValue={setFieldValue}
											/>

											<span >{kageyshowriData.content2}</span>
											<DashedLangDateField
												inline={true}
												name="date1"
												value={values.date1}
												error={errors.date1}
												setFieldValue={setFieldValue}
											/>

											<span >{kageyshowriData.content3}</span>

											<br />
											<br />
											

											<div className="div-indent">
												<table style={{ textAlign: 'left', width: "100%" }} border="1px" className="small-font-print-table">
													<thead>
														<tr>
															<th></th>
															<th colSpan="4">{kageyshowriData.tableData.headings.heading1}</th>
															<th rowSpan="2">{kageyshowriData.tableData.headings.heading2}</th>
														</tr>

														<tr>
															<th rowSpan="2">{kageyshowriData.tableData.headings.heading3}</th>
															<th rowSpan="2">{kageyshowriData.tableData.headings.heading4}</th>
															<th rowSpan="2">{kageyshowriData.tableData.headings.heading5}</th>
															<th colSpan="2">{kageyshowriData.tableData.headings.heading6}</th>
														</tr>

														<tr>
															<th>{kageyshowriData.tableData.headings.heading7}</th>
															<th>{kageyshowriData.tableData.headings.heading8}</th>
															<th></th>
														</tr>
													</thead>

													<tbody>
														<tr>
															<td>{kageyshowriData.tableData.notLoopable.number}</td>
															<td>{kageyshowriData.tableData.notLoopable.topic}</td>
															<td colSpan="4">
																{kageyshowriData.tableData.notLoopable.data.checkbox.map(e => {
																	return <label>
																		{`  ${e}  `}
																		<input type="checkbox"/>
																	</label>
																})}
															</td>
														</tr>
														{kageyshowriData.tableData.loopableData.map((e, i) => {
															return <tr>
																<td>{e.number}</td>
																<td>{e.topic}</td>
																<td>
																	<DashedLangInput
																		setFieldValue={setFieldValue}
																		handleChange={handleChange}
																		name={`row1${i}`}
																		value={values[`row1${i}`]}
																		error={errors[`row1${i}`]}
																	/>
																</td>
																<td>
																	<DashedLangInput
																		setFieldValue={setFieldValue}
																		handleChange={handleChange}
																			name={`row2${i}`}
																			value={values[`row2${i}`]}
																			error={errors[`row2${i}`]}
																	/>
																</td>
																<td>
																	<DashedLangInput
																		setFieldValue={setFieldValue}
																		handleChange={handleChange}
																		name={`row3${i}`}
																		value={values[`row3${i}`]}
																		error={errors[`row3${i}`]}
																	/>
																</td>
																<td>
																	<DashedLangInput
																		setFieldValue={setFieldValue}
																		handleChange={handleChange}
																		name={`row4${i}`}
																		value={values[`row4${i}`]}
																		error={errors[`row4${i}`]}
																	/>
																</td>
															</ tr>
														})}
													</tbody>
												</table>
											</div>
										</div>
									</div>
									
									<KageyshowriCertificateSignature
										subName={{
											prabesh: "पेश गर्ने",
											sifaris: "सिफारिस गर्ने",
											jach: "जाँच गर्ने",
											suekrit: "स्वीकृत गर्ने"  
										}}
										images={
											useSignatureImage && [
												values.subSignature,
												values.sipharisSignature,
												values.swikritSignature,
												values.pramukhSignature,
											]
										}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const SuperStructureConstructionKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.superStructureConstruction, objName: 'superStructure', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabidhikPratibedan',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowancePaperData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		fetchFiles={true}
		hasFileView={true}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <SuperStructureConstructionKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureConstructionKageyshowri;
