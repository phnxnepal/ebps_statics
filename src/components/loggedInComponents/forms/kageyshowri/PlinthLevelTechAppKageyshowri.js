import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import * as Yup from 'yup';
import { PlinthLevelTechLang, kageyshowriData, kageyshowriPlinthTechAppData } from '../../../../utils/data/PilenthTechAppLang';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import { DashedLangInput, DashedNormalInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate, DashedLangDateField } from '../../../shared/DateField';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { YesNoSection, yesNoSectionSchema } from '../formComponents/PlinthLevelComponents';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FooterSignature } from '../formComponents/FooterSignature';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { ImageDisplayInline } from '../../../shared/file/FileView';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import { SignatureImage } from '../formComponents/SignatureImage';

const PlinthTechLang = PlinthLevelTechLang;
const data = kageyshowriPlinthTechAppData;

const plinthLevelAppSchema = {
	plinthApprovalDate: validateNullableNepaliDate,
	date: validateNullableNepaliDate,
};

const schema = Yup.object().shape(Object.assign({ ...yesNoSectionSchema, ...plinthLevelAppSchema }));

class PlinthLevelTechAppKageyshowriComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, otherData, enterByUser, userData } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const asubData = getApproveBy(0);
		const serData = getApproveBy(1);
		const erData = getApproveBy(2);

		const json_data = getJsonData(prevData);
		const plinthOwner = getJsonData(otherData.plinthOwner);
		const samsodhitSup = getJsonData(otherData.samsodhitSup);
		
		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };
		// serInfo.subName = enterByUser.name;
		// serInfo.subDesignation = enterByUser.designation;
		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }
		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					letterSubmitDate: getCurrentDate(true),
					date: getCurrentDate(true),
				},
				reqFields: [],
			},
			{
				obj: {
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: plinthOwner, reqFields: [] },
			{ obj: json_data, reqFields: [] },
			{ obj: samsodhitSup, reqFields: [] },
			
			{
				obj: {
					erSignature: erData.signature,
					serSignature: serData.signature,
					asubSignature: asubData.signature,
				},
				reqFields: [],
			}
		);
		this.state = { initialValues, answers: {"name1_1" : false, "name1_2" : false, "name2_1" : false, "name2_2" : false, "name3_1" : false, "name3_2" : false} };
	}

	render() {
		const questionAnswer = (data, setFieldValue, values, handleChange, bibaran) => {
			return <>
				{data.questions.map((e,i) => {
					return <>
							{`${e.name}`}
							{e.options.map((f, j) => {
								return <>
								<div key={f} className="ui radio checkbox">
								<input
									type="radio"
									name={`${e.name}_${i}_box`}
									value={f}
									defaultChecked={values[`${e.name}_${i}_box`] === f}
									onChange={handleChange}
									onClick={() => {
										if(e.options[1] == f){
											const dataName = this.state.answers
											dataName[`${e.name}_${i}`] = true
											this.setState({...this.state, answers : dataName})
										}else{
											const dataName = this.state.answers
											dataName[`${e.name}_${i}`] = false
											this.setState({...this.state, answers : dataName})
										}
									}}
								/>
								<label>{f}</label>
							</div>
								{e.options[1] == f && this.state.answers[`${e.name}_${i}`] && (
									<>
									{console.log(values[`${e.name}_${i}`])}
									{' '}{bibaran}{' '}
										<DashedLangInput 
											name={`${e.name}_${i}_text`}
											value={values[`${e.name}_${i}_text`]}
											setFieldValue={setFieldValue}
										/>
									</>
								)}
							</>
							})}
						<br />
					</>
				})}
			</>
		}
		const {
			errors: reduxErrors,
			permitData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			userData,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues } = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// console.log('log');
						try {
							await this.props.postAction(`${api.samsodhitSupStruPratibedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleChange, handleSubmit, errors, setFieldValue, validateForm }) => (
						<Form 
						loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h3 className="underline end-section">{data.heading}</h3>
								</SectionHeader>
								<LetterSalutation
									lines={[letterSalutation.prashasakhiya, `${userData.organization.name} ${letterSalutation.karyalaya}`, `${userData.organization.address} ${userData.organization.province}`]}
								/>
								<br />
								<div className="no-margin-field margin-bottom">
									

									{' '}{userData.organization.name}{' '}
									
									<span>{data.wardNo}</span>
									{' '}{permitData.newWardNo}{' '}

									<span>{data.basne}</span>
									{' '}{permitData.applicantName}{' '}

									<span>{data.leNagarpalika}</span>
									<DashedLangDateField
										name="date1"
										inline={true}
										value={values.date1}
										error={errors.date1}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>

									<span>{data.content1}</span>

									<br />
									<b><span>{data.tapsil}</span></b>
									<br />

									<b><span>{data.list.l1.heading}</span></b>
									<br />

									{' '}{userData.organization.name}{' '}
									
									<span>{data.list.l1.palika}</span>
									{' '}{permitData.oldWardNo}{' '}


									<span>{data.list.l1.haalko}</span>
									{' '}{permitData.newWardNo}{' '}

									<span>{data.kittaNo}</span>
									{' '}{permitData.kittaNo}{' '}

									<span>{data.list.l1.chhetrafal}</span>
									{' '}{permitData.landArea}{' '}

									<span>{data.list.l1.bami}</span>
									<DashedLangInput 
										name="bami"
										setFieldValue={setFieldValue}
										handleChange={handleChange}
										value={values.bami}
										error={errors.bami}
									/>

									<br />
									<b><span>{data.list.l2.heading}</span></b>
									<br />
									{questionAnswer(data.list.l2, setFieldValue, values, handleChange, data.chhainaBhaneBibarad)}

									<br />
									<br />

									<b><span>{data.list.l3.heading}</span></b>
									<br />
									{questionAnswer(data.list.l3, setFieldValue, values, handleChange, data.chhainaBhaneBibarad)}

									<br />
									<br />

									<b><span>{data.list.l4.heading}</span></b>
									<br />
									{questionAnswer(data.list.l4, setFieldValue, values, handleChange, data.chhainaBhaneBibarad)}
									<br/>

									<b><span>{data.list.l5.heading}</span></b>
									
									<br/>

									<span>{data.list.l5.question[0].name}</span>
									<DashedLangInput 
										name="name1"
										value={values.name1}
										error={errors.name1}
										setFieldValue={setFieldValue}
									/>

									<span>{data.list.l5.question[0].lambae}</span>

									<DashedLengthInputWithRelatedUnits
										name="lambai"
										unitName="floorUnit"
										relatedFields={["chaudahi", "uchahi"]}
									/>{' '}

									<span>{data.list.l5.question[0].chaudahi}</span>

									<DashedLengthInputWithRelatedUnits
										name="chaudahi"
										unitName="floorUnit"
										relatedFields={["uchahi", "lambai"]}
									/>
									<span>{data.list.l5.question[0].ucahi}</span>

									<DashedLengthInputWithRelatedUnits
										name="uchahi"
										unitName="floorUnit"
										relatedFields={["lambai", "chaudahi"]}
									/>

									<span>{data.list.l5.question[0].talla}</span>
									<DashedLangInput 
										name="talla"
										value={values.talla}
										error={errors.talla}
										setFieldValue={setFieldValue}
									/>
									<br/>

									<span>{data.list.l5.question[1].name}</span>
									<DashedLangDateField
										name="date2"
										inline={true}
										value={values.date2}
										error={errors.date2}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>
									<br/>

									<span>{data.list.l5.question[2].name}</span>
									<DashedLangInput 
										name="plinth"
										value={values.plinth}
										error={errors.plinth}
										setFieldValue={setFieldValue}
									/>
									<br/>
									<br/>

									<b><span>{data.list.l6.heading}</span></b>

									<DashedLangInput 
										name="khulaune1"
										value={values.khulaune1}
										error={errors.khulaune1}
										setFieldValue={setFieldValue}
									/>
									<DashedLangInput 
										name="khulaune2"
										value={values.khulaune2}
										error={errors.khulaune2}
										setFieldValue={setFieldValue}
									/>
									<DashedLangInput 
										name="khulaune3"
										value={values.khulaune3}
										error={errors.khulaune3}
										setFieldValue={setFieldValue}
									/>
									<br/>

									<span>{data.list.l6.content1}</span>
									<DashedNormalInput
										name="poisa1"
										value={values.poisa1}
										error={errors.poisa1}
										setFieldValue={setFieldValue}
									/>


									<span>{data.list.l6.dhrauti}</span>
									<DashedLangInput 
										name="poisa2"
										value={values.poisa2}
										error={errors.poisa2}
										setFieldValue={setFieldValue}
									/>

									<span>{data.list.l6.karyalaya}</span>
									<DashedLangInput 
										name="karyalaya"
										value={values.karyalaya}
										error={errors.karyalaya}
										setFieldValue={setFieldValue}
									/>

									<span>{data.list.l6.date} </span>
									<DashedLangDateField
										name="date3"
										inline={true}
										value={values.date3}
										error={errors.date3}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>

									<span>{data.list.l6.content2} </span>
									<DashedLangInput 
										name="patra"
										value={values.patra}
										error={errors.patra}
										setFieldValue={setFieldValue}
									/>

									<span>{data.wardNo} </span>
									{' '}{permitData.newWardNo}{' '}

									<span>{data.list.l6.basneShree} </span>
									{' '}{permitData.applicantName}{' '}

									<span>{data.list.l6.ko} {data.list.l6.kittaNo}</span>
									{' '}{permitData.kittaNo}{' '}

									<span>{data.list.l6.chhetrafal} </span>
									{' '}{permitData.landArea}{' '}

									<span>{data.list.l6.bimaBhayeko} </span>
									
									<DashedLengthInputWithRelatedUnits
										name="lambai"
										unitName="floorUnit"
										relatedFields={["chaudahi", "uchahi"]}
									/>{' '}

									<span>{data.list.l6.chaudahi} </span>

									<DashedLengthInputWithRelatedUnits
										name="chaudahi"
										unitName="floorUnit"
										relatedFields={["uchahi", "lambai"]}
									/>

									<span>{data.list.l6.uchahi} </span>
									<DashedLengthInputWithRelatedUnits
										name="uchahi"
										unitName="floorUnit"
										relatedFields={["lambai", "chaudahi"]}
									/>


									<span>{data.list.l6.bamojim} </span>
									<DashedLangInput 
										name="bamojim"
										value={values.bamojim}
										error={errors.bamojim}
										setFieldValue={setFieldValue}
									/>

									<span>{data.list.l6.content3} </span>
									<br/>
									<br/>

									<b><span>{data.list.l7.heading} </span></b>
									<br/>
									
									<span>{data.list.l7.content1} </span>
									<DashedLangInput 
										name="name2"
										value={values.name2}
										error={errors.name2}
										setFieldValue={setFieldValue}
									/>
									<br/>

									<span>{data.list.l7.thaar} : </span>
									<DashedLangInput 
										name="cast"
										value={values.cast}
										error={errors.cast}
										setFieldValue={setFieldValue}
									/>
									<br/>
									
									<span>{data.list.l7.padh} : </span>
									<DashedLangInput 
										name="padh"
										value={values.padh}
										error={errors.padh}
										setFieldValue={setFieldValue}
									/>
									<br/>
									
									<span>{data.list.l7.dasthakhat} : </span>
									<DashedLangInput 
										name="dasthakhat"
										value={values.dasthakhat}
										error={errors.dasthakhat}
										setFieldValue={setFieldValue}
									/>
									<br/>
									<span>{data.list.l7.date} : </span>
									<DashedLangDateField
										name="date3"
										inline={true}
										value={values.date3}
										error={errors.date3}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
									/>
									
									
									
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const PlinthLevelTechAppKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.samsodhitSupStruPratibedan,
				objName: 'samsodhitSup',
				form: true,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui checkbox', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByTagName', 'input', 'value'],
		}}
		fetchFiles={true}
		render={(props) => <PlinthLevelTechAppKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default PlinthLevelTechAppKageyshowri;
