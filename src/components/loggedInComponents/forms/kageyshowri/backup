import React, { Component } from 'react';
import { Form, Table } from 'semantic-ui-react';

import { Formik, Field, getIn } from 'formik';

import * as Yup from 'yup';

// import * as Yup from 'yup';
import { bhawansthalgatData, sthalgatNirikshyanGariPratibedanReqData, noObjectionSthalgatData } from '../../../../utils/data/bhawanData';
import { CompactDashedLangDate, DashedDateField } from '../../../shared/DateField';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { prepareMultiInitialValues, getJsonData, floorMappingFlat, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import FormikCheckbox, { FormikCheckboxIm } from '../../../shared/FormikCheckbox';
import TableInput from '../../../shared/TableInput';
import { checkError } from '../../../../utils/dataUtils';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { validateNullableOfficialReqNumbers, validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { getSaveByUserDetails } from '../../../../utils/formUtils';
import { isKamalamai } from '../../../../utils/clientUtils';
import { DashedAreaUnitInput, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight, FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { ApiParam } from '../../../../utils/paramUtil';

const stringify = (value) => {
	if (value) {
		if (typeof value === 'string') return value;
		else return JSON.stringify(value);
	} else return '';
};

let data = bhawansthalgatData.structureDesign;
const customInputs = bhawansthalgatData.structureDesign.customInputs;
// const footerData = naksaData.structureDesign.footer;
const reqData = sthalgatNirikshyanGariPratibedanReqData;
const arrayValueIds = [21, 30, 7, 5, 1, 2];
const objectValueIds = [29, 4, 20, 16, 25];
const toArray = (field) => {
	if (field.includes('[')) {
		const returnVal = field.replace(/[[\]]+/g, '');
		const retArr = returnVal.split(',');
		return retArr.map((val) => val.trim());
	} else return [field];
};
let initialVal = {
	chana: '',
	dwaraNakshaDesignMiti: '',
	date: '',
	kaufiyat: '',
	peshGarne: '',
	juEKoNaam: '',
	peshGarneMiti: '',
	engineerKoRaya: '',
	engineerKoSahiMiti: '',
};

const MapCheckSchema = Yup.object().shape(
	Object.assign({
		mapCheckReportDate: validateNullableNepaliDate,

		chana: validateNullableOfficialReqNumbers,
		dwaraNakshaDesignMiti: validateNullableNepaliDate,
		date: validateNullableNepaliDate,

		peshGarneMiti: validateNullableNepaliDate,

		engineerKoSahiMiti: validateNullableNepaliDate,
	})
);
class SthalgatNirikshyanGariPeshKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, userData, prevData, otherData, enterByUser, DEFAULT_UNIT_LENGTH } = this.props;
		const mapTech = getJsonData(otherData.mapTechnicalDescription);
		const pulledData = {
			details: [
				{
					description: customInputs.buildingClass,
					designData: otherData.designApprovalData.buildingClass,
				},
				{
					description: customInputs.purposeOfConstruction,
					// designData: permitData.purposeOfConstruction,
					designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther },
				},
				{
					description: customInputs.constructionType,
					designData: getConstructionTypeValue(permitData.constructionType),
				},

				// {
				// 	description: purposeOfConstructionOther,
				// 	designData: { option: permitData.purposeOfConstruction, other: permitData.purposeOfConstructionOther }
				// },
				{
					description: customInputs.floorNumber,
					designData: mapTech.buildingDetailfloor || '',
				},
				{
					description: customInputs.plinthArea,
					designData: [mapTech.plinthDetails || 0, mapTech.plinthDetailsUnit || DEFAULT_UNIT_LENGTH],
				},
				{
					description: customInputs.buildingHeight,
					designData: [
						'',
						mapTech.buildingHeight || 0,
						mapTech.buildingHeightUnit || DEFAULT_UNIT_LENGTH,
						mapTech.buildingHeightUnit || DEFAULT_UNIT_LENGTH,
					],
				},
				{
					description: customInputs.landLength,
					designData: [mapTech.fieldLength || 0, mapTech.fieldLengthUnit || DEFAULT_UNIT_LENGTH],
				},
				{
					description: customInputs.landWidth,
					designData: [mapTech.fieldWidth || 0, mapTech.fieldLengthUnit || DEFAULT_UNIT_LENGTH],
				},
			],
		};

		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// } else if (!isStringEmpty(checkError(prevData).enterBy)) {
		// 	serInfo.subName = prevData.enterBy;
		// 	serInfo.subDesignation = getUserTypeValueNepali(UserType.SUB_ENGINEER);
		// }

		const initialDetails = prepareMultiInitialValues(
			{
				obj: getSaveByUserDetails(enterByUser, userData),
				reqFields: [],
			},
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true), dwaraNakshaDesignMiti: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: { mapCheckReportDate: getCurrentDate(true) },
				reqFields: [],
			},
			{ obj: prepareInitialVaules(prevData, pulledData), reqFields: [] }
		);

		initialDetails.details.forEach((row) => {
			if (arrayValueIds.includes(row.descriptionId)) {
				if (Array.isArray(row.designData)) return;
				row.designData = toArray(row.designData);
			} else if (objectValueIds.includes(row.descriptionId)) {
				try {
					if (typeof row.designData === 'string') {
						row.designData = JSON.parse(row.designData);
					}
					if (typeof row.designData !== 'object') {
						row.designData = {};
					}
				} catch (err) {
					// console.log('err', err, ' in ', row.designData, row.descriptionId, row);
					return {};
				}
			}
		});

		// to maintain index integrity
		prevData.details && prevData.details.map((row, inputIndex) => (row.inputIndex = inputIndex));

		let prevDatas = prevData.enterBy == null && prevData.enterDate == null ? {} : prevData;

		this.state = {
			toggle: {
				index: 0,
			},
			initialDetails,
			prevDatas,
		};
	}

	handleFilterInputChangeRoofStyle = (e) => {
		const updateRoofStyle = e.target.value;
		if ('अन्य' === updateRoofStyle) {
			this.setState({
				inputRoofStyleFilter: true,
			});
		} else {
			this.setState({ inputRoofStyleFilter: false });
		}
		this.setState({ RoofStyle: updateRoofStyle });
	};

	render() {
		const { permitData, prevData, hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl, userData } = this.props;

		const { initialDetails, prevDatas } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialDetails}
					validationSchema={MapCheckSchema}
					onSubmit={async (values, actions) => {
						// values.details.filter(row => row.descriptionId === 9).map(row => (row.designData = this.state.toggle.value));
						actions.setSubmitting(true);
						values.details[28].designData = stringify(values.details[28].designData);
						values.details[3].designData = stringify(values.details[3].designData);
						values.details[19].designData = stringify(values.details[19].designData);
						values.details[24].designData = stringify(values.details[24].designData);
						values.details[15].designData = stringify(values.details[15].designData);

						const dataToSend = { details: values.details, jsonData: JSON.stringify({ ...values, details: '' }) };

						try {
							await this.props.postAction(api.SthalgatNiirikshyanGariPesh, dataToSend, true);

							actions.setSubmitting(false);

							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, values, handleChange, setFieldValue, isSubmitting, errors, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								<div ref={this.props.setRef}>
									<br />
									<br />

									<SectionHeader>
										<h3>{data.table.heading}</h3>
									</SectionHeader>
										<table width="100%" border="1px">
											<thead>
												<tr>
													<th>{data.table.rows[0].sn}</th>
													<th>{data.table.rows[0].name}</th>
													<th colSpan="3">{data.table.rows[0].input_1}</th>
													<th>{data.table.rows[0].input_2}</th>
												</tr>
											</thead>
											<tbody>
												{data.table.rows.map((e) => {
													const designRow = (type, data_1) => {
														switch(type){
															case 'checkbox': 
																	return <td colSpan="3">
																		{data_1.map(e => <label>
																			<input type="checkbox" />
																			{`  ${e}  `}
																		</label>)}
																	</td>
															case 'text': 
																	return <td colSpan="3">
																		{data_1.map(e => `  ${e}  `)}
																	</td>
															default : return <td colSpan="3">&nbsp;</td>
														}
													}

													const getRows = (type) => {
														switch(type){
															case 'blank': return <tr>
																<td>{e.sn}</td>
																<td colSpan="5">{e.name}</td>
															</tr>
															case 'Garoko': return <>
																<tr>
																	<td rowSpan="2">{e.sn}</td>
																	<td>{e.input_1[0]}</td>
																	<td>{e.input_2[0]}</td>
																	<td>{e.input_2[1]}</td>
																	<td>{e.input_2[2]}</td>
																	<td>&nbsp;</td>
																</tr>
																<tr>
																	<td>{e.input_1[1]}</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
															</> 
															default : return <tr>
																<td>{e.sn}</td>
																<td>{e.name}</td>
																{designRow(type, e.input_1)}
																<td>&nbsp;</td>
															</tr>
														}
													}
													return getRows(e.type)
												})}
											</tbody>
										</table>
										<br />
										<br />
										<br />

										<div style={{display: 'grid', gridTemplateColumns: "1fr 1fr", gridGap: '20px'}}>
											<div>
												<div>
													<span>{noObjectionSthalgatData.footer.gharDhani}</span> <br />
													<span>{noObjectionSthalgatData.footer.address}</span> <br />
													<span>{noObjectionSthalgatData.footer.sahi}</span> <br />
													<span>{noObjectionSthalgatData.footer.date}</span> <br />
													<span>{noObjectionSthalgatData.footer.samparka}</span> <br />
												</div>
												
												<br />
												<br />

												<div>
													<span>{noObjectionSthalgatData.footer.sartha.list1}</span><br /><br />
													<span>{noObjectionSthalgatData.footer.sartha.list2}</span><br /><br />
												</div>
											</div>
											<div>
												<div style={{border: "1px solid black"}}>{noObjectionSthalgatData.footer.note}</div>
												<br />
												<br />
												
												<div>
													<b><u>{noObjectionSthalgatData.footer.paramDartha.heading}</u></b>
													<br />

													<span>{noObjectionSthalgatData.footer.paramDartha.en}</span>
													<DashedLangInput/>
													<br />
													<br />

													<span>{noObjectionSthalgatData.footer.paramDartha.signature}</span>
													<DashedLangInput/>
													<br />
													<br />

													<span>{noObjectionSthalgatData.footer.samparka}</span>
													<DashedLangInput/>
													<br />

												</div>
											</div>
										</div>

										<FlexSingleRight>
											<span>{noObjectionSthalgatData.footer.chhap}</span>
										</FlexSingleRight>
									</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevDatas)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const prepareInitialVaules = (prevData, pulledData) => {
	// const filteredPrevData = Object.entries(prevData).reduce((acc, [key, value]) => {
	// 	if (value && ['subDate', 'subDesignation', 'subName', 'subSignature'].includes(key)) {
	// 		return { ...acc, [key]: value };
	// 	} else return acc;
	// }, {});
	let jsonData = {};
	try {
		jsonData = prevData && prevData.jsonData && JSON.parse(prevData.jsonData);
	} catch (error) {}
	const ret = { ...jsonData, details: [] };

	if (prevData.details && prevData.details.length > 0) {
		prevData.details.forEach((row, index) => {
			const pulledVal = pulledData.details.find((el) => el.description === row.description);
			if (row.descriptionId === 8) {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? getConstructionTypeValue(row.designData) : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			} else {
				ret.details.push({
					descriptionId: row.descriptionId,
					designData: !isStringEmpty(row.designData) ? row.designData : pulledVal ? pulledVal.designData : '',
					remark: row.remark || '',
				});
			}
		});
	}

	return ret;
};

const SthalgatNirikshyanGariPeshKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.SthalgatNiirikshyanGariPesh).setForm().getParams(),
			// { api: api.allowancePaper, objName: 'allowancePaper', form: true },
			// {
			// 	api: api.mapCheckReport,
			// 	objName: 'mapCheckReport',
			// 	form: true,
			// },
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTechnicalDescription',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'buildingClass',
				form: false,
				utility: true,
			},
		]}
		onBeforeGetContent={{
			param: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <SthalgatNirikshyanGariPeshKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default SthalgatNirikshyanGariPeshKageyshowri;
