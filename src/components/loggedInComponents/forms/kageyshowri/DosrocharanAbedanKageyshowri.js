import React, { Component } from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import { NirmanKarya } from '../../../../utils/data/mockLangFile';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import api from '../../../../utils/api';
import { showToast } from '../../../../utils/functionUtils';
import { DashedLangInputWithSlash, DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from '../../../shared/DateField';
import { Select, Label, Grid, Form } from 'semantic-ui-react';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { letterSalutation } from '../../../../utils/data/genericFormData';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { HaalSection } from '../formComponents/PermitInfo';
import { anushuchi_ga } from '../../../../utils/data/mockLangFile';
import { PrintParams } from '../../../../utils/printUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SectionHeader } from '../../../uiComponents/Headers';

const nmlang = NirmanKarya.NirmanKarya_Kageyshowri_data;
const data = anushuchi_ga.anushuchiGaView;
const opt = [
	{ key: 'A', value: 'सुपरस्ट्रक्चर', text: 'सुपरस्ट्रक्चर' },
	{ key: 'B', value: 'कम्पाउण्डवाल', text: 'कम्पाउण्डवाल' },
];

const opt1 = [
	{ key: 'ma', value: 'गरेको', text: 'गरेको' },
	{ key: 'hami', value: 'गरेका', text: 'गरेका' },
];

const getMs = value => {
	const ret = opt1.filter(row => row.value === value);
	return ret[0] ? ret[0].key : 'ma';
};

const DosrocharanSchema = Yup.object().shape(
	Object.assign({
		dosrocharanDate: validateNullableNepaliDate,
		supStrucDate: validateNullableNepaliDate,
		wareshDate: validateNullableNepaliDate,
	})
);
class DosrocharanAbedanKageyshowriComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		initVal = prepareMultiInitialValues(
			{
				// Default values.
				obj: {
					abedanMs: nmlang.data_2.data_2_ms.maile,
					supercomp: opt[0].value,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(otherData.superStructCons),
				reqFields: [],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.dosrocharanDate)) {
			initVal.dosrocharanDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.wareshDate)) {
			initVal.wareshDate = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}

	render() {
		const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal } = this.state;
		const udata = userData;
		return (
			<div>
				<Formik
					initialValues={{ applicantMs: opt1[0].value, ...initVal }}
					validationSchema={DosrocharanSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.DosrocharanAbedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//     getNextUrl(
								//         this.props.parentProps.location.pathname
								//     )
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, handleChange, isSubmitting, values, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef} className="build-gaps">
								<div className="NJ-Main superStruConsView">
									<SectionHeader>
										<h2>{nmlang.heading.heading_1}</h2>
									</SectionHeader>
									<div style={{ textAlign: 'right' }}>
										{nmlang.data_2.data_2_2}
										{': '}
										<DashedLangDateField
											name="dosrocharanDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.dosrocharanDate}
											// label={nmlang.data_2.data_2_2}
											className="dashedForm-control"
											error={errors.dosrocharanDate}
										/>
									</div>
									<LetterSalutation
										lines={[letterSalutation.prashasakhiya, `${userData.organization.name} ${letterSalutation.karyalaya}`, `${userData.organization.address} ${userData.organization.province}`]}
									/>

									<SectionHeader>
										<h3 className="underline">{nmlang.heading.SubjectSundar}</h3>
									</SectionHeader>
									
									<br />
									<br />
									<div style={{ textAlign: 'justify' }}>


										<span>{nmlang.mainContent.content1}</span>
										{' '}{userData.organization.name}{' '}
										<span>{nmlang.mainContent.content1_2}</span>

										{' '}{permitData.newWardNo}{' '}
										{console.log(permitData)}
										<span>{nmlang.mainContent.tol}</span>
										{' '}{permitData.nibedakTol}{' '}

										<span>{nmlang.mainContent.kittaNo}</span>
										{' '}{permitData.kittaNo}{' '}

										<span>{nmlang.mainContent.chhetrafal}</span>
										{' '}{permitData.landArea}{' '}

										<span>{nmlang.mainContent.bimiBhayeko}</span>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name="bimiBhayeko"
											value={values.bimiBhayeko}
											error={errors.bimiBhayeko}
											handleChange={handleChange}
										/>

										<span>{nmlang.mainContent.content2}</span>
										{' '}{userData.buildingJoinRoad}{' '}

										<span>{nmlang.mainContent.content3}</span>
										<DashedLangDateField
											inline={true}
											setFieldValue={setFieldValue}
											name="date1"
											value={values.date1}
											error={errors.date1}
											handleChange={handleChange}
										/>

										<span>{nmlang.mainContent.content4}</span>

									</div>

									<div style={{ paddingTop: '20px', paddingLeft: '30px' }}>
									<h2>{nmlang.listData.heading}</h2>
									{nmlang.listData.data.map((e) => {
										return <div>
												<span>{e}</span>
											</div>
									})}
									</div>


									<div style={{ paddingTop: '20px', paddingLeft: '30px' }}>
										<span>{nmlang.mainContent.nibedhaknam} {permitData.applicantName}</span>

										<br />

										<span>{nmlang.mainContent.barisnam}</span>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name="barisnam"
											value={values.barisnam}
											error={errors.barisnam}
											handleChange={handleChange}
										/>
										<br />

										<span>{nmlang.mainContent.thegana} {permitData.sadak}</span>

										<br />

										<span>{nmlang.mainContent.dartakhata}</span>
									
										<br />

										<span>{nmlang.mainContent.mobile} {userData.info.mobile}</span>
									
										<br />

										<span>{nmlang.mainContent.email}</span>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name="bimiBhayeko"
											value={values.bimiBhayeko}
											error={errors.bimiBhayeko}
											handleChange={handleChange}
										/>
										<br />

									</div>


								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const DosrocharanAbedanKageyshowri = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.DosrocharanAbedan,
				objName: 'dosrocharanAbedan',
				form: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'superStructCons',
				form: false,
			},
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			...PrintParams.INLINE_FIELD,
		}}
		useInnerRef={true}
		prepareData={data => data}
		parentProps={parentProps}
		render={props => <DosrocharanAbedanKageyshowriComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanAbedanKageyshowri;
