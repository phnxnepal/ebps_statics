import React from 'react';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { getIn } from 'formik';
import { GharSurjaminData } from '../../../../utils/data/GharSurjaminData';
import { surroundingMappingFlat } from '../../../../utils/dataUtils';

const GSData = GharSurjaminData.structureDesign;
export const NeighbourWitnessSection = ({ userData, values, errors, setFieldValue }) => {
	return (
		<div>
			<h2 className="NJ-title">{GSData.tab2.subheading.subheading2}</h2>
			{GSData.tab2.content.neighbours.map(idx => (
				<div key={idx}>
					<span>{GSData.tab2.content.signature}</span>
					<span className="ui input dashedForm-control" />
					<span>
						{userData.organization.name}
						{GSData.tab2.content.content5}
					</span>
					<DashedLangInput
						compact={true}
						name={`neighbour.${idx}.ward`}
						setFieldValue={setFieldValue}
						value={getIn(values, `neighbour.${idx}.ward`)}
						error={getIn(errors, `neighbour.${idx}.ward`)}
					/>
					<span>{GSData.tab2.content.content51}</span>
					<DashedLangInput
						compact={true}
						name={`neighbour.${idx}.age`}
						setFieldValue={setFieldValue}
						value={getIn(values, `neighbour.${idx}.age`)}
						error={getIn(errors, `neighbour.${idx}.age`)}
					/>
					<span>{GSData.tab2.content.content6}</span>
					<DashedLangInput
						name={`neighbour.${idx}.name`}
						setFieldValue={setFieldValue}
						value={getIn(values, `neighbour.${idx}.name`)}
						error={getIn(errors, `neighbour.${idx}.name`)}
					/>
				</div>
			))}
		</div>
	);
};

export const SurroundingSection = ({ values, setFieldValue, errors }) => {
	return (
		<div>
			<h2 className="NJ-title">{GSData.tab2.subheading.subheading1}</h2>
			{surroundingMappingFlat.map(surr => {
				let dataSurr = {};
				let index = 0;
				if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
					dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
					index = values.surrounding.indexOf(dataSurr);
				}
				return (
					<div key={surr.side}>
						{GSData.tab2.content.signature} <span type="text" name="sign1" className=" ui input dashedForm-control" /> {surr.value}{' '}
						{GSData.tab2.content.kittaNo}{' '}
						<DashedLangInput
							compact={true}
							name={`surrounding.${index}.kittaNo`}
							setFieldValue={setFieldValue}
							value={getIn(values, `surrounding.${index}.kittaNo`)}
							error={getIn(errors, `surrounding.${index}.kittaNo`)}
						/>{' '}
						{GSData.tab2.content.directedcontent11}
						<DashedLangInput
							name={`surrounding.${index}.sandhiyar`}
							setFieldValue={setFieldValue}
							value={getIn(values, `surrounding.${index}.sandhiyar`)}
							error={getIn(errors, `surrounding.${index}.sandhiyar`)}
						/>{' '}
					</div>
				);
			})}
		</div>
	);
};

export const AbsentSurroundingSection = ({ values, setFieldValue, errors }) => {
	return (
		<div>
			<h2 className="NJ-title">{GSData.tab2.subheading.subheading3}</h2>
			{surroundingMappingFlat.map(surr => {
				let dataSurr = {};
				let index = 0;
				if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
					dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
					index = values.surrounding.indexOf(dataSurr);
				}
				return (
					<div key={surr.side}>
						{surr.value} {GSData.tab2.content.kittaNo}{' '}
						<DashedLangInput
							compact={true}
							name={`absent.${index}.kittaNo`}
							setFieldValue={setFieldValue}
							value={getIn(values, `absent.${index}.kittaNo`)}
							error={getIn(errors, `absent.${index}.kittaNo`)}
						/>
						{GSData.tab2.content.directedcontent11}
						<DashedLangInput
							name={`absent.${index}.name`}
							setFieldValue={setFieldValue}
							value={getIn(values, `absent.${index}.name`)}
							error={getIn(errors, `absent.${index}.name`)}
						/>
					</div>
				);
			})}
		</div>
	);
};

export const SurroundingSectionParagraph = ({ values, setFieldValue, errors }) => {
	return (
		<>
			{surroundingMappingFlat.map(surr => {
				let dataSurr = {};
				let index = 0;
				if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
					dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
					index = values.surrounding.indexOf(dataSurr);
				}
				return (
					<React.Fragment key={surr.side}>
						{surr.value} {GSData.tab1.sandhiyar}{' '}
						<DashedLangInput
							name={`surrounding.${index}.sandhiyar`}
							setFieldValue={setFieldValue}
							value={getIn(values, `surrounding.${index}.sandhiyar`)}
							error={getIn(errors, `surrounding.${index}.sandhiyar`)}
						/>{' '}
					</React.Fragment>
				);
			})}
		</>
	);
};
