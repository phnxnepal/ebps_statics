import React, { Component } from 'react';
import { List, Form } from 'semantic-ui-react';
import { DashedLangInput } from '../../shared/DashedFormInput';

import { GharSurjaminData } from '../../../utils/data/GharSurjaminData';
import { Formik, Field } from 'formik';
//Redux
import { connect } from 'react-redux';
import { getFormDataWithPermitAndUser } from './../../../store/actions/formActions';
import api from '../../../utils/api';
import { isEmpty } from '../../../utils/functionUtils';

import { getJsonData } from '../../../utils/dataUtils';

import UnitDropdown from './../../shared/UnitDropdown';
import SaveButton from './../../shared/SaveButton';
import { EbpsTextArea } from '../../shared/EbpsForm';
import { getPermitAndUserData } from './../../../store/actions/AllowancePaperAction';
import { setPrint } from '../../../store/actions/general';
import FallbackComponent from '../../shared/FallbackComponent';
import {
	NeighbourWitnessSection,
	SurroundingSection,
	AbsentSurroundingSection,
	SurroundingSectionParagraph,
} from './gharNaksaComponents/GharNaksaComponents';
import { isBirtamod } from '../../../utils/clientUtils';

const options = [
	{ key: 'a', text: 'फिट', value: 'FEET' },
	{ key: 'b', text: 'मिटर', value: 'METRE' },
];

const GSData = GharSurjaminData.structureDesign;

class GharNakshaSurjaminMuchulkaPrintViewComponent extends Component {
	componentDidMount() {
		this.props.getFormDataWithPermitAndUser(api.gharnakshaSurjaminMuchulka, data => data);
	}
	render() {
		if (!isEmpty(this.props.permitData) && !isEmpty(this.props.userData) && !isEmpty(this.props.prevData)) {
			const permitData = this.props.permitData;
			const userData = this.props.userData;
			const prevData = this.props.prevData;

			const json_data = getJsonData(prevData);
			//const surjaminMuchulka = getJsonData(prevData.surjaminMuchulka);

			let initVal = {};
			initVal.eastwestMeter = 'METRE';
			initVal.eastwestMeter1 = 'METRE';
			initVal.southNorthMeter = 'METRE';
			initVal.southNorthMeter1 = 'METRE';
			initVal.height1 = 'METRE';
			//initVal.aminName = surjaminMuchulka ? surjaminMuchulka.aminName : '';
			//initVal.prashashanName = surjaminMuchulka ? surjaminMuchulka.prashashanName : '';

			const initVal1 = { ...initVal, ...json_data };
			// console.log('initval', initVal);
			return (
				<>
					<Formik initialValues={initVal1}>
						{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors }) => (
							<Form loading={isSubmitting} className="ui form">
								<div>
									<div className="NJ-header Tabs">
										<div className="NJ-Main">
											<h2 className="NJ-title">{GSData.tab1.title}</h2>
											<br />
											<p style={{ textAlign: 'justify' }}>
												{GSData.tab1.line1} <span>{userData.organization.name}</span>
												{','} {GSData.tab1.line2}
												<DashedLangInput name="aminName" setFieldValue={setFieldValue} value={values.aminName} error={errors.aminName} />{' '}
												{GSData.tab1.line3}
												<DashedLangInput
													name="prashashanName"
													setFieldValue={setFieldValue}
													value={values.prashashanName}
													error={errors.prashashanName}
												/>{' '}
												{GSData.tab1.line4} <span>{userData.organization.name}</span> {GSData.tab1.wardNo}
												<DashedLangInput name="munWard" setFieldValue={setFieldValue} value={values.munWard} error={errors.munWard} />{' '}
												{GSData.tab1.line5}
												<span>{userData.organization.name}</span>,{GSData.tab1.wardNo} <span>{permitData.nibedakTol}</span> {GSData.tab1.line6}{' '}
												<span>{permitData.nibedakName}</span> {GSData.tab1.line6_1} <span>{permitData.applicantName}</span>
												{GSData.tab1.line7} <span>{permitData.oldMunicipal}</span> {GSData.tab1.wardNo} <span>{permitData.oldWardNo}</span>
												{GSData.tab1.line8} <span>{userData.organization.name}</span> {GSData.tab1.wardNo} <span>{permitData.newWardNo}</span>{' '}
												{GSData.tab1.line9} {permitData.sadak} {GSData.tab1.toll}
												{/* <Input type="text" Placeholder="marga"></Input> */}
												<DashedLangInput name="marga" setFieldValue={setFieldValue} value={values.marga} error={errors.marga} /> {GSData.tab1.line10}{' '}
												<SurroundingSectionParagraph setFieldValue={setFieldValue} values={values} errors={errors} />
												{GSData.tab1.line11} <span>{permitData.kittaNo}</span> {GSData.tab1.line12}
												<span> {permitData.landAreaType}</span>
												{GSData.tab1.line12_1}
												{GSData.tab1.line12_2}
												<DashedLangInput
													name="eastwestMeter_input"
													setFieldValue={setFieldValue}
													value={values.eastwestMeter_input}
													error={errors.eastwestMeter_input}
												/>{' '}
												<UnitDropdown
													// label={GSData.tab1.line12_2}
													name="eastwestMeter"
													setFieldValue={setFieldValue}
													value={values.eastwestMeter}
													options={options}
												/>{' '}
												{GSData.tab1.line13}
												<DashedLangInput
													name="southNorthMeter_input"
													setFieldValue={setFieldValue}
													value={values.southNorthMeter_input}
													error={errors.southNorthMeter_input}
												/>{' '}
												<UnitDropdown
													// label={GSData.tab1.line13}
													name="southNorthMeter"
													setFieldValue={setFieldValue}
													value={values.southNorthMeter}
													options={options}
												/>{' '}
												<span>{GSData.tab1.line14}</span> {GSData.tab1.line14_1}
												<DashedLangInput
													name="eastwestMeter1_input"
													setFieldValue={setFieldValue}
													value={values.eastwestMeter1_input}
													error={errors.eastwestMeter1_input}
												/>{' '}
												<UnitDropdown
													// label={GSData.tab1.line14_1}
													name="eastwestMeter1"
													setFieldValue={setFieldValue}
													value={values.eastwestMeter1}
													options={options}
												/>{' '}
												{GSData.tab1.line15}
												<DashedLangInput
													name="southNorthMeter1_input"
													setFieldValue={setFieldValue}
													value={values.southNorthMeter1_input}
													error={errors.southNorthMeter1_input}
												/>{' '}
												<UnitDropdown
													// label={GSData.tab1.line15}
													name="southNorthMeter1"
													setFieldValue={setFieldValue}
													value={values.southNorthMeter1}
													options={options}
												/>{' '}
												{GSData.tab1.line16}
												<DashedLangInput
													name="height1_input"
													setFieldValue={setFieldValue}
													value={values.height1_input}
													error={errors.height1_input}
												/>{' '}
												<UnitDropdown
													// label={GSData.tab1.line16}
													name="height1"
													setFieldValue={setFieldValue}
													value={values.height1}
													options={options}
												/>{' '}
												{GSData.tab1.line17}
												<span>{userData.organization.name}</span> {/* <Input type="text" name=""></Input> */}
												<DashedLangInput name="karalaya" setFieldValue={setFieldValue} value={values.karalaya} error={errors.karalaya} />{' '}
												{GSData.tab1.line18}
											</p>

											<h2 className="NJ-title">
												<u>{GSData.tab1.tapasil}</u>
											</h2>
											<br />
											<p>{GSData.tab1.line19}</p>
											<EbpsTextArea name="uttar" onChange={handleChange} setFieldValue={setFieldValue} value={values.uttar} />
										</div>
										<div className="NJ-Main" style={{ pageBreakBefore: 'always' }}>
											<SurroundingSection setFieldValue={setFieldValue} values={values} errors={errors} />
											<br />
											<NeighbourWitnessSection userData={userData} setFieldValue={setFieldValue} values={values} errors={errors} />

											<br />

											{!isBirtamod && <AbsentSurroundingSection setFieldValue={setFieldValue} values={values} errors={errors} />}

											<h3 className="NJ-title" style={{ textAlign: 'left' }}>
												{GSData.tab2.subheading.subheading4}
											</h3>
											<br />
											<h2 className="NJ-title" style={{ textAlign: 'center' }}>
												{GSData.tab2.subheading.subheading6}
											</h2>
											<List>
												<List.Item>
													<span>{GSData.tab2.content.signature}</span> <span type="text" name="signs1" className=" ui input dashedForm-control" />{' '}
													<span>
														{GSData.tab2.content.content5} <span type="text" name="wardest1" className=" ui input dashedForm-control" />{' '}
														{GSData.tab2.content.content7}
													</span>{' '}
													<Field type="text" name="shree5" className="dashedForm-control" /> <br />
												</List.Item>

												<List.Item>
													<span>{GSData.tab2.content.signature}</span> <span type="text" name="signs2" className=" ui input dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content8}</span> <Field type="text" name="wadakarylaya" className="dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content8_1}</span> <Field type="text" name="shree6" className="dashedForm-control" /> <br />
												</List.Item>
											</List>
											<br />

											<h2 className="NJ-title" style={{ textAlign: 'left', textDecoration: 'underline' }}>
												{GSData.tab2.subheading.subheading5}
											</h2>

											<List>
												<List.Item>
													<span>{GSData.tab2.content.signature}</span> <span type="text" name="signs3" className=" ui input dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content9}</span> <Field type="text" name="shrii1" className="dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content9_1}</span> <Field type="text" name="paadh1" className="dashedForm-control" /> <br />
												</List.Item>

												<List.Item>
													<span>{GSData.tab2.content.signature}</span> <span type="text" name="signs4" className=" ui input dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content10}</span> <Field type="text" name="shrii2" className="dashedForm-control" />{' '}
													<span>{GSData.tab2.content.content9_1}</span> <Field type="text" name="paadh2" className="dashedForm-control" /> <br />
												</List.Item>
											</List>
											<br />
											<div style={{ textAlign: 'justify' }}>
												<span>
													{GSData.tab2.subcontent.sub1} <Field type="text" name="saal" className="dashedForm-control" /> {GSData.tab2.subcontent.sub2}{' '}
													<Field type="text" name="mahina" className="dashedForm-control" /> {GSData.tab2.subcontent.sub3}{' '}
													<Field type="text" name="gaate" className="dashedForm-control" /> {GSData.tab2.subcontent.sub4}{' '}
													<Field type="text" name="rooj" className="dashedForm-control" /> {GSData.tab2.subcontent.sub5}{' '}
													<Field type="text" name="subham" className="dashedForm-control" /> {GSData.tab2.subcontent.sub6}{' '}
												</span>
												<br />
											</div>
										</div>
									</div>
								</div>
								<br />
								<SaveButton hasSavePermission={this.props.hasSavePermission} prevData={getJsonData(prevData)} handleSubmit={handleSubmit} />
							</Form>
						)}
					</Formik>
				</>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	getPermitAndUserData,
	getFormDataWithPermitAndUser,
	setPrint: res => setPrint(res),
};
const mapStateToProps = state => {
	return {
		formData: state.root.formData,
		permitData: state.root.formData.permitData,
		userData: state.root.formData.userData,
		prevData: state.root.formData.prevData,
		errors: state.root.ui.errors,
		loading: state.root.ui.loading,
		printcontent: state.root.general.printcontent,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(GharNakshaSurjaminMuchulkaPrintViewComponent);
