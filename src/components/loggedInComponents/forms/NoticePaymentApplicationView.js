import React, { Component } from 'react';
import { Grid, Form, Button, Modal, Icon, Label, Divider } from 'semantic-ui-react';
import { noticePaymentLang, sundarHaraichaNotice, inaruwaNotice } from '../../../utils/data/noticePaymentApplicationLang';
import { Formik, getIn } from 'formik';
import { connect } from 'react-redux';
import api from '../../../utils/api';
import { showToast, isEmpty, getUserStatusNepali } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, toArray, checkError } from '../../../utils/dataUtils';
import { EbpsFormDateField, CompactDashedLangDate, DashedLangDateField } from '../../shared/DateField';
import { FileInputWithMultiplePreview } from '../../shared/FileUploadInput';
import { saveButton } from '../../../utils/data/genericData';
import { getDisableStatus } from '../../../utils/urlUtils';
import { getBackUrl } from '../../../utils/config';
import { getCurrentDate } from '../../../utils/dateUtils';
import { putFormDataByUrl } from '../../../store/actions/noticePaymentAction';
import * as Yup from 'yup';
import { validateExistingSingleFile, validateExistingMultiFile, validateNepaliDate } from '../../../utils/validationUtils';
import { FileView, FileViewThumbnail } from '../../shared/file/FileView';
import { CompactInfoMessage } from '../../shared/formComponents/CompactInfoMessage';
import { isIllam, isKankai, checkClient, Client, isInaruwa } from '../../../utils/clientUtils';
import { translateNepToEng, translateEngToNep } from '../../../utils/langUtils';
import { NoticePaymentFileView } from '../../shared/file/NoticePaymentFileView';
import { PrintIdentifiers, PrintParams } from '../../../utils/printUtils';
import { FormUtility } from '../../../utils/formUtils';
import { FormUrlFull } from '../../../utils/enums/url';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { LongFormDateNotice } from './formComponents/LongFormDate';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { getSalutation, getTitle } from '../../../utils/clientConfigs/noticePayment';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { SectionHeader } from '../../uiComponents/Headers';
import { DashedLangInput, DashedLangInputWithSlash } from '../../shared/DashedFormInput';
import { ShreeDropdown } from './formComponents/ShreeDropdown';
import { shreeOptions } from '../../../utils/optionUtils';

const noticeAppLang = noticePaymentLang;
const noticeFileValidations = Yup.object().shape({
	file: validateExistingMultiFile('fileUrl1'),
	file2: validateExistingSingleFile('fileUrl2'),
	noticeFileDate: validateNepaliDate,
});

class NoticePaymentApplicationViewComponent extends Component {
	constructor(props) {
		super(props);

		this.state = {
			open: false,
			formUrl: this.props.formUrl,
			salutation: {},
			head: {},
			isSundarHaraicha: false,
			isBirtamod: false,
			// fileCategories: this.props.otherData.fileCategories,
		};
	}

	componentDidMount() {
		const { orgCode } = this.props;
		const isBirtamod = checkClient(orgCode, Client.BIRTAMOD);
		const isSundarHaraicha = checkClient(orgCode, Client.SUNDARHARAICHA);

		this.setState({
			formUrl: this.props.formUrl,
			isSundarHaraicha: isSundarHaraicha,
			isBirtamod: isBirtamod,
			salutation: getSalutation(orgCode, this.props.userData.organization),
			head: getTitle(orgCode),
		});
	}

	handleSubmitModal = () => {
		this.handleClose();
		this.props.handleSubmit();
	};

	handleModalOpen = () => {
		this.setState({ open: true });
	};
	handleClose = () => {
		this.setState({ open: false });
	};
	// componentDidMount(){
	//     this.props.getFormDataWithPermit(api.noticePaymentApplication, data =>  data);
	// }

	render() {
		// const { fileCatId, hasMultipleFiles } = getFileCatId(
		// 	this.props.otherData.fileCategories,
		// 	this.props.parentProps.location.pathname
		// );

		// console.log(
		// 	'fileCatId',
		// 	fileCatId,
		// 	hasMultipleFiles,
		// 	this.props.otherData.fileCategories
		// );

		let initVal = {};

		const { permitData, prevData, userData, otherData } = this.props;
		const { isSundarHaraicha, isBirtamod, salutation, head } = this.state;

		// if(permitData && prevData){
		const json_data = getJsonData(prevData);
		const notice15 = getJsonData(otherData.notice15);
		const isSave = isEmpty(json_data);
		const { erStatus, serStatus, rwStatus, chiefStatus, aminiStatus } = prevData;
		let isDisabled = false;

		try {
			//   anyone accept and not anyone reject
			isDisabled = getDisableStatus(this.state.formUrl, {
				erStatus,
				serStatus,
				rwStatus,
				chiefStatus,
				aminiStatus,
			});
			// console.log('in trye', isDisabled);
			// (erStatus === 'A' || serStatus === 'A' || rwStatus === 'A') &&
			// !(erStatus === 'R' || serStatus === 'R' || rwStatus === 'R');
		} catch {
			isDisabled = false;
		}
		// console.log('is disbale', isDisabled);
		// notice15.date = '2019-12-12'

		initVal = prepareMultiInitialValues(
			{
				obj: notice15,
				reqFields: ['date', 'surrounding', 'Sangyardate', 'chalaniNumber'],
			},
			{
				obj: {
					shree: shreeOptions[0].value,
				},
				reqFields: [],
			},
			{
				obj: json_data,
				reqFields: [],
			}
		);

		// console.log('notice', notice15, initVal)

		// const { erStatus: erStatus = 'P', serStatus: serStatus = 'P'} = this.props.prevData;

		// initVal = json_data;

		let notice15EnterDate = getCurrentDate();

		if (checkError(prevData).noticeDate) {
			notice15EnterDate = checkError(prevData).noticeDate;
		} else {
			if (isIllam) {
				notice15EnterDate = translateNepToEng(notice15.Sangyardate);
			}
		}

		const fileUploadInitialValue = { noticeFileDate: notice15EnterDate, fileUrl1: prevData.fileUrl1, fileUrl2: prevData.fileUrl2 };

		return (
			<div className="noticePayAppForomWrap">
				<Formik
					key="notice-data"
					initialValues={initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;

						// const data = new FormData();
						// const selectedFile = values.file;
						// let isFileSelected = false;
						// if (selectedFile) {
						// 	for (var x = 0; x < selectedFile.length; x++) {
						// 		data.append('file', selectedFile[x]);
						// 	}
						// 	data.append('fileType', fileCatId);
						// 	data.append('applicationNo', permitData.applicantNo);
						// 	isFileSelected = true;
						// 	values.file && delete values.file;
						// }

						// Object.keys(values).forEach(key => {
						// 	data.set(key, values[key]);
						// });

						try {
							await this.props.postAction(`${api.noticePaymentApplication}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								// 	getNextUrl(this.props.parentProps.location.pathname)
								// );
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, setFieldValue, values, errors }) => (
						// <FormWrapper erStatus={erStatus}>
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="print-small-font">
								{isInaruwa ? (
									<>
										<SectionHeader>
											<h3>{userData.organization.name}</h3>
											<h4 className="underline">{inaruwaNotice.heading}</h4>
											<h4>{inaruwaNotice.subTitle}</h4>
										</SectionHeader>
									</>
								) : (
									<>
										{head.letterHead && (
											<>
												<LetterHeadFlex userInfo={userData} />
												<div className="section-header">
													<h4 className="bottom-margin">{head.subTitle}</h4>
												</div>
											</>
										)}
										<div className="section-header">
											<h3 className="underline">{head.heading}</h3>
										</div>
										{salutation.needsSalutation && (
											<>
												<br />
												<LetterSalutation lines={salutation.lines} boldLines={salutation.boldLines} />
											</>
										)}
									</>
								)}
								<br />
								<p style={{ textAlign: 'justify' }}>
									{isInaruwa ? (
										<>
											<span className="indent-span">{userData.organization.name}</span>
											{noticeAppLang.contentLine_2}
											{permitData.nibedakTol}
											{inaruwaNotice.content_5}
											{permitData.nibedakSadak}
											{inaruwaNotice.content_6}
											<ShreeDropdown name="shree" />
											{permitData.nibedakName}
											{noticeAppLang.contentLine_6.contentLine_6_1}
											{userData.organization.name}
											{inaruwaNotice.content_7}
											<span className="ui input dashedForm-control" name="date"></span>
											{noticeAppLang.contentLine_6.contentLine_6_3}
											<DashedLangInputWithSlash
												name="chalaniNumber"
												handleChange={handleChange}
												setFieldValue={setFieldValue}
												value={values.chalaniNumber}
												error={errors.chalaniNumber}
												inline={true}
											/>
											{inaruwaNotice.contentLine_8}
											<DashedLangInput
												name="nirmanSthal"
												setFieldValue={setFieldValue}
												value={values.nirmanSthal}
												error={errors.nirmanSthal}
											/>
											{inaruwaNotice.content_9}
										</>
									) : (
										<>
											{isBirtamod ? (
												<>
													{noticeAppLang.contentLine_1_birtamod}
													{noticeAppLang.contentLine_1_birtamod_ko} {noticeAppLang.contentLine_1_birtamod_sthan}{' '}
													{permitData.nibedakSadak}
													{noticeAppLang.contentLine_2}
													{permitData.nibedakTol}
													{noticeAppLang.contentLine_2_birtamod}
													{permitData.kittaNo}
												</>
											) : (
												<>
													{userData.organization.name} {permitData.nibedakSadak}
													{noticeAppLang.contentLine_2}
													{permitData.nibedakTol}
													{noticeAppLang.contentLine_3}
													{permitData.kittaNo}
												</>
											)}
											{noticeAppLang.contentLine_4}
											{permitData.landArea} {permitData.landAreaType}
											{noticeAppLang.contentLine_5}
											{permitData.nibedakName}
											{noticeAppLang.contentLine_6.contentLine_6_1}
											{isBirtamod ? <>{noticeAppLang.contentLine_1_birtamod} </> : <>{userData.organization.name}</>}
											<>
												{isKankai ? (
													<span>
														{noticeAppLang.contentLine_6.contentLine_6_2} {translateEngToNep(permitData.applicantNo)}{' '}
													</span>
												) : (
													<span>
														{noticeAppLang.contentLine_6.contentLine_6_3}
														<DashedLangInput
															name="chalaniNumber"
															setFieldValue={setFieldValue}
															value={values.chalaniNumber}
															error={errors.chalaniNumber}
														/>
													</span>
												)}
											</>
											{noticeAppLang.contentLine_7}
											<DashedLangDateField
												compact={true}
												inline={true}
												name="Sangyardate"
												setFieldValue={setFieldValue}
												value={values.Sangyardate}
												error={errors.Sangyardate}
											/>
											{noticeAppLang.contentLine_8}
										</>
									)}
								</p>

								<div className="sandhiyarGrid-wrap">
									{isInaruwa ? (
										<>
											<Grid columns={3} className="flex-item-flex-start">
												{Object.values(noticeAppLang.surrounding).map((sur, index) => {
													const side = values.surrounding && values.surrounding.find((sr) => sr.side === index + 1);
													const name = !!side ? side.sandhiyar : '';
													return (
														<Grid.Row key={index}>
															<Grid.Column>
																<p>
																	{sur} {name}
																</p>
															</Grid.Column>
															<Grid.Column>
																<p>
																	{noticeAppLang.dashtakhat}
																	<span className="ui input dashedForm-control" />
																</p>
															</Grid.Column>
															<Grid.Column>
																<p>
																	{noticeAppLang.contentLine_7}
																	<CompactDashedLangDate
																		name={`surroundDate.${index}`}
																		value={getIn(values, `surroundDate.${index}`)}
																		error={errors.surroundDate}
																		setFieldValue={setFieldValue}
																	/>
																</p>
															</Grid.Column>
														</Grid.Row>
													);
												})}
											</Grid>
										</>
									) : (
										<>
											<Grid columns={2} className="flex-item-flex-start">
												{Object.values(noticeAppLang.surrounding).map((sur, index) => {
													const side = values.surrounding && values.surrounding.find((sr) => sr.side === index + 1);
													const name = !!side ? side.sandhiyar : '';
													return (
														<Grid.Row key={index}>
															<Grid.Column>
																<p>
																	{sur} {name}
																</p>
															</Grid.Column>
															<Grid.Column>
																<p>
																	{noticeAppLang.dashtakhat}
																	<span className="ui input dashedForm-control" />
																</p>
															</Grid.Column>
														</Grid.Row>
													);
												})}
											</Grid>
										</>
									)}
									<br />
									<div className="section-header">
										<p className="left-align small">{noticeAppLang.sachiHaru}</p>
									</div>
									<br />
									<Grid columns="2" className="flex-item-flex-start">
										{noticeAppLang.sachiCount.map((count, idx) => (
											<Grid.Row key={count}>
												<Grid.Column>
													{count}. {noticeAppLang.shree}{' '}
													<DashedLangInput
														name={`witness.${idx}`}
														setFieldValue={setFieldValue}
														value={getIn(values, `witness.${idx}`)}
														error={getIn(errors, `witness.${idx}`)}
													/>
												</Grid.Column>
												{isInaruwa ? (
													<></>
												) : (
													<Grid.Column>
														<p>
															{noticeAppLang.dashtakhat} <span className="ui input dashedForm-control" />
														</p>
													</Grid.Column>
												)}
											</Grid.Row>
										))}
									</Grid>
								</div>
								{isBirtamod ? (
									<GharDhaniSectionBirtamod permitData={permitData} />
								) : isKankai ? (
									<GharDhaniSectionKankai permitData={permitData} setFieldValue={setFieldValue} errors={errors} values={values} />
								) : isSundarHaraicha ? (
									<GharDhaniSectionSundarHaraicha
										permitData={permitData}
										setFieldValue={setFieldValue}
										errors={errors}
										values={values}
									/>
								) : isInaruwa ? (
									<GharDhaniSectionInaruwa setFieldValue={setFieldValue} values={values} errors={errors} userData={userData} />
								) : (
									<GharDhaniSection permitData={permitData} setFieldValue={setFieldValue} errors={errors} values={values} />
								)}

								<LongFormDateNotice />
								<br />

								{/* <p>
									<FileInputWithPreview
										hasMultipleFiles={hasMultipleFiles}
										fileCatId={fileCatId}
										name="file"
									/>
								</p> */}
							</div>
							{/* <SaveButtonNoticePayment
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}

							<Button
								primary
								type="submit"
								onClick={() => {
									handleSubmit();
									this.handleModalOpen();
								}}
							>
								{isSave ? 'Save' : 'Update'}
							</Button>
							{isDisabled && (
								<Label basic color="green" size="large">
									<Icon name="check" />
									{getUserStatusNepali('A')}
								</Label>
							)}

							{/* {this.props.hasSavePermission && (
								<Button
									type="submit"
									disabled={isSubmitting}
									onClick={handleSubmit}
								>
									Save
								</Button>
							)
							// :
							// <ApprovalGroup history={this.props.history} url={`${api.noticePaymentApplication}${permitData.applicationNo}`}/>
							} */}
						</Form>
						// {/* </FormWrapper> */}
					)}
				</Formik>
				{/* <ApprovalStatusDisplay erStatus={erStatus} serStatus={serStatus} comments={comment} /> */}
				{/* <HistorySidebar history={formHistory}/> */}
				<Formik
					key="notice-file-upload"
					initialValues={{ ...fileUploadInitialValue }}
					validationSchema={noticeFileValidations}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);

						const data = new FormData();

						const selectedFile1 = values.file;
						const selectedFile2 = values.file2;
						// console.log('selectedFile');
						// console.log(selectedFile);
						if (selectedFile1) {
							selectedFile1.forEach((file) => data.append('file', file));
						} else {
							actions.setError('Please select a file before uploading.');
						}

						if (selectedFile2) {
							selectedFile2.forEach((file) => data.append('file2', file));
						} else {
							actions.setError('Please select a file before uploading.');
						}

						try {
							await this.props.parentProps.putFormDataByUrl(
								`${getBackUrl()}${api.noticePaymentApplication}${this.props.permitData.applicantNo}/${values.noticeFileDate}`,
								data
							);

							if (this.props.success && this.props.success.success) {
								showToast(this.props.success.data.message || 'File uploaded');
								this.setState(() => ({
									needToGet: true,
								}));
								actions.resetForm({
									imagePreviewUrl: [],
									uploadFile: '',
								});
								this.props.parentProps.history.push('/user/task-list');
								// document.getElementById(`file_${category.id}`).value = "";
								this.handleClose();
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('File upload failed', err);
						}

						// console.log('data to send', data, /);
					}}
				>
					{({ handleSubmit, errors, handleChange, values, isSubmitting }) => {
						return (
							<Modal key="save-confirmation" closeIcon open={this.state.open} onClose={this.handleClose}>
								<Modal.Header>{saveButton.uploadFileReminder}</Modal.Header>
								<Modal.Content scrolling>
									<Form loading={isSubmitting}>
										{ isBirtamod ? 
											(
												<div>
													<label>{noticePaymentLang.fileDateLabel}</label>
													{values.noticeFileDate}
												</div>
											) 
											: (
											<div className="ui form input">
												<EbpsFormDateField
													label={noticePaymentLang.fileDateLabel}
													handleChange={isIllam ? undefined : handleChange}
													disabled={isIllam}
													name="noticeFileDate"
													value={values.noticeFileDate}
													error={errors.noticeFileDate}
												/>
											</div> 
											) 
										}
										<Grid>
											<Grid.Row centered>
												<Divider horizontal>{noticePaymentLang.fileTitle}</Divider>
											</Grid.Row>
											<Grid.Row columns={2} divided>
												<Grid.Column>
													<div className="ui form input">
														<FileInputWithMultiplePreview name="file" fileCatId={12} hasMultipleFiles={true} />
													</div>
												</Grid.Column>
												<Grid.Column>
													{values.fileUrl1 && Array.isArray(toArray(values.fileUrl1)) ? (
														<>
															<strong>{noticePaymentLang.existingFile}</strong>
															<Grid.Row columns="1">
																{toArray(values.fileUrl1).map((fl, index) => (
																	<Grid.Column key={index}>
																		<FileViewThumbnail el={{ fileUrl: fl }} deletable={false} />
																	</Grid.Column>
																))}
															</Grid.Row>
														</>
													) : (
														<CompactInfoMessage content={noticePaymentLang.noFilesUploaded} />
													)}
												</Grid.Column>
											</Grid.Row>
											<Grid.Row centered>
												<Divider horizontal>{noticePaymentLang.fileTitle2}</Divider>
											</Grid.Row>
											<Grid.Row columns={2} divided>
												<Grid.Column>
													<div className="ui form input">
														<FileInputWithMultiplePreview name="file2" fileCatId={11} hasMultipleFiles={false} />
													</div>
												</Grid.Column>
												<Grid.Column>
													{values.fileUrl2 ? (
														<>
															<strong>{noticePaymentLang.existingFile}</strong>
															<FileView el={{ fileUrl: values.fileUrl2 }} deletable={false} />
														</>
													) : (
														<CompactInfoMessage content={noticePaymentLang.noFilesUploaded} />
													)}
												</Grid.Column>
											</Grid.Row>
										</Grid>
									</Form>
								</Modal.Content>
								<Modal.Actions>
									<Button negative size="tiny" disabled={isSubmitting} onClick={this.handleClose}>
										Exit
									</Button>
									<Button
										positive
										disabled={isSubmitting}
										type="submit"
										size="tiny"
										icon="checkmark"
										labelPosition="right"
										content={isSave ? 'Save Form' : 'Update Form'}
										onClick={handleSubmit}
									/>
								</Modal.Actions>
							</Modal>
						);
					}}
				</Formik>
			</div>
		);
		// }
		// else {
		//     return (
		//         <FallbackComponent errors={reduxErrors} loading={loading}/>
		//     );
		// }
	}
}

const mapDispatchToProps = { putFormDataByUrl };

// const mapStateToProps = state => ({
//     permitData: state.root.formData.permitData,
// 	prevData: state.root.formData.prevData,
// 	comment: state.root.formData.comment,
// 	formHistory: state.root.formData.history,
// 	loading: state.root.ui.loading,
// 	errors: state.root.ui.errors,
// 	success: state.root.formData.success
// })

const NoticeApprovalView = (props) => {
	return (
		<div ref={props.setRef}>
			<NoticePaymentFileView prevData={checkError(props.prevData)} />
		</div>
	);
};

const GharDhaniSection = ({ permitData, setFieldValue, values, errors }) => {
	return (
		<div className="top-margin-1">
			<br />
			<div>{noticeAppLang.gharDhani}:-</div>
			<div className="div-indent">
				<Grid>
					<Grid.Row columns="2">
						<Grid.Column width="4">
							{noticeAppLang.shree} {permitData.nibedakName}
						</Grid.Column>
						<Grid.Column>
							{noticeAppLang.dashtakhat}:
							<span className="ui input dashedForm-control" />
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
			<br />
			<UpauktaSection setFieldValue={setFieldValue} values={values} errors={errors} />

			<div className="flex-item-flex-end">
				<WadaSachibNameSignature permitData={permitData} />
			</div>
			<div>
				<div>
					{sundarHaraichaNotice.signature.kamTalmel} <span className="ui input dashedForm-control" />
				</div>
				<div>
					{sundarHaraichaNotice.signature.name}{' '}
					<DashedLangInput name="kamTamelName" setFieldValue={setFieldValue} value={values.kamTamelName} error={errors.kamTamelName} />
				</div>
				<div>
					{sundarHaraichaNotice.signature.designation}
					<DashedLangInput
						name="kamTamelDesignation"
						setFieldValue={setFieldValue}
						value={values.kamTamelDesignation}
						error={errors.kamTamelDesignation}
					/>
				</div>
				<br />
			</div>
		</div>
	);
};
const GharDhaniSectionInaruwa = ({ setFieldValue, values, errors, userData }) => {
	return (
		<div>
			<br />
			<UpauktaSection setFieldValue={setFieldValue} errors={errors} values={values} />
			<div className="flex-item-flex-start">
				<p>
					{inaruwaNotice.content_4}:
					<span className="ui input dashedForm-control" />
				</p>
			</div>
			<div className="flex-item-flex-end">
				<p>
					{noticeAppLang.dashtakhat}:
					<span className="ui input dashedForm-control" />
				</p>
			</div>
			<div>
				<p>
					{inaruwaNotice.content_1}
					<DashedLangInput name="tamelName" setFieldValue={setFieldValue} value={values.tamelName} error={errors.tamelName} />
					{inaruwaNotice.content_2}
					<DashedLangInput name="pad" setFieldValue={setFieldValue} value={values.pad} error={errors.pad} />
					<br />
					<DashedLangInput name="ward" setFieldValue={setFieldValue} value={values.ward} error={errors.ward} />
					{inaruwaNotice.content_3}
					{userData.organization.name}
					<br />
					{noticeAppLang.dashtakhat}:
					<span className="ui input dashedForm-control" />
				</p>
			</div>
		</div>
	);
};

const GharDhaniSectionSundarHaraicha = ({ permitData, setFieldValue, errors, values }) => {
	return (
		<div>
			<br />
			<UpauktaSection setFieldValue={setFieldValue} errors={errors} values={values} />
			<div className="flex-item-flex-start">
				<p>
					{noticeAppLang.dashtakhat}:
					<span className="ui input dashedForm-control" />
				</p>
				<p>
					{noticeAppLang.gharDhaniName} {permitData.nibedakName}
				</p>
			</div>
			<div className="flex-item-space-between">
				<div>
					<div>
						{noticeAppLang.name}:
						<span className="ui input dashedForm-control" />
					</div>
					<div>
						{noticeAppLang.post}: {noticeAppLang.desgination} {permitData.newWardNo} {sundarHaraichaNotice.signature.wada}
					</div>
					<div>
						{noticeAppLang.dashtakhat}:
						<span className="ui input dashedForm-control" />
					</div>
				</div>
				<div>
					<span className="ui input signature-placeholder" />
					<div>{sundarHaraichaNotice.signature.wadaAdhaksya}</div>
					<div>
						{permitData.newWardNo} {sundarHaraichaNotice.signature.noWada}
					</div>
				</div>
			</div>
			<br />
			<div>
				<div>
					{sundarHaraichaNotice.signature.kamTalmel} <span className="ui input dashedForm-control" />
				</div>
				<div>
					{sundarHaraichaNotice.signature.name} <span className="ui input dashedForm-control" />
				</div>
				<div>
					{sundarHaraichaNotice.signature.designation} <span className="ui input dashedForm-control" />
				</div>
			</div>
			<br />
		</div>
	);
};

const GharDhaniSectionBirtamod = ({ permitData }) => {
	return (
		<div>
			<div className="section-header">
				<br />
				<p className="underline small left-align">{noticeAppLang.gharDhani}</p>
			</div>
			<Grid columns={2}>
				<Grid.Row>
					<Grid.Column>
						<div className="inline fields">
							<label style={{ marginLeft: '15px' }}>{noticeAppLang.shree}</label>
							<div style={{ marginTop: '3px' }}>{permitData.nibedakName}</div>
						</div>
					</Grid.Column>
					<Grid.Column>
						<p>
							{noticeAppLang.dashtakhat}
							<span className="ui input dashedForm-control" />
						</p>
					</Grid.Column>
				</Grid.Row>
			</Grid>
			<br />
			<p>
				{noticeAppLang.concluContent_1}
				<span className="ui input signature-placeholder" />
				{noticeAppLang.concluContent_2}
			</p>
			<div className="flex-item-flex-start">
				<WadaSachibNameSignature permitData={permitData} />
			</div>
			<br />
		</div>
	);
};

const GharDhaniSectionKankai = ({ permitData, setFieldValue, errors, values }) => {
	return (
		<div>
			<br />
			<UpauktaSection setFieldValue={setFieldValue} errors={errors} values={values} />
			<div className="flex-item-flex-start">
				<p>
					{noticeAppLang.dashtakhat}:
					<span className="ui input dashedForm-control" />
				</p>
				<p>
					{noticeAppLang.gharDhaniName} {permitData.nibedakName}
				</p>
			</div>
			<div className="flex-item-flex-start">
				<p>
					{noticeAppLang.talmelGarne} {noticeAppLang.dashtakhat}: <span className="ui input dashedForm-control" />
				</p>
				<p>
					{noticeAppLang.name}: <span className="ui input dashedForm-control" />
				</p>
				<p>
					{noticeAppLang.post}: <span className="ui input dashedForm-control" />
				</p>
			</div>
		</div>
	);
};

const UpauktaSection = ({ setFieldValue, values, errors }) => {
	return (
		<div className="div-indent margin-bottom">
			{noticeAppLang.concluContent_1}
			<DashedLangInput name="submittedBy" setFieldValue={setFieldValue} value={values.submittedBy} error={errors.submittedBy} />
			{noticeAppLang.concluContent_2}
		</div>
	);
};

const WadaSachibNameSignature = ({ permitData }) => {
	return (
		<div>
			<p>
				{noticeAppLang.dashtakhat}:
				<span className="ui input dashedForm-control" />
			</p>
			<p>
				{noticeAppLang.name}:
				<span className="ui input dashedForm-control" />
			</p>
			<p>
				{noticeAppLang.post}: {noticeAppLang.desgination}
			</p>
			<p>
				{permitData.newWardNo} {noticeAppLang.officeWardNo}
			</p>
		</div>
	);
};

// export default connect(mapStateToProps, mapDispatchToProps)(NoticePaymentApplicationView)
const NoticePaymentApplicationView = (parentProps) => (
	<FormContainerV2
		hasFileView={true}
		api={[
			{ api: api.noticePaymentApplication, objName: 'noticeApp', form: true },
			{ api: api.noticePeriodFor15Days, objName: 'notice15', form: false },
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.DASHED_INPUT,
			param1: [PrintIdentifiers.GENERIC_REMOVE_ON_PRINT],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		formUtility={new FormUtility(FormUrlFull.NOTICE_PAYMENT_APPLICATION)}
		// hasFile={true}
		render={(props) => {
			return props.hasSavePermission ? (
				<NoticePaymentApplicationViewComponent {...props} parentProps={parentProps} />
			) : (
				<NoticeApprovalView {...props} />
			);
		}}
	/>
);

export default connect(null, mapDispatchToProps)(NoticePaymentApplicationView);
