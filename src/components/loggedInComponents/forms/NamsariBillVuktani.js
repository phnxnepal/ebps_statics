import React from 'react';
import api from '../../../utils/api';
import { checkError } from '../../../utils/dataUtils';
import { GenericApprovalFileView } from '../../shared/file/GenericApprovalFileView';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { GenericBillVulktani } from './formComponents/GenericBillVulktani';

const NamsariBillVuktani = parentProps => (
	<FormContainerV2
		api={[{ api: api.namsariBillVuktani, objName: 'namsariBillVuktani', form: true }]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={props =>
			props.hasSavePermission ? (
				<GenericBillVulktani {...props} parentProps={parentProps} api={api.namsariBillVuktani} titleKey="namsariBill" />
			) : (
				<div ref={props.setRef}>
					<GenericApprovalFileView
						fileCategories={props.fileCategories}
						files={props.files}
						url={props.formUrl}
						prevData={checkError(props.prevData)}
					/>
				</div>
			)
		}
	/>
);

export default NamsariBillVuktani;
