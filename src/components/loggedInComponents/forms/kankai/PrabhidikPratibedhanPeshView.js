import React, { Component } from 'react';
import { Formik } from 'formik';
import merge from 'lodash/merge';
import { Form } from 'semantic-ui-react';
import { prabidhikpratibedhan } from '../../../../utils/data/mockLangFile';
import { isEmpty } from '../../../../utils/functionUtils';
import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangDateField } from './../../../shared/DateField';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { DashedLangInput, DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { checkError } from './../../../../utils/dataUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { kankaiprabidhik, sundarHariacha } from '../../../../utils/data/prabidhikData';
import { DashedUnitInput, DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import EbpsTextareaField from '../../../shared/MyTextArea';
import {
	getConstructionTypeValue,
	getCertificateConstructionType,
	ConstructionTypeValue,
} from '../../../../utils/enums/constructionType';
import { BuildingRoadCheckbox } from '../mapPermitComponents/BuildingRoadCheckbox';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { getSubHeading } from '../../../../utils/clientConfigs/prabidhikPratibedan';
import { checkClient, Client, isKageyshowri } from '../../../../utils/clientUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { PurposeOfConstructionRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FlexSingleRight, FlexSpaceBetween } from '../../../uiComponents/FlexDivs';
import { prabidhikSchemaKamalamai } from '../../formValidationSchemas/prabidhikPratibedanSchema';
import { getSaveByUserDetails, getApproveByObject } from '../../../../utils/formUtils';
import { DetailedSignature } from '../formComponents/FooterSignature';
import { SansodhanCheckbox } from '../formComponents/SansodhanCheckbox';
import { RoadSetbackTable } from '../mapTechFormComponents/RoadSetbackTable';

const kankai_data = kankaiprabidhik;
const pppv_data = prabidhikpratibedhan.prabidhikpratibedhan_data;

class PrabhidikPratibedhanPeshKageyshowriViewComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, orgCode, enterByUser, userData, DEFAULT_UNIT_LENGTH } = this.props;

		const { heading, salutation } = getSubHeading(orgCode);
		const isSundarHaraicha = checkClient(orgCode, Client.SUNDARHARAICHA);
		const isKamalamai = checkClient(orgCode, Client.KAMALAMAI);
		// Other_SEt_BacK
		// const otherSetBackMaster = this.otherData.otherSetBack;

		// let nadiOptions = [{ value: mapTechLang.mapTech_23.defaultNadiOption, text: mapTechLang.mapTech_23.defaultNadiOption }];
		// otherSetBackMaster.map(
		// 	row =>
		// 		row.status === 'Y' &&
		// 		nadiOptions.push({
		// 			value: row.placeName,
		// 			text: row.placeName,
		// 		})
		// );
		let initialValues = {};
		// initialValues.topFloorLen = []
		initialValues.sanso = pppv_data.sansodhan_check[0];
		initialValues.batoChaudaiUnit = 'METRE';
		// initialValues.jaggaDuriUnit = 'METRE';
		// initialValues.bahiriBhagDuriUnit = 'METRE';
		const mapTech = getJsonData(otherData.mapTech);
		const RajaswoData = getJsonData(otherData.RajaswoData);

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);

		const initialValues1 = prepareMultiInitialValues(
			{ obj: { publicPropertyUnit: DEFAULT_UNIT_LENGTH, highTensionUnit: DEFAULT_UNIT_LENGTH }, reqFields: [] },
			{
				obj: mapTech,
				reqFields: [
					// 'purposeOfConstruction',
					// 'purposeOfConstructionOther',
					// 'highTension',
					// 'highTensionUnit',
					// 'publicPropertyRequiredDistance',
					// 'publicPropertyDistance',
					// 'publicPropertyUnit',
					// 'setBack',
					// 'setBackUnit',
				],
			},
			{
				obj: RajaswoData,
				reqFields: ['namedMapdanda', 'namedAdhikar', 'namedBorder', 'namedUnit'],
			},
			{
				obj: getSaveByUserDetails(enterByUser, userData),
				reqFields: [],
			},
			{
				obj: {
					bottomDate : getCurrentDate(true),
					constructionType: permitData.constructionType,
					isHighTensionLine: permitData.isHighTensionLine,
					buildingJoinRoadType: permitData.buildingJoinRoadType,
					buildingJoinRoadTypeOther: permitData.buildingJoinRoadTypeOther,
					floor: permitData.floor.find((row) => row.floor === 1) || {},
				},
				reqFields: ['constructionType', 'isHighTensionLine', 'floor', 'buildingJoinRoadType', 'buildingJoinRoadTypeOther'],
			},
			{ obj: getJsonData(otherData.SurjaminDate), reqFields: ['surjaminDate'] },
			{ obj: getJsonData(prevData), reqFields: [] },
			{
				obj: {
					sifarisName: sipharisData.name,
					sifarisDesignation: sipharisData.designation,
					sifarisSignature: sipharisData.signature,
				},
				reqFields: [],
			}
		);
		initialValues1.floorUnit = initialValues1.floorUnit && initialValues1.floorUnit.replace('SQUARE', '').trim();
		initialValues1.constructionType = getConstructionTypeValue(initialValues1.constructionType);

		const isPuranoGhar = initialValues1.constructionType === ConstructionTypeValue.PURANO_GHAR;
		// console.log('trimed floorunit', initialValues1.floorUnit);
		const initVal = merge(initialValues, initialValues1);
		// const detailed_Info = this.permitData;

		// let prabhidik_data = {};
		let prabhidik_option_data = {};
		// let initVal = {};

		// if (this.prevData) {
		// 	prabhidik_data.constructionType = getConstructionTypeValue(this.prevData.constructionType);
		// }

		if (this.props.otherData.mapTech) {
			prabhidik_option_data.roof = this.props.otherData.mapTech.roof;
			prabhidik_option_data.plinthDetails = this.props.otherData.mapTech.plinthDetails;
		}

		if (isStringEmpty(initVal.prabidhikPratibedanDate)) {
			initVal.prabidhikPratibedanDate = getCurrentDate(true);
		}
		if (isStringEmpty(initVal.date1)) {
			initVal.date1 = getCurrentDate(true);
		}
		if (isStringEmpty(initVal.date2)) {
			initVal.date2 = getCurrentDate(true);
		}
		if (isStringEmpty(initVal.rightDate)) {
			initVal.rightDate = getCurrentDate(true);
		}
		
		this.state = {
			initVal,
			isSundarHaraicha,
			isKamalamai,
			heading,
			salutation,
			isPuranoGhar,
		};
	}

	render() {
		const { isSundarHaraicha, isKamalamai, initVal, heading, salutation, isPuranoGhar } = this.state;
		const {
			permitData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			userData: user_info,
			useSignatureImage,
		} = this.props;

		return (
			<div className="view-Form">
				<Formik
					initialValues={initVal}
					validationSchema={prabidhikSchemaKamalamai(isPuranoGhar)}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						// const datatosend = {};

						try {
							let url = '';
							if (values.sanso === pppv_data.sansodhan_check[1]) {
								url = `${api.prabhidikPratibedhanPesh}${permitData.applicantNo}/Y`;
							} else {
								url = `${api.prabhidikPratibedhanPesh}${permitData.applicantNo}/N`;
							}

							// refetch the menu as the menu will be updated
							await this.props.postAction(url, values, false, false, true);

							// await this.props.postAction(
							// 	api.prabhidikPratibedhanPesh,
							// 	values,
							// 	true
							// );
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 1000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);

								// if (
								// 	permitData.constructionType === ConstructionType.PURANO_GHAR
								// ) {
								// 	this.props.parentProps.history.push(
								// 		`/user${FormUrl.PURANO_GHAR_NIRMAN_TIPPANI}`
								// 	);
								// } else {
								// 	if (values.sanso === pppv_data.sansodhan_check[1]) {
								// 		setLocalStorage(PRABIDHIK_SANSODHAN, 'Yes');
								// 		this.props.parentProps.history.push(
								// 			`/user${FormUrl.SANSODHAN_TIPPANI}`
								// 		);
								// 	} else {
								// 		this.props.parentProps.history.push(
								// 			// `/user${FormUrl.NOTE_ORDER_PLINTH_LEVEL}`
								// 			getNextUrl(this.props.parentProps.location.pathname)
								// 		);
								// 	}
								// }

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 2000);
							}
							window.scroll(0, 0);
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, setFieldValue, errors, handleChange, isSubmitting, handleSubmit, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef}>
									<div className="no-margin-field">
										{isKageyshowri ?  null : <LetterHeadFlex userInfo={user_info} /> }
										
										<div className="section-header">
											<h3 className="underline">{heading}</h3>
										</div>


										{isKamalamai ? (
											<FlexSingleRight>
												{kankai_data.kamalamaiData.date}
												<DashedLangDateField
													name="prabidhikPratibedanDate"
													handleChange={handleChange}
													value={values.prabidhikPratibedanDate}
													error={errors.prabidhikPratibedanDate}
													setFieldValue={setFieldValue}
													inline={true}
													compact={true}
												/>
											</FlexSingleRight>
										) : (
											''
										)}
										{/* <FormHeading userinfo={this.userData} /> */}
										{isKamalamai ? (
											<>
												{kankai_data.kamalamaiData.data_1}
												{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
												{kankai_data.content_2_3}
												{permitData.newWardNo}
												{kankai_data.kamalamaiData.data_2}
												{permitData.kittaNo}
												{kankai_data.kamalamaiData.data_3}
												{permitData.landArea} {permitData.landAreaType}
												{kankai_data.kamalamaiData.data_4}
												{permitData.nibedakName}
												{kankai_data.kamalamaiData.data_5}
												<DashedLangDateField
													name="surjaminDate"
													handleChange={handleChange}
													value={values.surjaminDate}
													error={errors.surjaminDate}
													setFieldValue={setFieldValue}
													inline={true}
													compact={true}
												/>
												{kankai_data.content_2_8}
											</>
										) : isKageyshowri ? (
											<>
											<FlexSingleRight>
												{kankai_data.date} : 
												<DashedLangDateField
													name="rightDate"
													inline={true}
													compact={true}
													onChange={handleChange}
													value={values.rightDate}
													setFieldValue={setFieldValue}
												/>
											</FlexSingleRight>
											<br/>
											<SectionHeader>
												<h4 className="underline">{kankai_data.subject}</h4>
											</SectionHeader>
											<br/>

												<br />
												<span className="indent-span">{kankai_data.content_2_1}</span>
												{' '}{permitData.nibedakName}{' '}
												{kankai_data.content_2_2}
												{' '}{user_info.organization.name}{' '}
												{kankai_data.content_2_3}
												{' '}{permitData.newWardNo}{' '}
												{kankai_data.content_2_4}
												{' '}{permitData.kittaNo}{' '}
												{kankai_data.content_2_5}
												{' '}{permitData.landArea}{' '}
												{kankai_data.kageyshowriData.bamiBhayeko}
												{' '}{permitData.sadak}{' '}

												<DashedLangDateField 
													name="date1"
													inline={true}
													label={kankai_data.kageyshowriData.content1}
													setFieldValue={setFieldValue}
													value={values.date1}
													error={errors.date1}
												/>
												{kankai_data.kageyshowriData.content2}

												<br />
											</>	
										) 
										:(
											<div>
												{salutation}
												<br />
												<span className="indent-span">{kankai_data.content_2_1}</span>
												{permitData.nibedakName}
												{kankai_data.content_2_2}
												{user_info.organization.name}
												{kankai_data.content_2_3}
												{permitData.newWardNo}
												{kankai_data.content_2_4}
												{permitData.kittaNo}
												{kankai_data.content_2_5}
												{permitData.landArea} {permitData.landAreaType} {kankai_data.content_2_6}{' '}
												{getCertificateConstructionType(permitData.constructionType)} {kankai_data.content_2_7}
												<DashedLangDateField
													name="surjaminDate"
													handleChange={handleChange}
													value={values.surjaminDate}
													error={errors.surjaminDate}
													setFieldValue={setFieldValue}
													inline={true}
													compact={true}
												/>
												{kankai_data.content_2_8}
											</div>
										)}
										<div>
											<PurposeOfConstructionRadio
												space={true}
												fieldLabel={kankai_data.data_1}
												values={values}
												setFieldValue={setFieldValue}
												errors={errors}
											/>
											{isKamalamai ? (
												<div>
													<div>{kankai_data.data_2}</div>
													<div>
														<span className="indent-span">{kankai_data.data_2_1}: </span>
														<BuildingRoadCheckbox
															space={true}
															values={values}
															errors={errors}
															setFieldValue={setFieldValue}
														/>
													</div>
													{/* <div>
													<span className="indent-span">{kankai_data.data_2_2}</span>
													<DashedLangInput
														name="goreto"
														setFieldValue={setFieldValue}
														value={values.goreto}
														error={errors.goreto}
														line="full-line"
													/>
												</div> */}
													<div>
														<span className="indent-span">{kankai_data.kamalamaiData.data_6}</span>
														<DashedUnitInput name="batoChaudai" unitName="batoChaudaiUnit" value={values.batoChaudai} />
													</div>
													<div>
														<span className="indent-span">{kankai_data.kamalamaiData.data_7}</span>
														{kankai_data.data_2_4_radio.map((name, index) => (
															<RadioInput key={index} option={name} name="sadakMapAdhikar" space={true} />
														))}
														{values.sadakMapAdhikar === kankai_data.data_2_4_radio[1] && (
															<DashedLangInput
																name="sadakMapAdhikarData"
																setFieldValue={setFieldValue}
																value={values.sadakMapAdhikarData}
																error={errors.sadakMapAdhikarData}
																line="full-line"
															/>
														)}
													</div>
													<div className=" div-indent">
														<RoadSetbackTable
															title={kankai_data.kamalamaiData.roadSetBackTitle}
															values={values}
															errors={errors}
															setFieldValue={setFieldValue}
															handleChange={handleChange}
														/>
													</div>
												</div>
											) : (
												<div>
													<div>{kankai_data.data_2}</div>
													<div>
														<span className="indent-span">{kankai_data.data_2_1}: </span>
														<BuildingRoadCheckbox
															space={true}
															values={values}
															errors={errors}
															setFieldValue={setFieldValue}
														/>
													</div>
													{isKageyshowri ? (<div>
																<span className="indent-span">{kankai_data.data_2_2_1}</span>
																<DashedLangInput 
																	name="buildingJoinRoad" 
																	setFieldValue={setFieldValue}
																	error={errors.buildingJoinRoad}
																	value={values.buildingJoinRoad ? values.buildingJoinRoad : permitData.buildingJoinRoad} 
																/>
														</div>
													): (<div>
															<div>
															<span className="indent-span">{kankai_data.data_2_2}</span>
															<DashedLangInput
																name="goreto"
																setFieldValue={setFieldValue}
																value={values.goreto}
																error={errors.goreto}
																line="full-line"
															/>
														</div>

													</div>)}
													<div>
														<span className="indent-span">{kankai_data.data_2_3}</span>
														{kankai_data.data_3_radio.map((option) => (
															<RadioInput space={true} key={option} name="data_2_3" option={option} />
														))}
													</div>
													{isKageyshowri ? (<div>
																<span className="indent-span">{kankai_data.data_2_4k}</span>
																<DashedUnitInput name="data_2_4k" unitName="data_2_4kUnit" value={values.data_2_4k} />
																<br />
																<div style={{marginLeft: "3em"}}>

																	<span className="indent-span">{kankai_data.data_2_4_1}</span>
																	<DashedUnitInput name="data_2_4_1" unitName="data_2_4_1Unit" value={values.data_2_4_1} />
																	<br />

																	<span className="indent-span">{kankai_data.data_2_4_2.content1}</span>
																	<DashedUnitInput name="data_2_4_2content1" unitName="data_2_4_2content1Unit" value={values.data_2_4_2content1} />

																	<span className="indent-span">{kankai_data.data_2_4_2.content2}</span>
																	<DashedUnitInput name="data_2_4_2content2" unitName="data_2_4_2content2Unit" value={values.data_2_4_2content2} />
																	<br />
													

																	<span className="indent-span">{kankai_data.data_2_4_3}</span>
																	<DashedUnitInput name="data_2_4_3" unitName="data_2_4_3Unit" value={values.data_2_4_3} />
																	<br />
													
																	<span className="indent-span">{kankai_data.data_2_4_4}</span>
																	<DashedUnitInput name="data_2_4_4" unitName="data_2_4_4Unit" value={values.data_2_4_4} />
													
																</div>
																<br />

																<span className="indent-span">{kankai_data.data_2_5.content1}</span>
																<DashedUnitInput name="data_2_5content1" unitName="data_2_5content1Unit" value={values.data_2_5content1} />

																<span className="indent-span">{kankai_data.data_2_5.content2}</span>
																<DashedUnitInput name="data_2_5content2" unitName="data_2_5content2Unit" value={values.data_2_5content2} />
																<br />

																<span className="indent-span">{kankai_data.data_2_6}</span>

																{kankai_data.data_2_6_radio.map((name, index) => (
																	<RadioInput key={index} option={name} name="data_2_6Radio" space={true} />
																))}
																<br />
												
															</div>
													) 
													
													: (
														<div>
															<span className="indent-span">{kankai_data.data_2_4}</span>
															{kankai_data.data_2_4_radio.map((name, index) => (
																<RadioInput key={index} option={name} name="sadakMapAdhikar" space={true} />
															))}
															{values.sadakMapAdhikar === kankai_data.data_2_4_radio[1] && (
																<DashedLangInput
																	name="sadakMapAdhikarData"
																	setFieldValue={setFieldValue}
																	value={values.sadakMapAdhikarData}
																	error={errors.sadakMapAdhikarData}
																	line="full-line"
																/>
															)}
													</div>
													)}
												</div>
											)}
											<div>
												<div>
													<span>{kankai_data.data_3}</span>
													<DashedLangInput
														name="nirman"
														className="dashedForm-control"
														setFieldValue={setFieldValue}
														value={values.nirman}
														error={errors.nirman}
													/>
													{kankai_data.data_3_1}
													{kankai_data.data_3_radio.map((name, index) => (
														<RadioInput key={index} option={name} name="nirmanOption" space={true} />
													))}
													{values.nirmanOption === kankai_data.data_3_radio[0] && (
														<DashedLangInput
															name="nirmanOptionData"
															className="dashedForm-control"
															setFieldValue={setFieldValue}
															value={values.nirmanOptionData}
															error={errors.nirmanOptionData}
															line="full-line"
														/>
													)}
												</div>
											</div>
											<div>
												<div>{kankai_data.data_4}</div>
												<div>
													<span className="indent-span">{kankai_data.data_4_1}</span>
													<DashedLengthInputWithRelatedUnits
														name="publicPropertyRequiredDistance"
														unitName="publicPropertyUnit"
														relatedFields={['publicPropertyDistance']}
													/>
												</div>
												<div>
													<span className="indent-span">{kankai_data.data_4_2}</span>
													<DashedLengthInputWithRelatedUnits
														name="publicPropertyDistance"
														unitName="publicPropertyUnit"
														relatedFields={['publicPropertyRequiredDistance']}
													/>
												</div>
											</div>
											{!isSundarHaraicha && (
												<div>
													{kankai_data.data_5}
													{kankai_data.data_3_radio.map((option) => (
														<RadioInput space={true} key={option} name="isHighTensionLine" option={option} />
													))}
													<br />
													{values.isHighTensionLine === kankai_data.data_3_radio[0] && (
														<div>
															<span style={{ marginLeft: '20px' }}>{kankai_data.data_5_1}</span>
															<br />
															<span style={{ marginLeft: '40px' }}>
																{/* {kankai_data.data_5_1_1} */}
																<DashedLengthInputWithRelatedUnits
																	name="highTension.0.value"
																	label={kankai_data.data_5_1_1}
																	unitName="highTensionUnit"
																	relatedFields={['highTension.1.value']}
																/>
															</span>
															<br />
															<span style={{ marginLeft: '40px' }}>
																{/* {kankai_data.data_5_1_2} */}
																<DashedLengthInputWithRelatedUnits
																	name="highTension.1.value"
																	label={kankai_data.data_5_1_2}
																	unitName="highTensionUnit"
																	relatedFields={['highTension.0.value']}
																/>
															</span>
														</div>
													)}
												</div>
											)}
											<div>
												{isSundarHaraicha
													? sundarHariacha.data_5
													: isKamalamai
													? kankai_data.kamalamaiData.data_8
													: kankai_data.data_6}
												{kankai_data.data_6_radio.map((name, index) => (
													<RadioInput space={true} option={name} key={index} name="nakshaOption" />
												))}
												{!isKamalamai && (
													<SansodhanCheckbox
														label={pppv_data.sansodhan_checklabel}
														values={values}
														handleChange={handleChange}
														options={pppv_data.sansodhan_check}
														constructionType={permitData.constructionType}
													/>
												)}
												{isKageyshowri && (
													<> 
														<span>{kankai_data.kageyshowriData.data_1}</span>
														<DashedLangInputWithSlash
															name="num1"
															setFieldValue={setFieldValue}
															value={values.num1}
															error={errors.num1}
														/>

														<span>{kankai_data.kageyshowriData.date}</span>
														
														<DashedLangDateField
															inline={true}
															name="date2"
															setFieldValue={setFieldValue}
															value={values.date2}
															error={errors.date2}
														
														/>
														<span>{kankai_data.kageyshowriData.data_2}</span>
														{' '}{user_info.organization.name}{' '}
														
														<span>{kankai_data.kageyshowriData.data_2_1}</span>
														{' '}{permitData.newWardNo}{' '}

														<span>{kankai_data.kageyshowriData.data_3}</span>
														{' '}{permitData.applicantName}{' '}
														
														<span>{kankai_data.kageyshowriData.data_4}</span>
														{' '}{permitData.kittaNo}{' '}

														
														<span>{kankai_data.kageyshowriData.data_5}</span>
														{' '}{permitData.landArea}{' '}

														
														<span>{kankai_data.kageyshowriData.data_6}</span>
														



														<DashedLengthInputWithRelatedUnits
															name="landLength"
															unitName="floorUnit"
														/>{' '}

														<span>{kankai_data.kageyshowriData.data_7}</span>

														<DashedLengthInputWithRelatedUnits
															name="landWidth"
															unitName="floorUnit"
														/>
														<span>{kankai_data.kageyshowriData.data_7_1}</span>
														
														<DashedLengthInputWithRelatedUnits
															name="buildingFloorHeight"
															unitName="floorUnit"
														/>
										
														
														<span>{kankai_data.kageyshowriData.data_8}</span>
														<DashedLangInput 
															setFieldValue={setFieldValue}
															name="data_8"
															value={values.data_8}
															error={errors.data_8}
														/>
														
														<span>{kankai_data.kageyshowriData.data_9}</span>

														<br />														
													</>
												)}
												{values.nakshaOption === kankai_data.data_6_radio[1] && (
													<DashedLangInput
														name="nakshaOptionData"
														className="dashedForm-control"
														setFieldValue={setFieldValue}
														value={values.nakshaOptionData}
														error={errors.nakshaOptionData}
														line="full-line"
													/>
												)}
											</div>
											{isKamalamai ? (
												<>
													{kankai_data.kamalamaiData.data_9}
													{kankai_data.kamalamaiData.data_10_radio.map((name, index) => (
														<RadioInput space={true} option={name} key={index} name="lalpurja" />
													))}
													<SansodhanCheckbox
														label={pppv_data.sansodhan_checklabel}
														values={values}
														handleChange={handleChange}
														options={pppv_data.sansodhan_check}
														constructionType={permitData.constructionType}
													/>
													<div>
														{kankai_data.kamalamaiData.data_9_5}
														<br />
														<span className="indent-span">{kankai_data.kamalamaiData.data_11}</span>
														<EbpsTextareaField
															placeholder="....."
															name="bhiralo"
															setFieldValue={setFieldValue}
															value={values.bhiralo}
															error={errors.bhiralo}
														/>
														<span className="indent-span">{kankai_data.kamalamaiData.data_12}</span>
														<EbpsTextareaField
															placeholder="....."
															name="bhaugarvik"
															setFieldValue={setFieldValue}
															value={values.bhaugarvik}
															error={errors.bhaugarvik}
														/>
													</div>
												</>
											) : (
												<div>
													{isSundarHaraicha ? sundarHariacha.data_6 : kankai_data.data_7}
													<EbpsTextareaField
														placeholder="....."
														name="anya"
														setFieldValue={setFieldValue}
														value={values.anya}
														error={errors.anya}
													/>
												</div>
											)}
										</div>
										<FlexSpaceBetween>
											<div>
												<br />
												<div>{kankaiprabidhik.footer}</div>
												<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors}>
													<DetailedSignature.Name name="subName" />
													<DetailedSignature.Designation name="subDesignation" />
													<DetailedSignature.Signature
														name="subSignature"
														label={kankaiprabidhik.footer_3}
														showSignature={useSignatureImage}
													/>
												</DetailedSignature>
											</div>
											{isKamalamai && (
												<div>
													<div>{kankaiprabidhik.kamalamaiData.data_13}</div>
													<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors}>
														<DetailedSignature.Name name="sifarisName" />
														<DetailedSignature.Designation name="sifarisDesignation" />
														<DetailedSignature.Signature
															name="sifarisSignature"
															label={kankaiprabidhik.footer_3}
															showSignature={useSignatureImage}
														/>
													</DetailedSignature>
												</div>
											)}
										</FlexSpaceBetween>
									</div>
								</div>
								<br />

								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const PrabhidikPratibedhanKageyshowri = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'SurjaminDate',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		onBeforeGetContent={{
			param: [PrintIdentifiers.CHECKBOX_LABEL],
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			// param8: ['15dayspecial']

			// param8: [
			//   'getElementsByClassName',
			//   'ui dropdown',
			//   'innerText',

			// ]
		}}
		render={(props) => <PrabhidikPratibedhanPeshKageyshowriViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default PrabhidikPratibedhanKageyshowri;
