import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
// import merge from 'lodash/merge';
import { DashedLangDateField, DashedDateField } from './../../../shared/DateField';
import { getCurrentDate } from './../../../../utils/dateUtils';
import api from '../../../../utils/api';
import * as Yup from 'yup';
import { showToast, getUserRole, getUserTypeValueNepali } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, surroundingMappingFlat } from '../../../../utils/dataUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { validateNepaliDate, validateNormalNepaliDateWithRange } from '../../../../utils/validationUtils';
import { kankaiNoteView } from '../../../../utils/data/mockLangFile';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { UserType } from '../../../../utils/userTypeUtils';
import { landAreaTypeOptions } from '../../../../utils/optionUtils';
import UnitDropdown from '../../../shared/UnitDropdown';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { WARD_MASTER } from '../../../../utils/constants';
import { LocalAPI } from '../../../../utils/urlUtils';
import { DashedSelect } from '../../../shared/Select';

const kankaiData = kankaiNoteView.kankaiNoteView_data;

const opts = [
	{ key: 1, value: 'मेरो', text: 'मेरो' },
	{ key: 2, value: 'हाम्रो', text: 'हाम्रो' },
];
class NoteOrderPlinthLevelKankaiView extends Component {
	constructor(props) {
		super(props);

		const { permitData, userData, prevData, otherData, localDataObject, DEFAULT_UNIT_LENGTH } = props;

		const surjaminMuchulka = getJsonData(otherData.surjaminMuchulka);

		const wardOption = localDataObject.wardMaster.map(row => ({ key: row.id, value: row.name, text: row.name }));
		// const prabhidikPratibedhan = getJsonData(this.props.otherData.prabhidikPratibedhan);
		// const gharNaksaSurjamin = getJsonData(this.props.otherData.gharNaksaSurjamin);
		// const mapCheckReport = this.props.otherData.mapCheckReport;
		// const maptech = getJsonData(this.props.otherData.maptech);
		// const permitEdit = getJsonData(this.props.otherData.permitEdit)

		let serInfo = {
			subName: '',
			subDesignation: '',
		};

		if (getUserRole() === UserType.SUB_ENGINEER) {
			serInfo.subName = userData.userName;
			serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		}

		const intialValues = prepareMultiInitialValues(
			{
				obj: serInfo,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['newWardNo', 'oldMunicipal', 'kittaNo', 'nibedakName', 'landArea', 'landAreaType', 'surrounding', 'applicantDateBS'],
			},
			{
				obj: surjaminMuchulka,
				reqFields: ['surjaminDate'],
			},
			// {
			// 	obj: prabhidikPratibedhan,
			// 	reqFields: ['namedBorder', 'namedUnit', 'floor.length', 'floor.height', 'floor.width', 'floor.area'],
			// },
			{
				obj: {
					constructionType: permitData.constructionType,
				},
				reqFields: ['constructionType'],
			},
			// {
			// 	obj: {
			// 		newBuild: buildInput,
			// 		newBamojim: bamojimInput,
			// 		saal: permitData && translateEngToNep(permitData.applicantDateBS.substring(0, 4)),
			// 	},
			// 	reqFields: ['newBuild', 'newBamojim', 'saal'],
			// },
			// {
			// 	obj: gharNaksaSurjamin,
			// 	reqFields: ['dakshinSandhiyar', 'uttarSandhiyar', 'paschimSandhiyar', 'purbaSandhiyar'],
			// },
			{
				obj: {
					lambaiUnit: DEFAULT_UNIT_LENGTH,
					chaudaiUnit: DEFAULT_UNIT_LENGTH,
					uchaiUnit: DEFAULT_UNIT_LENGTH,
					namedUnit: DEFAULT_UNIT_LENGTH,
					noteOrderDate: getCurrentDate(true),
					peshGarneDate: getCurrentDate(true),
				},
				reqFields: [],
			},
			// {
			// 	obj: mapCheckReport,
			// 	reqFields: ['details'],
			// },
			// { obj: maptech, reqFields: ['buildingDetailfloor'] },
			{
				obj: getJsonData(prevData),
				reqFields: [],
			}
		);

		intialValues.constructionType = getConstructionTypeValue(intialValues.constructionType);

		this.state = { intialValues, wardOption };
	}
	render() {
		const { intialValues, wardOption } = this.state;

		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const udata = userData;
		const user_info = this.props.userData;
		const NoteOrderPlinthSchema = Yup.object().shape(
			Object.assign({
				noteOrderDate: validateNepaliDate,
				// miiti: validateNepaliDate,
				peshGarneDate: validateNepaliDate,
				surjaminDate: validateNepaliDate,
				applicantDateBS: validateNormalNepaliDateWithRange,
			})
		);
		// console.log("x",getIn(mapCheckReport.details[5].designData))

		// if (isStringEmpty(intialValues.date)) {
		//     intialValues.date = getCurrentDate(true);
		// }

		// const initVal = merge(intialValues, intialValues1);

		// intialValues.constructionType = JSON.parse( prevData.jsonData).constructionType
		return (
			<div>
				<Formik
					initialValues={intialValues}
					validationSchema={NoteOrderPlinthSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.noteorderPilengthLevel}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								// 	getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div>
								<div ref={this.props.setRef}>
									<LetterHeadFlex userInfo={user_info} />

									<div className="section-header">
										<h2 className="underline end-section">{kankaiData.tip}</h2>
									</div>
									<div className="section-header">
										<h3 className="underline end-section">{kankaiData.subtopic}</h3>
									</div>

									<div className="flex-item-single-right">
										<div>
											<span>{kankaiData.miti}</span>
											<DashedLangDateField
												label={udata.enterDate}
												name="noteOrderDate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.noteOrderDate}
												error={errors.noteOrderDate}
											/>
										</div>
									</div>
									<div className="section-header">
										<h3 className="underline end-section">{kankaiData.sub}</h3>
									</div>

									<div className="no-margin-field" style={{ textAlign: 'justify' }}>
										{udata.organization.name}
										{kankaiData.data1}
										<DashedSelect name="newWardNo" options={wardOption} />
										{kankaiData.data2}
										<DashedLangInput
											name="nibedakName"
											setFieldValue={setFieldValue}
											value={values.nibedakName}
											error={errors.nibedakName}
										/>
										{kankaiData.data3}
										<Select name="selectMero" placeholder="मेरो/हाम्रो" options={opts} />
										{kankaiData.data4}
										<DashedLangInput
											name="oldMunicipal"
											setFieldValue={setFieldValue}
											value={values.oldMunicipal}
											error={errors.oldMunicipal}
										/>
										{kankaiData.data5}
										{udata.organization.name}
										{kankaiData.data1}
										{/* <DashedLangInput
											compact={true}
											name="newWardNo"
											setFieldValue={setFieldValue}
											value={values.newWardNo}
											error={errors.newWardNo}
										/> */}
										<DashedSelect name="newWardNo" options={wardOption} />
										{kankaiData.data6}
										<DashedLangInput
											compact={true}
											name="kittaNo"
											setFieldValue={setFieldValue}
											value={values.kittaNo}
											error={errors.kittaNo}
										/>
										{kankaiData.data7}
										<DashedLangInput
											name="landArea"
											value={values.landArea}
											setFieldValue={setFieldValue}
											error={errors.landArea}
										/>
										<UnitDropdown
											setFieldValue={setFieldValue}
											value={values.landAreaType}
											options={landAreaTypeOptions}
											name="landAreaType"
										/>
										{kankaiData.data8}
										<DashedDateField name="applicantDateBS" inline={true} compact={true} />
										{kankaiData.data9}
										<DashedLangInput
											name="shriName"
											setFieldValue={setFieldValue}
											value={values.shriName}
											error={errors.shriName}
										/>
										{kankaiData.data10}
										<DashedLangDateField
											name="Date2"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.Date2}
											error={errors.Date2}
										/>
										{kankaiData.data11}
										<DashedLangDateField
											name="Date3"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.Date3}
											error={errors.Date3}
										/>
										{kankaiData.data12}
										<DashedLangInput name="field" setFieldValue={setFieldValue} value={values.field} error={errors.field} />
										{kankaiData.data13}
										<DashedLangDateField
											name="surjaminDate"
											inline={true}
											compact={true}
											setFieldValue={setFieldValue}
											value={values.surjaminDate}
											error={errors.surjaminDate}
										/>
										{kankaiData.data14}
									</div>
									<br />

									<div className="flex-item-single-right">
										<div>
											<div>{kankaiData.data15}</div>
											<div>
												{kankaiData.data16}
												<span className="ui input dashedForm-control" />
											</div>

											<div>
												{kankaiData.data17}
												<DashedLangInput
													name="subName"
													setFieldValue={setFieldValue}
													value={values.subName}
													error={errors.subName}
												/>
											</div>
											<div>
												{kankaiData.data18}
												<DashedLangInput
													name="subDesignation"
													setFieldValue={setFieldValue}
													value={values.subDesignation}
													error={errors.subDesignation}
												/>
											</div>
											<div>
												{kankaiData.miti}
												<DashedLangDateField
													name="peshGarneDate"
													inline={true}
													setFieldValue={setFieldValue}
													value={values.peshGarneDate}
													error={errors.peshGarneDate}
												/>
											</div>
										</div>
									</div>
									<div className="flex-item-single-left">
										<div>
											<div>{kankaiData.data19}:</div>

											{surroundingMappingFlat.map(surr => {
												let dataSurr = {};
												// let index = 0;
												if (values.surrounding && Array.isArray(values.surrounding) && values.surrounding.length > 0) {
													dataSurr = values.surrounding.find(row => row.side === surr.side) || {};
													// index = values.surrounding.indexOf(dataSurr);
												}
												return (
													<div>
														{surr.value}: {dataSurr && dataSurr.sandhiyar}
													</div>
												);
											})}
											<br />
											<div>
												{kankaiData.data24}

												<DashedLangInput
													name="sahiGarne"
													setFieldValue={setFieldValue}
													value={values.sahiGarne}
													error={errors.sahiGarne}
												/>
											</div>
										</div>

										{/* </div> */}
									</div>
								</div>
								<br />

								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</div>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const NoteOrderKankaiView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteOrderPlinth',
				form: true,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidikPratibedhan',
				form: false,
			},
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: false,
			},
			{
				api: api.surjaminMuchulka,
				objName: 'surjaminMuchulka',
				form: false,
			},
			{
				api: api.mapCheckReport,
				objName: 'mapCheckReport',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'maptech',
				form: false,
			},
		]}
		localData={[new LocalAPI(WARD_MASTER, 'wardMaster')]}
		prepareData={data => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
		}}
		render={props => <NoteOrderPlinthLevelKankaiView {...props} parentProps={parentProps} />}
	/>
);
export default NoteOrderKankaiView;
