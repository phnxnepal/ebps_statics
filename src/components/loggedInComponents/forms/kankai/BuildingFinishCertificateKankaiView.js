import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import { DashedLangInput, DashedNormalInput } from '../../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, getOptionText } from '../../../../utils/dataUtils';
import { mappingDirection } from '../../../../utils/data/BuildingBuildCertificateData';
import EbpsTextareaField from '../../../shared/MyTextArea';
import { DashedMultiUnitLengthInput, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import api from '../../../../utils/api';
import { FinishCertificateBlockFloors } from '../certificateComponents/CertificateFloorInputs';
import { constructionTypeSelectOptions } from '../../../../utils/data/genericData';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { CertificateSubHeading } from '../certificateComponents/CertificateComponents';
import { FooterSignature } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';

const Detail = BuildingFinishCertificateData.details;
const formData = BuildingFinishCertificateData.formdata;
// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];
const field_9 = [
	{ key: 1, value: 'छ (भएको)', text: 'छ (भएको)' },
	{ key: 2, value: 'छैन (नभएको)', text: 'छैन (नभएको)' },
];
// const squareUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];
class BuildingFinishCertificateView extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};
		// const prabhidik = getJsonData(otherData.prabhidik);
		const dosrocharan = getJsonData(otherData.dosrocharan);
		const RajaswoData = getJsonData(otherData.RajaswoData);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		// const organization = otherData.organization;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['isHighTensionLineDistance', 'highTensionLineUnit', 'photo'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingArea', floorArray.getBottomFloor()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: [
					'coverageDetails',
					'constructionType',
					'roof',
					'purposeOfConstruction',
					'buildingHeight',
					'publicPropertyName',
					'publicPropertyUnit',
					'publicPropertyDistance',
				],
			},
			// {
			//     obj: prabhidik,
			//     reqFields: [
			//         'publicProperty',
			//         'publicPropertyUnit',
			//         'publicPropertyName',
			//     ],
			// },
			{
				obj: dosrocharan,
				reqFields: ['roomCount', 'windowCount', 'doorCount', 'shutterCount'],
			},
			{
				obj: RajaswoData,
				reqFields: ['namedMapdanda', 'namedAdhikar', 'requiredDistance', 'sadakAdhikarUnit'],
			},
			// { obj: { buildingArea: floorArray.getSumOfAreas() }, reqFields: [] },
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		initialValues1.constructionType = getOptionText(getConstructionTypeValue(initialValues1.constructionType), constructionTypeSelectOptions);

		const initVal = merge(initialValues, initialValues1);
		// console.log(initVal);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray, formattedFloors } = this.state;
		const user_info = this.props.userData;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								// 	this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								// 	this.props.parentProps.history.push(
								// 		getNextUrl(this.props.parentProps.location.pathname)
								// 	);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<CertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
									/>

									<div>
										{userData.organization.name} {Detail.wda}
										{permitData.newWardNo} {Detail.data1}
										{permitData.applicantName} {Detail.data2}
										{permitData.oldMunicipal}
										{Detail.wda} {permitData.oldWardNo}
										{Detail.data4}
										{userData.organization.name}
										{Detail.wda}
										{permitData.newWardNo}
										{Detail.data5}
										{permitData.sadak}
										{Detail.data6}
										{permitData.buildingJoinRoadType}
										{Detail.data7}
										{permitData.kittaNo}
										{Detail.data8}
										{permitData.landArea} {permitData.landAreaType}
										{Detail.data9} <DashedLangInput name="tapasil" setFieldValue={setFieldValue} value={values.tapasil} />{' '}
										{Detail.data10}
									</div>
									<br />
									<div>
										{formData.fdata1}
										{permitData.nibedakName}
										{/* <DashedLangInput
                                                name='formName'
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                                value={values.formName}
                                                className='dashedForm-control'
                                            /> */}
										{formData.address}
										{userData.organization.name} {permitData.nibedakTol} {permitData.nibedakSadak}
										{/* <DashedLangInput
                                                name='formAddress'
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                                value={values.formAddress}
                                                className='dashedForm-control'
                                            /> */}
									</div>
									<div>
										{formData.fdata2}
										<div style={{ marginLeft: '20px' }}>
											{values.surrounding &&
												values.surrounding.map((index, i) => (
													<div>
														{mappingDirection.find((fl) => fl.direction === index.side).value}{' '}
														<DashedNormalInput
															name={`surrounding.${i}.kittaNo`}
															value={getIn(values, `surrounding.${i}.kittaNo`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.kittaNo`)}
														/>
														{formData.sandhiyar}
														<DashedNormalInput
															name={`surrounding.${i}.sandhiyar`}
															value={getIn(values, `surrounding.${i}.sandhiyar`)}
															handleChange={handleChange}
															error={getIn(errors, `surrounding.${i}.sandhiyar`)}
														/>
													</div>
												))}
										</div>
									</div>
									<div>
										{formData.fdata3}
										<DashedLangInput name="constructionType" setFieldValue={setFieldValue} value={values.constructionType} />
										{formData.fdata4}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
										{formData.fdata5}
										<DashedLangInput name="roofLen" setFieldValue={setFieldValue} value={values.roofLen} />
										{formData.fdata6}
										<DashedLangInput
											name="purposeOfConstruction"
											setFieldValue={setFieldValue}
											value={values.purposeOfConstruction}
										/>
									</div>
									<div>
										{formData.fdata7}
										<DashedLangInput name="roomCount" setFieldValue={setFieldValue} value={values.roomCount} />
										{formData.fdata8}
										<DashedLangInput name="windowCount" setFieldValue={setFieldValue} value={values.windowCount} />
										{formData.fdata9}
										<DashedLangInput name="doorCount" setFieldValue={setFieldValue} value={values.doorCount} />
										{formData.fdata10}
										<DashedLangInput name="shutterCount" setFieldValue={setFieldValue} value={values.shutterCount} />
									</div>
									<div>
										{formData.fdata11}
										<DashedLangInput name="karyaDisc" setFieldValue={setFieldValue} value={values.karyaDisc} />
										{formData.fdata12}
										<DashedLangInput name="haalKaryaDics" setFieldValue={setFieldValue} value={values.haalKaryaDics} />
									</div>
									<DashedMultiUnitLengthInput
										name="namedMapdanda"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedAdhikar']}
										label={formData.fdata13}
									/>

									<DashedMultiUnitLengthInput
										name="namedAdhikar"
										unitName="sadakAdhikarUnit"
										relatedFields={['requiredDistance', 'namedMapdanda']}
										label={formData.fdata14}
									/>
									<DashedMultiUnitLengthInput
										name="requiredDistance"
										unitName="sadakAdhikarUnit"
										relatedFields={['namedAdhikar', 'namedMapdanda']}
										label={formData.fdata15}
									/>
									<div>
										{formData.seven}
										<div className="div-indent">
											<FinishCertificateBlockFloors floorArray={floorArray} formattedFloors={formattedFloors} />
										</div>
									</div>
									<div>
										{formData.fdata24}
										<DashedLangInput name="coverageDetails" setFieldValue={setFieldValue} value={values.coverageDetails} />
									</div>
									<div>
										{formData.fdata25}
										<DashedUnitInput
											name="isHighTensionLineDistance"
											// label={formData.fdata25}
											unitName="highTensionLineUnit"
											inline={true}
										/>

										{formData.volt}
										<DashedLangInput name="elecVolt" setFieldValue={setFieldValue} value={values.elecVolt} />
									</div>
									<div>
										{formData.fdata26}
										<DashedLangInput
											name="publicPropertyName"
											setFieldValue={setFieldValue}
											value={values.publicPropertyName}
											className="publicPropertyName"
										/>
										<DashedUnitInput name="publicPropertyDistance" label={formData.fdata27} unitName="publicPropertyUnit" />
									</div>

									<div>
										{formData.fdata28}
										<Select
											options={field_9}
											name="nikash"
											placeholder="छ (भएको)"
											onChange={(e, { value }) => setFieldValue('nikash', value)}
											value={values['nikash']}
											error={errors.nikash}
											// defaultValue={props.values['nikash']}
										/>
										{/* <DashedLangInput
                                                name='nikash'
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                                value={values.nikash}
                                                className='dashedForm-control'
                                            /> */}
									</div>
									<div>
										{formData.fdata29}
										<EbpsTextareaField
											placeholder="....."
											name="miscDesc"
											setFieldValue={setFieldValue}
											value={values.miscDesc}
											error={errors.miscDesc}
										/>
										{/* <DashedLangInput
                                                name='miscDesc'
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                                value={values.miscDesc}
                                                className='dashedForm-control'
                                            /> */}
									</div>
									<FooterSignature designations={[formData.fdata30, formData.fdata31, formData.sakhaAdhikrit]} />
									<br />
									<div>
										{formData.fdata33}
										<span className="ui input dashedForm-control " />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishView = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.DosrocharanPrabidhikView,
				objName: 'dosrocharan',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'RajaswoData',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
			// param7: ["15dayspecial"]
		}}
		hasFile={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateView {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishView;
