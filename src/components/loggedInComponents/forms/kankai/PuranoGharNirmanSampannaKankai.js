import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { DashedLangDateField } from '../../../shared/DateField';
import * as Yup from 'yup';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, floorMappingFlat, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { mappingLetter, surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { DashedMultiUnitLengthInput, DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { translateEngToNep } from '../../../../utils/langUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { PuranoGharCertificateSubHeading } from '../certificateComponents/PuranoGharComponents';
import { DatePosition, TextSize } from '../../../../utils/constants/formComponentConstants';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { FooterSignature } from '../formComponents/FooterSignature';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { PrintParams } from '../../../../utils/printUtils';
import { getHighTensionData } from '../../../../utils/formUtils';
import { footerSignature } from '../../../../utils/data/genericFormData';
const naaya = BuildingFinishCertificateData.naya;
const Detail = BuildingFinishCertificateData.details;
// const formData = BuildingFinishCertificateData.formdata;
const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];
// const field_9 = [
//     { key: 1, value: 'पालना भएको', text: 'पालना भएको' },
//     { key: 2, value: ' पालना नभएको', text: 'पालना नभएको' }
// ];
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

const convertbib = (inputIndex) => {
	if (inputIndex === 'A') {
		return '(क)';
	} else if (inputIndex === 'B') {
		return '(ख)';
	} else if (inputIndex === 'C') {
		return '(ग)';
	} else {
		return '(घ)';
	}
};
const puranoGharNirmanSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNullableNepaliDate,
		patraDate: validateNullableNepaliDate,
		endDate: validateNullableNepaliDate,
	})
);
class PuranoGharNirmanSampanna extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};
		const prabhidik = getJsonData(otherData.prabhidik);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		// const designApprovalData=getJsonData(otherData.designApprovalData)
		const anuSucKaJsonData = otherData.anukaMaster;
		const desApprovJsonData = otherData.designApprovalData;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		// const organization = otherData.organization;
		// const supStruCons = getJsonData(otherData.supStruCons);
		// const prabhidikPratibedhan = getJsonData(otherData.prabhidikPratibedhan);
		let inputIndex = buildingClass ? buildingClass.id : '';

		initialValues.sadakUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.buildingAreaUnit = DEFAULT_UNIT_AREA;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionUnit = DEFAULT_UNIT_LENGTH;
		initialValues.nirmanDate = translateEngToNep(permitData.applicantDateBS);
		initialValues.palna = naaya.data9.j13_option.option_1;

		const floorArray = new FloorBlockArray(permitData.floor);
		const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		// const floorMax = topFloor.nepaliCount;
		// const highNepaliFloor = topFloor.value;
		// const bottomNepaliFloor = bottomFloor.value;

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['floor', 'photo', 'dhalNikasArrangement', 'surrounding'],
			},
			// {
			//     obj: organization[0],
			//     reqFields: 'photo'
			// },
			{
				obj: mapTech,
				// obj:{allowable:'asa'},
				reqFields: [
					'allowable',
					'coveragePercent',
					'purposeOfConstruction',
					'roof',
					'buildingHeight',
					'sadakAdhikarKshytra0',
					'requiredDistance0',
					'sadakAdhikarUnit',
					'publicPropertyRequiredDistance',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'highTensionUnit',
				],
			},
			{
				obj: {
					...floorArray.getInitialValue('nepaliName', 'startFloor', bottomFloor),
					...floorArray.getInitialValue('nepaliName', 'endFloor', topFloor),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', topFloor),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: {
					buildclass: inputIndex,
					highTension: getHighTensionData(mapTech, prabhidik),
				},
				reqFields: [],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		// initialValues = getJsonData(prevData);

		// const floorData = permitData.floor;

		// const floorCount = permitData.floor ? _.filter(floorData, fl => fl.floor < 11) : [];

		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues1.patraDate)) {
			initialValues1.patraDate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues1.endDate)) {
			initialValues1.endDate = getCurrentDate(true);
		}

		const initVal = merge(initialValues, initialValues1);
		this.state = {
			initVal,
			floorArray,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray } = this.state;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={puranoGharNirmanSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.puranoPramanPatra, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast(
								//     this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								//     this.props.parentProps.history.push(
								//         getNextUrl(this.props.parentProps.location.pathname)
								//     );
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="print-small-font">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<PuranoGharCertificateSubHeading
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={Detail}
										datePosition={DatePosition.BOTTOM}
										textSize={TextSize.MEDIUM}
									/>

									<div>
										<div>
											{naaya.shree}
											{permitData.applicantName}
											{naaya.le}
											{userData.organization.name}
											{naaya.wadano}
											{permitData.newWardNo}
											{naaya.text}
										</div>
										<div>
											{naaya.data1}
											{permitData.applicantName}
										</div>
										<div>
											{naaya.data2}
											{permitData.nibedakName}
										</div>
										<div>
											{naaya.data3}
											{naaya.data4.d1}
											{permitData.oldMunicipal}
											{naaya.data4.d2}
											{permitData.oldWardNo}
											{naaya.data4.d3}
											<DashedLangInput
												name="houseNo"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.houseNo}
												className="dashedForm-control"
											/>
											{naaya.data4.d4}
											{permitData.buildingJoinRoad}
											{naaya.data4.d5}
											{permitData.kittaNo}
											{naaya.data4.d6}
											{permitData.landArea} {permitData.landAreaType}
											{naaya.data4.d7}
											<DashedLangInput
												name="purposeOfConstruction"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.purposeOfConstruction}
												className="dashedForm-control"
											/>
										</div>
										<div>
											<div
												style={{
													display: 'flex',
													// justifyContent: 'space-between'
												}}
											>
												<div>{naaya.data5.e1}</div>
												<div style={{ display: 'flex' }}>
													{values.surrounding &&
														values.surrounding.map((index, i) => (
															<div style={{ marginLeft: '5px' }}>
																{surrounding.find((fl) => fl.direction === index.side).value}
																{/* <DashedUnitInput
                                                                            name={`surrounding.${i}.feet`}
                                                                            unitName={`surrounding.${i}.sideUnit`}
                                                                            value={getIn(values, `surrounding.${i}.feet`)}
                                                                            handleChange={handleChange}
                                                                            error={getIn(
                                                                                errors,
                                                                                `surrounding.${i}.feet`
                                                                            )}
                                                                        /> */}{' '}
																<DashedLangInput
																	name={`surrounding.${i}.sandhiyar`}
																	setFieldValue={setFieldValue}
																	value={getIn(values, `surrounding.${i}.sandhiyar`)}
																	handleChange={handleChange}
																	error={getIn(errors, `surrounding.${i}.sandhiyar`)}
																/>{' '}
															</div>
														))}
												</div>
											</div>
										</div>
										<div>
											{naaya.data6.f1}
											{naaya.data6.input_1.map((input) => (
												// <Table.Cell>
												<div className="ui radio checkbox prabidhik">
													<input
														type="radio"
														name="buildclass"
														defaultChecked={values.buildclass === input}
														value={input}
													/>
													<label>{convertbib(input)}</label>
												</div>
												// </Table.Cell>
											))}
											&emsp;
											{naaya.data7.g1}
											<DashedLangInput
												name="roof"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.roof}
												className="dashedForm-control"
											/>
										</div>

										<div>
											{naaya.data7.g2}
											{': '}
											{translateEngToNep(permitData.applicationNo)} &emsp;
											{naaya.data7.g3}
											<DashedLangDateField
												name="patraDate"
												inline={true}
												setFieldValue={setFieldValue}
												error={errors.patraDate}
												value={values.patraDate}
												className="dashedForm-control"
											/>
										</div>

										<div>
											<table className="certificate-table">
												<tr>
													<th>{naaya.data8.g1}</th>
													<th>{naaya.data8.g2}</th>
													<th>{naaya.data8.g3}</th>
													<th>{naaya.data8.g4}</th>
													<th>{naaya.data8.g5}</th>
												</tr>

												{permitData.floor &&
													permitData.floor.length > 0 &&
													permitData.floor.map((floor, index) => (
														<>
															<tr>
																<td>
																	{mappingLetter.find((fl) => fl.letter === index).value}{' '}
																	{floorMappingFlat.find((fl) => fl.floor === floor.floor).value}{' '}
																</td>
																<td style={{ paddingLeft: '20px' }}>
																	<DashedLengthInputWithRelatedUnits
																		name={`floor.${index}.length`}
																		unitName={`floorUnit`}
																		relatedFields={[
																			...floorArray.getAllFields(`floor.${index}.length`),
																			'buildingArea',
																			'buildingHeight',
																		]}
																	/>
																</td>
																{/* </div> */}
																<td>
																	<DashedLengthInputWithRelatedUnits
																		name={`floor.${index}.width`}
																		unitName={`floorUnit`}
																		relatedFields={[
																			...floorArray.getAllFields(`floor.${index}.width`),
																			'buildingArea',
																			'buildingHeight',
																		]}
																	/>
																</td>
																<td>
																	<DashedLengthInputWithRelatedUnits
																		name={`floor.${index}.height`}
																		unitName={`floorUnit`}
																		relatedFields={[
																			...floorArray.getAllFields(`floor.${index}.height`),
																			'buildingArea',
																			'buildingHeight',
																		]}
																	/>
																</td>
																<td>
																	<DashedAreaInputWithRelatedUnits
																		name={`floor.${index}.area`}
																		unitName={`floorUnit`}
																		// label={formData.area}
																		squareOptions={squareUnitOptions}
																		relatedFields={[
																			...floorArray.getAllFields(`floor.${index}.area`),
																			'buildingArea',
																			'buildingHeight',
																		]}
																	/>
																</td>
															</tr>

															{/* {` ${getUnitValue(floor.floorUnit)}`} */}
														</>
													))}
											</table>
											<div>
												<DashedLangInput
													name="startFloor"
													setFieldValue={setFieldValue}
													inline={true}
													value={values.startFloor}
													className="dashedForm-control"
												/>
												{naaya.data8.g6}
												<DashedLangInput
													name="endFloor"
													setFieldValue={setFieldValue}
													inline={true}
													value={values.endFloor}
													className="dashedForm-control"
												/>
											</div>
											<span style={{ fontWeight: 'bold' }}>{naaya.data9.j1}</span>

											<DashedAreaInputWithRelatedUnits
												name="buildingArea"
												unitName="floorUnit"
												squareOptions={squareUnitOptions}
												relatedFields={[...floorArray.getAllFields(), 'buildingHeight']}
											/>
										</div>

										<div>
											{naaya.data9.j2}
											<DashedLangInput
												name="coveragePercent"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.coveragePercent}
												className="dashedForm-control"
											/>
											{naaya.data9.j3}
											<DashedLangInput
												name="allowable"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.allowable}
												className="dashedForm-control"
											/>
										</div>
										<div>
											{naaya.data9.j4}
											<DashedLengthInputWithRelatedUnits
												name="buildingHeight"
												unitName="floorUnit"
												relatedFields={[...floorArray.getAllFields(), 'buildingArea']}
											/>
											{naaya.data9.j5}{' '}
											<DashedLangInput
												name="floorNumber"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.floorNumber}
												className="dashedForm-control"
											/>
										</div>

										<div>
											<DashedMultiUnitLengthInput
												name="requiredDistance0"
												unitName="sadakAdhikarUnit"
												relatedFields={['sadakAdhikarKshytra0']}
												squareOptions={distanceOptions}
												label={naaya.data9.j6}
											/>
											<DashedMultiUnitLengthInput
												name="sadakAdhikarKshytra0"
												unitName="sadakAdhikarUnit"
												relatedFields={['requiredDistance0']}
												areaField={'requiredDistance0'}
												label={naaya.data9.j7}
											/>
										</div>

										<div>
											<DashedMultiUnitLengthInput
												name="highTension.0.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.1.value']}
												squareOptions={distanceOptions}
												label={naaya.data9.j8}
											/>
											<DashedMultiUnitLengthInput
												name="highTension.1.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.0.value']}
												areaField={'highTension.0.value'}
												label={naaya.data9.j7}
											/>
										</div>
										<div className="div-ko-para">
											<DashedMultiUnitLengthInput
												name="publicPropertyRequiredDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyDistance']}
												squareOptions={distanceOptions}
												label={naaya.data9.j9}
											/>
											<DashedMultiUnitLengthInput
												name="publicPropertyDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyRequiredDistance']}
												areaField={'publicPropertyRequiredDistance'}
												label={naaya.data9.j7}
											/>
										</div>
										<div>
											{naaya.data9.j10}
											{/* <span
                                                        className='ui input dashedForm-control'
                                                    >{permitData.dhalNikasArrangement}</span> */}
											<DashedLangInput
												name="dhalNikasArrangement"
												setFieldValue={setFieldValue}
												inline={true}
												value={values.dhalNikasArrangement}
												className="dashedForm-control"
											/>
										</div>
										<div className="no-margin-field">
											{naaya.data9.j11}
											<DashedLangDateField
												name="nirmanDate"
												inline={true}
												setFieldValue={setFieldValue}
												error={errors.nirmanDate}
												value={values.nirmanDate}
												className="dashedForm-control"
											/>
											{naaya.data9.j12}
											<DashedLangDateField
												name="endDate"
												inline={true}
												setFieldValue={setFieldValue}
												value={values.endDate}
												error={errors.endDate}
												className="dashedForm-control"
											/>
										</div>

										<div className="no-margin-field">
											{naaya.data9.j13}
											{Object.values(naaya.data9.j13_option).map((option) => (
												<div className="ui radio checkbox prabidhik" key={option}>
													<input
														type="radio"
														name="palna"
														value={option}
														defaultChecked={values.palna === option}
														onChange={handleChange}
													/>
													<label>{option}</label>
												</div>
											))}

											{values.palna === naaya.data9.j13_option.option_2 && (
												<div>
													{naaya.data9.j14}
													<DashedLangInput
														// inline={true}
														name="palanaBibaran"
														placeholder="Additional Information..."
														setFieldValue={setFieldValue}
														value={values.palanaBibaran}
														error={errors.palanaBibaran}
														// label={naaya.data9.j14}
													/>
												</div>
											)}
										</div>
										<br />
										<FooterSignature designations={[naaya.signs.s1, footerSignature.sthalgatNirikshyan, naaya.signs.s3, naaya.signs.sakhaAdhikrit]} />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* </div> */}
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const PuranoGharSampannaPramanPatra = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.puranoPramanPatra, objName: 'puranoPramanPatra', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},

			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			//param1: ["getElementsByTagName", "input", "value"],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <PuranoGharNirmanSampanna {...props} parentProps={parentProps} />}
	/>
);
export default PuranoGharSampannaPramanPatra;
