import React, { Component } from 'react';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import { Button, Form, Input, Divider, Label, List, Dropdown, Grid, Icon } from 'semantic-ui-react';

import { postFile, getUploadedFilesMaster, getUploadedFiles } from '../../../store/actions/fileActions';
import { getPermitAndUserData } from '../../../store/actions/formActions';
import api from '../../../utils/api';
import { getBackUrl, getDocUrl } from '../../../utils/config';
import FallbackComponent from '../../shared/FallbackComponent';
import { hasMultipleFiles, showToast, getSplitUrl, getUserInfoObj } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import IconPdf from '../../../assets/images/pdf.svg';

import { validateFile } from '../../../utils/validationUtils';
import { deleteFile } from './../../../store/actions/fileActions';
import { ImageDisplayPreview } from '../../shared/file/FileView';
import { SectionHeader } from '../../uiComponents/Headers';
import { CompactInfoMessage } from '../../shared/formComponents/CompactInfoMessage';
import { fileData } from '../../../utils/data/genericData';
import { SearchableDropdown } from '../../shared/formComponents/SearchableDropdown';

class FileUpload extends Component {
	constructor(props) {
		super(props);
		this.state = {
			id: '',
			formOptions: [],
			selectedForm: '',
			fileCategories: [],
			//isDeleteModalOpen: false
		};
	}

	getFormWiseCategory = (form) => {
		return this.props.fileCategories.filter((cat) => String(cat.formId) === String(form));
	};

	handleFormSelection = (form) => {
		this.setState({ selectedForm: form, fileCategories: this.getFormWiseCategory(form) });
	};

	componentDidMount() {
		this.props.getPermitAndUserData();

		if (this.props.files) {
			this.props.getUploadedFilesMaster(true);
		} else {
			this.props.getUploadedFilesMaster();
		}
	}

	componentDidUpdate(prevProps) {
		if (prevProps.success !== this.props.success && this.props.success) {
			this.props.getUploadedFiles();
		}

		if (prevProps.formMaster !== this.props.formMaster) {
			const currentUser = getUserInfoObj().userType;
			const requiredForms = this.props.fileCategories ? this.props.fileCategories.map((cat) => String(cat.formId)) : [];
			const formMaster = this.props.formMaster.filter((form) => form.enterBy === currentUser && requiredForms.includes(String(form.id)));
			const formOptions =
				formMaster && formMaster.length > 0
					? formMaster.map((row) => ({
							value: row.id,
							text: row.name,
					  }))
					: [];
			const selectedForm = formOptions && formOptions.length > 0 ? formOptions[0].value : '';
			const fileCategories = this.getFormWiseCategory(selectedForm);
			this.setState({ formOptions, selectedForm, fileCategories });
		}
	}
	/**
	 * @TODO Delete currently removed as there is not prev data here to check form status
	 */

	// handleDeleteModalOpen = id => {
	//   this.setState({
	//     isDeleteModalOpen: true,
	//     id: id
	//   });
	// };

	// handleDeleteModalClose = () => {
	//   this.setState({
	//     isDeleteModalOpen: false
	//   });
	// };

	// onChangeHandler = event => {
	//     const id = event.target.id;

	//     this.setState({
	//         [`selectedFile_${id}`]: event.target.files,
	//         loaded: 0
	//     });
	// };

	// onUpload = event => {
	//   console.log('Message', event);
	//   const data = new FormData();
	//   for (var x = 0; x < this.state.selectedFile.length; x++) {
	//     data.append('file', this.state.selectedFile[x]);
	//   }
	//   data.append('fileType', 3);
	//   data.append('applicationNo', this.props.permitData.applicantNo);

	//   console.log('data', data, this.props, event);

	//   // this.props.postFile(
	//   //   `${getBackUrl()}${api.fileUpload}${this.props.permitData.applicantNo}`,
	//   //   data
	//   // );
	// };

	render() {
		const { formOptions, selectedForm, fileCategories } = this.state;
		if (this.props.permitData && this.props.userData && this.props.fileCategories) {
			const { files } = this.props;
			// const files = this.props.files;

			const schema = Yup.object().shape({
				uploadFile: validateFile,
			});

			// let currentUrls = fileCategories.find(
			//   category => category.formName.viewUrl ==
			// );
			// let currentUrl = currentUrls.formName.viewUrl;
			// console.log(currentUrl);

			// let currentUser = getUserTypeValue(userData.userType);
			// let isDisabled = getDisableDeleteStatus(currentUrl, currentUser);

			// console.log("this.props la ", isDisabled);
			return (
				<div>
					{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
					<SectionHeader>
						<h2 className="bottom-margin">{fileData.fileUpload}</h2>
					</SectionHeader>

					{formOptions.length < 1 ? (
						<CompactInfoMessage content={fileData.noFormWithFile} />
					) : (
						<SearchableDropdown
							label={fileData.selectForm}
							options={formOptions}
							name="selectedForm"
							value={selectedForm}
							handleChange={this.handleFormSelection}
							bold
						/>
					)}
					<br />
					{fileCategories.length < 1 ? (
						<CompactInfoMessage content={fileData.noFileCategoryForm} />
					) : (
						<Grid columns={2} divided doubling padded>
							{fileCategories.map((category) => (
								<Grid.Column key={category.id}>
									<Formik
										key={category.id}
										validationSchema={schema}
										onSubmit={async (values, actions) => {
											actions.setSubmitting(true);
											// console.log('values', values);
											const data = new FormData();

											// const selectedFile = this.state[
											//     `selectedFile_${category.id}`
											// ];

											const selectedFile = values.uploadFile;
											if (selectedFile) {
												selectedFile.forEach((file) => data.append('file', file));

												data.append('fileType', category.id);
												data.append('applicationNo', this.props.permitData.applicantNo);

												try {
													await this.props.postFile(
														`${getBackUrl()}${api.fileUpload}${this.props.permitData.applicantNo}`,
														data
													);

													if (this.props.success && this.props.success.success) {
														showToast(this.props.success.data.message || 'File uploaded');
														this.setState(() => ({
															needToGet: true,
														}));
														// actions.resetForm(
														//   values.uploadFile,
														//   values.imagePreviewUrl
														// );

														actions.resetForm({
															imagePreviewUrl: [],
															uploadFile: '',
														});
														document.getElementById(`file_${category.id}`).value = '';
													}
													actions.setSubmitting(false);
												} catch (err) {
													actions.setSubmitting(false);
													console.log('File upload failed', err);
												}
											} else {
												actions.setError('Please select a file before uploading.');
											}
											// console.log("data to send", data, selectedFile);
										}}
									>
										{({ handleSubmit, errors, setFieldValue, values, isSubmitting }) => {
											// console.log('errors', errors, values);
											return (
												<Form loading={isSubmitting}>
													<Divider horizontal>{category.name}</Divider>
													<Form.Field error={errors.uploadFile}>
														<Input
															type="file"
															name="uploadFile"
															id={`file_${category.id}`}
															multiple={hasMultipleFiles(category.isMultiple)}
															onChange={(e) => {
																setFieldValue('uploadFile', Array.from(e.target.files));
																const arrFiles = Array.from(e.target.files);
																const multipleFiles = arrFiles.map((file, index) => {
																	const src = window.URL.createObjectURL(file);
																	return { file, id: index, src };
																});

																setFieldValue('imagePreviewUrl', multipleFiles);
															}}
														/>
														{errors.uploadFile && (
															<Label pointing prompt size="large">
																{errors.uploadFile}
															</Label>
														)}
														{values.imagePreviewUrl && (
															<div className="previewFileUpload">
																{values.imagePreviewUrl.map((value, index) => (
																	<div key={index} className={'eachPreviewFile'}>
																		<ImageDisplayPreview
																			isInput={true}
																			src={value.src}
																			alt={value.file.name}
																			title={value.file.name}
																			type={value.file.type}
																		/>
																	</div>
																))}
															</div>
														)}
													</Form.Field>

													<List relaxed>
														<List.Header>Existing Files</List.Header>
														<div className="previewFileUpload">
															{files && files.length > 0 && files.some((file) => file.fileType.id === category.id) ? (
																files
																	.filter((file) => file.fileType.id === category.id)
																	.map((el) => {
																		// const pdfValidate = el.fileUrl.split('.')[el.fileUrl.split('.').length - 1];
																		const pdfValidate = getSplitUrl(el.fileUrl, '.');
																		const fileTitleName = getSplitUrl(el.fileUrl, '/');

																		return (
																			<div
																				key={el.fileUrl}
																				className={
																					pdfValidate === 'pdf'
																						? 'eachPreviewFile pdfFilePreview'
																						: 'eachPreviewFile'
																				}
																				title={fileTitleName}
																			>
																				{pdfValidate === 'pdf' ? (
																					<>
																						<img src={IconPdf} alt="pdfIconFile" />
																						{/* <Icon
																					name="close"
																					className="closePdf"
																					onClick={() => this.handleDeleteModalOpen(el.id)}
																				/> */}
																					</>
																				) : (
																					<>
																						<img src={`${getDocUrl()}${el.fileUrl}`} alt="imageFile" />
																						{/* {category.formName.enterBy ==
                                          userData.userType && (
                                          <Icon
                                            name="close"
                                            className="closeImg"
                                            onClick={() =>
                                              this.handleDeleteModalOpen(el.id)
                                            }
                                          />
                                        )} */}
																					</>
																				)}
																			</div>
																		);
																	})
															) : (
																<List.Item>No files uploaded.</List.Item>
															)}
														</div>
													</List>
													<Button className="primary-btn" type="submit" id={category.id} onClick={handleSubmit}>
														<Icon name="upload" />
														Upload
													</Button>
												</Form>
											);
										}}
									</Formik>
								</Grid.Column>
							))}
							{/* <DeleteModal
            open={this.state.isDeleteModalOpen}
            onClose={this.handleDeleteModalClose}
            history={this.props.parentHistory}
            loadingModal={this.props.loadingModal}
            id={this.state.id}
            deleteFile={() => this.props.deleteFile(this.state.id)}
          /> */}
						</Grid>
					)}
				</div>
			);
		} else {
			return <FallbackComponent errors={this.props.errors} loading={this.props.loading} />;
		}
	}
}

const mapDispatchToProps = {
	postFile,
	getPermitAndUserData,
	getUploadedFilesMaster,
	getUploadedFiles,
	deleteFile,
};

const mapStateToProps = (state) => ({
	permitData: state.root.formData.permitData,
	userData: state.root.formData.userData,
	files: state.root.formData.files,
	fileCategories: state.root.formData.fileCategories,
	formMaster: state.root.formData.formMaster,
	success: state.root.formData.success,
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
});

export default connect(mapStateToProps, mapDispatchToProps)(FileUpload);
