import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { Form, Table, } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../../utils/dataUtils';
import { isEmpty, showToast, getUserRole } from '../../../../utils/functionUtils';
import api from '../../../../utils/api';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedLangDateField } from './../../../shared/DateField';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';

import { naksaFile } from '../../../../utils/data/naksaFilePathayeko';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { RadioInput } from './../../../shared/formComponents/RadioInput';
import { FlexSingleLeft } from './../../../uiComponents/FlexDivs';
import EbpsForm from './../../../shared/EbpsForm';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserTypeValueNepali } from './../../../../utils/functionUtils';
const NaksaFileSchema = Yup.object().shape(
    Object.assign({
        date: validateNullableNepaliDate,
    })
);
const naksaFileData = naksaFile.data.naksaFileInaruwaData;
class NaksaFileInaruwaComponent extends Component {
    constructor(props) {
        super(props);
        let initVal = {};
        const { prevData, userData } = this.props;
        const json_data = getJsonData(prevData);
        let serInfo = {
            subName: '',
            subDesignation: '',
        };
        if (getUserRole() === UserType.SUB_ENGINEER) {
            serInfo.subName = userData.userName;
            serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
        }
        initVal = prepareMultiInitialValues(
            {
                obj: serInfo,
                reqFields: [],
            },
            { obj: json_data, reqFields: [] }
        );

        if (isStringEmpty(initVal.date)) {
            initVal.date = getCurrentDate(true);
        }
        this.state = {
            initVal,
        };
    }
    render() {
        const { initVal } = this.state;
        const { permitData, errors: reduxErrors, userData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
        const salutationData = [naksaFileData.salutation_1, naksaFileData.content_1, userData.organization.name];
        return (
            <>
                {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
                {!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
                <Formik
                    initialValues={initVal}
                    validationSchema={NaksaFileSchema}
                    onSubmit={async (values, actions) => {
                        actions.setSubmitting(true);
                        values.applicationNo = permitData.applicationNo;
                        values.error && delete values.error;
                        try {
                            await this.props.postAction(`${api.NaksaFilePathayekoBare}${permitData.applicationNo}`, values);
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            if (this.props.success && this.props.success.success) {
                                handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                            }
                        } catch (err) {
                            actions.setSubmitting(false);
                            window.scrollTo(0, 0);
                            console.log('Error', err);
                            showToast('Something went wrong !!');
                        }
                    }}
                >
                    {({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
                        <Form loading={isSubmitting} className="ui form">
                            <div ref={this.props.setRef}>
                                <div>
                                    <SectionHeader>
                                        <h2>{userData.organization.name}</h2>
                                        <h3>{naksaFileData.title}</h3>
                                    </SectionHeader>
                                    <LetterSalutation lines={salutationData} />
                                    <SectionHeader>
                                        <h3 className="normal underline end-section">{naksaFileData.content_2}</h3>
                                    </SectionHeader>
                                    <div style={{ textAlign: 'justify ' }}>
                                        <p>
                                            {naksaFileData.content_3}<br />
                                            <span className='indent-span'> {naksaFileData.content_4}</span>{permitData.nibedakName}
                                            {userData.organization.name}
                                            {naksaFileData.content_5}{permitData.nibedakTol}
                                            {naksaFileData.content_6}{permitData.kittaNo}
                                            {naksaFileData.content_7}{permitData.landArea}{permitData.landAreaType}
                                            {naksaFileData.content_8}
                                        </p>
                                        <br />
                                    </div>
                                    <div>
                                        <Table className="certificate-ui-table">
                                            <Table.Header>
                                                <Table.Row>
                                                    {naksaFileData.tableHeader.map((headerName, index) => (
                                                        <Table.HeaderCell key={index}>
                                                            {headerName}
                                                        </Table.HeaderCell>
                                                    ))}
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_1} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_1}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_1}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='jaggaDhani' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='jaggaDhaniKaifiyat'
                                                            value={values.jaggaDhaniKaifiyat}
                                                            error={errors.jaggaDhaniKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_2} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_2}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_1}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='chalu' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='chaluKaifiyat'
                                                            value={values.chaluKaifiyat}
                                                            error={errors.chaluKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_3} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_3}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_2}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='naapiNaksa' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='naapiNaksaKaifiyat'
                                                            value={values.naapiNaksaKaifiyat}
                                                            error={errors.naapiNaksaKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_4} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_4}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_1}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='registration' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='registrationKaifiyat'
                                                            value={values.registrationKaifiyat}
                                                            error={errors.registrationKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_5} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_5}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_3}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='bhawanNaksa' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='bhawanNaksaKaifiyat'
                                                            value={values.bhawanNaksaKaifiyat}
                                                            error={errors.bhawanNaksaKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_6} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_6}</Table.Cell>
                                                    <Table.Cell>{naksaFileData.quantity_3}</Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='structure' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='structureKaifiyat'
                                                            value={values.structureKaifiyat}
                                                            error={errors.structureKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_7} </Table.Cell>
                                                    <Table.Cell>
                                                        <DashedLangInput
                                                            name="sartaNama"
                                                            setFieldValue={setFieldValue}
                                                            value={values.sartaNama}
                                                            className="dashedForm-control"
                                                        />{naksaFileData.cellData_7}
                                                    </Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='sambandhit' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='sambandhitKaifiyat'
                                                            value={values.sambandhitKaifiyat}
                                                            error={errors.sambandhitKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_8} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_8}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='bhogChalan' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='bhogChalanKaifiyat'
                                                            value={values.bhogChalanKaifiyat}
                                                            error={errors.bhogChalanKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_9} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_9}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='warisnama' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='warisnamaKaifiyat'
                                                            value={values.warisnamaKaifiyat}
                                                            error={errors.warisnamaKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_10} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_10}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='myad' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='myadKaifiyat'
                                                            value={values.myadKaifiyat}
                                                            error={errors.myadKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_11} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_11}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='surjamin' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='surjaminKaifiyat'
                                                            value={values.surjaminKaifiyat}
                                                            error={errors.surjaminKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_12} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_12}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='pratibedan' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='pratibedanKaifiyat'
                                                            value={values.pratibedanKaifiyat}
                                                            error={errors.pratibedanKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_13} </Table.Cell>
                                                    <Table.Cell>{naksaFileData.cellData_13}</Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {naksaFileData.cellData_14}
                                                        {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='pratilipi' space={true} />
                                                        ))}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='pratilipiKaifiyat'
                                                            value={values.pratilipiKaifiyat}
                                                            error={errors.pratilipiKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_14} </Table.Cell>
                                                    <Table.Cell>
                                                        <DashedLangInput
                                                            name="extraOne"
                                                            setFieldValue={setFieldValue}
                                                            value={values.extraOne}
                                                            className="dashedForm-control"
                                                        />
                                                    </Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {/* {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='extraOneRadio' space={true} />
                                                        ))} */}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='extraOneKaifiyat'
                                                            value={values.extraOneKaifiyat}
                                                            error={errors.extraOneKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>{naksaFileData.tableId_15} </Table.Cell>
                                                    <Table.Cell>
                                                        <DashedLangInput
                                                            name="extraTwo"
                                                            setFieldValue={setFieldValue}
                                                            value={values.extraTwo}
                                                            className="dashedForm-control"
                                                        />
                                                    </Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                    <Table.Cell>
                                                        {/* {naksaFileData.radioOption.map((name, id) => (
                                                            <RadioInput key={id} option={name} name='extraTwoRadio' space={true} />
                                                        ))} */}
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <EbpsForm
                                                            setFieldValue={setFieldValue}
                                                            name='extraTwoKaifiyat'
                                                            value={values.extraTwoKaifiyat}
                                                            error={errors.extraTwoKaifiyat} />
                                                    </Table.Cell>
                                                </Table.Row>
                                            </Table.Body>
                                        </Table>
                                    </div>
                                    <br />
                                    <FlexSingleLeft>
                                        <div className="no-margin-field">
                                            {naksaFileData.footer_1}<br />
                                            {naksaFileData.footer_2}<span className="ui input signature-placeholder" /><br />
                                            {naksaFileData.footer_3}
                                            <DashedLangInput
                                                name="subName"
                                                setFieldValue={setFieldValue}
                                                value={values.subName}
                                                className="dashedForm-control"
                                            /><br />
                                            {naksaFileData.footer_4}
                                            <DashedLangInput
                                                name="subDesignation"
                                                setFieldValue={setFieldValue}
                                                value={values.subDesignation}
                                                className="dashedForm-control"
                                            /><br />
                                            {naksaFileData.footer_5}
                                            <DashedLangDateField
                                                compact={true}
                                                name="date"
                                                value={values.date}
                                                error={errors.date}
                                                setFieldValue={setFieldValue}
                                                inline={true}
                                            />
                                        </div>
                                    </FlexSingleLeft>
                                </div>
                            </div>
                            <br />
                            <SaveButtonValidation
                                errors={errors}
                                validateForm={validateForm}
                                formUrl={formUrl}
                                hasSavePermission={hasSavePermission}
                                hasDeletePermission={hasDeletePermission}
                                isSaveDisabled={isSaveDisabled}
                                prevData={checkError(prevData)}
                                handleSubmit={handleSubmit}
                            />
                        </Form>
                    )}
                </Formik>
            </>
        );
    }
}

const NaksaFilePathayekoInaruwa = parentProps => (
    <FormContainerV2
        api={[
            {
                api: api.NaksaFilePathayekoBare,
                objName: 'naksaFilePathayekoBare',
                form: true,
            },
        ]}
        prepareData={data => data}
        onBeforeGetContent={{
            param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
            // param2: ['getElementsByTagName', 'input', 'value'],
            // param2: ['getElementsByTagName', 'span', 'innerText'],
            param2: ['getElementsByClassName', 'ui label', 'innerText'],
            param3: ['15dayspecial'],
            param4: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
        }}
        useInnerRef={true}
        parentProps={parentProps}
        render={props => <NaksaFileInaruwaComponent {...props} parentProps={parentProps} />}
    />
);

export default NaksaFilePathayekoInaruwa;
