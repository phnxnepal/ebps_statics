import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik, Form } from 'formik';
import { Table, TableCell } from 'semantic-ui-react';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangDateField, CompactDashedLangDate } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { SansodhanBibaranInaruwaData } from '../../../../utils/data/SansodhanBibaranPahiloInaruwa';
import { checkError, handleSuccess, prepareMultiInitialValues, getJsonData } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import EbpsForm from '../../../shared/EbpsForm';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import * as Yup from 'yup';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { PrintParams } from './../../../../utils/printUtils';

const inaruwaData = SansodhanBibaranInaruwaData.Inaruwa_data;
const floorMain = [0, 1, 2, 3, 4, 5, 6];
const sansodhanSchema = Yup.object().shape(
	Object.assign({
		sansodhanBibaranDate: validateNullableNepaliDate,
		karyaMiti: validateNullableNepaliDate,
		supStrucDate: validateNullableNepaliDate,
	})
);

class SansodhanBibaranPahiloInaruwaComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const initialValues = prepareMultiInitialValues(
			{
				obj: getJsonData(prevData),
				reqFields: [],
			}
		);
		if (isStringEmpty(initialValues.sansodhanBibaranDate)) {
			initialValues.sansodhanBibaranDate = getCurrentDate(true);
		}
		this.state = {
			initialValues,
			floorArray,
		};
	}

	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<>
				<div className="pilenthLevelTechViewWrap">
					{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
					<Formik
						initialValues={this.state.initialValues}
						validationSchema={sansodhanSchema}
						onSubmit={async (values, actions) => {
							actions.setSubmitting(true);

							try {
								await this.props.postAction(api.sansodhanBibaranPahilo, values, true);
								window.scroll(0, 0);
								if (this.props.success && this.props.success.success) {
									handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
								}
								actions.setSubmitting(false);
							} catch (err) {
								actions.setSubmitting(false);
								console.log('Error', err);
							}
						}}
					>
						{({ isSubmitting, values, handleChange, handleSubmit, errors, setFieldValue, validateForm }) => (
							<Form loading={isSubmitting} className="ui form">
								<div ref={this.props.setRef}>
									<SectionHeader>
										<h3 className="underline">
											<span>({inaruwaData.title})</span>
										</h3>
									</SectionHeader>
									<br />
									<div style={{ display: 'flex', justifyContent: 'space-between' }}>
										<div>
											{inaruwaData.content.content_1} <br />
											{userData.organization.name}
											<br />
											{userData.organization.officeName}
											<br />
											{userData.organization.address}
											<br />
										</div>
										<DashedLangDateField
											name="sansodhanBibaranDate"
											handleChange={handleChange}
											value={values.sansodhanBibaranDate}
											setFieldValue={setFieldValue}
											label={'मिति :'}
											inline={true}
										/>
									</div>
									<br />
									<div className="section-header margin-section">
										<h3 className="underline">{inaruwaData.subject}।</h3>
									</div>
									<br />
									<div>
										<p>{inaruwaData.content.content_2}</p>
										<div className="no-margin-field margin-section">
											{inaruwaData.content.content_3}
											{permitData.oldMunicipal}
											{inaruwaData.content.content_4} {userData.organization.name} {inaruwaData.content.content_5}
											{permitData.newWardNo} {inaruwaData.content.content_7}
											{permitData.landArea} {inaruwaData.content.content_6}{' '}
											<CompactDashedLangDate
												name="karyaMiti"
												setFieldValue={setFieldValue}
												error={errors.karyaMiti}
												value={values.karyaMiti}
											/>{' '}
											{inaruwaData.content.content_8}
										</div>
										<br />
										<div className="section-header margin-section">
											<h3>{inaruwaData.content.content_9}</h3>
										</div>
										<div>{inaruwaData.content.content_10}</div>
										<Table celled>
											<Table.Header>
												<Table.Row textAlign="center">
													<Table.HeaderCell />
													<Table.HeaderCell>{inaruwaData.table.content_1}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_2}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_3}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_4}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_5}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_6}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_8}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`lambai_${m}`}
																setFieldValue={setFieldValue}
																value={values[`lambai_${m}`]}
																error={errors[`lambai_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_9}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`chaudai_${m}`}
																setFieldValue={setFieldValue}
																value={values[`chaudai_${m}`]}
																error={errors[`chaudai_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_10}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`feet_${m}`}
																setFieldValue={setFieldValue}
																value={values[`feet_${m}`]}
																error={errors[`feet_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_11}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`uchai_${m}`}
																setFieldValue={setFieldValue}
																value={values[`uchai_${m}`]}
																error={errors[`uchai_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
											</Table.Body>
										</Table>
										<div>{inaruwaData.content.content_11}</div>
										<Table celled>
											<Table.Header>
												<Table.Row textAlign="center">
													<Table.HeaderCell />
													<Table.HeaderCell>{inaruwaData.table.content_1}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_2}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_3}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_4}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_5}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_6}</Table.HeaderCell>
													<Table.HeaderCell>{inaruwaData.table.content_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_8}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`length_${m}`}
																setFieldValue={setFieldValue}
																value={values[`length_${m}`]}
																error={errors[`length_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_9}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`breadth_${m}`}
																setFieldValue={setFieldValue}
																value={values[`breadth_${m}`]}
																error={errors[`breadth_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_10}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`bargafeet_${m}`}
																setFieldValue={setFieldValue}
																value={values[`bargafeet_${m}`]}
																error={errors[`bargafeet_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row textAlign="center">
													<TableCell>{inaruwaData.table.content_11}</TableCell>
													{floorMain.map(m => (
														<TableCell>
															<EbpsForm
																name={`height_${m}`}
																setFieldValue={setFieldValue}
																value={values[`height_${m}`]}
																error={errors[`height_${m}`]}
															/>
														</TableCell>
													))}
												</Table.Row>
												<Table.Row>
													<TableCell colspan={8}>{inaruwaData.table.content_12}</TableCell>
												</Table.Row>
											</Table.Body>
										</Table>
										<div style={{ display: 'flex', justifyContent: 'space-between' }}>
											<div>
												{inaruwaData.content.content_12}{' '}
												<DashedLangInput
													name="dasturRu"
													value={values.dasturRu}
													setFieldValue={setFieldValue}
													error={errors.dasturRu}
												/>
												<br />
												{inaruwaData.content.content_13}{' '}
												<DashedLangInput
													name="samsodhitRu"
													value={values.samsodhitRu}
													setFieldValue={setFieldValue}
													error={errors.samsodhitRu}
												/>
												<br />
												{inaruwaData.content.content_14}{' '}
												<DashedLangInput
													name="dharautiRu"
													value={values.dharautiRu}
													setFieldValue={setFieldValue}
													error={errors.dharautiRu}
												/>
											</div>
											<div>
												{inaruwaData.content.content_15}{' '}
												{permitData.nibedakName}
												<br />
												{inaruwaData.content.content_16}{' '}
												<span className="ui input dashedForm-control" />
												<br />
												{inaruwaData.content.content_17}{' '}
												<CompactDashedLangDate
													name="supStrucDate"
													setFieldValue={setFieldValue}
													error={errors.supStrucDate}
													value={values.supStrucDate}
												/>
											</div>
										</div>

									</div>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						)}
					</Formik>
				</div>
			</>
		);
	}
}

const SansodhanBibaranPahiloInaruwa = parentProps => (
	<FormContainerV2
		api={[new ApiParam(api.sansodhanBibaranPahilo, 'sansodhanPahilo').setForm(true).getParams()]}
		prepareData={data => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		render={props => <SansodhanBibaranPahiloInaruwaComponent {...props} parentProps={parentProps} />}
	/>
);

export default SansodhanBibaranPahiloInaruwa;
