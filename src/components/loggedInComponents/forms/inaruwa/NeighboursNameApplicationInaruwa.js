import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Table, Select, Grid } from 'semantic-ui-react';
import { NeighboursNameApplicationData } from '../../../../utils/data/NeighboursNameApplicationData';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { getJsonData, handleSuccess, getRelatedFields } from '../../../../utils/dataUtils';
import { getCurrentDate } from './../../../../utils/dateUtils';
import { checkError, prepareMultiInitialValues } from './../../../../utils/dataUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { SurroundingArray } from '../../../../utils/surroundingUtils';
import { DashedUnitInput, UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { FingerPrint, FingerPrintBiratnager } from './../formComponents/FingerprintSignature';
import { DashedLangDateField } from './../../../shared/DateField';
import { validateNullableNepaliDate, validateNullableNumber } from '../../../../utils/validationUtils';
import * as Yup from 'yup';
import { distanceOptions } from '../../../../utils/optionUtils';
import { isBiratnagar } from '../../../../utils/clientUtils';

const Ndata = NeighboursNameApplicationData.structureDesign;

const gharOpt = [
	{ key: 1, value: 'घर', text: 'घर' },
	{ key: 2, value: 'जग्गा', text: 'जग्गा' },
	{ key: 3, value: 'पर्खाल', text: 'पर्खाल' },
];

const NeighbourInaruwaSchema = Yup.object().shape(
	Object.assign({
		date: validateNullableNepaliDate,
		newMeasurement: Yup.array().of(
			Yup.object().shape({
				length: validateNullableNumber,
				width: validateNullableNumber,
				height: validateNullableNumber,
			})
		),
		oldMeasurement: Yup.array().of(
			Yup.object().shape({
				length: validateNullableNumber,
				width: validateNullableNumber,
				height: validateNullableNumber,
			})
		),
	})
);

const measurementFields = ['length', 'width', 'height'];

class NeighboursNameInaruwaComponenet extends Component {
	constructor(props) {
		super(props);
		const { prevData, otherData, permitData, DEFAULT_UNIT_LENGTH } = props;
		const surrArray = new SurroundingArray(permitData.surrounding);
		const completeSurroundingObject = { surrounding: surrArray.getAllSurrounding() };
		const jsonData = getJsonData(prevData);
		const maptech = getJsonData(otherData.maptech);
		let prevDataFormatted;
		try {
			if (jsonData.surrounding) {
				const prevSurrounding = new SurroundingArray(getJsonData(prevData).surrounding);
				prevDataFormatted = { ...getJsonData(prevData), surrounding: prevSurrounding.getAllSurrounding() };
			} else {
				prevDataFormatted = getJsonData(prevData);
			}
		} catch {
			prevDataFormatted = {};
		}

		const initialValues = prepareMultiInitialValues(
			{ obj: { measurementUnit: DEFAULT_UNIT_LENGTH }, reqFields: [] },
			{ obj: maptech, reqFields: ['buildingDetailfloor'] },
			{
				obj: completeSurroundingObject,
				reqFields: [],
			},
			{
				obj: prevDataFormatted,
				reqFields: [],
			}
		);
		if (isStringEmpty(initialValues.date)) {
			initialValues.date = getCurrentDate(true);
		}
		this.state = {
			initialValues,
		};
	}

	render() {
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, userData } = this.props;
		const { initialValues } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={NeighbourInaruwaSchema}
					onSubmit={async (values, actions) => {
						values.surrounding = values.surrounding.filter(
							(surr) => !isStringEmpty(surr.kittaNo) && !isStringEmpty(surr.sandhiyar) && !isStringEmpty(surr.feet)
						);
						try {
							await this.props.postAction(`${api.noticePeriodFor15Days}${permitData.applicantNo}`, values);

							window.scrollTo(0, 0);
							actions.setSubmitting(false);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, handleChange, isSubmitting, errors, setFieldValue, values, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="print-small-font">
									<div className="section-header">
										<h2>{userData.organization.name}</h2>
										<h3>{Ndata.inaruwa.title}</h3>
									</div>
									<div style={{ textAlign: 'justify' }}>
										<DashedLangInput
											name="sandhiyar"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.sandhiyar}
											error={errors.sandhiyar}
										/>
										{Ndata.inaruwa.date_1}
										<DashedLangInput
											name="basney"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.basney}
											error={errors.basney}
										/>
										{Ndata.inaruwa.date_2}
										<DashedLangInput
											name="shri"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.shri}
											error={errors.shri}
										/>
										{Ndata.inaruwa.date_3}
										<DashedLangInput
											name="moji"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.moji}
											error={errors.moji}
										/>
										{Ndata.inaruwa.date_4}
										<DashedLangInput
											name="bibaran"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.bibaran}
											error={errors.bibaran}
										/>
										{Ndata.inaruwa.date_5}
										{Ndata.inaruwa.date_6}
										<DashedLangInput
											name="bamojim"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.bamojim}
											error={errors.bamojim}
										/>
										{Ndata.inaruwa.date_7}
									</div>
									{Ndata.inaruwa.data_8}
									<br />
									{Ndata.inaruwa.data_9}
									<Table className="certificate-ui-table">
										<Table.Body>
											{Ndata.inaruwa.tableData.row.map((row, index) => (
												<Table.Row key={index}>
													<Table.Cell>
														<DashedUnitInput
															name={`surrounding.${index}.feet`}
															label={row}
															unitName={`surrounding.${index}.sideUnit`}
														/>
														{Ndata.inaruwa.tableData.data_1}
														<DashedLangInput
															name={`surrounding.${index}.kittaNo`}
															className="dashedForm-control"
															handleChange={handleChange}
															setFieldValue={setFieldValue}
															value={getIn(values, `surrounding.${index}.kittaNo`)}
															error={getIn(errors, `surrounding.${index}.kittaNo`)}
														/>
														{Ndata.inaruwa.tableData.data_2}
														<DashedLangInput
															name={`${index}.ko`}
															className="dashedForm-control"
															handleChange={handleChange}
															setFieldValue={setFieldValue}
															value={getIn(values, `${index}.ko`)}
															error={getIn(errors, `${index}.ko`)}
														/>
														{Ndata.inaruwa.tableData.data_3}
														<DashedLangInput
															name={`surrounding.${index}.sandhiyar`}
															className="dashedForm-control"
															handleChange={handleChange}
															setFieldValue={setFieldValue}
															value={getIn(values, `surrounding.${index}.sandhiyar`)}
															error={getIn(errors, `surrounding.${index}.sandhiyar`)}
														/>
														<Select
															options={gharOpt}
															name={`${index}.ghar`}
															placeholder="घर"
															onChange={(e, { value }) => setFieldValue(`${index}.ghar`, value)}
															value={getIn(values, `${index}.ghar`)}
															error={getIn(errors, `${index}.ghar`)}
														/>
													</Table.Cell>
												</Table.Row>
											))}
										</Table.Body>
									</Table>
									<Table fixed className="certificate-ui-table">
										<Table.Header>
											<Table.Row>
												{Ndata.inaruwa.tableData.table_2.map((col, index) => (
													<Table.HeaderCell key={index}>{col}</Table.HeaderCell>
												))}
											</Table.Row>
										</Table.Header>
										<Table.Body>
											<Table.Row>
												<Table.Cell>{permitData.newWardNo}</Table.Cell>
												<Table.Cell>{permitData.kittaNo}</Table.Cell>
												<Table.Cell>
													{permitData.landArea}
													{permitData.landAreaType}
												</Table.Cell>
												<Table.Cell>
													<DashedLangInput
														name="tol"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.tol}
														error={errors.tol}
													/>
												</Table.Cell>
												<Table.Cell>{values.buildingDetailfloor}</Table.Cell>
												<Table.Cell>
													<DashedLangInput
														name="kotha"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.kotha}
														error={errors.kotha}
													/>
												</Table.Cell>
												<Table.Cell>
													<DashedLangInput
														name="mohada"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.mohada}
														error={errors.mohada}
													/>
												</Table.Cell>
												<Table.Cell>
													<DashedLangInput
														name="bato"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.bato}
														error={errors.bato}
													/>
												</Table.Cell>
												<Table.Cell>
													<DashedLangInput
														name="kaifiyat"
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={values.kaifiyat}
														error={errors.kaifiyat}
													/>
												</Table.Cell>
											</Table.Row>
										</Table.Body>
									</Table>
									<Table fixed className="certificate-ui-table">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell rowSpan="2">
													{Ndata.inaruwa.tableData.table_3.head_7}
													<UnitDropdownWithRelatedFields
														options={distanceOptions}
														unitName="measurementUnit"
														relatedFields={[
															...getRelatedFields(
																null,
																values.newMeasurement ? values.newMeasurement : [],
																'newMeasurement',
																measurementFields
															),
															...getRelatedFields(
																null,
																values.oldMeasurement ? values.oldMeasurement : [],
																'oldMeasurement',
																measurementFields
															),
														]}
													/>
												</Table.HeaderCell>
												<Table.HeaderCell colSpan="3">{Ndata.inaruwa.tableData.table_3.head_1}</Table.HeaderCell>
												<Table.HeaderCell colSpan="3">{Ndata.inaruwa.tableData.table_3.head_2}</Table.HeaderCell>
												<Table.HeaderCell rowSpan="2">{Ndata.inaruwa.tableData.table_3.head_3}</Table.HeaderCell>
											</Table.Row>
											<Table.Row>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_4}</Table.HeaderCell>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_5}</Table.HeaderCell>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_6}</Table.HeaderCell>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_4}</Table.HeaderCell>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_5}</Table.HeaderCell>
												<Table.HeaderCell>{Ndata.inaruwa.tableData.table_3.head_6}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{Ndata.inaruwa.tableData.table_3.description.map((description, idx) => (
												<Table.Row>
													<Table.Cell>{description}</Table.Cell>
													{measurementFields.map((field) => (
														<Table.Cell>
															<DashedNormalInputIm name={`newMeasurement.${idx}.${field}`} />
														</Table.Cell>
													))}
													{measurementFields.map((field) => (
														<Table.Cell>
															<DashedNormalInputIm name={`oldMeasurement.${idx}.${field}`} />
														</Table.Cell>
													))}
													<Table.Cell>
														{' '}
														<DashedLangInput
															name={`measurement.${idx}.remark`}
															handleChange={handleChange}
															setFieldValue={setFieldValue}
															value={values.secondKaifiyat}
															error={errors.secondKaifiyat}
														/>
													</Table.Cell>
												</Table.Row>
											))}
										</Table.Body>
									</Table>
									<div>
										{Ndata.inaruwa.data_10} <br />
										{Ndata.inaruwa.data_11}
										<DashedLangInput
											name="bato"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.bato}
											error={errors.bato}
										/>
										{Ndata.inaruwa.data_12}
										<br />
										{Ndata.inaruwa.data_13}
										<br />
										{Ndata.inaruwa.data_14}
										<br />
										{Ndata.inaruwa.data_15}
									</div>
									<Grid columns={2}>
										<Grid.Row>
											<Grid.Column>
												<span style={{ textDecoration: 'underline' }}> {Ndata.inaruwa.footer_4}</span>
												{isBiratnagar ? <FingerPrintBiratnager /> : <FingerPrint />}
												<br />
												{Ndata.inaruwa.footer_1}
												<span className="ui input dashedForm-control" />
												<br />
												{Ndata.inaruwa.footer_2}
												<DashedLangInput
													name="name"
													className="dashedForm-control"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.name}
													error={errors.name}
												/>
												<br />
												{Ndata.inaruwa.footer_3}
												<DashedLangInput
													name="nagarikta"
													className="dashedForm-control"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.nagarikta}
													error={errors.nagarikta}
												/>
											</Grid.Column>
											<Grid.Column>
												<span style={{ textDecoration: 'underline' }}> {Ndata.inaruwa.footer_5}</span>
												{isBiratnagar ? <FingerPrintBiratnager /> : <FingerPrint />}
												<br />
												{Ndata.inaruwa.footer_1}
												<span className="ui input dashedForm-control" />
												<br />
												{Ndata.inaruwa.footer_2}
												{permitData.nibedakName}
												<br />
												{Ndata.inaruwa.footer_3}
												{permitData.citizenshipNo}
											</Grid.Column>
										</Grid.Row>
									</Grid>
									<hr />
									<Grid columns={2}>
										<Grid.Row>
											<Grid.Column>
												<span style={{ textDecoration: 'underline' }}> {Ndata.inaruwa.footer_6}</span>
												<DashedLangDateField
													compact={true}
													name="date"
													value={values.date}
													error={errors.date}
													setFieldValue={setFieldValue}
													inline={true}
												/>
											</Grid.Column>
											<Grid.Column>
												<span style={{ textDecoration: 'underline' }}>{Ndata.inaruwa.footer_7}</span>
												<DashedLangInput
													name="nibedak"
													className="dashedForm-control"
													handleChange={handleChange}
													setFieldValue={setFieldValue}
													value={values.nibedak}
													error={errors.nibedak}
												/>
											</Grid.Column>
										</Grid.Row>
									</Grid>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const NeighboursNameApplicationInaruwa = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'maptech',
				form: false,
			},
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param4: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param3: ['15dayspecial'],
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		render={(props) => <NeighboursNameInaruwaComponenet {...props} parentProps={parentProps} />}
	/>
);

export default NeighboursNameApplicationInaruwa;
