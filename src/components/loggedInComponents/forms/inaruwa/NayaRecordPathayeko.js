import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select, Table, TableCell } from 'semantic-ui-react';

import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { NayaRecordData } from '../../../../utils/data/NayaRecordData';
import { PatraSankhyaAndDate } from '../formComponents/PatraSankhyaAndDate';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { genderOptions } from '../../../../utils/optionUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { getDataConstructionType } from '../../../../utils/enums/constructionType';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import EbpsForm from '../../../shared/EbpsForm';
import { DashedSelect } from '../../../shared/Select';
import { SectionHeader } from '../../../uiComponents/Headers';

const nayaRecordDetail = NayaRecordData.nayaRecord;
const formLanguage = NayaRecordData.data;
const initialVal = {
	namePrefix: genderOptions[0].value,
	secondNamePrefix: genderOptions[0].value,
	thirdNamePrefix: genderOptions[0].value,
};
class NayaRecordComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData } = this.props;
		const jsonData = getJsonData(prevData);

		let initialValues = prepareMultiInitialValues(
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: {
					nirnayaMiti: getCurrentDate(true),
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.allowanceDate)) {
			initialValues.allowanceDate = getCurrentDate(true);
		}

		this.state = {
			initialValues,
		};
	}
	render() {
		const { userData, permitData, prevData, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialValues } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					// validationSchema={AllowancePaperInaruwaSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.NayaRecordPathayekoNamsari, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="build-gaps">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} />
									<div style={{ marginTop: '20px' }}>
										{nayaRecordDetail.rajaswa}
										<br />
										{userData.organization.name}
										{nayaRecordDetail.karyalaya}
									</div>
									<SectionHeader>
										<h4 className="underline">{nayaRecordDetail.title}</h4>
									</SectionHeader>
									<br />
									<div>
										{formLanguage.uparyukta}
										{userData.organization.name}
										{formLanguage.wada}
										{permitData.newWardNo}
										{formLanguage.basne}
										<DashedSelect name="namePrefix" options={genderOptions} /> {permitData.nibedakName}{' '}
										<DashedLangDateField
											name="suikritDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.suikritDate}
											label={formLanguage.lai}
										/>
										{formLanguage.suikriti} {getDataConstructionType(permitData.constructionType)}
										{formLanguage.nirman}
									</div>
									<div>
										{formLanguage.wada}
										{permitData.newWardNo}
										{formLanguage.ghar} <DashedLangInput name="gharNo" setFieldValue={setFieldValue} value={values.gharNo} />
										{formLanguage.sadak}
										{permitData.buildingJoinRoad}
									</div>
									<br />
									<div>
										{getDataConstructionType(permitData.constructionType)}
										{formLanguage.bibaran}
										<Table celled compact collapsing>
											<Table.Header>
												<Table.Row textAlign="center">
													{Object.keys(formLanguage.bibaranTable).map((key) => (
														<Table.HeaderCell key={key}>{formLanguage.bibaranTable[key]}</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="gharSN"
														setFieldValue={setFieldValue}
														value={values.gharSN}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="tallaNo"
														setFieldValue={setFieldValue}
														value={values.tallaNo}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="lambai"
														setFieldValue={setFieldValue}
														value={values.lambai}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="chaudai"
														setFieldValue={setFieldValue}
														value={values.chaudai}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="uchai"
														setFieldValue={setFieldValue}
														value={values.uchai}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="gharKshetrafal"
														setFieldValue={setFieldValue}
														value={values.gharKshetrafal}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="gharKisim"
														setFieldValue={setFieldValue}
														value={values.gharKisim}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="gharNirman"
														setFieldValue={setFieldValue}
														value={values.gharNirman}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '95px' }}
														name="gharBaki"
														setFieldValue={setFieldValue}
														value={values.gharBaki}
													/>
												</TableCell>
											</Table.Body>
										</Table>
									</div>
									<br />
									<div>
										{formLanguage.jaggaBibaran}
										<Table celled compact collapsing>
											<Table.Header>
												<Table.Row textAlign="center">
													{Object.keys(formLanguage.jaggaBibaranTable).map((key) => (
														<Table.HeaderCell key={key}>{formLanguage.jaggaBibaranTable[key]}</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="theliMoth"
														setFieldValue={setFieldValue}
														value={values.theliMoth}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="kshetra"
														setFieldValue={setFieldValue}
														value={values.kshetra}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaSabik"
														setFieldValue={setFieldValue}
														value={values.jaggaSabik}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaHal"
														setFieldValue={setFieldValue}
														value={values.jaggaHal}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaKitta"
														setFieldValue={setFieldValue}
														value={values.jaggaKitta}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaKshetrafal"
														setFieldValue={setFieldValue}
														value={values.jaggaKshetrafal}
													/>
												</TableCell>
											</Table.Body>
										</Table>
									</div>
									<br />
									<div>
										{formLanguage.namsari}
										<br />
										<DashedLangDateField
											name="nirnayaMiti"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.nirnayaMiti}
											label={formLanguage.miti}
										/>{' '}
										{formLanguage.koNirnay}
										<DashedSelect name="secondNamePrefix" options={genderOptions} />
										<DashedLangInput name="koNamBata" setFieldValue={setFieldValue} value={values.koNamBata} />
										{formLanguage.koNambata}
										<DashedSelect name="thirdNamePrefix" options={genderOptions} />
										<DashedLangInput name="koNamMa" setFieldValue={setFieldValue} value={values.koNamMa} />
										{formLanguage.koNamma}
									</div>
									<br />
									<div>
										{formLanguage.jaggaBibaran}
										<Table celled compact collapsing>
											<Table.Header>
												<Table.Row textAlign="center">
													{Object.keys(formLanguage.jaggaBibaranTable).map((key) => (
														<Table.HeaderCell key={key}>{formLanguage.jaggaBibaranTable[key]}</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="theliMothNext"
														setFieldValue={setFieldValue}
														value={values.theliMothNext}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="kshetraNext"
														setFieldValue={setFieldValue}
														value={values.kshetraNext}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaSabikNext"
														setFieldValue={setFieldValue}
														value={values.jaggaSabikNext}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaHalNext"
														setFieldValue={setFieldValue}
														value={values.jaggaHalNext}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaKittaNext"
														setFieldValue={setFieldValue}
														value={values.jaggaKittaNext}
													/>
												</TableCell>
												<TableCell>
													<EbpsForm
														style={{ width: '150px' }}
														name="jaggaKshetrafalNext"
														setFieldValue={setFieldValue}
														value={values.jaggaKshetrafalNext}
													/>
												</TableCell>
											</Table.Body>
										</Table>
									</div>
									<br />
									<div>
										{formLanguage.kittaKat}
										<br />
										{formLanguage.sabikKitta}
										<DashedLangInput name="sabikKitta" setFieldValue={setFieldValue} value={values.sabikKitta} />
										{formLanguage.jaggaBibaranTable.kshetrafal}
										<DashedLangInput name="sabikKshetrafal" setFieldValue={setFieldValue} value={values.sabikKshetrafal} />
										{formLanguage.halKayam}
										<DashedLangInput name="halKitta" setFieldValue={setFieldValue} value={values.halKitta} />
										{formLanguage.jaggaBibaranTable.kshetrafal}
										<DashedLangInput name="halKshetrafal" setFieldValue={setFieldValue} value={values.halKshetrafal} />
									</div>
									<br />
									<div style={{ textAlign: 'right' }}>
										<div className="signature-div">
											<span className="ui input signature-placeholder" />
											<p>{formLanguage.footer}</p>
										</div>
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const NayaRecordPathayeko = (parentProps) => (
	<FormContainerV2
		api={[{ api: api.NayaRecordPathayekoNamsari, objName: 'NayaRecordPathayekoNamsari', form: true }]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <NayaRecordComponent {...props} parentProps={parentProps} />}
	/>
);

export default NayaRecordPathayeko;
