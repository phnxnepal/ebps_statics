import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import * as Yup from 'yup';

import { GharSurjaminData } from '../../../../utils/data/GharSurjaminData';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues, surroundingMappingFlat } from '../../../../utils/dataUtils';
import api from '../../../../utils/api';
import { showToast, getUserRole } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { GenericApprovalFileView } from '../../../shared/file/GenericApprovalFileView';
import { FormUrlFull } from '../../../../utils/enums/url';
import { WARD_MASTER } from '../../../../utils/constants';
import LongFormDate from '../formComponents/LongFormDate';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { DashedSelect } from '../../../shared/Select';
import { getLocalStorage } from '../../../../utils/secureLS';
import { UserType } from '../../../../utils/userTypeUtils';
import { getUserTypeValueNepali } from './../../../../utils/functionUtils';
import { PrintSelectors } from '../../../../utils/printUtils';

const data = GharSurjaminData.inaruwa;
const schema = Yup.object().shape({
	tamelDate: validateNullableNepaliDate,
});

class GharNakshaSurjaminInaruwaComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData, userData, permitData } = this.props;
		let erInfo = {
			erName: '',
			erDesignation: '',
			erDate: '',
		};
		if (getUserRole() === UserType.ENGINEER) {
			erInfo.erName = userData.userName;
			erInfo.erDesignation = getUserTypeValueNepali(userData.userType);
			erInfo.erDate = userData.info.joinDate;
		}
		const json_data = getJsonData(prevData);
		const wardOption = JSON.parse(getLocalStorage(WARD_MASTER)).map(ward => {
			return { key: ward.id, value: ward.name, text: ward.name };
		});
		const initialValues = prepareMultiInitialValues(
			{
				obj: erInfo,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['surrounding'],
			},
			{ obj: json_data, reqFields: [] }
		);

		this.state = { initialValues, wardOption };
	}
	render() {
		const { initialValues, wardOption } = this.state;
		const { permitData, prevData, userData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, errors: reduxErrors } = this.props;
		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.gharnakshaSurjaminMuchulka}`, values, true);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div className="section-header">
									<h2 className="end-section">{userData.organization.name}</h2>
									<h3 className="underline">{data.title}</h3>
									<h3 className="end-section">({data.title1})</h3>
								</div>

								<div style={{ textAlign: 'justify' }}>
									{data.line1}{' '}
									<span>
										{userData.organization.address} {userData.organization.name}{' '}
									</span>
									{data.wardNo}
									{permitData.newWardNo}
									{data.line2}
									{permitData.sadak}
									{data.shree}
									{permitData.applicantName}
									{data.line4}
									{permitData.kittaNo}
									{data.sadakSlash}
									{permitData.landArea}
									{permitData.landAreaType}
									{data.line5}
									{permitData.constructionType}
									{data.line6}
									<DashedLangInput
										name="tapasilbamojim"
										setFieldValue={setFieldValue}
										value={values.tapasilbamojim}
										error={errors.tapasilbamojim}
									/>{' '}
									{data.line6_1} {userData.organization.name},{userData.organization.address}
									{data.line7} {userData.organization.name} {data.line8}
								</div>
								<br />
								<div>
									<div className="section-header margin-section">
										<h3 className="underline">{data.line8_1}</h3>
										<br />
										<h3>{data.jaggaBibaran}</h3>
									</div>
									{data.number.map((num, index) => (
										<div className="left-align" key={index}>
											{num} {userData.organization.name}
											{data.wardNo}
											<DashedSelect name={`ward.${index}`} options={wardOption} />
											{data.line2}
											<DashedLangInput
												name={`address.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `address.${index}`)}
												error={getIn(errors, `address.${index}`)}
											/>
											{data.basne}
											<DashedLangInput
												name={`age.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `age.${index}`)}
												error={getIn(errors, `age.${index}`)}
											/>
											{data.koShree}
											<DashedLangInput
												name={`person.${index}`}
												setFieldValue={setFieldValue}
												value={getIn(values, `person.${index}`)}
												error={getIn(errors, `person.${index}`)}
											/>
											{data.data_4}
										</div>
									))}
									<br />
									<div className="section-header margin-section">
										<h3>{data.sabik}</h3>
									</div>
									<DashedSelect name="wardAdhachya" options={wardOption} />
									{data.data_1}
									<DashedLangInput
										name="sadasyaShree"
										setFieldValue={setFieldValue}
										value={values.sadasyaShree}
										error={errors.sadasyaShree}
									/>
									{data.data_2}
									<DashedLangInput
										name="darkhastaShree"
										setFieldValue={setFieldValue}
										value={values.darkhastaShree}
										error={errors.darkhastaShree}
									/>
									{data.data_4}
									<br />
									<br />
									{data.data_5}
									<span className="ui input signature-placeholder" />
									<div className="section-header margin-section">
										<h3>{data.data_6}</h3>
									</div>
									<div>
										{permitData.surrounding &&
											permitData.surrounding.map((surr, idx) => (
												<React.Fragment key={idx}>
													{surroundingMappingFlat.find(fl => fl.side === surr.side).value}
													<DashedUnitInput name={`surrounding.${idx}.feet`} unitName={`surrounding.${idx}.sideUnit`} />
													{data.koShreeMati}
													<DashedLangInput
														name={`surrounding.${idx}.sandhiyar`}
														className="dashedForm-control"
														handleChange={handleChange}
														setFieldValue={setFieldValue}
														value={getIn(values, `surrounding.${idx}.sandhiyar`)}
														error={getIn(errors, `surrounding.${idx}.sandhiyar`)}
													/>
													<br />
												</React.Fragment>
											))}
									</div>
									<br />
									<div className="section-header">
										<h3 className="left-align">{data.rohabarMain}</h3>
									</div>
									<div>
										{userData.organization.name} {data.wardNo}
										{permitData.nibedakTol}
										{data.basne}
										{permitData.tol}
										{data.main1}
										{permitData.applicantName}
										{data.wardNo}
										{permitData.newWardNo}
										{data.main2}
										<DashedLangInput
											name="rohabarPerson1"
											setFieldValue={setFieldValue}
											value={values.rohabarPerson1}
											error={errors.rohabarPerson1}
										/>
									</div>
									<br />
									<FlexSingleLeft>
										{data.data_7}
										<br />
										{data.data_8}
										<DashedLangInput name="erName" setFieldValue={setFieldValue} value={values.erName} error={errors.erName} />
										<br />
										{data.data_9}
										<DashedLangInput
											name="erDesignation"
											setFieldValue={setFieldValue}
											value={values.erDesignation}
											error={errors.erDesignation}
										/>
										<br />
										{data.data_10}
										<CompactDashedLangDate
											name="erDate"
											setFieldValue={setFieldValue}
											value={values.erDate}
											error={errors.erDate}
										/>
										<br />
									</FlexSingleLeft>
									<div className="section-header margin-section">
										<h3>
											{userData.organization.name}
											{data.bata}
										</h3>
									</div>
									<div className="margin-section">
										<div>
											{data.prabi}
											<DashedLangInput name="prabi" setFieldValue={setFieldValue} value={values.prabi} error={errors.prabi} />
											{data.pad}
											<DashedLangInput
												name="prabiPad"
												setFieldValue={setFieldValue}
												value={values.prabiPad}
												error={errors.prabiPad}
											/>
										</div>
										<div className="margin-section">
											{data.prasanik}
											<DashedLangInput
												name="prasanik"
												setFieldValue={setFieldValue}
												value={values.prasanik}
												error={errors.prasanik}
											/>
											{data.pad}
											<DashedLangInput
												name="prasanikPad"
												setFieldValue={setFieldValue}
												value={values.prasanikPad}
												error={errors.prasanikPad}
											/>
										</div>
										<LongFormDate />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const GharNaksaApproveView = props => {
	return (
		<div ref={props.setRef}>
			<GenericApprovalFileView
				fileCategories={props.fileCategories}
				files={props.files}
				url={FormUrlFull.GHAR_NAKSA_SURJAMIN}
				prevData={checkError(props.prevData)}
			/>
		</div>
	);
};

const GharNakshaSurjaminInaruwaView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.gharnakshaSurjaminMuchulka,
				objName: 'gharNaksaSurjamin',
				form: true,
			},
		]}
		prepareData={data => data}
		onBeforeGetContent={{
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param9: ['getElementsByClassName', PrintSelectors.DASHED_DROPDOWN],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		hasFileView={true}
		render={props =>
			props.hasSavePermission ? (
				<GharNakshaSurjaminInaruwaComponent {...props} parentProps={parentProps} />
			) : (
				<GharNaksaApproveView {...props} parentProps={parentProps} />
			)
		}
	/>
);

export default GharNakshaSurjaminInaruwaView;
