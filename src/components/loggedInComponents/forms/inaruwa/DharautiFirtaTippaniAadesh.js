import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';

import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DharautiFirtaTippaniData } from '../../../../utils/data/DharautiFirtaTippaniAadeshData';
import { genderOptions, setBackOptions } from '../../../../utils/optionUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';

const tippaniAadeshTitle = DharautiFirtaTippaniData.title;
const tippaniAadeshLanguage = DharautiFirtaTippaniData.data;

const prabhidikOpt = [
    { key: 'a1', value: 'कन्सल्टेन्ट', text: 'कन्सल्टेन्ट' },
    { key: 'a2', value: 'को ', text: 'को' },
];

class DharautiFirtaTippaniComponent extends Component {

    constructor(props) {
        super(props);
        const { prevData, otherData } = this.props;
        const jsonData = getJsonData(prevData);
        const sansodhanSuper = getJsonData(otherData.sansodhanSuperStructure);

        let initialValues = prepareMultiInitialValues(
            {
                obj: sansodhanSuper,
                reqFields: ['date'],
            },
            {
                obj: {
                    consultant: 'कन्सल्टेन्ट',
                },
                reqFields: ['consultant'],
            },
            {
                obj: jsonData,
                reqFields: [],
            },
            {
                obj: {
                    dharautiMiti: getCurrentDate(true)
                },
                reqFields: []
            },
        );

        this.state = {
            initialValues,
        };

    }
    render() {
        const { userData, permitData, prevData, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled } = this.props;
        const { initialValues } = this.state;
        return (
            <div>
                {this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
                <Formik
                    initialValues={initialValues}
                    // validationSchema={AllowancePaperInaruwaSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        setSubmitting(true);

                        values.applicationNo = this.props.permitData.applicantNo;

                        try {
                            await this.props.postAction(api.DharautiPhirtaTippaniAdes, values, true);
                            window.scroll(0, 0);
                            if (this.props.success && this.props.success.success) {
                                handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                            }
                            setSubmitting(false);
                        } catch (err) {
                            setSubmitting(false);
                            console.log('Error', err);
                        }
                    }}
                >
                    {({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
                        <Form loading={isSubmitting}>
                            <div ref={this.props.setRef} className="build-gaps">
                                <div>
                                    <LetterHeadFlex userInfo={userData} />
                                    <SectionHeader>
                                        <h3 className="underline">{tippaniAadeshTitle.tippani}</h3>
                                    </SectionHeader>
                                    <SectionHeader>
                                        <h3 className="underline">{tippaniAadeshTitle.heading}</h3>
                                    </SectionHeader>
                                </div>
                                <br />
                                <div>
                                    {userData.organization.name}{tippaniAadeshLanguage.wada}
                                    {permitData.nibedakTol}{tippaniAadeshLanguage.basne}
                                    <Select placeholder="श्री" options={genderOptions} />
                                    {' '}{permitData.nibedakName}{tippaniAadeshLanguage.le}
                                    {userData.organization.name}{tippaniAadeshLanguage.wada}
                                    {permitData.newWardNo}{tippaniAadeshLanguage.ko}
                                    {permitData.applicantName}{tippaniAadeshLanguage.namnaDarta}
                                    {userData.organization.name}{tippaniAadeshLanguage.wada}
                                    {permitData.newWardNo}{tippaniAadeshLanguage.ko}
                                    {permitData.buildingJoinRoad}{tippaniAadeshLanguage.sadak}
                                    {permitData.kittaNo}{tippaniAadeshLanguage.kshetrafal}
                                    {permitData.landArea}{' '}({permitData.landAreaType})
                                    {tippaniAadeshLanguage.ma} <DashedLangInput name="chetrafalMa" setFieldValue={setFieldValue} value={values.chetrafalMa} />
                                    <DashedLangDateField
                                        name="suikritDate"
                                        inline={true}
                                        setFieldValue={setFieldValue}
                                        value={values.suikritDate}
                                        label={tippaniAadeshLanguage.lagiMiti}
                                    />
                                    {tippaniAadeshLanguage.suikriti} <DashedLangInput name="patralei" setFieldValue={setFieldValue} value={values.patralei} />
                                    {tippaniAadeshLanguage.koNirman}
                                    {/* <DashedLangInput name="prabidhik" setFieldValue={setFieldValue} value={values.prabidhik} /> */}
                                    {values.consultant === 'कन्सल्टेन्ट' ? `${values.consultancyName}` : `${values.userName}`}{' '}
                                    <Select
                                        name="consultant"
                                        options={prabhidikOpt}
                                        value={values.consultant}
                                        onChange={(e, { value }) => setFieldValue('consultant', value)}
                                    />
                                    {tippaniAadeshLanguage.consultant} <Select placeholder="सेट व्याक" options={setBackOptions} />{' '}
                                    <DashedLangDateField
                                        name="dharautiMiti"
                                        inline={true}
                                        setFieldValue={setFieldValue}
                                        value={values.dharautiMiti}
                                        label={tippaniAadeshLanguage.nachodeko}
                                    />
                                    {tippaniAadeshLanguage.maRakheko} <DashedLangInput name="dharautiRakam" setFieldValue={setFieldValue} value={values.dharautiRakam} />
                                    {tippaniAadeshLanguage.sahitNirman}
                                </div>
                            </div>
                            <SaveButtonValidation
                                errors={errors}
                                validateForm={validateForm}
                                formUrl={formUrl}
                                hasSavePermission={hasSavePermission}
                                hasDeletePermission={hasDeletePermission}
                                isSaveDisabled={isSaveDisabled}
                                prevData={checkError(prevData)}
                                handleSubmit={handleSubmit}
                            />
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}

const DharautiFirtaTippaniAadesh = parentProps => (
    <FormContainerV2
        api={[
            { api: api.DharautiPhirtaTippaniAdes, objName: 'DharautiPhirtaTippaniAdes', form: true },
            {
                api: api.sansodhanSupStruIjjaat,
                objName: 'sansodhanSuperStructure',
                form: false,
            },
        ]}
        prepareData={data => data}
        useInnerRef={true}
        parentProps={parentProps}
        onBeforeGetContent={{
            //param1: ["getElementsByTagName", "input", "value"],
            ...PrintParams.INLINE_FIELD,
            param4: ['getElementsByTagName', 'textarea', 'value'],
            // param2: ['getElementsByTagName', 'span', 'innerText'],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
            param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
            param9: [PrintIdentifiers.CHECKBOX_LABEL],
            // param7: ['getElementsByClassName', 'ui checkbox', 'value'],
        }}
        render={props => <DharautiFirtaTippaniComponent {...props} parentProps={parentProps} />}
    />
);

export default DharautiFirtaTippaniAadesh;