import React, { Component } from 'react';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getJsonData, prepareMultiInitialValues, FloorArray, handleSuccess, checkError } from '../../../../utils/dataUtils';
import * as Yup from 'yup';
import { getCurrentDate } from '../../../../utils/dateUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { Formik, getIn } from 'formik';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { PrintParams } from '../../../../utils/printUtils';
import { CertificateSubHeadingWithoutPhoto } from '../certificateComponents/CertificateComponents';
import { InaruwaNamsariData } from '../../../../utils/data/BuildingBuildCertificateData';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { Form, Select } from 'semantic-ui-react';
import { shreeOptions } from '../../../../utils/optionUtils';
import { ShreeDropdown } from '../formComponents/ShreeDropdown';
import { DashedLangInput, DashedLangInputWithSlash } from '../../../shared/DashedFormInput';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { DashedLangDateField } from './../../../shared/DateField';
import { FlexSingleLeft } from '../../../uiComponents/FlexDivs';

const finishDetail = InaruwaNamsariData.finishDetail;
const inaruwaLanguage = InaruwaNamsariData.data;

const charan_option = [
	{ key: 1, value: 'प्रथम चरणसम्मको', text: 'प्रथम चरणसम्मको' },
	{ key: 2, value: 'दोश्रो चरणको', text: 'दोश्रो चरणको' },
];

const nirman_option = [
	{ key: 1, value: 'इजाजत', text: 'इजाजत' },
	{ key: 2, value: 'कार्य सम्पन्न', text: 'कार्य सम्पन्न' },
];

const convertbib = (inputIndex) => {
	if (inputIndex === 'A') {
		return '(क)';
	} else if (inputIndex === 'B') {
		return '(ख)';
	} else if (inputIndex === 'C') {
		return '(ग)';
	} else {
		return '(घ)';
	}
};

const NamsariSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNullableNepaliDate,
		date: validateNullableNepaliDate,
		pariwartanMiti: validateNullableNepaliDate,
	})
);

class NamsariPramanpatraInaruwaComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData } = this.props;
		const mapTech = getJsonData(otherData.mapTech);
		const floorArray = new FloorArray(permitData.floor);
		const groundFloor = floorArray.getFloorByFloor(1);
		const tallaUnit = floorArray.getNepaliFloorUnit();
		const prabhidik = getJsonData(otherData.prabhidik);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const certificateNote = getJsonData(otherData.certificateNote);
		const anuSucKaJsonData = otherData.anukaMaster;
		const desApprovJsonData = otherData.designApprovalData;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let inputIndex = buildingClass ? buildingClass.id : '';

		const initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: [
					'floor',
					'isHighTensionLineDistance',
					'highTensionLineUnit',
					'photo',
					'purposeOfConstruction',
					'purposeOfConstructionOther',
				],
			},
			{
				obj: mapTech,
				reqFields: ['coverageDetails', 'buildingHeight'],
			},
			{
				obj: prabhidik,
				reqFields: ['roof'],
			},
			{
				obj: { namsariName: certificateNote.shreeName || certificateNote.sasthaYaExtraName },
				reqFields: [],
			},
			{
				obj: {
					Bdate: getCurrentDate(true),
					date: getCurrentDate(true),
					pariwartanMiti: getCurrentDate(true),
					shree: shreeOptions[0].value,
					buildclass: inputIndex,
				},
				reqFields: [],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		this.state = {
			initialValues,
			groundFloor,
			tallaUnit,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const user_info = userData;
		const { initialValues, groundFloor, tallaUnit } = this.state;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={NamsariSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(`${api.buildingBuild}${this.props.permitData.nameTransaferId}`, values);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} photo={values.photo} compact={true} />
									<CertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										titles={finishDetail.title}
										textSize={TextSize.MEDIUM}
									/>
									<FlexSingleLeft>
										{inaruwaLanguage.gharNo}
										<DashedLangInputWithSlash
											compact={true}
											name="ghar"
											setFieldValue={setFieldValue}
											value={values.ghar}
											error={errors.ghar}
										/>
										<br />
										{inaruwaLanguage.sanketNo}
										<DashedLangInputWithSlash
											compact={true}
											name="sanket"
											setFieldValue={setFieldValue}
											value={values.sanket}
											error={errors.sanket}
										/>
									</FlexSingleLeft>
									<br />
									<ShreeDropdown name="shree" />
									<DashedLangInput name="shreeName" setFieldValue={setFieldValue} inline={true} value={values.shreeName} />
									<div className="indent" style={{ textAlign: 'justify' }}>
										{inaruwaLanguage.tapaiShree}
										{values.namsariName}
										{inaruwaLanguage.wadaNo}
										{permitData.newWardNo}
										{inaruwaLanguage.jaggaBibaran} {permitData.landArea} ({permitData.landAreaType}){inaruwaLanguage.nirmanMiti}{' '}
										{permitData.applicantDateBS}
										{inaruwaLanguage.ijajat} {permitData.applicantDateBS.substring(0, 4)}
										{inaruwaLanguage.lambai} {groundFloor.length} {tallaUnit.unit}
										{inaruwaLanguage.chaudai} {groundFloor.width} {tallaUnit.unit}
										{inaruwaLanguage.uchai} {groundFloor.height} {tallaUnit.unit}
										{inaruwaLanguage.ko} {groundFloor.floor}
										{inaruwaLanguage.talle} {groundFloor.area} {tallaUnit.areaUnit}
										{inaruwaLanguage.ko}
										<Select
											options={charan_option}
											name="charan"
											placeholder="प्रथम चरणसम्मको"
											onChange={(e, { value }) => setFieldValue('charan', value)}
											value={values['charan']}
											error={errors.charan}
										/>
										{inaruwaLanguage.nirman}
										<Select
											options={nirman_option}
											name="nirman"
											placeholder="इजाजत"
											onChange={(e, { value }) => setFieldValue('nirman', value)}
											value={values['nirman']}
											error={errors.nirman}
										/>
										{inaruwaLanguage.pramanPatra}
										<DashedLangInput name="tapaiKo" setFieldValue={setFieldValue} inline={true} value={values.tapaiKo} />
										{inaruwaLanguage.tapaiKo}
									</div>
									<br />
									<div>
										{inaruwaLanguage.bargikaran}
										{inaruwaLanguage.bargikaran_opt.map((input, idx) => (
											<div className="ui radio checkbox prabidhik" key={idx}>
												<input type="radio" name="buildclass" defaultChecked={values.buildclass === input} value={input} />
												<label>{convertbib(input)}</label>
											</div>
										))}
										&emsp;
										{inaruwaLanguage.nirmanStructure}
										<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
									</div>
									<div>
										<div
											style={{
												display: 'flex',
											}}
										>
											<div>{inaruwaLanguage.charkilla.e1}</div>
											<div style={{ display: 'flex' }}>
												{values.surrounding &&
													values.surrounding.map((index, i) => (
														<div style={{ marginLeft: '5px' }}>
															{surrounding.find((fl) => fl.direction === index.side).value}{' '}
															<DashedLangInput
																name={`surrounding.${i}.sandhiyar`}
																setFieldValue={setFieldValue}
																value={getIn(values, `surrounding.${i}.sandhiyar`)}
																handleChange={handleChange}
																error={getIn(errors, `surrounding.${i}.sandhiyar`)}
															/>{' '}
														</div>
													))}
											</div>
										</div>
									</div>
									<div>
										{inaruwaLanguage.sabikOwner}
										<DashedLangInput name="sabikName" setFieldValue={setFieldValue} value={values.sabikName} />
									</div>
									<div>
										{inaruwaLanguage.sabikKitta}
										<DashedLangInput name="kitta" setFieldValue={setFieldValue} value={values.kitta} />
									</div>
									<div>
										{inaruwaLanguage.sabikJaBi}
										<DashedLangInput name="jaggaBibaran" setFieldValue={setFieldValue} value={values.jaggaBibaran} />
									</div>
									<div>
										{inaruwaLanguage.currentOwner}
										{permitData.applicantName}
									</div>
									<div>
										{inaruwaLanguage.currentKitta}
										{permitData.kittaNo}
									</div>
									<div>
										{inaruwaLanguage.currentJaBi}
										{permitData.landArea}
										{permitData.landAreaType}
									</div>
									<div>
										{inaruwaLanguage.paribartanMiti}
										<DashedLangDateField
											compact={true}
											name="pariwartanMiti"
											value={values.pariwartanMiti}
											error={errors.pariwartanMiti}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</div>
									<div className="signature-div flex-item-space-between">
										<div>
											<span style={{ textDecoration: 'underline' }}>{inaruwaLanguage.footer.bodartha}</span>
											<br />
											{inaruwaLanguage.footer.rajaswa}
											<br />
											{userData.organization.name}
										</div>
										<div>
											{inaruwaLanguage.footer.peshGarne}
											<DashedLangInput name="peshGarney" setFieldValue={setFieldValue} value={values.peshGarney} />
										</div>
										<div>
											{inaruwaLanguage.footer.pramanit.garne}
											<DashedLangInput name="pramanit" setFieldValue={setFieldValue} value={values.pramanit} />
											<br />
											{inaruwaLanguage.footer.pramanit.sign}
											<span className="ui input signature-placeholder" />
											<br />
											{inaruwaLanguage.footer.pramanit.name}
											<DashedLangInput name="name" setFieldValue={setFieldValue} value={values.name} />

											<br />
											{inaruwaLanguage.footer.pramanit.pad}
											<DashedLangInput name="pad" setFieldValue={setFieldValue} value={values.pad} />
											<br />
											{inaruwaLanguage.footer.pramanit.miti}
											<DashedLangDateField
												compact={true}
												name="date"
												value={values.date}
												error={errors.date}
												setFieldValue={setFieldValue}
												inline={true}
											/>
										</div>
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const NamsariPramanpatraInaruwa = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingBuild, objName: 'buildingBuild', form: true },
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.certificateNote,
				objName: 'certificateNote',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <NamsariPramanpatraInaruwaComponent {...props} parentProps={parentProps} />}
	/>
);

export default NamsariPramanpatraInaruwa;
