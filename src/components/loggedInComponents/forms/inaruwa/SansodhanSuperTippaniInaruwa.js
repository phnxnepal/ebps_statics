import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Select } from 'semantic-ui-react';
import * as Yup from 'yup';
import { SansodhanInaruwaData } from '../../../../utils/data/SansodhanTippaniInaruwaData';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { getCurrentDate } from './../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import merge from 'lodash/merge';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { validateNullableNepaliDate, validateNumber } from '../../../../utils/validationUtils';
import { PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { ShreeDropdown } from './../formComponents/ShreeDropdown';
import { shreeOptions } from '../../../../utils/optionUtils';
import { DashedLangDateField, CompactDashedLangDate } from './../../../shared/DateField';
import { getCertificateConstructionType } from '../../../../utils/enums/constructionType';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { getUserRole } from '../../../../utils/functionUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { NepaliNumberToWord } from './../../../../utils/nepaliAmount';
import { DashedNormalInput } from './../../../shared/DashedFormInput';

const sansodhanInaruwa = SansodhanInaruwaData.sansodhan_data;
const gharOption = [
    { key: 1, value: 'थप कोठा', text: 'थप कोठा' },
    { key: 2, value: 'थपतला निर्माण गर्न', text: 'थपतला निर्माण गर्न' },
    { key: 3, value: 'निर्माण गरेकोमा', text: 'निर्माण गरेकोमा' },
];

const sansodhamSchema = Yup.object().shape(
    Object.assign({
        sansodhanDate: validateNullableNepaliDate,
        supStrucDate: validateNullableNepaliDate,
        dasturAmount: validateNumber,
    })
);
class SansodhanTippaniInaruwa extends Component {
    constructor(props) {
        super(props);
        const { otherData, prevData, userData } = this.props;
        let initialValues = {};
        const superData = getJsonData(otherData.supStruCons);
        let userInfo = {
            subName: '',
        };
        if (getUserRole() === UserType.SUB_ENGINEER) {
            userInfo.subName = userData.userName;
        }
        const initialValues1 = prepareMultiInitialValues(
            {
                obj: superData,
                reqFields: ['supStrucDate'],
            },
            {
                obj: userInfo,
                reqFields: [],
            },
            {
                obj: {
                    shree: shreeOptions[0].value,
                    ghar: gharOption[0].value,
                },
                reqFields: [],
            },
            { obj: getJsonData(prevData), reqFields: [] }
        );
        if (isStringEmpty(initialValues1.sansodhanDate)) {
            initialValues1.sansodhanDate = getCurrentDate(true);
        }
        const initVal = merge(initialValues, initialValues1);
        this.state = {
            initVal
        };

    }
    render() {
        const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
        const { initVal } = this.state;
        return (
            <div>
                {this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
                <Formik
                    initialValues={initVal}
                    validationSchema={sansodhamSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        setSubmitting(true);
                        values.applicationNo = this.props.permitData.applicantNo;
                        try {
                            await this.props.postAction(api.sansodhanSuperTippaniAdesh, values, true);
                            window.scroll(0, 0);
                            if (this.props.success && this.props.success.success) {
                                handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                            }
                            setSubmitting(false);
                        } catch (err) {
                            setSubmitting(false);
                            console.log('Error', err);
                        }
                    }}
                >
                    {({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
                        <Form loading={isSubmitting}>
                            <div ref={this.props.setRef}>
                                <div>
                                    <LetterHeadFlex compact={true} userInfo={userData} />
                                    <div style={{ textAlign: 'right' }}>
                                        <DashedLangDateField
                                            label={sansodhanInaruwa.date}
                                            name="sansodhanDate"
                                            setFieldValue={setFieldValue}
                                            error={errors.sansodhanDate}
                                            value={values.sansodhanDate}
                                            inline={true}

                                        />
                                    </div>
                                    <div style={{ textAlign: 'center' }}>
                                        <h3 >
                                            <span style={{ textDecoration: 'underline' }}>{sansodhanInaruwa.subject}</span><br />
                                            <span>{sansodhanInaruwa.tippani_title}</span>
                                        </h3>
                                    </div>
                                    <div>
                                        <div>
                                            <span className="indent-span">
                                                {userData.organization.name}
                                            </span>
                                            {sansodhanInaruwa.content.content_1}
                                            {permitData.newWardNo}
                                            {sansodhanInaruwa.content.content_2}
                                            <ShreeDropdown name="shree" />{' '}
                                            {permitData.applicantName}
                                            {sansodhanInaruwa.content.content_3}
                                            {permitData.oldMunicipal}
                                            {sansodhanInaruwa.content.content_4}
                                            {permitData.newWardNo}
                                            {sansodhanInaruwa.content.content_5}
                                            {permitData.sadak}
                                            {sansodhanInaruwa.content.content_6}
                                            {permitData.kittaNo}
                                            {sansodhanInaruwa.content.content_7}
                                            {permitData.landArea}{permitData.landAreaType}
                                            {sansodhanInaruwa.content.content_8}
                                            {permitData.constructionType && getCertificateConstructionType(permitData.constructionType)}
                                            {sansodhanInaruwa.content.content_9}
                                            <CompactDashedLangDate name="supStrucDate" setFieldValue={setFieldValue} error={errors.supStrucDate} value={values.supStrucDate} />
                                            {sansodhanInaruwa.content.content_10}
                                            <Select
                                                options={gharOption}
                                                name="ghar"
                                                onChange={(e, { value }) => setFieldValue('ghar', value)}
                                                value={values['ghar']}
                                                error={errors.ghar}
                                            />
                                            {sansodhanInaruwa.content.content_11}
                                            <DashedLangInput
                                                name="subName"
                                                value={values.subName}
                                                setFieldValue={setFieldValue}
                                                error={errors.subName}
                                            />
                                            {sansodhanInaruwa.content.content_12}
                                            <DashedNormalInput
                                                name="dasturAmount"
                                                value={values.dasturAmount}
                                                handleChange={handleChange}
                                                error={errors.dasturAmount}
                                            />
                                            {sansodhanInaruwa.content.content_13}
                                            {NepaliNumberToWord.getNepaliWord(values.dasturAmount)}
                                            {sansodhanInaruwa.content.content_14}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <SaveButtonValidation
                                errors={errors}
                                validateForm={validateForm}
                                formUrl={formUrl}
                                hasSavePermission={hasSavePermission}
                                hasDeletePermission={hasDeletePermission}
                                isSaveDisabled={isSaveDisabled}
                                prevData={checkError(prevData)}
                                handleSubmit={handleSubmit}
                            />
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}
const SansodhanSuperTippaniInaruwa = parentProps => (
    <FormContainerV2
        api={[
            { api: api.sansodhanSuperTippaniAdesh, objName: 'sansodhanSuperTippani', form: true },
            {
                api: api.superStructureConstruction,
                objName: 'supStruCons',
                form: false,
            },
        ]}
        onBeforeGetContent={{
            ...PrintParams.INLINE_FIELD,
            param4: ['getElementsByTagName', 'textarea', 'value'],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
            param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
            param7: ['getElementsByClassName', 'ui checkbox', 'value'],
        }}
        prepareData={data => data}
        parentProps={parentProps}
        useInnerRef={true}
        render={props => <SansodhanTippaniInaruwa {...props} parentProps={parentProps} />}
    />
);
export default SansodhanSuperTippaniInaruwa;

