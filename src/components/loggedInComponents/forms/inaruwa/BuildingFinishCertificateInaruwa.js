import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { CompactDashedLangDate } from '../../../shared/DateField';
import * as Yup from 'yup';
import { DashedLangInput, CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { BuildingFinishCertificateData } from '../../../../utils/data/BuildingFinishCertificateData';
import ErrorDisplay from './../../../shared/ErrorDisplay';
import api from '../../../../utils/api';
import { isStringEmpty } from './../../../../utils/stringUtils';
import { getCurrentDate } from './../../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../../utils/dataUtils';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { DashedLengthInputWithRelatedUnits } from '../../../shared/EbpsUnitLabelValue';
import merge from 'lodash/merge';
import { translateEngToNep } from '../../../../utils/langUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { PuranoGharCertificateSubHeadingWithoutPhoto } from '../certificateComponents/PuranoGharComponents';
import { TextSize } from '../../../../utils/constants/formComponentConstants';
import { PrintParams } from '../../../../utils/printUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { ShreeDropdown } from './../formComponents/ShreeDropdown';
import { shreeOptions } from '../../../../utils/optionUtils';
import EbpsTextareaField from './../../../shared/MyTextArea';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { CertificateFloorBlockBorderlessTable, HeightFloorNumberSection } from '../certificateComponents/CertificateFloorBlockInputs';

const naaya = BuildingFinishCertificateData.naya;
const inaruwaData = BuildingFinishCertificateData.inaruwa;

const convertbib = (inputIndex) => {
	if (inputIndex === 'A') {
		return '(क)';
	} else if (inputIndex === 'B') {
		return '(ख)';
	} else if (inputIndex === 'C') {
		return '(ग)';
	} else {
		return '(घ)';
	}
};
const puranoGharNirmanSchema = Yup.object().shape(
	Object.assign({
		Bdate: validateNullableNepaliDate,
		patraDate: validateNullableNepaliDate,
		paasDate: validateNullableNepaliDate,
		talaSwikriti: validateNullableNepaliDate,
	})
);
class BuildingInaruwa extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;
		let initialValues = {};
		const prabhidik = getJsonData(otherData.prabhidik);
		const mapTech = getJsonData(otherData.mapTech);
		const noticeFifteen = getJsonData(otherData.noticeFifteen);
		const anuSucKaJsonData = otherData.anukaMaster;
		const desApprovJsonData = otherData.designApprovalData;
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);
		let inputIndex = buildingClass ? buildingClass.id : '';

		initialValues.sadakUnit = DEFAULT_UNIT_LENGTH;
		initialValues.floorUnit = DEFAULT_UNIT_LENGTH;
		initialValues.buildingAreaUnit = DEFAULT_UNIT_AREA;
		initialValues.highTensionLineUnit = DEFAULT_UNIT_LENGTH;
		initialValues.publicPropertyUnit = DEFAULT_UNIT_LENGTH;
		initialValues.sadakAdhikarUnit = DEFAULT_UNIT_LENGTH;
		initialValues.highTensionUnit = DEFAULT_UNIT_LENGTH;
		initialValues.nirmanDate = translateEngToNep(permitData.applicantDateBS);

		// const floorArray = new FloorArray(permitData.floor);
		// const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		// const floorMax = topFloor.nepaliCount;
		// const highNepaliFloor = topFloor.nepaliName;
		// const bottomNepaliFloor = bottomFloor.nepaliName;
		const floorArray = new FloorBlockArray(permitData.floor);
		const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();
		const formattedFloors = floorArray.getFormattedFloors();

		const initialValues1 = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['photo', 'dhalNikasArrangement'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue('nepaliCount', 'floorNumber', topFloor),
					...floorArray.getInitialValue('nepaliName', 'startFloor', bottomFloor),
					...floorArray.getInitialValue('nepaliName', 'endFloor', topFloor),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: [
					'allowable',
					'coveragePercent',
					'purposeOfConstruction',
					'roof',
					'buildingHeight',
					'sadakAdhikarKshytra0',
					'requiredDistance0',
					'sadakAdhikarUnit',
					'highTensionUnit',
					'highTension',
				],
			},
			{
				obj: prabhidik,
				reqFields: ['publicPropertyRequiredDistance', 'publicPropertyUnit', 'publicPropertyDistance'],
			},
			// { obj: { startFloor: bottomNepaliFloor }, reqFields: [] },
			// { obj: { endFloor: highNepaliFloor }, reqFields: [] },
			// { obj: { floorNumber: floorMax }, reqFields: [] },
			// { obj: { buildingArea: floorArray.getSumOfAreas() }, reqFields: [] },
			{
				obj: {
					buildclass: inputIndex,
					shree: shreeOptions[0].value,
				},
				reqFields: [],
			},
			{ obj: noticeFifteen, reqFields: ['surrounding'] },
			{ obj: getJsonData(prevData), reqFields: [] }
		);
		if (isStringEmpty(initialValues1.Bdate)) {
			initialValues1.Bdate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues1.patraDate)) {
			initialValues1.patraDate = getCurrentDate(true);
		}

		if (isStringEmpty(initialValues1.paasDate)) {
			initialValues1.paasDate = getCurrentDate(true);
		}
		if (isStringEmpty(initialValues1.talaSwikriti)) {
			initialValues1.talaSwikriti = getCurrentDate(true);
		}

		const initVal = merge(initialValues, initialValues1);
		this.state = {
			initVal,
			floorArray,
			formattedFloors,
			blocks: floorArray.getBlocks(),
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initVal, floorArray, formattedFloors, blocks } = this.state;

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					validationSchema={puranoGharNirmanSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);
						values.applicationNo = this.props.permitData.applicantNo;
						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex compact={true} userInfo={userData} />
									<PuranoGharCertificateSubHeadingWithoutPhoto
										setFieldValue={setFieldValue}
										values={values}
										errors={errors}
										Detail={inaruwaData.buildingTitle}
										textSize={TextSize.MEDIUM}
									/>
									<br />
									<div>
										<div>
											<ShreeDropdown name="shree" /> {permitData.applicantName}
											{naaya.le}
											{userData.organization.name}
											{naaya.wadano}
											{permitData.newWardNo}
											{inaruwaData.content.content_1}
										</div>
										<div>
											{inaruwaData.content.content_3}
											{permitData.applicantName}
										</div>
										<div>
											{inaruwaData.content.content_4}
											{permitData.nibedakName}
										</div>
										<div>
											{naaya.data3}
											{naaya.data4.d1}
											{permitData.oldMunicipal}
											{naaya.data4.d2}
											{permitData.oldWardNo}
											{naaya.data4.d3}
											<CompactDashedLangInput name="houseNo" setFieldValue={setFieldValue} value={values.houseNo} />
											{naaya.data4.d4}
											{permitData.buildingJoinRoad}
											{naaya.data4.d5}
											{permitData.kittaNo}
											{naaya.data4.d6}
											{permitData.landArea} {permitData.landAreaType}
											{naaya.data4.d7}
											<DashedLangInput
												name="purposeOfConstruction"
												setFieldValue={setFieldValue}
												value={values.purposeOfConstruction}
											/>
										</div>
										<div>
											<div
												style={{
													display: 'flex',
												}}
											>
												<div>{naaya.data5.e1}</div>
												<div style={{ display: 'flex' }}>
													{values.surrounding &&
														values.surrounding.map((index, i) => (
															<div style={{ marginLeft: '5px' }}>
																{surrounding.find((fl) => fl.direction === index.side).value}{' '}
																<DashedLangInput
																	name={`surrounding.${i}.sandhiyar`}
																	setFieldValue={setFieldValue}
																	value={getIn(values, `surrounding.${i}.sandhiyar`)}
																	handleChange={handleChange}
																	error={getIn(errors, `surrounding.${i}.sandhiyar`)}
																/>{' '}
															</div>
														))}
												</div>
											</div>
										</div>

										<div>
											{naaya.data6.f1}
											{naaya.data6.input_1.map((input, idx) => (
												<div className="ui radio checkbox prabidhik" key={idx}>
													<input
														type="radio"
														name="buildclass"
														defaultChecked={values.buildclass === input}
														value={input}
													/>
													<label>{convertbib(input)}</label>
												</div>
											))}
											&emsp;
											{naaya.data7.g1}
											<DashedLangInput name="roof" setFieldValue={setFieldValue} value={values.roof} />
										</div>

										<div className="no-margin-field">
											{inaruwaData.content.content_5}
											{permitData.applicationNo} &emsp;
											{inaruwaData.content.content_6}
											<CompactDashedLangDate
												name="patraDate"
												setFieldValue={setFieldValue}
												error={errors.patraDate}
												value={values.patraDate}
											/>
											<br />
											<span style={{ marginLeft: '20px' }}>
												{inaruwaData.content.content_7}
												<CompactDashedLangDate
													name="paasDate"
													setFieldValue={setFieldValue}
													error={errors.paasDate}
													value={values.paasDate}
												/>
											</span>
										</div>
										<div className="no-margin-field">
											{inaruwaData.content.content_8}
											<CompactDashedLangDate
												name="talaSwikriti"
												setFieldValue={setFieldValue}
												error={errors.talaSwikriti}
												value={values.talaSwikriti}
											/>

											<div className="div-indent">
												<CertificateFloorBlockBorderlessTable
													formattedFloors={formattedFloors}
													floorArray={floorArray}
													data={{ listHeader: '' }}
													showAreaColumn={true}
													showFloorRange={true}
												/>
											</div>
										</div>
										<div>
											{naaya.data9.j2}
											<DashedLangInput name="coveragePercent" setFieldValue={setFieldValue} value={values.coveragePercent} />
											{naaya.data9.j3}
											<DashedLangInput name="allowable" setFieldValue={setFieldValue} value={values.allowable} />
										</div>
										<HeightFloorNumberSection floorArray={floorArray} blocks={blocks} />
										<div>
											<DashedLengthInputWithRelatedUnits
												name="requiredDistance0"
												unitName="sadakAdhikarUnit"
												relatedFields={['sadakAdhikarKshytra0']}
												label={naaya.data9.j6}
											/>
											<DashedLengthInputWithRelatedUnits
												name="sadakAdhikarKshytra0"
												unitName="sadakAdhikarUnit"
												relatedFields={['requiredDistance0']}
												label={naaya.data9.j7}
											/>
										</div>

										<div>
											<DashedLengthInputWithRelatedUnits
												name="highTension.0.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.1.value']}
												label={naaya.data9.j8}
											/>
											<DashedLengthInputWithRelatedUnits
												name="highTension.1.value"
												unitName="highTensionUnit"
												relatedFields={['highTension.0.value']}
												label={naaya.data9.j7}
											/>
										</div>
										<div>
											<DashedLengthInputWithRelatedUnits
												name="publicPropertyRequiredDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyDistance']}
												label={naaya.data9.j9}
											/>
											<DashedLengthInputWithRelatedUnits
												name="publicPropertyDistance"
												unitName="publicPropertyUnit"
												relatedFields={['publicPropertyRequiredDistance']}
												label={naaya.data9.j7}
											/>
										</div>
										<div className="no-margin-field">
											{naaya.data9.j10}
											<DashedLangInput
												name="dhalNikasArrangement"
												setFieldValue={setFieldValue}
												value={values.dhalNikasArrangement}
											/>
										</div>
										<div className="no-margin-field">
											{inaruwaData.content.content_10}
											<EbpsTextareaField
												placeholder="....."
												name="bibaran"
												setFieldValue={setFieldValue}
												value={values.bibaran}
												error={errors.bibaran}
											/>
										</div>
										<FooterSignatureMultiline designations={inaruwaData.content.content_16} />
										<br />
										{inaruwaData.content.content_15}
										<span className="ui input signature-placeholder" />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishCertificateInaruwa = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.buildingFinish, objName: 'buildingFinish', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabhidik',
				form: false,
			},

			{
				api: api.designApproval,
				objName: 'designApprovalData',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: false,
			},
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingInaruwa {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishCertificateInaruwa;
