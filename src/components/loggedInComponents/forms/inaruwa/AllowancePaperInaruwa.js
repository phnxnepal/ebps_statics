import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';

import api from '../../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { AllowancePaperInaruwaSchema, InaruwaSubject, AllowanceFooterInaruwa } from '../ijajatPatraComponents/AllowancePaperComponents';
import { PrintIdentifiers, PrintParams } from '../../../../utils/printUtils';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../../shared/SaveButtonValidation';
import { AllowancePaperBodyInaruwa, AllowancePaperInaruwaFloorTable } from './../ijajatPatraComponents/AllowancePaperComponents';
import { getApproveByObject } from '../../../../utils/formUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import { footerSignature } from '../../../../utils/data/genericFormData';

class AllowanceInaruwa extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } = this.props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const constructionType = permitData.constructionType;
		const rajasowData = checkError(otherData.rajaswoVoucher);
		const mapTechData = getJsonData(otherData.mapTech);
		const prabhidhikData = getJsonData(otherData.prabidhikPratibedan);
		const jsonData = getJsonData(prevData);
		let inaruwaData = {
			heightUnit: (mapTechData.floor && mapTechData.floor[0].floorUnit) || DEFAULT_UNIT_LENGTH,
			areaUnit: prabhidhikData.plinthDetailsUnit || DEFAULT_UNIT_AREA,
			lineUnit: prabhidhikData.highTensionLineUnit || DEFAULT_UNIT_LENGTH,
			nadiUnit: prabhidhikData.publicPropertyUnit || DEFAULT_UNIT_LENGTH,
			setBackUnit: prabhidhikData.sadakAdhikarUnit || DEFAULT_UNIT_LENGTH,
		};
		if (constructionType) {
			if (getConstructionTypeValue(constructionType) === ConstructionTypeValue.PURANO_GHAR) {
				inaruwaData.sabikGround = mapTechData && mapTechData.allowable;
				inaruwaData.oldHeight = mapTechData && mapTechData.buildingHeight;
				inaruwaData.oldLine = prabhidhikData.isHighTensionLineDistance;
				inaruwaData.oldArea = prabhidhikData.floor && prabhidhikData.floor.area;
				inaruwaData.oldNadi = prabhidhikData.publicPropertyDistance;
				inaruwaData.oldSetBack = prabhidhikData.requiredDistance;
			}
			if (getConstructionTypeValue(constructionType) === ConstructionTypeValue.NAYA_NIRMAN) {
				inaruwaData.haalGround = mapTechData && mapTechData.allowable;
				inaruwaData.newHeight = mapTechData && mapTechData.buildingHeight;
				inaruwaData.newLine = prabhidhikData.isHighTensionLineDistance;
				inaruwaData.newArea = prabhidhikData.floor && prabhidhikData.floor.area;
				inaruwaData.newNadi = prabhidhikData.publicPropertyDistance;
				inaruwaData.newSetBack = prabhidhikData.requiredDistance;
			}
		}
		let initialValues = prepareMultiInitialValues(
			{
				obj: rajasowData,
				reqFields: ['rashidNumber', 'amtOfDharauti'],
			},
			{
				obj: prabhidhikData,
				reqFields: ['publicPropertyRequiredDistance'],
			},
			{
				obj: inaruwaData,
				reqFields: [],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);
		this.state = {
			initialValues,
		};
	}
	render() {
		const { userData, permitData, prevData, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, useSignatureImage } = this.props;
		const { initialValues } = this.state;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={AllowancePaperInaruwaSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.allowancePaper, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ handleChange, values, errors, handleSubmit, isSubmitting, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef} className="build-gaps print-small-font">
								<div>
									<LetterHeadFlex userInfo={userData} />
									<InaruwaSubject />
									<div>
										<AllowancePaperBodyInaruwa
											values={values}
											errors={errors}
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											permitData={permitData}
											userData={userData}
										/>
										<AllowancePaperInaruwaFloorTable setFieldValue={setFieldValue} values={values} errors={errors} />
										<FooterSignatureMultiline
											designations={[[footerSignature.ser], [footerSignature.er], [footerSignature.chief]]}
											signatureImages={
												useSignatureImage && [values.sipharisSignature, values.swikritSignature, values.pramukhSignature]
											}
										/>
									</div>
									<AllowanceFooterInaruwa />
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const AllowancePaperInaruwa = (parentProps) => (
	<FormContainerV2
		api={[
			{ api: api.allowancePaper, objName: 'allowancePaper', form: true },
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.prabhidikPratibedhanPesh,
				objName: 'prabidhikPratibedan',
				form: false,
			},
			{
				api: api.rajaswaVoucher,
				objName: 'rajaswoVoucher',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ["getElementsByTagName", "input", "value"],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param9: [PrintIdentifiers.CHECKBOX_LABEL],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
		}}
		render={(props) => <AllowanceInaruwa {...props} parentProps={parentProps} />}
	/>
);

export default AllowancePaperInaruwa;
