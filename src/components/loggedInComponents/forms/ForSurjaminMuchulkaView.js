import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Dropdown, Label, Icon, Message } from 'semantic-ui-react';
import { SurjaminMuchulkaData } from './../../../utils/data/SurjaminMuchulkaData';

import { DashedLangDateField, CompactDashedLangDate } from './../../shared/DateField';
import { TimeField } from './../../shared/TimeField';
import api from '../../../utils/api';
import { getCurrentDate, getDateCount, getCurrentTime } from '../../../utils/dateUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import ErrorDisplay from '../../shared/ErrorDisplay';
import SurjaminValidateSchema from '../formValidationSchemas/ForJSurjaminMuchulkaValidaion';
// import SaveButton from './../../shared/SaveButton';
import { checkError, getJsonData, handleSuccess, prepareMultiInitialValues } from './../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { translateEngToNep } from '../../../utils/langUtils';
import { getDataConstructionTypeAll } from '../../../utils/enums/constructionType';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { SectionHeader } from '../../uiComponents/Headers';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { PrintParams } from '../../../utils/printUtils';
import { isMechi, isBirtamod } from '../../../utils/clientUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';

let SMData = SurjaminMuchulkaData.structureDesign;

const wardPostOpt = Object.values(SMData.bottom_info1_options).map((option, index) => ({ key: index, value: option, text: option }));

class SurjaminMuchulkaViewComponent extends Component {
	render() {
		const { getSaveButtonProps } = this.props;
		const detailed_Info = this.props.permitData;
		const user_info = this.props.userData;

		const noticePaymentApp = checkError(this.props.otherData.noticePaymentApplication);

		const tillDate = noticePaymentApp.noticeTillDate;

		/**
		 * @TODO confirm if i day needs to be substracted.
		 */
		const daysRemaining = getDateCount(tillDate) - 1;

		let initialValues = prepareMultiInitialValues(
			{
				obj: {
					surjaminDate: getCurrentDate(true),
					surjMuchulkaDate: getCurrentDate(true),
					surjaminTime: getCurrentTime(true),
					wardOfficerPost: SMData.bottom_info1_options.option1,
				},
				reqFields: [],
			},
			{ obj: getJsonData(this.props.prevData), reqFields: [] }
		);

		// if (this.props.prevData && this.props.prevData.jsonData) {
		// 	initialValues = JSON.parse(this.props.prevData.jsonData);
		// }

		// if (isStringEmpty(initialValues.surjMuchulkaDate)) {
		// 	initialValues.surjMuchulkaDate = getCurrentDate(true);
		// }

		// if (isStringEmpty(initialValues.surjaminDate)) {
		// 	initialValues.surjaminDate = getCurrentDate(true);
		// }

		// if (isStringEmpty(initialValues.wardOfficerPost)) {
		// 	initialValues.wardOfficerPost = SMData.bottom_info1_options.option1;
		// }

		return (
			<div>
				<Formik
					initialValues={initialValues}
					validationSchema={SurjaminValidateSchema}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = this.props.permitData.applicantNo;

						try {
							await this.props.postAction(api.surjaminMuchulka, values, true);

							window.scroll(0, 0);

							if (this.props.success && this.props.success.success) {
								// showToast(
								//     this.props.success.data.message ||
								//     'Data saved successfully'
								// );

								// setTimeout(() => {
								//     this.props.parentProps.history.push(
								//         getNextUrl(
								//             this.props.parentProps.location
								//                 .pathname
								//         )
								//     );
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ validateForm, isSubmitting, handleSubmit, setFieldValue, values, errors }) => {
						// console.log('values -- ', values, errors);
						return (
							<Form
								// onChange={handleSubmit}
								loading={isSubmitting}
							>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								{daysRemaining > 0 && (
									<div>
										<Message icon info size="big">
											<Icon name="clock outline" />
											<Message.Content>
												<Message.Header>{SMData.message.title}</Message.Header>
												{SMData.message.message_1st}
												{translateEngToNep(daysRemaining)}
												{SMData.message.message_2nd}
											</Message.Content>
										</Message>
									</div>
								)}
								<div ref={this.props.setRef} className="build-gap">
									<fieldset className="borderlessFieldset" disabled={daysRemaining > 0}>
										<LetterHeadFlex userInfo={user_info} />
										<div className="flex-item-space-between">
											<div>
												<div>
													{SMData.heading}
													<DashedLangInput
														name="aminName"
														value={values.aminName}
														setFieldValue={setFieldValue}
														error={errors.aminName}
													/>
												</div>
												<div>
													{SMData.heading}
													<DashedLangInput
														name="prashashanName"
														value={values.prashashanName}
														setFieldValue={setFieldValue}
														error={errors.prashashanName}
													/>
												</div>
											</div>
											<div>
												<CompactDashedLangDate
													label={SMData.date}
													name="surjMuchulkaDate"
													value={values.surjMuchulkaDate}
													error={errors.surjMuchulkaDate}
													setFieldValue={setFieldValue}
												/>
											</div>
										</div>
										<br />
										<LetterSalutation
											lines={[
												SMData.subheading,
												user_info.organization.name,
												user_info.organization.officeName,
												`${user_info.organization.address}, ${user_info.organization.province}`,
											]}
										/>

										<br />
										<SectionHeader>
											<h3 className="end-section underline">{SMData.title}</h3>
										</SectionHeader>
										<div className="no-margin-field">
											{SMData.main1} <span>{user_info.organization.name}</span> {SMData.main8}
											<span> {detailed_Info.nibedakTol} </span>
											{SMData.main2} <span> {detailed_Info.nibedakName} </span>
											{SMData.main2_1} {detailed_Info.oldMunicipal} {SMData.main8}
											{` ${detailed_Info.oldWardNo} ${SMData.main3} 
                                    ${getDataConstructionTypeAll(detailed_Info.constructionType)} ${SMData.main4} 
                                    ${SMData.main5} `}
											<DashedLangDateField
												name="surjaminDate"
												value={values.surjaminDate}
												error={errors.surjaminDate}
												setFieldValue={setFieldValue}
												inline={true}
											/>
											{` ${SMData.main6} `}
											<TimeField
												name="surjaminTime"
												value={values.surjaminTime}
												setFieldValue={setFieldValue}
												error={errors.surjaminTime}
												inline={true}
											/>
											{SMData.main7}
										</div>
										<div style={{ float: 'right', textAlign: 'center', marginTop: '150px' }}>
											<span className="ui input dashedForm-control" style={{ margin: 0 }}></span>
											<br />
											{user_info.organization.name}
										</div>

										<div style={{ clear: 'right' }}>
											<p>{SMData.bottom_info}</p>
											<p>
												{SMData.bottom_info1}{' '}
												<Dropdown
													options={wardPostOpt}
													name="wardOfficerPost"
													onChange={(e, { value }) => {
														setFieldValue('wardOfficerPost', value);
													}}
													value={values.wardOfficerPost}
												/>
												{errors.wardOfficerPost && (
													<Label pointing="left" prompt size="small" basic color="red">
														{errors.wardOfficerPost}
													</Label>
												)}{' '}
												{SMData.bottom_info2}
											</p>
											<p>
												{`${SMData.bottom_info3_1}`}
												<DashedLangInput
													name="wardOfficerName"
													value={values.wardOfficerName}
													setFieldValue={setFieldValue}
													error={errors.wardOfficerName}
												/>
												<span> {`${user_info.organization.name} ${SMData.bottom_info3_2} ${detailed_Info.newWardNo}`}</span>{' '}
											</p>
											{isMechi || isBirtamod ? null : <p>{SMData.bottom_info4}</p>}
										</div>
										<br />
										<div style={{ clear: 'right' }}>
											<p>
												{SMData.ghardhani_name}
												{detailed_Info.nibedakName}
											</p>

											{isMechi || isBirtamod ? <p>{SMData.bottom_info4}</p> : null}
										</div>
										<br />
										<div>
											<p>{SMData.botton_info5}</p>
											<p>
												{SMData.bottom_info6}{' '}
												<DashedLangInput
													name="OfficerName"
													value={values.OfficerName}
													setFieldValue={setFieldValue}
													error={errors.OfficerName}
												/>
											</p>
											<p>
												{SMData.bottom_info7}{' '}
												<DashedLangInput
													name="OfficerDesig"
													value={values.OfficerDesig}
													setFieldValue={setFieldValue}
													error={errors.OfficerDesig}
												/>
											</p>
											<p>
												{SMData.bottom_info8}{' '}
												<DashedLangInput
													name="wardOfficerNumber"
													value={values.wardOfficerNumber}
													setFieldValue={setFieldValue}
													error={errors.wardOfficerNumber}
												/>
											</p>
										</div>
									</fieldset>
								</div>

								{/* <SaveButton
									errors={errors}
									formUrl={this.props.parentProps.location.pathname}
									hasSavePermission={this.props.hasSavePermission}
									prevData={checkError(this.props.prevData)}
									handleSubmit={handleSubmit}
								/> */}

								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									handleSubmit={handleSubmit}
									{...getSaveButtonProps()}
								/>
								{/* {this.props.hasSavePermission && (
                                    <Button
                                        primary
                                        type='submit'
                                        onClick={handleSubmit}
                                    >
                                        Save
                                    </Button>
                                )} */}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const SurjaminMuchulkaView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.surjaminMuchulka,
				objName: 'surjaminMuchulka',
				form: true,
			},
			{
				api: api.noticePaymentApplication,
				objName: 'noticePaymentApplication',
				form: false,
			},
		]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SurjaminMuchulkaViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SurjaminMuchulkaView;
