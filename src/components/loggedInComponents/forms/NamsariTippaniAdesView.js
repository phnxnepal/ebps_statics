import React, { Component } from 'react';
import { Divider, Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import api from '../../../utils/api';
import { getJsonData, handleSuccess, checkError, prepareMultiInitialValues } from '../../../utils/dataUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { EbpsTextArea } from '../../shared/EbpsForm';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { NamsariStructure } from '../../../utils/data/namsariData';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';

const namsari = NamsariStructure.structureDesign;

class NamsariTippaniAdesViewComponent extends Component {
	constructor(props) {
		super(props);
		const { prevData } = this.props;
		const initialValues = prepareMultiInitialValues({
			obj: getJsonData(prevData),
			reqFields: [],
		});

		this.state = {
			initialValues: initialValues,
		};
	}
	render() {
		const { userData, permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;

		return (
			<>
				<Formik
					initialValues={this.state.initialValues}
					onSubmit={async (values, { setSubmitting }) => {
						values.applicationNo = permitData.applicantNo;

						try {
							await this.props.postAction(`${api.namsariTippani}${permitData.nameTransaferId}`, values);

							window.scroll(0, 0);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(prevData), this.props.parentProps, this.props.success);
								setSubmitting(false);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, isSubmitting, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />
								<div className="section-header">
									<h2 className="underline">{namsari.tip}</h2>
									<br />
									<Divider horizontal>{namsari.topic}</Divider>
								</div>
								<EbpsTextArea placeholder="निवेदन" name="nibedhan" setFieldValue={setFieldValue} value={values.nibedhan} />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
							{/* <SaveButton
								errors={errors}
								formUrl={this.props.parentProps.location.pathname}
								hasSavePermission={this.props.hasSavePermission}
								prevData={getJsonData(prevData)}
								handleSubmit={handleSubmit}
							/> */}
						</Form>
					)}
				/>
			</>
		);
	}
}

const NamsariTippaniAdesView = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.namsariTippani,
				objName: 'namsariTippani',
				form: true,
			},
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'textarea', 'value'],
		}}
		prepareData={data => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <NamsariTippaniAdesViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default NamsariTippaniAdesView;
