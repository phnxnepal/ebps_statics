import React from 'react';
import api from '../../../utils/api';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { PrintParams } from '../../../utils/printUtils';
import { GenericDesignerSansodhan } from './formComponents/GenericDesignerSansodhan';
import { designerSansodhan } from '../../../utils/data/genericFormData';

const SuperStructureKoSansodhan = parentProps => (
	<FormContainerV2
		api={[
			{
				api: api.superstructurekoTippaniadesh,
				objName: 'superstructurekoTippaniadesh',
				form: true,
			},
		]}
		prepareData={data => data}
		parentProps={parentProps}
		onBeforeGetContent={PrintParams.TEXT_AREA}
		hasFile={true}
		useInnerRef={true}
		render={props => (
			<GenericDesignerSansodhan
				fieldName="nibedan"
				topic={designerSansodhan.sansodhanBibaranSuperStructure.topic}
				api={api.superstructurekoTippaniadesh}
				{...props}
				parentProps={parentProps}
			/>
		)}
	/>
);

export default SuperStructureKoSansodhan;
