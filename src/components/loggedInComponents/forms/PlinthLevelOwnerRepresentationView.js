import React, { Component } from 'react';
import { Formik, Field } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import { PlinthLvlOwnerRepresentation } from '../../../utils/data/mockLangFile';
import { showToast, getFileCatId, getUserRole, isEmpty } from '../../../utils/functionUtils';
import { getMessage } from '../../shared/getMessage';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getCurrentDate } from '../../../utils/dateUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import EbpsTextareaField from '../../shared/MyTextArea';
import * as Yup from 'yup';
import { validateNormalNumber } from '../../../utils/validationUtils';
import { DashedLangInputWithSlash } from './../../shared/DashedFormInput';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../utils/enums/constructionType';
import { UserType } from '../../../utils/userTypeUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../shared/SaveButtonValidation';
import { isInaruwa, isKamalamai } from '../../../utils/clientUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import { SansodhanCheckbox } from './formComponents/SansodhanCheckbox';
import { PrintParams } from '../../../utils/printUtils';
import { SignatureImage } from './formComponents/SignatureImage';

const plor_data = PlinthLvlOwnerRepresentation.PlinthLvlOwnerRepresentation_data;

const messageId = 'PlinthLvlOwnerRepresentation.PlinthLvlOwnerRepresentation_data';

const plinthSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: Yup.array().of(
				Yup.object().shape({
					feet: validateNormalNumber,
				})
			),
		}
		// ...formDataSchema,
		// ...numericalNepaliSchema
	)
);

class PlinthLevelOwnerRepresentationViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { fileCatId } = getFileCatId(this.props.otherData.fileCategories, this.props.parentProps.location.pathname);
		const { permitData, prevData, userData, otherData, hasDesignerChanged } = this.props;

		const json_data = getJsonData(prevData);
		const anuGhaData = checkError(otherData.anuGha);
		initVal = json_data;
		if (isStringEmpty(initVal.certificateInstDate)) {
			initVal.certificateInstDate = getCurrentDate(true);
		}

		initVal.prabidhikName = userData.designer ? userData.designer.userName : '';
		initVal.consultingNameStamp = userData.designer ? userData.designer.stamp : '';
		initVal.pramadPatraNo = userData.designer ? userData.designer.municipalRegNo : '';

		let mapSideObj = {};
		if (getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR || hasDesignerChanged) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					userName: userData.info.userName,
					educationQualification: userData.info.educationQualification,
					consultancyName: userData.info.consultancyName,
					licenseNo: userData.info.licenseNo,
					municipalRegNo: userData.info.municipalRegNo,
					stamp: userData.info.stamp,
					signature: userData.info.signature,
					enterDate: getCurrentDate(),
				};
			} else {
				mapSideObj = {};
			}
		} else {
			if (!isEmpty(anuGhaData)) {
				if (!isEmpty(anuGhaData.dName && anuGhaData.dDesignation && anuGhaData.dDate)) {
					mapSideObj = {
						userName: anuGhaData.dName,
						educationQualification: anuGhaData.dDesignation,
						enterDate: anuGhaData.dDate,
					};
				} else
					mapSideObj = {
						userName: anuGhaData.enterBy.userName,
						educationQualification: anuGhaData.enterBy.educationQualification,
						licenseNo: anuGhaData.enterBy.licenseNo,
						municipalRegNo: anuGhaData.enterBy.municipalRegNo,
						stamp: anuGhaData.enterBy.stamp,
						signature: anuGhaData.enterBy.signature,
						enterDate: anuGhaData.enterDate,
					};
			}
		}

		initVal = prepareMultiInitialValues(
			{
				obj: anuGhaData,
				reqFields: ['thekdarName', 'tAddress', 'tDarta', 'mistiriName', 'mDarta', 'mAddress'],
			},
			{
				obj: permitData,
				reqFields: ['applicantName'],
			},
			{ obj: mapSideObj, reqFields: [] },
			{ obj: json_data, reqFields: [] }
		);
		this.state = {
			initVal,
			fileCatId,
		};
	}
	render() {
		const { permitData, prevData, userData, errors: reduxErrors, formUrl, hasDeletePermission, isSaveDisabled, hasSavePermission, staticFiles } = this.props;
		const { initVal, fileCatId } = this.state;
		// const plinthData = ['thekdarName', 'tAddress', 'mistiriName', 'mAddress'];
		// const plinthNepaliNum = ['mDarta', 'tDarta']
		// const formDataSchema = plinthData.map(row => {
		//     return {
		//         [row]: validateString
		//     };
		// });

		// const numericalNepaliSchema = plinthNepaliNum.map(row => {
		//     return {
		//         [row]: validateNullableOfficialReqNumbers
		//     };
		// });

		return (
			<Formik
				initialValues={{
					mapdandaPalanaBhayeko: plor_data.radioBox[0],
					setBackPlanBhayeko: plor_data.radioBox[0],
					groundCoverageBhayeko: plor_data.radioBox[0],
					buildingConstFollowBhayeko: plor_data.radioBox[0],
					sanso: plor_data.check[0],
					engParisadDartNo: userData.designer ? userData.designer.licenseNo : '',
					...initVal,
				}}
				validationSchema={plinthSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;
					const data = new FormData();
					const selectedFile = values.uploadFile;
					// let isFileSelected = false;
					if (selectedFile) {
						for (var x = 0; x < selectedFile.length; x++) {
							data.append('file', selectedFile[x]);
						}
						data.append('fileType', fileCatId);
						data.append('applicationNo', permitData.applicantNo);
						// isFileSelected = true;
					}
					try {
						let url = '';
						if (values.sanso === plor_data.check[1]) {
							url = `${api.plinthLevelOwnerRepresentation}${permitData.applicantNo}/Y`;
						} else {
							url = `${api.plinthLevelOwnerRepresentation}${permitData.applicantNo}/N`;
						}

						await this.props.postAction(
							url,
							values,
							false,
							false,
							true // refetch the menu as the menu will be updated
						);

						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// // if (values.sanso === plor_data.check[1]) {
							// //     setLocalStorage(PLOR_SANSODHAN, 'Yes')
							// //     this.props.parentProps.history.push(
							// //         `/user${FormUrl.SANSODHAN_BIBARAN_PAHILO}`
							// //     );
							// // } else {
							// //     this.props.parentProps.history.push(
							// //         getNextUrl(this.props.parentProps.location.pathname)
							// //     );

							// // }
							// setTimeout(() => {
							//     this.props.parentProps.history.push(
							//         getNextUrl(this.props.parentProps.location.pathname)
							//     );
							// }, 1000);
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, handleChange, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							{isInaruwa ? (
								<div className="section-header margin-section">
									<h3 className="underline">{getMessage(`${messageId}.plor_heading2`, plor_data.plor_heading2)}</h3>
								</div>
							) : (
								<div className="section-header margin-section">
									<h3 className="underline">{getMessage(`${messageId}.plor_heading1`, plor_data.plor_heading1)}</h3>
									<h3 className="underline">{getMessage(`${messageId}.plor_heading2`, plor_data.plor_heading2)}</h3>
								</div>
							)}
							<FormComponent detailedInfo={permitData} />
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_1`, plor_data.plor_data_5.plor_data_5_1)}
								{plor_data.radioBox.map((name, index) => (
									<div key={index} className="ui radio checkbox">
										<Field
											type="radio"
											name="mapdandaPalanaBhayeko"
											id={index}
											defaultChecked={values.mapdandaPalanaBhayeko === name}
											value={name}
											onClick={handleChange}
										/>
										<label>{name}</label>
									</div>
								))}
								{values.mapdandaPalanaBhayeko === plor_data.radioBox[1] && (
									<EbpsTextareaField
										placeholder={plor_data.plor_else_placeholder}
										name="mapdandaPalana"
										setFieldValue={setFieldValue}
										value={values.mapdandaPalana}
										error={errors.mapdandaPalana}
									/>
								)}
							</div>
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_2`, plor_data.plor_data_5.plor_data_5_2)}
								{plor_data.radioBox.map((name, index) => (
									<div key={index} className="ui radio checkbox">
										<Field
											type="radio"
											name="setBackPlanBhayeko"
											id={index}
											defaultChecked={values.setBackPlanBhayeko === name}
											value={name}
											onClick={handleChange}
										/>
										<label>{name}</label>
									</div>
								))}
								{values.setBackPlanBhayeko === plor_data.radioBox[1] && (
									// <Field
									//     type="text"
									//     name="setBackPlan"
									//     className="dashedForm-control"
									// />

									<EbpsTextareaField
										placeholder={plor_data.plor_else_placeholder}
										name="setBackPlan"
										setFieldValue={setFieldValue}
										value={values.setBackPlan}
										error={errors.setBackPlan}
									/>
								)}
							</div>
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_3`, plor_data.plor_data_5.plor_data_5__3)}
								{plor_data.radioBox.map((name, index) => (
									<div key={index} className="ui radio checkbox">
										<Field
											type="radio"
											name="groundCoverageBhayeko"
											id={index}
											defaultChecked={values.groundCoverageBhayeko === name}
											value={name}
											onClick={handleChange}
										/>
										<label>{name}</label>
									</div>
								))}
								{values.groundCoverageBhayeko === plor_data.radioBox[1] && (
									// <Field
									//     type="text"
									//     name="groundCoverage"
									//     className="dashedForm-control"
									// />

									<EbpsTextareaField
										placeholder={plor_data.plor_else_placeholder}
										name="groundCoverage"
										setFieldValue={setFieldValue}
										value={values.groundCoverage}
										error={errors.groundCoverage}
									/>
								)}
							</div>
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_4`, plor_data.plor_data_5.plor_data_5_4)}
								{/* <Field
                                        type="text"
                                        name="changesMade"
                                        className="dashedForm-control"
                                    /> */}
								{/* <DashedLangInput
                                        name="changesMade"
                                        setFieldValue={setFieldValue}
                                        value={values.changesMade}
                                        error={errors.changesMade}
                                    /> */}
								<EbpsTextareaField
									placeholder="....."
									name="changesMade"
									setFieldValue={setFieldValue}
									value={values.changesMade}
									error={errors.changesMade}
								/>
							</div>
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_5`, plor_data.plor_data_5.plor_data_5_5)}
								{plor_data.radioBox.map((name, index) => (
									<div key={index} className="ui radio checkbox">
										<Field
											type="radio"
											name="buildingConstFollowBhayeko"
											id={index}
											defaultChecked={values.buildingConstFollowBhayeko === name}
											value={name}
											onClick={handleChange}
										/>
										<label>{name}</label>
									</div>
								))}
								{values.buildingConstFollowBhayeko === plor_data.radioBox[1] && (
									// <Field
									//     type="text"
									//     name="buildingConstFollow"
									//     className="dashedForm-control"
									// />

									<EbpsTextareaField
										placeholder={plor_data.plor_else_placeholder}
										name="buildingConstFollow"
										setFieldValue={setFieldValue}
										value={values.buildingConstFollow}
										error={errors.buildingConstFollow}
									/>
								)}
							</div>
							<div>
								{getMessage(`${messageId}.plor_data_5.plor_data_5_6`, plor_data.plor_data_5.plor_data_5_6)}
								{/* <Field
                                        type="text"
                                        name="buildingConstFollow"
                                        className="dashedForm-control"
                                    /> */}
								{/* <DashedLangInput
                                        name="buildingConstFollow1"
                                        setFieldValue={setFieldValue}
                                        value={values.buildingConstFollow1}
                                        error={errors.buildingConstFollow1}
                                    /> */}
								<EbpsTextareaField
									placeholder="....."
									name="buildingConstFollow1"
									setFieldValue={setFieldValue}
									value={values.buildingConstFollow1}
									error={errors.buildingConstFollow1}
								/>
							</div>
							<SansodhanCheckbox
								label={plor_data.checklabel}
								values={values}
								handleChange={handleChange}
								options={plor_data.check}
								constructionType={permitData.constructionType}
							/>
							<p>{getMessage(`${messageId}.plor_data_6`, plor_data.plor_data_6)}</p>
							<div className="inline field">
								<SignatureImage
									label={getMessage(`${messageId}.plor_data_7.plor_data_7_1`, plor_data.plor_data_7.plor_data_7_1)}
									value={initVal.signature}
									showSignature={true}
								/>
								<span>{getMessage(`${messageId}.plor_data_7.plor_data_7_2`, plor_data.plor_data_7.plor_data_7_2)}</span>

								{initVal.userName}
								<br />
								<span>{getMessage(`${messageId}.plor_data_7.plor_data_7_3`, plor_data.plor_data_7.plor_data_7_3)}</span>
								{initVal.licenseNo}
								{/* need to add */}
								{/* {udata.designer && udata.designer.licenseNo} */}
								<br />
								<SignatureImage
									label={getMessage(`${messageId}.plor_data_7.plor_data_7_4`, plor_data.plor_data_7.plor_data_7_4)}
									value={initVal.stamp}
									showSignature={true}
								/>

								<div>
									{userData.organization.name}{' '}
									{getMessage(`${messageId}.plor_data_7.plor_data_7_5`, plor_data.plor_data_7.plor_data_7_5)}
									{initVal.municipalRegNo}
								</div>
							</div>
							<p>{getMessage(`${messageId}.plor_data_7.plor_data_7_6`, plor_data.plor_data_7.plor_data_7_6)}</p>
							<br />
							{isInaruwa ? (
								<Grid columns={2}>
									<Grid.Row>
										<Grid.Column>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_5`,
														plor_data.plor_data_footer_left.plor_data_footer_left_5
													)}
												</span>
												<span className="ui input dashedForm-control"></span>
											</div>
											<h3>
												{getMessage(
													`${messageId}.plor_data_footer_left.plor_data_footer_left_1_1`,
													plor_data.plor_data_footer_left.plor_data_footer_left_1_1
												)}
											</h3>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_2`,
														plor_data.plor_data_footer_left.plor_data_footer_left_2
													)}
												</span>
												<div className="ui input">
													<DashedLangInput name="thekdarName" setFieldValue={setFieldValue} value={values.thekdarName} />
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_3`,
														plor_data.plor_data_footer_left.plor_data_footer_left_3
													)}
												</span>
												<div className="ui input">
													<DashedLangInput name="tAddress" setFieldValue={setFieldValue} value={values.tAddress} />
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_4`,
														plor_data.plor_data_footer_left.plor_data_footer_left_4
													)}
												</span>
												<div className="ui input">
													<DashedLangInputWithSlash name="tDarta" setFieldValue={setFieldValue} value={values.tDarta} />
												</div>
											</div>
										</Grid.Column>
										<Grid.Column>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_5`,
														plor_data.plor_data_footer_left.plor_data_footer_left_5
													)}
												</span>
												<span className="ui input dashedForm-control"></span>
											</div>
											<h3>
												{getMessage(
													`${messageId}.plor_data_footer_right.plor_data_footer_right_1_1`,
													plor_data.plor_data_footer_right.plor_data_footer_right_1_1
												)}
											</h3>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_2`,
														plor_data.plor_data_footer_right.plor_data_footer_right_2
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="dakarmiName"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="mistiriName"
														setFieldValue={setFieldValue}
														value={values.mistiriName}
														// error={errors.mistiriName}
													/>
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_3`,
														plor_data.plor_data_footer_right.plor_data_footer_right_3
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="dakarmiAddress"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="mAddress"
														setFieldValue={setFieldValue}
														value={values.mAddress}
														// error={errors.mAddress}
													/>
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_4`,
														plor_data.plor_data_footer_right.plor_data_footer_right_4
													)}
												</span>
												{/* <div className="ui input">
                                                {/* <Field
                                                    type="text"
                                                    name="dakarmiPramadPatraNo"
                                                    className="dashedForm-control"
                                                />     
                                            </div> */}
												{/* <DashedLangInputWithSlash
                                                    name="engParisadDartNo"
                                                    setFieldValue={setFieldValue}
                                                    value={values.engParisadDartNo}
                                                    error={errors.engParisadDartNo}
                                                    onChange={handleChange}
                                                /> */}
												<DashedLangInputWithSlash
													name="mDarta"
													setFieldValue={setFieldValue}
													value={values.mDarta}
													// error={errors.mDarta}
													onChange={handleChange}
												/>
												<br />
											</div>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							) : isKamalamai ? (
								<Grid columns={3} className="no-padding-grid">
									<Grid.Row>
										<Grid.Column>
											<SignatureImage 
												showSignature={true}	
												value={staticFiles.nirmanByabasayeSignature}	
											/>
											<SectionHeader>
												<h4 className="left-align">
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_1_1`,
														plor_data.plor_data_footer_left.plor_data_footer_left_1_1
													)}
												</h4>
											</SectionHeader>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_left.plor_data_footer_left_2`,
													plor_data.plor_data_footer_left.plor_data_footer_left_2
												)}
												<DashedLangInput name="thekdarName" setFieldValue={setFieldValue} value={values.thekdarName} />
											</div>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_left.plor_data_footer_left_3`,
													plor_data.plor_data_footer_left.plor_data_footer_left_3
												)}
												<DashedLangInput name="tAddress" setFieldValue={setFieldValue} value={values.tAddress} />
											</div>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_left.plor_data_footer_left_4`,
													plor_data.plor_data_footer_left.plor_data_footer_left_4
												)}
												<DashedLangInputWithSlash name="tDarta" setFieldValue={setFieldValue} value={values.tDarta} />
											</div>
										</Grid.Column>
										<Grid.Column>
											<SignatureImage 
												showSignature={true}	
												value={staticFiles.dakarmiSignature}	
											/>
											<SectionHeader>
												<h4 className="left-align">
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_1_1`,
														plor_data.plor_data_footer_right.plor_data_footer_right_1_1
													)}
												</h4>
											</SectionHeader>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_right.plor_data_footer_right_2`,
													plor_data.plor_data_footer_right.plor_data_footer_right_2
												)}
												<DashedLangInput
													name="mistiriName"
													setFieldValue={setFieldValue}
													value={values.mistiriName}
													error={errors.mistiriName}
												/>
											</div>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_right.plor_data_footer_right_3`,
													plor_data.plor_data_footer_right.plor_data_footer_right_3
												)}
												<DashedLangInput
													name="mAddress"
													setFieldValue={setFieldValue}
													value={values.mAddress}
													error={errors.mAddress}
												/>
											</div>
											<div>
												{getMessage(
													`${messageId}.plor_data_footer_right.plor_data_footer_right_4`,
													plor_data.plor_data_footer_right.plor_data_footer_right_4
												)}
												<DashedLangInputWithSlash
													name="mDarta"
													setFieldValue={setFieldValue}
													value={values.mDarta}
													error={errors.mDarta}
													onChange={handleChange}
												/>
											</div>
										</Grid.Column>
										<Grid.Column>
											<SignatureImage 
												showSignature={true}	
												value={staticFiles.ghardhaniSignature}	
											/>
											<SectionHeader>
												<h4 className="left-align">{plor_data.footer_gharDhani.gharDhani}</h4>
											</SectionHeader>
											<div>
												{plor_data.footer_gharDhani.name}
												<DashedLangInput
													name="applicantName"
													setFieldValue={setFieldValue}
													value={values.applicantName}
													error={errors.applicantName}
												/>
											</div>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							) : (
								<Grid columns={2}>
									<Grid.Row>
										<Grid.Column>
											<h3>
												{getMessage(
													`${messageId}.plor_data_footer_left.plor_data_footer_left_1`,
													plor_data.plor_data_footer_left.plor_data_footer_left_1
												)}
											</h3>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_2`,
														plor_data.plor_data_footer_left.plor_data_footer_left_2
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="contractorName"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="thekdarName"
														setFieldValue={setFieldValue}
														value={values.thekdarName}
														// error={errors.thekdarName}
													/>
													{/* {console.log("e", errors.thekdarName)}
                                                {errors.thekdarName && (
                                                    <Label
                                                        pointing="left"
                                                        prompt
                                                        size="large"
                                                        basic
                                                        color="red"
                                                    >
                                                        {errors.thekdarName}
                                                    </Label>
                                                )} */}
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_3`,
														plor_data.plor_data_footer_left.plor_data_footer_left_3
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="contractorAddress"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="tAddress"
														setFieldValue={setFieldValue}
														value={values.tAddress}
														// error={errors.tAddress}
													/>
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_left.plor_data_footer_left_4`,
														plor_data.plor_data_footer_left.plor_data_footer_left_4
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="contractorPramadPatraNo"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInputWithSlash
														name="tDarta"
														setFieldValue={setFieldValue}
														value={values.tDarta}
														// error={errors.tDarta}
													/>
												</div>
											</div>
										</Grid.Column>
										<Grid.Column>
											<h3>
												{getMessage(
													`${messageId}.plor_data_footer_right.plor_data_footer_right_1`,
													plor_data.plor_data_footer_right.plor_data_footer_right_1
												)}
											</h3>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_2`,
														plor_data.plor_data_footer_right.plor_data_footer_right_2
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="dakarmiName"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="mistiriName"
														setFieldValue={setFieldValue}
														value={values.mistiriName}
														// error={errors.mistiriName}
													/>
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_3`,
														plor_data.plor_data_footer_right.plor_data_footer_right_3
													)}
												</span>
												<div className="ui input">
													{/* <Field
                                                    type="text"
                                                    name="dakarmiAddress"
                                                    className="dashedForm-control"
                                                /> */}
													<DashedLangInput
														name="mAddress"
														setFieldValue={setFieldValue}
														value={values.mAddress}
														// error={errors.mAddress}
													/>
												</div>
											</div>
											<div className="inline field">
												<span>
													{getMessage(
														`${messageId}.plor_data_footer_right.plor_data_footer_right_4`,
														plor_data.plor_data_footer_right.plor_data_footer_right_4
													)}
												</span>
												{/* <div className="ui input">
                                                {/* <Field
                                                    type="text"
                                                    name="dakarmiPramadPatraNo"
                                                    className="dashedForm-control"
                                                />     
                                            </div> */}
												{/* <DashedLangInputWithSlash
                                                    name="engParisadDartNo"
                                                    setFieldValue={setFieldValue}
                                                    value={values.engParisadDartNo}
                                                    error={errors.engParisadDartNo}
                                                    onChange={handleChange}
                                                /> */}
												<DashedLangInputWithSlash
													name="mDarta"
													setFieldValue={setFieldValue}
													value={values.mDarta}
													// error={errors.mDarta}
													onChange={handleChange}
												/>
												<br />
											</div>
										</Grid.Column>
									</Grid.Row>
								</Grid>
							)}
						</div>
						<br />

						{/* {permitData.constructionType === ConstructionType.NAYA_NIRMAN && <div>

                                {plor_data.checklabel}{" "}
                                {plor_data.check.map((row, index) => (

                                    <div
                                        key={index}
                                        classrow="ui radio checkbox"
                                    >
                                        <Field
                                            type="radio"
                                            name="sanso"
                                            id={index}
                                            value={row}
                                            onClick={handleChange}
                                            defaultChecked={
                                                values.sanso ===
                                                row
                                            }
                                        />
                                        <label>{row}</label>
                                    </div>
                                ))}
                            </div>} */}

						{/* {values.sanso !==
                                    plor_data.check[0] && (
                                        <Segment>
                                            <Form.Field>
                                                <div style={{ textAlign: 'right' }}>
                                                    <DashedLangDateField
                                                        name="certificateInstDate"
                                                        setFieldValue={setFieldValue}
                                                        value={values.certificateInstDate}
                                                        label={plor_data.miti}
                                                    />
                                                </div>
                                            </Form.Field>
                                            <h2 style={{ textAlign: "center" }}>{plor_data.sansodhantitle}</h2><Header as="h3">
                                                <Divider horizontal>
                                                    {plor_data.nibedan}
                                                </Divider>
                                            </Header><EbpsTextareaField
                                                placeholder="..."
                                                name="sansodhanTextArea"
                                                setFieldValue={setFieldValue}
                                                value={values.sansodhanTextArea}
                                                error={errors.sansodhanTextArea}
                                            /><br /><br />
                                            <p>
                                                <Form.Field error={errors.uploadFile}>
                                                    <Input
                                                        type="file"
                                                        name="uploadFile"
                                                        id={fileCatId}
                                                        multiple={hasMultipleFiles}
                                                        onChange={e => {
                                                            setFieldValue(
                                                                'uploadFile',
                                                                Array.from(e.target.files)
                                                            );
                                                        }}
                                                    />
                                                    {errors.uploadFile && (
                                                        <Label pointing prompt size="large">
                                                            {errors.uploadFile}
                                                        </Label>
                                                    )}{' '}
                                                </Form.Field>
                                            </p>
                                        </Segment>
                                    )} */}

						<br />
						<br />
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>

						{/* <SaveButton
							errors={errors}
							formUrl={this.props.parentProps.location.pathname}
							hasSavePermission={this.props.hasSavePermission}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/> */}
						{/* {this.props.hasSavePermission && (
                                <Button
                                    primary
                                    type="submit"
                                    disabled={isSubmitting}
                                    onClick={handleSubmit}
                                >
                                    Save
                                </Button>
                            )} */}
					</Form>
				)}
			</Formik>
		);
	}
}

const FormComponent = (props) => {
	const DetailInfodata = props.detailedInfo;
	return (
		<p>
			{getMessage(`${messageId}.plor_data_1`, plor_data.plor_data_1)}
			<span>{DetailInfodata.applicantName}</span>
			{getMessage(`${messageId}.plor_data_2`, plor_data.plor_data_2)}
			<span>{DetailInfodata.newWardNo}</span>
			{getMessage(`${messageId}.plor_data_3`, plor_data.plor_data_3)}
			<span>{DetailInfodata.sadak}</span>
			{getMessage(`${messageId}.plor_data_4`, plor_data.plor_data_4)}
		</p>
	);
};

const PlinthLevelOwnerRepresentationView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: true,
			},
			{
				api: api.anusuchiGha,
				objName: 'anuGha',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		fetchFiles={true}
		onBeforeGetContent={{
			...PrintParams.REMOVE_ON_PRINT,
			param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		// hasFile={true}
		render={(props) => <PlinthLevelOwnerRepresentationViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default PlinthLevelOwnerRepresentationView;
