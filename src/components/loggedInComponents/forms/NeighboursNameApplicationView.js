import React, { Component } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Table, Select } from 'semantic-ui-react';
import { NeighboursNameApplicationData } from '../../../utils/data/NeighboursNameApplicationData';
// import { DashedLangInput } from '../../shared/DashedFormInput';
import { getUnitValue } from './../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { getJsonData, handleSuccess, distanceOptions } from '../../../utils/dataUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import EbpsForm, { EbpsNormalForm } from '../../shared/EbpsForm';
import { validateWardNo, validatePhoneNumber, validateString, validateMoreThanZeroNumber, validateNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { checkError, prepareMultiInitialValues } from './../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { checkClient, Client } from '../../../utils/clientUtils';
import { getConstructionTypeText } from '../../../utils/enums/constructionType';
import { FooterSignature, DetailedSignature } from './formComponents/FooterSignature';
import { PatraSankhyaAndDate } from './formComponents/PatraSankhyaAndDate';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SurroundingArray } from '../../../utils/surroundingUtils';
import { NumberLabelValue } from '../../shared/EbpsUnitLabelValue';
import { validateSurroundingNotice } from '../formValidationSchemas/buildPermitValidation';
import { isStringEmpty } from '../../../utils/stringUtils';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import { LocalAPI } from '../../../utils/urlUtils';
import { WARD_MASTER } from '../../../utils/constants';
import { DashedSelect } from '../../shared/Select';
import { DashedLangDateField } from '../../shared/DateField';
import { date } from '../../../utils/data/formCommonData';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { getApproveByObject, getSaveByUserDetails } from '../../../utils/formUtils';

const Ndata = NeighboursNameApplicationData.structureDesign;

const banauneOpt = [
	{ key: 1, value: 'बनाउने', text: 'बनाउने' },
	{ key: 2, value: 'बनाएको', text: 'बनाएको' },
];

const shreeOpt = [
	{ key: 1, value: 'श्री', text: 'श्री' },
	{ key: 2, value: 'श्रीमती', text: 'श्रीमती' },
];

const parchaOpt = [
	{ key: 1, value: 'पर्छ ?', text: 'पर्छ ?' },
	{ key: 2, value: 'पर्दैन ?', text: 'पर्दैन ?' },
];

// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];
const strData = ['banaune', 'shree', 'parcha'];
const neighbourNum = ['newWardNo'];
const neighbourPhoneNum = ['serNumber'];
const formDataSchema = neighbourNum.map((row) => {
	return {
		[row]: validateWardNo,
	};
});
const phoneNumberSchema = neighbourPhoneNum.map((row) => {
	return {
		[row]: validatePhoneNumber,
	};
});
const dropdownSchema = strData.map((row) => {
	return {
		[row]: validateString,
	};
});
const neighbourSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: validateSurroundingNotice,
			floor: Yup.array().of(
				Yup.object().shape({
					length: validateMoreThanZeroNumber,
					width: validateMoreThanZeroNumber,
					height: validateMoreThanZeroNumber,
				})
			),
			Sangyardate: validateNepaliDate,
		},
		...formDataSchema,
		...phoneNumberSchema,
		...dropdownSchema
	)
);

const noNumberSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: validateSurroundingNotice,
			floor: Yup.array().of(
				Yup.object().shape({
					length: validateMoreThanZeroNumber,
					width: validateMoreThanZeroNumber,
					height: validateMoreThanZeroNumber,
				})
			),
			Sangyardate: validateNepaliDate,
		},
		...formDataSchema,
		...dropdownSchema
	)
);

const notice15IllamSchema = Yup.object().shape(
	Object.assign(
		{
			surrounding: validateSurroundingNotice,
			floor: Yup.array().of(
				Yup.object().shape({
					length: validateMoreThanZeroNumber,
					width: validateMoreThanZeroNumber,
					height: validateMoreThanZeroNumber,
				})
			),
			Sangyardate: validateNepaliDate,
			serName: validateString,
			serDesignation: validateString,
		},
		...formDataSchema,
		...phoneNumberSchema,
		...dropdownSchema
	)
);

const kamalaiSchema = Yup.object().shape(
	Object.assign({
		surrounding: validateSurroundingNotice,
		floor: Yup.array().of(
			Yup.object().shape({
				length: validateMoreThanZeroNumber,
				width: validateMoreThanZeroNumber,
				height: validateMoreThanZeroNumber,
			})
		),
		Sangyardate: validateNepaliDate,
		subName: validateString,
		subDesignation: validateString,
	})
);

class NeighboursNameApplicationViewComponent extends Component {
	constructor(props) {
		super(props);

		const { prevData, userData, permitData, orgCode, localDataObject, enterByUser } = props;
		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const serData = getApproveBy(0);
		const erData = getApproveBy(1);
		const pramukhData = getApproveBy(2);

		const surrArray = new SurroundingArray(permitData.surrounding);
		const floorArray = new FloorBlockArray(permitData.floor);
		const completeSurroundingObject = { surrounding: surrArray.getAllSurrounding() };
		const jsonData = getJsonData(prevData);

		const wardOption = localDataObject.wardMaster && localDataObject.wardMaster.map((row) => ({ key: row.id, value: row.name, text: row.name }));
		let prevDataFormatted;
		try {
			if (jsonData.surrounding) {
				const prevSurrounding = new SurroundingArray(getJsonData(prevData).surrounding);
				prevDataFormatted = { ...getJsonData(prevData), surrounding: prevSurrounding.getAllSurrounding() };
			} else {
				prevDataFormatted = getJsonData(prevData);
			}
		} catch {
			prevDataFormatted = {};
		}

		const initialValues = prepareMultiInitialValues(
			{
				obj: permitData,
				reqFields: ['newWardNo'],
			},
			{
				obj: completeSurroundingObject,
				reqFields: [],
			},
			{
				obj: {
					// default values
					banaune: 'बनाउने',
					shree: 'श्री',
					parcha: 'पर्छ ?',
					Sangyardate: getCurrentDate(true),
					floor: floorArray.getMultipleInitialValues(['length', 'width', 'height', 'block', 'floor'], floorArray.getFloors(), false),
					floorUnit: floorArray.getFloorUnit(),
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			// { obj: { ...erInfo, ...serInfo }, reqFields: [] },
			{
				obj: prevDataFormatted,
				reqFields: [],
			},
			{
				obj: {
					serName: serData.name,
					serDesignation: serData.designation,
					serSignature: serData.signature,
					erName: erData.name,
					erSignature: erData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		this.state = {
			initialValues,
			floorArray,
			wardOption,
			isKankai: checkClient(orgCode, Client.KANKAI),
			isSundarHaraicha: checkClient(orgCode, Client.SUNDARHARAICHA),
			isBirtamod: checkClient(orgCode, Client.BIRTAMOD),
			isKamalamai: checkClient(orgCode, Client.KAMALAMAI),
			isIllam: checkClient(orgCode, Client.ILLAM),
		};
	}

	render() {
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, userData, useSignatureImage } = this.props;
		const { initialValues, floorArray, isKankai, isSundarHaraicha, isBirtamod, isIllam, isKamalamai, wardOption } = this.state;
		const user_info = this.props.userData;
		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={
						isIllam
							? notice15IllamSchema
							: isKankai || isSundarHaraicha || isBirtamod
							? noNumberSchema
							: isKamalamai
							? kamalaiSchema
							: neighbourSchema
					}
					onSubmit={async (values, actions) => {
						values.surrounding = values.surrounding.filter(
							(surr) => !isStringEmpty(surr.kittaNo) && !isStringEmpty(surr.sandhiyar) && !isStringEmpty(surr.feet)
						);
						try {
							await this.props.postAction(`${api.noticePeriodFor15Days}${permitData.applicantNo}`, values);

							window.scrollTo(0, 0);
							actions.setSubmitting(false);

							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, handleChange, isSubmitting, errors, setFieldValue, values, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="print-small-font">
									<div>
										<LetterHeadFlex userInfo={user_info} />
										{isKamalamai ? (
											<FlexSingleRight>
												{/* {Ndata.Sangyardate}: */}
												<DashedLangDateField
													name={'Sangyardate'}
													inline={true}
													setFieldValue={setFieldValue}
													value={values.Sangyardate}
													label={date.date}
													error={errors.Sangyardate}
												/>
											</FlexSingleRight>
										) : (
											<PatraSankhyaAndDate
												setFieldValue={setFieldValue}
												values={values}
												errors={errors}
												dateFieldName="Sangyardate"
											/>
										)}
										<div className="section-header">
											<h3 className="underline">{Ndata.title}</h3>
										</div>
										{isBirtamod || isSundarHaraicha || isKamalamai ? (
											<FirstParagraphBirtamod
												permitData={permitData}
												userData={userData}
												setFieldValue={setFieldValue}
												values={values}
												errors={errors}
											/>
										) : (
											<div className="NJ-header">
												{/* <p>
                                                {Ndata.main_head1}
                                                <DashedLangInput
                                                    name='aminName'
                                                    value={values.aminName}
                                                    setFieldValue={setFieldValue}
                                                    error={errors.aminName}
                                                />
                                            </p> */}
												<p className="neighbours-para">
													{Ndata.main_head2}
													{/* <DashedLangInput
                                                    name='prashashanName'
                                                    value={values.prashashanName}
                                                    setFieldValue={setFieldValue}
                                                    error={errors.prashashanName}
                                                /> */}
												</p>

												<div>
													{/* {console.log("inspan", permitData)} */}
													<span>{userData.organization.name}</span> {Ndata.ward_no} <span>{permitData.newWardNo}</span>{' '}
													{Ndata.kitta_no} <span>{permitData.kittaNo}</span> {Ndata.area} {permitData.landArea}{' '}
													{permitData.landAreaType} {Ndata.main_data1}{' '}
													<Select
														options={banauneOpt}
														name="banaune"
														placeholder="Select an option"
														onChange={(e, { value }) => setFieldValue('banaune', value)}
														value={values['banaune']}
														error={errors.banaune}
														// defaultValue={'बनाउने'}
													/>{' '}
													{getConstructionTypeText(permitData.constructionType)}
													{Ndata.main_data2} {permitData.applicantAddress} {Ndata.main_data3}{' '}
													<Select
														options={shreeOpt}
														name="shree"
														placeholder="Select an option"
														onChange={(e, { value }) => setFieldValue('shree', value)}
														value={values['shree']}
														error={errors.shree}
														// defaultValue={'श्री'}
													/>{' '}
													<span>{permitData.nibedakName} </span>
													{Ndata.main_data4} {getConstructionTypeText(permitData.constructionType)} {Ndata.main_data5}
													<Select
														options={parchaOpt}
														name="parcha"
														placeholder="Select an option"
														onChange={(e, { value }) => setFieldValue('parcha', value)}
														value={values['parcha']}
														error={errors.parcha}
														// defaultValue={'पर्छ ?'}
													/>
													{Ndata.main_data}
												</div>
											</div>
										)}

										{isBirtamod || isSundarHaraicha || isKamalamai ? (
											<p>{Ndata.Table_1.headingBirtamod}</p>
										) : (
											<SectionHeader>
												<h4 className="left-align">{Ndata.Table_1.heading}</h4>
											</SectionHeader>
										)}
										{/* <FormWrapper erStatus={erStatus}> */}
										<Table celled compact="very" className="compact-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width="1">{Ndata.Table_1.rows.row1.name}</Table.HeaderCell>
													{isKamalamai && (
														<Table.HeaderCell width="3">{Ndata.Table_1.rows.row1.afnoJaggaLength}</Table.HeaderCell>
													)}
													<Table.HeaderCell width="3">{Ndata.Table_1.rows.row1.input_1}</Table.HeaderCell>
													<Table.HeaderCell width="3">{Ndata.Table_1.rows.row1.input_2_1}</Table.HeaderCell>
													<Table.HeaderCell width="3">{Ndata.Table_1.rows.row1.input_2_2}</Table.HeaderCell>
													<Table.HeaderCell width="3">{Ndata.Table_1.rows.row1.input_3}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{Object.values(Ndata.Table_1.rows)
													.splice(1)
													.map((row) => {
														const currentSide = values.surrounding.find((sd) => sd.side === parseInt(row.side));
														const side = values.surrounding.indexOf(currentSide);
														return (
															<Table.Row key={side}>
																<Table.Cell>{row.name}</Table.Cell>
																{isKamalamai && (
																	<Table.Cell>
																		<NumberLabelValue
																			name={`surrounding.${side}.afnoJagga`}
																			options={distanceOptions}
																			unitName={`surrounding.${side}.sideUnit`}
																		/>
																	</Table.Cell>
																)}
																<Table.Cell>
																	<NumberLabelValue
																		name={`surrounding.${side}.feet`}
																		options={distanceOptions}
																		unitName={`surrounding.${side}.sideUnit`}
																	/>
																</Table.Cell>
																<Table.Cell>
																	<EbpsForm
																		name={`surrounding.${side}.kittaNo`}
																		setFieldValue={setFieldValue}
																		value={getIn(values, `surrounding.${side}.kittaNo`)}
																	/>
																</Table.Cell>
																<Table.Cell>
																	<EbpsForm
																		name={`surrounding.${side}.sandhiyar`}
																		setFieldValue={setFieldValue}
																		value={getIn(values, `surrounding.${side}.sandhiyar`)}
																	/>
																</Table.Cell>
																<Table.Cell>
																	{/* <Field
                                                                name={`surrounding_${side}_input_4`}
                                                            /> */}
																	<EbpsForm
																		name={`surrounding.${side}.remarks`}
																		setFieldValue={setFieldValue}
																		value={getIn(values, `surrounding.${side}.remarks`)}
																		error={getIn(errors, `surrounding.${side}.remarks`)}
																	/>
																</Table.Cell>
															</Table.Row>
														);
													})}
											</Table.Body>
										</Table>
										{/* Second table  */}
										{isBirtamod || isSundarHaraicha || isKamalamai ? (
											<b>{Ndata.Table_2.headingBirtamod}</b>
										) : (
											<SectionHeader>
												<h4 className="left-align">{Ndata.Table_2.heading}</h4>
											</SectionHeader>
										)}
										{console.log(values)}

										<Table celled className="compact-table">
											<Table.Header>
												
												<Table.Row>
													<Table.HeaderCell width={2}>{Ndata.Table_2.rows.row1.input_1}</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${Ndata.Table_2.rows.row1.input_2} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${Ndata.Table_2.rows.row1.input_3} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>
														{`${Ndata.Table_2.rows.row1.input_4} ( ${getUnitValue(values.floorUnit)} )`}
													</Table.HeaderCell>
													<Table.HeaderCell width={3}>{Ndata.Table_2.rows.row1.input_5}</Table.HeaderCell>
													<Table.HeaderCell width={2}>{Ndata.Table_2.rows.row1.input_6}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{floorArray.getFloorsWithLabels().map((row, input) => (
													<Table.Row key={input}>
														<Table.Cell>
															<EbpsForm
																name={`tapasil_${input}_description`}
																setFieldValue={setFieldValue}
																value={getIn(values, `tapasil_${input}_description`)}
																error={getIn(errors, `tapasil_${input}_description`)}
															/>
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.length`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.length`)}
																error={getIn(errors, `floor.${input}.length`)}
															/>

															{/* <Field
                                                                    name={`tapasil_${input}_length`}
                                                                    value={
                                                                        row.length
                                                                    }
                                                                /> */}
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.width`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.width`)}
																error={getIn(errors, `floor.${input}.width`)}
															/>
														</Table.Cell>
														<Table.Cell>
															<EbpsNormalForm
																name={`floor.${input}.height`}
																onChange={handleChange}
																value={getIn(values, `floor.${input}.height`)}
																error={getIn(errors, `floor.${input}.height`)}
															/>
														</Table.Cell>
														<Table.Cell>
															{/* <Field
																	name={`tapasil_${input}_floorNo`}
																	value={floorMappingFlat.find(fl => fl.floor === row.floor).value}
																/> */}
															{row.label.floorBlockName}
														</Table.Cell>
														<Table.Cell>
															<EbpsForm
																name={`tapasil_${input}_kaifiyat`}
																setFieldValue={setFieldValue}
																value={getIn(values, `tapasil_${input}_kaifiyat`)}
																error={getIn(errors, `tapasil_${input}_kaifiyat`)}
															/>
														</Table.Cell>
													</Table.Row>
												))}
											</Table.Body>
										</Table>
										{/* </FormWrapper> */}

										{isKamalamai ? (
											<KamalamaiFooter wardOption={wardOption} permitData={permitData} />
										) : (
											<>
												<p>{Ndata.Note}</p>

												{isKankai || isBirtamod ? (
													<div>
														<FooterSignature
															designations={[Ndata.kankai.sign1, '', '', Ndata.kankai.sign2]}
															signatureImages={
																useSignatureImage && [values.subSignature, '', '', values.pramukhSignature]
															}
														/>
														{/* <div className="firstalign">
															<div>
																<p className="neighbours-para">
																	<span className="ui input dashedForm-control " />
																</p>
																<p style={{ textAlign: 'center' }}>{Ndata.kankai.sign1}</p>
															</div>
															<div>
																<span className="ui input dashedForm-control " />
															</div>
															<div>
																<span className="ui input dashedForm-control " />
															</div>
															<div>
																<p className="neighbours-para">
																	<span className="ui input dashedForm-control " />
																</p>
																<p style={{ textAlign: 'center' }}>{Ndata.kankai.sign2}</p>
															</div>
														</div> */}
														<p className="NJ-para">{Ndata.kankai.summary}</p>
														<div>
															<span style={{ marginLeft: '20px' }}>{Ndata.kankai.summary_1}</span>{' '}
															<DashedSelect name="newWardNo" options={wardOption} />
															{' : '}
															{Ndata.kankai.summary_1_1}
														</div>
														<div>
															<span style={{ marginLeft: '20px' }}>{Ndata.kankai.summary_2}</span>{' '}
															<span>{permitData.nibedakName}</span> {Ndata.kankai.summary_2_1}
														</div>
													</div>
												) : isSundarHaraicha ? (
													<div>
														<FooterSignature
															designations={[Ndata.sundarHaraicha.sign1, Ndata.sundarHaraicha.sign2]}
															signatureImages={useSignatureImage && [values.serSignature]}
														/>
														<div className="section-header">
															<p className="left-align">{Ndata.sundarHaraicha.summary}</p>
														</div>
														<div>
															{Ndata.sundarHaraicha.number[0]}.
															<DashedSelect name="newWardNo" options={wardOption} />
															{Ndata.summary}
															<p>
																{Ndata.sundarHaraicha.number[1]}. {Ndata.writer} <span>{permitData.nibedakName}</span>
															</p>
														</div>
													</div>
												) : (
													<div>
														{/* <div>
																<p className="NJ-right">
																	<span className="ui input dashedForm-control"></span>
																</p>
																<p className="NJ-right">{values.erName}</p>
																{Ndata.upasakha}
																<DashedLangInput
																	name="upasakha"
																	className="dashedForm-control"
																	handleChange={handleChange}
																	setFieldValue={setFieldValue}
																	value={values.upasakha}
																	error={errors.upasakha}
																/>
															</div> */}

														<FooterSignature designations={[Ndata.upasakha, userData.organization.name]} />
														<br />

														<div>
															{Ndata.summary1}
															<DashedSelect name="newWardNo" options={wardOption} />
															{Ndata.summary}
														</div>
														<p>
															{Ndata.writer} <span>{permitData.nibedakName}</span>
														</p>
														<br />
														<div>
															{Ndata.TalName}
															<div className="no-margin-field">
																<DetailedSignature setFieldValue={setFieldValue} values={values} errors={errors}>
																	{/* <DetailedSignature.Name name="serName" label={Ndata.naam} />
																	<DetailedSignature.Designation name="serDesignation" label={Ndata.TalLevel} /> */}
																	<DetailedSignature.Name name="serNumber" label={Ndata.contact} />
																</DetailedSignature>
															</div>
														</div>
													</div>
												)}
											</>
										)}
									</div>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
								{/* {this.props.hasSavePermission && (
                                    <Button primary onClick={handleSubmit}>
                                        Save
                                    </Button>
                                )} */}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const NeighboursNameApplicationView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.noticePeriodFor15Days,
				objName: 'noticeFifteen',
				form: true,
			},
		]}
		localData={[new LocalAPI(WARD_MASTER, 'wardMaster')]}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param4: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param3: ['15dayspecial'],
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'input', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		useInnerRef={true}
		parentProps={parentProps}
		render={(props) => <NeighboursNameApplicationViewComponent {...props} parentProps={parentProps} />}
	/>
);

const FirstParagraphBirtamod = ({ userData, permitData, setFieldValue, values, errors }) => {
	return (
		<div>
			{/* {console.log("inspan", permitData)} */}
			<span>{userData.organization.name} 
			{` ${Ndata.ward_no}`}{permitData.newWardNo}
			{Ndata.kitta_no}{permitData.kittaNo}
			{Ndata.area}{permitData.landArea}{permitData.landAreaType}
			{Ndata.birtamod_main_data5}
			{userData.organization.name}
			{Ndata.main_data3}{' '}
			</span>
			<Select
				options={shreeOpt}
				name="shree"
				placeholder="Select an option"
				onChange={(e, { value }) => setFieldValue('shree', value)}
				value={values['shree']}
				error={errors.shree}
				// defaultValue={'श्री'}
			/>{' '}	{permitData.nibedakName}
			{Ndata.birtamod_main_data6}

			{/* {Ndata.main_data3}{' '}
			<Select
				options={shreeOpt}
				name="shree"
				placeholder="Select an option"
				onChange={(e, { value }) => setFieldValue('shree', value)}
				value={values['shree']}
				error={errors.shree}
				// defaultValue={'श्री'}
			/>{' '}
			<span>
				{permitData.nibedakName}
				{`${Ndata.le} `}
				{permitData.applicantName}
				{`${Ndata.ko} ${Ndata.birtamod_main_data1} 
				${permitData.oldMunicipal} ${Ndata.haal} 
				${permitData.newMunicipal} ${Ndata.ward_no} 
				${permitData.newWardNo} ${permitData.nibedakSadak} 
				${Ndata.sadak} ${Ndata.kitta_no} ${permitData.kittaNo}
				 ${Ndata.area} ${permitData.landArea} ${permitData.landAreaType} ${Ndata.birtamod_main_data2} `}
				{`${getConstructionTypeText(permitData.constructionType, true)} `}
				{` ${Ndata.birtamod_main_data3}`}
			</span>
			<p>{` ${Ndata.birtamod_main_data4}`}</p> */}
		</div>
	);
};

const KamalamaiFooter = ({ wardOption, permitData }) => {
	return (
		<div>
			<div>{Ndata.kamalamai.sitePlan}</div>
			<br />
			<div>{Ndata.kankai.summary}</div>
			<div className="div-indent">
				<div>
					१. <DashedSelect name="newWardNo" options={wardOption} />
					<span>{Ndata.kamalamai.sign1}</span>
				</div>
				<div>
					<span>{Ndata.kankai.summary_2}</span> <span>{permitData.nibedakName}</span> {Ndata.kankai.summary_2_1}
				</div>
			</div>
		</div>
	);
};
export default NeighboursNameApplicationView;
