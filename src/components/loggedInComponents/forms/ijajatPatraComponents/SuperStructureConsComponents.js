import React from 'react';
import { superstructureconstructionview, mapTechnical, buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import { getMessage } from '../../../shared/getMessage';
import {
	DashedMultiUnitLengthInput,
	DashedUnitInput,
	DashedMultiUnitAreaInput,
	DashedAreaUnitInput,
	DashedAreaInputWithRelatedUnits,
	DashedLengthInputWithRelatedUnits,
} from '../../../shared/EbpsUnitLabelValue';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { Field } from 'formik';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { areaUnitOptions } from '../../../../utils/optionUtils';
import { isBirtamod } from '../../../../utils/clientUtils';
import { tallaThapIjajat } from '../../../../utils/data/tallaThapData';
import { squareUnitOptions } from '../../../../utils/dataUtils';
import { PurposeOfConstructionRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import { isInaruwa } from '../../../../utils/clientUtils';

const messageId = 'superstructureconstructionview.superstructureconstructionview_data';
const sscv_data = superstructureconstructionview.superstructureconstructionview_data;
const mapTechLang = mapTechnical.mapTechnicalDescription;
const tallaThapData = tallaThapIjajat;
const buildPermitLang = buildingPermitApplicationForm.permitApplicationFormView;

export const SuperStructConsBody = ({ permitData, userData, values, buildingClass, setFieldValue, errors, floorMax, suikritTalla }) => {
	return (
		<>
			<div>
				{getMessage(`${messageId}.data_1.data_1_1`, sscv_data.data_1.data_1_1)} {permitData.nibedakName} {userData.organization.name}{' '}
				{getMessage(`${messageId}.data_1.data_1_2`, sscv_data.data_1.data_1_2)} {permitData.nibedakTol}{' '}
				{getMessage(`${messageId}.data_1.data_1_3`, sscv_data.data_1.data_1_3)} {values.gharNo}{' '}
				{getMessage(`${messageId}.data_1.data_1_4`, sscv_data.data_1.data_1_4)}
				{permitData.nibedakSadak}
			</div>
			<div>
				<div className="no-margin-field">
					{getMessage(`${messageId}.data_2.data_2_1`, sscv_data.data_2.data_2_1)}{' '}
					<DashedLangDateField
						name="noteOrderDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.noteOrderDate}
						error={errors.noteOrderDate}
					/>
					{getMessage(`${messageId}.data_2.data_2_2`, sscv_data.data_2.data_2_2)}
					{userData.organization.name}
					{isInaruwa ? (
						<>
							{getMessage(`${messageId}.data_2.data_2_4`, sscv_data.data_2.data_2_4)}
							<DashedLangInput
								name="dasturAmount"
								setFieldValue={setFieldValue}
								value={values.dasturAmount}
								error={errors.dasturAmount}
							/>
							{getMessage(`${messageId}.data_2.data_2_5`, sscv_data.data_2.data_2_5)}
							<DashedLangInput
								name="jariwanaAmount"
								setFieldValue={setFieldValue}
								value={values.jariwanaAmount}
								error={errors.jariwanaAmount}
							/>
							{getMessage(`${messageId}.data_2.data_2_5`, sscv_data.data_2.data_2_6)}
						</>
					) : (
						getMessage(`${messageId}.data_2.data_2_3`, sscv_data.data_2.data_2_3)
					)}
					{/* <DashedLangInput name="jariwanaRate" value={values.jariwanaRate} setFieldValue={setFieldValue} error={errors.jariwanaRate} /> */}
					{/* {getMessage(`${messageId}.data_1.data_2_4`, sscv_data.data_2.data_2_4)} */}
				</div>
				<div>{getMessage(`${messageId}.data_3`, sscv_data.data_3)}</div>
				<div>
					{getMessage(`${messageId}.data_4.data_4_1`, sscv_data.data_4.data_4_1)} {permitData.oldMunicipal}{' '}
					{getMessage(`${messageId}.data_4.data_4_2`, sscv_data.data_4.data_4_2)} {userData.organization.name}{' '}
					{getMessage(`${messageId}.data_4.data_4_3`, sscv_data.data_4.data_4_3)} {permitData.newWardNo}{' '}
					{getMessage(`${messageId}.data_4.data_4_5`, sscv_data.data_4.data_4_5)} {permitData.buildingJoinRoad}{' '}
					{getMessage(`${messageId}.data_4.data_4_6`, sscv_data.data_4.data_4_6)} {permitData.kittaNo}{' '}
					{/* {getMessage(`${messageId}.data_4.data_4_7`, sscv_data.data_4.data_4_7)} */}
					{sscv_data.data_4.data_4_7}
					{permitData.landArea} {permitData.landAreaType}
					{/* <UnitDropdownWithRelatedFields options={landAreaTypeOptions} unitName="landAreaType" defaultValue={'रोपनी–आना–पैसा–दाम'} relatedFields={[]}/> */}
				</div>
			</div>

			<ConstructionTypeRadio space={true} fieldLabel={getMessage(`${messageId}.maindata.maindata_1`, sscv_data.maindata.maindata_1)} />
			<div>
				{getMessage(`${messageId}.maindata.maindata_2`, sscv_data.maindata.maindata_2)}{' '}
				{Object.values(mapTechLang.mapTech_22.mapTech_22_radioBox).map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field type="radio" name="roof" id={index} defaultChecked={values.roof === name} value={name} />
						<label>{name}</label>
					</div>
				))}
			</div>
			<div>
				<DashedAreaUnitInput name="plinthDetails" label={sscv_data.maindata.maindata_3} unitName="plinthDetailsUnit" />
				<DashedAreaUnitInput name="buildCoverArea" unitName="buildCoverAreaUnit" label={sscv_data.maindata.maindata_3_1} />
				<DashedAreaUnitInput name="coverageDetails" unitName="coverageDetailsUnit" label={sscv_data.maindata.maindata_3_2} />
				{isBirtamod ? (
					<div style={{ marginLeft: '15px' }}>
						<DashedAreaUnitInput name="sabikPlinthArea" label={sscv_data.maindata.maindata_3_sabik} unitName="plinthDetailsUnit" />
						<DashedAreaUnitInput name="sabikCoverArea" unitName="buildCoverAreaUnit" label={sscv_data.maindata.maindata_3_1} />
					</div>
				) : (
					<>
						<DashedMultiUnitAreaInput
							name="buildingArea"
							unitName="buildingAreaUnit"
							label={sscv_data.maindata.maindata_3_3}
							squareOptions={areaUnitOptions}
							relatedFields={['houseLength', 'houseBreadth', 'houseHeight']}
						/>
						<DashedAreaUnitInput name="coverageArea" unitName="coverageAreaUnit" label={sscv_data.maindata.maindata_3_4} />
						<DashedMultiUnitLengthInput
							name="houseLength"
							unitName="houseLengthUnit"
							relatedFields={['houseBreadth', 'houseHeight']}
							areaField={'buildingArea'}
							label={sscv_data.maindata.maindata_3_5}
						/>
						<DashedMultiUnitLengthInput
							name="houseBreadth"
							unitName="houseBreadthUnit"
							relatedFields={['houseLength', 'houseHeight']}
							areaField={'buildingArea'}
							label={sscv_data.maindata.maindata_3_6}
						/>
						<DashedMultiUnitLengthInput
							name="houseHeight"
							unitName="houseHeightUnit"
							relatedFields={['houseLength', 'houseBreadth']}
							areaField={'buildingArea'}
							label={sscv_data.maindata.maindata_3_7}
						/>
					</>
				)}
			</div>
			<div>
				{getMessage(`${messageId}.maindata.maindata_4`, sscv_data.maindata.maindata_4)}
				{/* <DashedLangInput name="buildingHeight" value={values.buildingHeight} setFieldValue={setFieldValue} error={errors.buildingHeight} /> */}
				<span> {floorMax} </span>
				{getMessage(`${messageId}.maindata.maindata_4_1`, sscv_data.maindata.maindata_4_1)}
				<span> {suikritTalla}</span>
				{/* <DashedLangInput name="suikritTalla" value={values.suikritTalla} setFieldValue={setFieldValue} error={errors.suikritTalla} /> */}
			</div>
			<div>
				<DashedMultiUnitLengthInput
					name="constructionHeight"
					unitName="constructionHeightUnit"
					relatedFields={['allowableHeight']}
					label={sscv_data.maindata.maindata_5}
				/>
				<DashedMultiUnitLengthInput
					name="allowableHeight"
					unitName="constructionHeightUnit"
					relatedFields={['constructionHeight']}
					label={sscv_data.maindata.maindata_5_1}
				/>
			</div>
			<div>
				{getMessage(`${messageId}.maindata.maindata_6`, sscv_data.maindata.maindata_6)}{' '}
				{Object.values(buildPermitLang.form_step5.checkBox_option).map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field type="radio" name="purposeOfConstruction" defaultChecked={values.purposeOfConstruction === name} value={name} />
						<label>{name}</label>
					</div>
				))}
			</div>
			<div>
				{getMessage(`${messageId}.maindata.maindata_7`, sscv_data.maindata.maindata_7)}&nbsp;
				{buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
			</div>
			<div>
				<DashedUnitInput name="requiredDistance" label={sscv_data.maindata.maindata_8} unitName="sadakAdhikarUnit" />
			</div>
			<div>
				<DashedUnitInput
					name="publicPropertyDistance"
					label={sscv_data.maindata.maindata_9}
					unitName="publicPropertyUnit"
					value={values.setBack}
				/>
			</div>
		</>
	);
};

export const SuperStructConsBodySundarHaraicha = ({ permitData, userData, values, buildingClass, setFieldValue, errors, floorMax, suikritTalla }) => {
	return (
		<>
			<div>
				{getMessage(`${messageId}.data_1.data_1_1`, sscv_data.data_1.data_1_1)} {permitData.nibedakName} {userData.organization.name}{' '}
				{getMessage(`${messageId}.data_1.data_1_2`, sscv_data.data_1.data_1_2)} {permitData.nibedakTol}{' '}
				{getMessage(`${messageId}.data_1.data_1_3`, sscv_data.data_1.data_1_3)} {values.gharNo}{' '}
				{getMessage(`${messageId}.data_1.data_1_4`, sscv_data.data_1.data_1_4)}
				{permitData.nibedakSadak}
			</div>
			<div>
				<div className="no-margin-field">
					{getMessage(`${messageId}.data_2.data_2_1`, sscv_data.data_2.data_2_1)}{' '}
					<DashedLangDateField
						name="noteOrderDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.noteOrderDate}
						error={errors.noteOrderDate}
					/>
					{getMessage(`${messageId}.newData.data1`, sscv_data.newData.data1)}
					<DashedLangDateField
						name="permissionOrderDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.permissionOrderDate}
						error={errors.permissionOrderDate}
					/>
					{getMessage(`${messageId}.newData.data2`, sscv_data.newData.data2)}
					<DashedLangInput name="fineRupees" value={values.fineRupees} setFieldValue={setFieldValue} error={errors.fineRupees} />
					{getMessage(`${messageId}.newData.data3`, sscv_data.newData.data3)}
					{/* {userData.organization.name} */}
					{/* <DashedLangInput name="nakshaPassDastur" value={values.nakshaPassDastur} setFieldValue={setFieldValue} error={errors.nakshaPassDastur} /> */}
					{/* {getMessage(`${messageId}.data_2.data_2_3`, sscv_data.data_2.data_2_3)} */}
					{/* <DashedLangInput name="jariwanaRate" value={values.jariwanaRate} setFieldValue={setFieldValue} error={errors.jariwanaRate} /> */}
					{/* {getMessage(`${messageId}.data_1.data_2_4`, sscv_data.data_2.data_2_4)} */}
				</div>
				<SectionHeader>
					<p className="underline">{sscv_data.maindata.maindata_01}</p>
				</SectionHeader>

				{/* <div>{getMessage(`${messageId}.data_3`, sscv_data.data_3)}</div> */}
				<div>
					{getMessage(`${messageId}.data_4.data_4_2`, sscv_data.data_4.data_4_2)}
					{/* {permitData.oldMunicipal}{' '}
					{getMessage(`${messageId}.data_4.data_4_2`, sscv_data.data_4.data_4_2)} */}
					{userData.organization.name} {getMessage(`${messageId}.data_4.data_4_3`, sscv_data.data_4.data_4_3)} {permitData.newWardNo}{' '}
					{getMessage(`${messageId}.data_4.data_4_1`, sscv_data.data_4.data_4_1)} {permitData.oldMunicipal}{' '}
					{getMessage(`${messageId}.data_4.data_4_6`, sscv_data.data_4.data_4_6)} {permitData.kittaNo}{' '}
					{/* {getMessage(`${messageId}.data_4.data_4_7`, sscv_data.data_4.data_4_7)} */}
					{sscv_data.data_4.data_4_7}
					{permitData.landArea} {permitData.landAreaType}
					{/* <UnitDropdownWithRelatedFields options={landAreaTypeOptions} unitName="landAreaType" defaultValue={'रोपनी–आना–पैसा–दाम'} relatedFields={[]}/> */}
				</div>
			</div>
			<SectionHeader>
				<p className="underline">{sscv_data.maindata.maindata_02}</p>
			</SectionHeader>
			<ConstructionTypeRadio space={true} fieldLabel={getMessage(`${messageId}.maindata.maindata_1`, sscv_data.maindata.maindata_1)} />
			<div>
				{getMessage(`${messageId}.maindata.maindata_2`, sscv_data.maindata.maindata_2)}{' '}
				{Object.values(mapTechLang.mapTech_22.mapTech_22_radioBox).map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field type="radio" name="roof" id={index} defaultChecked={values.roof === name} value={name} />
						<label>{name}</label>
					</div>
				))}
			</div>
			<div>
				<DashedAreaUnitInput name="plinthDetails" label={sscv_data.maindata.maindata_3} unitName="plinthDetailsUnit" />
				<DashedAreaUnitInput name="buildCoverArea" unitName="buildCoverAreaUnit" label={sscv_data.maindata.maindata_3_1} />
			</div>
			<div>
				<DashedAreaUnitInput name="coverageDetails" unitName="coverageDetailsUnit" label={sscv_data.maindata.maindata_11_3} />
				<DashedAreaUnitInput name="adhikCoverageDetails" unitName="coverageDetailsUnit" label={sscv_data.maindata.maindata_11_4} />

				{/* {isBirtamod ? (
					<div style={{ marginLeft: '15px' }}>
						<DashedAreaUnitInput name="sabikPlinthArea" label={sscv_data.maindata.maindata_3_sabik} unitName="plinthDetailsUnit" />
						<DashedAreaUnitInput name="sabikCoverArea" unitName="buildCoverAreaUnit" label={sscv_data.maindata.maindata_3_1} />
					</div>
				) : (
						<>
							<DashedMultiUnitAreaInput
								name="buildingArea"
								unitName="buildingAreaUnit"
								label={sscv_data.maindata.maindata_3_3}
								squareOptions={areaUnitOptions}
								relatedFields={['houseLength', 'houseBreadth', 'houseHeight']}
							/>
							<DashedAreaUnitInput name="coverageArea" unitName="coverageAreaUnit" label={sscv_data.maindata.maindata_3_4} />
							<DashedMultiUnitLengthInput
								name="houseLength"
								unitName="houseLengthUnit"
								relatedFields={['houseBreadth', 'houseHeight']}
								areaField={'buildingArea'}
								label={sscv_data.maindata.maindata_3_5}
							/>
							<DashedMultiUnitLengthInput
								name="houseBreadth"
								unitName="houseBreadthUnit"
								relatedFields={['houseLength', 'houseHeight']}
								areaField={'buildingArea'}
								label={sscv_data.maindata.maindata_3_6}
							/>
							<DashedMultiUnitLengthInput
								name="houseHeight"
								unitName="houseHeightUnit"
								relatedFields={['houseLength', 'houseBreadth']}
								areaField={'buildingArea'}
								label={sscv_data.maindata.maindata_3_7}
							/>
						</>
					)} */}
			</div>

			<div>
				{getMessage(`${messageId}.maindata.maindata_11_5`, sscv_data.maindata.maindata_11_5)}
				{/* <DashedLangInput name="buildingHeight" value={values.buildingHeight} setFieldValue={setFieldValue} error={errors.buildingHeight} /> */}
				<span> {floorMax} </span>
				{getMessage(`${messageId}.maindata.maindata_4_1`, sscv_data.maindata.maindata_4_1)}

				<DashedLangInput name="suikritTalla" value={values.suikritTalla} setFieldValue={setFieldValue} error={errors.suikritTalla} />
			</div>
			{/* <div>
				<DashedMultiUnitLengthInput
					name="constructionHeight"
					unitName="constructionHeightUnit"
					relatedFields={['allowableHeight']}
					label={sscv_data.maindata.maindata_5}
				/>
				<DashedMultiUnitLengthInput
					name="allowableHeight"
					unitName="constructionHeightUnit"
					relatedFields={['constructionHeight']}
					label={sscv_data.maindata.maindata_5_1}
				/>
			</div> */}

			<div>
				<DashedMultiUnitLengthInput
					name="constructionHeight"
					unitName="constructionHeightUnit"
					relatedFields={['adhikConstructionHeight']}
					label={sscv_data.maindata.maindata_11_7}
				/>
				<DashedMultiUnitLengthInput
					name="adhikConstructionHeight"
					unitName="constructionHeightUnit"
					relatedFields={['constructionHeight']}
					label={sscv_data.maindata.maindata_11_8}
				/>
			</div>

			{/* <div>
				{getMessage(`${messageId}.maindata.maindata_6`, sscv_data.maindata.maindata_6)}{' '}
				{Object.values(buildPermitLang.form_step5.checkBox_option).map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field type="radio" name="purposeOfConstruction" defaultChecked={values.purposeOfConstruction === name} value={name} />
						<label>{name}</label>
					</div>
				))}
			</div> */}
			<div>
				<PurposeOfConstructionRadio
					space={true}
					fieldLabel={sscv_data.maindata.maindata_11_9}
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
					name="purposeOfConstruction"
				/>
			</div>
			<div>
				<PurposeOfConstructionRadio
					space={true}
					fieldLabel={sscv_data.maindata.maindata_12_1}
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
					name="bhuUpayogXetra"
				/>
			</div>

			<div>
				{getMessage(`${messageId}.maindata.maindata_12_2`, sscv_data.maindata.maindata_12_2)}&nbsp;
				{buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
			</div>
			<div>
				{sscv_data.maindata.maindata_12_3}
				<DashedLangInput name="blockCount" value={values.blockCount} setFieldValue={setFieldValue} error={errors.blockCount} />
			</div>
			<div>
				<DashedUnitInput name="requiredDistance" label={sscv_data.maindata.maindata_12_4} unitName="sadakAdhikarUnit" />
			</div>
			<div>
				<DashedUnitInput
					name="publicPropertyDistance"
					label={sscv_data.maindata.maindata_12_5}
					unitName="publicPropertyUnit"
					value={values.setBack}
				/>
			</div>
			<div>
				{sscv_data.maindata.maindata_12_6}
				{/* {sscv_data.maindata.input_1.map(input => (

					<div className="ui radio checkbox prabidhik">
						<input type="radio" name="check" value={input} />
						<label>{input}</label>
					</div>

				))} */}

				{sscv_data.maindata.input_1.map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field
							type="radio"
							name="check"
							id={index}
							defaultChecked={values.check === name}
							value={name}
							// onClick={handleChange}
						/>
						<label>{name}</label>
					</div>
				))}
				<br />
				{values.check === sscv_data.maindata.input_1[1] && (
					<DashedLangInput
						name="palanaNabhako"
						className="dashedForm-control"
						setFieldValue={setFieldValue}
						value={values.palanaNabhako}
						error={errors.palanaNabhako}
						line="full-line"
					/>
				)}
			</div>
		</>
	);
};

export const SuperStructConstBodyLastSection = ({ setFieldValue, values, errors }) => {
	return (
		<>
			{isInaruwa ? (
				<div className="no-margin-field">
					{getMessage(`${messageId}.maindata.maindata_10`, sscv_data.maindata.maindata_10)}{' '}
					<DashedLangDateField
						name="allowanceDate"
						setFieldValue={setFieldValue}
						value={values.allowanceDate}
						error={errors.allowanceDate}
						inline={true}
						className="dashedForm-control"
					/>
					{/* <DashedLangDateField name='allowanceDate' handleChange={handleChange} value={values.allowanceDate} setFieldValue={setFieldValue} error={errors.allowanceDate} /> */}
					{getMessage(`${messageId}.maindata.maindata_10_1_inaruwa`, sscv_data.maindata.maindata_10_1_inaruwa)}
				</div>
			) : (
				<div className="no-margin-field">
					{getMessage(`${messageId}.maindata.maindata_10`, sscv_data.maindata.maindata_10)}{' '}
					<DashedLangDateField
						name="allowanceDate"
						setFieldValue={setFieldValue}
						value={values.allowanceDate}
						error={errors.allowanceDate}
						inline={true}
						className="dashedForm-control"
					/>
					{/* <DashedLangDateField name='allowanceDate' handleChange={handleChange} value={values.allowanceDate} setFieldValue={setFieldValue} error={errors.allowanceDate} /> */}
					{getMessage(`${messageId}.maindata.maindata_10_1`, sscv_data.maindata.maindata_10_1)}
				</div>
			)}
			<div>
				{getMessage(`${messageId}.maindata.maindata_11`, sscv_data.maindata.maindata_11)}{' '}
				<DashedLangInput
					name="anya"
					className="dashedForm-control"
					setFieldValue={setFieldValue}
					value={values.anya}
					error={errors.anya}
					line="full-line"
				/>
			</div>
		</>
	);
};

export const SuperStructConstBodyLastSectionBirtamod = ({ setFieldValue, values, errors }) => {
	return (
		<>
			<div className="no-margin-field">
				{getMessage(`${messageId}.maindata.maindata_10`, sscv_data.maindata.maindata_10)}{' '}
				<DashedLangDateField
					name="allowanceDate"
					setFieldValue={setFieldValue}
					value={values.allowanceDate}
					error={errors.allowanceDate}
					inline={true}
					className="dashedForm-control"
				/>
				{/* <DashedLangDateField name='allowanceDate' handleChange={handleChange} value={values.allowanceDate} setFieldValue={setFieldValue} error={errors.allowanceDate} /> */}
				{getMessage(`${messageId}.maindata.maindata_10_1_birtamod`, sscv_data.maindata.maindata_10_1_birtamod)}
			</div>
			<div>
				{getMessage(`${messageId}.maindata.maindata_11_birtamod`, sscv_data.maindata.maindata_11_birtamod)}
				<br />
				<br />
				<div>
					{sscv_data.footer.footer_5}
					<span className="ui input dashedForm-control " />
				</div>
				<div className="no-margin-field">
					{sscv_data.maindata.maindata_11_2}{' '}
					<DashedLangDateField
						name="gharDhaniDate"
						setFieldValue={setFieldValue}
						value={values.gharDhaniDate}
						error={errors.gharDhaniDate}
						inline={true}
					/>
				</div>
			</div>
		</>
	);
};

export const TallaThapIjajatBody = ({ permitData, userData, values, buildingClass, setFieldValue, errors, floorMax, suikritTalla }) => {
	return (
		<>
			{/* <div>
				{getMessage(`${messageId}.data_1.data_1_1`, sscv_data.data_1.data_1_1)} {permitData.nibedakName} {userData.organization.name}{' '}
				{getMessage(`${messageId}.data_1.data_1_2`, sscv_data.data_1.data_1_2)} {permitData.nibedakTol}{' '}
				{getMessage(`${messageId}.data_1.data_1_3`, sscv_data.data_1.data_1_3)} {values.gharNo}{' '}
				{getMessage(`${messageId}.data_1.data_1_4`, sscv_data.data_1.data_1_4)}
				{permitData.nibedakSadak}
			</div> */}
			<div>
				<br />
				<div className="no-margin-field">
					<span className="indent-span">{tallaThapData.body.data1}</span>{' '}
					<DashedLangDateField
						name="noteOrderDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.noteOrderDate}
						error={errors.noteOrderDate}
					/>{' '}
					{tallaThapData.body.data2}{' '}
					<DashedLangDateField
						name="supStrucDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.supStrucDate}
						error={errors.supStrucDate}
					/>{' '}
					{tallaThapData.body.data3} {suikritTalla} {tallaThapData.body.data4} <DashedNormalInputIm name="tallaThapFloor" />{' '}
					{tallaThapData.body.data5} <DashedNormalInputIm name="totalTallaThapFloor" /> {tallaThapData.body.data6}{' '}
					<DashedLangDateField
						name="tallaThapRequestDate"
						inline={true}
						setFieldValue={setFieldValue}
						value={values.tallaThapRequestDate}
						error={errors.tallaThapRequestDate}
					/>{' '}
					{tallaThapData.body.data7} <DashedNormalInputIm name="tallaThapFloor" /> {tallaThapData.body.data8}
				</div>
				<div>
					<span className="indent-span">{tallaThapData.body.data9}</span>
				</div>
				<div className="section-header">
					<h3 className="underline">{tallaThapData.body2.title}</h3>
				</div>
				<div>
					{getMessage(`${messageId}.data_4.data_4_1`, sscv_data.data_4.data_4_1)} {permitData.oldMunicipal}{' '}
					{getMessage(`${messageId}.data_4.data_4_2`, sscv_data.data_4.data_4_2)} {permitData.newMunicipal}{' '}
					{getMessage(`${messageId}.data_4.data_4_3`, sscv_data.data_4.data_4_3)} {permitData.newWardNo}{' '}
					{getMessage(`${messageId}.data_4.data_4_6`, sscv_data.data_4.data_4_6)} {permitData.kittaNo}{' '}
					{/* {getMessage(`${messageId}.data_4.data_4_7`, sscv_data.data_4.data_4_7)} */}
					{sscv_data.data_4.data_4_7}
					{permitData.landArea} {permitData.landAreaType}
					{/* <UnitDropdownWithRelatedFields options={landAreaTypeOptions} unitName="landAreaType" defaultValue={'रोपनी–आना–पैसा–दाम'} relatedFields={[]}/> */}
				</div>
			</div>

			<div>
				<ConstructionTypeRadio space={true} fieldLabel={tallaThapData.body2.maindata_1} /> {tallaThapData.body2.tallaThap}{' '}
				<DashedNormalInputIm name="tallaThapFloor" /> {tallaThapData.body2.thallaThap1}
			</div>

			<div>
				{tallaThapData.body2.maindata_2}{' '}
				{Object.values(mapTechLang.mapTech_22.mapTech_22_radioBox).map((name, index) => (
					<div key={index} className="ui radio checkbox prabidhik">
						<Field type="radio" name="roof" id={index} defaultChecked={values.roof === name} value={name} />
						<label>{name}</label>
					</div>
				))}
			</div>

			<div>
				<DashedAreaInputWithRelatedUnits
					name="plinthDetails"
					unitName="plinthDetailsUnit"
					label={tallaThapData.body2.maindata_3}
					squareOptions={squareUnitOptions}
					relatedFields={['buildingHeight', 'houseBreadth', 'houseHeight']}
				/>
			</div>
			<div>
				<DashedLengthInputWithRelatedUnits
					name="houseLength"
					unitName="plinthDetailsUnit"
					relatedFields={['houseBreadth', 'buildingArea', 'buildingHeight']}
					label={tallaThapData.body2.maindata_4}
				/>
				<DashedLengthInputWithRelatedUnits
					name="houseBreadth"
					unitName="plinthDetailsUnit"
					relatedFields={['houseLength', 'buildingHeight', 'buildingArea']}
					label={tallaThapData.body2.width}
				/>
				<DashedLengthInputWithRelatedUnits
					name="buildingHeight"
					unitName="plinthDetailsUnit"
					relatedFields={['plinthDetails', 'houseLength', 'houseBreadth']}
					label={tallaThapData.body2.height}
				/>
			</div>
			<div>
				<PurposeOfConstructionRadio
					space={true}
					fieldLabel={tallaThapData.body2.maindata_5}
					values={values}
					errors={errors}
					setFieldValue={setFieldValue}
				/>
			</div>
			<div>
				{tallaThapData.body2.maindata_6}: {buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
			</div>
			<div className="no-margin-field">
				{tallaThapData.body2.maindata_7}{' '}
				<DashedLangDateField
					name="tallaThapDate"
					inline={true}
					setFieldValue={setFieldValue}
					value={values.tallaThapDate}
					error={errors.tallaThapDate}
				/>
				{tallaThapData.body2.maindata_8}
			</div>
		</>
	);
};
