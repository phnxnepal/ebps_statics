import React from 'react';
import { Select, Table } from 'semantic-ui-react';
import * as Yup from 'yup';

import { AllowancePaperData } from '../../../../utils/data/AllowancePaperData';
import { mapTechnical } from '../../../../utils/data/mockLangFile';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import TableInput from '../../../shared/TableInput';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import {
	DashedMultiUnitAreaInput,
	DashedMultiUnitLengthInput,
	DashedUnitInput,
	DashedAreaUnitInput,
	DashedLengthInputWithRelatedUnits,
} from '../../../shared/EbpsUnitLabelValue';
import { squareUnitOptions, genderOptions } from '../../../../utils/optionUtils';
import { DashedLangDateField, DashedDateField } from '../../../shared/DateField';
import EbpsTextareaField from '../../../shared/MyTextArea';
import {
	validateNullableNepaliDate,
	validateString,
	validateNormalNepaliDateWithRange,
	validateNullableNumber,
	validateNumber,
} from '../../../../utils/validationUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { HaalSection } from '../formComponents/PermitInfo';
import { PurposeOfConstructionRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import { FooterSignature } from '../formComponents/FooterSignature';
import { BlockComponents } from '../../../shared/formComponents/BlockFloorComponents';
import { getIn } from 'formik';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { AllowanceKamalamaiBlockFloors } from '../certificateComponents/CertificateFloorInputs';
import { NirmanTypeAnyaRadio } from '../mapTechFormComponents/NirmanTypeRadio';
import { FlexSpaceBetween } from '../../../uiComponents/FlexDivs';

const Adata = AllowancePaperData.structureDesign;
const inaruwaData = AllowancePaperData.inaruwaData;
const sundarData = AllowancePaperData.sundarData;
const mapTech_data = mapTechnical.mapTechnicalDescription;
const kamalamaiData = AllowancePaperData.kamalamaiData;
const kageyshowriData = AllowancePaperData.kageyshowriData;

export const AllowancePaperBody = ({ permitData, userData, values, handleChange, setFieldValue, errors, buildingClass }) => {
	return (
		<>
			<div>
				<Select placeholder="श्री" options={genderOptions} /> {`${permitData.nibedakName} `}
				{userData.organization.name} {Adata.wardNo} {permitData.newWardNo} {Adata.gharNo}
				<DashedLangInput
					name="gharNo"
					className="dashedForm-control"
					setFieldValue={setFieldValue}
					value={values.gharNo}
					error={errors.gharNo}
				/>{' '}
				{/* {permitData.landRegNo}  */}
				{Adata.sadakName} {permitData.buildingJoinRoad}{' '}
			</div>
			<div>
				{/* <p className="neighbours-para"> */}
				{Adata.main_data} <br /> {Adata.sabik} {permitData.oldMunicipal} {Adata.Haal} {userData.organization.name} {Adata.wardNo}{' '}
				{permitData.newWardNo} {Adata.sadak}
				{permitData.buildingJoinRoad} {Adata.kitaa} {permitData.kittaNo} {Adata.area}
				{permitData.landArea} {permitData.landAreaType}
				{/* </p> */}
			</div>
			{/* <p className="neighbours-para"> */}
			<div>
				<ConstructionTypeRadio space={true} fieldLabel={Adata.list1} />
			</div>
			<div>
				<span className="inline field">
					<span>{Adata.list2}</span>
					{Object.values(mapTech_data.mapTech_22.mapTech_22_radioBox).map((option) => (
						<div className="ui radio checkbox prabidhik" key={option}>
							<input type="radio" name="roof" value={option} defaultChecked={values.roof === option} onChange={handleChange} />
							<label>{option}</label>
						</div>
					))}

					{values.roof === mapTech_data.mapTech_22.mapTech_22_radioBox.mapTech_22_Opt_6 && (
						<DashedLangInput
							// inline={true}
							name="roofOther"
							placeholder="Additional Information..."
							setFieldValue={setFieldValue}
							value={values.roofOther}
							error={errors.roofOther}
						/>
					)}
				</span>
			</div>

			<div>
				<DashedMultiUnitAreaInput
					name="floor.area"
					unitName="floorareaUnit"
					label={Adata.list3}
					squareOptions={squareUnitOptions}
					relatedFields={['floor.length', 'floor.width', 'floor.height']}
				/>
				<DashedMultiUnitLengthInput
					name="floor.length"
					unitName="floorareaUnit"
					relatedFields={['floor.height', 'floor.width']}
					areaField={'floor.area'}
					label={Adata.lambae}
				/>
				<DashedMultiUnitLengthInput
					name="floor.width"
					unitName="floorareaUnit"
					relatedFields={['floor.height', 'floor.length']}
					areaField={'floor.area'}
					label={Adata.chaudai}
				/>
				<DashedMultiUnitLengthInput
					name="floor.height"
					unitName="floorareaUnit"
					relatedFields={['floor.width', 'floor.length']}
					areaField={'floor.area'}
					label={Adata.uchai}
				/>
			</div>

			<div>
				<span className="inline field">
					<span>{Adata.list4}</span>
					{Object.values(Adata.checkBox_option2).map((option) => (
						<div className="ui radio checkbox prabidhik" key={option}>
							<input
								type="radio"
								name="purposeOfConstruction"
								value={option}
								defaultChecked={values.purposeOfConstruction === option}
								onChange={handleChange}
							/>
							<label>{option}</label>
						</div>
					))}

					{values.purposeOfConstruction === Adata.checkBox_option2.option_3 && (
						<DashedLangInput
							// inline={true}
							name="purposeOfConstructionOther"
							placeholder="Additional Information..."
							setFieldValue={setFieldValue}
							value={values.purposeOfConstructionOther}
							error={errors.purposeOfConstructionOther}
						/>
					)}
				</span>
			</div>
			<div>
				<span>
					{Adata.list5}
					{buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
				</span>
			</div>

			{Adata.list6}

			<DashedMultiUnitLengthInput
				name="requiredDistance"
				unitName="sadakAdhikarUnit"
				// relatedFields={['namedAdhikar', 'namedMapdanda']}
				// label={pppv_data.ppbv_footer_data_3_2}
			/>
			<br />
			{/* {Adata.list7} */}
			<DashedUnitInput name="publicPropertyDistance" unitName="publicPropertyUnit" label={Adata.list7} />

			<div className="no-margin-field">
				{Adata.list8}{' '}
				<DashedLangDateField
					name="allowanceDate"
					inline={true}
					setFieldValue={setFieldValue}
					value={values.allowanceDate}
					label={Adata.allowanceDate}
				/>{' '}
				{Adata.list8_1}
			</div>
			<div>
				{Adata.list9}
				<EbpsTextareaField
					placeholder="....."
					name="miscDesc"
					setFieldValue={setFieldValue}
					value={values.miscDesc}
					error={errors.miscDesc}
				/>
			</div>
		</>
	);
};








export const AllowancePaperKageyshowriBody = ({ permitData, userData, values, handleChange, setFieldValue, errors, buildingClass }) => {
	values.buildingJoinRoad = permitData.buildingJoinRoad ? permitData.buildingJoinRoad : values.buildingJoinRoad
	values.sadak = permitData.sadak ? permitData.sadak : values.sadak
	return (
		<>
			<div>
				<span>{kageyshowriData.shree}</span>
				{' '}{permitData.applicantName}{' '}
				<br />


				<span>{userData.organization.name} {kageyshowriData.content1}</span>
				{' '}{permitData.newWardNo}{' '}
				<span>{kageyshowriData.alphabiram}</span>
				<br />
				<br />

				<span>{kageyshowriData.shabik}</span>
				{' '}{permitData.oldMunicipal}{' '}

				<span>{kageyshowriData.gabisha}</span>
				{' '}{permitData.oldWardNo}{' '}
				
				<span>{kageyshowriData.hal}</span>
				{' '}{permitData.newMunicipal}{' '}

				<span>{kageyshowriData.wadaNo}</span>
				{' '}{permitData.newWardNo}{' '}

				<span>{kageyshowriData.toll}</span>
				<DashedLangInput 
					name="sadak"
					value={values.sadak ? values.sadak : permitData.sadak}
					error={errors.sadak}
					setFieldValue={setFieldValue}
				/>

				<span>{kageyshowriData.kittaNo}</span>
				{' '}{permitData.kittaNo}{' '}

				<span>{kageyshowriData.chhetrafal}</span>
				{' '}{permitData.landArea}{' '}

				<span>{kageyshowriData.bamiBhayeko} {permitData.nibedakTol}</span>
				<DashedLangInput 
					name="bami"
					value={values.bami}
					error={errors.bami}
					setFieldValue={setFieldValue}
				/>

				<span>{kageyshowriData.content2}</span>

				<br />
				<br />
				

				<div className="div-indent">
					<table style={{ textAlign: 'left', width: "100%" }} border="1px" className="small-font-print-table">
						<thead>
							<tr>
								<th></th>
								<th colSpan="4">{kageyshowriData.tableData.headings.heading1}</th>
								<th rowSpan="2">{kageyshowriData.tableData.headings.heading2}</th>
							</tr>

							<tr>
								<th rowSpan="2">{kageyshowriData.tableData.headings.heading3}</th>
								<th rowSpan="2">{kageyshowriData.tableData.headings.heading4}</th>
								<th rowSpan="2">{kageyshowriData.tableData.headings.heading5}</th>
								<th colSpan="2">{kageyshowriData.tableData.headings.heading6}</th>
							</tr>

							<tr>
								<th>{kageyshowriData.tableData.headings.heading7}</th>
								<th>{kageyshowriData.tableData.headings.heading8}</th>
								<th></th>
							</tr>
						</thead>
						{console.log(permitData)}
						<tbody>
							<tr>
								<td>{kageyshowriData.tableData.notLoopable.number}</td>
								<td>{kageyshowriData.tableData.notLoopable.topic}</td>
								<td colSpan="4">
									{kageyshowriData.tableData.notLoopable.data.checkbox.map(e => {
										return <label>
											<input name={e} value={`kageyshowriData.tableData.notLoopable.data.checkboxData.${e}`} type="checkbox"/>
											{`  ${e}  `}
										</label>
									})}
								</td>
							</tr>
							{kageyshowriData.tableData.loopableData.map((e, i) => {
								return <tr>
									<td>{e.number}</td>
									<td>{e.topic}</td>
									<td>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name={`row1${i}`}
											value={values[`row1${i}`]}
											error={errors[`row1${i}`]}
										/>
									</td>
									<td>
										<DashedLangInput
											setFieldValue={setFieldValue}
												name={`row2${i}`}
												value={values[`row2${i}`]}
												error={errors[`row2${i}`]}
										/>
									</td>
									<td>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name={`row3${i}`}
											value={values[`row3${i}`]}
											error={errors[`row3${i}`]}
										/>
									</td>
									<td>
										<DashedLangInput
											setFieldValue={setFieldValue}
											name={`row4${i}`}
											value={values[`row4${i}`]}
											error={errors[`row4${i}`]}
										/>
									</td>
								</ tr>
							})}
						</tbody>
					</table>
				</div>





			</div>
		</>
	);
};








export const AllowancePaperBodySundarHariacha = ({
	permitData,
	userData,
	values,
	handleChange,
	setFieldValue,
	errors,
	buildingClass,
	floorArray,
	blocks,
}) => {
	return (
		<>
			<div>
				<Select placeholder="श्री" options={genderOptions} /> {`${permitData.nibedakName} `}
				{userData.organization.name} {Adata.wardNo} {permitData.newWardNo} {Adata.sadakName} {permitData.buildingJoinRoad}{' '}
			</div>
			<div className="no-margin-field">
				<span className="indent-span">{sundarData.body1.applicantDate}</span>
				<DashedDateField inline={true} compact={true} name="applicantDateBS" />
				{sundarData.body1.allowanceDate}{' '}
				<DashedLangDateField
					name="allowanceDate"
					setFieldValue={setFieldValue}
					value={values.allowanceDate}
					error={errors.allowanceDate}
					inline={true}
					compact={true}
				/>{' '}
				{sundarData.body1.ko}{' '}
				<DashedLangInput name="allowanceKo" setFieldValue={setFieldValue} value={values.allowanceKo} error={errors.allowanceKo} />{' '}
				{sundarData.body1.bodyEnd}
			</div>
			<SectionHeader>
				<p className="normal underline">{sundarData.jaggaBibaran}</p>
			</SectionHeader>
			<HaalSection permitData={permitData} />
			<div>
				<ConstructionTypeRadio space={true} fieldLabel={Adata.list1} />
			</div>
			<div>
				<div className="inline field">
					<span>{Adata.list2}</span>
					{Object.values(mapTech_data.mapTech_22.mapTech_22_radioBox).map((option) => (
						<div className="ui radio checkbox prabidhik" key={option}>
							<input type="radio" name="roof" value={option} defaultChecked={values.roof === option} onChange={handleChange} />
							<label>{option}</label>
						</div>
					))}
					{values.roof === mapTech_data.mapTech_22.mapTech_22_radioBox.mapTech_22_Opt_6 && (
						<DashedLangInput
							// inline={true}
							name="roofOther"
							placeholder="Additional Information..."
							setFieldValue={setFieldValue}
							value={values.roofOther}
							error={errors.roofOther}
						/>
					)}{' '}
					<DashedMultiUnitAreaInput
						name="floor.area"
						unitName="floorareaUnit"
						label={sundarData.body2.kulArea}
						squareOptions={squareUnitOptions}
						relatedFields={['buildingLength', 'buildingWidth', 'buildingHeight']}
					/>
				</div>
			</div>
			<div>
				<DashedMultiUnitAreaInput
					name="floor.area"
					unitName="floorareaUnit"
					label={Adata.list3}
					squareOptions={squareUnitOptions}
					relatedFields={['buildingLength', 'buildingWidth', 'buildingHeight']}
				/>{' '}
				<DashedAreaUnitInput name="coverageArea" unitName="coverageUnit" label={sundarData.body2.coverageArea} />
			</div>
			<div>
				{sundarData.body2.groundCoverage}:
				<DashedLangInput
					name="coverageDetails"
					setFieldValue={setFieldValue}
					value={values.coverageDetails}
					error={errors.coverageDetails}
				/>{' '}
				<div className="div-indent">
					<BlockComponents floorArray={floorArray} blocks={blocks}>
						{(block) => {
							const buildingLength = floorArray.getReducedFieldName('buildingLength', block);
							return (
								<>
									{sundarData.body2.buildingLength}
									<DashedLengthInputWithRelatedUnits
										name={buildingLength}
										unitName="floorareaUnit"
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['floor.area', 'buildingWidth', 'buildingHeight'], buildingLength),
										]}
									/>
								</>
							);
						}}
					</BlockComponents>
				</div>
				{/* <DashedMultiUnitLengthInput
					name="floor.length"
					unitName="floorareaUnit"
					label={sundarData.body2.buildingLength}
					relatedFields={['floor.area', 'floor.width', 'buildingHeight']}
				/> */}
			</div>
			<div>
				{sundarData.body2.floor_5}
				<div className="div-indent">
					<BlockComponents floorArray={floorArray} blocks={blocks}>
						{(block) => {
							const floorNumber = floorArray.getReducedFieldName('floorNumber', block);
							const buildingWidth = floorArray.getReducedFieldName('buildingWidth', block);
							return (
								<>
									{sundarData.body2.floors}
									<DashedLangInput name={floorNumber} setFieldValue={setFieldValue} value={getIn(values, floorNumber)} />
									{sundarData.body2.buildingWidth}
									<DashedLengthInputWithRelatedUnits
										name={buildingWidth}
										unitName="floorareaUnit"
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['floor.area', 'buildingLength', 'buildingHeight'], buildingWidth),
										]}
									/>
								</>
							);
						}}
					</BlockComponents>
				</div>
				{/* {sundarData.body2.floors}:
				<DashedLangInput name="floors" setFieldValue={setFieldValue} value={values.floors} error={errors.floors} />{' '} */}
				{/* <DashedMultiUnitLengthInput
					name="floor.width"
					unitName="floorareaUnit"
					label={sundarData.body2.buildingWidth}
					relatedFields={['floor.area', 'floor.length', 'buildingHeight']}
				/> */}
			</div>
			<div>
				{sundarData.body2.floor_6}
				<div className="div-indent">
					<BlockComponents floorArray={floorArray} blocks={blocks}>
						{(block) => {
							const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
							return (
								<>
									{sundarData.body2.buildingHeight}
									<DashedLengthInputWithRelatedUnits
										name={buildingHeight}
										unitName="floorareaUnit"
										relatedFields={[
											...floorArray.getAllFields(),
											...floorArray.getAllReducedBlockFields(['floor.area', 'buildingLength', 'buildingWidth'], buildingHeight),
										]}
									/>
								</>
							);
						}}
					</BlockComponents>
				</div>
				{/* {sundarData.body2.buildingHeight}:
				<DashedMultiUnitLengthInput name="buildingHeight" unitName="floorareaUnit" relatedFields={['floor.area', 'floor.length', 'floor.height']} /> */}
			</div>
			<div>
				<PurposeOfConstructionRadio
					space={true}
					fieldLabel={sundarData.body2.purposeOfConst}
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
				/>
			</div>
			<div>
				<PurposeOfConstructionRadio
					space={true}
					name="bhupayog"
					otherName="bhupayogOther"
					fieldLabel={sundarData.body2.bhupayog}
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
				/>
			</div>{' '}
			<div>
				{sundarData.body2.block}:
				<DashedLangInput name="block" setFieldValue={setFieldValue} value={values.block} error={errors.block} />{' '}
			</div>
			<div>
				<span>
					{sundarData.body2.buildingClass}: {buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
				</span>
			</div>
			{sundarData.body2.requiredDistance}: <DashedUnitInput name="requiredDistance" unitName="sadakAdhikarUnit" />
			<br />
			<DashedUnitInput name="publicPropertyDistance" unitName="publicPropertyUnit" label={sundarData.body2.setBack} />
			<div>
				{sundarData.body2.anya}:
				<EbpsTextareaField
					placeholder="....."
					name="miscDesc"
					setFieldValue={setFieldValue}
					value={values.miscDesc}
					error={errors.miscDesc}
				/>
			</div>
		</>
	);
};
export const AllowancePaperBodyInaruwa = ({ permitData, userData, values, handleChange, setFieldValue, errors, buildingClass }) => {
	return (
		<>
			<div>
				<Select placeholder="श्री" options={genderOptions} /> {`${permitData.nibedakName} `}
				<br />
				{inaruwaData.theganaInaruwa}
				{permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
				{inaruwaData.content_1}
				<br />
				{inaruwaData.content_2}
			</div>
			<div>
				<span className="indent-span">{inaruwaData.content_3}</span>
				{userData.organization.name}
				{inaruwaData.wardNo}
				{permitData.newWardNo}
				{inaruwaData.sabik}
				{permitData.newMunicipal}
				{permitData.oldMunicipal}
				{inaruwaData.content_4}
				{inaruwaData.content_5}
				{permitData.kittaNo}
				{inaruwaData.content_6}
				{permitData.landArea}
				{permitData.landAreaType}
				{inaruwaData.content_7}
				{permitData.sadak}
				{inaruwaData.content_8}
				<DashedLangInput name="bamojim" setFieldValue={setFieldValue} value={values.bamojim} error={errors.bamojim} />
				{inaruwaData.content_9}
				<DashedLangInput name="name" setFieldValue={setFieldValue} value={values.name} error={errors.name} />
				{inaruwaData.content_10}
			</div>
		</>
	);
};

export const AllowancePaperBodyKamalamai = ({
	permitData,
	userData,
	values,
	handleChange,
	setFieldValue,
	floorArray,
	formattedFloors,
	errors,
	buildingClass,
	floorMax,
}) => {
	return (
		<>
			<div>
				<Select placeholder="श्री" options={genderOptions} /> {`${permitData.nibedakName} `}
				{kamalamaiData.body.yesh}
				{userData.organization.name} {kamalamaiData.body.wardNo} {permitData.newWardNo}
				{kamalamaiData.body.antargat} {permitData.sadak} {kamalamaiData.body.tol} {permitData.kittaNo}
				{kamalamaiData.body.kshetrafal} {permitData.landArea} ({permitData.landAreaType}) {kamalamaiData.body.content}
			</div>
			<div>
				<ConstructionTypeRadio space={true} fieldLabel={kamalamaiData.content.list1} />
			</div>
			<div>
				<DashedMultiUnitLengthInput
					name="plinthLength"
					unitName="plinthLengthUnit"
					relatedFields={['plinthWidth', 'buildingHeight']}
					areaField={'plinthDetails'}
					label={kamalamaiData.content.list2}
				/>
				<DashedMultiUnitLengthInput
					name="plinthWidth"
					unitName="plinthLengthUnit"
					relatedFields={['plinthLength', 'buildingHeight']}
					areaField={'plinthDetails'}
					label={kamalamaiData.content.chaudai}
				/>
				{kamalamaiData.content.tala}
				<DashedLangInput name="floorMax" setFieldValue={setFieldValue} value={values['floorMax']} error={errors.floorMax} />
				<DashedMultiUnitLengthInput
					name="buildingHeight"
					unitName="heightUnit"
					relatedFields={['plinthLength', 'plinthWidth']}
					areaField={'plinthDetails'}
					label={kamalamaiData.content.uchai}
				/>
				<DashedMultiUnitAreaInput
					name="plinthDetails"
					unitName="plinthLengthUnit"
					label={kamalamaiData.content.area}
					squareOptions={squareUnitOptions}
					relatedFields={['plinthLength', 'plinthWidth', 'buildingHeight']}
				/>
			</div>
			<div>
				<PurposeOfConstructionRadio
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
					space={true}
					fieldLabel={`${kamalamaiData.content.list3}:`}
				/>
			</div>
			<div>
				<NirmanTypeAnyaRadio
					fieldLabel={kamalamaiData.content.list4}
					values={values}
					setFieldValue={setFieldValue}
					errors={errors}
					space={true}
				/>
			</div>
			<div>
				{kamalamaiData.content.list5}
				<div className="div-indent">
					<table style={{ textAlign: 'left' }} className="small-font-print-table">
						<thead>
							<th>{kamalamaiData.content.list5_heading1}</th>
							<th>{kamalamaiData.content.list5_heading2}</th>
							<th>{kamalamaiData.content.list5_heading3}</th>
						</thead>
						<tbody>
							<tr>
								<td>{kamalamaiData.content.list5_1}</td>
								<td>
									<DashedMultiUnitLengthInput
										name="batoChodne"
										unitName="batoUnit"
										relatedFields={['batoChodeko']}
										label={kamalamaiData.content.blank}
									/>
								</td>
								<td>
									<DashedMultiUnitLengthInput
										name="batoChodeko"
										unitName="batoUnit"
										relatedFields={['batoChodne']}
										label={kamalamaiData.content.blank}
									/>
								</td>
							</tr>
							<tr>
								<td>{kamalamaiData.content.list5_2}</td>
								<td>
									<DashedMultiUnitLengthInput
										name="bidhutChodne"
										unitName="bidhutUnit"
										relatedFields={['bidhutChodeko']}
										label={kamalamaiData.content.blank}
									/>
								</td>
								<td>
									<DashedMultiUnitLengthInput
										name="bidhutChodeko"
										unitName="bidhutUnit"
										relatedFields={['bidhutChodne']}
										label={kamalamaiData.content.blank}
									/>
								</td>
							</tr>
							<tr>
								<td>{kamalamaiData.content.list5_3}</td>
								<td>
									<DashedMultiUnitLengthInput
										name="nadiChodne"
										unitName="nadiUnit"
										relatedFields={['nadiChodeko']}
										label={kamalamaiData.content.blank}
									/>
								</td>
								<td>
									<DashedMultiUnitLengthInput
										name="nadiChodeko"
										unitName="nadiUnit"
										relatedFields={['nadiChodne']}
										label={kamalamaiData.content.blank}
									/>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div>
				<div
					style={{
						display: 'flex',
					}}
				>
					<div>{kamalamaiData.content.list6}</div>
					<div style={{ display: 'flex' }}>
						{values.surrounding &&
							values.surrounding.map((index, i) => (
								<div style={{ marginLeft: '5px' }}>
									{surrounding.find((fl) => fl.direction === index.side).value}{' '}
									<DashedLangInput
										name={`surrounding.${i}.kittaNo`}
										setFieldValue={setFieldValue}
										value={getIn(values, `surrounding.${i}.kittaNo`)}
										handleChange={handleChange}
										error={getIn(errors, `surrounding.${i}.kittaNo`)}
									/>{' '}
								</div>
							))}
					</div>
				</div>
			</div>
			<div>
				{kamalamaiData.content.list7}
				<div className="div-indent">
					{kamalamaiData.content.list7_1}
					<div>
						<DashedUnitInput name="chodekoDuri" unitName="chodekoUnit" label={kamalamaiData.content.list7_2_1} />
						{kamalamaiData.content.list7_2_2}
					</div>
				</div>
			</div>
			<div>
				{kamalamaiData.content.list8}
				<div className="div-indent">
					<AllowanceKamalamaiBlockFloors floorArray={floorArray} formattedFloors={formattedFloors} />
				</div>
			</div>
			<div>
				{kamalamaiData.content.list9}
				<EbpsTextareaField
					placeholder="....."
					name="miscDesc"
					setFieldValue={setFieldValue}
					value={values.miscDesc}
					error={errors.miscDesc}
				/>
			</div>
		</>
	);
};

export const Subject = () => {
	return (
		<SectionHeader>
			<h3 className="underline">{Adata.title}</h3>
			<h3 className="underline">{Adata.patra}</h3>
		</SectionHeader>
	);
};
export const InaruwaSubject = () => {
	return (
		<SectionHeader>
			<h3>
				<span>{inaruwaData.gharTitleInaruwa}</span>
				<p>
					<span>{inaruwaData.titleInaruwa}</span>
				</p>
				<p>
					<span>{inaruwaData.subTitleInaruwa}</span>
				</p>
			</h3>
		</SectionHeader>
	);
};
export const KamalamaiSubject = () => {
	return (
		<SectionHeader>
			<h3 className="underline">{kamalamaiData.kamalamaiTitle}</h3>
			<h3 className="underline">{kamalamaiData.kamalamaiSubTitle}</h3>
		</SectionHeader>
	);
};

export const KageyshowriSubject = () => {
	return (
		<SectionHeader>
			<h3 className="underline">{kageyshowriData.kamalamaiTitle}</h3>
			<h3 className="underline">{kageyshowriData.kamalamaiSubTitle}</h3>
		</SectionHeader>
	);
};
export const AllowanceFooterInaruwa = () => {
	return (
		<div>
			<p className="neighbours-para">
				<strong
					style={{
						marginLeft: '70px',
						marginRight: '70px',
					}}
				>
					{Adata.list16}
				</strong>
			</p>
			<br />
			<FooterSignature designations={inaruwaData.footerSignature} />
		</div>
	);
};

export const AllowanceFooter = () => {
	return (
		<div>
			<p className="neighbours-para">
				<strong
					style={{
						marginLeft: '70px',
						marginRight: '70px',
					}}
				>
					{Adata.list16}
				</strong>
			</p>
			<br />
			<div
				className="firstalign"
				style={{
					marginLeft: '70px',
					marginRight: '70px',
					textAlign: 'center',
				}}
			>
				<div>
					<p className="neighbours-para">
						<span className="ui input dashedForm-control" />
					</p>
					<p className="neighbours-para">{Adata.list17}</p>
				</div>
				{/* <div>
                                                <p>
                                                    <span className='ui input dashedForm-control' />
                                                </p>
                                                <p>{Adata.list18}</p>
                                            </div> */}
			</div>
		</div>
	);
};

export const AllowanceFooterBirtamod = () => {
	return (
		<div>
			<p>{Adata.list10}</p>
			<p className="neighbours-para">
				{Adata.list11}
				<span className="ui input dashedForm-control" />
			</p>
			<div className="signature-div">
				<span className="ui input signature-placeholder" />
				<p>{Adata.list12}</p>
			</div>
		</div>
	);
};

export const AllowanceFooterSundarHaraicha = () => {
	return (
		<div>
			<div className="ol-div">
				<ol>
					{sundarData.footerList.map((data, index) => (
						<li key={index}>{data}</li>
					))}
				</ol>
			</div>
			<FooterSignature designations={sundarData.footerSignature} />
		</div>
	);
};

export const AllowancePaperInaruwaFloorTable = ({ setFieldValue, values, errors }) => {
	return (
		<>
			<SectionHeader>
				<h3 className="underline">{inaruwaData.tapasil}</h3>
			</SectionHeader>
			<Table fixed className="certificate-ui-table">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell rowSpan="3">{inaruwaData.inaruwaTable.header_1}</Table.HeaderCell>
						<Table.HeaderCell colSpan="4">{inaruwaData.inaruwaTable.header_2}</Table.HeaderCell>
						<Table.HeaderCell rowSpan="3">{inaruwaData.inaruwaTable.header_8}</Table.HeaderCell>
					</Table.Row>
					<Table.Row>
						<Table.HeaderCell rowSpan="2">{inaruwaData.inaruwaTable.header_3}</Table.HeaderCell>
						<Table.HeaderCell rowSpan="2">{inaruwaData.inaruwaTable.header_4}</Table.HeaderCell>
						<Table.HeaderCell colSpan="2">{inaruwaData.inaruwaTable.header_5}</Table.HeaderCell>
					</Table.Row>
					<Table.Row>
						<Table.HeaderCell>{inaruwaData.inaruwaTable.header_6}</Table.HeaderCell>
						<Table.HeaderCell>{inaruwaData.inaruwaTable.header_7}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_1}
							<DashedLangInput
								compact={true}
								name="naksaKar"
								setFieldValue={setFieldValue}
								value={values.naksaKar}
								error={errors.naksaKar}
							/>
						</Table.Cell>
						<Table.Cell colSpan="2">
							{inaruwaData.inaruwaTable.body_2}
							<DashedAreaUnitInput compact={true} name="area" unitName="areaUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedAreaUnitInput compact={true} name="sabikArea" unitName="areaUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedAreaUnitInput compact={true} name="haalArea" unitName="areaUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								name="surjaminNaksa"
								setFieldValue={setFieldValue}
								value={values.surjaminNaksa}
								error={errors.surjaminNaksa}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_3}
							<DashedLangInput
								compact={true}
								name="rashidNumber"
								setFieldValue={setFieldValue}
								value={values.rashidNumber}
								error={errors.rashidNumber}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_4}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaGround"
								setFieldValue={setFieldValue}
								value={values.mapdandaGround}
								error={errors.mapdandaGround}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="sabikGround"
								setFieldValue={setFieldValue}
								value={values.sabikGround}
								error={errors.sabikGround}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="haalGround"
								setFieldValue={setFieldValue}
								value={values.haalGround}
								error={errors.haalGround}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								name="surjaminRasid"
								setFieldValue={setFieldValue}
								value={values.surjaminRasid}
								error={errors.surjaminRasid}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_5}
							<DashedLangInput
								compact={true}
								name="amtOfDharauti"
								setFieldValue={setFieldValue}
								value={values.amtOfDharauti}
								error={errors.amtOfDharauti}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_6}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaHeight"
								setFieldValue={setFieldValue}
								value={values.mapdandaHeight}
								error={errors.mapdandaHeight}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="oldHeight" unitName="heightUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="newHeight" unitName="heightUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								name="surjaminDharauti"
								setFieldValue={setFieldValue}
								value={values.surjaminDharauti}
								error={errors.surjaminDharauti}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_3}
							<DashedLangInput
								compact={true}
								name="areaRasid"
								setFieldValue={setFieldValue}
								value={values.areaRasid}
								error={errors.areaRasid}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_7}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaArea"
								setFieldValue={setFieldValue}
								value={values.mapdandaArea}
								error={errors.mapdandaArea}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedAreaUnitInput compact={true} name="oldArea" unitName="areaUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedAreaUnitInput compact={true} name="newArea" unitName="areaUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								ame="kulFloorRasid"
								setFieldValue={setFieldValue}
								value={values.kulFloorRasid}
								error={errors.kulFloorRasid}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_8}
							<DashedLangInput
								compact={true}
								name="napugKar"
								setFieldValue={setFieldValue}
								value={values.napugKar}
								error={errors.napugKar}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_9}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaSetBack"
								setFieldValue={setFieldValue}
								value={values.mapdandaSetBack}
								error={errors.mapdandaSetBack}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="oldSetBack" unitName="setBackUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="newSetBack" unitName="setBackUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								name="surjaminNapugKar"
								setFieldValue={setFieldValue}
								value={values.surjaminNapugKar}
								error={errors.surjaminNapugKar}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_3}
							<DashedLangInput
								compact={true}
								name="lineRasid"
								setFieldValue={setFieldValue}
								value={values.lineRasid}
								error={errors.lineRasid}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_10}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaLine"
								setFieldValue={setFieldValue}
								value={values.mapdandaLine}
								error={errors.mapdandaLine}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="oldLine" unitName="lineUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="newLine" unitName="lineUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput name="bidhutRasid" setFieldValue={setFieldValue} value={values.bidhutRasid} error={errors.bidhutRasid} />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_5}
							<DashedLangInput
								compact={true}
								name="nadiDharauti"
								setFieldValue={setFieldValue}
								value={values.nadiDharauti}
								error={errors.nadiDharauti}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_11}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="publicPropertyRequiredDistance"
								setFieldValue={setFieldValue}
								value={values.publicPropertyRequiredDistance}
								error={errors.publicPropertyRequiredDistance}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="oldNadi" unitName="nadiUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="newNadi" unitName="nadiUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								name="nadiDharauti"
								setFieldValue={setFieldValue}
								value={values.nadiDharauti}
								error={errors.nadiDharauti}
							/>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>
							{inaruwaData.inaruwaTable.body_3}
							<DashedLangInput
								compact={true}
								name="lightRasid"
								setFieldValue={setFieldValue}
								value={values.lightRasid}
								error={errors.lightRasid}
							/>
						</Table.Cell>
						<Table.Cell>{inaruwaData.inaruwaTable.body_12}</Table.Cell>
						<Table.Cell>
							<DashedLangInput
								compact={true}
								name="mapdandaLight"
								setFieldValue={setFieldValue}
								value={values.mapdandaLight}
								error={errors.mapdandaLight}
							/>
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="oldLight" unitName="nadiUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedUnitInput compact={true} name="newLight" unitName="nadiUnit" />
						</Table.Cell>
						<Table.Cell>
							<DashedLangInput name="tallaRasid" setFieldValue={setFieldValue} value={values.tallaRasid} error={errors.tallaRasid} />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>{inaruwaData.inaruwaTable.body_13}</Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
						<Table.Cell></Table.Cell>
					</Table.Row>
				</Table.Body>
			</Table>
			<span>
				{inaruwaData.anya}
				<EbpsTextareaField name="anya" setFieldValue={setFieldValue} value={values['anya']} />
			</span>
		</>
	);
};
export const AllowancePaperSchema = Yup.object().shape(
	Object.assign({
		allowanceDate: validateNullableNepaliDate,
		// gharNo: validateString,
	})
);
export const AllowancePaperInaruwaSchema = Yup.object().shape(
	Object.assign({
		rashidNumber: validateNumber,
		amtOfDharauti: validateNumber,
	})
);

export const AllowancePaperSundarHaraichaSchema = Yup.object().shape(
	Object.assign({
		allowanceDate: validateNullableNepaliDate,
		applicantDateBS: validateNormalNepaliDateWithRange,
		floors: validateNullableNumber,
	})
);
