import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { LandAreaCalculationSection } from '../NapiNirikshyanPratibedan';
import { DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } from '../../../../utils/constants';
import { prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { PrintParams } from '../../../../utils/printUtils';
class LandAreaCalculationOfSitePlanComponent extends Component {
	constructor(props) {
		super(props);
		const initVal = prepareMultiInitialValues(
			{
				obj: {
					sideAUnit: DEFAULT_UNIT_LENGTH,
					sideBUnit: DEFAULT_UNIT_LENGTH,
					sideCUnit: DEFAULT_UNIT_LENGTH,
					areaUnit: DEFAULT_UNIT_AREA,
					landAreaFeetUnit: DEFAULT_UNIT_AREA,

					landAreaCalculation: [{ sn: '1', sideA: 0, sideB: 0, sideC: 0, area: 0, shape: 'Rectangle', sym: '' }],
				},
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { fromUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, prevData } = this.props;
		return (
			<div>
				<Formik
					initialValues={this.state.initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.LandAreaCalculation, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, setFieldValue, errors, validateForm, handleSubmit }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="form-section">
								<LandAreaCalculationSection setFieldValue={setFieldValue} values={values} errors={errors} />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={fromUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const LandAreaCalculationOfSitePlan = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.LandAreaCalculation).setForm().getParams()]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
			...PrintParams.REMOVE_ON_PRINT,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <LandAreaCalculationOfSitePlanComponent {...props} parentProps={parentProps} />}
	/>
);
export default LandAreaCalculationOfSitePlan;
