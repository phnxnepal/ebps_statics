import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { buildingFinishCertidicateData } from '../../../../utils/data/BuildingFinishCertificateViewBiratnagarData';
import { Formik, getIn } from 'formik';
import { Form } from 'semantic-ui-react';
import { LetterHeadPhoto } from '../../../shared/LetterHead';
import { FlexSpaceBetween } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangInput, DashedNormalInput, DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedSelect } from '../../../shared/Select';
import { getJsonData, prepareMultiInitialValues, surroundingMappingFlat, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';

import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { FinishCertificateBiratnagarBlockFloors } from '../certificateComponents/CertificateFloorInputs';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import {
	validateNullableOfficialNumbers,
	validateNullableNepaliDate,
} from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { DashedMultiUnitLengthInput, DashedLengthInputWithRelatedUnits, DashedAreaUnitInput } from '../../../shared/EbpsUnitLabelValue';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { PrintParams } from '../../../../utils/printUtils';
import { BlockComponentsV2 } from '../../../shared/formComponents/BlockFloorComponents';
const { object } = require('yup');

const reqData = buildingFinishCertidicateData;

const shriShrimanShrimatiOptions = [
	{
		key: reqData.shir,
		text: reqData.shir,
		value: reqData.shir,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shushri,
		text: reqData.shushri,
		value: reqData.shushri,
	},
];
const intialVal = {
	pasa: '',
	chana: '',
	miti: '',
	shriShrimanShrimati: shriShrimanShrimatiOptions[0].value,
	nimnaBamojim: '',
	gharNo: '',
	buildingClass: '',
	surrounding: '',
	nirmanStructuralSystem: '',
	chalaniNumber: '',
	chalaniNumberMiti: '',
	sixNakshaPassNagariBanayekoMiti: '',
};
const validate = object({
	pasa: validateNullableOfficialNumbers,
	chana: validateNullableOfficialNumbers,
	miti: validateNullableNepaliDate,
	allowanceDate: validateNullableNepaliDate,
	noNaksaPassDate: validateNullableNepaliDate,
});

class BuildingFinishCertificateViewBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, permitData, DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } = this.props;
		const allPaper = getJsonData(otherData.allowancePaper);
		const prabidhik = getJsonData(otherData.prabhidik);
		const floorArray = new FloorBlockArray(this.props.permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const designAppr = otherData.designApproval;
		const mapTech = getJsonData(otherData.mapTech);
		let initialValue = prepareMultiInitialValues(
			{
				obj: intialVal,
				reqFields: [],
			},
			{
				obj: {
					miti: getCurrentDate(true),
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					coverageUnit: DEFAULT_UNIT_AREA,
					highTensionUnit: DEFAULT_UNIT_LENGTH,
				},
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: ['surrounding', 'dhalNikasArrangement'],
			},
			{
				obj: mapTech,
				reqFields: [
					'publicPropertyRequiredDistance',
					'publicPropertyDistance',
					'publicPropertyUnit',
					'highTension',
					'highTensionUnit',
					'coverageArea',
					'coverageDetails',
					'coverageUnit',
				],
			},
			{
				obj: prabidhik,
				reqFields: ['namedMapdanda', 'namedAdhikar', 'requiredDistance', 'sadakAdhikarUnit'],
			},
			{
				obj: {
					...floorArray.getInitialValue(null, 'buildingArea', floorArray.getSumOfAreas()),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('nepaliCountName', 'noOfStorey', floorArray.getTopFloor()),
					floor: floorArray.getMultipleInitialValues(['area'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: allPaper,
				reqFields: ['gharNo', 'chalaniNumber', 'allowanceDate'],
			},
			{
				obj: designAppr,
				reqFields: ['buildingClass'],
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValue,
			allPaper,
			floorArray,
			formattedFloors,
		};
	}
	render() {
		const { initialValue, floorArray, formattedFloors } = this.state;
		const { prevData, permitData, userData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div>
				<Formik
					initialValues={initialValue}
					validationSchema={validate}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.buildingFinish, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, errors, setFieldValue, handleSubmit, validateForm, handleChange }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="print-small-font">
								<LetterHeadPhoto userInfo={userData} photo={permitData.photo} />
								<FlexSpaceBetween>
									<div>
										<div>
											{reqData.pasa}
											<DashedNormalInput
												name="pasa"
												value={values.pasa}
												error={errors.pasa}
												setFieldValue={setFieldValue}
												handleChange={handleChange}
											/>
										</div>
										<div>
											{reqData.chana}
											<DashedNormalInput
												name="chana"
												value={values.chana}
												error={errors.chana}
												setFieldValue={setFieldValue}
												handleChange={handleChange}
											/>
										</div>
									</div>
									<div>
										{reqData.miti}
										<CompactDashedLangDate name="miti" value={values.miti} error={errors.miti} setFieldValue={setFieldValue} />
									</div>
								</FlexSpaceBetween>
								<SectionHeader>
									<h2>{reqData.bisaye}</h2>
								</SectionHeader>
								<br />
								<div>
									<div>
										<DashedSelect
											name="shriShrimanShrimati"
											options={shriShrimanShrimatiOptions}
											value={values.shriShrimanShrimati}
											error={errors.shriShrimanShrimati}
											setFieldValue={setFieldValue}
										/>
										{reqData.le}
										{permitData.newMunicipal}
										{reqData.wardNo}
										{permitData.newWardNo}

										{reqData.nimnaBamojim}
										<CompactDashedLangInput
											name="nimnaBamojim"
											value={values.nimnaBamojim}
											error={errors.nimnaBamojim}
											setFieldValue={setFieldValue}
										/>
										{reqData.paritNaksha}
									</div>
									<div>
										{reqData.oneJaggaBhani}
										{permitData.applicantName}
									</div>
									<div>
										{reqData.twoGharDhani}
										{permitData.naksawalaName}
									</div>
									<div>
										{reqData.threeJaggaBibaran}
										{reqData.sabik}
										{permitData.oldMunicipal}
										{reqData.halkoWadaNumber}
										{permitData.newWardNo}
										{reqData.gharNo}
										<CompactDashedLangInput
											name="gharNo"
											value={values.gharNo}
											error={errors.gharNo}
											setFieldValue={setFieldValue}
										/>
										{reqData.sadakKoNaam}
										{permitData.nearestLocation}
										{reqData.kiNa}
										{permitData.kittaNo}
										{reqData.chetrafal}
										{permitData.landArea}
										{` (${permitData.landAreaType})`}
										{reqData.bhuUpayogChetra}
										{permitData.purposeOfConstruction}
									</div>
									<div>
										{reqData.fourJaggaKoCharKilla}

										{permitData.surrounding &&
											permitData.surrounding.map((index, i) => (
												<React.Fragment key={i}>
													{surroundingMappingFlat.find((fl) => fl.side === index.side).value}{' '}
													<DashedNormalInput
														name={`surrounding.${i}.sandhiyar`}
														value={getIn(values, `surrounding.${i}.sandhiyar`)}
														handleChange={handleChange}
														error={getIn(errors, `surrounding.${i}.sandhiyar`)}
													/>
												</React.Fragment>
											))}
									</div>
									<div>
										{reqData.fiveBhawanKoBargikaran}
										<RadioInput name="buildingClass" label={reqData.ka} option="A" space={true} />
										<RadioInput name="buildingClass" label={reqData.kha} option="B" space={true} />
										<RadioInput name="buildingClass" label={reqData.ga} option="C" space={true} />
										<RadioInput name="buildingClass" label={reqData.gha} option="D" space={true} />{' '}
										<span> {reqData.nirmanStructuralSystem}</span>
										<CompactDashedLangInput name="nirmanStructuralSystem" />
									</div>
									<div>
										{reqData.sixNirmanKaryaIjajatPramanPatraNo}
										<CompactDashedLangInput
											name="chalaniNumber"
											value={values.chalaniNumber}
											error={errors.chalaniNumber}
											setFieldValue={setFieldValue}
										/>
										{reqData.miti}
										<CompactDashedLangDate
											name="allowanceDate"
											value={values.allowanceDate}
											error={errors.allowanceDate}
											setFieldValue={setFieldValue}
										/>

										{reqData.sixNakshaPassNagariBanayeko}
										<CompactDashedLangDate
											name="noNaksaPassDate"
											value={values.noNaksaPassDate}
											error={errors.noNaksaPassDate}
											setFieldValue={setFieldValue}
										/>
									</div>
									<div>
										<div style={{ display: 'flex' }}>
											<div style={{ paddingRight: '0.5em' }}>७. </div>
											<div>
												<FinishCertificateBiratnagarBlockFloors
													floorArray={floorArray}
													formattedFloors={formattedFloors}
													values={values}
													errors={errors}
													setFieldValue={setFieldValue}
												/>
											</div>
										</div>
									</div>

									<div>
										{reqData.eightBhawanBanekoCoverArea}
										<DashedAreaUnitInput name="coverageArea" unitName="coverageUnit" />
										{reqData.groundCoverage}
										<DashedNormalInputIm name="coverageDetails" /> %
									</div>
									<div style={{ display: 'flex' }}>
										<div style={{ paddingRight: '0.5em' }}>{reqData.nine}</div>
										<div>
											<BlockComponentsV2 floorArray={floorArray} blocks={floorArray.getBlocks()}>
												{(block) => {
													const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
													const noOfStorey = floorArray.getReducedFieldName('noOfStorey', block);
													return (
														<div key={block}>
															{reqData.nineBanekoBhawanKoUchai}
															{block ? `Block ${block}` : ''}
															<DashedLengthInputWithRelatedUnits
																name={buildingHeight}
																unitName="floorUnit"
																relatedFields={[
																	...floorArray.getAllFields(),
																	...floorArray.getAllReducedBlockFields(
																		['buildingArea', 'buildingHeight'],
																		buildingHeight
																	),
																]}
															/>
															{reqData.tallaSankhya}
															<DashedLangInput
																name={noOfStorey}
																value={getIn(values, noOfStorey)}
																error={getIn(errors, noOfStorey)}
																setFieldValue={setFieldValue}
															/>
														</div>
													);
												}}
											</BlockComponentsV2>
										</div>
									</div>

									<div>
										{reqData.tenGharBanekoPlotSangaJodiyekoChodnuParneDuri}
										<DashedMultiUnitLengthInput
											name="requiredDistance"
											unitName="sadakAdhikarUnit"
											relatedFields={['namedAdhikar', 'namedMapdanda']}
										/>
										{reqData.sadakKoBichBataChodiyekoDuri}
										<DashedMultiUnitLengthInput
											name="namedAdhikar"
											unitName="sadakAdhikarUnit"
											relatedFields={['requiredDistance', 'namedMapdanda']}
										/>
									</div>
									<div>
										{reqData.elevenBijuliKoTarNajik}
										<DashedMultiUnitLengthInput
											name="highTension.0.value"
											unitName="highTensionUnit"
											relatedFields={['highTension.1.value']}
										/>
										{reqData.BijuliChodekoDuri}
										<DashedMultiUnitLengthInput
											name="highTension.1.value"
											unitName="highTensionUnit"
											relatedFields={['highTension.0.value']}
										/>
									</div>
									<div>
										{reqData.twelveNadiKuloKahareKoKinarBhayemaChodnuParneDuri}
										<DashedMultiUnitLengthInput
											name="publicPropertyRequiredDistance"
											unitName="publicPropertyUnit"
											relatedFields={['publicPropertyDistance']}
										/>
										{reqData.nadiChodekoDuri}
										<DashedMultiUnitLengthInput
											name="publicPropertyDistance"
											unitName="publicPropertyUnit"
											relatedFields={['publicPropertyRequiredDistance']}
										/>
									</div>
									<div>
										{reqData.thirteenDhalNikas}
										<DashedLangInput
											name="dhalNikasArrangement"
											value={values.dhalNikasArrangement}
											error={errors.dhalNikasArrangement}
											setFieldValue={setFieldValue}
										/>
									</div>
									<div>
										{reqData.fourteenAanyaKunai}
										<EbpsTextArea rows={10} name="anya" value={values.anya} error={errors.anya} setFieldValue={setFieldValue} />
									</div>
									<div>
										{reqData.fifteenGharBanekoAawa}
										<CompactDashedLangInput
											name="gharkoAawa"
											value={values.gharkoAawa}
											error={errors.gharkoAawa}
											setFieldValue={setFieldValue}
										/>

										{reqData.sixteenSampannaIjajatNa}
										<CompactDashedLangInput
											name="ijajatNumber"
											value={values.ijajatNumber}
											error={errors.ijajatNumber}
											setFieldValue={setFieldValue}
										/>
									</div>
								</div>
								<FooterSignatureMultiline
									designations={[
										[reqData.gharDhaniKoSahi],
										[reqData.sthalgatNirikxak, reqData.subEngineer],
										[reqData.jachGarneEngineer, reqData.gharNakshaPramukhSakha],
										[reqData.pramanPatraPradanGarne, reqData.pramukhPrasaasaliyaAdhikrit],
										[reqData.phatWalaKoSahi],
									]}
								/>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const BuildingFinishCertificateViewBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.buildingFinish).setForm().getParams(),
			new ApiParam(api.allowancePaper, 'allowancePaper').getParams(),
			new ApiParam(api.designApproval, 'designApproval').getParams(),
			new ApiParam(api.prabhidikPratibedhanPesh, 'prabidhik').getParams(),
			new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams(),
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <BuildingFinishCertificateViewBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default BuildingFinishCertificateViewBiratnagar;
