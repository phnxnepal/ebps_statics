import React, { Component } from 'react';
import { Form, TextArea } from 'semantic-ui-react';
import { Formik, getIn } from 'formik';
import { SurjaminMuchulkaViewBiratnagarData } from '../../../../utils/data/SurjaminMuchulkaViewBiratnagarData';
import { SectionHeader } from '../../../uiComponents/Headers';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { surroundingMappingFlat, getJsonData, handleSuccess } from '../../../../utils/dataUtils';
import { checkError } from '../../../../utils/dataUtils';
import { getUnitValue } from '../../../../utils/functionUtils';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { DashedSelect } from '../../../shared/Select';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { EbpsTextArea } from '../../../shared/EbpsForm';
import { PrintParams } from '../../../../utils/printUtils';

const reqData = SurjaminMuchulkaViewBiratnagarData;
const banauneBanisakeko = [
	{
		key: reqData.banaune,
		text: reqData.banaune,
		value: reqData.banaune,
	},
	{
		key: reqData.banisakeko,
		text: reqData.banisakeko,
		value: reqData.banisakeko,
	},
];
class SurjaminMuchulkaViewBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData } = this.props;
		const mapTech = getJsonData(otherData.mapTech);

		this.state = {
			mapTech,
		};
	}
	render() {
		const { permitData, postAction, formUrl, hasSavePermission, userData, hasDeletePermission, isSaveDisabled, prevData } = this.props;
		const { mapTech } = this.state;

		return (
			<Formik
				initialValues={{ lambaiChaudaiKoBhawan: '', banauneBanisakeko: banauneBanisakeko[0].value, nakshaAnusar: '', tapasilTextArea: '' }}
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);

					values.applicationNo = permitData.applicantNo;

					try {
						await postAction(api.surjaminMuchulka, values, true);

						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, handleSubmit, errors, validateForm, values, setFieldValue }) => (
					<div>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}

						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
							<SectionHeader>
								<h1>{userData.organization.name}</h1>
								<h3>
									{userData.organization.name}, {userData.organization.province}, {reqData.nepal} 
								</h3>
							</SectionHeader>
								<br />
								<br />
								<div>
									{reqData.sabik}
									{/* ----- */}
									{permitData.newMunicipal}
									{reqData.wardNo}
									{/* ----- */}
									{permitData.oldWardNo}
									{reqData.kittaNumber}
									{/* ----- */}
									{permitData.kittaNo}
									{reqData.jaggaBigaha}
									{/* ----- */}
									{permitData.landArea}
									{permitData.landAreaType}{' '}
									{permitData.surrounding &&
										permitData.surrounding.map((surr, idx) => (
											<>
												{surroundingMappingFlat.find((fl) => fl.side === surr.side).value}{' '}
												{getIn(permitData, `surrounding.${idx}.feet`)}{' '}
												{getUnitValue(getIn(permitData, `surrounding.${idx}.sideUnit`))}
												{/* <DashedUnitInput name={`surrounding.${idx}.feet`} unitName={`surrounding.${idx}.sideUnit`} /> */}
											</>
										))}
									{reqData.lambai}
									{mapTech.fieldLength}
									{/* ----- */}
									{reqData.chaudai}
									{mapTech.fieldWidth}
									{/* ----- */}
									{reqData.ko}
									{/* ----- */}
									<CompactDashedLangInput
										name="lambaiChaudaiKoBhawan"
										value={values.lambaiChaudaiKoBhawan}
										error={errors.lambaiChaudaiKoBhawan}
										setFieldValue={setFieldValue}
									/>
									{/* -------- */}
									<DashedSelect name="banauneBanisakeko" options={banauneBanisakeko} />
									{/* -------- */}
									{reqData.nakshaWadaNumber}
									{permitData.newWardNo}
									{/* -------- */}
									{reqData.basneShree}
									{permitData.applicantName}
									{/* ----- */}
									{reqData.nakshaAnusar}
									<CompactDashedLangInput
										name="nakshaAnusar"
										value={values.nakshaAnusar}
										error={errors.nakshaAnusar}
										setFieldValue={setFieldValue}
									/>
									{/* ------- */}
									{reqData.dataline}
									<br />
									<br />
									<SectionHeader>
										<h3 className="underline">{reqData.tapasil}</h3>
									</SectionHeader>
									<br />
									<EbpsTextArea
										name="tapasilTextArea"
										placeholder={reqData.tapasil}
										value={values.tapasilTextArea}
										error={errors.tapasilTextArea}
										setFieldValue={setFieldValue}
									/>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					</div>
				)}
			</Formik>
		);
	}
}
const SurjaminMuchulkaViewBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.surjaminMuchulka).setForm().getParams(), new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams()]}
		onBeforeGetContent={{ ...PrintParams.TEXT_AREA, ...PrintParams.DASHED_INPUT }}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SurjaminMuchulkaViewBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default SurjaminMuchulkaViewBiratnagar;
