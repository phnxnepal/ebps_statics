import React, { Component } from 'react';
import { Form, Table } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { LayoutIjajat } from '../../../../utils/data/LayoutPratibedanIjajatData';
import { validateNullableNumber, validateNepaliDate, validateString, validateNullableZeroNumber } from '../../../../utils/validationUtils';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess, getRelatedFields } from '../../../../utils/dataUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import api from '../../../../utils/api';
import { showToast } from '../../../../utils/functionUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangDateField, CompactDashedLangDate } from '../../../shared/DateField';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedLangInputWithSlash, DashedLangInput } from '../../../shared/DashedFormInput';
import { ShreeDropdown } from '../formComponents/ShreeDropdown';
import { DashedLengthInputWithRelatedUnits, UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { TableInputIm } from '../../../shared/TableInput';
// import MultifieldUnitDropDown from '../../../shared/MultiFieldUnitDropdown';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { squareUnitOptions } from '../../../../utils/optionUtils';

const LayoutGariIjajat = LayoutIjajat.data;
const floorFields = [
	{ name: 'length', label: LayoutGariIjajat.lambai },
	{ name: 'width', label: LayoutGariIjajat.chaudai },
	{ name: 'area', label: LayoutGariIjajat.bargaFeet },
	{ name: 'height', label: LayoutGariIjajat.uchai },
];
// const relatedField = [
// 	'basementLambai',
// 	'pakkiBhuiGharLambai',
// 	'prathamTallaLambai',
// 	'dosroTallaLambai',
// 	'tesroTallaLambai',
// 	'chauthoTallaLambai',
// 	'pachauTallaLambai',
// 	'pakkiParkhalLambai',
// 	'basementChaudai',
// 	'pakkiBhuiGharChaudai',
// 	'prathamTallaChaudai',
// 	'dosroTallaChaudai',
// 	'tesroTallaChaudai',
// 	'chauthoTallaChaudai',
// 	'pachauTallaChaudai',
// 	'pakkiParkhalChaudai',
// 	'basementUchai',
// 	'pakkiBhuiGharUchai',
// 	'prathamTallaUchai',
// 	'dosroTallaUchai',
// 	'tesroTallaUchai',
// 	'chauthoTallaUchai',
// 	'pachauTallaUchai',
// 	'pakkiParkhalUchai',
// ];
// const areaField = [
// 	'basementBargaFeet',
// 	'pakkiBhuiGharBargaFeet',
// 	'prathamTallaBargaFeet',
// 	'dosroTallaBargaFeet',
// 	'tesroTallaBargaFeet',
// 	'chauthoTallaBargaFeet',
// 	'pachauTallaBargaFeet',
// 	'pakkiParkhalBargaFeet',
// ];
const layoutNames = [
	'naapiLambai',
	'naapiChaudai',
	'fieldLambai',
	'fieldChaudai',
	'mapdandaLambai',
	'purwa',
	'uttar',
	'dakshin',
	'sabikLambai',
	'sabikChaudai',
];
const stringValidator = ['sadakKisim', 'sadakName', 'sadakMapdanda'];
const numberValidator = [
	'basementLambai',
	'pakkiBhuiGharLambai',
	'prathamTallaLambai',
	'dosroTallaLambai',
	'tesroTallaLambai',
	'chauthoTallaLambai',
	'pachauTallaLambai',
	'pakkiParkhalLambai',
	'basementChaudai',
	'pakkiBhuiGharChaudai',
	'prathamTallaChaudai',
	'dosroTallaChaudai',
	'tesroTallaChaudai',
	'chauthoTallaChaudai',
	'pachauTallaChaudai',
	'pakkiParkhalChaudai',
	'basementUchai',
	'pakkiBhuiGharUchai',
	'prathamTallaUchai',
	'dosroTallaUchai',
	'tesroTallaUchai',
	'chauthoTallaUchai',
	'pachauTallaUchai',
	'pakkiParkhalUchai',
	'basementBargaFeet',
	'pakkiBhuiGharBargaFeet',
	'prathamTallaBargaFeet',
	'dosroTallaBargaFeet',
	'tesroTallaBargaFeet',
	'chauthoTallaBargaFeet',
	'pachauTallaBargaFeet',
	'pakkiParkhalBargaFeet',
];
const numberValidatorSchema = numberValidator.map((line) => {
	return {
		[line]: validateNullableNumber,
	};
});
const stringValidatorSchema = stringValidator.map((lines) => {
	return {
		[lines]: validateString,
	};
});

const layoutNepali = ['date', 'allowanceDate', 'layoutMiti', 'layoutDate'];
const LayoutSchema = layoutNames.map((rows) => {
	return {
		[rows]: validateNullableNumber,
	};
});
const LayoutNepaliSchema = layoutNepali.map((rows) => {
	return {
		[rows]: validateNepaliDate,
	};
});
const LayoutNameSchema = Yup.object().shape(
	Object.assign(...LayoutSchema, ...LayoutNepaliSchema, ...stringValidatorSchema, ...numberValidatorSchema, {
		floor: Yup.array().of(
			Yup.object().shape({
				length: validateNullableZeroNumber,
				width: validateNullableZeroNumber,
				height: validateNullableZeroNumber,
				area: validateNullableZeroNumber,
			})
		),
	})
);
class LayoutGariPratibedanIjajatBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { permitData, prevData, otherData, DEFAULT_UNIT_LENGTH } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const json_data = getJsonData(prevData);
		const allowance = getJsonData(otherData.allowance);
		initVal = prepareMultiInitialValues(
			{
				obj: {
					surrUnit: DEFAULT_UNIT_LENGTH,
					naapiUnit: DEFAULT_UNIT_LENGTH,
					fieldUnit: DEFAULT_UNIT_LENGTH,
					mapdandaUnit: DEFAULT_UNIT_LENGTH,
					sabikUnit: DEFAULT_UNIT_LENGTH,
					bibaranUnit: DEFAULT_UNIT_LENGTH,
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height', 'label', 'floor'], formattedFloors),
					floorUnit: floorArray.getFloorUnit(),
				},
				reqFields: [],
			},
			{
				obj: allowance,
				reqFields: ['allowanceDate'],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.date)) {
			initVal.date = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { permitData, userData, prevData, errors: reduxErrors, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const salutationData = [LayoutGariIjajat.layout_salutation_1, LayoutGariIjajat.layout_salutation_2, userData.organization.name];
		return (
			<>
				<Formik
					initialValues={initVal}
					validationSchema={LayoutNameSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.LayoutGariPratibedanIjaja}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterSalutation lines={salutationData} />
									<FlexSingleRight>
										{LayoutGariIjajat.miti}
										<DashedLangDateField
											compact={true}
											name="date"
											value={values.date}
											error={errors.date}
											setFieldValue={setFieldValue}
											inline={true}
										/>
									</FlexSingleRight>
									<SectionHeader>
										<h3 className="underline end-section">{LayoutGariIjajat.title}</h3>
									</SectionHeader>
									<div className="no-margin-field">
										{LayoutGariIjajat.mainData_1}
										<DashedLangInputWithSlash
											name="chaNo"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.chaNo}
											error={errors.chaNo}
										/>
										{LayoutGariIjajat.miti}
										<CompactDashedLangDate
											name="layoutMiti"
											value={values.layoutMiti}
											error={errors.layoutMiti}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_2}
										{userData.organization.name}
										{LayoutGariIjajat.mainData_3}
										{permitData.nibedakTol}
										{LayoutGariIjajat.mainData_4}
										<ShreeDropdown name="shree" options={LayoutGariIjajat.option} />
										{permitData.nibedakName}
										{LayoutGariIjajat.mainData_5}
										<CompactDashedLangDate
											name="allowanceDate"
											value={values.allowanceDate}
											error={errors.allowanceDate}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_6}
										<DashedLangInput name="pasa" setFieldValue={setFieldValue} value={values.pasa} error={errors.pasa} />
										{LayoutGariIjajat.mainData_7}
										<DashedLangInput name="chana" setFieldValue={setFieldValue} value={values.chana} error={errors.chana} />
										{LayoutGariIjajat.mainData_8}
										<CompactDashedLangDate
											name="layoutDate"
											value={values.layoutDate}
											error={errors.layoutDate}
											setFieldValue={setFieldValue}
										/>
										{LayoutGariIjajat.mainData_9}
										<SectionHeader>
											<h4 className="underline">{LayoutGariIjajat.tapasilTitle}</h4>
										</SectionHeader>
										{LayoutGariIjajat.tapasilData_1}
										{permitData.applicantName}
										<br />
										{LayoutGariIjajat.tapasilData_2}
										{permitData.oldMunicipal}
										<br />
										{LayoutGariIjajat.tapasilData_3}
										{permitData.newWardNo}
										{LayoutGariIjajat.tapasilData_4}
										{permitData.kittaNo}
										{LayoutGariIjajat.tapasilData_5}
										{permitData.landArea}
										{permitData.landAreaType}
										<br />
										<DashedLengthInputWithRelatedUnits
											name="naapiLambai"
											label={LayoutGariIjajat.tapasilData_6}
											unitName="naapiUnit"
											relatedFields={['naapiChaudai']}
										/>
										<DashedLengthInputWithRelatedUnits
											name="naapiChaudai"
											label={LayoutGariIjajat.tapasilData_8}
											unitName="naapiUnit"
											relatedFields={['naapiLambai']}
										/>
										<br />
										<DashedLengthInputWithRelatedUnits
											name="fieldLambai"
											label={LayoutGariIjajat.tapasilData_7}
											unitName="fieldUnit"
											relatedFields={['fieldChaudai']}
										/>
										<DashedLengthInputWithRelatedUnits
											name="fieldChaudai"
											label={LayoutGariIjajat.tapasilData_8}
											unitName="fieldUnit"
											relatedFields={['fieldLambai']}
										/>
										<br />

										<label htmlFor="sadakName">{LayoutGariIjajat.sadakName}</label>
										<DashedLangInput
											id="sadakName"
											name="sadakName"
											setFieldValue={setFieldValue}
											error={errors.sadakName}
											value={values.sadakName}
											className="dashedForm-control"
										/>
										<label htmlFor="sadakMapdanda">{LayoutGariIjajat.sadakMapdanda}</label>
										<DashedLangInput
											id="sadakMapdanda"
											name="sadakMapdanda"
											setFieldValue={setFieldValue}
											error={errors.sadakMapdanda}
											value={values.sadakMapdanda}
											className="dashedForm-control"
										/>
										<label htmlFor="sadakKisim">{LayoutGariIjajat.sadakKisim}</label>
										<DashedLangInput
											id="sadakKisim"
											name="sadakKisim"
											setFieldValue={setFieldValue}
											value={values.sadakKisim}
											error={errors.sadakKisim}
										/>
										{values.floor && values.floor.length > 0 && (
											<Table className="certificate-ui-table">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell>{LayoutGariIjajat.bibaran}</Table.HeaderCell>
														{values.floor.map((floor) => (
															<Table.HeaderCell>
																{/**@TODO Fix for blocks */}
																{floor.label && floor.label.floorName}
															</Table.HeaderCell>
														))}
													</Table.Row>
												</Table.Header>
												<Table.Body>
													{floorFields.map(({ name, label }) => (
														<Table.Row key={name}>
															<Table.Cell>
																{name === 'area' ? (
																	<UnitDropdownWithRelatedFields
																		unitName="floorUnit"
																		options={squareUnitOptions}
																		relatedFields={getRelatedFields(null, values.floor, 'floor', [
																			'length',
																			'width',
																			'area',
																			'height',
																		])}
																	/>
																) : (
																	label
																)}
															</Table.Cell>
															{values.floor.map((floor, index) => (
																<Table.Cell key={floor.floor}>
																	<TableInputIm name={`floor.${index}.${name}`} />
																</Table.Cell>
															))}
														</Table.Row>
													))}
												</Table.Body>
											</Table>
										)}

										{/* <Table size="small" collapsing className="certificate-ui-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>
														{LayoutGariIjajat.bibaran}
														<MultifieldUnitDropDown
															unitName="bibaranUnit"
															setFieldValue={setFieldValue}
															relatedFields={relatedField}
															errors={errors}
															values={values}
															areaFields={areaField}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.basement}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.pakkiBhuiGhar}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.prathamTalla}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.dosroTalla}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.tesroTalla}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.chauthoTalla}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.pachauTalla}</Table.HeaderCell>
													<Table.HeaderCell>{LayoutGariIjajat.pakkiParkhal}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell>{LayoutGariIjajat.lambaiTable}</Table.Cell>
													<Table.Cell>
														<TableInputIm name="basementLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiBhuiGharLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="prathamTallaLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="dosroTallaLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="tesroTallaLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="chauthoTallaLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pachauTallaLambai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiParkhalLambai" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{LayoutGariIjajat.chaudai}</Table.Cell>
													<Table.Cell>
														<TableInputIm name="basementChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiBhuiGharChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="prathamTallaChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="dosroTallaChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="tesroTallaChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="chauthoTallaChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pachauTallaChaudai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiParkhalChaudai" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{LayoutGariIjajat.bargaFeet}</Table.Cell>
													<Table.Cell>
														<TableInputIm name="basementBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiBhuiGharBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="prathamTallaBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="dosroTallaBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="tesroTallaBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="chauthoTallaBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pachauTallaBargaFeet" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiParkhalBargaFeet" />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{LayoutGariIjajat.uchai}</Table.Cell>
													<Table.Cell>
														<TableInputIm name="basementUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiBhuiGharUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="prathamTallaUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="dosroTallaUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="tesroTallaUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="chauthoTallaUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pachauTallaUchai" />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name="pakkiParkhalUchai" />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table> */}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const LayoutGariPratibedanIjajatBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.LayoutGariPratibedanIjaja,
				objName: 'layoutGariPratibedanIjaja',
				form: true,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'fields inline-group', 'innerText'],
			param3: ['getElementsByTagName', 'textarea', 'value'],
			param4: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			// param5: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		useInnerRef={true}
		render={(props) => <LayoutGariPratibedanIjajatBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);

export default LayoutGariPratibedanIjajatBiratnagar;
