import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik, getIn } from 'formik';
import { Form, Table, TableBody } from 'semantic-ui-react';
import { pratibedanPeshBare } from '../../../../utils/data/CertificatePratibedanPeshBareData';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleLeft, FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangDateField, CompactDashedLangDate } from '../../../shared/DateField';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { TableInputIm } from '../../../shared/TableInput';
import { FooterSignature } from '../formComponents/FooterSignature';
import {
	validateNullableNormalNumber,
	validateNumber,
	validateNepaliDate,
	validateNullableOfficialReqNumbers,
	validateString,
	validateArrayNullableNumber,
} from '../../../../utils/validationUtils';
import * as Yup from 'yup';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { DEFAULT_UNIT_LENGTH } from '../../../../utils/constants';
import { PrintParams } from '../../../../utils/printUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { BlockFloorComponents } from '../../../shared/formComponents/BlockFloorComponents';
import { UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { squareUnitOptions } from '../../../../utils/optionUtils';

/**
 * @TODO print ko kaam baki cha ani karyalaya ko chana ra miti lai import garna baki cha
 * tyo kaam chai api bane pachi garne
 */
const requiredData = pratibedanPeshBare;
const initialVal = {
	bibaranUnit: DEFAULT_UNIT_LENGTH,
	//form information
	mainDate: '',
	chana: '',
	karyalayakoChana: '',
	jariBhayekoMiti: '',
	newWardNumber: '',
	name: '',
	landIssueDate: '',
	nirmanKisim: '',
};
// const areaField = [
// 	//ghar ko barga feet

// 	'basementGharBargaFeet',
// 	'pakkiBhuiGharBargaFeet',
// 	'prathamGharBargaFeet',
// 	'dosroGharBargaFeet',
// 	'tesroGharBargaFeet',
// 	'chauthoGharBargaFeet',
// 	'pachauGharBargaFeet',
// ];
// const relatedField = [
// 	//ghar ko lambai
// 	'basementLambai',
// 	'pakkiBhuiGharLambai',
// 	'prathamLambai',
// 	'dosroLambai',
// 	'tesroLambai',
// 	'chauthoLambai',
// 	'pachauLambai',
// 	//ghar ko chaudai

// 	'basementGharChaudai',
// 	'pakkiBhuiGharChaudai',
// 	'prathamGharChaudai',
// 	'dosroGharChaudai',
// 	'tesroGharChaudai',
// 	'chauthoGharChaudai',
// 	'pachauGharChaudai',

// 	//ghar ko uchai

// 	'basementGharUchai',
// 	'pakkiBhuiGharUchai',
// 	'prathamGharUchai',
// 	'dosroGharUchai',
// 	'tesroGharUchai',
// 	'chauthoGharUchai',
// 	'pachauGharUchai',

// 	// //sadak ko naam

// 	// 'sadakBasement',
// 	// 'sadekPakkiBhuiGhar',
// 	// 'sadakPratham',
// 	// 'sadakDosro',
// 	// 'sadakTesro',
// 	// 'sadakChautho',
// 	// 'sadakPachau',

// 	//sadak adhikar chetra

// 	'sadakAdhikarChetraBasement',
// 	'sadakAdhikarChetraPakkiBhuiGhar',
// 	'sadakAdhikarChetraPratham',
// 	'sadakAdhikarChetraDosro',
// 	'sadakAdhikarChetraTesro',
// 	'sadakAdhikarChetraChautho',
// 	'sadakAdhikarChetraPachau',

// 	//sadak bich bata chadnu parne

// 	'sadakKoBichBataChadnuParneBasement',
// 	'sadakKoBichBataChadnuParnePakkiBhuiGhar',
// 	'sadakKoBichBataChadnuParnePratham',
// 	'sadakKoBichBataChadnuParneDosro',
// 	'sadakKoBichBataChadnuParneTesro',
// 	'sadakKoBichBataChadnuParneChautho',
// 	'sadakKoBichBataChadnuParnePachau',

// 	//chadnu parne setback

// 	'chadnuParneSetBackBasement',
// 	'chadnuParneSetBackPakkiBhuiGhar',
// 	'chadnuParneSetBackPratham',
// 	'chadnuParneSetBackDosro',
// 	'chadnuParneSetBackTesro',
// 	'chadnuParneSetBackChautho',
// 	'chadnuParneSetBackPachau',

// 	//chadeko setback

// 	'chadekoSetBackBasement',
// 	'chadekoSetBackPakkiBhuiGhar',
// 	'chadekoSetBackPratham',
// 	'chadekoSetBackDosro',
// 	'chadekoSetBackTesro',
// 	'chadekoSetBackChautho',
// 	'chadekoSetBackPachau',

// 	//sadak bich bata chadeko kul

// 	'sadakKoBichBataChadekoKulBasement',
// 	'sadakKoBichBataChadekoKulBhuiGhar',
// 	'sadakKoBichBataChadekoKulPratham',
// 	'sadakKoBichBataChadekoKulDosro',
// 	'sadakKoBichBataChadekoKulTesro',
// 	'sadakKoBichBataChadekoKulChautho',
// 	'sadakKoBichBataChadekoKulPachau',

// 	//pakki parkhal

// 	'lambai',
// 	'chaudai',
// 	'uchai',
// 	'foot',
// ];

class CertificatePratibedanPeshBareComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, permitData } = this.props;

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const floorSchema = floorArray.getFloorSchema(
			requiredData.tableFields.filter((field) => field.isNumeric).map((row) => row.fieldName),
			validateArrayNullableNumber
		);

		const validation = Yup.object().shape(
			Object.assign({
				//form information
				mainDate: validateNepaliDate,
				chana: validateNullableOfficialReqNumbers,
				karyalayakoChana: validateNullableOfficialReqNumbers,
				jariBhayekoMiti: validateNepaliDate,
				newWardNumber: validateNumber,
				name: validateString,
				landIssueDate: validateNepaliDate,
				nirmanKisim: validateString,

				floor: Yup.array().of(Yup.object().shape(floorSchema)).nullable(),

				//pakki parkhal

				lambai: validateNullableNormalNumber,
				chaudai: validateNullableNormalNumber,
				uchai: validateNullableNormalNumber,
				foot: validateNullableNormalNumber,
			})
		);
		const nayaNirmanPramanPatraDharautiPhirta = getJsonData(otherData.nayaNirmanPramanPatraDharautiPhirta);
		const supernirmaanKarya = getJsonData(otherData.supernirmaanKarya);

		const initialValues = prepareMultiInitialValues(
			{ obj: initialVal, reqFields: [] },
			{
				obj: {
					name: this.props.permitData.applicantName,
					nirmanKisim: this.props.permitData.constructionType,
					newWardNumber: this.props.permitData.newWardNo,
					jariBhayekoMiti: nayaNirmanPramanPatraDharautiPhirta.chanaMiti,
					landIssueDate: supernirmaanKarya.prabhidikDate,
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height', 'floor'], formattedFloors),
				},
				reqFields: [],
			},

			{
				obj: { mainDate: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValues,
			blocks: floorArray.getBlocks(),
			formattedFloors,
			floorArray,
			validation,
		};
	}

	render() {
		const { permitData, formUrl, hasDeletePermission, isSaveDisabled, prevData, postAction, hasSavePermission, userData } = this.props;
		const { initialValues, formattedFloors, floorArray, validation } = this.state;
		return (
			<div>
				<Formik
					initialValues={initialValues}
					validationSchema={validation}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.PratibeydanPeshGarekoBare, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ values, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef} className="view-Form">
								<SectionHeader>
									<h1>{userData.organization && userData.organization.name}</h1>
									<h2>{userData.organization && userData.organization.address}</h2>
								</SectionHeader>
								<FlexSingleRight>
									<DashedLangDateField
										name="mainDate"
										setFieldValue={setFieldValue}
										value={values.mainDate}
										error={errors.mainDate}
										label={requiredData.date}
									/>
								</FlexSingleRight>
								<FlexSingleLeft>
									<label htmlFor="chana">{requiredData.chana}</label>
									<DashedLangInput name="chana" value={values.chana} setFieldValue={setFieldValue} error={errors.chana} />
								</FlexSingleLeft>
								<SectionHeader>
									<h2 className="underline">{requiredData.subject}</h2>
								</SectionHeader>
								<br />
								<br />
								{requiredData.karyalayaKoChana}
								<DashedLangInput
									name="karyalayakoChana"
									value={values.karyalayakoChana}
									setFieldValue={setFieldValue}
									error={errors.karyalayakoChana}
								/>
								{requiredData.date}
								<CompactDashedLangDate
									name="jariBhayekoMiti"
									value={values.jariBhayekoMiti}
									setFieldValue={setFieldValue}
									error={errors.jariBhayekoMiti}
								/>
								{requiredData.anusar}
								{userData.organization && userData.organization.name}
								{requiredData.wadanumber}
								<DashedLangInput
									name="newWardNumber"
									value={values.newWardNumber}
									setFieldValue={setFieldValue}
									error={errors.newWardNumber}
								/>
								{requiredData.name}
								<DashedLangInput name="name" value={values.name} setFieldValue={setFieldValue} error={errors.name} />
								{requiredData.landIssueDate}
								<CompactDashedLangDate
									name="landIssueDate"
									value={values.landIssueDate}
									setFieldValue={setFieldValue}
									error={errors.landIssueDate}
								/>
								{requiredData.nirmanKisim}
								<DashedLangInput
									name="nirmanKisim"
									value={values.nirmanKisim}
									setFieldValue={setFieldValue}
									error={errors.nirmanKisim}
								/>

								{requiredData.dataLine}
								{requiredData.note}
								<br />
								<br />
								<BlockFloorComponents floorArray={floorArray} formattedFloors={formattedFloors}>
									{(floorObj, block) => (
										<Table size="small" collapsing className="certificate-ui-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>
														<UnitDropdownWithRelatedFields
															unitName="bibaranUnit"
															options={squareUnitOptions}
															relatedFields={floorArray.getAllCustomsFields(null, 'floor', [
																'length',
																'area',
																'width',
																'height',
															])}
														/>
													</Table.HeaderCell>
													{floorObj.map((floor) => (
														<Table.HeaderCell>{floor.label.floorName}</Table.HeaderCell>
													))}
												</Table.Row>
											</Table.Header>
											<TableBody>
												{requiredData.tableFields.map((field) => (
													<Table.Row>
														<Table.Cell>{field.label}</Table.Cell>
														{floorObj.map((floor) => (
															<Table.Cell>
																{field.isNumeric ? (
																	<TableInputIm name={floor.fieldName('floor', field.fieldName)} />
																) : (
																	<DashedLangInput
																		name={floor.fieldName('floor', field.fieldName)}
																		value={getIn(values, floor.fieldName('floor', field.fieldName))}
																		error={getIn(errors, floor.fieldName('floor', field.fieldName))}
																		setFieldValue={setFieldValue}
																	/>
																)}
															</Table.Cell>
														))}
													</Table.Row>
												))}
												<Table.Row>
													<Table.Cell>{requiredData.pakkiParkhal}</Table.Cell>
													<Table.Cell colSpan={floorObj.length}>
														<label>{requiredData.lambai}</label>
														<DashedNormalInputIm name={`wall.${block ? `${block}.` : ''}length`} />
														<label>{requiredData.chaudai}</label>
														<DashedNormalInputIm name={`wall.${block ? `${block}.` : ''}width`} />
														<label>{requiredData.uchai}</label>
														<DashedNormalInputIm name={`wall.${block ? `${block}.` : ''}height`} />
														<label>{requiredData.foot}</label>
														<DashedNormalInputIm name={`wall.${block ? `${block}.` : ''}area`} />
													</Table.Cell>
												</Table.Row>
											</TableBody>
										</Table>
									)}
								</BlockFloorComponents>
								<FlexSingleRight>
									<FooterSignature designations={[requiredData.engineer]} />
								</FlexSingleRight>
								<br />
								<br />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const CertificatePratibedanPeshBare = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.PratibeydanPeshGarekoBare).setForm().getParams(),
			new ApiParam(api.NayaNirmanPramanPatraDharautiPhirta, 'nayaNirmanPramanPatraDharautiPhirta').getParams(),
			new ApiParam(api.supernirmaanKarya, 'supernirmaanKarya').getParams(),
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param7: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			// param7: ["15dayspecial"]
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <CertificatePratibedanPeshBareComponent {...props} parentProps={parentProps} />}
	/>
);
export default CertificatePratibedanPeshBare;
