import React, { Component } from 'react';
import * as Yup from 'yup';

import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { Form, Table } from 'semantic-ui-react';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleLeft, FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { requiredData } from '../../../../utils/data/SamsodhitSuperStructurePratibedanBiratnagarData';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { DashedSelect } from '../../../shared/Select';
import { DashedNormalInputIm, DashedLangInput } from '../../../shared/DashedFormInput';
import { prepareMultiInitialValues, getRelatedFields, getJsonData, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { squareUnitOptions } from '../../../../utils/optionUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate, validateNullableZeroNumber } from '../../../../utils/validationUtils';
import { PrintParams } from '../../../../utils/printUtils';
import { SignatureImage } from '../formComponents/SignatureImage';
import { getSaveByUserDetails } from '../../../../utils/formUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
const reqData = requiredData;
const shriShrimanShushriOptions = [
	{
		key: reqData.shri,
		text: reqData.shri,
		value: reqData.shri,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shushri,
		text: reqData.shushri,
		value: reqData.shushri,
	},
];
const SamsodhitSuperSchema = Yup.object().shape(
	Object.assign({
		date: validateNullableNepaliDate,
		superStructureBuildDate: validateNullableNepaliDate,
		sabikFloor: Yup.array().of(
			Yup.object().shape({
				length: validateNullableZeroNumber,
				width: validateNullableZeroNumber,
				height: validateNullableZeroNumber,
				area: validateNullableZeroNumber,
			})
		),
		haalFloor: Yup.array().of(
			Yup.object().shape({
				length: validateNullableZeroNumber,
				width: validateNullableZeroNumber,
				height: validateNullableZeroNumber,
				area: validateNullableZeroNumber,
			})
		),
	})
);

const floorFields = [
	{ name: 'length', label: reqData.lambai },
	{ name: 'width', label: reqData.chaudai },
	{ name: 'area', label: reqData.bargaFeet },
	{ name: 'height', label: reqData.uchai },
];
class SamsodhitSuperStructurePratibedanBiratnagarComponent extends Component {
	constructor(props) {
		super(props);

		const { permitData, prevData, otherData, enterByUser, userData } = this.props;
		const superStructureBuild = getJsonData(otherData.superStructureBuild);

		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();
		const initialValues = prepareMultiInitialValues(
			{ obj: { date: getCurrentDate(true), shriShrimanShushri: shriShrimanShushriOptions[0].value }, reqFields: [] },
			{
				obj: {
					sabikFloor: floorArray.getMultipleInitialValues(['length', 'area', 'width', 'height', 'label', 'floor'], formattedFloors),
					haalFloor: floorArray.getMultipleInitialValues(['label', 'floor'], formattedFloors),
					sabikFloorUnit: floorArray.getFloorUnit(),
					haalFloorUnit: floorArray.getFloorUnit(),
					superStructureBuildDate: superStructureBuild.superStructureBuildDate,
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);

		this.state = { initialValues };
	}
	render() {
		const { permitData, useSignatureImage, getSaveButtonProps, userData, errors: reduxErrors } = this.props;
		const salutationData = [ `${userData.organization.name}को कार्यालय`, `${userData.organization.address}`];
		const { initialValues } = this.state;
		return (
			<>
				{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={SamsodhitSuperSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.samsodhitSupStruPratibedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('Error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							// showToast('Something went wrong !!');
						}
					}}
				>
					{({ values, setFieldValue, errors, handleSubmit, validateForm }) => (
						<Form>
							<div ref={this.props.setRef} className="print-small-font">
								<FlexSingleRight>
									{reqData.miti}
									<CompactDashedLangDate name="date" setFieldValue={setFieldValue} value={values.date} error={errors.date} />
								</FlexSingleRight>
								<FlexSingleLeft>
									<div>{reqData.shrimanEngineerJiu}</div>

									<div>{userData.organization.name}</div>
								</FlexSingleLeft>
								<br />
								<br />
								<div>
									<SectionHeader>
										<h3 className="underline">{reqData.bisaye}</h3>
									</SectionHeader>
									<br />
									<div>
										{reqData.upayuktaSambandhama}
										{permitData.newWardNo}
										{reqData.maBanse}
										<DashedSelect name="shriShrimanShushri" options={shriShrimanShushriOptions} />
										{permitData.applicantName}
										{reqData.leNaPamaMiti}
										<CompactDashedLangDate
											name="superStructureBuildDate"
											setFieldValue={setFieldValue}
											value={values.superStructureBuildDate}
											error={errors.superStructureBuildDate}
										/>
										{reqData.dataLine}
										<br />
										<br />
										<SectionHeader>
											<h4>{reqData.tapasil}</h4>
										</SectionHeader>
										{reqData.jaggaRahekoSthan}
										{permitData.oldMunicipal}
										{reqData.halkoWadaNumber}
										{permitData.newWardNo}
										{reqData.sadak}
										{permitData.buildingJoinRoad}
										{reqData.kiNo}
										{permitData.kittaNo}
										{reqData.chetrafal}
										{permitData.landArea}({permitData.landAreaType})
										<br />
										<br />
										<SectionHeader>
											<h4 className="end-section">{reqData.tapasil}</h4>
											<h4>{reqData.sabikPeshGarekoNakshaBibaran}</h4>
										</SectionHeader>
										{/* table 2 */}
										{values.sabikFloor && values.sabikFloor.length > 0 && (
											<Table className="certificate-ui-table">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell></Table.HeaderCell>
														{values.sabikFloor.map((floor) => (
															<Table.HeaderCell>
																{/**@TODO fix for blocks */}
																{floor.label && floor.label.floorName}
															</Table.HeaderCell>
														))}
													</Table.Row>
												</Table.Header>
												<Table.Body>
													{floorFields.map(({ name, label }) => (
														<Table.Row key={name}>
															<Table.Cell>
																{name === 'area' ? (
																	<UnitDropdownWithRelatedFields
																		unitName="sabikFloorUnit"
																		options={squareUnitOptions}
																		relatedFields={getRelatedFields(null, values.sabikFloor, 'sabikFloor', [
																			'length',
																			'width',
																			'area',
																			'height',
																		])}
																	/>
																) : (
																	label
																)}
															</Table.Cell>
															{values.sabikFloor.map((floor, index) => (
																<Table.Cell key={floor.floor}>
																	<DashedNormalInputIm name={`sabikFloor.${index}.${name}`} />
																</Table.Cell>
															))}
														</Table.Row>
													))}
												</Table.Body>
											</Table>
										)}
										{/* table 2 */}
										<br />
										<br />
										<SectionHeader>
											<h4>{reqData.halPeshGarekoNakshaBibaran}</h4>
										</SectionHeader>
										{values.haalFloor && values.haalFloor.length > 0 && (
											<Table className="certificate-ui-table">
												<Table.Header>
													<Table.Row>
														<Table.HeaderCell></Table.HeaderCell>
														{values.haalFloor.map((floor) => (
															<Table.HeaderCell>
																{/**@TODO fix for blocks */}
																{floor.label && floor.label.floorName}
															</Table.HeaderCell>
														))}
													</Table.Row>
												</Table.Header>
												<Table.Body>
													{floorFields.map(({ name, label }) => (
														<Table.Row key={name}>
															<Table.Cell>
																{name === 'area' ? (
																	<UnitDropdownWithRelatedFields
																		unitName="haalFloorUnit"
																		options={squareUnitOptions}
																		relatedFields={getRelatedFields(null, values.haalFloor, 'haalFloor', [
																			'length',
																			'width',
																			'area',
																			'height',
																		])}
																	/>
																) : (
																	label
																)}
															</Table.Cell>
															{values.haalFloor.map((floor, index) => (
																<Table.Cell key={floor.floor}>
																	<DashedNormalInputIm name={`haalFloor.${index}.${name}`} />
																</Table.Cell>
															))}
														</Table.Row>
													))}
												</Table.Body>
											</Table>
										)}
									</div>
									<br />
									<div>
										<div>{reqData.ghatiBadiChetraFal}</div>
										<div>
											{reqData.one}
											<DashedNormalInputIm name="adjustArea.0" />
										</div>
										<div>
											{reqData.two}
											<DashedNormalInputIm name="adjustArea.1" />
										</div>
									</div>
									<FlexSingleRight>
										{reqData.pratibedanPeshGarnePrabidhik}
										<div>
											{reqData.naam}
											<DashedLangInput
												name="subName"
												setFieldValue={setFieldValue}
												value={values.subName}
												error={errors.subName}
											/>
										</div>
										<div>
											{reqData.pad}
											<DashedLangInput
												name="subDesignation"
												setFieldValue={setFieldValue}
												value={values.subDesignation}
												error={errors.subDesignation}
											/>
										</div>
										<SignatureImage value={values.subSignature} showSignature={useSignatureImage} label={reqData.dasthakhat} />
									</FlexSingleRight>
								</div>
							</div>

							<SaveButtonValidation errors={errors} validateForm={validateForm} handleSubmit={handleSubmit} {...getSaveButtonProps()} />
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const SamsodhitSuperStructurePratibedanBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.samsodhitSupStruPratibedan).setForm().getParams(),
			new ApiParam(api.superStructureBuild, 'superStructureBuild').getParams(),
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'label'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SamsodhitSuperStructurePratibedanBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default SamsodhitSuperStructurePratibedanBiratnagar;
