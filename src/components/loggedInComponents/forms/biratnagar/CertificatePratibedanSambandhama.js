import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { certificatePratibedanDetails } from '../../../../utils/data/CertificatePratibedanSambandhama';
import { Formik } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { FlexSingleRight, FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { Form } from 'semantic-ui-react';
import { FooterSignature } from '../formComponents/FooterSignature';
import { validateNullableOfficialNumbers, validateString, validateNullableNepaliDate } from '../../../../utils/validationUtils';
import { handleSuccess, checkError, prepareMultiInitialValues, getJsonData } from '../../../../utils/dataUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { commonMessages } from '../../../../utils/data/validationData';

const { object } = require('yup');
const validation = object({
	chana: validateNullableOfficialNumbers,
	pasa: validateString,
	mainDate: validateNullableNepaliDate.required(commonMessages.required),
	shriName: validateString,
});
const initialVal = { chana: '', pasa: '', mainDate: '', shriName: 'घर नक्शा शाखा' };
const data = certificatePratibedanDetails;

class CertificatePratibedanSambandhamaComponent extends Component {
	constructor(props) {
		super(props);

		let initVal = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: { mainDate: getCurrentDate(true) },
				reqFields: ['mainDate'],
			}
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { permitData, postAction, formUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, prevData, userData } = this.props;
		const initVal = (this.props.prevData && !this.props.prevData.jsonData) ? this.state.initVal : JSON.parse(this.props.prevData.jsonData);
		return (
			<Formik
				initialValues={initVal}
				validationSchema={validation}
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);

					values.applicationNo = permitData.applicantNo;

					try {
						await postAction(api.PratibeydanSambandhama, values, true);

						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
				render={({ errors, handleSubmit, values, isSubmitting, setFieldValue, validateForm }) => {
					return (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<div className="tipandi-letterHead">
									<LetterHeadFlex userInfo={userData} />
								</div>

								<div className="flex-item-space-between">
									<div className="flex-item-left">
										<label htmlFor="chana">{data.chana}</label>
										<DashedLangInput name="chana" value={values.chana} setFieldValue={setFieldValue} error={errors.chana} />
										<br/>
										<label htmlFor="pasa">{data.pasa}</label>
										<DashedLangInput name="pasa" value={values.pasa} setFieldValue={setFieldValue} error={errors.pasa} />
									</div>

									<div className="flex-item-right">
										<DashedLangDateField name="mainDate" value={values.mainDate} label={data.date} setFieldValue={setFieldValue} error={errors.mainDate} />
									</div>
								</div>

								<br/>
								<br/>
								<FlexSingleLeft>
									<label htmlFor="shriName">{data.shri}</label>
									<DashedLangInput
										name="shriName"
										value={values.shriName}
										setFieldValue={setFieldValue}
										error={errors.shriName}
									/>
									<br />
									{userData && userData.organization && userData.organization.name}
								</FlexSingleLeft>
								<br />

								<SectionHeader>
									<h3 className="underline">{data.subject}</h3>
								</SectionHeader>
								<br />
								<br />

								{data.uparokta_sambandhama_yash}
								{userData && userData.organization && userData.organization.name}
								{data.wada_no}
								{permitData.newWardNo}
								{data.basne_shri}
								{permitData.applicantName}
								{data.le_niji}
								{permitData.oldMunicipal}
								{data.space}
								{permitData.oldWardNo}
								{data.haal}
								{userData && userData.organization && userData.organization.name}
								{data.wada_no}
								{permitData.newWardNo}
								{data.kitta}
								{permitData.kittaNo}
								{data.chetrafal}
								{permitData.landArea}
								{data.space}
								{permitData.landAreaType}
								{data.jaggama_nirman}
								{permitData.floor.length}
								{data.tale}
								{data.dataline}

								<br />
								<br />
								<br />
								<br />
								<br/>
								<FlexSingleRight>
									<FooterSignature designations={[data.engineer]} />
								</FlexSingleRight>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					);
				}}
			></Formik>
		);
	}
}
const CertificatePratibedanSambandhama = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.PratibeydanSambandhama).setForm().getParams(),
			// new ApiParam(api.NayaNirmanPramanPatraDharautiPhirta, 'nirmanPramanPatra').getParams(),
		]}
		onBeforeGetContent={{
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <CertificatePratibedanSambandhamaComponent {...props} parentProps={parentProps} />}
	/>
);
export default CertificatePratibedanSambandhama;
