import React, { Component } from 'react';
import { Table, TextArea } from 'semantic-ui-react';
import { applicableToMultiStorey } from '../../../../../utils/data/ApplicableToMultiStoreyForensicEngineeringData';
import { TableInputIm } from '../../../../shared/TableInput';
import { FlexSingleLeft } from '../../../../uiComponents/FlexDivs';
import { FooterSignature } from '../../formComponents/FooterSignature';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
const reqData = applicableToMultiStorey.table2;
export class ApplicableTable2 extends Component {
	render() {
		return (
			<>
				<Table className="structure-ui-table">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={1}>{reqData.sn}</Table.HeaderCell>
							<Table.HeaderCell width={3}>{reqData.description}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.objects}</Table.HeaderCell>
							<Table.HeaderCell width={1}>{reqData.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell width={1}>
								<b>1</b>
							</Table.Cell>
							<Table.Cell width={3}>
								<b>{reqData.presentConditionOfBuilding}</b>
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="presentConditionOfBuildingObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="presentConditionOfBuildingRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>
								<b>2</b>
							</Table.Cell>
							<Table.Cell colSpan="3">
								<b>{reqData.assessment}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.yearOfCompletion}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="yearOfCompletionObjectsTable2" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="yearOfCompletionRemarksTable2" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.leadBearing}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="leadBearingObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="leadBearingRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.frame}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="frameObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="frameRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.yearOfBuilt}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="yearOfBuiltObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="yearOfBuiltRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.framingSystem}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="framingSystemObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="framingSystemRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.otherStructuralElement}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="otherStructuralElementObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="otherStructuralElementRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>
								<b>3</b>
							</Table.Cell>
							<Table.Cell colSpan="3">
								<b>{reqData.assessmentOfFoundation}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.typeOfFoundation}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="typeOfFoundationObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="typeOfFoundationRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.strip}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="stripObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="stripRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.isolated}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="isolatedObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="isolatedRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.combined}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="combinedObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="combinedRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.pile}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="pileObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="pileRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>
								<b>4</b>
							</Table.Cell>
							<Table.Cell colSpan="3">
								<b>{reqData.strengtheningOfGround}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.additionOfBeam}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="additionOfBeamObjectsTable2" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="additionOfBeamRemarksTable2" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.steel}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="steelObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="steelRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.concrete}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="concreteObjectsTable2" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="concreteRemarksTable2" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>
								<b>1</b>
							</Table.Cell>
							<Table.Cell width={3}>
								<b>{reqData.expectedLifeOfFloor}</b>
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="expectedLifeOfFloorObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="expectedLifeOfFloorRemarks" />
							</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
				<br />

				<h3>{reqData.shortCommentsAndRecommendations}</h3>
				<TextArea name="shortCommentsAndRecommendations" />
				<FlexSingleLeft>
					<FooterSignature designations={[reqData.signatureAndConsultant]} />
					<br />
					<label htmlFor="SignatureName">{reqData.name}</label>
					<DashedNormalInputIm name="SignatureName" />
					<br />
					<label htmlFor="nceRegdNo">{reqData.nceRegdNo}</label>
					<DashedNormalInputIm name="nceRegdNo" />
				</FlexSingleLeft>
			</>
		);
	}
}

export default ApplicableTable2;
