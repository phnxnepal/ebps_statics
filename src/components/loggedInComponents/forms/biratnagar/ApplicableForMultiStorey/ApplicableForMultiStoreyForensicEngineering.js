import React, { Component } from 'react';
import FormContainerV2 from '../../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../../utils/paramUtil';
import api from '../../../../../utils/api';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { applicableToMultiStorey } from '../../../../../utils/data/ApplicableToMultiStoreyForensicEngineeringData';
import { SectionHeader } from '../../../../uiComponents/Headers';
import { FlexSingleLeft } from '../../../../uiComponents/FlexDivs';
import ApplicableTable1 from './ApplicableTable1';
import { multIStoreyInitialVal } from './MultiStoreyInitialVal';
import ApplicableTable2 from './ApplicableTable2';
import ErrorDisplay from '../../../../shared/ErrorDisplay';
import { prepareMultiInitialValues, getJsonData, handleSuccess, checkError } from '../../../../../utils/dataUtils';
import SaveButtonValidation from '../../../../shared/SaveButtonValidation';

const reqData = applicableToMultiStorey;
class ApplicableForMultiStoreyForensicEngineeringComponent extends Component {
	constructor(props) {
		super(props);
		let initialval = prepareMultiInitialValues(
			{
				obj: multIStoreyInitialVal,
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = { initialval };
	}

	render() {
		const { permitData, formUrl, hasDeletePermission, isSaveDisabled, prevData, postAction, hasSavePermission } = this.props;
		return (
			<div className="english-div">
				<Formik
					initialValues={this.state.initialval}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.forMultiStoreyBuilding, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, handleChange, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h2>{reqData.heading}</h2>
									<br />
									<FlexSingleLeft>
										<h3 className="underline">{reqData.additionOfFloor}</h3>
									</FlexSingleLeft>
								</SectionHeader>

								{/* table1 */}
								<ApplicableTable1 />

								<br />
								<br />
								<ApplicableTable2 />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const ApplicableForMultiStoreyForensicEngineering = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.forMultiStoreyBuilding).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <ApplicableForMultiStoreyForensicEngineeringComponent {...props} parentProps={parentProps} />}
	/>
);
export default ApplicableForMultiStoreyForensicEngineering;
