import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { applicableToMultiStorey } from '../../../../../utils/data/ApplicableToMultiStoreyForensicEngineeringData';
import { TableInputIm } from '../../../../shared/TableInput';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';

const reqData = applicableToMultiStorey.table1;
export class ApplicableTable1 extends Component {
	render() {
		return (
			<Table className="structure-ui-table">
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell width={1}>{reqData.sn}</Table.HeaderCell>
						<Table.HeaderCell width={3}>{reqData.description}</Table.HeaderCell>
						<Table.HeaderCell width={2}>{reqData.objects}</Table.HeaderCell>
						<Table.HeaderCell width={2}>{reqData.remarks}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					<Table.Row>
						<Table.Cell width={1}>1</Table.Cell>
						<Table.Cell width={3}>{reqData.assessmentOfFloor}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="assessmentOfFloorObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="assessmentOfFloorRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}>2</Table.Cell>
						<Table.Cell width={3}>{reqData.assessmentOfFloor}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="yearOfCompletionObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="yearOfCompletionRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}>3</Table.Cell>
						<Table.Cell width={3}>{reqData.structureType}</Table.Cell>
						<Table.Cell width={2}>
							<RadioInput name="structureTypeObjects" option={reqData.frame} value={reqData.frame} space={true} />
							<RadioInput name="structureTypeObjects" option={reqData.loadBearing} value={reqData.loadBearing} space={true} />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="structureTypeRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}>4</Table.Cell>
						<Table.Cell width={3}>{reqData.expectedLife}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="expectedLifeObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="expectedLifeRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell colSpan="3">
							<b>{reqData.isItRecommended}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.additionOfBeam}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="additionOfBeamObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="additionOfBeamRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.retroFitting}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="retroFittingObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="retroFittingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.grouting}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="groutingObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="groutingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.compositeStructure}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="compositeStructureObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="compositeStructureRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell colSpan="3">
							<b>{reqData.designerShouldJustify}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell colSpan="3">
							<b>{reqData.increaseIn}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.loadBearingWallPanel}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="loadBearingWallPanelObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="loadBearingWallPanelRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.concrete}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="concreteObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="concreteRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.solid}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="solidObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="solidRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.hollow}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="hollowObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="hollowRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.ribbed}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="ribbedObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="ribbedRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.boxFrame}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="boxFrameObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="boxFrameRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.metal}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="metalObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="metalRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.singleDouble}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="singleDoubleObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="singleDoubleRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.composite}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="compositeObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="compositeRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.drift}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="driftObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="driftRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.heightWidth}</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="heightWidthObjects" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="heightWidthRemarks" />
						</Table.Cell>
					</Table.Row>
				</Table.Body>
			</Table>
		);
	}
}

export default ApplicableTable1;
