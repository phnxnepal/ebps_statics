import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { SectionHeader } from '../../../uiComponents/Headers';
import { Table, Form } from 'semantic-ui-react';
import { formData } from '../../../../utils/data/FormToBeFilledByDesignerData';
import { TableInputIm } from '../../../shared/TableInput';

import { Formik } from 'formik';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import MyCheckBox from '../../../shared/MyCheckbox';
import { DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';

import { FooterSignature } from '../formComponents/FooterSignature';
import {
	validateNullableNormalNumber,
	validateNotReqdPhone,
	validateArrayNullableNumber,
	validateNullableZeroNumber,
} from '../../../../utils/validationUtils';
import { getUserRole } from '../../../../utils/functionUtils';
import { UserType } from '../../../../utils/userTypeUtils';
import { getSaveByUserDetails } from '../../../../utils/formUtils';
import { prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { PrintIdentifiers } from '../../../../utils/printUtils';
import { PurposeOfConstructionEnglishRadio } from '../mapPermitComponents/PurposeOfConstructionRadio';
import { purposeOfConstructionEnglishOptionsV2, constructionTypeEnglishOptions } from '../../../../utils/optionUtils';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { BlockComponents, BlockComponentsV2, BlockFloorComponentsV2 } from '../../../shared/formComponents/BlockFloorComponents';
import { getRopaniAndFeet } from '../../../../utils/mathUtils';
import { CheckboxInput } from '../../../shared/formComponents/CheckboxInput';

const { object, array } = require('yup');
const data = formData;
const initialValues = {
	//table 1
	applicantName: '',
	naksawalaName: '',
	naksawalaAddress: '',
	newWardNo: '',
	kittaNo: '',
	ropaniLalPurja: '',
	inSquareFeetLalPurja: '',
	ropaniSitePlan: '',
	inSquareFeetSitePlan: '',

	//table 2
	purposeOfConstruction: purposeOfConstructionEnglishOptionsV2[0].value,
	purposeOfConstructionOther: '',

	//table 4
	designers: '',
	phoneNumber: '',
	address: '',
	regNo: '',

	//table 5
	zone: '',
	groundCoverage: '',
	threeByeLaws: '',
	buildingHeightDwg: '',
	threeRemarks: '',
	buildingHeight: '',
	twoByeLaws: '',
	twoAccToDrwng: '',
	twoRemarks: '',
	buildingArea: '',
	fourByeLaws: '',
	buildingAreaDwg: '',
	fourRemarks: '',
	nameOfRoad: '',
	fiveByeLaws: '',
	nameOfRoadDwg: '',
	fiveRemarks: '',
	rightOfWay: '',
	sixByeLaws: '',
	sixAccToDrwng: '',
	sixRemarks: '',
	sevenrightOfWay: '',
	sevenByeLaws: '',
	sevenAccToDrwng: '',
	sevenRemarks: '',
	highTensionLine: '',
	eightByeLaws: '',
	eightAccToDrwng: '',
	eightRemarks: '',
	nineByeLaws: '',
	noOfStoreyDwg: '',
	nineRemarks: '',

	//table 8
	taxReceipt: '',
	taxReceiptRs: '',
	depositReceipt: '',
	depositReceiptRs: '',
	totalRs: '',
};

class FormToBeFilledByDesignerComponent extends Component {
	constructor(props) {
		super(props);
		const { userData, enterByUser, permitData } = this.props;

		const floorArray = new FloorBlockArray(permitData.floor);
		const { unit, areaUnit } = floorArray.getEnglishFloorUnit();
		const formattedFloors = floorArray.getFormattedFloors();
		const { topFloor, bottomFloor } = floorArray.getTopAndBottomFloors();

		const { ropani, feet } = getRopaniAndFeet(permitData.landArea, permitData.landAreaType);
		const constructionType = permitData.constructionType;
		let floorAreasFieldName = constructionType === '2' ? 'otherBuilding' : constructionType === '3' ? 'storeyAdded' : 'farCountable';

		let designerInfo = {
			designerName: '',
			designerDesignation: '',
			address: '',
			regNo: '',
		};

		if (getUserRole() === UserType.DESIGNER) {
			const { subName } = getSaveByUserDetails(enterByUser, userData);
			designerInfo.designerName = subName;
			designerInfo.designerPhone = userData.info.mobile;
			designerInfo.address = userData.info.address;

			designerInfo.regNo = userData.info.municipalRegNo;
		}
		const initialVal = prepareMultiInitialValues(
			{
				obj: initialValues,
				reqFields: [],
			},
			{
				obj: designerInfo,
				reqFields: [],
			},
			{
				obj: permitData,
				reqFields: [
					'applicantName',
					'naksawalaName',
					'naksawalaAddress',
					'newWardNo',
					'kittaNo',
					'constructionType',
					'purposeOfConstruction',
				],
			},
			{
				obj: {
					nameOfRoad: permitData.nearestLocation,
					nameOfRoadDwg: permitData.nearestLocation,
					ropaniLalPurja: ropani,
					ropaniSitePlan: ropani,
					inSquareFeetLalPurja: feet,
					inSquareFeetSitePlan: feet,
				},
				reqFields: [],
			},
			{
				obj: {
					...floorArray.getInitialValue('englishCountName', 'noOfStorey', topFloor),
					...floorArray.getInitialValue('englishCountName', 'noOfStoreyDwg', topFloor),
					...floorArray.getInitialValue(null, 'buildingHeight', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingArea', bottomFloor),
					...floorArray.getInitialValue(null, 'buildingHeightDwg', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'buildingAreaDwg', bottomFloor),
					floorAreas: floorArray.getInitialValues('area', floorAreasFieldName, formattedFloors),
					floorUnitLenth: unit,
					floorUnitArea: areaUnit,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);

		const floorAreasSchema = floorArray.getFloorSchema(
			['otherBuilding', 'storeyAdded', 'farCountable', 'farNonCountable', 'totalFloorArea'],
			validateArrayNullableNumber
		);

		const schema = object({
			//table 1
			ropaniLalPurja: validateNullableZeroNumber,
			inSquareFeetLalPurja: validateNullableZeroNumber,
			ropaniSitePlan: validateNullableZeroNumber,
			inSquareFeetSitePlan: validateNullableZeroNumber,

			//table 4
			phoneNumber: validateNotReqdPhone,
			// regNo: validateNullableNormalNumber,

			//table 5
			groundCoverage: validateNullableNormalNumber,

			twoByeLaws: validateNullableNormalNumber,
			twoAccToDrwng: validateNullableNormalNumber,
			buildingHeight: validateNullableNormalNumber,
			threeByeLaws: validateNullableNormalNumber,
			threeAccToDrwng: validateNullableNormalNumber,
			floorAreaFAR: validateNullableNormalNumber,
			fourByeLaws: validateNullableNormalNumber,
			fourAccToDrwng: validateNullableNormalNumber,
			sixByeLaws: validateNullableNormalNumber,
			sixAccToDrwng: validateNullableNormalNumber,
			highTensionLine: validateNullableNormalNumber,
			sevenByeLaws: validateNullableNormalNumber,
			sevenAccToDrwng: validateNullableNormalNumber,
			eightByeLaws: validateNullableNormalNumber,
			eightAccToDrwng: validateNullableNormalNumber,
			nineByeLaws: validateNullableNormalNumber,

			//table 6
			floorAreas: array().of(object().shape(floorAreasSchema)).nullable(),

			//table 8
			taxReceipt: validateNullableNormalNumber,
			taxReceiptRs: validateNullableNormalNumber,
			depositReceipt: validateNullableNormalNumber,
			depositReceiptRs: validateNullableNormalNumber,
			totalRs: validateNullableNormalNumber,
		});
		this.state = {
			initialVal,
			hasBlocks: floorArray.hasBlocks,
			blocks: floorArray.getBlocks(),
			formattedFloors,
			floorArray,
			schema,
		};
	}
	render() {
		const { fromUrl, hasSavePermission, hasDeletePermission, isSaveDisabled, prevData } = this.props;
		const { formattedFloors, initialVal, blocks, floorArray, hasBlocks, schema } = this.state;
		return (
			<div>
				<Formik
					initialValues={initialVal}
					enableReinitialize
					validationSchema={schema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.FormToBeFilledByDesigner, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ errors, handleSubmit, values, isSubmitting, setFieldValue, validateForm, handleChange, from }) => {
						return (
							<Form loading={isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef} className="form-section print-small-font">
									<SectionHeader>
										<h2>{data.header}</h2>
									</SectionHeader>
									{/* table 1 */}
									<Table border="1px solid black" className="english-div">
										<Table.Row>
											<Table.Cell>{data.landOwner}</Table.Cell>
											<Table.Cell colspan="5">
												<TableInputIm name="applicantName" />
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>{data.houseOwner}</Table.Cell>
											<Table.Cell colspan="5">
												<TableInputIm name="naksawalaName" />
											</Table.Cell>
										</Table.Row>

										<Table.Row>
											<Table.Cell colspan="2">{data.location}</Table.Cell>
											<Table.Cell colspan="4">{data.landArea}</Table.Cell>
										</Table.Row>

										<Table.Row>
											<Table.Cell colspan="2">
												<TableInputIm name="naksawalaAddress" />
											</Table.Cell>
											<Table.Cell colspan="2">{data.lalpurja}</Table.Cell>
											<Table.Cell colspan="2">{data.sitePlan}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>{data.wardNo}</Table.Cell>
											<Table.Cell>{data.plotNo}</Table.Cell>
											<Table.Cell>{data.ropani}</Table.Cell>
											<Table.Cell>{data.inSquareFeet}</Table.Cell>
											<Table.Cell>{data.ropani}</Table.Cell>
											<Table.Cell>{data.inSquareFeet}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												<TableInputIm name="newWardNo" sizeClass="c-small" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="kittaNo" sizeClass="c-small" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="ropaniLalPurja" sizeClass="c-small" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="inSquareFeetLalPurja" sizeClass="c-small" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="ropaniSitePlan" sizeClass="c-small" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="inSquareFeetSitePlan" sizeClass="c-small" />
											</Table.Cell>
										</Table.Row>
									</Table>
									{/* table 2 */}
									<Table border="1px solid black" className="english-div">
										<Table.Row>
											<Table.Cell colspan="1">{data.purposeOfBuilding}</Table.Cell>
											<Table.Cell colspan="2">
												<PurposeOfConstructionEnglishRadio space={true} values={values} />
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell colspan="1">{data.natureOfConstruction}</Table.Cell>
											<Table.Cell colspan="2">
												<RadioInput name="natureOfConstruction" option={data.row} />{' '}
												<RadioInput name="natureOfConstruction" option={data.semiAttached} />{' '}
												<RadioInput name="natureOfConstruction" option={data.deattached} />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell colspan="1">{data.typesOfBuilding}</Table.Cell>
											<Table.Cell colspan="2">
												<ConstructionTypeRadio space={true} options={constructionTypeEnglishOptions} />
											</Table.Cell>
										</Table.Row>
									</Table>
									{/* table 3 */}
									<Table border="1px solid black" className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell colspan="1">{data.drawingSize}</Table.HeaderCell>
												<Table.HeaderCell colspan="4">{data.drawingScale}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>

										<Table.Row>
											<Table.Cell colspan="1">{data.a1}</Table.Cell>
											<Table.Cell colspan="2">{data.sitePlan}</Table.Cell>
											<Table.Cell colspan="2">{data.floorPlans}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell colspan="1">{data.a2}</Table.Cell>
											<Table.Cell colspan="2">
												<MyCheckBox name="sitePlan" labelName={data.oneEqualToEight} Value={data.oneEqualToEight} />{' '}
												<MyCheckBox name="sitePlan" labelName={data.oneEqualToSixteen} Value={data.oneEqualToSixteen} />{' '}
												<MyCheckBox name="sitePlan" labelName={data.oneEqualToThirtyTwo} Value={data.oneEqualToThirtyTwo} />{' '}
											</Table.Cell>
											<Table.Cell colspan="2">
												<MyCheckBox name="floorPlans" labelName={data.oneEqualToFour} Value={data.oneEqualToFour} />{' '}
												<MyCheckBox name="floorPlans" labelName={data.oneEqualToEight} Value={data.oneEqualToEight} />{' '}
											</Table.Cell>
										</Table.Row>
									</Table>
									{/* table 4 */}
									<Table columns="6" border="1px solid black" className="english-div">
										<Table.Row>
											<Table.Cell colspan="1">{data.designers}</Table.Cell>
											<Table.Cell colspan="2">
												<TableInputIm name="designerName" />
											</Table.Cell>
											<Table.Cell colspan="1">{data.phoneNumber}</Table.Cell>
											<Table.Cell colspan="2">
												<TableInputIm name="designerPhone" />
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell colspan="1">{data.address}</Table.Cell>
											<Table.Cell colspan="1">
												<TableInputIm name="address" />
											</Table.Cell>
											<Table.Cell colspan="1">{data.regNo}</Table.Cell>
											<Table.Cell colspan="1">
												<TableInputIm name="regNo" />
											</Table.Cell>
											<Table.Cell colspan="1">{data.class}</Table.Cell>
											<Table.Cell colspan="1">
												<CheckboxInput name="class" options={['A', 'B', 'C', 'D']} inline={true} />
												{/* <MyCheckBox name="class" labelName={data.a} Value={data.a} />{' '}
												<MyCheckBox name="class" labelName={data.b} Value={data.b} /> */}
											</Table.Cell>
										</Table.Row>
									</Table>
									{/* table 5 */}
									<Table collasping border="1px solid black" className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell>{data.sn}</Table.HeaderCell>
												<Table.HeaderCell>{data.description}</Table.HeaderCell>
												<Table.HeaderCell>{data.byeLaws}</Table.HeaderCell>
												<Table.HeaderCell>{data.AccToDrawg}</Table.HeaderCell>
												<Table.HeaderCell>{data.remarks}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>

										<Table.Row>
											<Table.Cell>1</Table.Cell>
											<Table.Cell colspan="3">
												{data.zone} <DashedNormalInputIm name="zone" />
											</Table.Cell>

											<Table.Cell> </Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>2</Table.Cell>
											<Table.Cell>
												{data.groundCoverage}
												<DashedNormalInputIm name="groundCoverage" /> %
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="twoByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="twoAccToDrwng" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="twoRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>3</Table.Cell>
											<Table.Cell>
												{data.heightOfBuilding}
												<BlockComponents floorArray={floorArray} blocks={blocks} blockLabel="Block">
													{(block) => {
														const buildingHeight = floorArray.getReducedFieldName('buildingHeight', block);
														return (
															<>
																<DashedNormalInputIm name={buildingHeight} /> {values.floorUnitLenth}
															</>
														);
													}}
												</BlockComponents>
											</Table.Cell>
											<Table.Cell>
												<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
													{(block) => {
														const threeByeLaws = floorArray.getReducedFieldName('threeByeLaws', block);
														return (
															<div className="inline-table-el" key={block}>
																{block ? `Block ${block}` : ''} <TableInputIm name={threeByeLaws} />{' '}
																{values.floorUnitLenth}
															</div>
														);
													}}
												</BlockComponentsV2>
												{/* <TableInputIm name="threeByeLaws" />{' '} */}
											</Table.Cell>
											<Table.Cell>
												<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
													{(block) => {
														const fieldName = floorArray.getReducedFieldName('buildingHeightDwg', block);
														return (
															<div className="inline-table-el" key={block}>
																{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} />{' '}
																{values.floorUnitLenth}
															</div>
														);
													}}
												</BlockComponentsV2>
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="threeRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>4</Table.Cell>
											<Table.Cell>
												{data.floorAreaFAR}{' '}
												<BlockComponents floorArray={floorArray} blocks={blocks} blockLabel="Block">
													{(block) => {
														const buildingArea = floorArray.getReducedFieldName('buildingArea', block);
														return (
															<>
																<DashedNormalInputIm name={buildingArea} /> {values.floorUnitArea}
															</>
														);
													}}
												</BlockComponents>
												{/* <DashedNormalInputIm name="floorAreaFAR" /> */}
											</Table.Cell>
											<Table.Cell>
												<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
													{(block) => {
														const fieldName = floorArray.getReducedFieldName('fourByeLaws', block);
														return (
															<div className="inline-table-el" key={block}>
																{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} />{' '}
																{values.floorUnitArea}
															</div>
														);
													}}
												</BlockComponentsV2>
												{/* <TableInputIm name="fourByeLaws" />{' '} */}
											</Table.Cell>
											<Table.Cell>
												<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
													{(block) => {
														const fieldName = floorArray.getReducedFieldName('buildingAreaDwg', block);
														return (
															<div className="inline-table-el" key={block}>
																{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} />{' '}
																{values.floorUnitArea}
															</div>
														);
													}}
												</BlockComponentsV2>
												{/* <TableInputIm name="fourAccToDrwng" />{' '} */}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="fourRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>5</Table.Cell>
											<Table.Cell>
												{data.nameOfRoad}
												<DashedNormalInputIm name="nameOfRoad" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="fiveByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="nameOfRoadDwg" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="fiveRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>6</Table.Cell>
											<Table.Cell>{data.rightOfWay}</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sixByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sixAccToDrwng" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sixRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>7</Table.Cell>
											<Table.Cell>
												{data.highTensionLine}
												<DashedNormalInputIm name="highTensionLine" />
												{'KV'}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sevenByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sevenAccToDrwng" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="sevenRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>8</Table.Cell>
											<Table.Cell>{data.riverBank}</Table.Cell>
											<Table.Cell>
												<TableInputIm name="eightByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="eightAccToDrwng" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="eightRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>9</Table.Cell>
											<Table.Cell>
												{data.numberOfStorey}
												<BlockComponents floorArray={floorArray} blocks={blocks} blockLabel="Block">
													{(block) => {
														const fieldName = floorArray.getReducedFieldName('noOfStorey', block);
														return (
															<>
																<DashedNormalInputIm name={fieldName} />
															</>
														);
													}}
												</BlockComponents>
												{/* <DashedNormalInputIm name="noOfStorey" /> */}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="nineByeLaws" />{' '}
											</Table.Cell>
											<Table.Cell>
												<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
													{(block) => {
														const fieldName = floorArray.getReducedFieldName('noOfStoreyDwg', block);
														return (
															<div className="inline-table-el" key={block}>
																{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} />{' '}
															</div>
														);
													}}
												</BlockComponentsV2>
												{/* <TableInputIm name="noOfStoreyDwg" />{' '} */}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="nineRemarks" />{' '}
											</Table.Cell>
										</Table.Row>
									</Table>
									{/* table 6 */}
									<Table columns="4" border="1px solid black" className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width="2" rowspan="3">
													{data.sn}
												</Table.HeaderCell>
												<Table.HeaderCell rowspan="3">{data.floorName}</Table.HeaderCell>
												<Table.HeaderCell colspan="4">
													{data.floorArea} {values.floorUnitArea}
												</Table.HeaderCell>
												<Table.HeaderCell rowspan="3">{data.totalFloorArea}</Table.HeaderCell>
											</Table.Row>
											<Table.Row>
												<Table.HeaderCell colspan="2">{data.existing}</Table.HeaderCell>
												<Table.HeaderCell colspan="2">{data.proposedNewConstruction}</Table.HeaderCell>
											</Table.Row>
											<Table.Row>
												<Table.HeaderCell colspan="1">{data.otherBuilding}</Table.HeaderCell>
												<Table.HeaderCell colspan="1">{data.purposedStoreyAdded}</Table.HeaderCell>
												<Table.HeaderCell colspan="1">{data.FARnonCountable}</Table.HeaderCell>
												<Table.HeaderCell colspan="1">{data.FARcountable}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{hasBlocks ? (
												<BlockFloorComponentsV2 floorArray={floorArray} formattedFloors={formattedFloors}>
													{(floorObj, block) => (
														<>
															{floorObj.map((currentRow) => (
																<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
																	<Table.Cell>{currentRow.floor}</Table.Cell>
																	<Table.Cell>{`${currentRow.label.englishName} ${'Block ' + block}`}</Table.Cell>
																	<Table.Cell>
																		<TableInputIm
																			name={currentRow.fieldName('floorAreas', 'otherBuilding')}
																			sizeClass="c-small"
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInputIm
																			name={currentRow.fieldName('floorAreas', 'storeyAdded')}
																			sizeClass="c-small"
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInputIm
																			name={currentRow.fieldName('floorAreas', 'farNonCountable')}
																			sizeClass="c-small"
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInputIm
																			name={currentRow.fieldName('floorAreas', 'farCountable')}
																			sizeClass="c-small"
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<TableInputIm
																			name={currentRow.fieldName('floorAreas', 'totalFloorArea')}
																			sizeClass="c-small"
																		/>
																	</Table.Cell>
																</Table.Row>
															))}
														</>
													)}
												</BlockFloorComponentsV2>
											) : (
												<>
													{formattedFloors.map((currentRow) => {
														return (
															<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
																<Table.Cell>{currentRow.floor}</Table.Cell>
																<Table.Cell>{currentRow.label.englishName}</Table.Cell>
																<Table.Cell>
																	<TableInputIm
																		name={currentRow.fieldName('floorAreas', 'otherBuilding')}
																		sizeClass="c-small"
																	/>
																</Table.Cell>
																<Table.Cell>
																	<TableInputIm
																		name={currentRow.fieldName('floorAreas', 'storeyAdded')}
																		sizeClass="c-small"
																	/>
																</Table.Cell>
																<Table.Cell>
																	<TableInputIm
																		name={currentRow.fieldName('floorAreas', 'farNonCountable')}
																		sizeClass="c-small"
																	/>
																</Table.Cell>
																<Table.Cell>
																	<TableInputIm
																		name={currentRow.fieldName('floorAreas', 'farCountable')}
																		sizeClass="c-small"
																	/>
																</Table.Cell>
																<Table.Cell>
																	<TableInputIm
																		name={currentRow.fieldName('floorAreas', 'totalFloorArea')}
																		sizeClass="c-small"
																	/>
																</Table.Cell>
															</Table.Row>
														);
													})}
												</>
											)}
										</Table.Body>
										<Table.Row>
											<Table.Cell>{floorArray && floorArray.floors.length + 1}</Table.Cell>
											<Table.Cell>
												<DashedNormalInputIm name="customFloorName" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="customFloorNameOtherBuilding" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="customFloorNamePurposedStoreyAdded" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="customFloorNameFarNon" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="customFloorNameFar" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="customFloorNameTotalFloorArea" sizeClass="c-small" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>{floorArray && floorArray.floors.length + 2}</Table.Cell>
											<Table.Cell>{data.miscellaneous} </Table.Cell>
											<Table.Cell>
												<TableInputIm name="miscellaneousOtherBuilding" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="miscellaneousPurposedStoreyAdded" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="miscellaneousFarNon" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="miscellaneousFar" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="miscellaneousTotalFloorArea" sizeClass="c-small" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												<TableInputIm name="blankSn" sizeClass="c-tiny" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankFloorName" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankOtherBuilding" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankPurposedStoreyAdded" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankFarNon" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankFar" sizeClass="c-small" />{' '}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="blankTotalFloorArea" sizeClass="c-small" />{' '}
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell></Table.Cell>
											<Table.Cell colspan="3">
												{data.tax}
												<DashedNormalInputIm name="tax" />
												{data.perSqFt}
											</Table.Cell>
											<Table.Cell colspan="2">
												{data.rs}
												<DashedNormalInputIm name="taxRs" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="taxTotalArea" sizeClass="c-small" />
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell></Table.Cell>
											<Table.Cell colspan="3">
												{data.deposit}
												<DashedNormalInputIm name="deposit" />
												{data.perSqFt}
											</Table.Cell>
											<Table.Cell colspan="2">
												{data.rs}
												<DashedNormalInputIm name="depositRs" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="depositTotalArea" sizeClass="c-small" />
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell></Table.Cell>
											<Table.Cell colspan="3">
												{data.totalAmount}
												<DashedNormalInputIm name="totalAmount" />
												{data.perSqFt}
											</Table.Cell>
											<Table.Cell colspan="2">
												{data.rs}
												<DashedNormalInputIm name="totalAmountRs" />
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="totalAmountTotalArea" sizeClass="c-small" />
											</Table.Cell>
										</Table.Row>
									</Table>
									<p>{data.dataLine}</p>
									<FlexSingleRight>
										<FooterSignature designations={[data.designersSignature]} />
									</FlexSingleRight>
									<br />
									<hr />
									<SectionHeader>
										<h2> {data.officeUse}</h2>
									</SectionHeader>
									<br />
									{/* table 7 */}

									<Table border="1px solid black" collasping className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell>{data.reportedBy}</Table.HeaderCell>
												<Table.HeaderCell>{data.remarks}</Table.HeaderCell>
												<Table.HeaderCell>{data.signature}</Table.HeaderCell>
											</Table.Row>
										</Table.Header>

										<Table.Row>
											<Table.Cell>{data.documentCheck}</Table.Cell>
											<Table.Cell>
												<TableInputIm name="documentCheckRemarks" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												{data.amin}
												<br />
												{data.batoField}
												<br />
												{data.batoNaksha}
											</Table.Cell>
											<Table.Cell>
												<TableInputIm name="batoFieldRemarks" />
												<TableInputIm name="batoNakshaRemarks" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>{data.juniorEngineer}</Table.Cell>
											<Table.Cell>
												<TableInputIm name="juniorEngineerRemarks" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>{data.approvedBy}</Table.Cell>
											<Table.Cell>
												<TableInputIm name="approvedByRemarks" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
									</Table>

									{/* table 8 */}
									<Table border="1px solid black" collasping columns="3" className="english-div">
										<Table.Row>
											<Table.Cell colspan="3">
												<SectionHeader>
													<h2>{data.taxSection}</h2>
												</SectionHeader>
											</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												{data.taxReceiptNo}
												<DashedNormalInputIm name="taxReceipt" />
											</Table.Cell>
											<Table.Cell>
												{data.rs}
												<DashedNormalInputIm name="taxReceiptRs" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												{data.depositNo}
												<DashedNormalInputIm name="depositReceipt" />
											</Table.Cell>
											<Table.Cell>
												{data.rs}
												<DashedNormalInputIm name="depositReceiptRs" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>
												<FlexSingleRight>{data.total}</FlexSingleRight>
											</Table.Cell>
											<Table.Cell>
												{data.rs}
												<DashedNormalInputIm name="totalRs" />
											</Table.Cell>
											<Table.Cell></Table.Cell>
										</Table.Row>
									</Table>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									handleSubmit={handleSubmit}
									formUrl={fromUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
								/>
							</Form>
						);
					}}
				></Formik>
			</div>
		);
	}
}
const FormToBeFilledByDesigner = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.FormToBeFilledByDesigner).setForm().getParams()]}
		onBeforeGetContent={{
			param1: [PrintIdentifiers.CHECKBOX_LABEL],
			param2: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <FormToBeFilledByDesignerComponent {...props} parentProps={parentProps} />}
	/>
);
export default FormToBeFilledByDesigner;
