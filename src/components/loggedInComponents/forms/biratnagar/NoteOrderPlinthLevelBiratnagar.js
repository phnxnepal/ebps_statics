import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { noteOrderPilenthLang } from '../../../../utils/data/noteOrderPilenthLang';
import { Formik, getIn } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { Form } from 'semantic-ui-react';
import { getJsonData, prepareMultiInitialValues, handleSuccess } from '../../../../utils/dataUtils';
import { DashedLangDateField } from '../../../shared/DateField';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { FooterSignature } from '../formComponents/FooterSignature';
import { mappingDirection } from '../../../../utils/data/BuildingBuildCertificateData';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { validateNormalNumber, validateNullableNormalNumber } from '../../../../utils/validationUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { checkError } from '../../../../utils/dataUtils';
import { DashedMultiUnitAreaInput, DashedMultiUnitLengthInput } from '../../../shared/EbpsUnitLabelValue';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
const { object } = require('yup');
const validate = object({
	groundCoverage: validateNormalNumber,
	plinthLength: validateNullableNormalNumber,
	plinthWidth: validateNullableNormalNumber,
	buildingHeight: validateNormalNumber,
	// totalArea: validateNullableNormalNumber,

	areaFromMiddle: validateNormalNumber,
	chodnuparneSetBack: validateNormalNumber,
	minFromMiddle: validateNormalNumber,
	chodekoSetback: validateNormalNumber,
	totalChodeko: validateNormalNumber,
});
const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];
const initialVal = {
	amin: '',
	direction: '',
	road: '',
	roadArea: '',
	areaFromMiddle: '',
	chodnuparneSetBack: '',
	minFromMiddle: '',
	chodekoSetback: '',
	totalChodeko: '',
	buildingHeight: '',
	pratibedan: '',
	juniorEngineer: '',
};

const requiredData = noteOrderPilenthLang.biratnagar;
class NoteOrderPlinthLevelBiratnagarComponent extends Component {
	constructor(props) {
		super(props);

		const { otherData, prevData, permitData, DEFAULT_UNIT_LENGTH } = this.props;

		const mapTech = getJsonData(otherData.mapTech);

		const initialValues = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: {
					areaFromMiddleUnit: DEFAULT_UNIT_LENGTH,
					minFromMiddleUnit: DEFAULT_UNIT_LENGTH,
					chodekoSetbackUnit: DEFAULT_UNIT_LENGTH,
					totalChodekoUnit: DEFAULT_UNIT_LENGTH,
					buildingHeightUnit: DEFAULT_UNIT_LENGTH,
					chodnuparneSetBackUnit: DEFAULT_UNIT_LENGTH,
					plinthLengthUnit: DEFAULT_UNIT_LENGTH,
				},
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true) },
				reqFields: ['date'],
			},
			{
				obj: permitData,
				reqFields: ['surrounding'], // {buildingHeight: 30}
			},
			{
				obj: mapTech,
				reqFields: [
					'buildingHeight',
					'buildingDetailfloor',
					'plinthLength',
					'plinthDetails',
					'plinthLengthUnit',
					'purposeOfConstruction',
					'cType',
					'buildingDetailfloor',
					'coverageDetails',
				], // {buildingHeight: 30}
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);

		this.state = { initialValues };
	}

	render() {
		const { permitData, formUrl, hasDeletePermission, userData, isSaveDisabled, prevData, postAction, hasSavePermission, errors: reduxErrors } = this.props;
		const { initialValues } = this.state;
		return (
			<>
				<div>{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}</div>

				<Formik
					initialValues={initialValues}
					validationSchema={validate}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.noteorderPilengthLevel, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ values, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form>
							<div ref={this.props.setRef}>
								<LetterHeadFlex userInfo={userData} />
									
								{/* <SectionHeader>
									<h1>{userData.organization.name}</h1>
										<h3>{requiredData.nagarkaryalaya}</h3>
									<h3>
										{userData.organization.name}, {userData.organization.province}, {requiredData.nepal}
									</h3>
								</SectionHeader> */}

								<FlexSingleRight>
									<label htmlFor="date">{requiredData.date}</label>
									<DashedLangDateField
										inline={true}
										name="date"
										setFieldValue={setFieldValue}
										value={values.date}
										error={errors.date}
									/>
								</FlexSingleRight>
								<SectionHeader>
									<h2>{requiredData.subject}</h2>
								</SectionHeader>
								<br />
								<br />
								<label htmlFor="oldMunicipal">{requiredData.sabik}</label>
								<span id="oldMunicipal">{permitData.oldMunicipal}</span>
								<label htmlFor="oldWardNo">{requiredData.wadanumber}</label>
								<span id="oldWardNo">{permitData.oldWardNo}</span>
								<label htmlFor="newWardNo">{requiredData.currentwadanumber}</label>
								<span id="newWardNo">{permitData.newWardNo}</span>
								{/* <label htmlFor="east">{requiredData.east}</label>
							<DashedLangInput id="east" name="surrounding.0.kittaNo" value={getIn(values, `surrounding.0.kittaNo`)} /> */}
								{values.surrounding &&
									values.surrounding.map((index, i) => (
										<>
											{mappingDirection.find((fl) => fl.direction === index.side).value}{' '}
											<DashedLangInput
												name={`surrounding.${i}.kittaNo`}
												setFieldValue={setFieldValue}
												value={getIn(values, `surrounding.${i}.kittaNo`)}
												error={getIn(errors, `surrounding.${i}.kittaNo`)}
											/>
										</>
									))}
								{/* <label htmlFor="west">{requiredData.west}</label>

							<DashedLangInput id="west" name="west" />

							<label htmlFor="north">{requiredData.north}</label>
							<DashedLangInput id="north" name="north" />

							<label htmlFor="south">{requiredData.south}</label>
							<DashedLangInput id="south" name="south" /> */}
								<label htmlFor="kittaNo">{requiredData.kittaNumber}</label>
								<span id="kittaNo">{permitData.kittaNo}</span>
								<label htmlFor="landArea">{requiredData.chetrafal}</label>
								<span id="landArea">
									{permitData.landArea}
									<span>{` ${permitData.landAreaType}`}</span>
								</span>
								<DashedMultiUnitLengthInput
									name="plinthLength"
									unitName="plinthLengthUnit"
									relatedFields={['plinthWidth', 'plinthHeight', 'plinthDetails']}
									label={requiredData.length}
								/>
								<DashedMultiUnitLengthInput
									name="plinthWidth"
									unitName="plinthLengthUnit"
									relatedFields={['plinthHeight', 'plinthLength', 'plinthDetails']}
									label={requiredData.breadth}
								/>
								<DashedMultiUnitLengthInput
									name="plinthHeight"
									unitName="plinthLengthUnit"
									relatedFields={['plinthDetails', 'plinthWidth', 'plinthLength']}
									label={requiredData.height}
								/>
								<DashedMultiUnitAreaInput
									name="plinthDetails"
									unitName="plinthLengthUnit"
									label={requiredData.kulBargaFoot}
									squareOptions={squareUnitOptions}
									relatedFields={['plinthLength', 'plinthWidth', 'plinthHeight']}
								/>
								{/* <label htmlFor="totalArea">{}</label>
							<DashedLangInput
								id="totalArea"
								name="totalArea"
								value={values.plinthDetails}
								setFieldValue={setFieldValue}
								error={errors.plinthDetails}
							/> */}
								<label htmlFor="purposeOfConstruction">{requiredData.prayojan}</label>
								<span id="purposeOfConstruction">{permitData.purposeOfConstruction}</span>
								{requiredData.pakki}
								{values.buildingDetailfloor}
								<label htmlFor="permission">{requiredData.permission}</label>
								<span id="permission">{permitData.newWardNo}</span>
								<label htmlFor="applicantName">{requiredData.nibedakKoNaam}</label>
								<span id="applicantName">{permitData.applicantName}</span>
								<label htmlFor="landPassDate">{requiredData.lemiti}</label>
								<span id="landPassDate">{permitData.landPassDate}</span>
								<label htmlFor="juniorEngineer">{requiredData.juniorEngineer}</label>
								<DashedLangInput
									id="juniorEngineer"
									name="juniorEngineer"
									value={values.juniorEngineer}
									error={errors.juniorEngineer}
									setFieldValue={setFieldValue}
								/>
								<label htmlFor="amin">{requiredData.amin}</label>
								<DashedLangInput id="amin" name="amin" value={values.amin} error={errors.amin} setFieldValue={setFieldValue} />
								<label htmlFor="direction">{requiredData.direction}</label>
								<DashedLangInput
									id="direction"
									name="direction"
									value={values.direction}
									error={errors.direction}
									setFieldValue={setFieldValue}
								/>
								<label htmlFor="road">{requiredData.road}</label>
								<DashedLangInput id="road" name="road" value={values.road} error={errors.road} setFieldValue={setFieldValue} />
								<label htmlFor="roadArea">{requiredData.roadArea}</label>
								<DashedLangInput
									id="roadArea"
									name="roadArea"
									value={values.roadArea}
									error={errors.roadArea}
									setFieldValue={setFieldValue}
								/>
								<DashedMultiUnitLengthInput name="areaFromMiddle" unitName="areaFromMiddleUnit" label={requiredData.areaFromMiddle} />
								<DashedMultiUnitLengthInput
									name="chodnuparneSetBack"
									unitName="chodnuparneSetBackUnit"
									label={requiredData.chodnuparneSetback}
								/>
								<DashedMultiUnitLengthInput name="minFromMiddle" unitName="minFromMiddleUnit" label={requiredData.minFromMiddle} />

								<DashedMultiUnitLengthInput name="chodekoSetback" unitName="chodekoSetbackUnit" label={requiredData.chodekoSetback} />

								<DashedMultiUnitLengthInput name="totalChodeko" unitName="totalChodekoUnit" label={requiredData.totalChodeko} />

								<DashedMultiUnitLengthInput name="buildingHeight" unitName="buildingHeightUnit" label={requiredData.gharKoUchai} />
								<label htmlFor="groundCoverage">{requiredData.groundCoverage}</label>
								<DashedNormalInputIm
									id="groundCoverage"
									name="groundCoverage"
									value={values.groundCoverage}
									setFieldValue={setFieldValue}
									error={errors.groundCoverage}
								/>
								<label htmlFor="pratibedan">{requiredData.pratibedan}</label>
								<DashedLangInput
									id="pratibedan"
									name="pratibedan"
									value={values.pratibedan}
									error={errors.pratibedan}
									setFieldValue={setFieldValue}
								/>
								{requiredData.nakshaPass}
								<FooterSignature
									designations={[
										requiredData.peshGarne,
										requiredData.jeng,
										requiredData.phatPramukh,
										requiredData.engineer,
										requiredData.pramukh,
									]}
								/>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const NoteOrderPlinthLevelBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.noteorderPilengthLevel).setForm().getParams(), new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams()]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
			...PrintParams.INLINE_FIELD,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <NoteOrderPlinthLevelBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default NoteOrderPlinthLevelBiratnagar;
