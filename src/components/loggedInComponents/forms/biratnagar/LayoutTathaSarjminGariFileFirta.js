import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { SectionHeader } from '../../../uiComponents/Headers';
import { LayoutTathaSarjminGariFileFirtaData } from '../../../../utils/data/LayoutTathaSarjminGariFileFirtaData';
import { FlexSingleRight, FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
const { object } = require('yup');
const reqData = LayoutTathaSarjminGariFileFirtaData;
const validate = object({
	date: validateNullableNepaliDate,
});
const initialVal = { date: '', binamanapaWadaNo: '', fileNumber: '', fileThan: '' };
class LayoutTathaSarjminGariFileFirtaComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true) },
				reqFields: [],
			}
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled, permitData } = this.props;
		const { initVal } = this.state;
		return (
			<div>
				<Formik
					initialValues={initVal}
					validationSchema={validate}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.layoutTathaSarjaminGariFileFirta, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, errors, setFieldValue, handleSubmit, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h1>{reqData.mahanagarpalika}</h1>
									<h3>{reqData.address}</h3>
								</SectionHeader>
								<FlexSingleRight>
									{reqData.miti}:{' '}
									<CompactDashedLangDate name="date" value={values.date} error={errors.date} setFieldValue={setFieldValue} />
								</FlexSingleRight>
								<br />
								<br />
								<SectionHeader>
									<h2 className="underline">{reqData.subject}</h2>
								</SectionHeader>
								<br />
								<br />
								<FlexSingleLeft>
									<div>{reqData.shreeWadaSamiti}</div>
									<div>
										{reqData.binamanapaWadaNo} {permitData.newWardNo}
									</div>
								</FlexSingleLeft>
								<br />
								<div>
									{reqData.wadaNumber}
									{permitData.newWardNo}

									{reqData.basneShree}
									{permitData.applicantName}

									{reqData.fileNo}
									<CompactDashedLangInput
										name="fileNumber"
										value={values.fileNumber}
										error={errors.fileNumber}
										setFieldValue={setFieldValue}
									/>

									{reqData.fileThan}
									<CompactDashedLangInput
										name="fileThan"
										value={values.fileThan}
										error={errors.fileThan}
										setFieldValue={setFieldValue}
									/>

									{reqData.dataLine}
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const LayoutTathaSarjminGariFileFirta = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.layoutTathaSarjaminGariFileFirta).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <LayoutTathaSarjminGariFileFirtaComponent {...props} parentProps={parentProps} />}
	/>
);
export default LayoutTathaSarjminGariFileFirta;
