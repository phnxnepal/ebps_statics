import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';

const reqData = requiredData.frameStructure;
export class PlainAndReinforcedConcrete extends Component {
	render() {
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colSpan={8}>
						<b>{reqData.nbc211}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.typeOfFoundation}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="typeOfFoundationAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="typeOfFoundationRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.depthOfFoundation}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="depthOfFoundationAsPerSubmittedDesignFrame" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="depthOfFoundationRemarksFrame" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.sizesOFFoundation}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="sizesOFFoundationAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="sizesOFFoundationRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.sizesAndNosOfBarsProvidedInColumns}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="sizesAndNosOfBarsProvidedInColumnsAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="sizesAndNosOfBarsProvidedInColumnsRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.concreteGrade}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="concreteGradeAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="concreteGradeRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.reinforcementSteelGrade}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="reinforcementSteelGradeAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="reinforcementSteelGradeRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.criticalSizeOfSlabPanel}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="criticalSizeOfSlabPanelAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="criticalSizeOfSlabPanelRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.slabThickness}</Table.Cell>
					<Table.Cell colSpan="4">
						<TableInputIm name="slabThicknessAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="slabThicknessRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.beamCharacteristics}</Table.Cell>
					<Table.Cell colSpan="4">{reqData.conditionsOfBeams}</Table.Cell>
					<Table.Cell width={2}></Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}></Table.Cell>
					<Table.Cell>{reqData.cantilever}</Table.Cell>
					<Table.Cell>{reqData.simplySupported}</Table.Cell>
					<Table.Cell>{reqData.oneSideContinuous}</Table.Cell>
					<Table.Cell>{reqData.bothSideContinuous}</Table.Cell>
					<Table.Cell width={2} rowSpan={4}></Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.maximumSpanOfCorrespondingBeam}</Table.Cell>
					<Table.Cell>
						<TableInputIm name="maximumSpanOfCorrespondingBeamcantilever" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="maximumSpanOfCorrespondingBeamsimplySupported" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="maximumSpanOfCorrespondingBeamoneSideContinuous" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="maximumSpanOfCorrespondingBeambothSideContinuous" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.depthOfCorrespondingBeam}</Table.Cell>
					<Table.Cell>
						<TableInputIm name="depthOfCorrespondingBeamcantilever" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="depthOfCorrespondingBeamsimplySupported" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="depthOfCorrespondingBeamoneSideContinuous" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="depthOfCorrespondingBeambothSideContinuous" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={5}>{reqData.widthOfCorrespondingBeam}</Table.Cell>
					<Table.Cell>
						<TableInputIm name="widthOfCorrespondingBeamcantilever" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="widthOfCorrespondingBeamsimplySupported" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="widthOfCorrespondingBeamoneSideContinuous" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="widthOfCorrespondingBeambothSideContinuous" />
					</Table.Cell>
				</Table.Row>
			</Table.Body>
		);
	}
}

export default PlainAndReinforcedConcrete;
