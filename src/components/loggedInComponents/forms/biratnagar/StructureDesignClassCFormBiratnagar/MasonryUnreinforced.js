import React from 'react';
import { Table } from 'semantic-ui-react';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';
import { BlockFloorComponentsV2 } from '../../../../shared/formComponents/BlockFloorComponents';

const reqData = requiredData.loadBearingWallStructure;
export const MasonryUnreinforced = ({ floorArray, formattedFloors }) => {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan={7}>
					<b>{reqData.nbc210}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.depthOfFoundation}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="depthOfFoundationAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="depthOfFoundationRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.widthOfFoundation}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="widthOfFoundationAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="widthOfFoundationRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.concreteGrade}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="concreteGradeAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="concreteGradeRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.brickCrusingStrength}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="brickCrusingStrengthAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="brickCrusingStrengthRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.mortarRatioForLoadBearing}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="mortarRatioForLoadBearingAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="mortarRatioForLoadBearingRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.floor}</Table.Cell>
				<Table.Cell>{reqData.wallHeight}</Table.Cell>
				<Table.Cell>{reqData.wallThickness}</Table.Cell>
				<Table.Cell>{reqData.maximumLength}</Table.Cell>
				<Table.Cell></Table.Cell>
			</Table.Row>
			<BlockFloorComponentsV2 floorArray={floorArray} formattedFloors={formattedFloors}>
				{(floorObj, block) => (
					<>
						{floorObj.map((currentRow) => (
							<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
								<Table.Cell></Table.Cell>
								<Table.Cell>{`${currentRow.label.englishName} ${floorArray.hasBlocks ? 'Block ' + block : ''}`}</Table.Cell>
								<Table.Cell>
									<TableInputIm name={currentRow.fieldName('floorAreas', 'wallHeight')} />
								</Table.Cell>
								<Table.Cell>
									<TableInputIm name={currentRow.fieldName('floorAreas', 'wallThickness')} />
								</Table.Cell>
								<Table.Cell>
									<TableInputIm name={currentRow.fieldName('floorAreas', 'maximumLength')} />
								</Table.Cell>
								<Table.Cell>
									<TableInputIm name={currentRow.fieldName('floorAreas', 'remarks')} />
								</Table.Cell>
							</Table.Row>
						))}
					</>
				)}
			</BlockFloorComponentsV2>
			{/* <Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.groundFloor}</Table.Cell>
				<Table.Cell>
					<TableInputIm name="groundFloorwallHeight" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="groundFloorwallThickness" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="groundFloormaximumLength" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.firstFloor}</Table.Cell>
				<Table.Cell>
					<TableInputIm name="firstFloorwallHeight" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="firstFloorwallThickness" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="firstFloormaximumLength" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.secondFloor}</Table.Cell>
				<Table.Cell>
					<TableInputIm name="secondFloorwallHeight" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="secondFloorwallThickness" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="secondFloormaximumLength" />
				</Table.Cell>
			</Table.Row> */}
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>
					<DashedNormalInputIm name="extraInput1" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput1wallHeight" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput1wallThickness" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput1maximumLength" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput1Remarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>
					<DashedNormalInputIm name="extraInput2" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput2wallHeight" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput2wallThickness" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput2maximumLength" />
				</Table.Cell>
				<Table.Cell>
					<TableInputIm name="extraInput2Remarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell colSpan={6}>
					<b>{reqData.openingDetails}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell colSpan={6}>{reqData.leastDistance}</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.totalLength}</Table.Cell>
				<Table.Cell colSpan="3">
					<RadioInput name="totalLengthAsPerSubmittedDesignLoadBearing" space={true} option={reqData.yes} />
					<RadioInput name="totalLengthAsPerSubmittedDesignLoadBearing" space={true} option={reqData.no} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="totalLengthRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.horizontalDistance}</Table.Cell>
				<Table.Cell colSpan="3">
					<RadioInput name="horizontalDistanceAsPerSubmittedDesignLoadBearing" space={true} option={reqData.yes} />
					<RadioInput name="horizontalDistanceAsPerSubmittedDesignLoadBearing" space={true} option={reqData.no} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="horizontalDistanceRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.verticalDistance}</Table.Cell>
				<Table.Cell colSpan="3">
					<RadioInput name="verticalDistanceAsPerSubmittedDesignLoadBearing" space={true} option={reqData.yes} />
					<RadioInput name="verticalDistanceAsPerSubmittedDesignLoadBearing" space={true} option={reqData.no} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="verticalDistanceRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.ifAnyOfAbove}</Table.Cell>
				<Table.Cell colSpan="3">
					<RadioInput name="ifAnyOfAboveAsPerSubmittedDesignLoadBearing" space={true} option={reqData.yes} />
					<RadioInput name="ifAnyOfAboveAsPerSubmittedDesignLoadBearing" space={true} option={reqData.no} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="ifAnyOfAboveRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.bandsProvided}</Table.Cell>
				<Table.Cell colSpan="3">
					<CheckboxInput
						options={[reqData.plinthLevel, reqData.lintelLevel, reqData.roofLevel, reqData.gableBand]}
						name="categoryOfBuildingAsPerSubmittedDesignLoadBearing"
					/>
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="categoryOfBuildingRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell colSpan={7}>{reqData.verticalSteelReinforcement}</Table.Cell>
			</Table.Row>
				<BlockFloorComponentsV2 floorArray={floorArray} formattedFloors={formattedFloors}>
				{(floorObj, block) => (
					<>
						{floorObj.map((currentRow) => (
							<Table.Row key={`${currentRow.floor}_${currentRow.block}`}>
								<Table.Cell></Table.Cell>
								<Table.Cell>{`${currentRow.label.englishName} ${floorArray.hasBlocks ? 'Block ' + block : ''}`}</Table.Cell>
								<Table.Cell colSpan="3">
									<TableInputIm name={currentRow.fieldName('floorAreas', 'diameter')} />
								</Table.Cell>
								<Table.Cell>
									<TableInputIm name={currentRow.fieldName('floorAreas', 'diameterRemarks')} />
								</Table.Cell>
							</Table.Row>
						))}
					</>
				)}
			</BlockFloorComponentsV2>
			{/* <Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.groundFloor}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="groundFloorAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="groundFloorRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.firstFloor}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="firstFloorAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="firstFloorRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.secondFloor}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="secondFloorAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="secondFloorRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row> */}
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>
					<DashedNormalInputIm name="extraOne" />
				</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="extraOneAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="extraOneRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell width={2}>{reqData.ccDistance}</Table.Cell>
				<Table.Cell colSpan="3">
					<TableInputIm name="ccDistanceAsPerSubmittedDesignLoadBearing" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="ccDistanceRemarksLoadBearing" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell colSpan={7}>
					<b>{reqData.note}</b>
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
};

export default MasonryUnreinforced;
