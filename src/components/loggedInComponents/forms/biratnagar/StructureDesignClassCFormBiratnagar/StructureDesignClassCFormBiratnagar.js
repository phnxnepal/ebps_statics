import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form, Table } from 'semantic-ui-react';
import FormContainerV2 from '../../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../../utils/paramUtil';
import api from '../../../../../utils/api';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { SectionHeader } from '../../../../uiComponents/Headers';
import GeneralStructureDesignClassCFormPiece from './GeneralStructureDesignClassCFormPiece';
import SiteConsiderationForSeismicHazards from './SiteConsiderationForSeismicHazards';
import ConstructionSafetyClassC from './ConstructionSafetyClassC';
import PlainAndReinforcedConcrete from './PlainAndReinforcedConcrete';
import MasonryUnreinforced from './MasonryUnreinforced';
import { initialVal } from './initialValuesStructureDesignClassC';
import { prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../../utils/dataUtils';
import ErrorDisplay from '../../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../../shared/SaveButtonValidation';
import { getLocalStorage } from '../../../../../utils/secureLS';
import { BUILDING_CLASS } from '../../../../../utils/constants';
import { FloorBlockArray } from '../../../../../utils/floorUtils';

const mainHeading = requiredData.mainHeading;
const tableHeading = requiredData.tableHeader;
class StructureDesignClassCFormBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const { unit, areaUnit } = floorArray.getEnglishFloorUnit();
		const formattedFloors = floorArray.getFormattedFloors();

		let initval = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: {
					categoryOfBuilding: getLocalStorage(BUILDING_CLASS) || '',
					...floorArray.getInitialValue('englishCount', 'numberOfStorey', floorArray.getTopFloor()),
					...floorArray.getInitialValue(null, 'totalHeightOfStructure', floorArray.getSumOfHeights()),
					...floorArray.getInitialValue('area', 'plinthAreaOfBuilding', floorArray.getBottomFloor()),
					floorUnitArea: areaUnit,
					floorUnit: unit,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initval,
			floorArray,
			blocks: floorArray.getBlocks(),
			formattedFloors,
		};
	}
	render() {
		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initval, floorArray, blocks, formattedFloors } = this.state;
		return (
			<Formik
				initialValues={initval}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						await this.props.postAction(api.structureDesignC, values, true);
						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						actions.setSubmitting(false);
					} catch (err) {
						actions.setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
						<div ref={this.props.setRef}>
							<div className="english-div">
								<SectionHeader>
									<h3>{mainHeading.prabidhikBibarn}</h3>
									<h3>{mainHeading.structuralDesignSambandhi}</h3>
									<h3 className="english-div">{mainHeading.checkList}</h3>
									<h3 className="english-div">{mainHeading.plinthAreaHeader}</h3>
									<h3 className="english-div">{mainHeading.nbc000}</h3>
									<h3 className="english-div">{mainHeading.inCaseOfManyUnits}</h3>
								</SectionHeader>
								<br />

								<Table columns={7} className="structure-ui-table">
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell width={1}>{tableHeading.sn}</Table.HeaderCell>
											<Table.HeaderCell width={4}>{tableHeading.description}</Table.HeaderCell>
											<Table.HeaderCell colSpan="3">{tableHeading.asPerSubmittedDesign}</Table.HeaderCell>
											<Table.HeaderCell width={2}>{tableHeading.remarks}</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									{/* general table  and 2.3*/}
									<GeneralStructureDesignClassCFormPiece value={values} floorArray={floorArray} blocks={blocks} />
									{/* 2.9 site consideration for seismic hazards*/}
									<SiteConsiderationForSeismicHazards />
									{/* 2.15 construction safety*/}
									<ConstructionSafetyClassC />
								</Table>
								<br />
								{values.structuralSystemBuilding === 'Frame' ? (
									<div>
										<SectionHeader>
											<h3 className="underline">I. STRUCTURAL DATA FOR FRAME STRUCTURE</h3>
										</SectionHeader>
										<Table columns={8} className="structure-ui-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={1}>{tableHeading.sn}</Table.HeaderCell>
													<Table.HeaderCell width={5}>{tableHeading.description}</Table.HeaderCell>
													<Table.HeaderCell colSpan="4">{tableHeading.asPerSubmittedDesign}</Table.HeaderCell>
													<Table.HeaderCell width={2}>{tableHeading.remarks}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<PlainAndReinforcedConcrete />
										</Table>
									</div>
								) : values.structuralSystemBuilding === 'Load Bearing' ? (
									<div>
										<SectionHeader>
											<h3 className="underline">II. STRUCTURAL DATA FOR LOAD BEARING WALL STRUCTURE</h3>
										</SectionHeader>
										<Table columns={7} className="structure-ui-table">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width={1}>{tableHeading.sn}</Table.HeaderCell>
													<Table.HeaderCell width={5}>{tableHeading.description}</Table.HeaderCell>
													<Table.HeaderCell colSpan="3">{tableHeading.asPerSubmittedDesign}</Table.HeaderCell>
													<Table.HeaderCell width={2}>{tableHeading.remarks}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<MasonryUnreinforced floorArray={floorArray} formattedFloors={formattedFloors} />
										</Table>
									</div>
								) : null}
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}
const StructureDesignClassCFormBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.structureDesignC).setForm().getParams()]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui radio checkbox prabidhik', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <StructureDesignClassCFormBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default StructureDesignClassCFormBiratnagar;
