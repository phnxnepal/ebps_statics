import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';

import { TableInputIm } from '../../../../shared/TableInput';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';

const reqData = requiredData.nbc291081994;
export class SiteConsiderationForSeismicHazards extends Component {
	render() {
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colSpan={7}>
						<b>{reqData.heading}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.distanceFromToe}</Table.Cell>
					<Table.Cell colSpan="3">
						<TableInputIm name="distanceFromToeAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="distanceFromToeRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.distanceFromRiverBank}</Table.Cell>
					<Table.Cell colSpan="3">
						<TableInputIm name="distanceFromRiverBankAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="distanceFromRiverBankRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.soilTypeInFooting}</Table.Cell>
					<Table.Cell colSpan="3">
						<TableInputIm name="soilTypeInFootingAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="soilTypeInFootingRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.adoptedSafeBearingCapacity}</Table.Cell>
					<Table.Cell colSpan="3">
						<TableInputIm name="adoptedSafeBearingCapacityAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="adoptedSafeBearingCapacityRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.depthOfFoundation}</Table.Cell>
					<Table.Cell colSpan="3">
						<TableInputIm name="depthOfFoundationAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="depthOfFoundationRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.soilTestReportAvailable}</Table.Cell>
					<Table.Cell colSpan="3">
						<RadioInput name="soilTestReportAvailableAsPerSubmittedDesign" space={true} option={reqData.yes} />
						<RadioInput name="soilTestReportAvailableAsPerSubmittedDesign" space={true} option={reqData.no} />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="soilTestReportAvailableRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell colSpan={7}>{reqData.note}</Table.Cell>
				</Table.Row>
			</Table.Body>
		);
	}
}

export default SiteConsiderationForSeismicHazards;
