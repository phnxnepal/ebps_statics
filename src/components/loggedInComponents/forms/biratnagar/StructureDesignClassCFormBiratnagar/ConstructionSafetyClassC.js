import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { TableInputIm } from '../../../../shared/TableInput';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';

const reqData = requiredData.nbc2151141994;
export class ConstructionSafetyClassC extends Component {
	render() {
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colSpan={7}>
						<b>{reqData.heading}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.areYouSure}</Table.Cell>
					<Table.Cell colSpan="3">
						<RadioInput name="areYouSureAsPerSubmittedDesign" space={true} option={reqData.yes} />
						<RadioInput name="areYouSureAsPerSubmittedDesign" space={true} option={reqData.no} />
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="areYouSureRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={2}>{reqData.safetyWares}</Table.Cell>
					<Table.Cell colSpan="3">
						<CheckboxInput
							options={[reqData.hardHat, reqData.safetyGoogles, reqData.safetyBoots, reqData.safetyBelt, reqData.firstAidFacility]}
							name="safetyWaresAsPerSubmittedDesign"
						/>
					</Table.Cell>
					<Table.Cell width={2}>
						<TableInputIm name="safetyWaresRemarks" />
					</Table.Cell>
				</Table.Row>
			</Table.Body>
		);
	}
}

export default ConstructionSafetyClassC;
