import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { requiredData } from '../../../../../utils/data/StructureDesignClassCFormBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';

import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
import { BlockComponentsV2 } from '../../../../shared/formComponents/BlockFloorComponents';

const secondReqData = requiredData.nbc231021994;
const reqData = requiredData.generalTable;
export class GeneralStructureDesignClassCFormPiece extends Component {
	render() {
		const { value, floorArray, blocks } = this.props;
		return (
			<>
				<Table.Body>
					<Table.Row>
						<Table.Cell colSpan={7}>
							<b>{reqData.heading}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.categoryOfBuilding}</Table.Cell>
						<Table.Cell colSpan="3">
							<RadioInput name="categoryOfBuilding" space={true} option={reqData.a} />
							<RadioInput name="categoryOfBuilding" space={true} option={reqData.b} />
							<RadioInput name="categoryOfBuilding" space={true} option={reqData.c} />
							<RadioInput name="categoryOfBuilding" space={true} option={reqData.d} />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="categoryOfBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.functionalUseOfBuilding}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="functionalUseOfBuildingAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="functionalUseOfBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.plinthAreaOfBuilding}</Table.Cell>
						<Table.Cell colSpan="3">
							<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
								{(block) => {
									const fieldName = floorArray.getReducedFieldName('plinthAreaOfBuilding', block);
									return (
										<div className="inline-table-el" key={block}>
											{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} /> {value.floorUnitArea}
										</div>
									);
								}}
							</BlockComponentsV2>
							{/* <TableInputIm name="plinthAreaOfBuilding" /> */}
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="plinthAreaOfBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.numberOfStorey}</Table.Cell>
						<Table.Cell colSpan="3">
							<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
								{(block) => {
									const fieldName = floorArray.getReducedFieldName('numberOfStorey', block);
									return (
										<div className="inline-table-el" key={block}>
											{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} />{' '}
										</div>
									);
								}}
							</BlockComponentsV2>
							{/* <TableInputIm name="numberOfStorey" /> */}
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="numberOfStoreyRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.totalHeightOfStructure}</Table.Cell>
						<Table.Cell colSpan="3">
							<BlockComponentsV2 floorArray={floorArray} blocks={blocks}>
								{(block) => {
									const fieldName = floorArray.getReducedFieldName('totalHeightOfStructure', block);
									return (
										<div className="inline-table-el" key={block}>
											{block ? `Block ${block}` : ''} <TableInputIm name={fieldName} /> {value.floorUnit}
										</div>
									);
								}}
							</BlockComponentsV2>
							{/* <TableInputIm name="totalHeightOfStructure" /> */}
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="totalHeightOfStructureRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.structuralSystemBuilding}</Table.Cell>
						<Table.Cell colSpan="3">
							<RadioInput name="structuralSystemBuilding" space={true} option={reqData.frame} />
							<RadioInput name="structuralSystemBuilding" space={true} option={reqData.loadBearing} />
							<RadioInput name="structuralSystemBuilding" space={true} option={reqData.other} />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="structuralSystemBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.typeOfSoilInFoundation}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="typeOfSoilInFoundationAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="typeOfSoilInFoundationRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.adoptedSageBearingCapacity}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="adoptedSageBearingCapacityAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="adoptedSageBearingCapacityRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{reqData.provisionForFutureExtension}</Table.Cell>
						<Table.Cell colSpan="3">
							<RadioInput name="provisionForFutureExtensionAsPerSubmittedDesign" space={true} option={reqData.yes} />
							<RadioInput name="provisionForFutureExtensionAsPerSubmittedDesign" space={true} option={reqData.no} />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="provisionForFutureExtensionRemarks" />
						</Table.Cell>
					</Table.Row>
					{value.provisionForFutureExtensionAsPerSubmittedDesign === 'Yes' ? (
						<>
							<Table.Row>
								<Table.Cell width={1}></Table.Cell>
								<Table.Cell width={2}>{reqData.ifYes}</Table.Cell>
								<Table.Cell colSpan="3">
									<DashedNormalInputIm name="ifYesAsPerSubmittedDesign" /> {reqData.floors}
								</Table.Cell>
								<Table.Cell width={2}>
									<TableInputIm name="ifYesRemarks" />
								</Table.Cell>
							</Table.Row>
							<Table.Row>
								<Table.Cell width={1}></Table.Cell>
								<Table.Cell width={2}>{reqData.structuralDesignConsideration}</Table.Cell>
								<Table.Cell colSpan="3">
									<RadioInput name="structuralDesignConsiderationAsPerSubmittedDesign" space={true} option={reqData.yes} />
									<RadioInput name="structuralDesignConsiderationAsPerSubmittedDesign" space={true} option={reqData.no} />
								</Table.Cell>
								<Table.Cell width={2}>
									<TableInputIm name="structuralDesignConsiderationRemarks" />
								</Table.Cell>
							</Table.Row>
						</>
					) : null}
					<Table.Row>
						<Table.Cell colSpan={7}>
							<b>{secondReqData.heading}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell colSpan={6}>{secondReqData.specifyTheDesignWeightOfMaterials}</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{secondReqData.steel}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="steelAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="steelRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{secondReqData.brick}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="brickAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="brickRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{secondReqData.rcc}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="rccAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="rccRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={2}>{secondReqData.brickMasonry}</Table.Cell>
						<Table.Cell colSpan="3">
							<TableInputIm name="brickMasonryAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={2}>
							<TableInputIm name="brickMasonryRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell colSpan={7}>{secondReqData.note}</Table.Cell>
					</Table.Row>
				</Table.Body>
			</>
		);
	}
}

export default GeneralStructureDesignClassCFormPiece;
