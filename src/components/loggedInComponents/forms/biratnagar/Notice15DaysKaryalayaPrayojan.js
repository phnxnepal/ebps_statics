import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { requiredData } from '../../../../utils/data/Notice15DaysKaryalaPrayojanData';
import { Formik } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import { surroundingMappingFlat, checkError, handleSuccess, prepareMultiInitialValues, getJsonData } from '../../../../utils/dataUtils';
import { isEmpty } from '../../../../utils/functionUtils';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { Form } from 'semantic-ui-react';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { EbpsTextArea } from '../../../shared/EbpsForm';
const initalVal = {
	tarfaJaggama: '',
	nakshaDarakhast: '',
	tapasil: '',
};
const reqData = requiredData;
class Notice15DaysKaryalaPrayojanComponent extends Component {
	constructor(props) {
		super(props);
		const initVal = prepareMultiInitialValues(
			{
				obj: initalVal,
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initVal,
		};
	}

	render() {
		const { initVal } = this.state;
		const { formUrl, hasDeletePermission, userData, isSaveDisabled, prevData, hasSavePermission, permitData } = this.props;
		let placeName = userData.organization.name.split(" ")
		placeName = placeName[0]
		return (
			<Formik
				initialValues={initVal}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						await this.props.postAction(api.fifteenDaysNotice, values, true);
						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						actions.setSubmitting(false);
					} catch (err) {
						actions.setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, values, handleSubmit, setFieldValue, handleChange, errors, validateForm }) => (
					<Form>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
						<div ref={this.props.setRef}>
						<SectionHeader>
							<h1>{userData.organization.name}</h1>
							<h3>
								{userData.organization.name}, {userData.organization.province}, {reqData.nepal} 
							</h3><br/>
							<h3>{reqData.karyalayaPrayojankoLagi}</h3>
						</SectionHeader>
							<br />
							<div>
								{reqData.likhitamWadaNumber}
								{permitData.newWardNo}
								{reqData.kittaNumber}
								{permitData.kittaNo}
								{reqData.chetraFal}
								{permitData.landArea}({permitData.landAreaType}){/* ------- */}
								{reqData.tol}
								{permitData.sadak}
								{reqData.kojagga}
								{surroundingMappingFlat.map((surr) => {
									let dataSurr = {};
									// let index = 0;
									if (permitData.surrounding && Array.isArray(permitData.surrounding) && permitData.surrounding.length > 0) {
										dataSurr = permitData.surrounding.find((row) => row.side === surr.side) || {};
										// index = values.surrounding.indexOf(dataSurr);
									}
									return (
										<>
											{!isEmpty(dataSurr) && (
												<span key={surr.side}>
													{' '}
													{surr.value}: {dataSurr && dataSurr.kittaNo}{' '}
												</span>
											)}
										</>
									);
								})}
								{reqData.tarfaJaggama}
								<CompactDashedLangInput
									name="tarfaJaggama"
									value={values.tarfaJaggama}
									error={errors.tarfaJaggama}
									setFieldValue={setFieldValue}
								/>
								{reqData.gharBanaunaPauBhani}
								{placeName}
								{reqData.manapa}
								{permitData.newWardNo}
								{/* wardNumber */}
								{reqData.tol}
								{permitData.sadak}
								{/* tol */}
								{reqData.basneShree}
								{permitData.basneShree}
								{reqData.nakshaDarakhast}
								<CompactDashedLangInput
									name="nakshaDarakhast"
									value={values.nakshaDarakhast}
									error={errors.nakshaDarakhast}
									setFieldValue={setFieldValue}
								/>
								{reqData.dataline}
								{permitData.newWardNo}
								{reqData.dataline1}
							</div>
							<br />
							<SectionHeader>
								<h4 className="underline">{reqData.tapasil}</h4>
							</SectionHeader>
							<br />
							<EbpsTextArea placeholder={reqData.tapasil} name="tapasil" setFieldValue={setFieldValue} value={values.tapasil} />
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							handleSubmit={handleSubmit}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}
const Notice15DaysKaryalaPrayojan = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.fifteenDaysNotice).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <Notice15DaysKaryalaPrayojanComponent {...props} parentProps={parentProps} />}
	/>
);
export default Notice15DaysKaryalaPrayojan;
