import React, { Component } from 'react';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { prathamCharanAnumatiData } from '../../../../utils/data/prathamCharanAnumati';
import { Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';

import { checkError, handleSuccess, prepareMultiInitialValues, getJsonData } from '../../../../utils/dataUtils';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { getCertificateConstructionType } from '../../../../utils/enums/constructionType';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SignatureImageVertical } from '../formComponents/SignatureImage';

const data = prathamCharanAnumatiData;

class PrathamCharanAnumatiRequestComponent extends Component {
	constructor(props) {
		super(props);
		const initialValues = prepareMultiInitialValues({
			obj: getJsonData(this.props.prevData),
			reqFields: [], // {}
		});
		this.state = { initialValues };
	}
	render() {
		const {
			staticFiles,
			userData,
			postAction,
			permitData,
			formUrl,
			isSaveDisabled,
			hasSavePermission,
			hasDeletePermission,
			prevData,
		} = this.props;
		return (
			<div>
				<Formik
					initialValues={this.state.initialValues}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.PrathamCharanKaamGarneAnumati, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ errors, handleSubmit, isSubmitting, validateForm }) => {
						return (
							<Form loading={isSubmitting}>
								{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
								<div ref={this.props.setRef}>
									<div>{data.shriman_pramukh}</div>
									<div>{userData.organization.name}</div>
									<SectionHeader>
										<h3 className="underline">{data.subject}</h3>{' '}
									</SectionHeader>
									<br />
									<br />
									<br />
									{data.mahodaye}
									<br />
									{data.line1}
									{permitData.oldMunicipal}

									{data.line2}
									{permitData.oldWardNo}

									{data.line3}
									{userData.organization.name}
									{data.line2}
									{permitData.newWardNo}

									{data.line4}
									{permitData.kittaNo}

									{data.line5}
									{permitData.landArea}
									{` (${permitData.landAreaType})`}

									<label htmlFor="talle">{data.line6}</label>
									{getCertificateConstructionType(permitData.constructionType)}

									{data.line7}
									<br />
									<br />
									<br />
									<br />
									<FlexSingleRight>
										<SignatureImageVertical value={staticFiles.ghardhaniSignature} label={data.nibedak} showSignature={true} />
									</FlexSingleRight>
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									handleSubmit={handleSubmit}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
								/>
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}

const PrathamCharanAnumatiRequest = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.PrathamCharanKaamGarneAnumati).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		checkPreviousFiles={true}
		render={(props) => <PrathamCharanAnumatiRequestComponent {...props} parentProps={parentProps} />}
	/>
);
export default PrathamCharanAnumatiRequest;
