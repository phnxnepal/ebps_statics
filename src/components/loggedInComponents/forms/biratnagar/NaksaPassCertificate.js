import React, { useState, useEffect } from 'react';
import { Formik, getIn } from 'formik';
import { Form, Grid } from 'semantic-ui-react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { LetterHeadPhoto } from '../../../shared/LetterHead';
import { SectionHeader } from '../../../uiComponents/Headers';
import { naksaPassCertificateBiratnagar } from '../../../../utils/data/naskaPassCertificate';
import { FlexSpaceBetween } from '../../../uiComponents/FlexDivs';
import { ShreeDropdown } from '../formComponents/ShreeDropdown';
import { DashedLangDateField } from '../../../shared/DateField';
import { FooterSignatureMultiline } from '../formComponents/FooterSignature';
import { DashedLangInput, DashedNormalInput } from '../../../shared/DashedFormInput';
import { AllowancePaperInaruwaFloorTable } from '../ijajatPatraComponents/AllowancePaperComponents';
import { surrounding } from '../../../../utils/data/BuildingBuildCertificateData';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { PrintParams } from '../../../../utils/printUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } from '../../../../utils/constants';
import { getConstructionTypeValue, ConstructionTypeValue } from '../../../../utils/enums/constructionType';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';

const data = naksaPassCertificateBiratnagar;

const NaksaPassCertificateComponent = ({
	setRef,
	userData,
	permitData,
	prevData,
	otherData,
	formUrl,
	hasSavePermission,
	hasDeletePermission,
	isSaveDisabled,
	postAction,
	success,
	parentProps,
	errors: containerErrors,
}) => {
	const [initialValues, setInitialValues] = useState({});

	useEffect(() => {
		const constructionType = permitData.constructionType;
		const rajasowData = checkError(otherData.rajaswoVoucher);
		const mapTechData = getJsonData(otherData.mapTech);
		const prabhidhikData = getJsonData(otherData.prabidhikPratibedan);
		let cTypeWiseData = {
			heightUnit: (mapTechData.floor && mapTechData.floor[0].floorUnit) || DEFAULT_UNIT_LENGTH,
			areaUnit: prabhidhikData.plinthDetailsUnit || DEFAULT_UNIT_AREA,
			lineUnit: prabhidhikData.highTensionLineUnit || DEFAULT_UNIT_LENGTH,
			nadiUnit: prabhidhikData.publicPropertyUnit || DEFAULT_UNIT_LENGTH,
			setBackUnit: prabhidhikData.sadakAdhikarUnit || DEFAULT_UNIT_LENGTH,
		};
		if (constructionType) {
			if (getConstructionTypeValue(constructionType) === ConstructionTypeValue.PURANO_GHAR) {
				cTypeWiseData.sabikGround = mapTechData && mapTechData.allowable;
				cTypeWiseData.oldHeight = mapTechData && mapTechData.buildingHeight;
				cTypeWiseData.oldLine = prabhidhikData.isHighTensionLineDistance;
				cTypeWiseData.oldArea = prabhidhikData.floor && prabhidhikData.floor.area;
				cTypeWiseData.oldNadi = prabhidhikData.publicPropertyDistance;
				cTypeWiseData.oldSetBack = prabhidhikData.requiredDistance;
			}
			if (getConstructionTypeValue(constructionType) === ConstructionTypeValue.NAYA_NIRMAN) {
				cTypeWiseData.haalGround = mapTechData && mapTechData.allowable;
				cTypeWiseData.newHeight = mapTechData && mapTechData.buildingHeight;
				cTypeWiseData.newLine = prabhidhikData.isHighTensionLineDistance;
				cTypeWiseData.newArea = prabhidhikData.floor && prabhidhikData.floor.area;
				cTypeWiseData.newNadi = prabhidhikData.publicPropertyDistance;
				cTypeWiseData.newSetBack = prabhidhikData.requiredDistance;
			}
		}
		const initialValues = prepareMultiInitialValues(
			{
				obj: { shree: 'श्रीमान्', naksaPassCertificateDate: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: rajasowData,
				reqFields: ['rashidNumber', 'amtOfDharauti'],
			},
			{
				obj: prabhidhikData,
				reqFields: ['publicPropertyRequiredDistance'],
			},
			{
				obj: permitData,
				reqFields: ['surrounding'],
			},
			{
				obj: cTypeWiseData,
				reqFields: [],
			},
			{ obj: getJsonData(prevData), reqFields: [] }
		);

		setInitialValues(initialValues);
	}, [permitData, prevData, otherData]);

	useEffect(() => {
		if (success && success.success) {
			handleSuccess(checkError(prevData), parentProps, success);
		}
	}, [success, prevData, parentProps]);

	return (
		<div>
			{containerErrors && <ErrorDisplay message={containerErrors.message} />}
			<Formik
				initialValues={initialValues}
				enableReinitialize
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);

					values.applicationNo = permitData.applicantNo;

					try {
						await postAction(api.naksaPassCertificate, values, true);

						window.scroll(0, 0);
						if (success && success.success) {
							handleSuccess(checkError(prevData), parentProps, success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ handleSubmit, values, errors, setFieldValue, handleChange, validateForm }) => (
					<Form>
						<div ref={setRef}>
							<LetterHeadPhoto userInfo={userData} photo={permitData.photo} />
							<SectionHeader>
								<h2 className="end-section">{data.titie}</h2>
							</SectionHeader>
							<FlexSpaceBetween>
								<div>
									<div>
										<ShreeDropdown name="shree" /> {permitData.applicantName}
									</div>
									<div>
										{data.address} {permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
										{data.basne}
									</div>
								</div>
								<div>
									{`${data.date}: `}
									<DashedLangDateField
										compact={true}
										name="naksaPassCertificateDate"
										value={values.naksaPassCertificateDate}
										error={errors.naksaPassCertificateDate}
										setFieldValue={setFieldValue}
										inline={true}
									/>
								</div>
							</FlexSpaceBetween>
							<br />
							<div>
								{data.mahasaye}
								<div>
									<span className="indent-span">
										{data.body.organization} {userData.organization.name} {data.body.wardNo} {data.body.sabik}{' '}
										{permitData.oldWardNo} {data.body.haal} {permitData.newWardNo} {data.body.kittaNo} {permitData.kittaNo}{' '}
										{data.body.area} {permitData.landArea} {permitData.landAreaType} {data.body.tol}{' '}
										<DashedLangInput
											name="constructionType"
											setFieldValue={setFieldValue}
											value={values.constructionType}
											error={errors.constructionType}
										/>{' '}
										{data.body.banaunaPau}
										<DashedLangInput
											name="banaune"
											setFieldValue={setFieldValue}
											value={values.banaune}
											error={errors.banaune}
										/>{' '}
										{data.body.end}
									</span>
								</div>
							</div>
							<Grid className="no-padding-grid no-margin-field">
								<Grid.Row columns={2}>
									{values.surrounding &&
										values.surrounding.map((index, i) => (
											<Grid.Column key={i}>
												{surrounding.find((fl) => fl.direction === index.side).value}{' '}
												<DashedNormalInput
													name={`surrounding.${i}.sandhiyar`}
													value={getIn(values, `surrounding.${i}.sandhiyar`)}
													handleChange={handleChange}
													error={getIn(errors, `surrounding.${i}.sandhiyar`)}
												/>
											</Grid.Column>
										))}
								</Grid.Row>
							</Grid>
							<AllowancePaperInaruwaFloorTable setFieldValue={setFieldValue} values={values} errors={errors} />
							<FooterSignatureMultiline designations={data.footer} />
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		</div>
	);
};

const NaksaPassCertificate = (parentProps) => (
	<FormContainerV2
		api={[
			new ApiParam(api.naksaPassCertificate).setForm().getParams(),
			new ApiParam(api.mapTechnicalDescription, 'mapTech').getParams(),
			new ApiParam(api.prabhidikPratibedhanPesh, 'prabidhikPratibedan').getParams(),
			new ApiParam(api.rajaswaVoucher, 'rajaswoVoucher').getParams(),
		]}
		onBeforeGetContent={{
			// param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			...PrintParams.INLINE_FIELD,
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param2: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param4: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param1: ['getElementsByClassName', 'ui textarea', 'innerText'],
			// param7: ["15dayspecial"]
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <NaksaPassCertificateComponent {...props} parentProps={parentProps} />}
	/>
);
export default NaksaPassCertificate;
