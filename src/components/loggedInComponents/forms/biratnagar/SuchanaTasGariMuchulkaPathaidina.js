import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { Form, Table } from 'semantic-ui-react';
import { SectionHeader } from '../../../uiComponents/Headers';
import { suncahanTasGariMuchulkaPathaidinaData } from '../../../../utils/data/suchanaTasGariMuchulkaPathaidinaData';
import { FlexSingleLeft, FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { DashedSelect } from '../../../shared/Select';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { prepareMultiInitialValues, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import { TableInputIm } from '../../../shared/TableInput';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { checkError } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import {
	validateNullableNumber,
	validateNullableNepaliDate,
	validateNullableOfficialNumbers,
	validateNullableZeroNumber,
} from '../../../../utils/validationUtils';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { DashedAreaUnitInput, UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { FooterSignature } from '../formComponents/FooterSignature';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { distanceOptions } from '../../../../utils/optionUtils';
import { BlockFloorComponentsV2 } from '../../../shared/formComponents/BlockFloorComponents';
import { PrintParams } from '../../../../utils/printUtils';
const { object, array } = require('yup');
const reqData = suncahanTasGariMuchulkaPathaidinaData;
const structureBearing = [
	{
		key: reqData.frameStructure,
		text: reqData.frameStructure,
		value: reqData.frameStructure,
	},
	{
		key: reqData.loadBearing,
		text: reqData.loadBearing,
		value: reqData.loadBearing,
	},
];
const baniBanneBanaune = [
	{
		key: reqData.banisakeko,
		text: reqData.banisakeko,
		value: reqData.banisakeko,
	},
	{
		key: reqData.baniraheko,
		text: reqData.baniraheko,
		value: reqData.baniraheko,
	},
	{
		key: reqData.banaune,
		text: reqData.banaune,
		value: reqData.banaune,
	},
];
const initialValues = {
	date: '',
	chana: '',
	empty: '',
	frameStructureLoadBearingSelect: structureBearing[0].value,
	kulBargaFeet: '',
	baniSakekoBanirahekoBanaune: baniBanneBanaune[0].value,
};

class SuchanaTasGariMuchulkaPathaidinaComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const formattedFloors = floorArray.getFormattedFloors();

		let initialVal = prepareMultiInitialValues(
			{
				obj: initialValues,
				reqFields: [],
			},
			{
				obj: {
					date: getCurrentDate(true),
					floorAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					floorUnit: floorArray.getFloorUnit(),
					...floorArray.getInitialValue(null, 'kulBargaFeet', floorArray.getSumOfAreas()),
					floor: floorArray.getMultipleInitialValues(['length', 'area', 'width'], formattedFloors),
				},
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);

		const floorSchema = floorArray.getFloorSchema(['length', 'area', 'width'], validateNullableZeroNumber);

		const schema = object({
			date: validateNullableNepaliDate,
			chana: validateNullableOfficialNumbers,
			kulBargaFeet: validateNullableNumber,
			floor: array().of(object().shape(floorSchema)),
		});

		this.state = {
			initialVal,
			formattedFloors,
			floorArray,
			schema,
		};
	}
	render() {
		const { formUrl, postAction, permitData, prevData, userData, hasSavePermission, hasDeletePermission, isSaveDisabled } = this.props;
		const { floorArray, formattedFloors, schema } = this.state;
		let placeName = userData.organization.address.split(",")
		placeName = placeName[placeName.length - 1]
		return (
			<Formik
				initialValues={this.state.initialVal}
				validationSchema={schema}
				onSubmit={async (values, { setSubmitting }) => {
					setSubmitting(true);

					values.applicationNo = permitData.applicantNo;

					try {
						await postAction(api.SuchanaTaasGariMuchulka, values, true);

						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						setSubmitting(false);
					} catch (err) {
						setSubmitting(false);
						console.log('Error', err);
					}
				}}
			>
				{({ isSubmitting, values, handleSubmit, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
						<div ref={this.props.setRef}>
							<SectionHeader>
								{console.log(userData)}
								<h1>{userData.organization.name}</h1>
								<h3>
									{userData.organization.name}, {userData.organization.province}, {reqData.nepal} 
								</h3>
							</SectionHeader>
							<FlexSingleRight>
								{reqData.date}:{' '}
								<CompactDashedLangDate name="date" value={values.date} error={errors.date} setFieldValue={setFieldValue} />
							</FlexSingleRight>
							<FlexSingleLeft>
								{reqData.chana}:{' '}
								<CompactDashedLangInput name="chana" value={values.chana} error={errors.chana} setFieldValue={setFieldValue} />
							</FlexSingleLeft>
							<SectionHeader>
								<h3 className="underline">{reqData.bisaye}</h3>
							</SectionHeader>
							<br />
							<br />
							<div>{reqData.wadaSamatiKoKaryalaya}</div>
							<div>
								{reqData.wadaNumber} {permitData.newWardNo}
							</div>
							<br />
							<br />
							<div>
								{reqData.bmWadaNumber}

								{permitData.newWardNo}
								{reqData.basneShree}

								{permitData.applicantName}
								{reqData.sabik}

								{permitData.oldMunicipal}
								{reqData.wadaNumber}

								{permitData.sabikWadaNumber}
								{reqData.hal}

								{permitData.oldWardNo}
								{reqData.ma}

								<DashedSelect name="frameStructureLoadBearingSelect" options={structureBearing} />

								{reqData.kulBargaFeet}
								<DashedAreaUnitInput name="kulBargaFeet" unitName="floorAreaUnit" />
								{/* <CompactDashedLangInput name="kulBargaFeet" /> */}

								{reqData.ko}
								<DashedSelect name="baniSakekoBanirahekoBanaune" options={baniBanneBanaune} />

								{reqData.tapasilMaUllekh}
							</div>
							<br />
							<br />
							<div>
								{reqData.gharKoKisim}
								<DashedSelect name="frameStructureLoadBearingSelect" options={structureBearing} />
							</div>
							<br />
							<br />
							<h3>{reqData.note}</h3>
							<br />
							{values.floor && values.floor.length > 0 && (
								<Table className="structure-ui-table">
									<Table.Header>
										<Table.Row>
											<Table.HeaderCell>
												<>
													{reqData.ghar}{' '}
													<UnitDropdownWithRelatedFields
														unitName="floorUnit"
														options={distanceOptions}
														relatedFields={floorArray.getAllCustomsFields(null, 'floor', ['length', 'width', 'area'])}
													/>
												</>
											</Table.HeaderCell>
											<Table.HeaderCell>{reqData.lambai}</Table.HeaderCell>
											<Table.HeaderCell>{reqData.chaudai}</Table.HeaderCell>
											<Table.HeaderCell>{reqData.bargaFeet}</Table.HeaderCell>
										</Table.Row>
									</Table.Header>
									<Table.Body>
										<BlockFloorComponentsV2 floorArray={floorArray} formattedFloors={formattedFloors}>
											{(floorObj, block) => {
												return floorObj.map((floor) => (
													<Table.Row>
														<Table.Cell>{floor.label.floorBlockName}</Table.Cell>
														<Table.Cell>
															<TableInputIm name={floor.fieldName('floor', 'length')} />
														</Table.Cell>
														<Table.Cell>
															<TableInputIm name={floor.fieldName('floor', 'width')} />
														</Table.Cell>
														<Table.Cell>
															<TableInputIm name={floor.fieldName('floor', 'area')} />
														</Table.Cell>
													</Table.Row>
												));
											}}
										</BlockFloorComponentsV2>
									</Table.Body>
								</Table>
							)}
							<br />
							<br />
							<FlexSingleRight>
								<FooterSignature designations={[reqData.karyalayaKoTarfaBata]} />
							</FlexSingleRight>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}
const SuchanaTasGariMuchulkaPathaidina = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.SuchanaTaasGariMuchulka).setForm().getParams()]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
			...PrintParams.INLINE_FIELD,
			param2: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SuchanaTasGariMuchulkaPathaidinaComponent {...props} parentProps={parentProps} />}
	/>
);
export default SuchanaTasGariMuchulkaPathaidina;
