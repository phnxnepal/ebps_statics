import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { GharNakshaNamsariSambandhamaData } from '../../../../utils/data/GharNakshaNamsariSambandhamaData';
import { Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import { FlexSingleRight, FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { DashedSelect } from '../../../shared/Select';
import { CompactDashedLangInput, DashedLangInput } from '../../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { validateNullableOfficialNumbers, validateNullableNepaliDate } from '../../../../utils/validationUtils';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { PrintParams } from '../../../../utils/printUtils';
import { LocalAPI } from '../../../../utils/urlUtils';
import { WARD_MASTER } from '../../../../utils/constants';
const { object } = require('yup');
const reqData = GharNakshaNamsariSambandhamaData;
const gharJaggaNamsariOptions = [
	{
		key: reqData.paasGariLinuBhayeko,
		text: reqData.paasGariLinuBhayeko,
		value: reqData.paasGariLinuBhayeko,
	},
	{
		key: reqData.naamSariGariLinuBhayeko,
		text: reqData.naamSariGariLinuBhayeko,
		value: reqData.naamSariGariLinuBhayeko,
	},
];

const srimanSrimatiSushriOptions = [
	{
		key: reqData.shri,
		text: reqData.shri,
		value: reqData.shri,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shushri,
		text: reqData.shushri,
		value: reqData.shushri,
	},
];
const initialVal = {
	date: '',
	patraSankhya: '',
	chalaniNumber: '',
	sasthaYaExtraName: '',
	shirShrimatiSushri: srimanSrimatiSushriOptions[0].value,
	passNamsari: gharJaggaNamsariOptions[0].value,
	bidhartha: '',
	newDate: '',
	gharNo: '',
	newWardNo: '',
	buildingJoinRoad: '',
};
const validate = object({
	date: validateNullableNepaliDate,
	patraSankhya: validateNullableOfficialNumbers,
	chalaniNumber: validateNullableOfficialNumbers,
	newDate: validateNullableNepaliDate,
});

class GharNakshaNamsariSambandhamaComponent extends Component {
	constructor(props) {
		super(props);
		const { otherData, prevData, localDataObject } = this.props;
		const allPaper = getJsonData(otherData.allowancePaper);
		const wardOption = localDataObject.wardMaster.map((row) => ({ key: row.id, value: row.name, text: row.name }));
		let initialValue = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true) },
				reqFields: ['date'],
			},

			{
				obj: this.props.permitData,
				reqFields: ['newWardNo', 'buildingJoinRoad'],
			},
			{
				obj: allPaper,
				reqFields: ['gharNo'],
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValue,
			wardOption,
		};
	}
	render() {
		const { initialValue, wardOption } = this.state;
		const { formUrl, postAction, permitData, userData, prevData, hasSavePermission, hasDeletePermission, isSaveDisabled } = this.props;

		return (
			<div>
				<Formik
					initialValues={initialValue}
					validationSchema={validate}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(`${api.certificateNote}${permitData.nameTransaferId}`, values);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ values, errors, setFieldValue, isSubmitting, validateForm, handleSubmit }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
							<SectionHeader>
								<h1>{userData.organization.name}</h1>
								<h3>
									{userData.organization.name}, {userData.organization.province}, {reqData.nepal} 
								</h3>
							</SectionHeader>
								<FlexSingleRight>
									{reqData.miti}
									<CompactDashedLangDate name="date" value={values.date} error={errors.date} setFieldValue={setFieldValue} />
								</FlexSingleRight>
								<FlexSingleLeft>
									{reqData.patraSankhya}
									<CompactDashedLangInput
										name="patraSankhya"
										value={values.patraSankhya}
										error={errors.patraSankhya}
										setFieldValue={setFieldValue}
									/>
									<br />
									{reqData.chalaniNumber}
									<CompactDashedLangInput
										name="chalaniNumber"
										value={values.chalaniNumber}
										error={errors.chalaniNumber}
										setFieldValue={setFieldValue}
									/>
								</FlexSingleLeft>
								<br />
								<SectionHeader>
									<h3 className="underline">{reqData.bisaye}</h3>
								</SectionHeader>
								<br />
								<br />
								<div>
									{reqData.shri}
									<CompactDashedLangInput
										name="sasthaYaExtraName"
										value={values.sasthaYaExtraName}
										error={errors.sasthaYaExtraName}
										setFieldValue={setFieldValue}
									/>
									<br />
									{reqData.bimanapaWadaNumber}
									<DashedSelect name="newWardNo" options={wardOption} />

									{reqData.gharNumber}
									<CompactDashedLangInput name="gharNo" value={values.gharNo} error={errors.gharNo} setFieldValue={setFieldValue} />

									{reqData.sadakKoNaam}
									<DashedLangInput
										name="buildingJoinRoad"
										value={values.buildingJoinRoad}
										error={errors.buildingJoinRoad}
										setFieldValue={setFieldValue}
									/>

									{reqData.upayuktaSambandhama}
									{reqData.bimanapaWadaNumber}
									{permitData.newWardNo}

									{permitData.basne}
									<DashedSelect name="shirShrimatiSushri" options={srimanSrimatiSushriOptions} />

									{permitData.applicantName}
									{reqData.naamMaDarta}

									{permitData.oldMunicipal}
									{reqData.wadaNumber}
									{permitData.oldWardNo}

									{reqData.kittaNumber}
									{permitData.kittaNo}

									{reqData.chetrafal}
									{permitData.landArea}
									{` (${permitData.landAreaType}) `}

									{reqData.jaggamaBanekoGhar}
									<DashedSelect name="passNamsari" options={gharJaggaNamsariOptions} />

									{reqData.pramanKoAadharMaMiti}
									<CompactDashedLangDate
										name="newDate"
										value={values.newDate}
										error={errors.newDate}
										setFieldValue={setFieldValue}
									/>

									{reqData.dataLine}
									<br />
									<br />
									{reqData.bidhartha}
									<CompactDashedLangInput
										name="bidhartha"
										value={values.bidhartha}
										error={errors.bidhartha}
										setFieldValue={setFieldValue}
									/>
									<br />
									{reqData.shrirajaswoUpasakha}
								</div>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
									handleSubmit={handleSubmit}
								/>
							</div>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const GharNakshaNamsariSambandhama = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.certificateNote).setForm().getParams(), new ApiParam(api.allowancePaper, 'allowancePaper').getParams()]}
		localData={[new LocalAPI(WARD_MASTER, 'wardMaster')]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <GharNakshaNamsariSambandhamaComponent {...props} parentProps={parentProps} />}
	/>
);
export default GharNakshaNamsariSambandhama;
