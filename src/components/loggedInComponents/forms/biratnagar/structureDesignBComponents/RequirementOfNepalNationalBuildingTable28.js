import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { TableInputIm } from '../../../../shared/TableInput';
const reqData = structureDesignClassBBiratnagarRequiredData.fireSafety;

function RequirementOfNepalNationalBuildingTable28() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqData.fireSafetyNBC}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.whereDoYouPlan}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="whereDoYouPlanOfFireSafety" option={reqData.specifications} space={true} />
					<RadioInput name="whereDoYouPlanOfFireSafety" option={reqData.designCalculations} space={true} />
					<RadioInput name="whereDoYouPlanOfFireSafety" option={reqData.billOfQuantity} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="whereDoYouPlanRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable28;
