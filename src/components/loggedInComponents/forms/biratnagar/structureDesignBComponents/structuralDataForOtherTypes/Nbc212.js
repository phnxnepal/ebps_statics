import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../../shared/TableInput';
import { RadioInput } from '../../../../../shared/formComponents/RadioInput';
import { CheckboxInput } from '../../../../../shared/formComponents/CheckboxInput';
const reqData = structureDesignClassBBiratnagarRequiredData.structuralDataForOtherTypesOfStructures.nbc212;
export class Nbc212 extends Component {
	render() {
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colspan="6">
						<b>{reqData.otherTypesOfStructureNBC}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.designAssumption}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<CheckboxInput options={Object.values(reqData.options)} name="designAssumption" />
						{/* <div>
							<MyCheckBox name="designAssumption" labelName={reqData.simpleConnection} Value={reqData.simpleConnection} />
						</div>
						<div>
							<MyCheckBox name="designAssumption" labelName={reqData.websOfStandardSize} Value={reqData.websOfStandardSize} />
						</div>
						<div>
							<MyCheckBox name="designAssumption" labelName={reqData.composedSection} Value={reqData.composedSection} />
						</div> */}
					</Table.Cell>
					<Table.Cell width={1}></Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.yeildStress}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="yeildStressAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="yeildStressRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell colSpan="5">
						<b>{reqData.leastWallThickness}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.exposeCondition}</Table.Cell>
					<Table.Cell>{reqData.pipe}</Table.Cell>
					<Table.Cell>{reqData.websOfStandardSize}</Table.Cell>
					<Table.Cell>{reqData.composedSection}</Table.Cell>
					<Table.Cell width={1}></Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.forExposedSection}</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forExposedSectionpipe" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forExposedSectionwebsOfStandardSize" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forExposedSectioncomposedSection" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="forExposedSectionRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.forNotExposedSection}</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forNotExposedSectionpipe" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forNotExposedSectionwebsOfStandardSize" />
					</Table.Cell>
					<Table.Cell>
						<TableInputIm name="forNotExposedSectioncomposedSection" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="forNotExposedSectionRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.haveYouUsedTruss}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<RadioInput name="haveYouUsedTruss" label={reqData.yes} option={reqData.yes} />
						<RadioInput name="haveYouUsedTruss" label={reqData.no} option={reqData.no} />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="haveYouUsedTrussRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.whatIsTheCriticalSpan}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="whatIsTheCriticalSpanAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="whatIsTheCriticalSpanRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.purlinSize}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="purlinSizeAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="purlinSizeRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.haveYouUsedSteelPost}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<RadioInput name="haveYouUsedSteelPost" label={reqData.yes} option={reqData.yes} />
						<RadioInput name="haveYouUsedSteelPost" label={reqData.no} option={reqData.no} />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="haveYouUsedSteelPostRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.slendernessRatioOfTheCriticalPost}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="slendernessRatioOfTheCriticalPostAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="slendernessRatioOfTheCriticalPostRemarks" />
					</Table.Cell>
				</Table.Row>
			</Table.Body>
		);
	}
}

export default Nbc212;
