import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../../shared/TableInput';
import { RadioInput } from '../../../../../shared/formComponents/RadioInput';
import { DashedNormalInputIm } from '../../../../../shared/DashedFormInput';
const reqData = structureDesignClassBBiratnagarRequiredData.structuralDataForOtherTypesOfStructures.nbc213;
export class Nbc213 extends Component {
	render() {
		const { values } = this.props;
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colSpan="6">
						<b>{reqData.woodNBC}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.nameOfStructuralWood}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="nameOfStructuralWoodAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="nameOfStructuralWoodRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.modulesOfElasticity}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="modulesOfElasticityAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="modulesOfElasticityRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.criticalSpanOfBeam}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="criticalSpanOfBeamAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="criticalSpanOfBeamRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.slendernessRatioOfTheCriticalPost}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="slendernessRatioOfTheCriticalPostAsPerSubmittedDesignTable213" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="slendernessRatioOfTheCriticalPostRemarksTable213" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.jointType}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<TableInputIm name="jointTypeAsPerSubmittedDesign" />
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="jointTypeRemarks" />
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell colSpan="6">
						<b>{reqData.nbc214}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1}></Table.Cell>
					<Table.Cell width={3}>{reqData.haveYouUsedAluminium}</Table.Cell>
					<Table.Cell width={3} colSpan="3">
						<div>
							<RadioInput name="haveYouUsedAluminium" space={true} option={reqData.yes} label={reqData.yes} />
							<RadioInput name="haveYouUsedAluminium" space={true} option={reqData.no} label={reqData.no} />
						</div>
						{values.haveYouUsedAluminium === 'Yes' ? (
							<div>
								<DashedNormalInputIm name="haveYouUsedAluminiumYes" />
							</div>
						) : null}
					</Table.Cell>
					<Table.Cell width={1}>
						<TableInputIm name="haveYouUsedAluminiumRemarks" />
					</Table.Cell>
				</Table.Row>
			</Table.Body>
		);
	}
}

export default Nbc213;
