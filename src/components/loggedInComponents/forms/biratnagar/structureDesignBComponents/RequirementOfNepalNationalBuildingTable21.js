import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';

import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';
const reqDataNBC0001994 = structureDesignClassBBiratnagarRequiredData.requirementForStateOfDesign;

function RequirementOfNepalNationalBuildingTable21(props) {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<h3>
						<b>{structureDesignClassBBiratnagarRequiredData.secondHeading.requirement}</b>
					</h3>
				</Table.Cell>
			</Table.Row>
			{/* table 2.1 */}
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqDataNBC0001994.nbc0001994}</b>
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataNBC0001994.levelOfDesign}</Table.Cell>
				<Table.Cell colSpan="2">
					<CheckboxInput name="levelOfDesignAsPerSubmittedDesign" options={Object.values(reqDataNBC0001994.options)} />
					{/* <div>
						<FormikCheckboxIm name="levelOfDesignAsPerSubmittedDesign" label={reqDataNBC0001994.internationalStateOfArt} />
					</div>
					<div>
						<FormikCheckboxIm name="levelOfDesignAsPerSubmittedDesign" label={reqDataNBC0001994.professionallyEngineeredStructures} />
					</div>
					<div>
						<FormikCheckboxIm name="levelOfDesignAsPerSubmittedDesign" label={reqDataNBC0001994.mandatoryRuleOfThumb} />
					</div>
					<div>
						<FormikCheckboxIm name="levelOfDesignAsPerSubmittedDesign" label={reqDataNBC0001994.guideLinesOfRuralBuilding} />
					</div> */}
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="levelOfDesignRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable21;
