import React, { Component } from 'react';
import { Table, TextArea } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import Nbc212 from './structuralDataForOtherTypes/Nbc212';
import Nbc213 from './structuralDataForOtherTypes/Nbc213';
import { FlexSingleLeft } from '../../../../uiComponents/FlexDivs';
import { FooterSignature } from '../../formComponents/FooterSignature';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';

const reqDataGeneralTable = structureDesignClassBBiratnagarRequiredData.generalTable;

class StructuralDataForOtherTypesOfStructures extends Component {
	render() {
		const { values } = this.props;
		return (
			<div>
				<h1>{structureDesignClassBBiratnagarRequiredData.structuralDataForOtherTypesOfStructures.nbc212.heading}</h1>
				<Table columns={6} className="structure-ui-table">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={1}>{reqDataGeneralTable.sn}</Table.HeaderCell>
							<Table.HeaderCell width={3}>{reqDataGeneralTable.description}</Table.HeaderCell>
							<Table.HeaderCell width={3} colSpan="3">
								{reqDataGeneralTable.asPerSubmittedDesign}
							</Table.HeaderCell>
							<Table.HeaderCell width={1}>{reqDataGeneralTable.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					{/* table 2.12 */}
					<Nbc212 />
					{/* table 2.13 and 2.14 */}
					<Nbc213 values={values} />
				</Table>
				<FlexSingleLeft>
					<FooterSignature designations={['Signature of consultant & Seal']} />
					<label htmlFor="regdNo">NCE Regd. No.</label>
					<DashedNormalInputIm name="regdNo" />
				</FlexSingleLeft>
				<br />

				<TextArea placeholder="Comment if any in short:" />
			</div>
		);
	}
}

export default StructuralDataForOtherTypesOfStructures;
