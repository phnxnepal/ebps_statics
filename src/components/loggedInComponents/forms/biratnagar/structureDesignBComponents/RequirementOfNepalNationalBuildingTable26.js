import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
const reqData = structureDesignClassBBiratnagarRequiredData.seismicDesign;
function RequirementOfNepalNationalBuildingTable26() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqData.seismicNBC}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.methodOfEarthquakeAnalysis}</Table.Cell>
				<Table.Cell colSpan="2">
					<div>{reqData.seismicCoefficientMethos}</div>
					<div>{reqData.modelResponseSpectrum}</div>
					<div>
						<DashedNormalInputIm name="methodOfEarthquakeAnalysisAsPerSubmittedDesign" />
					</div>
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="methodOfEarthquakeAnalysisRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.fundamentaltransactionPeriod}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="fundamentaltransactionPeriodAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="fundamentaltransactionPeriodRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.basicSeismicCoefficient}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="basicSeismicCoefficientAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="basicSeismicCoefficientRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.sesmicZoningFactor}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="sesmicZoningFactorAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="sesmicZoningFactorRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.importanceFactor}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="importanceFactorAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="importanceFactorRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.structuralPerformanceFactor}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="structuralPerformanceFactorAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="structuralPerformanceFactorRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable26;
