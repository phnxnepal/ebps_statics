import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import MyCheckBox from '../../../../shared/MyCheckbox';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';
const reqDataGeneralTable = structureDesignClassBBiratnagarRequiredData.generalTable;
const reqData = structureDesignClassBBiratnagarRequiredData.structuralDataForFrameStructure;
function StructuralDataForFrameStructure211() {
	return (
		<div style={{ pageBreakInside: 'avoid' }}>
			<h2>
				<b>{reqData.structuralData}</b>
			</h2>
			<Table className="structure-ui-table" columns={7}>
				<Table.Header>
					<Table.Row>
						<Table.HeaderCell width={1}>{reqDataGeneralTable.sn}</Table.HeaderCell>
						<Table.HeaderCell width={3}>{reqDataGeneralTable.description}</Table.HeaderCell>
						<Table.HeaderCell width={3} colSpan="4">
							{reqDataGeneralTable.asPerSubmittedDesign}
						</Table.HeaderCell>
						<Table.HeaderCell width={1}>{reqDataGeneralTable.remarks}</Table.HeaderCell>
					</Table.Row>
				</Table.Header>
				<Table.Body>
					<Table.Row>
						<Table.Cell colSpan="7">
							<b>{reqData.structuralDataFrameStructureNBC}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.concreteGrade}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="concreteGradeAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="concreteGradeRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.reinforcementSteelGrade}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="reinforcementSteelGradeAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="reinforcementSteelGradeRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.criticalSizeOfSlabPanel}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="criticalSizeOfSlabPanelAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="criticalSizeOfSlabPanelRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell colSpan="6">
							<b>{reqData.slabThickness}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.calculatedShortSpanToEffectiveDepthRatio}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="calculatedShortSpanToEffectiveDepthRatioAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="calculatedShortSpanToEffectiveDepthRatioRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.LDforCorrespondingSlab}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="LDforCorrespondingSlabAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="LDforCorrespondingSlabRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.permissibleLDratio}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="permissibleLDratioAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="permissibleLDratioRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.effectiveDepth}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="effectiveDepthAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="effectiveDepthRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.basicValue}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="basicValueAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="basicValueRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.spanCorrectionFactor}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="spanCorrectionFactorAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="spanCorrectionFactorRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.tensionReiforcement}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="tensionReiforcementAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="tensionReiforcementRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.modificationFactor}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="modificationFactorAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="modificationFactorRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.compressionReinforcementModificationFactor}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="compressionReinforcementModificationFactorAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="compressionReinforcementModificationFactorRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1} rowSpan="2"></Table.Cell>
						<Table.Cell width={3} rowSpan="2">
							<b>{reqData.beamCharacteristics}</b>
						</Table.Cell>
						<Table.Cell width={3} colSpan="5">
							<b>{reqData.conditionsOfBeams}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell>{reqData.cantilever}</Table.Cell>
						<Table.Cell>{reqData.simplySupported}</Table.Cell>
						<Table.Cell>{reqData.oneSideContinuous}</Table.Cell>
						<Table.Cell>{reqData.bothSideContinuous}</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="compressionReinforcementModificationFactorRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.maximumSpanDepthRatio}</Table.Cell>
						<Table.Cell>
							<TableInputIm name="maximumSpanDepthRatiocantilever" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="maximumSpanDepthRatiosimplySupported" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="maximumSpanDepthRatiooneSideContinuous" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="maximumSpanDepthRatiobothSideContinuous" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="maximumSpanDepthRatioRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.spanOfCorrespondingbeam}</Table.Cell>
						<Table.Cell>
							<TableInputIm name="spanOfCorrespondingbeamcantilever" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="spanOfCorrespondingbeamsimplySupported" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="spanOfCorrespondingbeamoneSideContinuous" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="spanOfCorrespondingbeambothSideContinuous" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="spanOfCorrespondingbeamRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.depthOfCorrespondingBeam}</Table.Cell>
						<Table.Cell>
							<TableInputIm name="depthOfCorrespondingBeamcantilever" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="depthOfCorrespondingBeamsimplySupported" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="depthOfCorrespondingBeamoneSideContinuous" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="depthOfCorrespondingBeambothSideContinuous" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="depthOfCorrespondingBeamRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.widthOfCorrespondingbeam}</Table.Cell>
						<Table.Cell>
							<TableInputIm name="widthOfCorrespondingbeamcantilever" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="widthOfCorrespondingbeamsimplySupported" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="widthOfCorrespondingbeamoneSideContinuous" />
						</Table.Cell>
						<Table.Cell>
							<TableInputIm name="widthOfCorrespondingbeambothSideContinuous" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="widthOfCorrespondingbeamRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.maximumSlendernessRatioOfColumnLateral}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="maximumSlendernessRatioOfColumnLateralAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="maximumSlendernessRatioOfColumnLateralRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.designPhilosophy}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<CheckboxInput
								options={[reqData.limitStateMethod, reqData.workingStressMethod, reqData.ultimateSteessMethod]}
								name="designPhilosophy"
							/>
							{/* <div>
								<MyCheckBox name="designPhilosophy" labelName={reqData.limitStateMethod} Value={reqData.limitStateMethod} />
							</div>
							<div>
								<MyCheckBox name="designPhilosophy" labelName={reqData.workingStressMethod} Value={reqData.workingStressMethod} />
							</div>
							<div>
								<MyCheckBox name="designPhilosophy" labelName={reqData.ultimateSteessMethod} Value={reqData.ultimateSteessMethod} />
							</div> */}
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="designPhilosophyRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell colSpan="1"></Table.Cell>
						<Table.Cell colSpan="6">
							<b>{reqData.loadCombination}</b>
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.workingStressMethod}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<div>
								<TableInputIm name="workingStressMethodAsPerSubmittedDesign1" label="1." />
							</div>
							<div>
								<TableInputIm name="workingStressMethodAsPerSubmittedDesign2" label="2." />
							</div>
							<div>
								<TableInputIm name="workingStressMethodAsPerSubmittedDesign3" label="3." />
							</div>
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="workingStressMethodRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.limitStateMethod}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<div>
								<TableInputIm name="limitStateMethodAsPerSubmittedDesign1" label="1." />
							</div>
							<div>
								<TableInputIm name="limitStateMethodAsPerSubmittedDesign2" label="2." />
							</div>
							<div>
								<TableInputIm name="limitStateMethodAsPerSubmittedDesign3" label="3." />
							</div>
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="limitStateMethodRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.valueOfHorizontalSeismicBaseShear}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="valueOfHorizontalSeismicBaseShearAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="valueOfHorizontalSeismicBaseShearRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.submitDesignCalculation}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="submitDesignCalculationAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="submitDesignCalculationRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.typeOfFoundation}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<CheckboxInput
								options={[reqData.combined, reqData.combineIndependent, reqData.matPile, reqData.strip]}
								name="typeOfFoundation"
							/>
							{/* <div>
								<MyCheckBox name="typeOfFoundation" labelName={reqData.combined} Value={reqData.combined} />
							</div>
							<div>
								<MyCheckBox name="typeOfFoundation" labelName={reqData.combineIndependent} Value={reqData.combineIndependent} />
							</div>
							<div>
								<MyCheckBox name="typeOfFoundation" labelName={reqData.matPile} Value={reqData.matPile} />
							</div>
							<div>
								<MyCheckBox name="typeOfFoundation" labelName={reqData.strip} Value={reqData.strip} />
							</div> */}
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="typeOfFoundationRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.depthOfFoundation}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="depthOfFoundationAsPerSubmittedDesignTable211" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="depthOfFoundationRemarksTable211" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.sizesOfFoundation}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="sizesOfFoundationAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="sizesOfFoundationRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.sizesOfColumns}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="sizesOfColumnsAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="sizesOfColumnsRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.sizeAndNUmbersOfBArsProvidedInColumns}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="sizeAndNUmbersOfBArsProvidedInColumnsAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="sizeAndNUmbersOfBArsProvidedInColumnsRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.typeOfBuilding}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<div>
								<MyCheckBox name="typeOfBuilding" labelName={reqData.frame} Value={reqData.frame} />
							</div>
							<div>
								<MyCheckBox name="typeOfBuilding" labelName={reqData.loadBearing} Value={reqData.loadBearing} />
							</div>
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="typeOfBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.storeyOfBUilding}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="storeyOfBUildingAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="storeyOfBUildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.load}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="loadAsPerSubmittedDesign" label={reqData.DLLLEQ} />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="loadRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.areaOfFoundation}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="areaOfFoundationAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="areaOfFoundationRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.sampleCalculationOfBase}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="sampleCalculationOfBaseAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="sampleCalculationOfBaseRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.typeOfSoil}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<CheckboxInput
								name="typeOfSoil"
								options={[reqData.clay, reqData.siltyClay, reqData.fineSand, reqData.blackCotton, reqData.specifyIfAny]}
							/>
							{/* 
							<div>
								<MyCheckBox name="typeOfSoil" labelName={reqData.clay} Value={reqData.clay} />
							</div>
							<div>
								<MyCheckBox name="typeOfSoil" labelName={reqData.siltyClay} Value={reqData.siltyClay} />
							</div>
							<div>
								<MyCheckBox name="typeOfSoil" labelName={reqData.fineSand} Value={reqData.fineSand} />
							</div>
							<div>
								<MyCheckBox name="typeOfSoil" labelName={reqData.blackCotton} Value={reqData.blackCotton} />
							</div>
							<div>
								<MyCheckBox name="typeOfSoil" labelName={reqData.specifyIfAny} Value={reqData.specifyIfAny} />
							</div> */}
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="typeOfSoilRemarksTable211" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.factorOfSafetyProvided}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="factorOfSafetyProvidedAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="factorOfSafetyProvidedRemarks" />
						</Table.Cell>
					</Table.Row>

					{/* ---------- */}
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.isTheSoilLiquefiable}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<RadioInput name="isTheSoilLiquefiable" label={reqData.yes} option={reqData.yes} space={true} />

							<RadioInput name="isTheSoilLiquefiable" label={reqData.no} option={reqData.no} space={true} />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="typeOfBuildingRemarks" />
						</Table.Cell>
					</Table.Row>
					<Table.Row>
						<Table.Cell width={1}></Table.Cell>
						<Table.Cell width={3}>{reqData.isItTestedByLab}</Table.Cell>
						<Table.Cell width={3} colSpan="4">
							<TableInputIm name="isItTestedByLabAsPerSubmittedDesign" />
						</Table.Cell>
						<Table.Cell width={1}>
							<TableInputIm name="isItTestedByLabRemarks" />
						</Table.Cell>
					</Table.Row>
				</Table.Body>
				<Table.Footer>
					<Table.Cell colSpan="7">
						<b>{reqData.note}</b>
					</Table.Cell>
				</Table.Footer>
			</Table>
		</div>
	);
}

export default StructuralDataForFrameStructure211;
