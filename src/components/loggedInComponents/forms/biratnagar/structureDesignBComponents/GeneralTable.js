import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';

const reqDataGeneralTable = structureDesignClassBBiratnagarRequiredData.generalTable;

function GeneralTable(props) {
	const { values } = props;
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="2">{reqDataGeneralTable.general}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="asPerSubmittedDesignGeneral" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="remarksGeneral" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.category}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="categoryOfBuilding" option={reqDataGeneralTable.a} space={true} />
					<RadioInput name="categoryOfBuilding" option={reqDataGeneralTable.b} space={true} />
					<RadioInput name="categoryOfBuilding" option={reqDataGeneralTable.c} space={true} />
					<RadioInput name="categoryOfBuilding" option={reqDataGeneralTable.d} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="categoryOfBuildingRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.functionalUseOfBuilding}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="functionalUseOfBuildingAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="functionalUseOfBuildingRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.plinthAreaOfBuilding}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="plinthAreaOfBuildingAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="plinthAreaOfBuildingRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.numberOfStorey}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="numberOfStoreyAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="numberOfStoreyRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.totalHeightOfStructure}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="totalHeightOfStructureAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="totalHeightOfStructureRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.structuralSystem}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="structuralSystem" option={reqDataGeneralTable.frame} space={true} />
					<RadioInput name="structuralSystem" option={reqDataGeneralTable.loadBearing} space={true} />
					<RadioInput name="structuralSystem" option={reqDataGeneralTable.other} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="structuralSystemRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.typeOfSoil}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="typeOfSoilAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="typeOfSoilRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.adoptedSafeBearing}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="adoptedSafeBearingAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="adoptedSafeBearingRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataGeneralTable.nameOfSoftware}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="nameOfSoftwareAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="nameOfSoftwareRemarks" />
				</Table.Cell>
			</Table.Row>

			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>
					{reqDataGeneralTable.provisionForFutureExtension}
					{values.provisionForFutureExtension === 'Yes' ? (
						<div>
							<div>{reqDataGeneralTable.IfYes}</div>
							<div>{reqDataGeneralTable.structuralDesignConsideration}</div>
						</div>
					) : null}
				</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="provisionForFutureExtension" option={reqDataGeneralTable.yes} space={true} />
					<RadioInput name="provisionForFutureExtension" option={reqDataGeneralTable.no} space={true} />
					{values.provisionForFutureExtension === 'Yes' ? (
						<div>
							<div>
								<DashedNormalInputIm name="numberOfFloors" />
								{reqDataGeneralTable.floors}
							</div>
							<div>
								<RadioInput name="structuralDesignConsiderationAsPerSubmittedDesign" option={reqDataGeneralTable.yes} space={true} />

								<RadioInput name="structuralDesignConsiderationAsPerSubmittedDesign" option={reqDataGeneralTable.no} space={true} />
							</div>
						</div>
					) : null}
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="provisionForFutureExtensionRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default GeneralTable;
