import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
const reqData = structureDesignClassBBiratnagarRequiredData.snowLoad;
const snowArea = { perennial: reqData.perennial, occasional: reqData.occasional, noSnowfall: reqData.noSnowfall };
function RequirementOfNepalNationalBuildingTable27() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqData.snowNBC}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.snowfallArea}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="snowfallArea" option={snowArea.perennial} space={true} />
					<RadioInput name="snowfallArea" option={snowArea.occasional} space={true} />
					<RadioInput name="snowfallArea" option={snowArea.noSnowfall} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="snowfallAreaRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.elevation}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="elevationAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="elevationRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.designDepth}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="designDepthAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="designDepthRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.designDensity}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="designDensityAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="designDensityRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable27;
