import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
const reqData = structureDesignClassBBiratnagarRequiredData.constructionsafely;
function RequirementOfNepalNationalBuildingTable215() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="5">
					<b>{reqData.construcationSafelyNBC}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.allSafetyMeasuresWillBeFulfilled}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="allSafetyMeasuresWillBeFulfilledAsPerSubmittedDesign" option={reqData.yes} space={true} />
					<RadioInput name="allSafetyMeasuresWillBeFulfilledAsPerSubmittedDesign" option={reqData.no} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="allSafetyMeasuresWillBeFulfilledRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.safetyWaresUse}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="safetyWaresUseAsPerSubmittedDesign" option={reqData.safetyHardHat} space={true} />
					<RadioInput name="safetyWaresUseAsPerSubmittedDesign" option={reqData.safetyGoogles} space={true} />
					<RadioInput name="safetyWaresUseAsPerSubmittedDesign" option={reqData.safetyBoots} space={true} />
					<RadioInput name="safetyWaresUseAsPerSubmittedDesign" option={reqData.safetyBelt} space={true} />
					<RadioInput name="safetyWaresUseAsPerSubmittedDesign" option={reqData.firstAidFacility} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="safetyWaresUse" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable215;
