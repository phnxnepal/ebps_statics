import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';
const reqDataGeneralTable = structureDesignClassBBiratnagarRequiredData.generalTable;
const reqData = structureDesignClassBBiratnagarRequiredData.structuralDataForLoadBearing;
class StructuralDataForLoadBearingWallStructure210 extends Component {
	render() {
		return (
			<div style={{ pageBreakInside: 'avoid' }}>
				<h1>{reqData.heading}</h1>
				<Table columns={6} className="structure-ui-table">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={1}>{reqDataGeneralTable.sn}</Table.HeaderCell>
							<Table.HeaderCell width={3}>{reqDataGeneralTable.description}</Table.HeaderCell>
							<Table.HeaderCell width={3} colSpan="3">
								{reqDataGeneralTable.asPerSubmittedDesign}
							</Table.HeaderCell>
							<Table.HeaderCell width={1}>{reqDataGeneralTable.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell colSpan="6">
								<b>{reqData.loadBearingNBC}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.depthOfFoundation}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="depthOfFoundationAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="depthOfFoundationRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.widthOfFoundation}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="widthOfFoundationAsPerOfSubmittedDesignTable210" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="widthOfFoundationRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.concreteGrade}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="concreteGradeAsPerOfSubmittedDesignTable210" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="concreteGradeRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.brickCrushingStrength}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="brickCrushingStrengthAsPerOfSubmittedDesignTable210" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="brickCrushingStrengthRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.mortarRatioForLoadBearingMasonry}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="mortarRatioForLoadBearingMasonryAsPerOfSubmittedDesignTable210" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="mortarRatioForLoadBearingMasonryRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>
								<b>{reqData.floor}</b>
							</Table.Cell>
							<Table.Cell>
								<b>{reqData.wallHeight}</b>
							</Table.Cell>
							<Table.Cell>
								<b>{reqData.wallThickness}</b>
							</Table.Cell>
							<Table.Cell>
								<b>{reqData.maximumLength}</b>
							</Table.Cell>
							<Table.Cell width={1}></Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.groundFloor}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="groundFloorWallHeight" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="groundFloorWallThickness" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="groundFloorMaximumLength" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="groundFloorRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.firstFloor}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="firstFloorWallHeight" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="firstFloorWallThickness" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="firstFloorMaximumLength" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="firstFloorRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.secondFloor}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="secondFloorWallHeight" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="secondFloorWallThickness" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="secondFloorMaximumLength" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="secondFloorRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="5">
								<b>{reqData.openingDetails}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.leastDistanceBetweenAnyTwoOpening}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<div>
									<RadioInput name="leastDistanceBetweenAnyTwoOpening" label={reqData.yes} option={reqData.yes} space={true} />
								</div>
								<div>
									<RadioInput name="leastDistanceBetweenAnyTwoOpening" label={reqData.no} option={reqData.no} space={true} />
								</div>
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="leastDistanceBetweenAnyTwoOpeningRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.horizontalDistanceBetweenAnyTwoOpening}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<div>
									<RadioInput name="horizontalDistanceBetweenAnyTwoOpening" label={reqData.yes} option={reqData.yes} space={true} />
								</div>
								<div>
									<RadioInput name="horizontalDistanceBetweenAnyTwoOpening" label={reqData.no} option={reqData.no} space={true} />
								</div>
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="horizontalDistanceBetweenAnyTwoOpeningRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.VerticalDistanceBetweenAnyTwoopening}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<div>
									<RadioInput name="VerticalDistanceBetweenAnyTwoopening" label={reqData.yes} option={reqData.yes} space={true} />
								</div>
								<div>
									<RadioInput name="VerticalDistanceBetweenAnyTwoopening" label={reqData.no} option={reqData.no} space={true} />
								</div>
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="VerticalDistanceBetweenAnyTwoopeningRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.anyOfTheAboveMentionedCases}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<div>
									<RadioInput name="anyOfTheAboveMentionedCases" label={reqData.yes} option={reqData.yes} space={true} />
								</div>
								<div>
									<RadioInput name="anyOfTheAboveMentionedCases" label={reqData.no} option={reqData.no} space={true} />
								</div>
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="anyOfTheAboveMentionedCasesRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.bandsProvided}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<CheckboxInput
									name="bandsProvidedAsPerSubmittedDesign"
									options={[reqData.plinthLevel, reqData.lintelLevel, reqData.roofLevel, reqData.gableBand]}
								/>
{/* 
								<div>
									<MyCheckBox
										name="bandsProvidedAsPerSubmittedDesign"
										labelName={reqData.lintelLevel}
										Value={reqData.lintelLevel}
									/>
								</div>
								<div>
									<MyCheckBox name="bandsProvidedAsPerSubmittedDesign" labelName={reqData.roofLevel} Value={reqData.roofLevel} />
								</div>
								<div>
									<MyCheckBox name="bandsProvidedAsPerSubmittedDesign" labelName={reqData.gableBand} Value={reqData.gableBand} />
								</div> */}
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="bandsProvidedRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="5">
								<b>{reqData.verticalSteelReinforcement}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.groundFloor}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="groundFloorAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="groundFloorRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.firstFloor}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="firstFloorAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="firstFloorRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.secondFloor}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="secondFloorAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="secondFloorRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>
								<DashedNormalInputIm name="steelreinforcementDiameterAtExtraField" />
							</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="steelreinforcementDiameterAtExtraFieldAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="steelreinforcementDiameterAtExtraFieldRemarksTable210" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.distanceOfCornerTee}</Table.Cell>
							<Table.Cell width={3} colSpan="3">
								<TableInputIm name="distanceOfCornerTeeAsPerOfSubmittedDesign" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="distanceOfCornerTeeRemarksTable210" />
							</Table.Cell>
						</Table.Row>
					</Table.Body>
					<Table.Footer>
						<Table.Row>
							<Table.Cell colSpan="6">
								<div>
									<h3>
										<b>{reqData.note}</b>
									</h3>
								</div>
								<div>
									<b>{reqData.noteContent}</b>
								</div>
							</Table.Cell>
						</Table.Row>
					</Table.Footer>
				</Table>
			</div>
		);
	}
}

export default StructuralDataForLoadBearingWallStructure210;
