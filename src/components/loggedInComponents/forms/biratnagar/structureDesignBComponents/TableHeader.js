import React from 'react';
import { Table } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
const reqDataGeneralTable = structureDesignClassBBiratnagarRequiredData.generalTable;
function TableHeaderForForm() {
	return (
		<Table.Header>
			<Table.Row>
				<Table.HeaderCell width={1}>{reqDataGeneralTable.sn}</Table.HeaderCell>
				<Table.HeaderCell>{reqDataGeneralTable.description}</Table.HeaderCell>
				<Table.HeaderCell colSpan="2">{reqDataGeneralTable.asPerSubmittedDesign}</Table.HeaderCell>
				<Table.HeaderCell width={2}>{reqDataGeneralTable.remarks}</Table.HeaderCell>
			</Table.Row>
		</Table.Header>
	);
}

export default TableHeaderForForm;
