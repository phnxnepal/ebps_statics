import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
const reqDataNBC1021994 = structureDesignClassBBiratnagarRequiredData.unitWeightOfMaterials;
function RequirementOfNepalNationalBuildingTable23() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{structureDesignClassBBiratnagarRequiredData.thirdHeading.unitWeight}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataNBC1021994.whereDoYouPlanToApplyNBC102}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="whereDoYouPlanToApplyNBC102" option={reqDataNBC1021994.specifications} space={true} />
					<RadioInput name="whereDoYouPlanToApplyNBC102" option={reqDataNBC1021994.designCalculations} space={true} />
					<RadioInput name="whereDoYouPlanToApplyNBC102" option={reqDataNBC1021994.billOfQuantity} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="whereDoYouPlanToApplyNBC102Remarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqDataNBC1021994.specifyTheDesignUnitsOfMaterials}</b>
				</Table.Cell>
			</Table.Row>
			{/* ------ */}
			<Table.Row>
				<Table.Cell width={1} rowSpan="4"></Table.Cell>
				<Table.Cell>{reqDataNBC1021994.steel}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="steelWeight" label="weight" />
				</Table.Cell>
				<Table.Cell width={2} rowSpan="4">
					<TableInputIm name="weightOfMaterialsRemarks" />
				</Table.Cell>
			</Table.Row>
			{/* ------ */}
			<Table.Row>
				<Table.Cell>{reqDataNBC1021994.brick}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="brickWeight" label="weight" />
				</Table.Cell>
			</Table.Row>
			{/* ------ */}
			<Table.Row>
				<Table.Cell>{reqDataNBC1021994.rcc}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="rccWeight" label="weight" />
				</Table.Cell>
			</Table.Row>
			{/* ------ */}
			<Table.Row>
				<Table.Cell>{reqDataNBC1021994.brickMasonry}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="brickMasonryWeight" label="weight" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell colSpan="6">{reqDataNBC1021994.note}</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable23;
