import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { CheckboxInput } from '../../../../shared/formComponents/CheckboxInput';
const reqDataNBC1011994 = structureDesignClassBBiratnagarRequiredData.materialsSpecification;
function RequirementOfNepalNationalBuildingTable22() {
	return (
		<Table.Body>
			<Table.Row></Table.Row>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqDataNBC1011994.nbc1011994}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataNBC1011994.tickTheListMaterials}</Table.Cell>
				<Table.Cell colSpan="2">
					<CheckboxInput options={Object.values(reqDataNBC1011994.options)} name="tickTheListMaterials"/>
					{/* <div>
						<MyCheckBox name="tickTheListMaterials" labelName={reqDataNBC1011994.cement} Value={reqDataNBC1011994.cement} />
					</div>
					<div>
						<MyCheckBox
							name="tickTheListMaterials"
							labelName={reqDataNBC1011994.fineAggregates}
							Value={reqDataNBC1011994.fineAggregates}
						/>
					</div>
					<div>
						<MyCheckBox
							name="tickTheListMaterials"
							labelName={reqDataNBC1011994.naturalBuildingStones}
							Value={reqDataNBC1011994.naturalBuildingStones}
						/>
					</div>
					<div>
						<MyCheckBox name="tickTheListMaterials" labelName={reqDataNBC1011994.tiles} Value={reqDataNBC1011994.tiles} />
					</div>
					<div>
						<MyCheckBox name="tickTheListMaterials" labelName={reqDataNBC1011994.metalFrames} Value={reqDataNBC1011994.metalFrames} />
					</div>
					<div>
						<MyCheckBox
							name="tickTheListMaterials"
							labelName={reqDataNBC1011994.coarseAggregates}
							Value={reqDataNBC1011994.coarseAggregates}
						/>
					</div>
					<div>
						<MyCheckBox name="tickTheListMaterials" labelName={reqDataNBC1011994.bricks} Value={reqDataNBC1011994.bricks} />
					</div>
					<div>
						<MyCheckBox name="tickTheListMaterials" labelName={reqDataNBC1011994.timber} Value={reqDataNBC1011994.timber} />
					</div>
					<div>
						<MyCheckBox
							name="tickTheListMaterials"
							labelName={reqDataNBC1011994.structuralSteel}
							Value={reqDataNBC1011994.structuralSteel}
						/>
					</div> */}
					<div>
						<TableInputIm name="tickTheListMaterialsExtraField" />
					</div>
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="tickTheListMaterialsRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqDataNBC1011994.inWhatManner}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="inWhatMannerAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="inWhatMannerRemarks" />
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable22;
