import React from 'react';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { RadioInput } from '../../../../shared/formComponents/RadioInput';
const reqData = structureDesignClassBBiratnagarRequiredData.seismicHazards;
function RequirementOfNepalNationalBuildingTable29() {
	return (
		<Table.Body>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqData.seismicHazardsNBC}</b>
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.distanceFromToe}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="distanceFromToeAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="distanceFromToeRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.distanceFromRiverBank}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="distanceFromRiverBankAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="distanceFromRiverBankRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.soilTypeInFooting}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="soilTypeInFootingAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="soilTypeInFootingRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.adoptedSafeBearingCapacity}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="adoptedSafeBearingCapacityAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="adoptedSafeBearingCapacityRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.depthOfFoundation}</Table.Cell>
				<Table.Cell colSpan="2">
					<TableInputIm name="depthOfFoundationAsPerSubmittedDesign" />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="depthOfFoundationRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell width={1}></Table.Cell>
				<Table.Cell>{reqData.soilTestReport}</Table.Cell>
				<Table.Cell colSpan="2">
					<RadioInput name="soilTestReportAsPerSubmittedDesign" option={reqData.yes} space={true} />
					<RadioInput name="soilTestReportAsPerSubmittedDesign" option={reqData.no} space={true} />
				</Table.Cell>
				<Table.Cell width={2}>
					<TableInputIm name="soilTestReportRemarks" />
				</Table.Cell>
			</Table.Row>
			<Table.Row>
				<Table.Cell colSpan="6">
					<b>{reqData.note}</b>
				</Table.Cell>
			</Table.Row>
		</Table.Body>
	);
}

export default RequirementOfNepalNationalBuildingTable29;
