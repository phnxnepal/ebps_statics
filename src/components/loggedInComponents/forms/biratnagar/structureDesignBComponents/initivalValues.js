export const initialValue = {
	//table 1
	//general table
	asPerSubmittedDesignGeneral: '',
	remarksGeneral: '',

	categoryOfBuilding: '', //radio button ho yo
	categoryOfBuildingRemarks: '',
	functionalUseOfBuildingAsPerSubmittedDesign: '',
	functionalUseOfBuildingRemarks: '',
	plinthAreaOfBuildingAsPerSubmittedDesign: '',
	numberOfStoreyAsPerSubmittedDesign: '',
	numberOfStoreyRemarks: '',
	totalHeightOfStructureAsPerSubmittedDesign: '',
	totalHeightOfStructureRemarks: '',

	structuralSystem: 'Frame', // radio button
	structuralSystemRemarks: '',

	typeOfSoilAsPerSubmittedDesign: '',
	typeOfSoilRemarks: '',

	adoptedSafeBearingAsPerSubmittedDesign: '',
	adoptedSafeBearingRemarks: '',
	nameOfSoftwareAsPerSubmittedDesign: '',
	nameOfSoftwareRemarks: '',

	provisionForFutureExtension: '', //radio button
	provisionForFutureExtensionRemarks: '',
	numberOfFloors: '',

	structuralDesignConsiderationAsPerSubmittedDesign: '', //checkbox @todo radio banaunu parne cha yesli

	//2.1
	levelOfDesignAsPerSubmittedDesign: '', //checkBox
	levelOfDesignRemarks: '',

	//2.2
	tickTheListMaterials: '', //checkbox ho yo
	tickTheListMaterialsExtraField: '',
	tickTheListMaterialsRemarks: '',

	inWhatMannerAsPerSubmittedDesign: '',
	inWhatMannerRemarks: '',

	whereDoYouPlanToApplyNBC102: '', //checkbox
	whereDoYouPlanToApplyNBC102Remarks: '',
	//2.3
	weightOfMaterialsRemarks: '',
	brickWeight: '',
	rccWeight: '',
	steelWeight: '',
	brickMasonryWeight: '',
	//2.4
	//UDL means uniformly distributed load
	roomsAndKitchenUDL: '',
	roomsAndKitchenConcentrated: '',
	roomsAndKitchenRemarks: '',

	corridorsStaircaseStoreUDL: '',
	corridorsStaircaseStoreConcentrated: '',
	corridorsStaircaseStoreRemarks: '',

	balconyUDL: '',
	balconyConcentrated: '',
	balconyRemarks: '',

	livingBedAndDormitoriesUDL: '',
	livingBedAndDormitoriesConcentrated: '',
	livingBedAndDormitoriesRemarks: '',
	kitchenCorridorsStaricaseUDL: '',
	kitchenCorridorsStaricaseConcentrated: '',
	kitchenCorridorsStaricaseRemarks: '',
	storeRoomsUDL: '',
	storeRoomsConcentrated: '',
	storeRoomsRemarks: '',
	diningResturantsUDL: '',
	diningResturantsConcentrated: '',
	diningResturantsRemarks: '',
	officeRoomsUDL: '',
	officeRoomsConcentrated: '',
	officeRoomsRemarks: '',

	classRoomsUDL: '',
	classRoomsConcentrated: '',
	classRoomsRemarks: '',
	kitchenUDL: '',
	kitchenConcentrated: '',
	kitchenRemarks: '',
	storesUDL: '',
	storesConcentrated: '',
	storesRemarks: '',
	librariesAndArchivesUDL: '',
	librariesAndArchivesConcentrated: '',
	librariesAndArchivesRemarks: '',
	blaconiesUDL: '',
	blaconiesConcentrated: '',
	blaconiesRemarks: '',
	bedRoomsWardsDressingRoomsUDL: '',
	bedRoomsWardsDressingRoomsConcentrated: '',
	bedRoomsWardsDressingRoomsRemarks: '',
	kitchensUDL: '',
	kitchensConcentrated: '',
	kitchensRemarks: '',
	xrayOperatingRoomsUDL: '',
	xrayOperatingRoomsConcentrated: '',
	xrayOperatingRoomsRemarks: '',
	corridorsAndStaircaseUDL: '',
	corridorsAndStaircaseConcentrated: '',
	corridorsAndStaircaseRemarks: '',
	IBblaconiesUDL: '',
	IBblaconiesConcentrated: '',
	IBblaconiesRemarks: '',

	assemblyAreasUDL: '',
	assemblyAreasConcentrated: '',
	assemblyAreasRemarks: '',
	projectionsRoomsUDL: '',
	projectionsRoomsConcentrated: '',
	projectionsRoomsRemarks: '',
	stagesUDL: '',
	stagesConcentrated: '',
	stagesRemarks: '',
	corridorsPassageStaricaseUDL: '',
	corridorsPassageStaricaseConcentrated: '',
	corridorsPassageStaricaseRemarks: '',
	ABblaconiesUDL: '',
	ABblaconiesConcentrated: '',
	ABblaconiesRemarks: '',

	roomsWithSeperateStorageUDL: '',
	roomsWithSeperateStorageConcentrated: '',
	roomsWithSeperateStorageRemarks: '',
	roomsWithoutSeperateStorageUDL: '',
	roomsWithoutSeperateStorageConcentrated: '',
	roomsWithoutSeperateStorageRemarks: '',
	filesRoomsAndStorageRoomsUDL: '',
	filesRoomsAndStorageRoomsConcentrated: '',
	filesRoomsAndStorageRoomsRemarks: '',
	stairAndPassageUDL: '',
	stairAndPassageConcentrated: '',
	stairAndPassageRemarks: '',
	BAOBblaconiesUDL: '',
	BAOBblaconiesConcentrated: '',
	BAOBblaconiesRemarks: '',

	MBstaricaseAndPassageConcentrated: '',
	MBstaricaseAndPassageUDL: '',
	officeRemarks: '',
	officeConcentrated: '',
	officeUDL: '',
	wholesaleShopsRemarks: '',
	wholesaleShopsConcentrated: '',
	wholesaleShopsUDL: '',
	retailShopsRemarks: '',
	retailShopsConcentrated: '',

	retailShopsUDL: '',

	MBstaricaseAndPassageRemarks: '',
	MBblaconiesUDL: '',
	MBblaconiesConcentrated: '',
	MBblaconiesRemarks: '',

	workAreaWithoutMachineryUDL: '',
	workAreaWithoutMachineryConcentrated: '',
	workAreaWithoutMachineryRemarks: '',
	withMachineryLightDutyUDL: '',
	withMachineryLightDutyConcentrated: '',
	withMachineryLightDutyRemarks: '',
	meduimDutyUDL: '',
	meduimDutyConcentrated: '',
	meduimDutyRemarks: '',
	heavyDutyUDL: '',
	heavyDutyConcentrated: '',
	heavyDutyRemarks: '',
	boilerUDL: '',
	boilerConcentrated: '',
	boilerRemarks: '',
	staircasePassageUDL: '',
	staircasePassageConcentrated: '',
	staircasePassageRemarks: '',

	storageRoomsUDL: '',
	storageRoomsConcentrated: '',
	storageRoomsRemarks: '',
	coldStoragUDL: '',
	coldStoragConcentrated: '',
	coldStoragRemarks: '',
	corridorAndPassageUDL: '',
	corridorAndPassageConcentrated: '',
	corridorAndPassageRemarks: '',
	boilerRoomsUDL: '',
	boilerRoomsConcentrated: '',
	boilerRoomsRemarks: '',

	//2.5
	windZoneAsPerSubmittedDesign: '',
	windZoneRemarks: '',
	basicWindVelocityAsPerSubmittedDesign: '',
	basicWindVelocityRemarks: '',

	//2.6
	methodOfEarthquakeAnalysisAsPerSubmittedDesign: '',
	methodOfEarthquakeAnalysisRemarks: '',
	fundamentaltransactionPeriodAsPerSubmittedDesign: '',
	fundamentaltransactionPeriodRemarks: '',
	basicSeismicCoefficientAsPerSubmittedDesign: '',
	basicSeismicCoefficientRemarks: '',
	sesmicZoningFactorAsPerSubmittedDesign: '',
	sesmicZoningFactorRemarks: '',
	importanceFactorAsPerSubmittedDesign: '',
	importanceFactorRemarks: '',
	structuralPerformanceFactorRemarks: '',
	structuralPerformanceFactorAsPerSubmittedDesign: '',

	//2.7
	snowfallAreaAsPerSubmittedDesign: '',
	snowfallAreaRemarks: '',
	elevationAsPerSubmittedDesign: '',
	elevationRemarks: '',
	designDepthAsPerSubmittedDesign: '',
	designDepthRemarks: '',
	designDensityAsPerSubmittedDesign: '',
	designDensityRemarks: '',
	snowfallArea: '',

	//2.8
	whereDoYouPlanRemarks: '',
	whereDoYouPlanOfFireSafety: '',

	//2.9
	distanceFromToeAsPerSubmittedDesign: '',
	distanceFromToeRemarks: '',
	distanceFromRiverBankAsPerSubmittedDesign: '',
	distanceFromRiverBankRemarks: '',
	soilTypeInFootingAsPerSubmittedDesign: '',
	soilTypeInFootingRemarks: '',
	adoptedSafeBearingCapacityAsPerSubmittedDesign: '',
	adoptedSafeBearingCapacityRemarks: '',
	depthOfFoundationAsPerSubmittedDesign: '',
	depthOfFoundationRemarks: '',
	soilTestReportAsPerSubmittedDesign: '',
	soilTestReportRemarks: '',
	//2.15

	allSafetyMeasuresWillBeFulfilledAsPerSubmittedDesign: '',
	allSafetyMeasuresWillBeFulfilledRemarks: '',
	safetyWaresUseAsPerSubmittedDesign: '',
	safetyWaresUse: '',

	//2.11
	concreteGradeAsPerSubmittedDesign: '',
	concreteGradeRemarks: '',
	reinforcementSteelGradeAsPerSubmittedDesign: '',
	reinforcementSteelGradeRemarks: '',
	criticalSizeOfSlabPanelAsPerSubmittedDesign: '',
	criticalSizeOfSlabPanelRemarks: '',
	calculatedShortSpanToEffectiveDepthRatioAsPerSubmittedDesign: '',
	calculatedShortSpanToEffectiveDepthRatioRemarks: '',
	LDforCorrespondingSlabAsPerSubmittedDesign: '',
	LDforCorrespondingSlabRemarks: '',
	permissibleLDratioAsPerSubmittedDesign: '',
	permissibleLDratioRemarks: '',
	basicValueAsPerSubmittedDesign: '',
	basicValueRemarks: '',
	spanCorrectionFactorAsPerSubmittedDesign: '',
	spanCorrectionFactorRemarks: '',
	tensionReiforcementAsPerSubmittedDesign: '',
	tensionReiforcementRemarks: '',
	modificationFactorAsPerSubmittedDesign: '',
	modificationFactorRemarks: '',
	compressionReinforcementModificationFactorAsPerSubmittedDesign: '',
	compressionReinforcementModificationFactorRemarks: '',

	maximumSpanDepthRatiocantilever: '',
	maximumSpanDepthRatiosimplySupported: '',
	maximumSpanDepthRatiooneSideContinuous: '',
	maximumSpanDepthRatiobothSideContinuous: '',

	spanOfCorrespondingbeamcantilever: '',
	spanOfCorrespondingbeambothSideContinuous: '',
	spanOfCorrespondingbeamsimplySupported: '',
	spanOfCorrespondingbeamoneSideContinuous: '',
	spanOfCorrespondingbeamRemarks: '',

	depthOfCorrespondingBeamcantilever: '',
	depthOfCorrespondingBeamsimplySupported: '',
	depthOfCorrespondingBeamoneSideContinuous: '',
	depthOfCorrespondingBeambothSideContinuous: '',
	depthOfCorrespondingBeamRemarks: '',

	widthOfCorrespondingbeamcantilever: '',
	widthOfCorrespondingbeamoneSideContinuous: '',
	widthOfCorrespondingbeamsimplySupported: '',
	widthOfCorrespondingbeambothSideContinuous: '',
	widthOfCorrespondingbeamRemarks: '',

	maximumSlendernessRatioOfColumnLateralAsPerSubmittedDesign: '',
	maximumSlendernessRatioOfColumnLateralRemarks: '',

	designPhilosophy: '',
	designPhilosophyRemarks: '',
	workingStressMethodAsPerSubmittedDesign1: '',
	workingStressMethodAsPerSubmittedDesign2: '',
	workingStressMethodAsPerSubmittedDesign3: '',
	workingStressMethodRemarks: '',

	limitStateMethodAsPerSubmittedDesign1: '',
	limitStateMethodAsPerSubmittedDesign2: '',
	limitStateMethodAsPerSubmittedDesign3: '',
	limitStateMethodRemarks: '',

	valueOfHorizontalSeismicBaseShearAsPerSubmittedDesign: '',
	valueOfHorizontalSeismicBaseShearRemarks: '',

	submitDesignCalculationAsPerSubmittedDesign: '',
	submitDesignCalculationRemarks: '',

	typeOfFoundation: '',
	typeOfFoundationRemarks: '',

	depthOfFoundationAsPerSubmittedDesignTable211: '',
	depthOfFoundationRemarksTable211: '',
	sizesOfFoundationAsPerSubmittedDesign: '',
	sizesOfFoundationRemarks: '',
	sizesOfColumnsAsPerSubmittedDesign: '',
	sizesOfColumnsRemarks: '',
	sizeAndNUmbersOfBArsProvidedInColumnsAsPerSubmittedDesign: '',
	sizeAndNUmbersOfBArsProvidedInColumnsRemarks: '',
	storeyOfBUildingAsPerSubmittedDesign: '',
	storeyOfBUildingRemarks: '',
	typeOfBuilding: '',
	typeOfBuildingRemarks: '',

	loadAsPerSubmittedDesign: '',
	loadRemarks: '',
	areaOfFoundationAsPerSubmittedDesign: '',
	areaOfFoundationRemarks: '',
	sampleCalculationOfBaseAsPerSubmittedDesign: '',
	sampleCalculationOfBaseRemarks: '',
	typeOfSoil: '',
	typeOfSoilRemarksTable211: '',
	factorOfSafetyProvidedAsPerSubmittedDesign: '',
	factorOfSafetyProvidedRemarks: '',
	isItTestedByLabAsPerSubmittedDesign: '',
	isItTestedByLabRemarks: '',

	//2.10
	depthOfFoundationAsPerOfSubmittedDesign: '',
	depthOfFoundationRemarksTable210: '',

	widthOfFoundationAsPerOfSubmittedDesignTable210: '',
	widthOfFoundationRemarksTable210: '',

	concreteGradeAsPerOfSubmittedDesignTable210: '',
	concreteGradeRemarksTable210: '',

	brickCrushingStrengthAsPerOfSubmittedDesignTable210: '',
	brickCrushingStrengthRemarksTable210: '',

	mortarRatioForLoadBearingMasonryAsPerOfSubmittedDesignTable210: '',
	mortarRatioForLoadBearingMasonryRemarksTable210: '',

	groundFloorWallHeight: '',
	groundFloorRemarks: '',
	groundFloorWallThickness: '',
	groundFloorMaximumLength: '',

	firstFloorWallHeight: '',
	firstFloorWallThickness: '',
	firstFloorMaximumLength: '',
	firstFloorRemarks: '',

	secondFloorWallHeight: '',
	secondFloorWallThickness: '',
	secondFloorMaximumLength: '',
	secondFloorRemarks: '',

	leastDistanceBetweenAnyTwoOpening: '',
	leastDistanceBetweenAnyTwoOpeningRemarks: '',

	horizontalDistanceBetweenAnyTwoOpening: '',
	horizontalDistanceBetweenAnyTwoOpeningRemarks: '',

	VerticalDistanceBetweenAnyTwoopening: '',
	VerticalDistanceBetweenAnyTwoopeningRemarks: '',

	anyOfTheAboveMentionedCases: '',
	anyOfTheAboveMentionedCasesRemarks: '',

	bandsProvidedAsPerSubmittedDesign: '',
	bandsProvidedRemarks: '',

	steelreinforcementDiameterAtExtraField: '',
	steelreinforcementDiameterAtExtraFieldAsPerOfSubmittedDesign: '',
	steelreinforcementDiameterAtExtraFieldRemarksTable210: '',

	secondFloorRemarksTable210: '',
	secondFloorAsPerOfSubmittedDesign: '',

	firstFloorRemarksTable210: '',
	firstFloorAsPerOfSubmittedDesign: '',

	groundFloorRemarksTable210: '',
	groundFloorAsPerOfSubmittedDesign: '',

	distanceOfCornerTeeAsPerOfSubmittedDesign: '',
	distanceOfCornerTeeRemarksTable210: '',

	//2.12
	designAssumption: '',
	yeildStressAsPerSubmittedDesign: '',
	yeildStressRemarks: '',

	forExposedSectionpipe: '',
	forExposedSectionwebsOfStandardSize: '',
	forExposedSectioncomposedSection: '',
	forExposedSectionRemarks: '',

	forNotExposedSectionpipe: '',
	forNotExposedSectionwebsOfStandardSize: '',
	forNotExposedSectioncomposedSection: '',
	forNotExposedSectionRemarks: '',

	haveYouUsedTruss: '',
	haveYouUsedTrussRemarks: '',

	whatIsTheCriticalSpanAsPerSubmittedDesign: '',
	whatIsTheCriticalSpanRemarks: '',

	purlinSizeAsPerSubmittedDesign: '',
	purlinSizeRemarks: '',

	haveYouUsedSteelPost: '',
	haveYouUsedSteelPostRemarks: '',
	slendernessRatioOfTheCriticalPostAsPerSubmittedDesign: '',
	slendernessRatioOfTheCriticalPostRemarks: '',

	nameOfStructuralWoodAsPerSubmittedDesign: '',
	nameOfStructuralWoodRemarks: '',

	modulesOfElasticityAsPerSubmittedDesign: '',
	modulesOfElasticityRemarks: '',
	criticalSpanOfBeamAsPerSubmittedDesign: '',
	criticalSpanOfBeamRemarks: '',
	slendernessRatioOfTheCriticalPostAsPerSubmittedDesignTable213: '',
	slendernessRatioOfTheCriticalPostRemarksTable213: '',
	jointTypeAsPerSubmittedDesign: '',
	jointTypeRemarks: '',

	haveYouUsedAluminiumYes: '',
	haveYouUsedAluminiumRemarks: '',
};
