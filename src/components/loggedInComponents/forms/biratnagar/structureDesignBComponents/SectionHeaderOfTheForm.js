import React from 'react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { SectionHeader } from '../../../../uiComponents/Headers';
const reqDataHeader = structureDesignClassBBiratnagarRequiredData.sectionHeader;
function SectionHeaderOfTheForm() {
	return (
		<SectionHeader>
			<h4>{reqDataHeader.prabidhikBibarn}</h4>
			<h3>{reqDataHeader.structuralDesignSambandhi}</h3>
			<h2>{reqDataHeader.checkList}</h2>
			<h2>{reqDataHeader.professionallyEngineered}</h2>
			<h3>{reqDataHeader.plinthAreaHeader}</h3>
			<h3>{reqDataHeader.nbc000}</h3>
			<h3>{reqDataHeader.inCaseOfManyUnits}</h3>
		</SectionHeader>
	);
}

export default SectionHeaderOfTheForm;
