import React, { Component } from 'react';
import { Table, Dropdown } from 'semantic-ui-react';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../utils/data/StructureDesignClassBBiratnagarData';
import { TableInputIm } from '../../../../shared/TableInput';

const reqDataNBC1031994 = structureDesignClassBBiratnagarRequiredData.occupancyLoad;

const occupancyType = [
	{
		key: 'forResidentialBuilding',
		text: 'For Residential Building',
		value: 'forResidentialBuilding',
	},
	{
		key: 'forHotelsHostelsDormitories',
		text: 'For Hotels, Hostels, Dormitories',
		value: 'forHotelsHostelsDormitories',
	},
	{
		key: 'forEducaitonalBuildings',
		text: 'For Educational Buildings',
		value: 'forEducaitonalBuildings',
	},
	{
		key: 'forInstitutionalBuildings',
		text: 'For Institutional Buildings',
		value: 'forInstitutionalBuildings',
	},
	{
		key: 'forAssemblyBuildings',
		text: 'For Assembly Buildings',
		value: 'forAssemblyBuildings',
	},
	{
		key: 'forBusinessAndOfficeBuildings',
		text: 'For Business And Office Buildings',
		value: 'forBusinessAndOfficeBuildings',
	},
	{
		key: 'mercantileBuilding',
		text: 'Mercantile Building',
		value: 'mercantileBuilding',
	},
	{
		key: 'industrialBuilding',
		text: 'Industrial Building',
		value: 'industrialBuilding',
	},
	{
		key: 'storagelBuilding',
		text: 'Storage Building',
		value: 'storageBuilding',
	},
];

class RequirementOfNepalNationalBuildingTable24 extends Component {
	constructor(props) {
		super(props);
		this.dropDown = this.dropDown.bind(this);
	}

	dropDown(a) {
		switch (a) {
			case 'forResidentialBuilding':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forResidentialBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.roomsAndKitchen}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsAndKitchenUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsAndKitchenConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="roomsAndKitchenRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.corridorsStaircaseStore}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsStaircaseStoreUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsStaircaseStoreConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="corridorsStaircaseStoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.balcony}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="balconyUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="balconyConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="balconyRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'forHotelsHostelsDormitories':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forHotelsHostelsDorm}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.livingBedAndDormitories}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="livingBedAndDormitoriesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="livingBedAndDormitoriesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="livingBedAndDormitoriesRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.kitchenCorridorsStaricase}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchenCorridorsStaricaseUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchenCorridorsStaricaseConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="kitchenCorridorsStaricaseRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.storeRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storeRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storeRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="storeRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.diningResturants}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="diningResturantsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="diningResturantsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="diningResturantsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.officeRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="officeRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="officeRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="officeRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'forEducaitonalBuildings':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forEducationalBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.classRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="classRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="classRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="classRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.kitchen}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchenUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchenConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="kitchenRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.stores}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="storesRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.librariesAndArchives}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="librariesAndArchivesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="librariesAndArchivesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="librariesAndArchivesRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.blaconies}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="blaconiesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="blaconiesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="blaconiesRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'forInstitutionalBuildings':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forInstitutionalBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.bedRoomsWardsDressingRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="bedRoomsWardsDressingRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="bedRoomsWardsDressingRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="bedRoomsWardsDressingRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.kitchens}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchensUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="kitchensConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="kitchensRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.xrayOperatingRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="xrayOperatingRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="xrayOperatingRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="xrayOperatingRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.corridorsAndStaircase}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsAndStaircaseUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsAndStaircaseConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="corridorsAndStaircaseRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.blaconies}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="IBblaconiesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="IBblaconiesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="IBblaconiesRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'forAssemblyBuildings':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forAssemblyBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.assemblyAreas}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="assemblyAreasUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="assemblyAreasConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="assemblyAreasRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.projectionsRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="projectionsRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="projectionsRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="projectionsRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.stages}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="stagesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="stagesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="stagesRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.corridorsPassageStaricase}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsPassageStaricaseUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsPassageStaricaseConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="corridorsPassageStaricaseRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.blaconies}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="ABblaconiesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="ABblaconiesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="ABblaconiesRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'forBusinessAndOfficeBuildings':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forBusinessAndOfficeBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.roomsWithSeperateStorage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsWithSeperateStorageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsWithSeperateStorageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="roomsWithSeperateStorageRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.roomsWithoutSeperateStorage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsWithoutSeperateStorageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsWithoutSeperateStorageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="roomsWithoutSeperateStorageRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.filesRoomsAndStorageRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="filesRoomsAndStorageRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="filesRoomsAndStorageRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="filesRoomsAndStorageRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.stairAndPassage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="stairAndPassageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="stairAndPassageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="stairAndPassageRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.blaconies}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="BAOBblaconiesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="BAOBblaconiesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="BAOBblaconiesRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'mercantileBuilding':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forMercantileBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.retailShops}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="retailShopsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="retailShopsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="retailShopsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.wholesaleShops}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="wholesaleShopsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="wholesaleShopsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="wholesaleShopsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.office}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="officeUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="officeConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="officeRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.staricaseAndPassage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="MBstaricaseAndPassageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="MBstaricaseAndPassageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="MBstaricaseAndPassageRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.blaconies}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="MBblaconiesUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="MBblaconiesConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="MBblaconiesRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);

			case 'industrialBuilding':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.industrialBuilding}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.workAreaWithoutMachinery}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="workAreaWithoutMachineryUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="workAreaWithoutMachineryConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="workAreaWithoutMachineryRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.withMachineryLightDuty}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="withMachineryLightDutyUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="withMachineryLightDutyConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="withMachineryLightDutyRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.meduimDuty}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="meduimDutyUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="meduimDutyConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="meduimDutyRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.heavyDuty}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="heavyDutyUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="heavyDutyConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="heavyDutyRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.boiler}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="boilerUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="boilerConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="boilerRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.staircasePassage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="staircasePassageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="staircasePassageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="staircasePassageRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);
			case 'storagelBuilding':
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.storageBuilding}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.storageRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storageRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="storageRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="storageRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.coldStorag}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="coldStoragUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="coldStoragConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="coldStoragRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.corridorAndPassage}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorAndPassageUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorAndPassageConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="corridorAndPassageRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.boilerRooms}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="boilerRoomsUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="boilerRoomsConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="boilerRoomsRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);
			default:
				return (
					<>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqDataNBC1031994.forResidentialBuildings}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.roomsAndKitchen}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsAndKitchenUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="roomsAndKitchenConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="roomsAndKitchenRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.corridorsStaircaseStore}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsStaircaseStoreUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="corridorsStaircaseStoreConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="corridorsStaircaseStoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell>{reqDataNBC1031994.balcony}</Table.Cell>
							<Table.Cell>
								<TableInputIm name="balconyUDL" />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name="balconyConcentrated" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="balconyRemarks" />
							</Table.Cell>
						</Table.Row>
					</>
				);
		}
	}
	render() {
		const { value, setFieldValue } = this.props;
		return (
			<Table.Body>
				<Table.Row>
					<Table.Cell colSpan="6">
						<b>{structureDesignClassBBiratnagarRequiredData.fourthHeading}</b>
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell width={1} rowSpan="2"></Table.Cell>
					<Table.Cell rowSpan="2">
						<div>{reqDataNBC1031994.porposedOccupancyType}</div>
						<div>{reqDataNBC1031994.fillInTheConcerning}</div>
						<div>
							<Dropdown
								name="occupancyDropDown"
								onChange={(e, { value }) => setFieldValue('occupancyDropDown', value)}
								placeholder="For Residential Building"
								fluid
								selection
								options={occupancyType}
							/>
						</div>
					</Table.Cell>
					<Table.Cell colSpan="2">{reqDataNBC1031994.occupancyLoad}</Table.Cell>
					<Table.Cell width={2} rowSpan="2">
						Remarks
					</Table.Cell>
				</Table.Row>
				<Table.Row>
					<Table.Cell>{reqDataNBC1031994.uniformlyDistributedLoad}</Table.Cell>
					<Table.Cell>{reqDataNBC1031994.concentratedLoad}</Table.Cell>
				</Table.Row>
				{this.dropDown(value.occupancyDropDown)}
			</Table.Body>
		);
	}
}

export default RequirementOfNepalNationalBuildingTable24;
