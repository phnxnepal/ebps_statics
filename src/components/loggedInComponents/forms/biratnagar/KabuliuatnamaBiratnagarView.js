import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Form } from 'semantic-ui-react';
import { Formik } from 'formik';
import { requiredDataForKabuliuatnamaBiratnagarView } from '../../../../utils/data/KabuliuatnamaBiratnagarViewData';
import { SectionHeader } from '../../../uiComponents/Headers';
import { DashedSelect } from '../../../shared/Select';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess, surroundingMappingFlat } from '../../../../utils/dataUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { isEmpty } from '../../../../utils/functionUtils';
const reqData = requiredDataForKabuliuatnamaBiratnagarView;

const natiNatinaOptions = [
	{
		key: reqData.nati,
		text: reqData.nati,
		value: reqData.nati,
	},
	{
		key: reqData.natini,
		text: reqData.natini,
		value: reqData.natini,
	},
];

const choraChoriPatiPatniOptions = [
	{
		key: reqData.chora,
		text: reqData.chora,
		value: reqData.chora,
	},
	{
		key: reqData.chori,
		text: reqData.chori,
		value: reqData.chori,
	},
	{
		key: reqData.pati,
		text: reqData.pati,
		value: reqData.pati,
	},
	{
		key: reqData.patni,
		text: reqData.patni,
		value: reqData.patni,
	},
];

const initialVal = {
	choraChoriPatiPatniAge: '',
	choraChoriPatiPatniName: '',
	choraChoriPatiPatni: choraChoriPatiPatniOptions[0].value,
	natiNatina: natiNatinaOptions[0].value,
	natiNatinaName: '',
	gravelKacchi: '',
	feetMeter: '',
	chadiBanunuParne: '',
	michiyekoDekhiyekale: '',
	nakshaPassHunaNasakeko: '',
	jaggaMa: '',
	tarfaKoSaAaChe: '',
	itiSambat: '',
	saal: '',
	mahina: '',
};

class KabuliuatnamaBiratnagarViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},

			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const { formUrl, hasDeletePermission, isSaveDisabled, prevData, hasSavePermission, permitData } = this.props;

		return (
			<div>
				<Formik
					initialValues={initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						try {
							await this.props.postAction(api.kabuliyatnama, values, true);
							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, handleSubmit, setFieldValue, handleChange, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div>
								<SectionHeader>
									<h2 className="underline">{reqData.heading}</h2>
								</SectionHeader>
								<br />
								<br />
								<div>
									{reqData.likhitimWadaNumber}
									{permitData.newWardNo}
									{/* ------ */}
									{reqData.basne}
									{permitData.applicantName}
									{/* ------ */}
									<DashedSelect name="natiNatina" options={natiNatinaOptions} />
									<CompactDashedLangInput
										name="natiNatinaName"
										value={values.natiNatinaName}
										error={errors.natiNatinaName}
										setFieldValue={setFieldValue}
									/>
									{reqData.ko}
									<DashedSelect name="choraChoriPatiPatni" options={choraChoriPatiPatniOptions} />
									{/* -------- */}
									{reqData.barsa}
									<CompactDashedLangInput
										name="choraChoriPatiPatniAge"
										value={values.choraChoriPatiPatniAge}
										error={errors.choraChoriPatiPatniAge}
										setFieldValue={setFieldValue}
									/>
									{/* -------- */}
									{reqData.ko}
									<CompactDashedLangInput
										name="choraChoriPatiPatniName"
										value={values.choraChoriPatiPatniName}
										error={errors.choraChoriPatiPatniName}
										setFieldValue={setFieldValue}
									/>
									{/* ---- */}
									{reqData.aageMaile}
									{permitData.newWardNo}
									{/* ---- */}
									{reqData.sitithKittaNumber}
									{permitData.kittaNo}
									{/* ---- */}
									{surroundingMappingFlat.map((surr) => {
										let dataSurr = {};
										// let index = 0;
										if (permitData.surrounding && Array.isArray(permitData.surrounding) && permitData.surrounding.length > 0) {
											dataSurr = permitData.surrounding.find((row) => row.side === surr.side) || {};
											// index = values.surrounding.indexOf(dataSurr);
										}
										return (
											<>
												{!isEmpty(dataSurr) && (
													<span key={surr.side}>
														{' '}
														{surr.value}: {dataSurr && dataSurr.kittaNo}{' '}
													</span>
												)}
											</>
										);
									})}
									{reqData.charKillaBhitrakoJagga}
									{permitData.landArea}({permitData.landAreaType}){/* ------ */}
									{reqData.nirmanGarnakaLagiNakshaPass}
									{permitData.buildingJoinRoad}
									{/* ------ */}
									{reqData.gravelKacchi}
									<CompactDashedLangInput
										name="gravelKacchi"
										value={values.gravelKacchi}
										error={errors.gravelKacchi}
										setFieldValue={setFieldValue}
									/>
									{/* ------ */}
									{reqData.feetMeter}
									<CompactDashedLangInput
										name="feetMeter"
										value={values.feetMeter}
										error={errors.feetMeter}
										setFieldValue={setFieldValue}
									/>
									{/* ------ */}
									{reqData.chadiBanunuParne}
									<CompactDashedLangInput
										name="chadiBanunuParne"
										value={values.chadiBanunuParne}
										error={errors.chadiBanunuParne}
										setFieldValue={setFieldValue}
									/>
									{/* ------ */}
									{reqData.michiyekoDekhiyekale}
									<CompactDashedLangInput
										name="michiyekoDekhiyekale"
										value={values.michiyekoDekhiyekale}
										error={errors.michiyekoDekhiyekale}
										setFieldValue={setFieldValue}
									/>
									{/* ------ */}
									{reqData.nakshaPassHunaNasakeko}
									<CompactDashedLangInput
										name="nakshaPassHunaNasakeko"
										value={values.nakshaPassHunaNasakeko}
										error={errors.nakshaPassHunaNasakeko}
										setFieldValue={setFieldValue}
									/>
									{/* ------ */}
									{reqData.nakshaPassGaridinuhuna}
									{permitData.kittaNo}
									{/* ------ */}
									{reqData.jaggaBigaha}
									{permitData.landArea}({permitData.landAreaType}){/* ------ */}
									{reqData.ma}
									<CompactDashedLangInput
										name="jaggaMa"
										value={values.jaggaMa}
										error={errors.jaggaMa}
										setFieldValue={setFieldValue}
									/>
									{/* ------- */}
									{reqData.tarfaKoSaAaChe}
									<CompactDashedLangInput
										name="tarfaKoSaAaChe"
										value={values.tarfaKoSaAaChe}
										error={errors.tarfaKoSaAaChe}
										setFieldValue={setFieldValue}
									/>
									{/* ------- */}
									{reqData.dataLine}
									<br />
									<br />
									<br />
									{reqData.itiSambat}
									<CompactDashedLangInput
										name="itiSambat"
										value={values.itiSambat}
										error={errors.itiSambat}
										setFieldValue={setFieldValue}
									/>
									{/* ------- */}
									{reqData.saal}
									<CompactDashedLangInput name="saal" value={values.saal} error={errors.saal} setFieldValue={setFieldValue} />
									{/* ------- */}
									{reqData.mahina}
									<CompactDashedLangInput name="mahina" value={values.mahina} error={errors.mahina} setFieldValue={setFieldValue} />
									{/* ------- */}
									{reqData.gate}
								</div>
							</div>
							<br />
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const KabuliuatnamaBiratnagarView = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.kabuliyatnama).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <KabuliuatnamaBiratnagarViewComponent {...props} parentProps={parentProps} />}
	/>
);
export default KabuliuatnamaBiratnagarView;
