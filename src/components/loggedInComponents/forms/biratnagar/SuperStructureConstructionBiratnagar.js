import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import api from '../../../../utils/api';
import { getApproveByObject, getSaveByUserDetails } from '../../../../utils/formUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, areaUnitOptions } from '../../../../utils/dataUtils';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { isStringEmpty } from '../../../../utils/stringUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import { getConstructionTypeValue } from '../../../../utils/enums/constructionType';
import { BiratnagarSuperStruConsSchema } from '../../formValidationSchemas/superStructureConstructionValidation';
import { Form } from 'semantic-ui-react';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { LetterHeadFlex } from '../../../shared/LetterHead';
import { PatraSankhyaAndDate } from '../formComponents/PatraSankhyaAndDate';
import { SectionHeader } from '../../../uiComponents/Headers';
import { superstructureconstructionview, mapTechnical, buildingPermitApplicationForm } from '../../../../utils/data/mockLangFile';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import { Formik, Field } from 'formik';
import { FooterSignature } from '../formComponents/FooterSignature';
import { PrintParams, PrintIdentifiers } from '../../../../utils/printUtils';
import { getMessage } from '../../../shared/getMessage';
import { DashedLangDateField } from '../../../shared/DateField';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { ConstructionTypeRadio } from '../mapPermitComponents/ConstructionTypeRadio';
import { DashedAreaUnitInput, DashedMultiUnitAreaInput, DashedMultiUnitLengthInput, DashedUnitInput } from '../../../shared/EbpsUnitLabelValue';

const sscv_data = superstructureconstructionview.superstructureconstructionview_data;
const footerArray = superstructureconstructionview.superstructureconstructionview_data.footerArray;
const buildPermitLang = buildingPermitApplicationForm.permitApplicationFormView;

const mapTechLang = mapTechnical.mapTechnicalDescription;
const messageId = 'superstructureconstructionview.superstructureconstructionview_data';

class SuperStructureConstructionBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData, otherData, enterByUser, userData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);
		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const rajaswo = getJsonData(otherData.rajaswo);
		// const suikritTalla = this.props.otherData.anusuchiGha.tallaThap;

		// const sadakAdhikar = () => _.max(otherJsonData.sadakAdhikarKshytra);

		// const setbackMaptech = () => {
		// 	if (otherJsonData.setBack) {
		// 		return Math.max.apply(
		// 			Math,
		// 			otherJsonData.setBack.map(value => value.distanceFromRoadCenter)
		// 		);
		// 	}
		// 	return null;
		// };

		const desApprovJsonData = otherData.designApproval;
		const anuSucKaJsonData = otherData.anukaMaster;
		const suikritTalla = otherData.anusuchiGha.thapTalla;
		// console.log('talla---', suikritTalla);

		const allowance = getJsonData(otherData.allowance);
		const noteorder = getJsonData(otherData.noteorder);
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);

		const floorArray = new FloorBlockArray(permitData.floor);
		const floorMax = floorArray.getTopFloor().nepaliCount;

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					// plinthDetailsUnit: 'METRE',
					// buildCoverAreaUnit: 'METRE',
					// coverageDetailsUnit: 'METRE',
					buildingAreaUnit: DEFAULT_UNIT_AREA,
					coverageAreaUnit: DEFAULT_UNIT_AREA,
					houseLengthUnit: DEFAULT_UNIT_LENGTH,
					houseBreadthUnit: DEFAULT_UNIT_LENGTH,
					houseHeightUnit: DEFAULT_UNIT_LENGTH,
					constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					plinthDetailsUnit: DEFAULT_UNIT_AREA,
					buildCoverAreaUnit: DEFAULT_UNIT_AREA,
					coverageDetailsUnit: DEFAULT_UNIT_AREA,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['constructionType', 'purposeOfConstruction', 'landArea', 'landAreaType'] },
			{
				obj: mapTech,
				reqFields: ['approveDate', 'plinthDetails', 'plinthDetailsUnit', 'roof', 'coverageDetails', 'buildingHeight', 'allowableHeight'],
			},
			{ obj: rajaswo, reqFields: ['allowableHeight', 'constructionHeight', 'constructionHeightUnit'] },
			// {
			// 	obj: {
			// 		setbackMetre: setbackMaptech(),
			// 		sadakMetre: sadakAdhikar()

			// 	}, reqFields: []
			// }
			{
				obj: allowance,
				reqFields: ['allowanceDate', 'gharNo', 'publicPropertyDistance', 'publicPropertyUnit', 'sadakAdhikarUnit', 'requiredDistance'],
			},
			{
				obj: noteorder,
				reqFields: ['noteOrderDate'],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.supStrucDate)) {
			initialValues.supStrucDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);
		initialValues.permissionOrderDate = initialValues.noteOrderDate;
		this.state = {
			isSucess: false,
			initialValues,
			suikritTalla,
			buildingClass,
			floorMax,
		};
	}

	render() {
		const {
			prevData,
			permitData,
			userData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;
		const { initialValues, suikritTalla, buildingClass, floorMax } = this.state;
		const user_info = this.props.userData;
		return (
			// <div className='superStruConsView'>

			<Formik
				initialValues={initialValues}
				validationSchema={BiratnagarSuperStruConsSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						await this.props.postAction(`${api.superStructureConstruction}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error in Structure Construction Save', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef} className="print-small-font">
							<div>
								<LetterHeadFlex userInfo={user_info} />
								<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} dateFieldName="supStrucDate" />
								<br />
								<SectionHeader>
									<h3 className="underline end-section">{sscv_data.title}</h3>
								</SectionHeader>
								<div>
									{getMessage(`${messageId}.data_1.data_1_1`, sscv_data.data_1.data_1_1)} {permitData.nibedakName} <br />
									{userData.organization.name} {getMessage(`${messageId}.data_1.data_1_2`, sscv_data.data_1.data_1_2)}{' '}
									{permitData.nibedakTol} {getMessage(`${messageId}.data_1.data_1_3`, sscv_data.data_1.data_1_3)} {values.gharNo}{' '}
									{getMessage(`${messageId}.data_1.data_1_4`, sscv_data.data_1.data_1_4)}
									{permitData.nibedakSadak}
								</div>
								<br />
								<div>
									<div className="no-margin-field">
										{getMessage(`${messageId}.data_2.data_2_1`, sscv_data.data_2.data_2_1)}{' '}
										<DashedLangDateField
											name="allowanceDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.allowanceDate}
											error={errors.allowanceDate}
										/>
										{getMessage(`${messageId}.data_2.data_2_2`, sscv_data.data_2.data_2_2)}
										{userData.organization.name}
										<>
											{getMessage(`${messageId}.data_2.data_2_4`, sscv_data.data_2.data_2_4)}
											<DashedLangInput
												name="dasturAmount"
												setFieldValue={setFieldValue}
												value={values.dasturAmount}
												error={errors.dasturAmount}
											/>
											{getMessage(`${messageId}.data_2.data_2_5`, sscv_data.data_2.data_2_5)}
											<DashedLangInput
												name="jariwanaAmount"
												setFieldValue={setFieldValue}
												value={values.jariwanaAmount}
												error={errors.jariwanaAmount}
											/>
											{getMessage(`${messageId}.data_2.data_2_5`, sscv_data.data_2.data_2_6)}
										</>
										{/* <DashedLangInput name="jariwanaRate" value={values.jariwanaRate} setFieldValue={setFieldValue} error={errors.jariwanaRate} /> */}
										{/* {getMessage(`${messageId}.data_1.data_2_4`, sscv_data.data_2.data_2_4)} */}
									</div>
									<div>{getMessage(`${messageId}.data_3`, sscv_data.data_3)}</div>
									<div>
										{getMessage(`${messageId}.data_4.data_4_1`, sscv_data.data_4.data_4_1)} {permitData.oldMunicipal}{' '}
										{getMessage(`${messageId}.data_4.data_4_2`, sscv_data.data_4.data_4_2)} {userData.organization.name}{' '}
										{getMessage(`${messageId}.data_4.data_4_3`, sscv_data.data_4.data_4_3)} {permitData.newWardNo}{' '}
										{getMessage(`${messageId}.data_4.data_4_5`, sscv_data.data_4.data_4_5)} {permitData.buildingJoinRoad}{' '}
										{getMessage(`${messageId}.data_4.data_4_6`, sscv_data.data_4.data_4_6)} {permitData.kittaNo}{' '}
										{/* {getMessage(`${messageId}.data_4.data_4_7`, sscv_data.data_4.data_4_7)} */}
										{sscv_data.data_4.data_4_7}
										{permitData.landArea} {permitData.landAreaType}
										{/* <UnitDropdownWithRelatedFields options={landAreaTypeOptions} unitName="landAreaType" defaultValue={'रोपनी–आना–पैसा–दाम'} relatedFields={[]}/> */}
									</div>
								</div>
								<ConstructionTypeRadio
									space={true}
									fieldLabel={getMessage(`${messageId}.maindata.maindata_1`, sscv_data.maindata.maindata_1)}
								/>
								<div>
									{getMessage(`${messageId}.maindata.maindata_2`, sscv_data.maindata.maindata_2)}{' '}
									{Object.values(mapTechLang.mapTech_22.mapTech_22_radioBox).map((name, index) => (
										<div key={index} className="ui radio checkbox prabidhik">
											<Field type="radio" name="roof" id={index} defaultChecked={values.roof === name} value={name} />
											<label>{name}</label>
										</div>
									))}
								</div>
								<div>
									<DashedAreaUnitInput name="plinthDetails" label={sscv_data.maindata.maindata_3} unitName="plinthDetailsUnit" />
									<DashedAreaUnitInput
										name="buildCoverArea"
										unitName="buildCoverAreaUnit"
										label={sscv_data.maindata.maindata_3_1}
									/>
									<DashedAreaUnitInput
										name="coverageDetails"
										unitName="coverageDetailsUnit"
										label={sscv_data.maindata.maindata_3_2}
									/>

									<div className="div-indent">
										<DashedAreaUnitInput
											name="oldPlinthDetails"
											label={sscv_data.maindata.sabikPlinthArea}
											unitName="plinthDetailsUnit"
										/>
										<DashedAreaUnitInput
											name="oldBuildCoverArea"
											unitName="buildCoverAreaUnit"
											label={sscv_data.maindata.maindata_3_1}
										/>
									</div>
								</div>
								<div>
									{getMessage(`${messageId}.maindata.maindata_4`, sscv_data.maindata.maindata_4)}
									{/* <DashedLangInput name="buildingHeight" value={values.buildingHeight} setFieldValue={setFieldValue} error={errors.buildingHeight} /> */}
									<span> {floorMax} </span>
									{getMessage(`${messageId}.maindata.maindata_4_1`, sscv_data.maindata.maindata_4_1)}
									<span> {suikritTalla}</span>
									{/* <DashedLangInput name="suikritTalla" value={values.suikritTalla} setFieldValue={setFieldValue} error={errors.suikritTalla} /> */}
								</div>
								<div>
									<DashedMultiUnitLengthInput
										name="constructionHeight"
										unitName="constructionHeightUnit"
										relatedFields={['allowableHeight']}
										label={sscv_data.maindata.maindata_5}
									/>
									<DashedMultiUnitLengthInput
										name="allowableHeight"
										unitName="constructionHeightUnit"
										relatedFields={['constructionHeight']}
										label={sscv_data.maindata.maindata_5_1}
									/>
								</div>
								<div>
									{getMessage(`${messageId}.maindata.maindata_6`, sscv_data.maindata.maindata_6)}{' '}
									{Object.values(buildPermitLang.form_step5.checkBox_option).map((name, index) => (
										<div key={index} className="ui radio checkbox prabidhik">
											<Field
												type="radio"
												name="purposeOfConstruction"
												defaultChecked={values.purposeOfConstruction === name}
												value={name}
											/>
											<label>{name}</label>
										</div>
									))}
								</div>
								<div>
									{getMessage(`${messageId}.maindata.maindata_7`, sscv_data.maindata.maindata_7)}&nbsp;
									{buildingClass && `${buildingClass.nameNepali} ${buildingClass.name}`}
								</div>
								<div>
									<DashedUnitInput name="requiredDistance" label={sscv_data.maindata.maindata_8} unitName="sadakAdhikarUnit" />
								</div>
								<div>
									<DashedUnitInput
										name="publicPropertyDistance"
										label={sscv_data.maindata.maindata_9}
										unitName="publicPropertyUnit"
										value={values.setBack}
									/>
								</div>
								<>
									<div className="no-margin-field">
										{getMessage(`${messageId}.maindata.maindata_10`, sscv_data.maindata.maindata_10)}{' '}
										<DashedLangDateField
											name="allowanceDate"
											setFieldValue={setFieldValue}
											value={values.allowanceDate}
											error={errors.allowanceDate}
											inline={true}
											className="dashedForm-control"
										/>
										{/* <DashedLangDateField name='allowanceDate' handleChange={handleChange} value={values.allowanceDate} setFieldValue={setFieldValue} error={errors.allowanceDate} /> */}
										{getMessage(`${messageId}.maindata.maindata_10_1`, sscv_data.maindata.maindata_10_1)}
									</div>
									<div>
										{sscv_data.maindata.eleven}{' '}
										<DashedMultiUnitLengthInput
											name="houseLength"
											unitName="houseLengthUnit"
											relatedFields={['houseBreadth', 'houseHeight']}
											areaField={'buildingArea'}
											label={sscv_data.maindata.maindata_3_5}
										/>
										<DashedMultiUnitLengthInput
											name="houseBreadth"
											unitName="houseBreadthUnit"
											relatedFields={['houseLength', 'houseHeight']}
											areaField={'buildingArea'}
											label={sscv_data.maindata.maindata_3_6}
										/>
										<DashedMultiUnitAreaInput
											name="buildingArea"
											unitName="buildingAreaUnit"
											label={sscv_data.maindata.maindata_3_3}
											squareOptions={areaUnitOptions}
											relatedFields={['houseLength', 'houseBreadth', 'houseHeight']}
										/>
									</div>
									<FooterSignature
										designations={[sscv_data.footer.footer_1, sscv_data.footer.footer_2, sscv_data.footer.nagarPramukh]}
										signatureImages={
											useSignatureImage && [values.sipharisSignature, values.swikritSignature, values.pramukhSignature]
										}
									/>

									<div>{sscv_data.footer.footer_4}</div>

									<div>
										<FooterSignature designations={footerArray} signatureImages={[staticFiles.ghardhaniSignature, '']} />
									</div>
								</>
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}

const SuperStructureConstructionBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.anusuchiGha,
				objName: 'anusuchiGha',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthTechApp',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteorder',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		checkPreviousFiles={true}
		onBeforeGetContent={{
			//param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			//param9: ['getElementsByClassName', 'brackets', 'value'],
		}}
		render={(props) => <SuperStructureConstructionBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default SuperStructureConstructionBiratnagar;
