import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { SectionHeader } from '../../../uiComponents/Headers';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { aabedanFaramData } from '../../../../utils/data/AabedanFaramData';
import { FlexSingleLeft } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangInput } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { prepareMultiInitialValues, checkError, handleSuccess, getJsonData } from '../../../../utils/dataUtils';
import { validateNullableOfficialNumbers, validateNullableNepaliDate, validateNullableNumber } from '../../../../utils/validationUtils';
import { DashedSelect } from '../../../shared/Select';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { SignatureImage } from '../formComponents/SignatureImage';
import { LetterSalutation } from '../formComponents/LetterSalutation';
import { PrintParams } from '../../../../utils/printUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
const { object } = require('yup');

const reqData = aabedanFaramData;
const maileHamile = [
	{
		key: reqData.maile,
		text: reqData.maile,
		value: reqData.maile,
	},
	{
		key: reqData.hamile,
		text: reqData.hamile,
		value: reqData.hamile,
	},
];
const supCom = [
	{
		key: reqData.superStructure,
		text: reqData.superStructure,
		value: reqData.superStructure,
	},
	{
		key: reqData.compoundWall,
		text: reqData.compoundWall,
		value: reqData.compoundWall,
	},
];
const chuChau = [
	{
		key: reqData.chu,
		text: reqData.chu,
		value: reqData.chu,
	},
	{
		key: reqData.chau,
		text: reqData.chau,
		value: reqData.chau,
	},
];
const initialVal = {
	chana: '',
	chanaMiti: '',
	maileHamileDropDown: maileHamile[0].value,
	superStructureCompoundWallDropdown: supCom[0].value,
	chetraFall: '',
	dharauti: '',
	chuChau1: chuChau[0].value,
	chuChau2: chuChau[0].value,
	nibedakKoNaam: '',
	waresKoNaam: '',
	thegana: '',
};

const validate = object({
	chana: validateNullableOfficialNumbers,
	chanaMiti: validateNullableNepaliDate,
	chetraFall: validateNullableNumber,
	dharauti: validateNullableNumber,
});
class NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaramComponent extends Component {
	constructor(props) {
		super(props);
		let initialValue = {};
		initialValue = prepareMultiInitialValues(
			{
				obj: initialVal,
				reqFields: [],
			},
			{
				obj: this.props.permitData,
				reqFields: [],
			},
			{
				obj: { chanaMiti: getCurrentDate(true) },
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValue,
		};
	}
	render() {
		const { formUrl, userData, hasDeletePermission, isSaveDisabled, prevData, hasSavePermission, permitData, staticFiles } = this.props;
		const salutationData = [ `${userData.organization.name}को कार्यालय`, `${userData.organization.address}`];
		console.log(userData)
		
		return (
			<Formik
				initialValues={this.state.initialValue}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					try {
						await this.props.postAction(api.NayaNirmanPramanPatraDharautiPhirta, values, true);
						window.scroll(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
						actions.setSubmitting(false);
					} catch (err) {
						actions.setSubmitting(false);
						console.log('Error', err);
					}
				}}
				validationSchema={validate}
			>
				{({ isSubmitting, values, handleSubmit, setFieldValue, handleChange, errors, validateForm }) => (
					<div>
						{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}

						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h1 className="underline">{reqData.aabedanFaram}</h1>
								</SectionHeader>
								<LetterSalutation lines={salutationData} />

								<br />
								<br />
								<SectionHeader>
									<h3 className="underline">{reqData.bisaye}</h3>
								</SectionHeader>
								<br />
								<div>{reqData.mahodaye}</div>
								<div>
									{reqData.chana}
									<CompactDashedLangInput name="chana" value={values.chana} error={errors.chana} setFieldValue={setFieldValue} />
									{reqData.miti}{' '}
									<CompactDashedLangDate
										name="chanaMiti"
										value={values.chanaMiti}
										error={errors.chanaMiti}
										setFieldValue={setFieldValue}
									/>
									{reqData.gharKo}
									{reqData.nirman}
									<DashedSelect name="superStructureCompoundWallDropdown" options={supCom} />
									<DashedSelect name="maileHamileDropDown" options={maileHamile} />
									{reqData.chetraFalFeet}
									<CompactDashedLangInput
										name="chetraFall"
										value={values.chetraFall}
										error={errors.chetraFall}
										setFieldValue={setFieldValue}
									/>
									{reqData.dharauti} <DashedSelect name="chuChau1" options={chuChau} />
									{reqData.nirmanKarya}
									<CompactDashedLangInput
										name="dharauti"
										value={values.dharauti}
										error={errors.dharauti}
										setFieldValue={setFieldValue}
									/>
									{reqData.fitraPau}
									<DashedSelect name="chuChau2" options={chuChau} />|
								</div>

								<br />
								<br />
								<div
									style={{
										display: 'flex',
										flexDirection: 'column',

										width: '350px',
										height: 'auto',

										margin: '0 70%',
									}}
								>
									<SectionHeader>
										<div>{reqData.nibedak}</div>
									</SectionHeader>
									<br />
									<div>
										{reqData.nibedakKoNaam} : {permitData.nibedakName}
									</div>
									<div>
										{reqData.waresKoNaam}:
										<CompactDashedLangInput name="waresKoNaam" value={values.waresKoNaam} setFieldValue={setFieldValue} />
									</div>
									<div>
										{reqData.thegana}: {permitData.newMunicipal} - {permitData.nibedakTol}, {permitData.nibedakSadak}
									</div>
									<div>
										<SignatureImage showSignature={true} value={staticFiles.ghardhaniSignature} label={reqData.dastaKhat} />
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								handleSubmit={handleSubmit}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
							/>
						</Form>
					</div>
				)}
			</Formik>
		);
	}
}
const NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaram = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.NayaNirmanPramanPatraDharautiPhirta).setForm().getParams()]}
		onBeforeGetContent={{
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			...PrintParams.DASHED_INPUT,
			...PrintParams.INLINE_FIELD,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		checkPreviousFiles={true}
		render={(props) => <NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaramComponent {...props} parentProps={parentProps} />}
	/>
);
export default NirmanKaryaSampannaPramanPatraTathaDharautiFirtaAabedanFaram;
