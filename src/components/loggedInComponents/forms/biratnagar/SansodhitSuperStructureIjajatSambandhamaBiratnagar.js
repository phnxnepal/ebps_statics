import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { SectionHeader } from '../../../uiComponents/Headers';
import { requiredData } from '../../../../utils/data/SansodhitSuperStructureIjajatSambandhamaData';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { CompactDashedLangInput, DashedNormalInputIm } from '../../../shared/DashedFormInput';
import { CompactDashedLangDate } from '../../../shared/DateField';
import { Form } from 'semantic-ui-react';

import { DashedSelect } from '../../../shared/Select';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess } from '../../../../utils/dataUtils';
import { validateNepaliDate, validateNormalNumber } from '../../../../utils/validationUtils';
import { getCurrentDate } from '../../../../utils/dateUtils';
import SaveButtonValidation from '../../../shared/SaveButtonValidation';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { PrintParams } from '../../../../utils/printUtils';
const { object } = require('yup');

const reqData = requiredData;
const shriShrimatiShushriOptions = [
	{
		key: reqData.shri,
		text: reqData.shri,
		value: reqData.shri,
	},
	{
		key: reqData.shrimati,
		text: reqData.shrimati,
		value: reqData.shrimati,
	},
	{
		key: reqData.shuShri,
		text: reqData.shuShri,
		value: reqData.shuShri,
	},
];
const kothaTallaOptions = [
	{
		key: reqData.thapKotha,
		text: reqData.thapKotha,
		value: reqData.thapKotha,
	},
	{
		key: reqData.thapTalla,
		text: reqData.thapTalla,
		value: reqData.thapTalla,
	},
];
const nirmanGarnaGarekomaOptions = [
	{
		key: reqData.nirmanGarne,
		text: reqData.nirmanGarne,
		value: reqData.nirmanGarne,
	},
	{
		key: reqData.nirmanGarekoma,
		text: reqData.nirmanGarekoma,
		value: reqData.nirmanGarekoma,
	},
];
const initialValue = {
	date: '',
	shriShrimatiShushri: shriShrimatiShushriOptions[0].value,
	nirmanType: '',
	nirmanGarnaMiti: '',
	thapKothaTalla: kothaTallaOptions[0].value,
	nirmanGarnaGarekoma: nirmanGarnaGarekomaOptions[0].value,
	subEngineerName: '',
	dasturRu: '',
	acharopi: '',
};
const validate = object({
	date: validateNepaliDate,
	nirmanGarnaMiti: validateNepaliDate,
	dasturRu: validateNormalNumber,
});
class SansodhitSuperStructureIjajatSambandhamaBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const initialVal = prepareMultiInitialValues(
			{
				obj: initialValue,
				reqFields: [],
			},
			{
				obj: { date: getCurrentDate(true) },
				reqFields: [],
			},

			{
				obj: getJsonData(this.props.prevData),
				reqFields: [],
			}
		);
		this.state = {
			initialVal,
		};
	}
	render() {
		const { permitData, postAction, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialVal } = this.state;
		return (
			<>
				<Formik
					initialValues={initialVal}
					validationSchema={validate}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.sansodhanSupStruIjjaat, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, handleChange, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<SectionHeader>
									<h1>{reqData.mahanagarpalika}</h1>
									<h2>{reqData.address}</h2>
								</SectionHeader>
								<FlexSingleRight>
									{reqData.miti}
									<CompactDashedLangDate name="date" value={values.date} error={errors.date} setFieldValue={setFieldValue} />
								</FlexSingleRight>
								<br />
								<SectionHeader>
									<h2 className="underline">{reqData.bisaye}</h2>
									<br />
									<h2>{reqData.tippaniAadesh}</h2>
								</SectionHeader>
								<br />
								<div>
									{reqData.biratnagarMahanagarpaliksWadaNumber}
									{permitData.newWardNo}
									{reqData.basne}
									<DashedSelect
										name="shriShrimatiShushri"
										options={shriShrimatiShushriOptions}
										value={values.shriShrimatiShushri}
										error={errors.shriShrimatiShushri}
										setFieldValue={setFieldValue}
									/>
									{permitData.applicantName}
									{reqData.leNijKoNamamaDartaRahekoSabik}
									{permitData.oldMunicipal}
									{reqData.halkoWadaNumber}
									{permitData.newWardNo}
									{reqData.sadak}
									{permitData.nearestLocation}
									{reqData.koKiNo}
									{permitData.kittaNo}
									{reqData.chetrafal}
									{permitData.landArea}({permitData.landAreaType}){reqData.ma}
									<CompactDashedLangInput
										name="nirmanType"
										value={values.nirmanType}
										error={errors.nirmanType}
										setFieldValue={setFieldValue}
									/>
									{reqData.nirmanGarnaMiti}
									<CompactDashedLangDate
										name="nirmanGarnaMiti"
										value={values.nirmanGarnaMiti}
										error={errors.nirmanGarnaMiti}
										setFieldValue={setFieldValue}
									/>
									{reqData.ijajat}
									<DashedSelect
										name="thapKothaTalla"
										options={kothaTallaOptions}
										value={values.thapKothaTalla}
										error={errors.thapKothaTalla}
										setFieldValue={setFieldValue}
									/>{' '}
									<DashedSelect
										name="nirmanGarnaGarekoma"
										options={nirmanGarnaGarekomaOptions}
										value={values.nirmanGarnaGarekoma}
										error={errors.nirmanGarnaGarekoma}
										setFieldValue={setFieldValue}
									/>
									{reqData.prabidhikSubEngineer}
									<CompactDashedLangInput
										name="subEngineerName"
										value={values.subEngineerName}
										error={errors.subEngineerName}
										setFieldValue={setFieldValue}
									/>
									{reqData.dasturRu}
									<DashedNormalInputIm name="dasturRu" />
									{reqData.acharopi}
									<CompactDashedLangInput
										name="acharopi"
										value={values.acharopi}
										error={errors.acharopi}
										setFieldValue={setFieldValue}
									/>
									{reqData.dataLine}
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}
const SansodhitSuperStructureIjajatSambandhamaBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.sansodhanSupStruIjjaat).setForm().getParams()]}
		onBeforeGetContent={{
			...PrintParams.DASHED_INPUT,
			...PrintParams.INLINE_FIELD,
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <SansodhitSuperStructureIjajatSambandhamaBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default SansodhitSuperStructureIjajatSambandhamaBiratnagar;
