import React, { Component } from 'react';
import { highRisedBuilding } from '../../../../../utils/data/HighRisedBuildingData';
import { Table } from 'semantic-ui-react';
import { TableInputIm } from '../../../../shared/TableInput';
const reqData = highRisedBuilding.table3;

class HighRisedTableThree extends Component {
	render() {
		return (
			<div>
				<Table className="structure-ui-table">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={1}>{reqData.sn}</Table.HeaderCell>
							<Table.HeaderCell width={4}>{reqData.description}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.objects}</Table.HeaderCell>
							<Table.HeaderCell width={1}>{reqData.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell width={1}>1</Table.Cell>
							<Table.Cell colSpan="3">{reqData.wallSystem}</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.crossWallSystem}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="crossWallSystemObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="crossWallSystemRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.longWallSystem}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="longWallSystemObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="longWallSystemRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.twoWaySystem}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="twoWaySystemObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="twoWaySystemRemarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={1}>2</Table.Cell>
							<Table.Cell colSpan="3">{reqData.shearCoreSystem}</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.openCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="openCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="openCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.singleCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="singleCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="singleCoreRemarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={1}>3</Table.Cell>
							<Table.Cell colSpan="3">{reqData.arrangementOfCore}</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.locationOfCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="locationOfCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="locationOfCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.singleCoreWithLinear}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="singleCoreWithLinearArrangementObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="singleCoreWithLinearArrangementRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.exteriorCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="exteriorCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="exteriorCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.interiorCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="interiorCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="interiorCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.eccentricCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="eccentricCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="eccentricCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						{/* -------- */}
						<Table.Row>
							<Table.Cell width={1}>4</Table.Cell>
							<Table.Cell colSpan="3">{reqData.numberOfCore}</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.singleCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="singleCoreNumberObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="singleCoreNumberRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.splitCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="splitCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="splitCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.multipleCore}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="multipleCoreObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="multipleCoreRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>5</Table.Cell>
							<Table.Cell colSpan="3">{reqData.shapeOfCore}</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.closedForm}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="closedFormObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="closedFormRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={4}>{reqData.openForm}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="openFormObjects" />
							</Table.Cell>
							<Table.Cell width={1}>
								<TableInputIm name="openFormRemarks" />
							</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</div>
		);
	}
}

export default HighRisedTableThree;
