import React, { Component } from 'react';
import FormContainerV2 from '../../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../../utils/paramUtil';
import api from '../../../../../utils/api';
import { Formik } from 'formik';
import { Form, TextArea } from 'semantic-ui-react';
import { highRisedBuilding } from '../../../../../utils/data/HighRisedBuildingData';
import HighRisedTableOne from './HighRisedTableOne';
import HighRisedTableTwo from './HighRisedTableTwo';
import HighRisedTableThree from './HighRisedTableThree';
import { FlexSingleLeft } from '../../../../uiComponents/FlexDivs';
import { FooterSignature } from '../../formComponents/FooterSignature';
import { DashedNormalInputIm } from '../../../../shared/DashedFormInput';
import { initialValOfHighRaisedBuilding } from './initialValOfHighRisedBuilding';
import ErrorDisplay from '../../../../shared/ErrorDisplay';
import { prepareMultiInitialValues, getJsonData, checkError, handleSuccess } from '../../../../../utils/dataUtils';
import SaveButtonValidation from '../../../../shared/SaveButtonValidation';
const reqData = highRisedBuilding.table3;
class ForHighRisedBuildingComponent extends Component {
	constructor(props) {
		super(props);
		let mulitpleInitialValue = prepareMultiInitialValues(
			{
				obj: initialValOfHighRaisedBuilding,
				reqFields: [],
			},
			{
				obj: getJsonData(this.props.prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			mulitpleInitialValue,
		};
	}
	render() {
		const { permitData, formUrl, hasDeletePermission, isSaveDisabled, prevData, postAction, hasSavePermission } = this.props;
		return (
			<>
				<Formik
					initialValues={this.state.mulitpleInitialValue}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.highRisedBuildingapi, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, handleChange, handleSubmit, setFieldValue, errors, validateForm }) => (
						<div ref={this.props.setRef}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div className="english-div">
								<Form loading={isSubmitting}>
									<div>
										<h1>{highRisedBuilding.table1.heading}</h1>
										<HighRisedTableOne />
										<br />
										<br />
										<HighRisedTableTwo />
										<br />
										<br />
										<HighRisedTableThree />
										<br />
										<br />
										<FlexSingleLeft>
											<FooterSignature designations={[reqData.signatureOfConsultant]} />
											<br />
											<label htmlFor="nceRegdNo">{reqData.nceRegdNo}</label>
											<DashedNormalInputIm name="nceRegdNo" />
										</FlexSingleLeft>
										<TextArea name="commentIfAnyTextArea" placeholder={reqData.commentIfAny} />
									</div>
								</Form>
								<SaveButtonValidation
									errors={errors}
									validateForm={validateForm}
									handleSubmit={handleSubmit}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={checkError(prevData)}
								/>
							</div>
						</div>
					)}
				</Formik>
			</>
		);
	}
}
const ForHighRisedBuilding = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.highRisedBuildingapi).setForm().getParams()]}
		onBeforeGetContent={{}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <ForHighRisedBuildingComponent {...props} parentProps={parentProps} />}
	/>
);
export default ForHighRisedBuilding;
