export const initialValOfHighRaisedBuilding = {
	//table 1
	lessThan5000eia: '',
	lessThan5000Remarks: '',

	fiveThousandeia: '',
	fiveThousandRemarks: '',

	greaterThan10000iee: '',
	greaterThan10000Remarks: '',

	lessThan1Hectareeia: '',
	lessThan1HectareRemarks: '',
	lessThan1Hectareiee: '',

	OneHectare4Hectareeia: '',
	OneHectare4HectareRemarks: '',

	greaterThan4Hectareiee: '',
	greaterThan4HectareRemarks: '',

	fronteia: '',
	frontRemarks: '',

	sideeia: '',
	sideRemarks: '',
	builtUpAreaeia: '',
	builtUpAreaRemarks: '',
	openAreaeia: '',
	openAreaRemarks: '',
	greeneryAreaeia: '',
	greeneryAreaRemarks: '',

	motorParkiee: '',
	motorParkeia: '',
	motorParkRemarks: '',

	heightAndWidthOfWallCoreBuildingiee: '',
	heightAndWidthOfWallCoreBuildingeia: '',
	heightAndWidthOfWallCoreBuildingRemarks: '',

	calculatedEarthquakeLateralLoadiee: '',
	calculatedEarthquakeLateralLoadeia: '',
	calculatedEarthquakeLateralLoadRemarks: '',

	typeOfFoundationProvidediee: '',
	typeOfFoundationProvidedeia: '',
	typeOfFoundationProvidedRemarks: '',

	soilTestProcedureSystemiee: '',
	soilTestProcedureSystemeia: '',
	soilTestProcedureSystemRemarks: '',

	impactAndDynamicLoadiee: '',
	impactAndDynamicLoadeia: '',
	impactAndDynamicLoadRemarks: '',

	//table 2
	deadloadInKnPermissible: '',
	deadremarks: '',

	liveloadInKnPermissible: '',
	liveremarks: '',
	earthquakeloadInKnPermissible: '',
	earthquakeremarks: '',

	windloadInKnPermissible: '',
	windremarks: '',

	constructionloadInKnPermissible: '',
	constructionremarks: '',

	seismicLoadingWaterloadInKnPermissible: '',
	seismicLoadingWaterremarks: '',

	loadDueToRestrainedloadInKnPermissible: '',
	loadDueToRestrainedremarks: '',
	changeOfMaterialImpactloadInKnPermissible: '',
	changeOfMaterialImpactremarks: '',

	blastLoadloadInKnPermissible: '',
	blastLoadremarks: '',

	combinationOfLoadloadInKnPermissible: '',
	combinationOfLoadremarks: '',

	//table 3

	crossWallSystemObjects: '',
	crossWallSystemRemarks: '',
	longWallSystemObjects: '',
	longWallSystemRemarks: '',
	twoWaySystemObjects: '',
	twoWaySystemRemarks: '',

	openCoreObjects: '',
	openCoreRemarks: '',
	singleCoreObjects: '',
	singleCoreRemarks: '',

	locationOfCoreObjects: '',
	locationOfCoreRemarks: '',
	singleCoreWithLinearArrangementObjects: '',
	singleCoreWithLinearArrangementRemarks: '',

	exteriorCoreObjects: '',
	exteriorCoreRemarks: '',
	interiorCoreObjects: '',
	interiorCoreRemarks: '',
	eccentricCoreObjects: '',
	eccentricCoreRemarks: '',

	singleCoreNumberObjects: '',
	singleCoreNumberRemarks: '',

	splitCoreObjects: '',
	splitCoreRemarks: '',
	multipleCoreObjects: '',
	multipleCoreRemarks: '',

	closedFormObjects: '',
	closedFormRemarks: '',

	openFormObjects: '',
	openFormRemarks: '',
	commentIfAnyTextArea: '',
};
