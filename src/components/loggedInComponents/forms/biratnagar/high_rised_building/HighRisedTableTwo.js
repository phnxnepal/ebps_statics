import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { highRisedBuilding } from '../../../../../utils/data/HighRisedBuildingData';
import { TableInputIm } from '../../../../shared/TableInput';
const reqData = highRisedBuilding.table2;
export class HighRisedTableTwo extends Component {
	render() {
		return (
			<div>
				<Table className="structure-ui-table" columns={3}>
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={4}>{reqData.loadAction}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.loadInKnPermissible}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{/* ===== */}
						<Table.Row>
							<Table.Cell width={4}>{reqData.dead}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="deadloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="deadremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.live}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="liveloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="liveremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.earthquake}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="earthquakeloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="earthquakeremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.wind}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="windloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="windremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.construction}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="constructionloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="constructionremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.seismicLoadingWater}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="seismicLoadingWaterloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="seismicLoadingWaterremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.loadDueToRestrained}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="loadDueToRestrainedloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="loadDueToRestrainedremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.changeOfMaterialImpact}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="changeOfMaterialImpactloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="changeOfMaterialImpactremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.blastLoad}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="blastLoadloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="blastLoadremarks" />
							</Table.Cell>
						</Table.Row>

						<Table.Row>
							<Table.Cell width={4}>{reqData.combinationOfLoad}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="combinationOfLoadloadInKnPermissible" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="combinationOfLoadremarks" />
							</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</div>
		);
	}
}

export default HighRisedTableTwo;
