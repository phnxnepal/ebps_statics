import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { highRisedBuilding } from '../../../../../utils/data/HighRisedBuildingData';
import { TableInputIm } from '../../../../shared/TableInput';
const reqData = highRisedBuilding.table1;
export class HighRisedTableOne extends Component {
	render() {
		return (
			<div>
				<Table className="structure-ui-table ">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell width={1}>{reqData.sn}</Table.HeaderCell>
							<Table.HeaderCell width={3}>{reqData.description}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.iee}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.eia}</Table.HeaderCell>
							<Table.HeaderCell width={2}>{reqData.remarks}</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell width={1}>1</Table.Cell>
							<Table.Cell width={3}>{reqData.lessThan5000}</Table.Cell>
							<Table.Cell width={2}>{reqData.na}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="lessThan5000eia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="lessThan5000Remarks" />
							</Table.Cell>
						</Table.Row>
						{/* second */}
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.fiveThousand}</Table.Cell>
							<Table.Cell width={2}>{reqData.applicable}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="fiveThousandeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="fiveThousandRemarks" />
							</Table.Cell>
						</Table.Row>
						{/* third */}
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.greaterThan10000}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greaterThan10000iee" />
							</Table.Cell>
							<Table.Cell width={2}>{reqData.applicable}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greaterThan10000Remarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>2</Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqData.landArea}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.lessThan1Hectare}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="lessThan1Hectareiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="lessThan1Hectareeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="lessThan1HectareRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.OneHectare4Hectare}</Table.Cell>
							<Table.Cell width={2}>{reqData.applicable}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="OneHectare4Hectareeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="OneHectare4HectareRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.greaterThan4Hectare}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greaterThan4Hectareiee" />
							</Table.Cell>
							<Table.Cell width={2}>{reqData.applicable}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greaterThan4HectareRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}>3</Table.Cell>
							<Table.Cell colSpan="4">
								<b>{reqData.approachRoad}</b>
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.front}</Table.Cell>
							<Table.Cell width={2}>{reqData.sixMeter}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="fronteia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="frontRemarks" />
							</Table.Cell>
						</Table.Row>
						{/* ------- */}
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.side}</Table.Cell>
							<Table.Cell width={2}>{reqData.threeMeter}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="sideeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="sideRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.builtUpArea}</Table.Cell>
							<Table.Cell width={2}>{reqData.fiftyPercent}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="builtUpAreaeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="builtUpAreaRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.openArea}</Table.Cell>
							<Table.Cell width={2}>{reqData.thirtyPercent}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="openAreaeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="openAreaRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.greeneryArea}</Table.Cell>
							<Table.Cell width={2}>{reqData.twentyPercent}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greeneryAreaeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="greeneryAreaRemarks" />
							</Table.Cell>
						</Table.Row>
						{/* ========== */}
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.motorPark}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="motorParkiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="motorParkeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="motorParkRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.heightAndWidthOfWallCoreBuilding}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="heightAndWidthOfWallCoreBuildingiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="heightAndWidthOfWallCoreBuildingeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="heightAndWidthOfWallCoreBuildingRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.calculatedEarthquakeLateralLoad}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="calculatedEarthquakeLateralLoadiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="calculatedEarthquakeLateralLoadeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="calculatedEarthquakeLateralLoadRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.typeOfFoundationProvided}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="typeOfFoundationProvidediee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="typeOfFoundationProvidedeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="typeOfFoundationProvidedRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.soilTestProcedureSystem}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="soilTestProcedureSystemiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="soilTestProcedureSystemeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="soilTestProcedureSystemRemarks" />
							</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell width={1}></Table.Cell>
							<Table.Cell width={3}>{reqData.impactAndDynamicLoad}</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="impactAndDynamicLoadiee" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="impactAndDynamicLoadeia" />
							</Table.Cell>
							<Table.Cell width={2}>
								<TableInputIm name="impactAndDynamicLoadRemarks" />
							</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</div>
		);
	}
}

export default HighRisedTableOne;
