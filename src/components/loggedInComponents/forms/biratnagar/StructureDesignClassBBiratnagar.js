import React, { Component } from 'react';
import FormContainerV2 from '../../../../containers/base/FormContainerV2';
import { ApiParam } from '../../../../utils/paramUtil';
import api from '../../../../utils/api';
import { Formik } from 'formik';
import { Form, Table } from 'semantic-ui-react';
import SectionHeaderOfTheForm from './structureDesignBComponents/SectionHeaderOfTheForm';
import TableHeaderForForm from './structureDesignBComponents/TableHeader';
import GeneralTable from './structureDesignBComponents/GeneralTable';
import { initialValue } from './structureDesignBComponents/initivalValues';
import RequirementOfNepalNationalBuildingTable21 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable21';
import RequirementOfNepalNationalBuildingTable22 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable22';
import RequirementOfNepalNationalBuildingTable23 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable23';
import RequirementOfNepalNationalBuildingTable24 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable24';
import RequirementOfNepalNationalBuildingTable25 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable25';
import RequirementOfNepalNationalBuildingTable26 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable26';
import RequirementOfNepalNationalBuildingTable27 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable27';
import RequirementOfNepalNationalBuildingTable28 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable28';
import RequirementOfNepalNationalBuildingTable29 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable29';
import RequirementOfNepalNationalBuildingTable215 from './structureDesignBComponents/RequirementOfNepalNationalBuildingTable215';
import StructuralDataForFrameStructure211 from './structureDesignBComponents/StructuralDataForFrameStructure211';
import StructuralDataForLoadBearingWallStructure210 from './structureDesignBComponents/StructuralDataForLoadBearingWallStructure210';
import StructuralDataForOtherTypesOfStructures from './structureDesignBComponents/StructuralDataForOtherTypesOfStructures212';
import { SaveButtonValidation } from '../../../shared/SaveButtonValidation';
import { checkError, handleSuccess, prepareMultiInitialValues, getJsonData } from '../../../../utils/dataUtils';
import { FloorBlockArray } from '../../../../utils/floorUtils';
import { structureDesignClassBBiratnagarRequiredData } from '../../../../../src/utils/data/StructureDesignClassBBiratnagarData';
import ErrorDisplay from '../../../shared/ErrorDisplay';
import { PrintIdentifiers } from '../../../../utils/printUtils';

class StructureDesignClassBBiratnagarComponent extends Component {
	constructor(props) {
		super(props);
		const desApprovJsonData = this.props.otherData.designApproval;
		let buildingClass = structureDesignClassBBiratnagarRequiredData.generalTable.classOptions.find(
			(value) => value === desApprovJsonData.buildingClass
		);

		const { permitData, prevData } = this.props;
		const floorArray = new FloorBlockArray(permitData.floor);
		const topFloor = floorArray.getTopFloor();
		let numberOfStoreyValue = {};
		let sumOfHeights = {};
		if (floorArray.hasBlocks) {
			Object.entries(topFloor).forEach(([block, row], index) => (numberOfStoreyValue[index] = row.englishCount));
			Object.entries(floorArray.getSumOfHeights()).forEach(([block, row], index) => (sumOfHeights[index] = row));
		} else {
			numberOfStoreyValue = topFloor.englishCount;
			sumOfHeights = floorArray.getSumOfHeights();
		}

		const initialValues = prepareMultiInitialValues(
			{
				obj: initialValue,
				reqFields: [],
			},
			{
				obj: {
					numberOfStoreyAsPerSubmittedDesign: numberOfStoreyValue,
					categoryOfBuilding: buildingClass,
					totalHeightOfStructureAsPerSubmittedDesign: sumOfHeights,
					// propOccpuType: propOccpuName[0].value,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(prevData),
				reqFields: [], // {}
			}
		);
		this.state = {
			initialValues,
		};
	}
	render() {
		const { formUrl, permitData, postAction, prevData, hasSavePermission, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<div className="english-div">
				<Formik
					initialValues={this.state.initialValues}
					onSubmit={async (values, { setSubmitting }) => {
						setSubmitting(true);

						values.applicationNo = permitData.applicantNo;

						try {
							await postAction(api.structureDesignB, values, true);

							window.scroll(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
				>
					{({ isSubmitting, values, handleChange, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting}>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								{/* header */}
								<SectionHeaderOfTheForm />
								<Table columns="6" className="structure-ui-table">
									{/* table Header */}
									<TableHeaderForForm />
									{/* general Table */}
									<GeneralTable values={values} />
									{/* Requirement of Nepal National Building CODE (NBC) */}
									{/* table 2.1 */}
									<RequirementOfNepalNationalBuildingTable21 />
									{/* table 2.2 */}
									<RequirementOfNepalNationalBuildingTable22 />
									{/* table 2.3 */}
									<RequirementOfNepalNationalBuildingTable23 />
									{/* table 2.4 */}
									<RequirementOfNepalNationalBuildingTable24 value={values} setFieldValue={setFieldValue} />
									{/* table 2.5 */}
									<RequirementOfNepalNationalBuildingTable25 />
									{/* table 2.6 */}
									<RequirementOfNepalNationalBuildingTable26 />
									{/* table 2.7*/}
									<RequirementOfNepalNationalBuildingTable27 />
									{/* table 2.8*/}
									<RequirementOfNepalNationalBuildingTable28 />
									{/* table 2.9*/}
									<RequirementOfNepalNationalBuildingTable29 />
									{/* table 2.15*/}
									<RequirementOfNepalNationalBuildingTable215 />
								</Table>
								{values.structuralSystem === 'Frame' ? (
									<div>
										{/* table 2.11*/}
										<StructuralDataForFrameStructure211 values={values} />
									</div>
								) : values.structuralSystem === 'Load Bearing' ? (
									<div>
										{/* table 2.10*/}
										<StructuralDataForLoadBearingWallStructure210 />
									</div>
								) : (
									<StructuralDataForOtherTypesOfStructures values={values} />
								)}

								<br />
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}
const StructureDesignClassBBiratnagar = (parentProps) => (
	<FormContainerV2
		api={[new ApiParam(api.structureDesignB).setForm().getParams(), new ApiParam(api.designApproval, 'designApproval').getParams()]}
		onBeforeGetContent={{
			param1: [PrintIdentifiers.CHECKBOX_LABEL],
			param2: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <StructureDesignClassBBiratnagarComponent {...props} parentProps={parentProps} />}
	/>
);
export default StructureDesignClassBBiratnagar;
