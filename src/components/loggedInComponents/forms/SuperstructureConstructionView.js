import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { superstructureconstructionview } from '../../../utils/data/mockLangFile';
import { getMessage } from '../../shared/getMessage';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError, FloorArray } from '../../../utils/dataUtils';
import { SundarSuperStruConsSchema } from '../formValidationSchemas/superStructureConstructionValidation';
import { InaruwaSchema } from '../formValidationSchemas/superStructureConstructionValidation';
import { LetterHeadFlex, LetterHeadPhone } from '../../shared/LetterHead';
import { getConstructionTypeValue } from '../../../utils/enums/constructionType';
import { PatraSankhyaAndDate } from './formComponents/PatraSankhyaAndDate';
// import { Subject } from './formComponents/Subject';
import {
	SuperStructConsBody,
	SuperStructConstBodyLastSection,
	SuperStructConsBodySundarHaraicha,
} from './ijajatPatraComponents/SuperStructureConsComponents';
import { FooterSignature, FooterSignatureMultiline } from './formComponents/FooterSignature';
import { PrintIdentifiers, PrintParams } from '../../../utils/printUtils';
import { isSundarHaraicha, isMechi } from '../../../utils/clientUtils';
import { isInaruwa } from '../../../utils/clientUtils';
import { SectionHeader } from '../../uiComponents/Headers';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../shared/SaveButtonValidation';
import { getApproveByObject, getSaveByUserDetails } from '../../../utils/formUtils';
import { mechiPhone } from '../../../utils/data/faxPhoneLang';
import { ConstructionRulesSection } from '../../shared/formComponents/ConstructionRulesSection';

const sscv_data = superstructureconstructionview.superstructureconstructionview_data;
const footerArray = superstructureconstructionview.superstructureconstructionview_data.footerArray;

// const permitLang = buildingPermitApplicationForm.permitApplicationFormView;

// const mapTechLang = mapTechnical.mapTechnicalDescription;

// const buildPermitLang = buildingPermitApplicationForm.permitApplicationFormView;

const messageId = 'superstructureconstructionview.superstructureconstructionview_data';
// const areaUnitOptions = [
// 	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
// 	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
// ];

// const distanceOptions = [
// 	{ key: 1, value: 'METRE', text: 'मिटर' },
// 	{ key: 2, value: 'FEET', text: 'फिट' },
// ];

// export const landAreaTypeOptions = [
// 	{ key: 1, value: 'रोपनी–आना–पैसा–दाम', text: 'रोपनी–आना–पैसा–दाम' },
// 	{ key: 2, value: 'बिघा–कठ्ठा–धुर', text: 'बिघा–कठ्ठा–धुर' },
// 	{ key: 3, value: 'वर्ग फिट', text: 'वर्ग फिट' },
// 	{ key: 4, value: 'वर्ग मिटर ', text: 'वर्ग मिटर' },
// ];

class SuperstructureConstructionViewComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, prevData, otherData, enterByUser, userData, DEFAULT_UNIT_AREA, DEFAULT_UNIT_LENGTH } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const sipharisData = getApproveBy(0);
		const swikritData = getApproveBy(1);
		const pramukhData = getApproveBy(2);
		const jsonData = getJsonData(prevData);
		const mapTech = getJsonData(otherData.mapTech);
		const rajaswo = getJsonData(otherData.rajaswo);
		// const suikritTalla = this.props.otherData.anusuchiGha.tallaThap;

		// const sadakAdhikar = () => _.max(otherJsonData.sadakAdhikarKshytra);

		// const setbackMaptech = () => {
		// 	if (otherJsonData.setBack) {
		// 		return Math.max.apply(
		// 			Math,
		// 			otherJsonData.setBack.map(value => value.distanceFromRoadCenter)
		// 		);
		// 	}
		// 	return null;
		// };

		const desApprovJsonData = otherData.designApproval;
		const anuSucKaJsonData = otherData.anukaMaster;
		const suikritTalla = otherData.anusuchiGha.thapTalla;
		// console.log('talla---', suikritTalla);

		const allowance = getJsonData(otherData.allowance);
		const noteorder = getJsonData(otherData.noteorder);
		let buildingClass = anuSucKaJsonData.find((value) => value.id === desApprovJsonData.buildingClass);

		const floorArray = new FloorArray(permitData.floor);
		const floorMax = floorArray.getTopFloor().nepaliCount;

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					// plinthDetailsUnit: 'METRE',
					// buildCoverAreaUnit: 'METRE',
					// coverageDetailsUnit: 'METRE',
					buildingAreaUnit: DEFAULT_UNIT_AREA,
					coverageAreaUnit: DEFAULT_UNIT_AREA,
					houseLengthUnit: DEFAULT_UNIT_LENGTH,
					houseBreadthUnit: DEFAULT_UNIT_LENGTH,
					houseHeightUnit: DEFAULT_UNIT_LENGTH,
					constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					plinthDetailsUnit: DEFAULT_UNIT_AREA,
					buildCoverAreaUnit: DEFAULT_UNIT_AREA,
					coverageDetailsUnit: DEFAULT_UNIT_AREA,
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					publicPropertyUnit: DEFAULT_UNIT_LENGTH,
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['constructionType', 'purposeOfConstruction', 'landArea', 'landAreaType'] },
			{
				obj: mapTech,
				reqFields: ['approveDate', 'plinthDetails', 'plinthDetailsUnit', 'roof', 'coverageDetails', 'buildingHeight', 'allowableHeight'],
			},
			{ obj: rajaswo, reqFields: ['allowableHeight', 'constructionHeight', 'constructionHeightUnit'] },
			// {
			// 	obj: {
			// 		setbackMetre: setbackMaptech(),
			// 		sadakMetre: sadakAdhikar()

			// 	}, reqFields: []
			// }
			{
				obj: allowance,
				reqFields: ['allowanceDate', 'gharNo', 'publicPropertyDistance', 'publicPropertyUnit', 'sadakAdhikarUnit', 'requiredDistance'],
			},
			{
				obj: noteorder,
				reqFields: ['noteOrderDate'],
			},
			{
				obj: jsonData,
				reqFields: [],
			},
			{
				obj: {
					sipharisSignature: sipharisData.signature,
					swikritSignature: swikritData.signature,
					pramukhSignature: pramukhData.signature,
				},
				reqFields: [],
			}
		);

		if (isStringEmpty(initialValues.supStrucDate)) {
			initialValues.supStrucDate = getCurrentDate(true);
		}

		initialValues.constructionType = getConstructionTypeValue(initialValues.constructionType);
		initialValues.permissionOrderDate = initialValues.noteOrderDate;
		this.state = {
			isSucess: false,
			initialValues,
			suikritTalla,
			buildingClass,
			floorMax,
		};
	}
	// state = {
	// 	isSucess: false,
	// };

	render() {
		const {
			prevData,
			permitData,
			userData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;
		const { initialValues, suikritTalla, buildingClass, floorMax } = this.state;
		const user_info = this.props.userData;
		return (
			// <div className='superStruConsView'>

			<Formik
				initialValues={initialValues}
				validationSchema={isInaruwa ? InaruwaSchema : SundarSuperStruConsSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;

					try {
						await this.props.postAction(`${api.superStructureConstruction}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(getNextUrl(this.props.parentProps.location.pathname))
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error in Structure Construction Save', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef} className="print-small-font">
							<div>
								{isMechi ? (
									<LetterHeadPhone userInfo={user_info} phoneData={mechiPhone} compact={true} />
								) : (
									<LetterHeadFlex userInfo={user_info} />
								)}
								<PatraSankhyaAndDate setFieldValue={setFieldValue} values={values} errors={errors} dateFieldName="supStrucDate" />
								{isMechi ? <hr></hr> : <br />}
								<SectionHeader>
									<h3 className="underline end-section">{[getMessage(`${messageId}.title`, sscv_data.title)]}</h3>
								</SectionHeader>

								{isSundarHaraicha ? (
									<SuperStructConsBodySundarHaraicha
										floorMax={floorMax}
										suikritTalla={suikritTalla}
										permitData={permitData}
										buildingClass={buildingClass}
										userData={userData}
										values={values}
										setFieldValue={setFieldValue}
										errors={errors}
									/>
								) : (
									<SuperStructConsBody
										floorMax={floorMax}
										suikritTalla={suikritTalla}
										permitData={permitData}
										buildingClass={buildingClass}
										userData={userData}
										values={values}
										setFieldValue={setFieldValue}
										errors={errors}
									/>
								)}

								{isSundarHaraicha ? (
									<>
										<FooterSignatureMultiline
											designations={sscv_data.newData.footerSignature}
											signatureImages={
												useSignatureImage && [
													values.subSignature,
													values.sipharisSignature,
													values.swikritSignature,
													values.pramukhSignature,
												]
											}
										/>
										<br />

										<div>{sscv_data.newData.data8}</div>
										<div>{sscv_data.newData.data9}</div>
										<div>{sscv_data.newData.data10}</div>
									</>
								) : (
									<>
										<SuperStructConstBodyLastSection setFieldValue={setFieldValue} values={values} errors={errors} />
										<FooterSignature
											designations={[sscv_data.footer.footer_1, sscv_data.footer.footer_2, sscv_data.footer.footer_3]}
											signatureImages={
												useSignatureImage && [values.sipharisSignature, values.swikritSignature, values.pramukhSignature]
											}
										/>
										{isInaruwa ? (
											<div>
												{sscv_data.footer.footer_4_1}
												{userData.organization.name}
												{sscv_data.footer.footer_4_2}
											</div>
										) : (
											<blockquote style={{ textAlign: 'center', fontWeight: 600 }}>{sscv_data.footer.footer_4}</blockquote>
										)}
										{isInaruwa ? (
											<div className="signature-div flex-item-space-between">
												{footerArray.map((designation, index) => (
													<div key={index} style={{ textAlign: 'center' }}>
														<p>
															<span className="ui input signature-placeholder" />
														</p>
														<p className="neighbours-para">{designation}</p>
													</div>
												))}
											</div>
										) : isMechi ? (
											<FooterSignature designations={[sscv_data.footer.footer_5, sscv_data.footer.footer_6]} />
										) : (
											<div>
												{sscv_data.footer.footer_5}
												<span className="ui input dashedForm-control " />
											</div>
										)}
									</>
								)}
								<br />
								<br />
								{isMechi ? (
									<div className="page-break-before">
										<ConstructionRulesSection />
									</div>
								) : null}
							</div>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
						{/* <SaveButton
							errors={errors}
							formUrl={this.props.parentProps.location.pathname}
							hasSavePermission={this.props.hasSavePermission}
							prevData={getJsonData(prevData)}
							handleSubmit={handleSubmit}
						/> */}
					</Form>
				)}
			</Formik>
		);
	}
}
const SuperstructureConstructionView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.superStructureConstruction,
				objName: 'supStruCons',
				form: true,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
			{
				api: api.anusuchiGha,
				objName: 'anusuchiGha',
				form: false,
			},
			{
				api: api.rajaswaEntry,
				objName: 'rajaswo',
				form: false,
			},
			{
				api: api.designApproval,
				objName: 'designApproval',
				form: false,
			},
			{
				api: api.anusuchiKaMaster,
				objName: 'anukaMaster',
				form: false,
				utility: true,
			},
			{
				api: api.plintLevelTechApplication,
				objName: 'plinthTechApp',
				form: false,
			},
			{
				api: api.allowancePaper,
				objName: 'allowance',
				form: false,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteorder',
				form: false,
			},
		]}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			//param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			...PrintParams.INLINE_FIELD,
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			// param7: ['getElementsByClassName', 'ui checkbox', 'value'],
			param7: [PrintIdentifiers.CHECKBOX_LABEL],
			param8: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
			//param9: ['getElementsByClassName', 'brackets', 'value'],
		}}
		render={(props) => <SuperstructureConstructionViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperstructureConstructionView;
