import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { DashedLangDateField } from './../../shared/DateField';
import { SuperStructureBuildData } from './../../../utils/data/SuperStructureBuildData';
import { showToast } from './../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import api from '../../../utils/api';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import * as Yup from 'yup';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { isBiratnagar, isInaruwa, isKageyshowri, isKamalamai } from '../../../utils/clientUtils';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { SignatureImage } from './formComponents/SignatureImage';

const Sdata = SuperStructureBuildData.structureDesign;

const SuperStructureSchema = Yup.object().shape(
	Object.assign({
		superStructureBuildDate: validateNullableNepaliDate,
		noteOrderDate: validateNullableNepaliDate,
	})
);

class SuperStructureBuildViewComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;

		const json_data = getJsonData(prevData);
		const noteOrder = getJsonData(otherData.noteOrder);
		initVal = prepareMultiInitialValues(
			{
				obj: noteOrder,
				reqFields: ['noteOrderDate'],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.superStructureBuildDate)) {
			initVal.superStructureBuildDate = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}

	render() {
		const {
			staticFiles,
			permitData,
			userData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			errors: reduxErrors,
		} = this.props;
		const user_info = this.props.userData;
		const { initVal } = this.state;
		return (
			<Formik
				initialValues={initVal}
				validationSchema={SuperStructureSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;
					try {
						await this.props.postAction(`${api.superStructureBuild}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.successFile) {
							showToast(this.props.success.data.message || 'File uploaded');
							this.setState(() => ({
								needToGet: true,
							}));
							// actions.resetForm({
							// 	imagePreviewUrl: [],
							// 	uploadFile: '',
							// });
							// document.getElementById(`file_${category.id}`).value = '';
						}
						if (this.props.success && this.props.success.success) {
							// showToast('Your data has been successfully');
							// this.props.parentProps.history.push(
							// 	getNextUrl(this.props.parentProps.location.pathname)
							// );
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleChange, handleSubmit, values, setFieldValue, errors, validateForm }) => (
					<Form loading={isSubmitting}>
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
						<div ref={this.props.setRef}>
							{isInaruwa || isKageyshowri || isBiratnagar ? (
								<LetterSalutation lines={[Sdata.salutationPramukh, userData.organization.name, userData.organization.address]} />
							) : !isKamalamai ?  (
								<LetterHeadFlex userInfo={user_info} />
							) : null}
							<div className="section-header margin-section">
								<h3 className="underline">{Sdata.subject}</h3>
							</div>
							<FlexSingleRight className="margin-section">
								<DashedLangDateField
									name="superStructureBuildDate"
									handleChange={handleChange}
									value={values.superStructureBuildDate}
									setFieldValue={setFieldValue}
									label={Sdata.date}
									inline={true}
								/>
							</FlexSingleRight>
							<p>{Sdata.main1}</p>
							<div className="no-margin-field margin-section">
								{Sdata.main_data1} {userData.organization.name}{' '}
								{/* <Field
                                type='text'
                                name='municipalLocattion'
                            {Sdata.main_data4}
                            <DashedLangDateField
                                name='municipalDate'
                                inline={true}
                                setFieldValue={setFieldValue}
                                value={values.municipalDate}
                                error={errors.municipalDate}
                                className='dashedForm-control'
                            /> */}
								{Sdata.main_data4}
								{/* {translateDate(permitData.applicantDateBS)} */}
								<DashedLangDateField
									name="noteOrderDate"
									setFieldValue={setFieldValue}
									value={values.noteOrderDate}
									error={errors.noteOrderDate}
									inline={true}
								/>
								{Sdata.main_data2.main_data2_1}
								{/* <Select placeholder='मैले' options={opt} /> */}
								{Sdata.main_data2}
								{Sdata.main_data2.main_data2_2}
								{Sdata.main_data2.main_data2_3}
								{/* <DashedLangDateField
                                    inline={true}
                                    label={Sdata.main_data4}
                                    name="tipandi-date"
                                    setFieldValue={setFieldValue}
                                    value={values["tipandi-date"]}
                                    error={errors["tipandi-date"]}
                                />
                        
                            {/* <Field
                                type='text'
                                name='municipalSabik'
                                className='dashedForm-control'
                            />{' '} */}
								{permitData.oldMunicipal}
								{Sdata.haal} {userData.organization.name} {Sdata.wardNo}
								{/* <Field
                                type='text'
                                name='wardNo'
                                className='dashedForm-control'
                            />{' '} */}
								{permitData.newWardNo}
								{Sdata.kittaNo}
								{/* <Field
                                type='text'
                                name='kitaNO'
                                className='dashedForm-control'
                            /> */}
								{permitData.kittaNo}
								{Sdata.area}
								{/* <Field
                                type='text'
                                name='area'
                                className='dashedForm-control'
                            />{' '} */}
								{permitData.landArea} {permitData.landAreaType}
								{Sdata.main_data3}
							</div>

							<FlexSingleRight>
								<div>{Sdata.writer}</div>
								<div>
									<SignatureImage label={Sdata.signature} value={staticFiles.ghardhaniSignature} showSignature={true} />
								</div>
								<div>
									{Sdata.name}
									{permitData.nibedakName}
								</div>
							</FlexSingleRight>
						</div>
						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}

const SuperStructureBuildView = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.superStructureBuild,
				objName: 'superStructBuild',
				form: true,
			},
			{
				api: api.noteorderPilengthLevel,
				objName: 'noteOrder',
				form: false,
			},
		]}
		prepareData={(data) => data}
		useInnerRef={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		render={(props) => <SuperStructureBuildViewComponent {...props} parentProps={parentProps} />}
	/>
);

export default SuperStructureBuildView;
