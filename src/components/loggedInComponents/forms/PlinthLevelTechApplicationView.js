import React, { Component } from 'react';
import { PlinthLevelTechLang } from '../../../utils/data/PilenthTechAppLang';
import { Formik, Form, Field } from 'formik';
import { Grid } from 'semantic-ui-react';
import { DashedLangDateField } from './../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, checkError, prepareMultiInitialValues, handleSuccess } from '../../../utils/dataUtils';
import * as Yup from 'yup';
// import { getUserTypeValue } from './../../../utils/functionUtils';
import { isStringEmpty } from './../../../utils/stringUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import { commonMessages } from '../../../utils/data/validationData';
import { validateNepaliDate } from '../../../utils/validationUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from './../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';

const PlinthTechLang = PlinthLevelTechLang;

const Schema = Yup.object().shape({
    letterSubmitDate: validateNepaliDate,
    mapdandaPalana: Yup.string().when('sadakPahilo', {
        is: val => val === 'छैन',
        then: Yup.string().required(commonMessages.required)
    }),
    setBackPlan: Yup.string().when('siteDosro', {
        is: val => val === 'छैन',
        then: Yup.string().required(commonMessages.required)
    }),
    groundCoverage: Yup.string().when('groundTesro', {
        is: val => val === 'छैन',
        then: Yup.string().required(commonMessages.required)
    }),
    buildingConstFollow: Yup.string().when('nepalChautho', {
        is: val => val === 'छैन',
        then: Yup.string().required(commonMessages.required)
    }),
});

class PlinthLevelTechApplicationViewComponent extends Component {
    constructor(props) {
        super(props);
        let initVal = {};
        const {
            prevData,
            enterByUser
        } = this.props;
        const json_data = getJsonData(prevData);
        let serInfo = {
            subName: '',
            subDesignation: ''
        };
        serInfo.subName = enterByUser.name;
        serInfo.subDesignation = enterByUser.designation;
        // if (getUserRole() === UserType.SUB_ENGINEER) {
        //     serInfo.subName = userData.userName;
        //     serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
        // }
        initVal = prepareMultiInitialValues(
            {
                obj: serInfo, reqFields: []
            },
            { obj: json_data, reqFields: [] }
        )
        if (isStringEmpty(initVal.letterSubmitDate)) {
            initVal.letterSubmitDate = getCurrentDate(true);
        }
        if (isStringEmpty(initVal.date)) {
            initVal.date = getCurrentDate(true);
        }
        this.state = {
            initVal,
        };

    }
    render() {
        const {
            permitData,
            userData,
            prevData,
            errors: reduxErrors,
            hasSavePermission,
            formUrl,
            hasDeletePermission,
            isSaveDisabled
        } = this.props;
        const pData = permitData;
        const uData = userData;
        const { initVal } = this.state;
        return (
            <>
                {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
                <div
                    className='pilenthLevelTechViewWrap'
                >
                    <Formik
                        initialValues={{
                            sadakPahilo: PlinthTechLang.radioBox[0],
                            siteDosro: PlinthTechLang.radioBox[0],
                            groundTesro: PlinthTechLang.radioBox[0],
                            nepalChautho: PlinthTechLang.radioBox[0],

                            ...initVal
                        }}
                        validationSchema={Schema}
                        onSubmit={async (values, actions) => {
                            actions.setSubmitting(true);
                            values.applicationNo = permitData.applicationNo;
                            values.error && delete values.error;

                            // console.log('log');
                            try {
                                await this.props.postAction(
                                    `${api.plintLevelTechApplication}${permitData.applicationNo}`,
                                    values
                                );
                                actions.setSubmitting(false);
                                window.scrollTo(0, 0);
                                if (
                                    this.props.success &&
                                    this.props.success.success
                                ) {
                                    // showToast(
                                    //     'Your data has been successfully'
                                    // );

                                    // setTimeout(() => {
                                    //     this.props.parentProps.history.push(
                                    //         getNextUrl(this.props.parentProps.location.pathname)
                                    //     );
                                    // }, 500);
                                    handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                                }
                            } catch (err) {
                                console.log('Error', err);
                                actions.setSubmitting(false);
                                window.scrollTo(0, 0);
                                // showToast('Something went wrong !!');
                            }
                        }}
                    >
                        {({
                            isSubmitting,
                            values,
                            handleChange,
                            handleSubmit,
                            errors,
                            setFieldValue,
                            validateForm
                        }) => (
                                <Form loading={isSubmitting} className='ui form'>
                                    <div ref={this.props.setRef}>
                                        <SectionHeader>
                                            <h3 className='underline'>
                                                <span>{PlinthTechLang.heading}</span>
                                            </h3>
                                            <h3 className='underline'>
                                                <span>{PlinthTechLang.heading2}</span>
                                            </h3>
                                        </SectionHeader>
                                        <h4 className='pramukhJiyu'>{PlinthTechLang.pramukh}</h4>
                                        <p>
                                            {PlinthTechLang.contentline1}{' '}
                                            {pData.applicantName}{' '}
                                            {PlinthTechLang.contentline2}{' '}
                                            {uData.organization.name}{' '}
                                            {PlinthTechLang.contentline3}{' '}
                                            {pData.newWardNo}{' '}
                                            {PlinthTechLang.contentline4}{' '}
                                            {pData.sadak}{' '}
                                            {PlinthTechLang.contentline5}
                                        </p>
                                        <ol>
                                            <li>
                                                {PlinthTechLang.listing1}
                                                {' '}{PlinthTechLang.radioBox.map(
                                                    (name, index) => (
                                                        <div
                                                            key={index}
                                                            className='ui radio checkbox'
                                                        >
                                                            <Field
                                                                type='radio'
                                                                name='sadakPahilo'
                                                                id={index}
                                                                defaultChecked={
                                                                    values.sadakPahilo ===
                                                                    name
                                                                }
                                                                value={name}
                                                                onClick={handleChange}
                                                            />
                                                            <label>{name}</label>
                                                        </div>
                                                    )
                                                )}
                                                <br />
                                                {values.sadakPahilo ===
                                                    PlinthTechLang.radioBox[1] && (
                                                        <DashedLangInput
                                                            name='mapdandaPalana'
                                                            className='dashedForm-control'
                                                            setFieldValue={setFieldValue}
                                                            value={values.mapdandaPalana}
                                                            error={errors.mapdandaPalana}
                                                            line='full-line'
                                                        />
                                                    )}
                                            </li>
                                            <li>
                                                {PlinthTechLang.listing2}
                                                {PlinthTechLang.radioBox.map(
                                                    (name, index) => (
                                                        <div
                                                            key={index}
                                                            className='ui radio checkbox'
                                                        >
                                                            <Field
                                                                type='radio'
                                                                name='siteDosro'
                                                                id={index}
                                                                defaultChecked={
                                                                    values.siteDosro ===
                                                                    name
                                                                }
                                                                value={name}
                                                                onClick={handleChange}
                                                            />
                                                            <label>{name}</label>
                                                        </div>
                                                    )
                                                )}
                                                <br />
                                                {values.siteDosro ===
                                                    PlinthTechLang.radioBox[1] && (
                                                        <DashedLangInput
                                                            name='setBackPlan'
                                                            className='dashedForm-control'
                                                            setFieldValue={setFieldValue}
                                                            value={values.setBackPlan}
                                                            error={errors.setBackPlan}
                                                            line='full-line'
                                                        />
                                                    )}
                                            </li>
                                            <li>
                                                {PlinthTechLang.listing3}
                                                {PlinthTechLang.radioBox.map(
                                                    (name, index) => (
                                                        <div
                                                            key={index}
                                                            className='ui radio checkbox'
                                                        >
                                                            <Field
                                                                type='radio'
                                                                name='groundTesro'
                                                                id={index}
                                                                defaultChecked={
                                                                    values.groundTesro ===
                                                                    name
                                                                }
                                                                value={name}
                                                                onClick={handleChange}
                                                            />
                                                            <label>{name}</label>
                                                        </div>
                                                    )
                                                )}
                                                <br />
                                                {values.groundTesro ===
                                                    PlinthTechLang.radioBox[1] && (
                                                        <DashedLangInput
                                                            name='groundCoverage'
                                                            className='dashedForm-control'
                                                            setFieldValue={setFieldValue}
                                                            value={values.groundCoverage}
                                                            error={errors.groundCoverage}
                                                            line='full-line'
                                                        />
                                                    )}
                                            </li>
                                            <li>
                                                {PlinthTechLang.listing4}
                                                <DashedLangInput
                                                    name='changesMade'
                                                    className='dashedForm-control'
                                                    setFieldValue={setFieldValue}
                                                    value={values.changesMade}
                                                    error={errors.changesMade}
                                                    line='full-line'
                                                />
                                            </li>
                                            <li>
                                                {PlinthTechLang.listing5}
                                                {PlinthTechLang.radioBox.map(
                                                    (name, index) => (
                                                        <div
                                                            key={index}
                                                            className='ui radio checkbox'
                                                        >
                                                            <Field
                                                                type='radio'
                                                                name='nepalChautho'
                                                                id={index}
                                                                defaultChecked={
                                                                    values.nepalChautho ===
                                                                    name
                                                                }
                                                                value={name}
                                                                onClick={handleChange}
                                                            />
                                                            <label>{name}</label>
                                                        </div>
                                                    )
                                                )}
                                                <br />
                                                {values.nepalChautho ===
                                                    PlinthTechLang.radioBox[1] && (
                                                        <DashedLangInput
                                                            name='buildingConstFollow'
                                                            className='dashedForm-control'
                                                            setFieldValue={setFieldValue}
                                                            value={values.buildingConstFollow}
                                                            error={errors.buildingConstFollow}
                                                            line='full-line'
                                                        />
                                                    )}
                                            </li>
                                        </ol>
                                        <p>
                                            {PlinthTechLang.footerLine1}{" "}
                                            <DashedLangDateField
                                                name='letterSubmitDate'
                                                inline={true}
                                                setFieldValue={setFieldValue}
                                                value={values.letterSubmitDate}
                                                error={errors.letterSubmitDate}
                                                className='dashedForm-control'
                                            />{" "}
                                            {PlinthTechLang.footerLine2}
                                        </p>
                                        <Grid columns={2}>
                                            <Grid.Row>
                                                <Grid.Column>
                                                    <div className='inline field'>
                                                        <label>
                                                            {PlinthTechLang.sign}
                                                        </label>

                                                        {/* <DashedLangInput
                                                                name='techPersonNameSign'
                                                                className='dashedForm-control'
                                                                setFieldValue={setFieldValue}
                                                                value={values.techPersonNameSign}
                                                                error={errors.techPersonNameSign}
                                                            />  */}
                                                        {" "}<span className="ui input dashedForm-control" />

                                                    </div>
                                                    <div className='inline field'>
                                                        <label>
                                                            {PlinthTechLang.techName}
                                                        </label>{" "}
                                                        <div className='ui input'>
                                                            <DashedLangInput
                                                                name='subName'
                                                                className='dashedForm-control'
                                                                setFieldValue={setFieldValue}
                                                                value={values.subName}
                                                                error={errors.subName}
                                                            // handleChange={handleChange}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className='inline field'>
                                                        <label>
                                                            {PlinthTechLang.orgName}
                                                        </label>{" "}
                                                        <div className='ui input'>
                                                            <DashedLangInput
                                                                name='orgName'
                                                                className='dashedForm-control'
                                                                setFieldValue={setFieldValue}
                                                                value={values.orgName}
                                                                error={errors.orgName}
                                                            />
                                                        </div>
                                                    </div>
                                                </Grid.Column>
                                                <Grid.Column>
                                                    {/* empty div for align */}
                                                    <div className='inline field'>
                                                        <span>
                                                            {" "}
                                                            <br />
                                                        </span>
                                                    </div>
                                                    <div className='inline field'>
                                                        <label>
                                                            {PlinthTechLang.position}
                                                        </label>{" "}
                                                        <div className='ui input'>
                                                            <DashedLangInput
                                                                name='subDesignation'
                                                                className='dashedForm-control'
                                                                setFieldValue={setFieldValue}
                                                                value={values.subDesignation}
                                                                error={errors.subDesignation}
                                                            />
                                                        </div>
                                                    </div>
                                                    <div className='inline field'>
                                                        <label>
                                                            {PlinthTechLang.date}
                                                        </label>{" "}
                                                        <div className='ui input'>
                                                            <DashedLangInput
                                                                name='date'
                                                                className='dashedForm-control'
                                                                setFieldValue={setFieldValue}
                                                                value={values.date}
                                                                error={errors.date}
                                                            />
                                                        </div>
                                                    </div>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </div>
                                    <SaveButtonValidation
                                        errors={errors}
                                        validateForm={validateForm}
                                        formUrl={formUrl}
                                        hasSavePermission={hasSavePermission}
                                        hasDeletePermission={hasDeletePermission}
                                        isSaveDisabled={isSaveDisabled}
                                        prevData={checkError(prevData)}
                                        handleSubmit={handleSubmit}
                                    />
                                    {/* <SaveButton
                                        errors={errors}
                                        formUrl={this.props.parentProps.location.pathname}
                                        hasSavePermission={this.props.hasSavePermission}
                                        prevData={checkError(prevData)}
                                        handleSubmit={handleSubmit}
                                    /> */}
                                    {/* {this.props.hasSavePermission && (
                                    <Button
                                        type='submit'
                                        disabled={isSubmitting}
                                        onClick={handleSubmit}
                                    >
                                        Save
                                    </Button>
                                )} */}
                                </Form>
                            )}
                    </Formik>
                </div>
            </>
        );
    }
}

const PlinthLevelTechApplicationView = parentProps => (
    <FormContainerV2
        api={[
            {
                api: api.plintLevelTechApplication,
                objName: 'plinthTechApp',
                form: true
            }
        ]}
        prepareData={data => data}
        parentProps={parentProps}
        useInnerRef={true}
        onBeforeGetContent={{
            param1: ["getElementsByClassName", "dashedForm-control", "value"],
            param2: ["getElementsByClassName", "ui checkbox", "value"],
            param3: ['getElementsByClassName', 'ui label', 'innerText'],
            param4: ['getElementsByTagName', 'input', 'value'],
            param7: [
                'getElementsByClassName',
                'equal width fields inline-group',
                'innerText'
            ],

        }}
        render={props => (
            <PlinthLevelTechApplicationViewComponent
                {...props}
                parentProps={parentProps}
            />
        )}
    />
);

export default PlinthLevelTechApplicationView;
