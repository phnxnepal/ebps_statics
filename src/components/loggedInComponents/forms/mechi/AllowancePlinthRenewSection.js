import React from 'react';
import { AllowancePaperData } from '../../../../utils/data/AllowancePaperData';
import { FlexSingleRight } from '../../../uiComponents/FlexDivs';
import { DashedLangInput } from '../../../shared/DashedFormInput';
import { SectionHeader } from '../../../uiComponents/Headers';
import { Table } from 'semantic-ui-react';
import { DashedLangDateField } from '../../../shared/DateField';

const data = AllowancePaperData.mechiData;
export const AllowancePlinthRenewSection = ({ setFieldValue, values, errors }) => {
	return (
		<div>
			<FlexSingleRight>
				{data.plinthLevelNo}{' '}
				<DashedLangInput setFieldValue={setFieldValue} name="plinthLevelNo" value={values.plinthLevelNo} error={errors.plinthLevelNo} />
			</FlexSingleRight>
			<br />
			<SectionHeader>
				<h3>{data.plinthTableHeader}</h3>
			</SectionHeader>
			<Table className="certificate-ui-table small-font-print-table">
				<Table.Header>
					{data.plinthTableColumns.map((obj) => (
						<Table.HeaderCell>{obj.label}</Table.HeaderCell>
					))}
				</Table.Header>
				<Table.Body>
					<Table.Row>
						{data.plinthTableColumns.map((obj) => (
							<Table.Cell className="top-margin-print-cell">
								{obj.type === 'signature' ? (
									<div>
										<span className="ui input signature-placeholder" />
									</div>
								) : obj.type === 'date' ? (
									<DashedLangDateField
										setFieldValue={setFieldValue}
										name={obj.field}
										value={values[obj.field]}
										error={errors[obj.field]}
									/>
								) : (
									<DashedLangInput
										setFieldValue={setFieldValue}
										name={obj.field}
										value={values[obj.field]}
										error={errors[obj.field]}
									/>
								)}
							</Table.Cell>
						))}
					</Table.Row>
				</Table.Body>
			</Table>
			<div>
				{data.checkedByEngineer}:
				<DashedLangInput
					setFieldValue={setFieldValue}
					name="checkedByEngineer"
					value={values.checkedByEngineer}
					error={errors.checkedByEngineer}
				/>
				<div>
					<DashedLangDateField
						label={data.date}
						setFieldValue={setFieldValue}
						name="checkedByDate"
						value={values.checkedByDate}
						error={errors.checkedByDate}
					/>
				</div>
			</div>
			<Table className="certificate-ui-table small-font-print-table">
				<Table.Header>
					{data.namsariTableColumns.map((obj) => (
						<Table.HeaderCell>{obj.label}</Table.HeaderCell>
					))}
				</Table.Header>
				<Table.Body>
					<Table.Row>
						{data.namsariTableColumns.map((obj) => (
							<Table.Cell className="top-margin-print-cell">
								{obj.type === 'signature' ? (
									<div>
										<span className="ui input signature-placeholder" />
									</div>
								) : obj.type === 'date' ? (
									<DashedLangDateField
										setFieldValue={setFieldValue}
										name={obj.field}
										value={values[obj.field]}
										error={errors[obj.field]}
									/>
								) : (
									<DashedLangInput
										setFieldValue={setFieldValue}
										name={obj.field}
										value={values[obj.field]}
										error={errors[obj.field]}
									/>
								)}
							</Table.Cell>
						))}
					</Table.Row>
				</Table.Body>
			</Table>
			<div>
				{data.checkedByEngineer}:
				<DashedLangInput
					setFieldValue={setFieldValue}
					name="checkedByEngineerNamsari"
					value={values.checkedByEngineerNamsari}
					error={errors.checkedByEngineerNamsari}
				/>
				<div>
					<DashedLangDateField
						label={data.date}
						setFieldValue={setFieldValue}
						name="checkedByDateNamsari"
						value={values.checkedByDateNamsari}
						error={errors.checkedByDateNamsari}
					/>
				</div>
			</div>
		</div>
	);
};
