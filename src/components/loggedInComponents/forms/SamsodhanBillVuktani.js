import React from 'react';
import api from '../../../utils/api';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { GenericBillVulktani } from './formComponents/GenericBillVulktani';
import { ApiParam } from '../../../utils/paramUtil';
import { isKamalamai } from '../../../utils/clientUtils';
import { checkError } from '../../../utils/dataUtils';

const apis = isKamalamai
	? [{ api: api.sansodhanVuktani, objName: 'sansodhanVuktani', form: true }, new ApiParam(api.sansodhanTippani, 'rajasowTippani').getParams()]
	: [{ api: api.sansodhanVuktani, objName: 'sansodhanVuktani', form: true }];

const SamsodhanVuktani = (parentProps) => (
	<FormContainerV2
		api={apis}
		prepareData={(data) => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		// hasFileView={true}
		render={
			(props) => (
				// props.hasSavePermission ? (
				<GenericBillVulktani
					{...props}
					parentProps={parentProps}
					api={api.sansodhanVuktani}
					titleKey="sansodhanBill"
					rajasowData={checkError(props.otherData.rajasowTippani)}
				/>
			)
			// ) : (
			// 	<div ref={props.setRef}>
			// 		<GenericApprovalFileView
			// 			fileCategories={props.fileCategories}
			// 			files={props.files}
			// 			url={FormUrlFull.SAMSODHAN_BILL_VUKTANI}
			// 			prevData={checkError(props.prevData)}
			// 		/>
			// 	</div>
			// )
		}
	/>
);

export default SamsodhanVuktani;
