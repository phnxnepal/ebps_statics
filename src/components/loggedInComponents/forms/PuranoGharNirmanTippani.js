import React, { Component } from 'react';
import { Formik } from 'formik';
import { KaryaSampana } from '../../../utils/data/certificateinstructiondata';
import { Form } from 'semantic-ui-react';
import { DashedLangDateField } from '../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { Select } from 'semantic-ui-react';
import api from '../../../utils/api';
import * as Yup from 'yup';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { getJsonData, handleSuccess, checkError } from '../../../utils/dataUtils';
// import JSONDataMultipleGetFormContainer from '../../../containers/base/JSONDataMultipleGetFormContainer';
import { prepareMultiInitialValues } from './../../../utils/dataUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { validateNepaliDate, validateNullableNepaliDate } from '../../../utils/validationUtils';
import { getDesignations, DetailedSignature } from './formComponents/FooterSignature';
import { PrintParams } from '../../../utils/printUtils';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { getApproveByObject, getSaveByUserDetails } from '../../../utils/formUtils';
import { footerInputSignature } from '../../../utils/data/genericFormData';
import { isBirtamod } from '../../../utils/clientUtils';

const ksamp = KaryaSampana.KaryaSampana_data;
const Options = [
	{ key: 'a1', value: 'अनुसार निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |', text: 'अनुसार निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |' },
	{
		key: 'a2',
		value: 'अनुसार सेटव्याक / सडक सेटव्याक नछोडेकोले प्रमाण सहित धरौटी रोक्का गर्नुपर्ने देखिन्छ |',
		text: 'अनुसार सेटव्याक / सडक सेटव्याक नछोडेकोले प्रमाण सहित धरौटी रोक्का गर्नुपर्ने देखिन्छ |',
	},
	{
		key: 'a3',
		value: ' नियमानुसार बनाएकोले नीजलाई निर्माण सम्पन्न प्रमाण-पत्र दिन मनासिब देखिएको हुँदा  निर्माणको लागि प्रेषित | ',
		text: ' नियमानुसार बनाएकोले नीजलाई निर्माण सम्पन्न प्रमाण-पत्र दिन मनासिब देखिएको हुँदा  निर्माणको लागि प्रेषित |',
	},
];

const birtamodOptions = [
	{ key: 'a1', value: 'अनुसार निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |', text: 'अनुसार निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |' },
	{
		key: 'a2',
		value: 'अनुसार सेटव्याक / सडक सेटव्याक नछोडेकोले प्रमाण सहित धरौटी रोक्का गर्नुपर्ने देखिन्छ |',
		text: 'अनुसार सेटव्याक / सडक सेटव्याक नछोडेकोले प्रमाण सहित धरौटी रोक्का गर्नुपर्ने देखिन्छ |',
	},
	{
		key: 'a3',
		value: ' नियमानुसार बनाएकोले नीजलाई निर्माण सम्पन्न प्रमाण-पत्र दिन मनासिब देखिएको हुँदा  निर्माणको लागि प्रेषित | ',
		text: ' नियमानुसार बनाएकोले नीजलाई निर्माण सम्पन्न प्रमाण-पत्र दिन मनासिब देखिएको हुँदा  निर्माणको लागि प्रेषित |',
	},
	{
		key: 'a4',
		value: 'स्विकृती लिएर मात्र भवन बनाउनु पर्नेमा सो नभएकोले नियमित गर्ने प्रयोजनको लागि दर्ता गरिएको ।',
		text: 'स्विकृती लिएर मात्र भवन बनाउनु पर्नेमा सो नभएकोले नियमित गर्ने प्रयोजनको लागि दर्ता गरिएको ।',
	},
];

const meroOpt = [
	{ key: 'a1', value: 'मेरो', text: 'मेरो' },
	{ key: 'a2', value: 'हाम्रो', text: 'हाम्रो' },
];
const prabhidikOpt = [
	{ key: 'a1', value: 'कन्सल्टेन्ट', text: 'कन्सल्टेन्ट' },
	{ key: 'a2', value: 'को ', text: 'को' },
];
const PuranoGharSchema = Yup.object().shape(
	Object.assign({
		certificateInstDate: validateNepaliDate,
		municipalDate: validateNepaliDate,
		certificatePeshDate: validateNullableNepaliDate,
		erDate: validateNullableNepaliDate,
		chiefDate: validateNullableNepaliDate,
	})
);
class PuranoGharNirmanTippaniComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData, enterByUser, userData } = this.props;

		const { getApproveBy } = getApproveByObject(this.props.getApproveByProps());
		const rujuData = getApproveBy(0);
		const swikritData = getApproveBy(1);

		const json_data = getJsonData(prevData);
		const SupSturData = getJsonData(otherData.superBuild);
		const mapTech = getJsonData(otherData.mapTech);
		// let serInfo = {
		// 	subName: '',
		// 	subDesignation: '',
		// };

		// serInfo.subName = enterByUser.name;
		// serInfo.subDesignation = enterByUser.designation;

		// if (getUserRole() === UserType.SUB_ENGINEER) {
		// 	serInfo.subName = userData.userName;
		// 	serInfo.subDesignation = getUserTypeValueNepali(userData.userType);
		// }

		// let { chiefInfo, erInfo } = getApprovalData(prevData, UserType.ADMIN, UserType.ENGINEER);

		initVal = prepareMultiInitialValues(
			{
				obj: SupSturData,
				reqFields: ['municipalDate'],
			},
			{
				obj: {
					consultant: 'कन्सल्टेन्ट',
					...getSaveByUserDetails(enterByUser, userData),
				},
				reqFields: [],
			},
			{
				obj: mapTech,
				reqFields: ['consultancyName', 'userName'],
			},
			// {
			// 	obj: serInfo,
			// 	reqFields: [],
			// },
			// { obj: { ...chiefInfo, ...erInfo }, reqFields: [] },
			{
				obj: {
					erName: rujuData.name,
					erDesignation: rujuData.designation,
					erSignature: rujuData.signature,
					erDate: rujuData.date,
					chiefName: swikritData.name,
					chiefDesignation: swikritData.designation,
					chiefSignature: swikritData.signature,
					chiefDate: swikritData.date,
				},
				reqFields: [],
			},
			/** @todo confirm this */
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.certificateInstDate)) {
			initVal.certificateInstDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.certificatePeshDate)) {
			initVal.certificatePeshDate = getCurrentDate(true);
		}

		this.state = {
			initVal,
			designations: getDesignations('certificatePeshDate'),
		};
	}
	render() {
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
		} = this.props;
		const { initVal, designations } = this.state;
		const pdata = permitData;
		const udata = userData;

		return (
			<div style={{ textAlign: 'justify' }}>
				<Formik
					initialValues={initVal}
					validationSchema={PuranoGharSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.puranoGharTippani}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully');
								// this.props.parentProps.history.push(
								//     getNextUrl(
								//         this.props.parentProps.location.pathname
								//     )
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={userData} />
									<FlexSingleRight>
										<DashedLangDateField
											name="certificateInstDate"
											setFieldValue={setFieldValue}
											error={errors.certificateInstDate}
											value={values.certificateInstDate}
											inline={true}
											compact={true}
											label={ksamp.date.date_1}
										/>
									</FlexSingleRight>
									<br />
									<div className="section-header margin-section">
										<h3 className="underline">{ksamp.subject.subject_1}</h3>
									</div>
									<div>
										<span>{udata.organization.name}</span>
										<span>{ksamp.data_1.data_1_2}</span>
										<span>{pdata.nibedakTol}</span>
										<span>{ksamp.data_1.data_1_3}</span>
										<span>{pdata.nibedakName}</span> <span>{ksamp.data_1.data_1_4}</span>{' '}
										<Select
											placeholder="मेरो"
											name="mero"
											options={meroOpt}
											defaultValue="मेरो"
											value={values.mero}
											onChange={(e, { value }) => setFieldValue('mero', value)}
										/>{' '}
										<span>{ksamp.data_1.data_1_8}</span>
										<span>{pdata.oldMunicipal}</span>
										<span>{ksamp.data_1.data_1_6}</span>
										<span>{pdata.newWardNo}</span>
										<span>{ksamp.data_1.data_1_7}</span> <span>{pdata.buildingJoinRoad}</span>{' '}
										{/* <span>{ksamp.data_1.data_1_8}</span> */}
										{/* <span>{udata.organization.name}</span>
										<span>{ksamp.data_1.data_1_10}</span>
										<span>{pdata.newWardNo}</span>
										<span>{ksamp.data_1.data_1_11}</span>{' '}
										<span>{pdata.sadak}</span>{' '} */}
										<span>{ksamp.data_1.data_1_12}</span>
										<span>{pdata.kittaNo}</span>
										<span>{ksamp.data_1.data_1_13}</span> <span>{pdata.landArea}</span> <span>{pdata.landAreaType}</span>
										<span>{ksamp.data_1.data_1_14}</span>{' '}
										<DashedLangInput
											name="ChetrafalMa"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.ChetrafalMa}
											error={errors.ChetrafalMa}
										/>
										<span>{ksamp.data_1.data_1_15}</span>{' '}
										<DashedLangDateField
											label={udata.enterDate}
											name="municipalDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.municipalDate}
											error={errors.municipalDate}
											className="dashedForm-control"
										/>{' '}
										<span>{ksamp.data_1.data_1_16}</span>
										<DashedLangInput
											name="Patralei"
											className="dashedForm-control"
											setFieldValue={setFieldValue}
											value={values.Patralei}
											error={errors.Patralei}
										/>{' '}
										<span>{ksamp.data_1.data_1_21}</span>{' '}
										{values.consultant === 'कन्सल्टेन्ट' && values.consultancyName
											? `${values.consultancyName}`
											: `${values.userName}`}{' '}
										<Select
											// placeholder="को"
											// defaultValue='को'
											name="consultant"
											options={prabhidikOpt}
											value={values.consultant}
											onChange={(e, { value }) => setFieldValue('consultant', value)}
										/>
										<span>{ksamp.data_1.data_1_22}</span>
										<Select
											// placeholder="निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |"
											defaultValue="अनुसार निजलाई नक्सा सुधार गर्न लगाउनु पर्ने देखिन्छ |"
											name="setBack"
											options={isBirtamod ? birtamodOptions : Options}
											value={values.setBack}
											onChange={(e, { value }) => setFieldValue('setBack', value)}
										/>
										{/* <span>{ksamp.data_1.data_1_20}</span> */}
									</div>
									<br />
									<br />
									<div className="signature-div flex-item-space-between">
										{designations.map((designation, index) => (
											<div key={index} style={{ textAlign: 'left' }}>
												<p>{designation.label}</p>
												<DetailedSignature
													setFieldValue={setFieldValue}
													values={values}
													errors={errors}
													designation={designation}
												>
													<DetailedSignature.Signature
														label={footerInputSignature.sahi}
														showSignature={useSignatureImage}
													/>
													<DetailedSignature.Name />
													<DetailedSignature.Designation />
													<DetailedSignature.Date />
												</DetailedSignature>
											</div>
										))}
									</div>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const PuranoGharNirmanTippani = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.puranoGharTippani,
				objName: 'certificateInst',
				form: true,
			},
			{
				api: api.superStructureBuild,
				objName: 'superBuild',
				form: false,
			},
			{
				api: api.mapTechnicalDescription,
				objName: 'mapTech',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			//param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			// param2: ['getElementsByTagName', 'span', 'innerText'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
			param8: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		render={(props) => <PuranoGharNirmanTippaniComponent {...props} parentProps={parentProps} />}
	/>
);

export default PuranoGharNirmanTippani;
