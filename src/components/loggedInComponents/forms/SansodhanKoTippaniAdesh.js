import React, { Component } from 'react';
import { Header, Form, Divider } from 'semantic-ui-react';
import { Formik } from 'formik';
import { SansodhanKoTippaniAdeshStructure } from '../../../utils/data/SansodhanKoTippaniAdeshData';
import api from '../../../utils/api';
import { getFileCatId } from '../../../utils/functionUtils';
import { prepareInitialValue, handleSuccess, checkError } from '../../../utils/dataUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';

import * as Yup from 'yup';
import { EbpsTextArea } from '../../shared/EbpsForm';

import { validateFileOptional } from '../../../utils/validationUtils';
import { LetterHeadFlex } from '../../shared/LetterHead';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { SectionHeader } from '../../uiComponents/Headers';

const sk = SansodhanKoTippaniAdeshStructure.StructureDesign;

const schema = Yup.object().shape({
	uploadFile: validateFileOptional,
});

class SansodhanKoTippaniComponent extends Component {
	constructor(props) {
		super(props);
		let jsonData = {};
		try {
			jsonData = JSON.parse(this.props.prevData.jsonData);
		} catch {
			jsonData = {};
		}
		const initialValues = prepareInitialValue({
			obj: jsonData,
			reqFields: [],
		});
		const { fileCatId } = getFileCatId(this.props.otherData.fileCategories, this.props.parentProps.location.pathname);
		this.state = {
			initialValues,
			fileCatId,
		};
	}
	render() {
		const { prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		const { initialValues, fileCatId } = this.state;
		const user_info = this.props.userData;
		return (
			<>
				<Formik
					initialValues={initialValues}
					validationSchema={schema}
					onSubmit={async (values, { setSubmitting }) => {
						values.applicationNo = this.props.permitData.applicantNo;
						const data = new FormData();

						const selectedFile = values.uploadFile;

						let isFileSelected = false;
						if (selectedFile) {
							for (var x = 0; x < selectedFile.length; x++) {
								data.append('file', selectedFile[x]);
							}
							data.append('fileType', fileCatId);
							data.append('applicationNo', this.props.permitData.applicantNo);
							isFileSelected = true;
							values.uploadFile && delete values.uploadFile;
						}

						try {
							await this.props.postAction(api.sansodhankoTippaniAdesh, values, api.fileUpload, data, isFileSelected);

							window.scroll(0, 0);

							if (this.props.success && this.props.success.success) {
								// showToast(
								//   this.props.success.data.message || 'Data saved successfully'
								// );

								// setTimeout(() => {
								//   this.props.parentProps.history.push(
								//     getNextUrl(this.props.parentProps.location.pathname)
								//   );
								// }, 2000);
								handleSuccess(checkError(prevData), this.props.parentProps, this.props.success);
								setSubmitting(false);
							}
							setSubmitting(false);
						} catch (err) {
							setSubmitting(false);
							console.log('Error', err);
						}
					}}
					render={({ values, isSubmitting, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form
							loading={isSubmitting}
							// className='NJ-right'
						>
							{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
							<div ref={this.props.setRef}>
								<div className="NJ-Main superStruConsView">
									<div className="tipandi-letterHead">
										<LetterHeadFlex userInfo={user_info} />
									</div>

									<SectionHeader>
										<h2 className="underline end-section">{sk.tip}</h2>
									</SectionHeader>

									<Header as="h3">
										<Divider horizontal>{sk.topic}</Divider>
									</Header>
									<p>
										<EbpsTextArea placeholder="निवेदन" name="nibedhan" setFieldValue={setFieldValue} value={values.nibedhan} />
									</p>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				/>
			</>
		);
	}
}

const SansodhanKoTippaniAdesh = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.sansodhankoTippaniAdesh,
				objName: 'sansodhankoTippaniAdesh',
				form: true,
			},
			{
				api: api.fileCategories,
				objName: 'fileCategories',
				form: false,
				utility: true,
			},
		]}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'textarea', 'value'],
			param2: ['getElementsByClassName', 'ui input', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		useInnerRef={true}
		hasFile={true}
		render={(props) => <SansodhanKoTippaniComponent {...props} parentProps={parentProps} />}
	/>
);

export default SansodhanKoTippaniAdesh;
