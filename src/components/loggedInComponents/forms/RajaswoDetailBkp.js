import React, { Component } from 'react';
import { Form, Table, Dropdown, Select, Input } from 'semantic-ui-react';
import { rajaswoFormLang } from '../../../utils/data/rajaswoDetailLang';
import { Formik, getIn } from 'formik';

//Redux
import { isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { calculateTotal, round } from '../../../utils/mathUtils';
import api from '../../../utils/api';
import JSONDataMultipleGetFormContainer from '../../../containers/base/JSONDataMultipleGetFormContainer';
import { isStringEmpty } from '../../../utils/stringUtils';
import { checkError, floorMappingFlat, prepareMultiInitialValues, getJsonData, handleSuccess, FloorArray } from '../../../utils/dataUtils';
import EbpsForm, { EbpsNumberForm } from '../../shared/EbpsForm';
import UnitDropdown from '../../shared/UnitDropdown';
import SaveButton from '../../shared/SaveButton';
import {
	validateNumber,
	validateNullableNormalNumber,
	validateMoreThanZeroNumber,
	validateMinDistanceField,
	// validateString,
	validateNullableNumber,
} from '../../../utils/validationUtils';
import * as Yup from 'yup';
import TableInput from '../../shared/TableInput';
import RajasowTableInput, {
	RajasowTable2Input,
	RajasowRateInput,
	RajasowDiscountPercent,
	RajasowUnitDropdownWithRelatedFields,
} from '../../shared/RajasowTableInput';
import { DashedMultiUnitLengthInput, DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from '../../shared/EbpsUnitLabelValue';
import { isKankai } from '../../../utils/clientUtils';
import { mapTechnical } from '../../../utils/data/mockLangFile';
import { DEFAULT_UNIT_LENGTH, DEFAULT_UNIT_AREA } from '../../../utils/constants';
// import { commonMessages } from '../../../utils/data/validationData';

const hasDharauti = details => {
	let hasValue = false;
	details.forEach(row => (hasValue = hasValue || row.depositAmount > 0 || row.depositRate > 0));
	return hasValue;
};

const numData = ['allowableHeight'];
// const numData = ['allowableHeight', 'constructionHeight', 'requiredDistance', 'namedMapdanda', 'namedAdhikar'];
const notreqData = [
	// 'namedMapdanda',
	'namedMeasurement',
	// 'namedAdhikar',
	'namedBorder',
	'namedDistance',
	// 'requiredDistance',
	'unnamedMapdanda',
	'unnamedMeasurement',
	'unnamedAdhikar',
	'unnamedBorder',
	'unnamedDistance',
	'constructionHeight',
	'requiredDistance',
	'namedMapdanda',
	'namedAdhikar',
];
// const detailsField = [
// 	'dArea',
// 	'depositAmount',
// 	'depositRate',
// 	'drAmount',
// 	'drRate',
// 	'floor',
// 	'jArea',
// 	'jType',
// 	'jrAmount',
// 	'jrRate',
// 	'totalAmount',
// ];

const detailsData = ['detailsTotal', 'anyeAmt', 'aminAmt', 'applicationAmt', 'totalAmt', 'formAmt'];

// const strData = ['roadName'];

const numSchema = numData.map(rows => {
	return {
		[rows]: validateNumber,
	};
});
const notreqSchema = notreqData.map(rows => {
	return {
		[rows]: validateNullableNormalNumber,
	};
});

const detailSchema = detailsData.map(rows => {
	return {
		[rows]: validateMoreThanZeroNumber,
	};
});

// const strSchema = strData.map(rows => {
// 	return {
// 		[rows]: validateString,
// 	};
// });

const rajaswoSchema = Yup.object().shape(
	Object.assign(...numSchema, ...notreqSchema, ...detailSchema, {
		floor: Yup.array().of(
			Yup.object().shape({
				length: validateMoreThanZeroNumber,
				width: validateMoreThanZeroNumber,

				area: validateMoreThanZeroNumber,
			})
		),
		details: Yup.array().of(
			Yup.object().shape({
				dArea: validateMoreThanZeroNumber,
				// drRate: validateNotLessThanOneNumber.required(),
				drAmount: validateMoreThanZeroNumber,
				depositRate: validateMoreThanZeroNumber,
				jDepositRate: validateMoreThanZeroNumber,
				depositAmount: validateMoreThanZeroNumber,
				jDepositAmount: validateMoreThanZeroNumber,
				jArea: validateMoreThanZeroNumber,
				jrAmount: validateMoreThanZeroNumber,
				jrRate: validateMoreThanZeroNumber,
			})
		),
		// sadakAdhikarKshytra: validateMinDistanceField('requiredDistance').required(commonMessages.required),
		sadakAdhikarKshytra: validateMinDistanceField('requiredDistance').nullable(),
	})
);

const rajaswoKankaiSchema = Yup.object().shape(
	Object.assign(...notreqSchema, ...detailSchema, {
		requiredDistance: validateNullableNumber,
		namedMapdanda: validateNullableNumber,
		namedAdhikar: validateNullableNumber,
		allowableHeight: validateNumber,
		constructionHeight: validateNumber,
		floor: Yup.array().of(
			Yup.object().shape({
				length: validateMoreThanZeroNumber,
				width: validateMoreThanZeroNumber,

				area: validateMoreThanZeroNumber,
			})
		),
		details: Yup.array().of(
			Yup.object().shape({
				dArea: validateMoreThanZeroNumber,
				// drRate: validateNotLessThanOneNumber.required(),
				drAmount: validateMoreThanZeroNumber,
				depositRate: validateMoreThanZeroNumber,
				jDepositRate: validateMoreThanZeroNumber,
				depositAmount: validateMoreThanZeroNumber,
				jDepositAmount: validateMoreThanZeroNumber,
				jArea: validateMoreThanZeroNumber,
				jrAmount: validateMoreThanZeroNumber,
				jrRate: validateMoreThanZeroNumber,
			})
		),
		sadakAdhikarKshytra: validateMinDistanceField('requiredDistance').nullable(),
	})
);

const areaUnitOptions = [
	{ key: 1, value: 'SQUARE METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'SQUARE FEET', text: 'वर्ग फिट' },
];

const distanceOptions = [
	{ key: 1, value: 'METRE', text: 'मिटर' },
	{ key: 2, value: 'FEET', text: 'फिट' },
];

const getFloorOptions = () => {
	let retFloor = [];
	Object.values(rajaswoForm.floorMapping)
		.splice(existingFloorsArr.length)
		.map(row => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));
	Object.values(rajaswoForm.floorMapping.otherFloors).map(row => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));
	return retFloor.filter(row => row.key !== undefined);
};

const getFloorOptionsDy = details => {
	let retFloor = [];

	const existingFloors = details.reduce((acc, next) => {
		acc.push(next.floor);
		return acc;
	}, []);

	floorMappingFlat.filter(fl => !existingFloors.includes(fl.floor)).map(row => retFloor.push({ key: row.floor, text: row.value, value: row.floor }));
	// Object.values(rajaswoForm.floorMapping.otherFloors).map(row =>
	// 	retFloor.push({ key: row.floor, text: row.value, value: row.floor })
	// );
	return retFloor.filter(row => row.key !== undefined);
};

const mapTech_data = mapTechnical.mapTechnicalDescription;
const rajaswoForm = rajaswoFormLang;

// const areaOptions = [
// 	{ key: 'वर्ग मिटर', text: 'वर्ग मिटर', value: 'वर्ग मिटर' },
// 	{ key: 'वर्ग फिट', text: 'वर्ग फिट', value: 'वर्ग फिट' },
// ];

const squareUnitOptions = [
	{ key: 1, value: 'METRE', text: 'वर्ग मिटर' },
	{ key: 2, value: 'FEET', text: 'वर्ग फिट' },
];

const jTypeOptions = [
	{ key: '1', text: 'जस्ता', value: 'जस्ता' },
	{ key: '2', text: 'अन्य', value: 'अन्य' },
];

const detailsField = [
	'dArea',
	'depositAmount',
	'jDepositAmount',
	'depositRate',
	'jDepositRate',
	'drAmount',
	'drRate',
	'floor',
	'jArea',
	'jType',
	'jrAmount',
	'jrRate',
	'totalAmount',
];

const initDetails = row =>
	detailsField.forEach(field => {
		if (isStringEmpty(row[field])) {
			row[field] = 0;
		}
	});

const getInitDetails = floor => {
	let row = {};
	detailsField.forEach(field => (row[field] = 0));
	row.floor = floor.floor;
	row.dArea = floor.area;
	return row;
};

const existingFloorsArr = Object.values(rajaswoForm.table_body);

const additionalFloorOptions = getFloorOptions();
const Opt = [
	{ key: 'a1', value: 'आवासिय क्षेत्र', text: 'आवासिय क्षेत्र' },
	{ key: 'a2', value: 'व्यापारिक क्षेत्र', text: 'व्यापारिक क्षेत्र' },
	{ key: 'a3', value: 'कृषि क्षेत्र', text: 'कृषि क्षेत्र' },
	{ key: 'd4', value: 'औद्योगिक क्षेत्र', text: 'औद्योगिक क्षेत्र' },
	{ key: 'a5', value: 'शैक्षिक क्षेत्र', text: 'शैक्षिक क्षेत्र' },
	{ key: 'a6', value: ' संस्थागत क्षेत्र', text: 'संस्थागत क्षेत्र' },
];

class RajaswoDetailViewComponent extends Component {
	constructor(props) {
		super(props);

		const prevData = getJsonData(props.prevData);
		this.state = {
			tableRows: [{}],
			floorOptions: isEmpty(prevData) ? additionalFloorOptions : getFloorOptionsDy(prevData.details),
			lastSelectedFloor: '',
			selectedFlorr: [],
		};
	}
	
	render() {
		// const { tableRows } = this.state;

		let initVal = {};
		// const details = [];
		// Object.keys(rajaswoForm.floorMapping)
		//   .splice(0, existingFloorsArr.length)
		//   .map(row => {
		//     console.log('row', row);
		//     details.push({ floor: rajaswoForm.floorMapping[row].floor });
		//   });

		// console.log('state floors', this.state);

		const { permitData, prevData } = this.props;
		const floorArray = new FloorArray(permitData.floor);

		// console.log('prevData', this.props.prevData);
		// console.log('initialValues la 	', initVal, initDetails({}));
		// if (!this.props.prevData.error) {
		initVal = getJsonData(prevData);
		if (isEmpty(initVal)) {
			initVal = {};
			initVal.totalAmt = 0;
			initVal.detailsTotal = 0;
			initVal.purposeOfConstruction = 'आवासिय क्षेत्र';
			initVal.details = [];
			floorArray.getFloorsWithUnit().forEach((row, index) => (initVal.details[index] = getInitDetails(row)));
		}
		// console.log('init val ', initVal);
		// const { rStatus, approveStatus } = initVal;
		initVal.details &&
			initVal.details.forEach((row, index) => {
				if (isEmpty(initVal.hasJasta)) {
					initVal.hasJasta = [];
				}

				initVal.hasJasta[index] = !!(row.jrAmount || row.jArea || row.jrRate);

				// 	row.floorText = Object.values(rajaswoForm.floorMappingFlat)[
				// 	row.floor
				// ].value
			});

		// Road set back
		const roadSetbackMaster = this.props.otherData.roadSetBack;
		let roadSetBackOption = [
			{
				value: mapTech_data.mapTech_23.defaultRoadOption,
				text: mapTech_data.mapTech_23.defaultRoadOption,
			},
		];
		roadSetbackMaster.forEach(row => row.status === 'Y' && roadSetBackOption.push({ value: row.roadName, text: row.roadName }));

		const initialValues = prepareMultiInitialValues(
			{
				obj: {
					sadakAdhikarUnit: DEFAULT_UNIT_LENGTH,
					unnamedUnit: DEFAULT_UNIT_LENGTH,
					jAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					dAreaUnit: `SQUARE ${floorArray.getFloorUnit()}`,
					// allowableHeightUnit: DEFAULT_UNIT_LENGTH,
					// constructionHeightUnit: DEFAULT_UNIT_LENGTH,
					// floorlengthUnit: DEFAULT_UNIT_LENGTH,
					// floorWidthUnit: DEFAULT_UNIT_LENGTH,
					// floorareaUnit: DEFAULT_UNIT_LENGTH,
					floorUnit: floorArray.getFloorUnit(),
					discountPercent: isKankai ? 5 : 0,
					roadName: roadSetBackOption[0].value,
				},
				reqFields: [],
			},
			{ obj: permitData, reqFields: ['floor', 'buildingJoinRoad'] },
			{
				obj: {
					constructionHeight: floorArray.getSumOfHeights(),
				},
				reqFields: [],
			},
			{ obj: initVal, reqFields: [] }
		);

		/**
		 * @todo remove later
		 */
		if (isEmpty(initialValues.jAreaUnit)) initialValues.jAreaUnit = DEFAULT_UNIT_AREA;
		if (isEmpty(initialValues.dAreaUnit)) initialValues.dAreaUnit = DEFAULT_UNIT_AREA;

		// let arrToConcat = [];

		// try {
		// 	arrToConcat = initVal.details
		// 		? Array(initVal.details.length - existingFloorsArr.length - 1).fill({})
		// 		: [];
		// } catch {
		// 	arrToConcat = [];
		// }
		// console.log('arrtoconcat', arrToConcat);
		// }

		return (
			<div>
				{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initialValues}
					validationSchema={isKankai ? rajaswoKankaiSchema : rajaswoSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						// console.log('rajaswoInputData====', values);

						values.applicationNo = permitData.applicantNo;
						//   values.enterBy = 'AAAA';
						//   values.enterDate = '2019-08-25';
						//   values.rEntery = '';
						//   values.rDate = '2019-08-25';
						//   values.rStatus = 'A';
						//   values.remark = '';

						values.details = values.details.filter(row => !isEmpty(row));
						values.details &&
							values.details.forEach((row, index) => {
								if (row) {
									initDetails(row);
									row.totalAmount = calculateTotal(row.drAmount, row.depositAmount, row.jrAmount);
									if (row.floorText) delete row.floorText;
								} else {
									// values.details[index] = {};
									// values.details[index].dArea = 0;
									// values.details[index].drAmount = 0;
									// values.details[index].drRate = 0;
								}
							});

						values.error && delete values.error;

						try {
							await this.props.postAction(api.rajaswaEntry, values, true);

							// console.log('response -- after post', response);
							window.scrollTo(0, 0);
							actions.setSubmitting(false);
							// console.log(this.props);

							if (this.props.success && this.props.success.success) {
								// showToast("Your data has been successfully");
								// this.props.parentProps.history.push(
								//   getNextUrl(this.props.parentProps.location.pathname)
								// );
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							console.log('error', err);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						}
					}}
					render={({ handleSubmit, isSubmitting, handleChange, values, setFieldValue, errors }) => {
						const hasDhrt = hasDharauti(values.details);
						const dharautiClass = hasDhrt ? undefined : 'remove-on-print';
						// console.log('rajaswa props', values);
						return (
							<Form loading={isSubmitting}>
								<div className="rajaswoDetailViewFormWrap" ref={this.props.setRef}>
									<br />
									<h2 className="rajaswoMain-heading">{rajaswoForm.heading}</h2>
									<p className="rajasoMain-desc">{rajaswoForm.sub_heading}</p>

									<div className="rajaswoForm-wrap">
										<br />
										{rajaswoForm.rajaswaFirstPara.content_line_1}
										<Select
											placeholder="आवासिय क्षेत्र"
											options={Opt}
											name="purposeOfConstruction"
											value={values.purposeOfConstruction}
											onChange={(e, { value }) => {
												setFieldValue('purposeOfConstruction', value);
											}}
										/>
										<br />
										<span>
											<DashedLengthInputWithRelatedUnits
												name="allowableHeight"
												unitName="floorUnit"
												relatedFields={['constructionHeight', ...floorArray.getAllFields()]}
												label={rajaswoForm.rajaswaFirstPara.content_line_2}
											/>
											<DashedLengthInputWithRelatedUnits
												name="constructionHeight"
												unitName="floorUnit"
												relatedFields={['allowableHeight', ...floorArray.getAllFields()]}
												label={rajaswoForm.rajaswaFirstPara.content_line_3}
											/>
											{/* <DashedUnitInput name="allowableHeight" unitName="allowableHeightUnit" label={rajaswoForm.rajaswaFirstPara.content_line_2} />
											<DashedUnitInput
												name="constructionHeight"
												unitName="constructionHeightUnit"
												label={rajaswoForm.rajaswaFirstPara.content_line_3}
											/> */}
										</span>
										<br />
										{floorArray.getFloors().map((floor, index) => (
											<div key={index}>
												{rajaswoForm.rajaswaFirstPara.prefix} {floorMappingFlat.find(fl => fl.floor === floor.floor).value}{' '}
												<DashedLengthInputWithRelatedUnits
													name={`floor.${index}.length`}
													unitName="floorUnit"
													relatedFields={[...floorArray.getAllFields(`floor.${index}.length`), 'constructionHeight', 'allowableHeight']}
													label={rajaswoForm.rajaswaFirstPara.length}
												/>
												<DashedMultiUnitLengthInput
													name={`floor.${index}.width`}
													unitName="floorUnit"
													relatedFields={[...floorArray.getAllFields(`floor.${index}.width`), 'constructionHeight', 'allowableHeight']}
													label={rajaswoForm.rajaswaFirstPara.width}
												/>
												<DashedAreaInputWithRelatedUnits
													name={`floor.${index}.area`}
													unitName="floorUnit"
													relatedFields={[...floorArray.getAllFields(`floor.${index}.area`), 'constructionHeight', 'allowableHeight']}
													label={rajaswoForm.rajaswaFirstPara.area}
													squareOptions={squareUnitOptions}
												/>
											</div>
										))}
										{/* <br /> */}
										<h3 className="section-header-with-bottom-border">
											<span>{rajaswoForm.rajaswo_table_1.header}</span>
										</h3>
										<Table celled className="table-negative-margin">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width="2">{rajaswoForm.rajaswo_table_1.table_head_1}</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_2}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_3}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_4}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_5} {'( '}
														<Dropdown
															name="sadakAdhikarUnit"
															onChange={(e, { value }) => {
																// surroundingMappingFlat.forEach((side, index) => {
																const reqRoad = roadSetbackMaster.find(road => road.roadName === getIn(values, `roadName`));
																if (reqRoad) {
																	if (value === 'METRE') {
																		setFieldValue(`requiredDistance`, reqRoad.setBackMeter);
																	} else {
																		setFieldValue(`requiredDistance`, reqRoad.setBackFoot);
																	}
																}
																// });

																setFieldValue('sadakAdhikarUnit', value);
															}}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
														{' ) '}
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_6}{' '}
														<UnitDropdown
															name="sadakAdhikarUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'sadakAdhikarUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>{rajaswoForm.rajaswo_table_1.table_head_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell>
														{/* <EbpsForm name="buildingJoinRoad" value={values.buildingJoinRoad} setFieldValue={setFieldValue} /> */}
														<Select
															options={roadSetBackOption}
															name={`roadName`}
															placeholder="Select an option"
															onChange={(e, { value }) => {
																setFieldValue(`roadName`, value);
																console.log(value);
																const reqRoad = roadSetbackMaster.find(road => road.roadName === value);
																if (reqRoad) {
																	if (values.sadakAdhikarUnit === 'METRE') {
																		setFieldValue(`sadakAdhikarKshytra`, reqRoad.setBackMeter);

																		setFieldValue(`requiredDistance`, reqRoad.setBackMeter);
																	} else {
																		setFieldValue(`sadakAdhikarKshytra`, reqRoad.setBackFoot);

																		setFieldValue(`requiredDistance`, reqRoad.setBackFoot);
																	}
																} else {
																	setFieldValue(`sadakAdhikarKshytra`, '');

																	setFieldValue(`requiredDistance`, '');
																}
																// console.log(listData.find(nadi => nadi.placeName === value));
																// console.log(nadiOptions);
															}}
															value={getIn(values, `roadName`)}
															error={!!errors.roadName}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedMapdanda"
															value={values.namedMapdanda}
															onChange={handleChange}
															error={errors.namedMapdanda}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedMeasurement"
															value={values.namedMeasurement}
															onChange={handleChange}
															error={errors.namedMeasurement}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="namedAdhikar"
															value={values.namedAdhikar}
															onChange={handleChange}
															error={errors.namedAdhikar}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														{getIn(values, 'roadName') === mapTech_data.mapTech_23.defaultRoadOption ? (
															<EbpsNumberForm
																name="requiredDistance"
																value={values.requiredDistance}
																onChange={handleChange}
																error={errors.requiredDistance}
																isTable={true}
															/>
														) : (
															getIn(values, `requiredDistance`) && `${getIn(values, `requiredDistance`)} `
														)}
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name={`sadakAdhikarKshytra`}
															value={getIn(values, `sadakAdhikarKshytra`)}
															onChange={handleChange}
															error={getIn(errors, `sadakAdhikarKshytra`)}
															isTable={true}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsForm name="namedRemarks" value={values.namedRemarks} setFieldValue={setFieldValue} />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										{/* <br /> */}
										{/* <h3
											style={{
												textAlign: 'center',
												textDecoration: 'underline',
											}}
										>
											{rajaswoForm.rajaswo_table_2.header}
										</h3> */}
										<h3 className="section-header-with-bottom-border">
											<span>{rajaswoForm.rajaswo_table_2.header}</span>
										</h3>
										<Table celled className="table-negative-margin">
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell width="2">{rajaswoForm.rajaswo_table_2.table_head_1}</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_2.table_head_2}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_3}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_4}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_2.table_head_3}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>
														{rajaswoForm.rajaswo_table_1.table_head_6}{' '}
														<UnitDropdown
															name="unnamedUnit"
															setFieldValue={setFieldValue}
															value={getIn(values, 'unnamedUnit')}
															options={distanceOptions}
														/>
													</Table.HeaderCell>
													<Table.HeaderCell>{rajaswoForm.rajaswo_table_1.table_head_7}</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												<Table.Row>
													<Table.Cell>
														<EbpsForm name="unnamedSadak" value={values.unnamedSadak} setFieldValue={setFieldValue} />
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedMapdanda"
															value={values.unnamedMapdanda}
															onChange={handleChange}
															error={errors.unnamedMapdanda}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedMeasurement"
															value={values.unnamedMeasurement}
															onChange={handleChange}
															error={errors.unnamedMeasurement}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedAdhikar"
															value={values.unnamedAdhikar}
															onChange={handleChange}
															error={errors.unnamedAdhikar}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm name="unnamedBorder" value={values.unnamedBorder} onChange={handleChange} error={errors.unnamedBorder} />
													</Table.Cell>
													<Table.Cell>
														<EbpsNumberForm
															name="unnamedDistance"
															value={values.unnamedDistance}
															onChange={handleChange}
															error={errors.unnamedDistance}
														/>
													</Table.Cell>
													<Table.Cell>
														<EbpsForm name="unnamedRemarks" value={values.unnamedRemarks} setFieldValue={setFieldValue} />
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
										<div>
											<h3>
												<span>{rajaswoForm.table_heading1}</span>
											</h3>
											<Table celled structured className="new">
												<Table.Header>
													<HeaderFirstRow dharautiClass={dharautiClass} />
													<Table.Row>
														{/* Dhalan */}
														<Table.HeaderCell rowSpan="2" width={3}>
															{rajaswoForm.table_head.thead_2}
															{/* {' ( '} */}
															{/* <Dropdown
																// defaultValue="वर्ग मिटर"
																options={areaUnitOptions}
																name="dAreaUnit"
																onChange={(e, { value }) => setFieldValue('dAreaUnit', value)}
																value={values['dAreaUnit']}
															/> */}
															{/* {' )'} */}
															<RajasowUnitDropdownWithRelatedFields
																options={areaUnitOptions}
																unitName="dAreaUnit"
																areaFields={['drAmount', 'jrAmount', 'depositAmount', 'jDepositAmount', 'detailsTotal']}
																relatedFields={[
																	'detailsTotal',
																	...floorArray.getAllCustomsFields('', 'details', [
																		'dArea',
																		'jArea',
																		'drAmount',
																		'jrAmount',
																		'depositAmount',
																		'jDepositAmount',
																	]),
																]}
															/>
														</Table.HeaderCell>
														<Table.HeaderCell className={dharautiClass} colSpan="2">
															{rajaswoForm.table_head.thead_3}
														</Table.HeaderCell>
														{/* Other */}
														{/* <Table.HeaderCell rowSpan="2" width={3}>
														{rajaswoForm.table_head.thead_4} {' ( '}
														<Dropdown
															// defaultValue="वर्ग मिटर"
															options={areaOptions}
															name="jAreaUnit"
															onChange={(e, { value }) =>
																setFieldValue('jAreaUnit', value)
															}
															value={values['jAreaUnit']}
														/>
														{' )'}
													</Table.HeaderCell> */}
														{/* <Table.HeaderCell colSpan="2">
														{rajaswoForm.table_head.thead_5}
													</Table.HeaderCell> */}
														{/* Dhazrauti */}
													</Table.Row>
													<HeaderThirdRow dharautiClass={dharautiClass} />
												</Table.Header>
												<Table.Body>
													{initVal.details
														// .filter(rw => rw.floor <= 2)
														.map((row, index) => {
															const isHeightRelevantFloor = row.floor < 11;
															return [
																<Table.Row key={index}>
																	<Table.Cell>
																		<div>{floorMappingFlat.find(fl => row.floor === fl.floor).value}</div>
																		{row.floor > 0 && (
																			<div
																				className="ui checkbox"
																				style={{
																					marginRight: 10,
																				}}
																			>
																				<input
																					type="checkbox"
																					name={`hasJasta.${index}`}
																					value={getIn(values, `hasJasta.${index}`)}
																					defaultChecked={getIn(values, `hasJasta.${index}`)}
																					onChange={e => {
																						setFieldValue(`details.${index}.jType`, jTypeOptions[0].value);
																						handleChange(e);
																					}}
																				/>
																				<label>{rajaswoForm.table_jasta}</label>
																			</div>
																		)}
																	</Table.Cell>
																	{/* Dhalan */}
																	<Table.Cell>
																		<RajasowTableInput
																			name={`details.${index}.dArea`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.drRate`}
																			amountFieldName={`details.${index}.drAmount`}
																			error={getIn(errors, `details.${index}.dArea`)}
																			floorFieldName={`details.${index}.floor`}
																			setFieldValue={setFieldValue}
																			floor={row.floor}
																		/>
																	</Table.Cell>
																	<Table.Cell>
																		<RajasowRateInput
																			name={`details.${index}.drRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.dArea`}
																			amountFieldName={`details.${index}.drAmount`}
																			error={getIn(errors, `details.${index}.drRate`)}
																			setFieldValue={setFieldValue}
																			isHeightRelevantFloor={isHeightRelevantFloor}
																		/>
																		{/* <TableInput
																	placeholder="0.00"
																	name={`details.${index}.drRate`}
																	value={getIn(values, `details.${index}.drRate`)}
																	onChange={e => {
																		setFieldValue(
																			`details.${index}.drRate`,
																			e.target.value
																		);
																		setFieldValue(
																			`details.${index}.drAmount`,
																			calculateAmount(
																				'dArea',
																				e.target.value,
																				index,
																				values,
																				'details'
																			),
																			false
																		);
																	}}
																	error={getIn(
																		errors,
																		`details.${index}.drRate`
																	)}
																/> */}
																	</Table.Cell>
																	<Table.Cell>
																		<TableInput
																			readOnly={true}
																			placeholder="0.0"
																			name={`details.${index}.drAmount`}
																			value={getIn(values, `details.${index}.drAmount`)}
																			error={getIn(errors, `details.${index}.drAmount`)}
																		/>
																	</Table.Cell>

																	{/* Dharauti */}
																	<Table.Cell className={dharautiClass}>
																		{/* <TableInput
																	placeholder="0.00"
																	name={`details.${index}.depositRate`}
																	value={getIn(values, `details.${index}.depositRate`)}
																	error={getIn(
																		errors,
																		`details.${index}.depositRate`
																	)}
																	onChange={handleChange}
																/> */}
																		<RajasowTableInput
																			name={`details.${index}.depositRate`}
																			index={index}
																			values={values}
																			mulFieldName={`details.${index}.dArea`}
																			amountFieldName={`details.${index}.depositAmount`}
																			error={getIn(errors, `details.${index}.depositRate`)}
																			setFieldValue={setFieldValue}
																		/>
																	</Table.Cell>
																	<Table.Cell className={dharautiClass}>
																		<TableInput
																			readOnly={true}
																			placeholder="0.00"
																			name={`details.${index}.depositAmount`}
																			value={getIn(values, `details.${index}.depositAmount`)}
																			error={getIn(errors, `details.${index}.depositAmount`)}
																			// onChange={handleChange}
																		/>
																	</Table.Cell>
																</Table.Row>,
																getIn(values, `hasJasta.${index}`) && (
																	<Table.Row>
																		<Table.Cell>
																			<Form.Field inline>
																				<span>{floorMappingFlat.find(fl => row.floor === fl.floor).value}</span> {/* <Label basic> */}
																				<Select
																					className="fit-content-select"
																					// defaultValue={jTypeOptions[0].text}
																					options={jTypeOptions}
																					name={`details.${index}.jType`}
																					onChange={(e, { value }) => setFieldValue(`details.${index}.jType`, value)}
																					value={getIn(values, `details.${index}.jType`)}
																				/>
																				{/* </Label> */}
																			</Form.Field>
																		</Table.Cell>
																		{/* Other */}
																		<Table.Cell>
																			<Form.Field>
																				<Input labelPosition="right">
																					{/* <TableInput
																				name={`details.${index}.jArea`}
																				value={getIn(values, `details.${index}.jArea`)}
																				onChange={e => {
																					setFieldValue(
																						`details.${index}.jArea`,
																						e.target.value
																					);
																					if (
																						!values[`details.${index}.floor`]
																					) {
																						setFieldValue(
																							`details.${index}.floor`,
																							row.floor
																						);
																					}
																					setFieldValue(
																						`details.${index}.jrAmount`,
																						calculateAmount(
																							'jrRate',
																							e.target.value,
																							index,
																							values,
																							'details'
																						),
																						false
																					);
																				}}
																				error={getIn(errors, `details.${index}.jArea`)}
																			/> */}
																					<RajasowTableInput
																						name={`details.${index}.jArea`}
																						index={index}
																						values={values}
																						mulFieldName={`details.${index}.jrRate`}
																						amountFieldName={`details.${index}.jrAmount`}
																						error={getIn(errors, `details.${index}.jArea`)}
																						floorFieldName={`details.${index}.floor`}
																						setFieldValue={setFieldValue}
																						floor={row.floor}
																					/>
																				</Input>
																			</Form.Field>
																		</Table.Cell>
																		<Table.Cell>
																			{/* <TableInput
																		placeholder="0.00"
																		name={`details.${index}.jrRate`}
																		value={getIn(values, `details.${index}.jrRate`)}
																		onChange={e => {
																			setFieldValue(
																				`details.${index}.jrRate`,
																				e.target.value
																			);
																			setFieldValue(
																				`details.${index}.jrAmount`,
																				calculateAmount(
																					'jArea',
																					e.target.value,
																					index,
																					values,
																					'details'
																				),
																				false
																			);
																		}}
																		error={getIn(errors, `details.${index}.jrRate`)}

																	/> */}
																			<RajasowRateInput
																				name={`details.${index}.jrRate`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jArea`}
																				amountFieldName={`details.${index}.jrAmount`}
																				error={getIn(errors, `details.${index}.jrRate`)}
																				setFieldValue={setFieldValue}
																				isHeightRelevantFloor={isHeightRelevantFloor}
																			/>
																		</Table.Cell>
																		<Table.Cell>
																			<TableInput
																				placeholder="0.00"
																				name={`details.${index}.jrAmount`}
																				error={getIn(errors, `details.${index}.jrAmount`)}
																				value={getIn(values, `details.${index}.jrAmount`)}
																				// onChange={handleChange}
																			/>
																		</Table.Cell>

																		{/* Dharauti */}
																		<Table.Cell className={dharautiClass}>
																			{/* <TableInput
																		placeholder="0.00"
																		name={`details.${index}.jDepositRate`}
																		error={getIn(errors, `details.${index}.jDepositRate`)}
																		value={getIn(values, `details.${index}.jDepositRate`)}
																		onChange={handleChange}
																	/> */}
																			<RajasowTableInput
																				name={`details.${index}.jDepositRate`}
																				index={index}
																				values={values}
																				mulFieldName={`details.${index}.jArea`}
																				amountFieldName={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositRate`)}
																				setFieldValue={setFieldValue}
																			/>
																		</Table.Cell>
																		<Table.Cell className={dharautiClass}>
																			<TableInput
																				placeholder="0.00"
																				name={`details.${index}.jDepositAmount`}
																				error={getIn(errors, `details.${index}.jDepositAmount`)}
																				value={getIn(values, `details.${index}.jDepositAmount`)}
																				onChange={handleChange}
																			/>
																		</Table.Cell>
																	</Table.Row>
																),
															];
														})}
													{/* Dynamic additional rows */}
													{/* ------------------------------------ */}

													<Table.Row>
														<Table.Cell className="print-col-span" data-colSpan={hasDhrt ? '5' : '3'} colSpan={'5'}>
															{rajaswoForm.table_total}
														</Table.Cell>
														<Table.Cell>
															<TableInput
																placeholder="0.00"
																name="detailsTotal"
																onBlur={() => {
																	if (values.details) {
																		const amt = values.details.reduce((amount, next) => {
																			if (next) {
																				return (
																					parseFloat(amount) + calculateTotal(next.drAmount, next.jrAmount, next.depositAmount, next.jDepositAmount)
																				);
																			} else {
																				return 0.0;
																			}
																		}, 0.0);
																		setFieldValue('detailsTotal', round(amt));
																	}
																}}
																value={values.detailsTotal}
																error={errors.detailsTotal}
																onChange={handleChange}
															/>
														</Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
										</div>
										<Table celled structured className="tableSecond">
											<Table.Body>
												<Table.Row>
													<Table.Cell width={4}>{rajaswoForm.table_foot.Child_1}</Table.Cell>
													<Table.Cell width={6}>
														<RajasowTable2Input name="formAmt" values={values} setFieldValue={setFieldValue} error={errors.formAmt} />
													</Table.Cell>
													<Table.Cell rowSpan={isKankai ? '6' : '5'}>{rajaswoForm.table_foot.Child_6}</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_2}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input name="applicationAmt" values={values} setFieldValue={setFieldValue} error={errors.applicationAmt} />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_3}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input name="aminAmt" values={values} setFieldValue={setFieldValue} error={errors.aminAmt} />
													</Table.Cell>
												</Table.Row>
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_4}</Table.Cell>
													<Table.Cell>
														<RajasowTable2Input name="anyeAmt" values={values} setFieldValue={setFieldValue} error={errors.anyeAmt} />
													</Table.Cell>
												</Table.Row>
												{isKankai && (
													<Table.Row>
														<Table.Cell>{rajaswoForm.table_foot.discount_row}</Table.Cell>
														<Table.Cell>
															<Table basic="very" compact="very">
																<Table.Header>
																	<Table.Row>
																		<Table.HeaderCell verticalAlign="bottom">{rajaswoForm.table_foot.discount_percent}</Table.HeaderCell>
																		<Table.HeaderCell verticalAlign="bottom">{rajaswoForm.table_foot.discount_amount}</Table.HeaderCell>
																	</Table.Row>
																</Table.Header>
																<Table.Body>
																	<Table.Row>
																		<Table.Cell>
																			<RajasowDiscountPercent
																				name="discountPercent"
																				values={values}
																				setFieldValue={setFieldValue}
																				error={errors.discountPercent}
																			/>
																		</Table.Cell>
																		<Table.Cell>
																			{' '}
																			<TableInput placeholder="0.00" name="discountAmount" value={values.discountAmount} />
																		</Table.Cell>
																	</Table.Row>
																</Table.Body>
															</Table>
														</Table.Cell>
													</Table.Row>
												)}
												<Table.Row>
													<Table.Cell>{rajaswoForm.table_foot.Child_5}</Table.Cell>
													<Table.Cell>
														<TableInput
															readOnly={true}
															placeholder="0.00"
															name="totalAmt"
															// onBlur={() => {
															//   setFieldValue(
															//     "totalAmt",
															//     calculateTotal(
															//       values.detailsTotal,
															//       values.anyeAmt,
															//       values.aminAmt,
															//       values.formAmt,
															//       values.applicationAmt
															//     )
															//   );
															// }}
															value={values.totalAmt}
															error={errors.totalAmt}
															// onChange={handleChange}
														/>
													</Table.Cell>
												</Table.Row>
											</Table.Body>
										</Table>
									</div>
								</div>
								<br />
								{/* {this.props.hasSavePermission && (
									<Button primary onClick={handleSubmit}>
										Save
									</Button>
								)} */}
								<SaveButton
									formUrl={this.props.parentProps.location.pathname}
									prevData={checkError(prevData)}
									hasSavePermission={this.props.hasSavePermission}
									handleSubmit={handleSubmit}
									errors={errors}
								/>
								{/* <RajaswaApprovalStatusDisplay
									rStatus={rStatus}
									status={approveStatus}
								/> */}
							</Form>
						);
					}}
				/>
			</div>
		);
	}
}
const RajaswoDetailView = parentProps => (
	<JSONDataMultipleGetFormContainer
		api={[
			{ api: api.rajaswaEntry, objName: 'rajaswoEntry', form: true },
			{ api: api.roadSetBack, objName: 'roadSetBack', form: false, utility: true },
		]}
		prepareData={data => data}
		useInnerRef={true}
		needsRajaswo={true}
		parentProps={parentProps}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value', null, 'parent'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param4: ['15dayspecial'],
			param5: ['removeOnPrint'],
		}}
		render={props => <RajaswoDetailViewComponent {...props} parentProps={parentProps} />}
	/>
);

const HeaderFirstRow = ({ dharautiClass }) => (
	<Table.Row>
		{/* sn */}
		<Table.HeaderCell rowSpan="3" width={2}>
			{rajaswoForm.table_head.thead_1}
		</Table.HeaderCell>
		<Table.HeaderCell colSpan="3" width={3}>
			{rajaswoForm.table_head.thead_type.dhalan}
		</Table.HeaderCell>
		{/* <Table.HeaderCell colSpan="3" width={3}>
			{rajaswoForm.table_head.thead_type.other}
		</Table.HeaderCell> */}
		<Table.HeaderCell className={dharautiClass} rowSpan="2" colSpan="2">
			{rajaswoForm.table_head.thead_6}
		</Table.HeaderCell>
	</Table.Row>
);

const HeaderThirdRow = ({ dharautiClass }) => (
	<Table.Row>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_1}</Table.HeaderCell>
		<Table.HeaderCell>{rajaswoForm.table_head.thead_3_hasChild.theadChild_2}</Table.HeaderCell>
		{/* <Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_1}
		</Table.HeaderCell>
		<Table.HeaderCell>
			{rajaswoForm.table_head.thead_5_hasChild.theadChild_2}
		</Table.HeaderCell> */}
		<Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_1}</Table.HeaderCell>
		<Table.HeaderCell className={dharautiClass}>{rajaswoForm.table_head.thead_6_hasChild.theadChild_2}</Table.HeaderCell>
	</Table.Row>
);

export default RajaswoDetailView;
