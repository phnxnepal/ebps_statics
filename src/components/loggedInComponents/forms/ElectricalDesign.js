import React, { Component } from 'react';
import { Form, Table, Message } from 'semantic-ui-react';
import { Formik } from 'formik';
import { ElectricalDesign } from '../../../utils/data/mockLangFile';
import { getMessage } from '../../shared/getMessage';

import { SemanticToastContainer } from 'react-semantic-toasts';
import * as Yup from 'yup';
import { LabeledInputWithVariableUnit } from '../../shared/LabeledInput';
import { isEmpty, replaceDot, parseKey } from '../../../utils/functionUtils';

import api from '../../../utils/api';
import { isStringEmpty } from '../../../utils/stringUtils';
import { validateMoreThanZeroNumber } from '../../../utils/validationUtils';
import { prepareElecData, handleSuccess } from '../../../utils/dataUtils';
import { checkError } from './../../../utils/dataUtils';
import { FormUrl } from '../../../utils/enums/url';
import { EnglishField } from '../../shared/fields/EnglishField';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
const EDdata = ElectricalDesign.ElectricalDesign_Data;
const messageId = 'ElectricalDesign.ElectricalDesign_Data';

class ElectricalDesignFormComponent extends Component {
	render() {
		const { prevData, listData, groups } = prepareElecData(this.props.prevData);
		const { hasSavePermission, hasDeletePermission, isSaveDisabled, formUrl } = this.props;
		const data = listData;

		// let prevDatas = listData.some(row => {
		// 	let qty = !isStringEmpty(row.qty.trim()) && parseInt(row.qty.trim()) !== 0;
		// 	let remarks = !isStringEmpty(row.remark.trim());
		// 	if (qty == false && remarks == false) {
		// 		return false;
		// 	} else {
		// 		return true;
		// 	}
		// });

		let list = (this.props.prevData && !isStringEmpty(this.props.prevData.enterBy) && !isStringEmpty(this.props.prevData.enterDate)) ? this.props.prevData : {};

		const formDataSchema = data
			.filter(el => !isStringEmpty(el.element))
			.map(row => {
				if (row.elementId) {
					const qtyField = `${replaceDot(row.elementId.toString())}_qty`;
					return {
						[qtyField]: validateMoreThanZeroNumber,
					};
				} else {
					return {};
				}
			});

		const ElectricalDesignSchema = Yup.object().shape(
			Object.assign(
				{
					// buildingType: Yup.string().required('Required')
				},
				...formDataSchema
			)
		);

		return (
			<>
				<Formik
					enableReinitialize
					initialValues={prevData}
					validationSchema={ElectricalDesignSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						const dataToSend = {
							applicationNo: this.props.permitData.applicantNo,
							obj: [],
						};

						Object.keys(values).forEach(key => {
							const { classId: elementId, field } = parseKey(key);

							listData
								.filter(doc => {
									return doc.elementId === elementId;
								})
								.forEach(updatedRow => {
									const index = dataToSend.obj.findIndex(x => {
										return x.elementId === updatedRow.elementId;
									});
									if (index > -1) {
										dataToSend.obj[index] = Object.assign(dataToSend.obj[index], { [field]: values[key] });
									} else {
										dataToSend.obj.push({
											elementId: updatedRow.elementId,
											[field]: values[key],
										});
									}
								});
						});

						try {
							await this.props.postAction(api.electricalDesign, dataToSend, true);

							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								// showToast('Your data has been successfully saved.');
								const url = `/user${FormUrl.ANUSUCHI_GA}`;
								// getNextUrl(
								//   this.props.parentProps.location.pathname
								// );
								// setTimeout(() => {
								//   this.props.parentProps.history.push(url);
								// }, 2000);
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success, url);
							}
							actions.setSubmitting(false);
						} catch (err) {
							actions.setSubmitting(false);
							console.log('Electrical form class b submission failed. ', err);
						}
					}}
					render={props => {
						return (
							<Form loading={props.isSubmitting}>
								<SemanticToastContainer />

								{!isEmpty(this.props.errors) && (
									<Message negative>
										<Message.Header>Error</Message.Header>
										<p>{this.props.errors.message}</p>
									</Message>
								)}
								<div ref={this.props.setRef}>
									<FormHeading />
									<Table celled collapsing compact className="english-div">
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell width={1}>S.N.</Table.HeaderCell>
												<Table.HeaderCell width={7}>Electrical Elements</Table.HeaderCell>
												<Table.HeaderCell width={4}>As per Submitted Design</Table.HeaderCell>

												<Table.HeaderCell width={4}>Remarks</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										{Object.values(groups).map((groupName, groupId) => {
											// console.log('props formData', props);
											return (
												<Table.Body key={groupId}>
													<Table.Row>
														<Table.Cell colSpan="4">
															<strong>{`${parseInt(groupId) + 1}. ${groups[groupId]}`}</strong>
														</Table.Cell>
													</Table.Row>
													{listData
														.filter(row => row.groupName === groupName && !isStringEmpty(row.element))
														.map(row => {
															const qtyField = `${replaceDot(row.elementId.toString())}_qty`;
															const remarkField = `${replaceDot(row.elementId.toString())}_remark`;
															const unitField = `${replaceDot(row.elementId.toString())}_usedUnit`;
															return (
																<Table.Row key={row.elementId}>
																	<Table.Cell textAlign="center">{row.elementId}</Table.Cell>
																	<Table.Cell>{row.element}</Table.Cell>
																	<Table.Cell>
																		<Form.Field>
																			<Form.Input error={props.errors[qtyField]}>
																				<LabeledInputWithVariableUnit
																					suffix={row.unit}
																					name={qtyField}
																					unitField={unitField}
																					values={props.values}
																					handleChange={props.handleChange}
																					setFieldValue={props.setFieldValue}
																					value={props.values[qtyField]}
																				/>
																			</Form.Input>
																		</Form.Field>
																	</Table.Cell>
																	<Table.Cell>
																		<EnglishField fluid placeholder="Remark" name={remarkField} />
																	</Table.Cell>
																</Table.Row>
															);
														})}
												</Table.Body>
											);
										})}
									</Table>
									<FormFooter />
								</div>
								<br />
								<br />
								<SaveButtonValidation
									errors={props.errors}
									formUrl={formUrl}
									hasSavePermission={hasSavePermission}
									hasDeletePermission={hasDeletePermission}
									isSaveDisabled={isSaveDisabled}
									prevData={list}
									handleSubmit={props.handleSubmit}
									validateForm={props.validateForm}
								/>
							</Form>
						);
					}}
				/>
			</>
		);
	}
}

const FormHeading = () => (
	<div className="centered-underlined-heading">
		<h3>{getMessage(`${messageId}.heading.heading_1`, EDdata.heading.heading_1)}</h3>
		<h5>{getMessage(`${messageId}.heading.heading_2`, EDdata.heading.heading_2)}</h5>
		<h3 className="english-div">{getMessage(`${messageId}.heading.heading_3`, EDdata.heading.heading_3)}</h3>
		<h5 className="english-div">{getMessage(`${messageId}.heading.heading_4`, EDdata.heading.heading_4)}</h5>
	</div>
);

const FormFooter = () => (
	<div className="english-div">
		<span style={{ marginLeft: '15px' }}>
			<b>{getMessage(`${messageId}.footer.footer_1`, EDdata.footer.footer_1)}</b>
		</span>
		<span style={{ marginLeft: '5px' }}>{getMessage(`${messageId}.footer.footer_2`, EDdata.footer.footer_2)}</span>
		<br />
		<span style={{ marginLeft: '60px' }}>{getMessage(`${messageId}.footer.footer_3`, EDdata.footer.footer_3)}</span>
	</div>
);

const ElectricalDesignForm = parentProps => (
	<FormContainerV2
		api={[{ api: api.electricalDesign, objName: 'electrical', form: true }]}
		prepareData={data => data}
		onBeforeGetContent={{
			param1: ['getElementsByTagName', 'input', 'value'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
		}}
		parentProps={parentProps}
		useInnerRef={true}
		render={props => <ElectricalDesignFormComponent {...props} parentProps={parentProps} />}
	/>
);

export default ElectricalDesignForm;
