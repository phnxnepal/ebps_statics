import React, { Component } from 'react';
import { Formik } from 'formik';
import { Form } from 'semantic-ui-react';
import { CompactDashedLangDate } from '../../shared/DateField';
import { SamsodhitPratibedan } from '../../../utils/data/SamsodhitPratibedanData';
import api from '../../../utils/api';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getCurrentDate } from '../../../utils/dateUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import EbpsTextareaField from '../../shared/MyTextArea';
import * as Yup from 'yup';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess, squareUnitOptions } from '../../../utils/dataUtils';
import { validateNepaliDate, validateNullableZeroNumber, validateNullableNumber } from '../../../utils/validationUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { getSalutation } from '../../../utils/clientConfigs/sansodhitSuperStructure';
import { SectionHeader } from '../../uiComponents/Headers';
import { ShreeDropdown } from './formComponents/ShreeDropdown';
import { chaChainaOptions, shreeOptions } from '../../../utils/optionUtils';
import { YesNoRadio } from './formComponents/YesNoRadio';
import { DashedLengthInputWithRelatedUnits, DashedAreaInputWithRelatedUnits } from '../../shared/EbpsUnitLabelValue';
import { FlexSingleRight } from '../../uiComponents/FlexDivs';
import { PrintParams } from '../../../utils/printUtils';
import { Client } from '../../../utils/clientUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { FloorBlockArray } from '../../../utils/floorUtils';
import { BlockComponents } from '../../shared/formComponents/BlockFloorComponents';
import { SignatureImage } from './formComponents/SignatureImage';

const sam = SamsodhitPratibedan.SamsodhitPratibedan_data;

const SamsodhitSuperSchema = Yup.object().shape(
	Object.assign({
		date: validateNepaliDate,
		superStructureBuildDate: validateNepaliDate,
		landPassDate: validateNepaliDate,
		thapLiyekoMiti: validateNepaliDate,
		floor: Yup.object().shape({
			length: validateNullableZeroNumber,
			width: validateNullableZeroNumber,
			area: validateNullableZeroNumber,
		}),
		thapHouse: Yup.object().shape({
			length: validateNullableZeroNumber,
			width: validateNullableZeroNumber,
			area: validateNullableZeroNumber,
		}),
		thapHouseDastur: validateNullableNumber,
		thapHouseDharauti: validateNullableNumber,
	})
);

class SamsodhitSuperStructurePratibedanComponent extends Component {
	constructor(props) {
		super(props);
		const { permitData, otherData, prevData, orgCode, userData, enterByUser, DEFAULT_UNIT_LENGTH } = props;
		const json_data = getJsonData(prevData);
		const superstructureBuild = getJsonData(otherData.superstructureBuild);
		const floorArray = new FloorBlockArray(permitData.floor);
		const groundFloor = floorArray.getFloorByFloor(1);
		let serInfo = {
			subName: '',
			subDesignation: '',
		};
		serInfo.subName = enterByUser.name;
		serInfo.subDesignation = enterByUser.designation;

		// console.log("s", superstructureBuild)
		const initialValues = prepareMultiInitialValues(
			{
				// Default values
				obj: {
					date: getCurrentDate(true),
					sadakMapdandaValid: 'Y',
					siteMargin: 'Y',
					shree: shreeOptions[0].value,
					landPassDate: permitData.landPassDate,
					thapHouse: { floorUnit: DEFAULT_UNIT_LENGTH },
					groundFloor: floorArray.getInitialValue(['length', 'width', 'height', 'area'], '', groundFloor),
					floorUnit: floorArray.getFloorUnit(),
					...serInfo,
				},
				reqFields: [],
			},
			{ obj: superstructureBuild, reqFields: ['superStructureBuildDate'] },
			//prev values
			{ obj: json_data, reqFields: [] }
		);

		const { salutations } = getSalutation(orgCode, userData);

		this.state = {
			initialValues,
			salutations,
			floorArray,
			blocks: floorArray.getBlocks(),
			isSundarHaraicha: 'orgCode' === Client.SUNDARHARAICHA,
		};
	}
	render() {
		const {
			permitData,
			errors: reduxErrors,
			userData,
			prevData,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			staticFiles,
		} = this.props;
		const { initialValues, salutations, isSundarHaraicha, floorArray, blocks } = this.state;
		return (
			<Formik
				initialValues={initialValues}
				validationSchema={SamsodhitSuperSchema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					values.applicationNo = permitData.applicationNo;
					values.error && delete values.error;
					try {
						await this.props.postAction(`${api.samsodhitSupStruPratibedan}${permitData.applicationNo}`, values);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						if (this.props.success && this.props.success.success) {
							handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
						}
					} catch (err) {
						console.log('Error', err);
						actions.setSubmitting(false);
						window.scrollTo(0, 0);
						// showToast('Something went wrong !!');
					}
				}}
			>
				{({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors }) => (
					<Form loading={isSubmitting} className="ui form">
						{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}

						<div ref={this.props.setRef}>
							<div className="flex-item-space-between margin-bottom">
								<LetterSalutation lines={salutations} />
								<CompactDashedLangDate
									name="date"
									setFieldValue={setFieldValue}
									value={values.date}
									error={errors.date}
									label={sam.date}
								/>
							</div>

							<SectionHeader>
								<h3 className="underline end-section">{sam.subject}</h3>
							</SectionHeader>

							<div className="no-margin-field">
								<span className="indent-span">{sam.data_1.data_1_1}</span>
								{userData.organization.name}
								<DashedLangInput
									name="nibedakSadak"
									className="dashedForm-control"
									setFieldValue={setFieldValue}
									value={permitData.nibedakSadak}
									error={errors.nibedakSadak}
								/>
								{sam.data_1.data_1_4}
								{permitData.nibedakTol} {sam.data_1.data_1_5}
								<ShreeDropdown name="shree" />
								{permitData.nibedakName}
								{sam.data_1.data_1_6}
								<CompactDashedLangDate
									name="superStructureBuildDate"
									setFieldValue={setFieldValue}
									value={values.superStructureBuildDate}
									error={errors.superStructureBuildDate}
								/>
								{sam.data_1.data_1_7}
							</div>
							<br />
							<div>
								<SectionHeader>
									<p className="underline end-section">{sam.heading_2.heading_2_1}</p>
								</SectionHeader>
								<p>
									{sam.data_2.data_2_1}
									{permitData.oldMunicipal}
									{sam.data_2.data_2_2}
									{permitData.newWardNo}
									{sam.data_2.data_2_3}
									{permitData.buildingJoinRoad}
									{sam.data_2.data_2_4}
									{permitData.kittaNo}
									{sam.data_2.data_2_5}
									{permitData.landArea} {permitData.landAreaType}
								</p>
							</div>
							<div className="margin-bottom">
								<SectionHeader>
									<p className="underline left-align">{sam.heading_3.heading_3_1}</p>
								</SectionHeader>
								<YesNoRadio
									name="sadakMapdandaValid"
									otherName="sadakMapdandaValidDetails"
									fieldLabel={sam.data_3.data_3_1}
									setFieldValue={setFieldValue}
									values={values}
									errors={errors}
									space={true}
									options={chaChainaOptions}
									placeholder={sam.data_3.data_3_4}
								/>
								<br />
								<YesNoRadio
									name="siteMargin"
									otherName="siteMarginDetails"
									fieldLabel={sam.data_3.data_3_5}
									setFieldValue={setFieldValue}
									values={values}
									errors={errors}
									space={true}
									options={chaChainaOptions}
									placeholder={sam.data_3.data_3_4}
								/>
							</div>
							<div className="no-margin-field margin-bottom">
								<SectionHeader>
									<p className="underline left-align">{sam.heading_4.heading_4_1}</p>
								</SectionHeader>
								<BlockComponents floorArray={floorArray} blocks={blocks}>
									{(block) => {
										const length = floorArray.getReducedFieldName('length', block, 'groundFloor');
										const width = floorArray.getReducedFieldName('width', block, 'groundFloor');
										return (
											<>
												<DashedLengthInputWithRelatedUnits
													name={length}
													label={sam.data_4.data_4_1}
													unitName="floorUnit"
													relatedFields={[
														...floorArray.getAllReducedBlockFields(['length', 'width', 'area'], length, 'groundFloor'),
													]}
												/>{' '}
												<DashedLengthInputWithRelatedUnits
													name={width}
													label={sam.data_4.data_4_2}
													unitName="floorUnit"
													relatedFields={[
														...floorArray.getAllReducedBlockFields(['length', 'width', 'area'], width, 'groundFloor'),
													]}
												/>{' '}
											</>
										);
									}}
								</BlockComponents>
								{sam.data_4.data_4_3}{' '}
								<CompactDashedLangDate
									name="landPassDate"
									setFieldValue={setFieldValue}
									value={values.landPassDate}
									error={errors.landPassDate}
								/>
								<BlockComponents floorArray={floorArray} blocks={blocks}>
									{(block) => {
										const area = floorArray.getReducedFieldName('area', block, 'groundFloor');
										return (
											<>
												<DashedAreaInputWithRelatedUnits
													name={area}
													label={sam.data_4.data_4_4}
													unitName="floorUnit"
													squareOptions={squareUnitOptions}
													relatedFields={[
														...floorArray.getAllReducedBlockFields(['length', 'width', 'area'], area, 'groundFloor'),
													]}
												/>
											</>
										);
									}}
								</BlockComponents>
							</div>
							<div className="no-margin-field margin-bottom">
								<SectionHeader>
									<p className="left-align underline">{sam.data_4.data_4_5}</p>
								</SectionHeader>
								<div>{sam.data_4.data_4_6}</div>
								<DashedLengthInputWithRelatedUnits
									name="thapHouse.length"
									label={sam.data_4.data_4_1}
									unitName="thapHouse.floorUnit"
									relatedFields={['thapHouse.width', 'thapHouse.area']}
								/>{' '}
								<DashedLengthInputWithRelatedUnits
									name="thapHouse.width"
									label={sam.data_4.data_4_2}
									unitName="thapHouse.floorUnit"
									relatedFields={['thapHouse.length', 'thapHouse.area']}
								/>{' '}
								{sam.data_4.data_4_3}{' '}
								<CompactDashedLangDate
									name="thapLiyekoMiti"
									setFieldValue={setFieldValue}
									value={values.thapLiyekoMiti}
									error={errors.thapLiyekoMiti}
								/>
								<div>
									<DashedAreaInputWithRelatedUnits
										name="thapHouse.area"
										label={sam.data_4.data_4_4}
										unitName="thapHouse.floorUnit"
										squareOptions={squareUnitOptions}
										relatedFields={['thapHouse.length', 'thapHouse.width']}
									/>
								</div>
								<div className="margin-bottom">
									{sam.data_4.data_4_8}
									<DashedLangInput
										name="thapHouseDastur"
										setFieldValue={setFieldValue}
										value={values.thapHouseDastur}
										error={errors.thapHouseDastur}
									/>{' '}
									{sam.data_4.data_4_9}
									<DashedLangInput
										name="thapHouseDharauti"
										setFieldValue={setFieldValue}
										value={values.thapHouseDharauti}
										error={errors.thapHouseDharauti}
									/>
								</div>
								{!isSundarHaraicha && (
									<div>
										<SectionHeader>
											<p className="underline left-align">{sam.heading_5.heading_5_1}</p>
										</SectionHeader>
										<EbpsTextareaField
											placeholder="विवरण"
											name="bibaran"
											value={values.bibaran}
											error={errors.bibaran}
											setFieldValue={setFieldValue}
										/>
									</div>
								)}
							</div>

							<FlexSingleRight>
								<div>{sam.footer_1.footer_1_1}</div>
								<div>
									{sam.footer_1.footer_1_2}
									<DashedLangInput name="subName" setFieldValue={setFieldValue} value={values.subName} error={errors.subName} />
								</div>
								<div>
									{sam.footer_1.footer_1_3}
									<DashedLangInput
										name="subDesignation"
										setFieldValue={setFieldValue}
										value={values.subDesignation}
										error={errors.subDesignation}
									/>
								</div>
								<div>
									<SignatureImage value={staticFiles.ghardhaniSignature} label={sam.footer_1.footer_1_4} showSignature={true} />
								</div>
							</FlexSingleRight>
						</div>

						<SaveButtonValidation
							errors={errors}
							validateForm={validateForm}
							formUrl={formUrl}
							hasSavePermission={hasSavePermission}
							hasDeletePermission={hasDeletePermission}
							isSaveDisabled={isSaveDisabled}
							prevData={checkError(prevData)}
							handleSubmit={handleSubmit}
						/>
					</Form>
				)}
			</Formik>
		);
	}
}

const SamsodhitSuperStructurePratibedan = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.samsodhitSupStruPratibedan,
				objName: 'sansodhitSupStruPratibedan',
				form: true,
			},
			{
				api: api.superStructureBuild,
				objName: 'superstructureBuild',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			...PrintParams.TEXT_AREA,
			param5: ['getElementsByClassName', 'ui dropdown', 'innerText'],
			param7: ['getElementsByClassName', 'ui checkbox', 'label'],
			param3: ['getElementsByClassName', 'ui label', 'innerText'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		prepareData={(data) => data}
		parentProps={parentProps}
		fetchFiles={true}
		useInnerRef={true}
		render={(props) => <SamsodhitSuperStructurePratibedanComponent {...props} parentProps={parentProps} />}
	/>
);
export default SamsodhitSuperStructurePratibedan;
