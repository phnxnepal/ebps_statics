import React from 'react';
import { Table, Dropdown } from 'semantic-ui-react';
import { distanceOptions } from '../../../../utils/dataUtils';
import { getIn } from 'formik';
import FormikInput from '../../../shared/FormikInput';
import { mapTechnical } from '../../../../utils/data/mockLangFile';

const mapTech_data = mapTechnical.mapTechnicalDescription;
const RoadSetBack = mapTech_data.mapTech_24_table;

export const RoadSetbackTable = ({ title, values, setFieldValue, errors, handleChange }) => {
	return (
		<div>
			<div>{title}</div>
			<Table celled compact size="small" collapsing className="certificate-ui-table road-table" style={{ marginBottom: '0.5em' }}>
				<Table.Header>
					<Table.Row>
						{Object.keys(RoadSetBack.mapTech_24_table_heading).map((key, index) => (
							<Table.HeaderCell key={index}>
								{RoadSetBack.mapTech_24_table_heading[key]}
								{index !== 0 && (
									<>
										{' ('}
										<Dropdown
											name="setBackUnit"
											onChange={(e, { value }) => setFieldValue('setBackUnit', value)}
											value={values.setBackUnit}
											options={distanceOptions}
										/>
										{')'}
									</>
								)}
							</Table.HeaderCell>
						))}
					</Table.Row>
				</Table.Header>
				<Table.Body>
					{Object.keys(RoadSetBack.mapTech_24_table_subheading).map((key, RoadSetBackValue) => {
						// RoadSetBackValue++;
						return (
							<Table.Row key={RoadSetBackValue}>
								<Table.Cell>
									<label htmlFor={`setBack[${RoadSetBackValue}]`}>{RoadSetBack.mapTech_24_table_subheading[key]}</label>
								</Table.Cell>
								<Table.Cell>
									<div className={getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadCenter`) ? 'error field' : 'field'}>
										<FormikInput name={`setBack.${RoadSetBackValue}.distanceFromRoadCenter`} handleChange={handleChange} />
										{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadCenter`) && (
											<div className="tableError">{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadCenter`)}</div>
										)}
									</div>
								</Table.Cell>
								<Table.Cell>
									<div className={getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadBoundary`) ? 'error field' : 'field'}>
										<FormikInput name={`setBack.${RoadSetBackValue}.distanceFromRoadBoundary`} handleChange={handleChange} />
										{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadBoundary`) && (
											<div className="tableError">{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadBoundary`)}</div>
										)}
									</div>
								</Table.Cell>
								<Table.Cell>
									<div className={getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`) ? 'error field' : 'field'}>
										<FormikInput name={`setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`} handleChange={handleChange} />
										{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`) && (
											<div className="tableError">{getIn(errors, `setBack.${RoadSetBackValue}.distanceFromRoadRightEdge`)}</div>
										)}
									</div>
								</Table.Cell>
							</Table.Row>
						);
					})}
				</Table.Body>
			</Table>
		</div>
	);
};
