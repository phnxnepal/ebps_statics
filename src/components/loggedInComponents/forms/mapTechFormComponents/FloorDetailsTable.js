import React from 'react';
import { Table, Grid } from 'semantic-ui-react';
import { mapTechnical } from '../../../../utils/data/mockLangFile';
import { areaUnitOptions, getFloorValue, distanceOptions } from '../../../../utils/dataUtils';
import { getIn } from 'formik';
import { UnitDropdownWithRelatedFields } from '../../../shared/EbpsUnitLabelValue';
import { EbpsFormInline } from '../../../shared/EbpsForm';
import { TableInputIm } from '../../../shared/TableInput';
import { floorData } from '../../../../utils/data/genericData';
import { DashedNormalInputIm } from '../../../shared/DashedFormInput';

const mapTech_data = mapTechnical.mapTechnicalDescription;
const buildingDetails = mapTech_data.mapTech_12_table;
const buildingFloorDetails = mapTech_data.mapTech_18_table;

export const FloorDetailsTable = React.memo(({ floorArray, formattedFloors, values, setFieldValue, errors }) => {
	if (floorArray.hasBlocks) {
		return (
			<>
				{Object.entries(formattedFloors).map(([block, floorObj]) => {
					return (
						<div key={block}>
							{floorData.block} {block}
							<Table celled compact collapsing>
								<Table.Header>
									<Table.Row>
										{Object.keys(buildingDetails.mapTech_12_table_heading).map((key, index) => (
											<Table.HeaderCell key={key}>
												{index === 0 || index === 4 ? (
													buildingDetails.mapTech_12_table_heading[key]
												) : (
													<>
														{buildingDetails.mapTech_12_table_heading[key]}
														<UnitDropdownWithRelatedFields
															unitName="floorUnit"
															options={areaUnitOptions}
															relatedFields={[
																...floorArray.getAllCustomsFields('', 'floorDetails', [
																	'proposedConstructionArea',
																	'oldConstructionArea',
																	'totalConstructionArea',
																]),
															]}
														/>
													</>
												)}
											</Table.HeaderCell>
										))}
									</Table.Row>
								</Table.Header>
								<Table.Body>
									{Array.isArray(floorObj) &&
										floorObj.map((floor, buildingIndex) => {
											return (
												<Table.Row key={buildingIndex}>
													<Table.Cell>
														<label htmlFor={`floorDetails[${buildingIndex}]`}>{getFloorValue(floor.floor)}</label>
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name={floor.fieldName('floorDetails', 'proposedConstructionArea')} />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name={`floorDetails.${buildingIndex}.${block}_oldConstructionArea`} />
													</Table.Cell>
													<Table.Cell>
														<TableInputIm name={`floorDetails.${buildingIndex}.${block}_totalConstructionArea`} />
													</Table.Cell>
													<Table.Cell>
														<EbpsFormInline
															name={`floorDetails.${buildingIndex}.${block}_remarks`}
															setFieldValue={setFieldValue}
															value={getIn(values, `floorDetails.${buildingIndex}.${block}_remarks`)}
															error={errors[`floorDetails.${buildingIndex}.${block}_remarks`]}
														/>
													</Table.Cell>
												</Table.Row>
											);
										})}
								</Table.Body>
							</Table>
						</div>
					);
				})}
			</>
		);
	}
	return (
		<Table celled compact collapsing>
			<Table.Header>
				<Table.Row>
					{Object.keys(buildingDetails.mapTech_12_table_heading).map((key, index) => (
						<Table.HeaderCell key={key}>
							{index === 0 || index === 4 ? (
								buildingDetails.mapTech_12_table_heading[key]
							) : (
								<>
									{buildingDetails.mapTech_12_table_heading[key]}
									<UnitDropdownWithRelatedFields
										unitName="floorUnit"
										options={areaUnitOptions}
										relatedFields={[
											...floorArray.getAllCustomsFields('', 'floorDetails', [
												'proposedConstructionArea',
												'oldConstructionArea',
												'totalConstructionArea',
											]),
										]}
									/>
								</>
							)}
						</Table.HeaderCell>
					))}
				</Table.Row>
			</Table.Header>
			<Table.Body>
				{floorArray.getFloors().map((floor, buildingIndex) => {
					// buildingIndex++;
					return (
						<Table.Row key={buildingIndex}>
							<Table.Cell>
								<label htmlFor={`floorDetails[${buildingIndex}]`}>{getFloorValue(floor.floor)}</label>
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name={`floorDetails.${buildingIndex}.proposedConstructionArea`} />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name={`floorDetails.${buildingIndex}.oldConstructionArea`} />
							</Table.Cell>
							<Table.Cell>
								<TableInputIm name={`floorDetails.${buildingIndex}.totalConstructionArea`} />
							</Table.Cell>
							<Table.Cell>
								<EbpsFormInline
									name={`floorDetails.${buildingIndex}.remarks`}
									setFieldValue={setFieldValue}
									value={getIn(values, `floorDetails.${buildingIndex}.remarks`)}
									error={errors[`floorDetails.${buildingIndex}.remarks`]}
								/>
							</Table.Cell>
						</Table.Row>
					);
				})}
			</Table.Body>
		</Table>
	);
});

export const HeightTable = ({ floorArray, formattedFloors, topFloor }) => {
	return (
		<>
			{floorArray.hasBlocks ? (
				<>
					<div className="inline field">
						{mapTech_data.mapTech_18}
						<UnitDropdownWithRelatedFields
							unitName="heightUnit"
							options={distanceOptions}
							relatedFields={[
								...floorArray.getAllCustomsFields('', 'floor', ['height']),
								...floorArray.getAllFieldBlockNames('buildingHeight'),
							]}
						/>
					</div>
					<Grid>
						<Grid.Row columns={floorArray.getBlocks().length}>
							{Object.entries(formattedFloors).map(([block, floorObj]) => {
								return (
									<Grid.Column key={block} computer={5} mobile={16}>
										{floorData.block} {block}
										<Table celled compact collapsing>
											<HeightTableHeader />
											<Table.Body>
												{floorObj.map(floor => (
													<React.Fragment key={floor.floor}>
														<HeightTableRow floor={floor} />
													</React.Fragment>
												))}
											</Table.Body>
										</Table>
									</Grid.Column>
								);
							})}
						</Grid.Row>
						<Grid.Row>
							<div className="div-indent-one">
								<span>{mapTech_data.mapTech_19}</span>
								{Object.entries(topFloor).map(([block, floorObj]) => {
									return (
										<div className="div-indent-one" key={block}>
											{floorData.block} {block}
											<DashedNormalInputIm name={floorObj.fieldName('buildingHeight')} />
											<UnitDropdownWithRelatedFields
												unitName="heightUnit"
												options={distanceOptions}
												relatedFields={[
													...floorArray.getAllCustomsFields('', 'floor', ['height']),
													...floorArray.getAllFieldBlockNames('buildingHeight'),
												]}
											/>
										</div>
									);
								})}
							</div>
						</Grid.Row>
					</Grid>
				</>
			) : (
				<div
					style={{
						marginBottom: '10px',
						pageBreakInside: 'avoid',
					}}
				>
					<div className="inline field">
						{mapTech_data.mapTech_18}
						<UnitDropdownWithRelatedFields
							unitName="heightUnit"
							options={distanceOptions}
							relatedFields={[...floorArray.getAllCustomsFields('', 'floor', ['height']), 'buildingHeight']}
						/>
					</div>
					<Table celled compact collapsing>
						<HeightTableHeader />
						<Table.Body>
							{formattedFloors.map(floor => (
								<React.Fragment key={floor.floor}>
									<HeightTableRow floor={floor} />
								</React.Fragment>
							))}
						</Table.Body>
					</Table>
					<div className="inline field">
						<span>{mapTech_data.mapTech_19}</span>
						<DashedNormalInputIm name={'buildingHeight'} />
						<UnitDropdownWithRelatedFields
							unitName="heightUnit"
							options={distanceOptions}
							relatedFields={[...floorArray.getAllCustomsFields('', 'floor', ['height']), 'buildingHeight']}
						/>
					</div>
				</div>
			)}
		</>
	);
};

const HeightTableRow = ({ floor }) => {
	return (
		<Table.Row key={floor.floor}>
			<Table.Cell>{floor.label.nepaliName}</Table.Cell>
			<Table.Cell>
				<TableInputIm name={floor.fieldName('floor', 'height')} />
			</Table.Cell>
		</Table.Row>
	);
};

const HeightTableHeader = () => {
	return (
		<Table.Header>
			<Table.Row>
				{Object.keys(buildingFloorDetails.mapTech_18_table_heading).map(key => (
					<Table.HeaderCell key={key}>{buildingFloorDetails.mapTech_18_table_heading[key]}</Table.HeaderCell>
				))}
			</Table.Row>
		</Table.Header>
	);
};
