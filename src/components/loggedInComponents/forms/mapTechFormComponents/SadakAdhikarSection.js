import React from 'react';
import { mapTechnical } from '../../../../utils/data/mockLangFile';
import { surroundingMappingFlat, distanceOptions } from '../../../../utils/dataUtils';
import { getIn } from 'formik';
import { lengthconvertor } from '../../../../utils/mathUtils';
import { Dropdown, Select } from 'semantic-ui-react';
import { DashedNormalInputIm, DashedLangInput } from '../../../shared/DashedFormInput';
import { statusNepaliOption } from '../../../../utils/optionUtils';
import { RadioInput } from '../../../shared/formComponents/RadioInput';

const mapTech_data = mapTechnical.mapTechnicalDescription;
export const SadakAdhikarSection = ({ values, errors, hasRoadSetbackMaster, roadSetbackMaster, setFieldValue, roadSetBackOption }) => {
	return (
		<div className="inline field">
			{mapTech_data.mapTech_23.mapTech_23_head}
			<div className="brackets" style={{ display: 'inline-flex' }}>
				{`( `}
				<Dropdown
					name="sadakAdhikarUnit"
					onChange={(e, { value }) => {
						const prevUnit = getIn(values, 'sadakAdhikarUnit');
						surroundingMappingFlat.forEach((side, index) => {
							const reqRoad = roadSetbackMaster.find((road) => road.roadName === getIn(values, `roadName${index}`));
							if (reqRoad) {
								if (value === 'METRE') {
									setFieldValue(`requiredDistance${index}`, reqRoad.setBackMeter);
								} else {
									setFieldValue(`requiredDistance${index}`, reqRoad.setBackFoot);
								}
							} else {
								setFieldValue(
									`requiredDistance${index}`,
									lengthconvertor(getIn(values, `requiredDistance${index}`), value, prevUnit)
								);
							}
							setFieldValue(
								`sadakAdhikarKshytra${index}`,
								lengthconvertor(getIn(values, `sadakAdhikarKshytra${index}`), value, prevUnit)
							);
						});

						setFieldValue('sadakAdhikarUnit', value);
					}}
					value={getIn(values, 'sadakAdhikarUnit')}
					options={distanceOptions}
				/>
				{' ) '}
			</div>{' '}
			<div className="div-indent">
				{surroundingMappingFlat.map((value, index) => (
					<div key={index}>
						{`${value.value} ${mapTech_data.mapTech_23.direction_suffix}:  `}
						{hasRoadSetbackMaster ? (
							<>
								{mapTech_data.mapTech_23.roadName}:
								<Select
									options={roadSetBackOption}
									name={`roadName${index}`}
									placeholder="Select an option"
									onChange={(e, { value }) => {
										setFieldValue(`roadName${index}`, value);
										const reqRoad = roadSetbackMaster.find((road) => road.roadName === value);
										if (reqRoad) {
											if (values.sadakAdhikarUnit === 'METRE') {
												setFieldValue(`sadakAdhikarKshytra${index}`, reqRoad.setBackMeter);

												setFieldValue(`requiredDistance${index}`, reqRoad.setBackMeter);
											} else {
												setFieldValue(`sadakAdhikarKshytra${index}`, reqRoad.setBackFoot);

												setFieldValue(`requiredDistance${index}`, reqRoad.setBackFoot);
											}
										} else {
											setFieldValue(`sadakAdhikarKshytra${index}`, 0);

											setFieldValue(`requiredDistance${index}`, 0);
										}
									}}
									value={getIn(values, `roadName${index}`)}
								/>{' '}
								{getIn(values, `roadName${index}`) !== mapTech_data.mapTech_23.defaultRoadOption && (
									<>
										{getIn(values, `requiredDistance${index}`) && getIn(values, `requiredDistance${index}`) > 0
											? `${mapTech_data.mapTech_23.requiredDistance}: ${getIn(values, `requiredDistance${index}`)} `
											: null}
										{mapTech_data.mapTech_23.adhikarChetra}:
										<DashedNormalInputIm name={`sadakAdhikarKshytra${index}`} />
									</>
								)}
							</>
						) : (
							<>
								({mapTech_data.mapTech_23.road}
								{statusNepaliOption.map((option) => (
									<React.Fragment key={option.value}>
										<RadioInput space={true} name={`adhikarRoadExists${index}`} option={option.value} label={option.text} />
									</React.Fragment>
								))}
								){' '}
								{getIn(values, `adhikarRoadExists${index}`) &&
								getIn(values, `adhikarRoadExists${index}`) === statusNepaliOption[0].value ? (
									<>
										{mapTech_data.mapTech_23.roadName}:
										<DashedLangInput
											name={`roadName${index}`}
											value={getIn(values, `roadName${index}`)}
											error={getIn(errors, `roadName${index}`)}
											setFieldValue={setFieldValue}
										/>
										{mapTech_data.mapTech_23.requiredDistance}:
										<DashedNormalInputIm name={`requiredDistance${index}`} />
										{mapTech_data.mapTech_23.adhikarChetra}:
										<DashedNormalInputIm name={`sadakAdhikarKshytra${index}`} />
									</>
								) : null}
							</>
						)}
					</div>
				))}
			</div>
		</div>
	);
};
