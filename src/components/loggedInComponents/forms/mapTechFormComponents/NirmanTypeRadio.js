import React from 'react';
import { RadioInput } from '../../../shared/formComponents/RadioInput';
import { nirmanTypeOptions, nirmanTypeAnyaOptions } from '../../../../utils/optionUtils';
import { DashedLangInput } from '../../../shared/DashedFormInput';

export const NirmanTypeRadio = ({ name = 'cType', space = false, fieldLabel }) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{nirmanTypeOptions.map((option) => (
				<RadioInput key={option.value} space={space} name={name} option={option.value} label={option.label} />
			))}
		</>
	);
};

export const NirmanTypeAnyaRadio = ({ name = 'cType', otherName = 'cTypeOther', space = false, fieldLabel, values, setFieldValue, errors }) => {
	return (
		<>
			<span>{fieldLabel}</span>
			{nirmanTypeAnyaOptions.map((option) => (
				<React.Fragment key={option.value}>
					<RadioInput space={space} name={name} option={option.value} label={option.text} />
				</React.Fragment>
			))}

			{values[name] && values[name] === nirmanTypeAnyaOptions[2].value ? (
				<DashedLangInput name={otherName} setFieldValue={setFieldValue} value={values[otherName]} error={errors[otherName]} />
			) : null}
		</>
	);
};
