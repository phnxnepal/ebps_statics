import React, { Component } from 'react';
import { Form, Grid, Select, Label } from 'semantic-ui-react';
import { NirmanKarya } from '../../../utils/data/mockLangFile';
import { Formik } from 'formik';
import api from '../../../utils/api';
import * as Yup from 'yup';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import { DashedLangInput, DashedLangInputWithSlash } from '../../shared/DashedFormInput';
import { DashedLangDateField } from '../../shared/DateField';

import { LetterHeadFlex } from '../../shared/LetterHead';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { SectionHeader } from '../../uiComponents/Headers';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { PrintParams } from '../../../utils/printUtils';
import { SuperStructureBuildData } from './../../../utils/data/SuperStructureBuildData';
import { LetterSalutation } from './formComponents/LetterSalutation';
import { isBiratnagar, isKamalamai } from '../../../utils/clientUtils';
import { SignatureImage } from './formComponents/SignatureImage';

const nmlang = NirmanKarya.NirmanKarya_data;

const opt = [
	{ key: 'A', value: 'सुपरस्ट्रक्चर', text: 'सुपरस्ट्रक्चर' },
	{ key: 'B', value: 'कम्पाउण्डवाल', text: 'कम्पाउण्डवाल' },
];
const opt2 = [
	{ key: 'C', value: 'मैले', text: 'मैले' },
	{ key: 'D', value: 'हामीले', text: 'हामीले' },
];
const Sdata = SuperStructureBuildData.structureDesign;

const getMs = (value) => (value === nmlang.data_2.data_2_ms.maile ? nmlang.data_2.data_2_ms.chu : nmlang.data_2.data_2_ms.chaun);
const DosrocharanSchema = Yup.object().shape(
	Object.assign({
		dosrocharanDate: validateNullableNepaliDate,
		supStrucDate: validateNullableNepaliDate,
		wareshDate: validateNullableNepaliDate,
	})
);
class DosrocharanAbedanComponent extends Component {
	constructor(props) {
		super(props);

		let initVal = {};
		const { permitData, prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);

		const othersData = {
			Address: `${permitData.nibedakSadak}, ${nmlang.footer_1.wardNo} ${permitData.nibedakTol}, ${permitData.newMunicipal}`,
		};

		initVal = prepareMultiInitialValues(
			{
				// Default values.
				obj: {
					abedanMs: nmlang.data_2.data_2_ms.maile,
					supercomp: opt[0].value,
				},
				reqFields: [],
			},
			{
				obj: getJsonData(otherData.superStructCons),
				reqFields: ['supStrucDate'],
			},
			{ obj: json_data, reqFields: [] }
		);

		if (isStringEmpty(initVal.dosrocharanDate)) {
			initVal.dosrocharanDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.wareshDate)) {
			initVal.wareshDate = getCurrentDate(true);
		}

		this.state = {
			initVal,
			othersData,
		};
	}
	render() {
		const { initVal, othersData } = this.state;
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			staticFiles,
		} = this.props;

		const udata = userData;
		const pdata = permitData;

		return (
			<div>
				<Formik
					initialValues={initVal}
					validationSchema={DosrocharanSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.DosrocharanAbedan}${permitData.applicationNo}`, values);
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ handleSubmit, handleChange, isSubmitting, values, errors, setFieldValue, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef} className="build-gaps">
								<div className="NJ-Main superStruConsView">
									{!isKamalamai && !isBiratnagar && <LetterHeadFlex userInfo={userData} />}
									{isBiratnagar && 
									<LetterSalutation lines={[Sdata.salutationPramukh, userData.organization.name, userData.organization.address]} />}
									<div style={{ textAlign: 'right' }}>
										{nmlang.data_2.data_2_2}
										{': '}
										<DashedLangDateField
											name="dosrocharanDate"
											inline={true}
											setFieldValue={setFieldValue}
											value={values.dosrocharanDate}
											// label={nmlang.data_2.data_2_2}
											className="dashedForm-control"
											error={errors.dosrocharanDate}
										/>
									</div>

									<SectionHeader>
										<h3 className="underline end-section">{nmlang.heading.Subject}</h3>
										<h2>{nmlang.heading.heading_1}</h2>
									</SectionHeader>

									<div style={{ textAlign: 'justify' }}>
										<span>{nmlang.data_1.data_1_1}</span>
										<br />
										<span style={{ marginLeft: '20px' }}>
											{nmlang.data_2.data_2_1.data_2_1_1}
											{udata.organization.name}
											{nmlang.data_2.data_2_1.data_2_1_2}
										</span>
										<DashedLangInputWithSlash
											name="chalaniNumber"
											className="dashedForm-control"
											handleChange={handleChange}
											setFieldValue={setFieldValue}
											value={values.chalaniNumber}
											error={errors.chalaniNumber}
											inline={true}
										/>
										<DashedLangDateField
											inline={true}
											label={nmlang.data_2.data_2_2}
											name="supStrucDate"
											setFieldValue={setFieldValue}
											value={values.supStrucDate}
											error={errors.supStrucDate}
										/>
										<span>{nmlang.data_2.data_2_3}</span>
										<span>{nmlang.data_2.data_2_4}</span>
										<Select
											placeholder=" सुपरस्ट्रक्चर "
											options={opt}
											name="supercomp"
											value={values.supercomp}
											onChange={(e, { value }) => setFieldValue('supercomp', value)}
										/>
										{errors.supercomp && (
											<Label pointing prompt size="large">
												{errors.supercomp}
											</Label>
										)}
										<span>{nmlang.data_2.data_2_7}</span>
										<Select
											placeholder=" मैले "
											options={opt2}
											name="abedanMs"
											value={values.abedanMs}
											onChange={(e, { value }) => setFieldValue('abedanMs', value)}
										/>
										{errors.abedanMs && (
											<Label pointing={'left'} basic color="red" prompt size="large">
												{errors.abedanMs}
											</Label>
										)}{' '}
										<DashedLangInput name="entity" setFieldValue={setFieldValue} value={values.entity} error={errors.entity} />
										<span>{nmlang.data_2.data_2_10}</span>
										{pdata.landArea} {pdata.landAreaType}
										<span>{nmlang.data_2.data_2_11}</span>
										{getMs(values.abedanMs)}
										<span>{nmlang.data_2.data_2_14}</span>
										{getMs(values.abedanMs)}
										<br />
										<br />
									</div>
									<div className="NJ-right">
										<Grid columns={2}>
											<Grid.Row>
												{/* <Form.Field inline> */}
												<Grid.Column></Grid.Column>
												<Grid.Column>
													<div>
														<span>{`${nmlang.footer_1.footer_1_1} `}</span>
														{permitData.nibedakName}
													</div>
													<div>
														<span>{`${nmlang.footer_1.footer_1_2} `}</span>
														{othersData.Address}
													</div>

													<SignatureImage
														value={staticFiles.ghardhaniSignature}
														label={nmlang.footer_1.footer_1_3}
														showSignature={true}
													/>
												</Grid.Column>
											</Grid.Row>
											{/* </Form.Field> */}
											<br />

											{/* </p>
                                <p> */}

											{/* <Form.Field inline> */}
											<Grid.Row>
												<Grid.Column></Grid.Column>
												<Grid.Column>
													{' '}
													<div>
														<span>{nmlang.footer_2.footer_2_1}</span>
														<DashedLangInput
															name="wareshName"
															setFieldValue={setFieldValue}
															value={values.wareshName}
															error={errors.wareshName}
														/>
													</div>
													<div>
														<span>{nmlang.footer_2.footer_2_2}</span>
														<DashedLangInput
															name="wareshAddress"
															setFieldValue={setFieldValue}
															value={values.wareshAddress}
															error={errors.wareshAddress}
														/>
													</div>
													<div>
														<span>{nmlang.footer_2.footer_2_3}</span>
														<span className="ui input dashedForm-control"></span>
													</div>
													<DashedLangDateField
														label={nmlang.footer_2.footer_2_4}
														name="wareshDate"
														setFieldValue={setFieldValue}
														value={values.wareshDate}
														error={errors.wareshDate}
													/>
													{/* </Form.Field> */}
												</Grid.Column>
											</Grid.Row>
										</Grid>
									</div>
								</div>
							</div>
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const DosrocharanAbedan = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.DosrocharanAbedan,
				objName: 'dosrocharanAbedan',
				form: true,
			},
			{
				api: api.superStructureConstruction,
				objName: 'superStructCons',
				form: false,
			},
		]}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByClassName', 'ui selection dropdown', 'innerText'],
			param6: ['getElementsByClassName', 'equal width fields inline-group', 'innerText'],
		}}
		useInnerRef={true}
		prepareData={(data) => data}
		parentProps={parentProps}
		render={(props) => <DosrocharanAbedanComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanAbedan;
