import React, { Component } from 'react';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import api from '../../../utils/api';
import { isEmpty, showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { Formik } from 'formik';
import { LetterHeadFlex } from '../../shared/LetterHead';
import { SectionHeader } from '../../uiComponents/Headers';
import { LayoutGaripratibedan } from '../../../utils/data/mockLangFile';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { getCurrentDate } from '../../../utils/dateUtils';
import { PatraSankhyaAndDate } from './formComponents/PatraSankhyaAndDate';
import { DashedLangDateField } from '../../shared/DateField';
import { PrintParams } from '../../../utils/printUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { Form } from 'semantic-ui-react';

// export const LayoutGariPratibedanRequestComponent = () => {
// 	/**
// 	 * @TODO implemnet form
// 	 */
// 	return <div>Layout Request</div>;
// };

const layoutdata = LayoutGaripratibedan;

class LayoutGariPratibedanRequestComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { prevData, otherData } = this.props;
		const json_data = getJsonData(prevData);
		const allowancePaperData = getJsonData(otherData.allowancePaper);
		initVal = prepareMultiInitialValues(
			{
				obj: allowancePaperData,
				reqFields: ['allowanceDate'],
			},
			{
				obj: {
					layoutDate: getCurrentDate(true),
				},
				reqFields: [],
			},
			{ obj: json_data, reqFields: [] }
		);
		if (isStringEmpty(initVal.date)) {
			initVal.date = getCurrentDate(true);
		}
		this.state = {
			initVal,
		};
	}
	render() {
		const { initVal } = this.state;
		const user_info = this.props.userData;
		const { permitData, prevData, hasSavePermission, formUrl, hasDeletePermission, isSaveDisabled } = this.props;
		return (
			<>
				{/* {reduxErrors && <ErrorDisplay message={reduxErrors.message} />} */}
				{!isEmpty(this.props.errors) && <ErrorDisplay message={this.props.errors.message} />}
				<Formik
					initialValues={initVal}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							await this.props.postAction(`${api.LayoutGariPratibedanRequest}${permitData.applicationNo}`, values);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							console.log('Error', err);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, handleSubmit, values, validateForm, setFieldValue, errors }) => (
						<Form loading={isSubmitting}>
							<div ref={this.props.setRef}>
								<div>
									<LetterHeadFlex userInfo={user_info} />
									<PatraSankhyaAndDate dateFieldName="layoutDate" setFieldValue={setFieldValue} values={values} errors={errors} />
									<SectionHeader>
										<h3 className="underline end-section">{layoutdata.title}</h3>
									</SectionHeader>
								</div>
								<div>
									<span>
										{layoutdata.shree}
										<DashedLangInput
											name="shreeName"
											setFieldValue={setFieldValue}
											value={values.shreeName}
											error={errors.shreeName}
										/>
										<br />
										&nbsp;&nbsp;&nbsp;
										<DashedLangInput
											name="sakhaName"
											setFieldValue={setFieldValue}
											value={values.sakhaName}
											error={errors.sakhaName}
										/>{' '}
										{layoutdata.sakhaekai}
									</span>
									<p>{user_info.organization.name}</p>
								</div>

								<div style={{ textAlign: 'justify ' }} className="no-margin-field">
									{layoutdata.maindata.data1}
									{' 	'}
									{user_info.organization.name}
									{''}
									{layoutdata.maindata.data2}
									{permitData.nibedakTol}
									{layoutdata.maindata.data3}
									{permitData.nibedakName}
									{layoutdata.maindata.data4}
									{permitData.oldMunicipal}
									{layoutdata.maindata.data5}
									{permitData.newWardNo}
									{layoutdata.maindata.data6}
									{permitData.kittaNo}
									{layoutdata.maindata.data7}
									{permitData.landArea} {permitData.landAreaType}
									{layoutdata.maindata.data8}
									<DashedLangDateField
										inline={true}
										name="allowanceDate"
										setFieldValue={setFieldValue}
										value={values.allowanceDate}
										error={errors.allowanceDate}
									/>
									{layoutdata.maindata.data9}
									<br />
								</div>
								<br />
								<div style={{ float: 'right' }}>
									<span className="ui input dashedForm-control"></span>
								</div>
							</div>
							<br />
							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</>
		);
	}
}

const LayoutGariPratibedanRequest = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.LayoutGariPratibedanRequest,
				objName: 'layoutGariPratibedanRequest',
				form: true,
			},
			{
				api: api.allowancePaper,
				objName: 'allowancePaper',
				form: false,
			},
		]}
		prepareData={(data) => data}
		parentProps={parentProps}
		onBeforeGetContent={{
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value'],
			param4: ['getElementsByTagName', 'textarea', 'value'],
			param6: ['getElementsByClassName', 'dashedForm-control', 'value'],
		}}
		useInnerRef={true}
		render={(props) => <LayoutGariPratibedanRequestComponent {...props} parentProps={parentProps} />}
	/>
);

export default LayoutGariPratibedanRequest;
