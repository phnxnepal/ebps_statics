import React, { Component } from 'react';
import { Form, Grid } from 'semantic-ui-react';
import { dosrocharansupervisor } from '../../../utils/data/mockLangFile';
import { Formik } from 'formik';
import * as Yup from 'yup';
import api from '../../../utils/api';
import { showToast, getUserRole, isEmpty } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';
import { getJsonData, prepareMultiInitialValues, checkError, handleSuccess } from '../../../utils/dataUtils';
import { DashedLangInput } from '../../shared/DashedFormInput';
import { DashedLangDateField } from '../../shared/DateField';
import EbpsTextareaField from '../../shared/MyTextArea';
import { DashedLangInputWithSlash } from './../../shared/DashedFormInput';
import { ConstructionTypeValue, getConstructionTypeValue } from '../../../utils/enums/constructionType';
import { getCurrentDate } from '../../../utils/dateUtils';
import { UserType } from '../../../utils/userTypeUtils';
import { isStringEmpty } from '../../../utils/stringUtils';
import { validateNullableNepaliDate } from '../../../utils/validationUtils';
import { RadioInput } from '../../shared/formComponents/RadioInput';
import { ImageDisplayInline } from '../../shared/file/FileView';
import FormContainerV2 from '../../../containers/base/FormContainerV2';
import { PostActionParams } from '../../../utils/formUtils';
import SaveButtonValidation from '../../shared/SaveButtonValidation';
import { isKamalamai } from '../../../utils/clientUtils';
import { SignatureImage } from './formComponents/SignatureImage';
import { SansodhanCheckbox } from './formComponents/SansodhanCheckbox';
import { PrintParams } from '../../../utils/printUtils';

const dcs = dosrocharansupervisor.dosrocharansupervisor_data;
const DosrocharanSchema = Yup.object().shape(
	Object.assign({
		thekedarDate: validateNullableNepaliDate,
		mukhyaDakarmiDate: validateNullableNepaliDate,
		// wareshDate: validateNullableNepaliDate,
	})
);
class DosrocharanSupervisorComponent extends Component {
	constructor(props) {
		super(props);
		let initVal = {};
		const { permitData, userData, prevData, otherData, hasDesignerChanged } = this.props;
		const json_data = getJsonData(prevData);
		const anusuchi = checkError(otherData.anusuchi);
		const plinthOwner = getJsonData(otherData.plinthOwner);

		let mapSideObj = {};
		if (getConstructionTypeValue(permitData.constructionType) === ConstructionTypeValue.PURANO_GHAR || hasDesignerChanged) {
			if (getUserRole() === UserType.DESIGNER) {
				mapSideObj = {
					userName: userData.info.userName,
					educationQualification: userData.info.educationQualification,
					consultancyName: userData.info.consultancyName,
					licenseNo: userData.info.licenseNo,
					municipalRegNo: userData.info.municipalRegNo,
					stamp: userData.info.stamp,
					signature: userData.info.signature,
					enterDate: getCurrentDate(),
				};
			} else {
				mapSideObj = {};
			}
		} else {
			if (!isEmpty(anusuchi)) {
				if (!isEmpty(anusuchi.dName && anusuchi.dDesignation && anusuchi.dDate)) {
					mapSideObj = {
						userName: anusuchi.dName,
						educationQualification: anusuchi.dDesignation,
						enterDate: anusuchi.dDate,
					};
				} else
					mapSideObj = {
						userName: anusuchi.enterBy && anusuchi.enterBy.userName,
						educationQualification: anusuchi.enterBy && anusuchi.enterBy.educationQualification,
						licenseNo: anusuchi.enterBy && anusuchi.enterBy.licenseNo,
						municipalRegNo: anusuchi.enterBy && anusuchi.enterBy.municipalRegNo,
						stamp: anusuchi.enterBy && anusuchi.enterBy.stamp,
						signature: anusuchi.enterBy.signature,
						enterDate: anusuchi.enterDate,
					};
			}
		}

		initVal = prepareMultiInitialValues(
			{
				obj: {
					sadakAdhikar: dcs.radioBox[0],
					sitePlan: dcs.radioBox[0],
					groundCoverage: dcs.radioBox[0],
					xaajibar: dcs.radioBox[0],
					nepalRastrya: dcs.radioBox[0],
					sanso: dcs.sansodhan_check[0],
					applicantDate: getCurrentDate(true),
					applicantName: permitData.applicantName,
				},
				reqFields: [],
			},
			{
				obj: anusuchi,
				reqFields: ['thekdarName', 'tAddress', 'tDarta', 'mistiriName', 'mAddress', 'mDarta'],
			},
			{
				obj: plinthOwner,
				reqFields: ['thekdarName', 'tAddress', 'tDarta', 'mistiriName', 'mAddress', 'mDarta'],
			},
			{ obj: mapSideObj, reqFields: [] },
			{
				obj: json_data,
				reqFields: [],
			}
		);

		if (isStringEmpty(initVal.thekedarDate)) {
			initVal.thekedarDate = getCurrentDate(true);
		}

		if (isStringEmpty(initVal.mukhyaDakarmiDate)) {
			initVal.mukhyaDakarmiDate = getCurrentDate(true);
		}

		this.state = { initVal };
	}
	render() {
		const {
			permitData,
			userData,
			prevData,
			errors: reduxErrors,
			hasSavePermission,
			formUrl,
			hasDeletePermission,
			isSaveDisabled,
			useSignatureImage,
			staticFiles,
		} = this.props;

		const { initVal } = this.state;

		const pdata = permitData;
		const udata = userData;
		return (
			<div className="pilenthLevelTechViewWrap">
				<Formik
					initialValues={initVal}
					validationSchema={DosrocharanSchema}
					onSubmit={async (values, actions) => {
						actions.setSubmitting(true);
						values.applicationNo = permitData.applicationNo;
						values.error && delete values.error;
						try {
							let url = '';
							if (values.sanso === dcs.sansodhan_check[1]) {
								url = `${api.dosrocharanSupervisor}${permitData.applicantNo}/Y`;
							} else if (values.sanso === dcs.sansodhan_check[2]) {
								url = `${api.dosrocharanSupervisor}${permitData.applicantNo}/T`;
							} else {
								url = `${api.dosrocharanSupervisor}${permitData.applicantNo}/N`;
							}

							// refetch the menu as the menu will be updated
							const params = new PostActionParams({ api: url, data: values, concatAppId: false, fetchMenu: true });
							await this.props.postAction(...params.getParams());
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							if (this.props.success && this.props.success.success) {
								handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
							}
						} catch (err) {
							actions.setSubmitting(false);
							window.scrollTo(0, 0);
							showToast('Something went wrong !!');
						}
					}}
				>
					{({ isSubmitting, values, handleChange, handleSubmit, setFieldValue, errors, validateForm }) => (
						<Form loading={isSubmitting} className="ui form">
							{reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
							<div ref={this.props.setRef}>
								<div style={{ textAlign: 'center' }}>
									<span>{dcs.heading_1}</span>
									<h2>{dcs.heading_2}</h2>
								</div>
								<div>
									<span>{dcs.sub_1.sub_1_1}</span>
									{/* <Field
										type="text"
										name="shree"
										className="dashedForm-control"
									/> */}
									{pdata.nibedakName}
									<span>{dcs.sub_1.sub_1_2}</span>
									{/* <Field
											type="text"
											name="ko"
											className="dashedForm-control"
										/> */}
									<span>{dcs.sub_1.sub_1_3}</span>
									<strong>{pdata.newWardNo}</strong>
									<span>{dcs.sub_1.sub_1_4}</span>
									<strong>{pdata.buildingJoinRoad}</strong>
									<span>{dcs.sub_1.sub_1_5}</span>
									<br />
								</div>
								{/* radio button starts */}
								<ol className="no-bullets">
									<li>
										{dcs.data_1.data_1_1}
										{/* <Field type="text" name="content1" className="dashedForm-control" /> */}
										{dcs.radioBox.map((name) => (
											<RadioInput key={name} option={name} name="sadakAdhikar" />
										))}
										{values.sadakAdhikar === dcs.radioBox[1] && (
											// <Field
											// 	type='text'
											// 	name='mapdandaPalana'
											// 	className="dashedForm-control"
											// 	setFieldValue={setFieldValue}
											// 	value={values.mapdandaPalana}
											// />
											<EbpsTextareaField
												placeholder={dcs.plor_else_placeholder}
												name="mapdandaPalana"
												setFieldValue={setFieldValue}
												value={values.mapdandaPalana}
												error={errors.mapdandaPalana}
											/>
										)}
									</li>

									<li>
										{dcs.data_1.data_1_2}
										{/* <Input name="content1" className="dashedForm-control" /> */}
										{dcs.radioBox.map((name) => (
											<RadioInput key={name} option={name} name="sitePlan" />
										))}
										{values.sitePlan === dcs.radioBox[1] && (
											// <Field
											// 	type="text"
											// 	name="byakplan"
											// 	className="dashedForm-control"
											// 	setFieldValue={setFieldValue}
											// 	value={values.byakplan}
											// />
											<EbpsTextareaField
												placeholder={dcs.plor_else_placeholder}
												name="byakplan"
												setFieldValue={setFieldValue}
												value={values.byakplan}
												error={errors.byakplan}
											/>
										)}
									</li>

									<li>
										{dcs.data_1.data_1_3}
										{/* <Input name="content1" className="dashedForm-control" /> */}
										{dcs.radioBox.map((name) => (
											<RadioInput key={name} option={name} name="groundCoverage" />
										))}
										{values.groundCoverage === dcs.radioBox[1] && (
											// <Field
											// 	type="text"
											// 	name="farakpareko"
											// 	className="dashedForm-control"
											// 	setFieldValue={setFieldValue}
											// 	value={values.farakpareko}
											// />
											<EbpsTextareaField
												placeholder={dcs.plor_else_placeholder}
												name="farakpareko"
												setFieldValue={setFieldValue}
												value={values.farakpareko}
												error={errors.farakpareko}
											/>
										)}
									</li>

									<li>
										{dcs.data_1.data_1_4}
										{/* <Input name="content1" className="dashedForm-control" /> */}
										{dcs.radioBox.map((name) => (
											<RadioInput key={name} option={name} name="xaajibar" />
										))}
										{values.xaajibar === dcs.radioBox[1] && (
											// <Field
											// 	type="text"
											// 	className="dashedForm-control"
											// 	name="nikalekoHaak"
											// 	setFieldValue={setFieldValue}
											// 	value={values.nikalekoHaak}
											// />
											<EbpsTextareaField
												placeholder={dcs.plor_else_placeholder}
												name="nikalekoHaak"
												setFieldValue={setFieldValue}
												value={values.nikalekoHaak}
												error={errors.nikalekoHaak}
											/>
										)}
									</li>

									<li>
										{dcs.data_1.data_1_5}
										{/* <Input name="content1" className="dashedForm-control" /> */}
										{dcs.radioBox.map((name) => (
											<RadioInput key={name} option={name} name="nepalRastrya" />
										))}
										{values.nepalRastrya === dcs.radioBox[1] && (
											// <Field
											// 	type="text"
											// 	name="anusarNirmaan"
											// 	className="dashedForm-control"
											// 	setFieldValue={setFieldValue}
											// 	value={values.anusarNirmaan}
											// />
											<EbpsTextareaField
												placeholder={dcs.plor_else_placeholder}
												name="anusarNirmaan"
												setFieldValue={setFieldValue}
												value={values.anusarNirmaan}
												error={errors.anusarNirmaan}
											/>
										)}
									</li>
									<li>
										{' '}
										<span>{dcs.data_1.data_1_6}</span>
										{/* <Field
												type="text"
												name="pherbadalDescription"
												className="dashedForm-control"
												setFieldValue={setFieldValue}
												value={values.pherbadalDescription}
											/> */}
										<EbpsTextareaField
											placeholder="..."
											name="pherbadalDescription"
											setFieldValue={setFieldValue}
											value={values.pherbadalDescription}
											error={errors.pherbadalDescription}
										/>
										<br />{' '}
									</li>
								</ol>
								<SansodhanCheckbox
									label={dcs.sansodhan_checklabel}
									values={values}
									handleChange={handleChange}
									options={dcs.sansodhan_check}
									constructionType={permitData.constructionType}
								/>
								<div>
									<span> {dcs.subinfo.subinfo_1}</span>
									<div>
										<SignatureImage value={values.signature} label={dcs.info_1.info_1_1} showSignature={useSignatureImage} />
									</div>
									<span>{dcs.info_1.info_1_2}</span>
									{/* <DashedLangInput
											setFieldValue={setFieldValue}
											name="pranaam"
											value={values.pranaam}
										/> */}
									{initVal.userName}
									<br />
									<span>{dcs.info_1.info_1_3}</span>
									{/* <DashedLangInput
											setFieldValue={setFieldValue}
											name="dartano"
											value={values.dartano}
										/> */}
									{/* <Field type="text" className="dashedForm-control" setFieldValue={setFieldValue} value={values.dartaNo} name="dartaNo" /> */}

									{/* need to add */}
									{initVal.licenseNo}
									<br />
									{/* <span>{dcs.info_1.info_1_4}</span>
									{/* <DashedLangInput
											setFieldValue={setFieldValue}
											name="naamxap"
											value={values.naamxap}
										/> */}
									{/* {initVal.stamp} */}
									<ImageDisplayInline label={dcs.info_1.info_1_4} src={initVal.stamp} atl="stamp" />
									{/* <br /> */}
									<span>
										{udata.organization.name}
										{dcs.info_1.info_1_5}
									</span>
									{/* <DashedLangInput
											setFieldValue={setFieldValue}
											name="patrano"
											value={values.patrano}
										/> */}
									{initVal.municipalRegNo}
									<br />
									<span>{dcs.info_1.info_1_6}</span>
									<br />
									<br />
								</div>
								<Grid divided="vertically">
									<Grid.Row columns={isKamalamai ? 3 : 2}>
										<Grid.Column>
											<Grid.Row>
												<SignatureImage showSignature={true} value={staticFiles.nirmanByabasayeSignature} />
											</Grid.Row>
											<Grid.Row>
												{' '}
												<strong>{dcs.thekdar.thekdarinfo_1}</strong>
											</Grid.Row>
											<Grid.Row>
												<span>{dcs.thekdar.thekdarinfo_2}</span>
												<DashedLangInput setFieldValue={setFieldValue} name="thekdarName" value={values.thekdarName} />
											</Grid.Row>
											<Grid.Row>
												<span>{dcs.thekdar.thekdarinfo_3}</span>
												<DashedLangInput setFieldValue={setFieldValue} name="tAddress" value={values.tAddress} />
											</Grid.Row>
											<Grid.Row>
												<span>{dcs.thekdar.thekdarinfo_4}</span>
												<DashedLangInputWithSlash setFieldValue={setFieldValue} name="tDarta" value={values.tDarta} />
											</Grid.Row>
											<Grid.Row>
												{' '}
												<span>{dcs.thekdar.thekdarinfo_5}</span>
												<DashedLangDateField
													inline={true}
													setFieldValue={setFieldValue}
													name="thekedarDate"
													value={values.thekedarDate}
													error={errors.thekedarDate}
												/>
											</Grid.Row>
										</Grid.Column>
										<Grid.Column>
											<Grid.Row>
												<SignatureImage showSignature={true} value={staticFiles.dakarmiSignature} />
											</Grid.Row>
											<Grid.Row>
												{' '}
												<strong>{dcs.mukhya.mukhya_2}</strong>
											</Grid.Row>
											<Grid.Row>
												{' '}
												<span>{dcs.mukhya.mukhya_3}</span>
												<DashedLangInput setFieldValue={setFieldValue} name="mistiriName" value={values.mistiriName} />
											</Grid.Row>
											<Grid.Row>
												{' '}
												<span>{dcs.mukhya.mukhya_4}</span>
												<DashedLangInput setFieldValue={setFieldValue} name="mAddress" value={values.mAddress} />
											</Grid.Row>
											<Grid.Row>
												<span>{dcs.mukhya.mukhya_5}</span>
												<DashedLangInputWithSlash setFieldValue={setFieldValue} name="mDarta" value={values.mDarta} />
											</Grid.Row>
											<Grid.Row>
												{' '}
												<span>{dcs.mukhya.mukhya_6}</span>
												<DashedLangDateField
													inline={true}
													setFieldValue={setFieldValue}
													name="mukhyaDakarmiDate"
													value={values.mukhyaDakarmiDate}
													error={errors.mukhyaDakarmiDate}
												/>
											</Grid.Row>
										</Grid.Column>
										{isKamalamai && (
											<Grid.Column>
												<Grid.Row>
													<SignatureImage showSignature={true} value={staticFiles.ghardhaniSignature} />
												</Grid.Row>
												<Grid.Row>
													<strong>{dcs.gharDhani.gharDhani}</strong>
												</Grid.Row>
												<Grid.Row>
													{dcs.mukhya.mukhya_3}
													<DashedLangInput
														setFieldValue={setFieldValue}
														name="applicantName"
														value={values.applicantName}
													/>
												</Grid.Row>
												<Grid.Row>
													<span>{dcs.mukhya.mukhya_6}</span>
													<DashedLangDateField
														inline={true}
														setFieldValue={setFieldValue}
														name="applicantDate"
														value={values.applicantDate}
														error={errors.applicantDate}
													/>
												</Grid.Row>
											</Grid.Column>
										)}
									</Grid.Row>
								</Grid>
							</div>

							<SaveButtonValidation
								errors={errors}
								validateForm={validateForm}
								formUrl={formUrl}
								hasSavePermission={hasSavePermission}
								hasDeletePermission={hasDeletePermission}
								isSaveDisabled={isSaveDisabled}
								prevData={checkError(prevData)}
								handleSubmit={handleSubmit}
							/>
						</Form>
					)}
				</Formik>
			</div>
		);
	}
}

const DosrocharanSupervisor = (parentProps) => (
	<FormContainerV2
		api={[
			{
				api: api.dosrocharanSupervisor,
				objName: 'dosroSuper',
				form: true,
			},
			{
				api: api.anusuchiGha,
				objName: 'anusuchi',
				form: false,
			},
			{
				api: api.plinthLevelOwnerRepresentation,
				objName: 'plinthOwner',
				form: false,
			},
		]}
		fetchFiles={true}
		prepareData={(data) => data}
		useInnerRef={true}
		onBeforeGetContent={{
			...PrintParams.REMOVE_ON_PRINT,
			...PrintParams.INLINE_FIELD,
			param1: ['getElementsByTagName', 'input', 'value'],
			param2: ['getElementsByClassName', 'ui label', 'innerText'],
			param3: ['getElementsByTagName', 'textarea', 'value'],
		}}
		parentProps={parentProps}
		render={(props) => <DosrocharanSupervisorComponent {...props} parentProps={parentProps} />}
	/>
);

export default DosrocharanSupervisor;
