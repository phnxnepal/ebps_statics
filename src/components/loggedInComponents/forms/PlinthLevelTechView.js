import React, { Component } from 'react';
import { Formik, Form, Field } from 'formik';
import { Grid } from 'semantic-ui-react';
import { DashedLangDateField } from './../../shared/DateField';
import { DashedLangInput } from '../../shared/DashedFormInput';

import api from '../../../utils/api';
import { showToast } from '../../../utils/functionUtils';
import ErrorDisplay from '../../shared/ErrorDisplay';

import { getJsonData, prepareInitialValues, handleSuccess, checkError } from '../../../utils/dataUtils';
import JSONDataMultipleGetFormContainer from '../../../containers/base/JSONDataMultipleGetFormContainer';
import { PlinthLevelTechdata } from '../../../utils/data/PlinthLevelTechdata';
import EbpsSelect from './../../shared/Select';
import EbpsTextareaField from './../../shared/MyTextArea';
import { isStringEmpty } from './../../../utils/stringUtils';
import { getCurrentDate } from './../../../utils/dateUtils';
import { getMessage } from '../../shared/getMessage';
import { getDataConstructionType, getConstructionTypeText } from '../../../utils/enums/constructionType';

const PTdata = PlinthLevelTechdata;
const messageId = 'PlinthLevelTechdata';

const opts = [
    { key: 'milne', value: 'मिल्ने', text: 'मिल्ने' },
    { key: 'namilne', value: 'नमिल्ने', text: 'नमिल्ने' }
];

class PlinthLevelTechViewComponent extends Component {
    render() {
        const {
            permitData,
            userData,
            prevData,
            otherData,
            errors: reduxErrors
        } = this.props;

        const pData = permitData;
        const uData = userData;

        const json_data = getJsonData(prevData);
        const otherJsonData = getJsonData(otherData.allowanceMaster);

        const initialValues = prepareInitialValues(
            {
                obj: permitData,
                reqFields: ['constructionType']
            },
            {
                obj: otherJsonData,
                reqFields: ['Allowancedate']
            },
            { obj: json_data, reqFields: [] }
        );

        if (isStringEmpty(initialValues.date)) {
            initialValues.date = getCurrentDate(true);
        }

        initialValues.constructionType = getConstructionTypeText(initialValues.constructionType)

        const floor1 = pData.floor.find(row => row.floor === 1);
        //console.log('floor data===', floor1);
        return (
            <>
                {reduxErrors && <ErrorDisplay message={reduxErrors.message} />}
                <br />
                <div
                    className='NJ-Main superStruConsView'
                    ref={this.props.setRef}
                >
                    <p className='tipandiMain-heading'>
                        {getMessage(`${messageId}.parmukh`, PTdata.parmukh)}
                    </p>
                    <p className='tipandiMain-heading'>
                        <span>{uData.organization.name}</span>
                    </p>
                    <p className='tipandiMain-heading'>
                        <span>{uData.organization.officeName}</span>
                    </p>
                    <p className='tipandiMain-heading'>
                        <span>{uData.organization.address}</span>
                    </p>
                    <br />
                    <div className='tipandi-letterHead'>
                        <h3 className='tipandi-title'>
                            {getMessage(`${messageId}.title`, PTdata.title)}
                        </h3>
                    </div>
                    <br />
                    <Formik
                        initialValues={initialValues}
                        onSubmit={async (values, actions) => {
                            actions.setSubmitting(true);
                            values.applicationNo = permitData.applicationNo;
                            values.error && delete values.error;
                            try {
                                await this.props.postAction(
                                    `${api.plintLevelTechApplication}${permitData.applicationNo}`,
                                    values
                                );
                                actions.setSubmitting(false);
                                window.scrollTo(0, 0);
                                if (
                                    this.props.success &&
                                    this.props.success.success
                                ) {
                                    // showToast(
                                    //     'Your data has been successfully'
                                    // );
                                    // this.props.history.push(
                                    //     getNextUrl(this.props.location.pathname)
                                    // );
                                    handleSuccess(checkError(this.props.prevData), this.props.parentProps, this.props.success);
                                }
                            } catch (err) {
                                actions.setSubmitting(false);
                                window.scrollTo(0, 0);
                                showToast('Something went wrong !!');
                            }
                        }}
                    >
                        {({
                            isSubmitting,
                            values,
                            handleChange,
                            handleSubmit,
                            errors,
                            setFieldValue
                        }) => (
                                <Form loading={isSubmitting} className='ui form'>
                                    <p>{getMessage(`${messageId}.mahodaye`, PTdata.mahodaye)}</p>
                                    <p>
                                        {getMessage(`${messageId}.bisaye`, PTdata.bisaye)}{' '}
                                        <span>{uData.organization.name}</span>{' '}
                                        {getMessage(`${messageId}.ward`, PTdata.ward)} {pData.newWardNo}{' '}
                                        {getMessage(`${messageId}.main_data`, PTdata.main_data)} {pData.nibedakName}{' '}
                                        {getMessage(`${messageId}.main_data1`, PTdata.main_data1)}{' '}
                                        <DashedLangDateField
                                            name='Allowancedate'
                                            inline={true}
                                            setFieldValue={setFieldValue}
                                            value={values.Allowancedate}
                                            label={PTdata.date}
                                            className='dashedForm-control'
                                        />
                                        {getMessage(`${messageId}.main_data2`, PTdata.main_data2)}{' '}
                                        <DashedLangInput
                                            name='DartaNo'
                                            className='dashedForm-control'
                                            setFieldValue={setFieldValue}
                                            handleChange={handleChange}
                                            value={values.DartaNo}
                                            error={errors.DartaNo}
                                            inline={true}
                                        />
                                        {getMessage(`${messageId}.main_data3`, PTdata.main_data3)}
                                        <DashedLangDateField
                                            label={PTdata.date}
                                            name='approveDate'
                                            setFieldValue={setFieldValue}
                                            value={values.date}
                                            error={errors.date}
                                            inline={true}
                                            className='dashedForm-control'
                                        />
                                        {getMessage(`${messageId}.main_data4`, PTdata.main_data4)}
                                    </p>
                                    <div style={{ textAlign: 'right' }}>
                                        <p>{getMessage(`${messageId}.main_data5`, PTdata.main_data5)}</p>
                                        <p>
                                            {getMessage(`${messageId}.name`, PTdata.name)} ,{getMessage(`${messageId}.thar`, PTdata.thar)}
                                            <DashedLangInput
                                                name='NameThar'
                                                className='dashedForm-control'
                                                setFieldValue={setFieldValue}
                                                handleChange={handleChange}
                                                value={values.NameThar}
                                                error={errors.NameThar}
                                                inline={true}
                                            />
                                        </p>
                                        <p>
                                            {getMessage(`${messageId}.position`, PTdata.position)}
                                            <DashedLangInput
                                                name='PlinthPosition'
                                                className='dashedForm-control'
                                                setFieldValue={setFieldValue}
                                                handleChange={handleChange}
                                                value={values.PlinthPosition}
                                                error={errors.PlinthPosition}
                                                inline={true}
                                            />
                                        </p>
                                        <p>
                                            <span>{uData.organization.name}</span>{' '}
                                            {getMessage(`${messageId}.mechi`, PTdata.mechi)}
                                        </p>
                                    </div>
                                    <p style={{ textAlign: 'left' }}>
                                        {getMessage(`${messageId}.sign`, PTdata.sign)}
                                        <DashedLangInput
                                            name='PlinthSign'
                                            className='dashedForm-control'
                                            setFieldValue={setFieldValue}
                                            handleChange={handleChange}
                                            value={values.PlinthSign}
                                            error={errors.PlinthSign}
                                            inline={true}
                                        />
                                    </p>
                                    <br />
                                    <h3 style={{ textAlign: 'center' }}>
                                        {getMessage(`${messageId}.tapasil`, PTdata.tapasil)}
                                    </h3>
                                    <br />
                                    <Grid>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                <h4>{getMessage(`${messageId}.side_left`, PTdata.side_left)}</h4>
                                                <p>
                                                    {getMessage(`${messageId}.side1`, PTdata.side1)}
                                                    <DashedLangInput
                                                        name='Duri'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={values.Duri}
                                                        error={errors.Duri}
                                                        inline={true}
                                                    />
                                                </p>
                                                <span>{getMessage(`${messageId}.side2`, PTdata.side2)}</span>
                                                <p style={{ paddingLeft: '30px' }}>
                                                    {getMessage(`${messageId}.side2_3`, PTdata.side2_3)} {floor1 && floor1.length}
                                                    <br />
                                                    {getMessage(`${messageId}.side2_1`, PTdata.side2_1)} {floor1 && floor1.width}
                                                    <br />
                                                    {getMessage(`${messageId}.side2_2`, PTdata.side2_2)} {floor1 && floor1.area}
                                                </p>
                                                <p>
                                                    {getMessage(`${messageId}.side3`, PTdata.side3)}
                                                    <DashedLangInput
                                                        name='PlingthHeight'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={values.PlingthHeight}
                                                        error={errors.PlingthHeight}
                                                        inline={true}
                                                    />
                                                </p>
                                                <p>
                                                    {getMessage(`${messageId}.side4`, PTdata.side4)}{' '}
                                                    {values.constructionType}
                                                </p>
                                                <p>
                                                    {getMessage(`${messageId}.side5`, PTdata.side5)}
                                                    <DashedLangInput
                                                        name='nirmanKarya'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={values.nirmanKarya}
                                                        error={errors.nirmanKarya}
                                                        inline={true}
                                                    />
                                                </p>
                                                <p>
                                                    {getMessage(`${messageId}.side6`, PTdata.side6)}
                                                    {PTdata.radioBox.map(
                                                        (name, index) => (
                                                            <div
                                                                key={index}
                                                                className='ui radio checkbox'
                                                            >
                                                                <Field
                                                                    type='radio'
                                                                    name='rastriyaBhawan'
                                                                    id={index}
                                                                    defaultChecked={
                                                                        name[0]
                                                                    }
                                                                    value={name}
                                                                    onClick={
                                                                        handleChange
                                                                    }
                                                                />
                                                                <label>
                                                                    {name}
                                                                </label>
                                                            </div>
                                                        )
                                                    )}
                                                </p>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <h4>{getMessage(`${messageId}.side_right`, PTdata.side_right)}</h4>
                                                <p>
                                                    {getMessage(`${messageId}.side7`, PTdata.side7)}
                                                    <DashedLangInput
                                                        name='PlinthDuri'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={values.PlinthDuri}
                                                        error={errors.PlinthDuri}
                                                        inline={true}
                                                    />
                                                </p>
                                                <span>{getMessage(`${messageId}.side2`, PTdata.side2)}</span>
                                                <p style={{ paddingLeft: '30px' }}>
                                                    {getMessage(`${messageId}.side2_3`, PTdata.side2_3)}
                                                    <DashedLangInput
                                                        name='Plinthlength1'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={floor1 && floor1.length}
                                                        error={errors.Plinthlength1}
                                                        inline={true}
                                                    />
                                                    <br />
                                                    {getMessage(`${messageId}.side2_1`, PTdata.side2_1)}
                                                    <DashedLangInput
                                                        name='PlinthBreadht1'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={floor1 && floor1.width}
                                                        error={
                                                            errors.PlinthBreadht1
                                                        }
                                                        inline={true}
                                                    />
                                                    <br />
                                                    {getMessage(`${messageId}.side2_2`, PTdata.side2_2)}
                                                    <DashedLangInput
                                                        name='PlinthArea1'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={floor1 && floor1.area}
                                                        error={errors.PlinthArea1}
                                                        inline={true}
                                                    />
                                                </p>
                                                <p>
                                                    {PTdata.side3}{getMessage(`${messageId}.side3`, PTdata.side3)}
                                                    <DashedLangInput
                                                        name='PlingthHeight1'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={
                                                            values.PlingthHeight1
                                                        }
                                                        error={
                                                            errors.PlingthHeight1
                                                        }
                                                        inline={true}
                                                    />
                                                </p>
                                                <p>
                                                    {getMessage(`${messageId}.side8`, PTdata.side8)}
                                                    <DashedLangInput
                                                        name='constructionType'
                                                        className='dashedForm-control'
                                                        setFieldValue={
                                                            setFieldValue
                                                        }
                                                        handleChange={handleChange}
                                                        value={
                                                            values.constructionType
                                                        }
                                                        error={errors.constructionType}
                                                        inline={true}
                                                    />
                                                </p>
                                                <br />
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                    <p>
                                        {getMessage(`${messageId}.main_data6`, PTdata.main_data6)}
                                        <EbpsSelect
                                            name='Milne'
                                            options={opts}
                                            setFieldValue={setFieldValue}
                                            value={values.Milne}
                                        />
                                        {getMessage(`${messageId}.main_data7`, PTdata.main_data7)}
                                    </p>
                                    <EbpsTextareaField
                                        name='PlinthTextArea'
                                        setFieldValue={setFieldValue}
                                        onChange={handleChange}
                                        value={values.PlinthTextArea}
                                    />
                                </Form>
                            )}
                    </Formik>
                </div>
            </>
        );
    }
}

const PlinthLevelTechComponent = parentProps => (
    <JSONDataMultipleGetFormContainer
        api={[
            {
                api: api.plintLevelTechApplication,
                objName: 'plinthTechApp',
                form: true
            },
            {
                api: api.anusuchiKaMaster,
                objName: 'anukaMaster',
                form: false,
                utility: true
            },
            {
                api: api.allowancePaper,
                objName: 'allowanceMaster',
                form: false
            }
        ]}
        prepareData={data => data}
        parentProps={parentProps}
        onBeforeGetContent={{
            param1: ['getElementsByTagName', 'input', 'value'],
            param4: ['getElementsByTagName', 'textarea', 'value'],
            param2: ['getElementsByTagName', 'span', 'innerText'],
            param3: ['getElementsByClassName', 'ui label', 'innerText']
        }}
        render={props => (
            <PlinthLevelTechViewComponent
                {...props}
                parentProps={parentProps}
            />
        )}
    />
);

export default PlinthLevelTechComponent;
