import React, { Component } from 'react';
import { Form, Button, Header, Grid, Segment, Icon } from 'semantic-ui-react';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { showToast } from './../../utils/functionUtils';
import { connect } from 'react-redux';
import { postPassword } from '../../store/actions/loginForm';
import ErrorDisplay from './../shared/ErrorDisplay';
import Helmet from 'react-helmet';
import { validateString, validatePassword } from '../../utils/validationUtils';
import { passwordErrors } from '../../utils/data/validationData';

const Schema = Yup.object().shape({
	oldPassword: validateString,
	newpassword: validatePassword,
	confirmpassword: Yup.string().when('newpassword', {
		is: (val) => (val && val.length > 0 ? true : false),
		then: Yup.string().oneOf([Yup.ref('newpassword')], passwordErrors.passwordMustMatch),
	}),
});

class Changepass extends Component {
	constructor(props) {
		super(props);

		this.state = {
			hidden: {
				0: true,
				1: true,
				2: true,
			},
			names: {
				0: 'eye',
				1: 'eye',
				2: 'eye',
			},
		};
		this.handlePasswordChange = this.handlePasswordChange.bind(this);
		this.toggleShow = this.toggleShow.bind(this);
	}

	handlePasswordChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	toggleShow = (id) => {
		this.setState((prevState) => ({
			hidden: {
				...prevState.hidden,
				[id]: !prevState.hidden[id],
			},
		}));
		const hidden = this.state.hidden;
		if (hidden[id] === true) {
			let sub = 'eye slash';
			this.setState((prevState) => ({
				names: {
					...prevState.names,
					[id]: sub,
				},
			}));
		} else {
			let sub = 'eye';
			this.setState((prevState) => ({
				names: {
					...prevState.names,
					[id]: sub,
				},
			}));
		}
	};

	render() {
		return (
			<Formik
				initialValues={{
					oldPassword: '',
					newpassword: '',
					confirmpassword: '',
					ui: {},
				}}
				validationSchema={Schema}
				onSubmit={async (values, actions) => {
					actions.setSubmitting(true);
					const data = new FormData();
					// console.log(data);

					data.append('password', values.newpassword);
					data.append('oldPassword', values.oldPassword);
					try {
						await this.props.postPassword(data);

						window.scrollTo(0, 0);

						if (this.props.success && this.props.success.success) {
							showToast('Your password has been successfully changed.');
						}
						actions.setSubmitting(false);
					} catch (err) {
						setTimeout(() => {
							actions.setSubmitting(false);
						}, 3000);
						window.scrollTo(0, 0);
					}
				}}
			>
				{({ values, errors, handleSubmit, handleChange, handleBlur, isSubmitting }) => {
					return (
						<Form onSubmit={handleSubmit} size="large" loading={isSubmitting} className="adminLogin-form">
							<Helmet>
								<title>Change Password</title>
							</Helmet>
							<div>
								<Grid textAlign="center" style={{ height: '80vh' }} verticalAlign="middle">
									<Grid.Column style={{ maxWidth: 450 }}>
										<Header as="h2" color="teal" textAlign="center">
											Change Password
										</Header>
										<Form size="large" className="adminLogin-form">
											{this.props.errors && <ErrorDisplay message={this.props.errors.message} />}
											<Segment stacked className="change-password-segment">
												<Form.Field error={errors.oldPassword}>
													<label>Old Password</label>
													<Form.Input
														icon={<Icon name={this.state.names[0]} link onClick={() => this.toggleShow(0)} />}
														iconPosition="right"
														type={this.state.hidden[0] ? 'password' : 'text'}
														name="oldPassword"
														value={values.oldPassword}
														onChange={handleChange}
														onBlur={handleBlur}
														placeholder="Old Password"
													/>
													{errors.oldPassword && <span className="tableError">{errors.oldPassword}</span>}
												</Form.Field>
												<Form.Field error={errors.newpassword}>
													<label>New Password</label>
													<Form.Input
														icon={<Icon name={this.state.names[1]} link onClick={() => this.toggleShow(1)} />}
														iconPosition="right"
														type={this.state.hidden[1] ? 'password' : 'text'}
														name="newpassword"
														value={values.newpassword}
														onChange={handleChange}
														onBlur={handleBlur}
														placeholder="New Password"
													/>
													{errors.newpassword && <span className="tableError english-div">{errors.newpassword}</span>}
												</Form.Field>
												<Form.Field error={errors.confirmpassword}>
													<label>Confirm New Password</label>
													<Form.Input
														icon={<Icon name={this.state.names[2]} link onClick={() => this.toggleShow(2)} />}
														iconPosition="right"
														onBlur={handleBlur}
														type={this.state.hidden[2] ? 'password' : 'text'}
														name="confirmpassword"
														value={values.confirmpassword}
														onChange={handleChange}
														placeholder="Confirm Password"
													/>
													{errors.confirmpassword && <span className="tableError">{errors.confirmpassword}</span>}
												</Form.Field>
												<Button fluid size="large" type="submit">
													Submit
												</Button>
											</Segment>
											{/* {errors.oldPassword && touched.oldPassword ? (
												<Label
													basic
													color="red"
													pointing="left"
													style={{
														top: '-227px',
														right: '-282px',
													}}
												>
													{errors.oldPassword}
												</Label>
											) : (
												''
											)}
											{errors.newpassword && touched.newpassword ? (
												<Label
													basic
													color="red"
													pointing="left"
													style={{
														top: '-176px',
														right: '-282px',
													}}
												>
													{errors.newpassword}
												</Label>
											) : (
												''
											)}
											{errors.confirmpassword && touched.confirmpassword ? (
												<Label
													basic
													color="red"
													pointing="left"
													style={{
														top: '-122px',
														right: '-324px',
													}}
												>
													{errors.confirmpassword}
												</Label>
											) : (
												''
											)} */}
										</Form>
									</Grid.Column>
								</Grid>
							</div>
						</Form>
					);
				}}
			</Formik>
		);
	}
}

const mapStateToProps = (state) => ({
	loading: state.root.ui.loading,
	errors: state.root.ui.errors,
	success: state.root.formData.success,
});

const mapDispatchToProps = { postPassword };

export default connect(mapStateToProps, mapDispatchToProps)(Changepass);
