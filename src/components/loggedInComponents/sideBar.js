import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Input, Header, Icon, Divider } from 'semantic-ui-react';
import { getStartingGroupIdPostionFromMenuList } from '../../utils/functionUtils';
import { getGroupMasterObj } from '../../utils/urlUtils';
import { isStringEmpty } from '../../utils/stringUtils';
import { getLocalStorage } from '../../utils/secureLS';
import { MENU_LIST } from '../../utils/constants';
import { FormUrlFull } from '../../utils/enums/url';
import { FlexSpaceBetween } from '../uiComponents/FlexDivs';

const childPaths = [FormUrlFull.STRUCTURE_B, FormUrlFull.STRUCTURE_C, FormUrlFull.ARCHITECTURE_B, FormUrlFull.ARCHITECTURE_C];

class MainSideBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			routes: [],
			menu: [],
		};
	}

	componentDidMount() {
		const menu = !isStringEmpty(getLocalStorage(MENU_LIST)) ? JSON.parse(getLocalStorage(MENU_LIST)) : [];
		this.setState({ menu });

		this.handleScrollToActive();

		const childBearingMenu = document.querySelector('.has-children');
		if (childPaths.includes(this.props.history.location.pathname) && childBearingMenu) {
			this.setState({ [childBearingMenu.dataset.key]: true });
		}
	}

	componentDidUpdate(prevProps) {
		this.handleScrollToActive();

		if (prevProps.location.pathname !== this.props.location.pathname) {
			const childBearingMenu = document.querySelector('.has-children');
			if (childPaths.includes(this.props.history.location.pathname) && childBearingMenu) {
				this.setState({ [childBearingMenu.dataset.key]: true });
			}
			// if (childBearingMenu) {
			// 	const activeChild = childBearingMenu.querySelectorAll('ul li a.sideBar.active');
			// 	console.log('active child', childBearingMenu, activeChild);
			// 	if (activeChild.length > 0) {
			// 		this.setState({ [childBearingMenu.dataset.key]: true });
			// 	}
			// }
		}
	}

	handleScrollToActive = () => {
		const mapScroll = document.querySelector('.sideBar.active');
		mapScroll && mapScroll.scrollIntoView({ behavior: 'smooth', block: 'start' });
	};

	handleChildrenActive = (key, toggle) => {
		this.setState(() => ({
			[key]: toggle,
		}));
	};
	handleChildrenActiveToggle = (key) => {
		this.setState((prevState) => ({
			[key]: !prevState[key],
		}));
	};

	groupGenerate = (id) => {
		let groupMaster = getGroupMasterObj();
		const group = groupMaster.find((group) => group.id === id);
		if (group) {
			return group.name;
		} else {
			return '';
		}
	};

	render() {
		const { routes } = this.props;
		const currentPath = this.props.history.location.pathname;

		let startingGroupPositions = {};
		if (routes) {
			startingGroupPositions = getStartingGroupIdPostionFromMenuList();
		}
		let sidebar = routes.map((prop, key) => {
			if (!prop.hasChild) {
				const groupId = prop.url === '/user/forms/forward-to-next' ? `/${prop.groupId}` : '';
				const pathUrl = prop.url ? `${prop.url}${groupId}` : `${prop.layout}${prop.path}`;
				return (
					<React.Fragment key={key}>
						{startingGroupPositions.some(
							(obj) => obj && String(obj.position) === String(prop.position) && String(obj.groupId) === String(prop.groupId)
						) && (
							<Header as="h4" className="header-removemargin">
								{this.groupGenerate(prop.groupId, prop.position)}
							</Header>
						)}
						<li key={key}>
							<NavLink to={pathUrl} className="sideBar">
								{`${prop.position ? prop.position + '.' : ''} ${prop.name}`}
							</NavLink>
							<Divider style={{ margin: 0 }} />
						</li>{' '}
					</React.Fragment>
				);
			} else {
				let hasChildrenToggle = false;
				if (key in this.state) {
					hasChildrenToggle = this.state[key];
				}
				return (
					<li key={key} data-key={key} className="has-children">
						<span
							className={hasChildrenToggle ? 'has-item-children span-link' : 'span-link'}
							onClick={() => this.handleChildrenActiveToggle(key)}
						>
							{prop.name}
						</span>
						{!hasChildrenToggle ? (
							<span style={{ position: 'absolute', right: 0, top: 5 }}>
								<Icon name="angle right" onClick={() => this.handleChildrenActive(key, true)} />
							</span>
						) : (
							<span style={{ position: 'absolute', right: 0, top: 5 }}>
								<Icon name="angle down" onClick={() => this.handleChildrenActive(key, false)} />
							</span>
						)}
						{hasChildrenToggle === true ? (
							<ul>
								{prop.children.map((child, key) => (
									<li key={key}>
										<NavLink to={`${child.url}`} className={`sideBar ${currentPath === prop.path ? 'active' : ''}`}>
											{`${child.position}. ${child.name}`}
										</NavLink>
										<Divider style={{ margin: 0 }} />
									</li>
								))}
							</ul>
						) : null}
						<Divider style={{ margin: 0 }} />
					</li>
				);
			}
		});

		return (
			<aside className="main-sidebar-navigation">
				<div className="sidebarSearchBar">
					<FlexSpaceBetween>
						<Input placeholder="Search..." name="search" className="sidebar-search" onChange={this.props.handleSearch} />
						<Icon title="हालको मेनु फोकस गर्नुहोस्" name="compress" className="focus-icon" inverted onClick={this.handleScrollToActive} />
					</FlexSpaceBetween>
				</div>
				<nav className="sidebar-navigation">
					<ul className="sidebar-primary-menus">{sidebar}</ul>
				</nav>
			</aside>
		);
	}
}

export default MainSideBar;
