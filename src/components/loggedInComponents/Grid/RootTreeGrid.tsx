import * as React from 'react';

import JqxTreeGrid, {
  ITreeGridProps,
  jqx
} from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxtreegrid';

class RootGrid extends React.PureComponent<{}, ITreeGridProps> {
  public render() {
    return (
      <div>
        <JqxTreeGrid
          //@ts-ignore
          ref={this.props.Grid}
          // @ts-ignore
          width={'100%'}
          //@ts-ignore
          onRowClick={this.props.onRowClick}
          //@ts-ignore
          onRowSelect={this.props.onRowSelect}
          height={350}
          // height={window.innerHeight}
          columnsResize={true}
          autoRowHeight={false}
          //@ts-ignore
          onRowSelect={this.props.onRowSelect}
          // selectionMode={'multipleRows'}
          // altRows={true}
          source={
            new jqx.dataAdapter({
              //@ts-ignore
              datafields: this.props.datafield,
              datatype: 'json',
              id: 'id',
              //@ts-ignore
              localdata: this.props.localdata,
              hierarchy: {
                //@ts-ignore
                keyDataField: { name: this.props.child },
                //@ts-ignore
                parentDataField: { name: this.props.parent }
              }
            })
          }
          //@ts-ignore
          columns={this.props.columns}
          showToolbar={true}
          //@ts-ignore
          renderToolbar={this.props.rendertoolbar}
          sortable={true}
          //@ts-ignore
          rendered={this.props.rendered}
          //@ts-ignore
          // ready={this.props.ready}
        />
      </div>
    );
  }
}

export default RootGrid;
