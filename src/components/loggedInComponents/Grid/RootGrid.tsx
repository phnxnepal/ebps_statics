import * as React from 'react';

import JqxGrid, {
	IGridProps,
	jqx
} from 'jqwidgets-scripts/jqwidgets-react-tsx/jqxgrid';
import { translate, getInputType } from '../../../utils/langUtils';
interface RootGridProps {
	localdata: object[];
}

class RootGrid extends React.PureComponent<{}, IGridProps & RootGridProps> {

	componentDidMount() {
		const filterLabels = document.querySelectorAll('.jqx-dropdownlist-content');

		//@ts-ignore
		Array.from(filterLabels).forEach(el => el.innerText = 'Select')
		
		const filterFields = document.getElementsByClassName('jqx-filter-input');

		for (var i = 1; i <= filterFields.length; i++) {
			if (filterFields[i]) {
				filterFields[i].addEventListener('input', function() {
					//@ts-ignore
					this.value = translate(this.value, getInputType());
				});
			}
		}
	}
	
	componentDidUpdate() {
		const filterLabels = document.querySelectorAll('.jqx-dropdownlist-content');

		//@ts-ignore
		Array.from(filterLabels).forEach(el => el.innerText = 'Select')
		
		const filterFields = document.getElementsByClassName('jqx-filter-input');

		for (var i = 1; i <= filterFields.length; i++) {
			if (filterFields[i]) {
				filterFields[i].addEventListener('input', function() {
					//@ts-ignore
					this.value = translate(this.value, getInputType());
				});
			}
		}
	}

	componentWillUnmount() {
		const filterFields = document.getElementsByClassName('jqx-filter-input');
		for (var i = 1; i <= filterFields.length; i++) {
			if (filterFields[i]) {
				filterFields[i].removeEventListener('input', function() {
					//@ts-ignore
					this.value = translate(this.value, getInputType());
				});
			}
		}
	}

	public render() {
		return (
			<div>
				{/*
         //@ts-ignore */}
				<JqxGrid
					//@ts-ignore
					ref={this.props.Grid}
					// @ts-ignore
					width={'100%'}
					//@ts-ignore
					// onRowselect={this.props.onRowselect}
					// selectionmode={'checkbox'}
					//@ts-ignore
					groups={this.props.groups || []}
					//@ts-ignore
					groupable={this.props.groupable}
					source={
						new jqx.dataAdapter({
							//@ts-ignore
							datafields: this.props.datafield,
							datatype: 'json',
							id: 'id',
							//@ts-ignore
							localdata: this.props.localdata
						})
					}
					//@ts-ignore
					columns={this.props.columns}
					// showtoolbar={true}
					//@ts-ignore
					// rendertoolbar={this.props.rendertoolbar}
					autoheight={true}
					// @ts-ignore
					// autorowheight={this.props.autorowheight}
					sortable={true}
					altrows={true}
					// enabletooltips={true}
					//@ts-ignore
					pageable={this.props.pageable !== undefined ? this.props.pageable :  true}
					columnsresize={true}
					rowsheight={35}
					filterable={true}
					// @ts-ignore
					showfilterrow={this.props.showfilterrow}
					//@ts-ignore
					onRowselect={e => {
						// console.log(e);
						//@ts-ignore
						this.props.onRowClick && this.props.onRowClick(e);
						// console.log(e);
					}}
				/>
			</div>
		);
	}
}

export default RootGrid;
