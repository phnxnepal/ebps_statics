import React, { useEffect, useState } from 'react';
import { Formik } from 'formik';
import { Form, Button, Header, Icon } from 'semantic-ui-react';

import { applicationTable, activityTable, searchData } from '../../../utils/data/genericSetupData';
import EbpsForm, { EbpsNormalForm } from '../../shared/EbpsForm';
import { SelectInput } from '../../shared/Select';
import { consTypeWithDefaultSelectOptions, apiMethodOptions, applicationStatusOptions } from '../../../utils/optionUtils';
import { useFiscalYearAll } from '../../../hooks/useFiscalYear';

import * as Yup from 'yup';
import api from '../../../utils/api';
import { EbpsFormDateField } from '../../shared/DateField';
import { normalNepaliDate } from '../../../utils/validationUtils';
import { getCurrentDate, getPriorDate } from '../../../utils/dateUtils';
import { commonMessages } from '../../../utils/data/validationData';
import { optionDefaults } from '../../../utils/data/genericData';

const applicationData = applicationTable;

const initialValues = {
	nibedakName: '',
	constructionType: '',
	year: '',
	kittaNo: '',
	wardNo: '',
	applicationNo: '',
	applicationStatus: '',
};

const activityInitialValues = {
	api: '',
	method: '',
	dateFrom: getPriorDate({ month: 1 }),
	dateTo: getCurrentDate(),
	loginBy: '',
};

const dateValidation = Yup.object().shape({
	dateFrom: normalNepaliDate.required(commonMessages.required),
	dateTo: normalNepaliDate.required(commonMessages.required),
});

const SearchForm = ({ heading = searchData.searchApp, fiscalYears, wardMaster, loading, getDataByUrl, api, searchParams = { ...initialValues } }) => {
	const { fy, fyOption } = useFiscalYearAll(fiscalYears, 'yearCode', true);
	const [wardOptions, setWardOptions] = useState([]);

	useEffect(() => {
		if (wardMaster && wardMaster.length > 0) {
			setWardOptions(
				[{ key: '', text: optionDefaults.allWards, value: '' }].concat(
					wardMaster.map(ward => {
						return { value: ward.name, text: ward.name, key: ward.id };
					})
				)
			);
		}
	}, [wardMaster]);

	return (
		<Formik
			key="get-app-info"
			enableReinitialize
			initialValues={{ ...searchParams, year: fy }}
			// validationSchema={searchSchema}
			onSubmit={(values, actions) => {
				actions.setSubmitting(true);
				// delete values.applicationNo;
				// values.applicationStatus = '';
				try {
					getDataByUrl(
						[
							{
								// api: `${api.applicationFilter}?nibedakName=${values.nibedakName}&constructionType=${values.constructionType}&year=${values.year}&kittaNo=${values.kittaNo}&wardNo=${values.wardNo}&applicationStatus=`,
								api,
								objName: 'applicationList',
							},
						],
						values
					);
					actions.setSubmitting(false);
				} catch (err) {
					actions.setSubmitting(false);
					console.log('err', err);
				}
			}}
		>
			{({ handleSubmit, errors, isSubmitting, values, setFieldValue, handleChange, handleReset }) => (
				// <Segment>
				<Form loading={isSubmitting || loading} className="search-form">
					<Header className="search-header">
						<Icon name="search" />
						{heading}
					</Header>
					<Form.Group>
						<Form.Field width="2">
							<EbpsNormalForm
								name="applicationNo"
								label={applicationData.applicationId}
								onChange={handleChange}
								errors={errors.applicationNo}
								value={values.applicationNo}
							/>
						</Form.Field>
						<Form.Field width="3">
							<EbpsForm
								name="nibedakName"
								label={applicationData.nibedakName}
								setFieldValue={setFieldValue}
								errors={errors.nibedakName}
								value={values.nibedakName}
							/>
						</Form.Field>
						<Form.Field width="2">
							<SelectInput
								compact={true}
								needsZIndex={true}
								name="constructionType"
								label={applicationData.constructionType}
								options={consTypeWithDefaultSelectOptions}
							/>
						</Form.Field>
						<Form.Field width="3">
							<SelectInput needsZIndex={true} compact={true} name="year" label={applicationData.year} options={fyOption} />
						</Form.Field>
						<Form.Field width="2">
							<EbpsForm
								name="kittaNo"
								label={applicationData.kittaNo}
								setFieldValue={setFieldValue}
								errors={errors.kittaNo}
								value={values.kittaNo}
							/>
						</Form.Field>
						<Form.Field width="2">
							<SelectInput compact={true} needsZIndex={true} name="wardNo" label={applicationData.wardNo} options={wardOptions} />
							{/* <EbpsForm
								name="wardNo"
								label={applicationData.wardNo}
								setFieldValue={setFieldValue}
								errors={errors.wardNo}
								value={values.wardNo}
							/> */}
						</Form.Field>
						<Form.Field width="2">
							<SelectInput
								needsZIndex={true}
								compact={true}
								name="applicationStatus"
								label={applicationData.applicationStatus}
								options={applicationStatusOptions}
							/>
						</Form.Field>
					</Form.Group>
					{/* <Form.Group widths="4"> */}
					{/* </Form.Group> */}
					<Form.Group widths="6">
						<Form.Field width="6">
							{/* <label></label> */}
							<Button
								type="button"
								className="primary-btn"
								icon="search"
								size="tiny"
								content={searchData.search}
								onClick={handleSubmit}
							/>
							{/* </Form.Field> */}
							{/* <Form.Field> */}
							<Button
								type="button"
								className="secondary-btn"
								icon="refresh"
								size="tiny"
								content={searchData.reset}
								onClick={handleReset}
							/>
						</Form.Field>
					</Form.Group>
				</Form>
				// </Segment>
			)}
		</Formik>
	);
};

export const ActivitySearchForm = ({ heading, apiList, userList, loading, getDataByUrl }) => {
	return (
		<Formik
			key="get-app-info"
			enableReinitialize
			initialValues={activityInitialValues}
			validationSchema={dateValidation}
			onSubmit={(values, actions) => {
				actions.setSubmitting(true);
				try {
					getDataByUrl(
						[
							{
								api: api.activity,
								// api: `${api.activity}?dateFrom=2076-05-01&dateTo=2076-10-01&loginBy=&method=GET&api=`,
								objName: 'activity',
							},
						],
						values
					);
					actions.setSubmitting(false);
				} catch (err) {
					actions.setSubmitting(false);
					console.log('err', err);
				}
			}}
		>
			{({ handleSubmit, errors, isSubmitting, values, handleChange, handleReset }) => (
				// <Segment>
				<Form loading={isSubmitting || loading} className="search-form english-div">
					<Header className="search-header">
						<Icon name="search" />
						{heading}
					</Header>
					<Form.Group widths="4">
						<Form.Field>
							<SelectInput needsZIndex={true} name="loginBy" label={activityTable.loginBy} options={userList} />
						</Form.Field>
						<Form.Field>
							<SelectInput needsZIndex={true} name="api" label={activityTable.api} options={apiList} />
						</Form.Field>
						<Form.Field width="2">
							<SelectInput compact={true} needsZIndex={true} name="method" label={activityTable.method} options={apiMethodOptions} />
						</Form.Field>
						<Form.Field>
							<EbpsFormDateField
								name="dateFrom"
								label={activityTable.dateFrom}
								handleChange={handleChange}
								error={errors.dateFrom}
								value={values.dateFrom}
							/>
						</Form.Field>
						<Form.Field>
							<EbpsFormDateField
								name="dateTo"
								label={activityTable.dateTo}
								handleChange={handleChange}
								error={errors.dateTo}
								value={values.dateTo}
							/>
						</Form.Field>
						<Form.Field width="5">
							<label></label>
							<Button type="button" className="primary-btn" icon="search" content="Search" onClick={handleSubmit} />
							{/* </Form.Field> */}
							{/* <Form.Field> */}
							{/* <label></label> */}
							<Button type="button" title="Reset" className="secondary-btn" icon="refresh" content="Reset" onClick={handleReset} />
						</Form.Field>
					</Form.Group>
					{/* <Form.Group widths="4" style={{ alignItems: 'center' }}> */}
					{/* </Form.Group> */}
					{/* <Form.Group widths="6"> */}
					{/* </Form.Group> */}
				</Form>
				// </Segment>
			)}
		</Formik>
	);
};

export default SearchForm;
