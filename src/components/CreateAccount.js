import React, { useEffect, useRef, useState } from 'react';
import * as Yup from 'yup';
import { Segment, Button, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { AddUserForm } from './ebps-setup/forms/AddUserForm';
import { userTypeDesignerOption } from '../utils/optionUtils';
import { userdata } from '../utils/data/ebps-setup/userformdata';
import { Formik } from 'formik';
import { showErrorToast, showToast } from '../utils/functionUtils';
import { validateString, validateNullableNumber, validateNotReqdPhone } from '../utils/validationUtils';
import { emailMessage, commonMessages } from '../utils/data/validationData';
import { postByUrl, postCatchaVerify } from '../store/actions/PublicAction';
import api from '../utils/api';
import ErrorDisplay from './shared/ErrorDisplay';
import { ReCaptcha } from 'react-recaptcha-v3';
import { getRecaptchaSiteKey } from '../utils/config';
import { ErrorBoundary } from './shared/ErrorBoundary';

const us = userdata.us_data;

const strData = ['userName', 'userType', 'address', 'loginId', 'educationQualification', 'status'];
const phoneData = ['municipalRegNo'];

const strSchema = strData.map((rows) => {
	return {
		[rows]: validateString,
	};
});

const phoneDataSchema = phoneData.map((row) => {
	return {
		[row]: validateNullableNumber,
	};
});

const UsersSchema = Yup.object().shape(
	Object.assign(
		{
			email: Yup.string().email(emailMessage.incorrect).required(commonMessages.required),
			mobile: validateNotReqdPhone,
		},
		...strSchema,
		...phoneDataSchema
	)
);

const initVal = {
	userName: '',
	address: '',
	mobile: '',
	email: '',
	educationQualification: '',
	licenseNo: '',
	loginId: '',
	municipalRegNo: '',
	userType: 'D',
	status: 'Y',
	consultancyName: '',
};

const CreateAccount = ({ success, errors, loading, postByUrl, postCatchaVerify }) => {
	const [captchaVerified, setCaptchaVerified] = useState(false);
	const [recaptchaInitialized, setRecaptchaInitialized] = useState(true);
	const formRef = useRef();
	const captchaRef = useRef();

	useEffect(() => {
		const successObj = success;
		if (successObj && successObj.success) {
			showToast('तपाईंको खाता सिर्जना गरिएको छ। कृपया प्रशासकीय स्वीकृतिको लागि प्रतीक्षा गर्नुहोस्।');
			formRef.current.resetForm(initVal);
		}
	}, [success]);

	// useEffect(() => {
	// 	try {
	// 		if (window.grecaptcha) {
	// 			console.log('window', window.grecaptcha);
	// 			window.grecaptcha.getResponse();
	// 			setRecaptchaInitialized(true);
	// 		} else {
	// 			console.log('oi');
	// 			setRecaptchaInitialized(false);
	// 		}
	// 	} catch (err) {
	// 		console.log('err', err);
	// 		setRecaptchaInitialized(false);
	// 	}
	// }, []);

	// console.log('rerere', captchaRef);

	return (
		<>
			<div className="public-div">
				<div style={{ margin: '0 100px 0 100px' }}>
					<>
						{errors && <ErrorDisplay message={errors.message}></ErrorDisplay>}

						<Segment attached className="tableSectionHeader">
							<Icon name="user plus" /> Create Designer Account
						</Segment>
						<br />
						<Formik
							ref={formRef}
							enableReinitialize
							validationSchema={UsersSchema}
							initialValues={initVal}
							onSubmit={async (values, actions) => {
								captchaRef.current && captchaRef.current.execute();
								actions.setSubmitting(true);

								const data = new FormData();
								const selectedSignFile = values.userSignature;
								const selectedStampFile = values.userStamp;
								const selectedFile = values.userImage;

								if (selectedFile) {
									selectedFile.forEach((file) => data.append('userImage', file));
								} else {
									data.append('userImage', null);
								}
								if (selectedSignFile) {
									selectedSignFile.forEach((file) => data.append('userSignature', file));
								} else {
									data.append('userSignature', null);
								}
								if (selectedStampFile) {
									selectedStampFile.forEach((file) => data.append('userStamp', file));
								} else {
									data.append('userStamp', null);
								}

								Object.keys(values).forEach((key) => {
									if (Object.keys(initVal).includes(key)) {
										data.append(key, values[key]);
									}
								});

								try {
									postByUrl(api.createAccount, data);
									actions.setSubmitting(false);
								} catch (error) {
									console.log('Error in Designer post', error);
									showErrorToast('Something went wrong.');
									actions.setSubmitting(false);
								}
							}}
						>
							{({ handleSubmit, isSubmitting, errors: formErrors, handleChange, setFieldValue, values }) => (
								<>
									<AddUserForm
										loading={isSubmitting || loading}
										labelData={us}
										errors={formErrors}
										handleChange={handleChange}
										setFieldValue={setFieldValue}
										values={values}
										userTypeOption={userTypeDesignerOption}
										isEditing={false}
										existingImages={{}}
									/>
									<div className="recaptcha-branding">
										{/**
										 * You are allowed to hide the badge as long as you include the reCAPTCHA
											branding visibly in the user flow
											@see [https://developers.google.com/recaptcha/docs/faq#id-like-to-hide-the-recaptcha-badge.-what-is-allowed]
										 */}
										This site is protected by reCAPTCHA and the Google
										<a href="https://policies.google.com/privacy"> Privacy Policy</a> and
										<a href="https://policies.google.com/terms"> Terms of Service</a> apply.
									</div>
									<ErrorBoundary hideError={true}>
										{recaptchaInitialized && (
											<ReCaptcha
												ref={captchaRef}
												sitekey={getRecaptchaSiteKey()}
												action="submit"
												verifyCallback={(token) => {
													if (token) {
														postCatchaVerify(token);
														setCaptchaVerified(true);
													}
													console.log('token', token);
												}}
											/>
										)}
									</ErrorBoundary>
									<Button
										primary
										type="submit"
										disabled={!captchaVerified && recaptchaInitialized}
										icon="user plus"
										labelPosition="right"
										content="Sign Up"
										onClick={handleSubmit}
									/>
								</>
							)}
						</Formik>
					</>
				</div>
			</div>
		</>
	);
};

const mapStateToProps = (state) => ({
	loading: state.root.ui.loading,
	success: state.root.public.success,
	errors: state.root.ui.errors,
	userData: state.root.public.userData,
});

export default connect(mapStateToProps, { postByUrl, postCatchaVerify })(CreateAccount);
