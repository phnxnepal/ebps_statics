import React from 'react';
import PublicHeader from './publicComponents/PublicHeader';
import Home from './publicComponents/Home';

export default function StartPage() {
  return (
    <>
      <PublicHeader />
      <Home />
    </>
  );
}
