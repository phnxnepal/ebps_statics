export interface UIState {
    loading: boolean;
    errors: ErrorState;
}

export interface ErrorState {
    message: string;
}