import { NamsariStatus } from "../utils/enums/apiTypes";

export interface PermitObject {
    constructionType: string;
    nameTransaferId: string;
    isRejected: string;
    applicantNo: number;
    applicationActionBy: string;
    yourAction: string;
    yourForm: string;
    designerChangeStatus?: string;
    newDesignerName?: string;
    naamsariStatus?: NamsariStatus
}

export interface FiscalYear {
    yearCode: number;
    yearName: string;
}

export interface WardMaster {
    id: string;
    name: string;
}

export interface OrganizationUser {
    userName: string;
}

export interface ApplicationListRawObject {
    yourStatus: string;
    applicantNo: number;
    constructionType: string;
    applicantAddress: string;
    yourForm: string;
    applicantMs: string;
    applicationActionBy: string;
    forwardTo: string;
    applicantName: string;
    applicantMobileNo: string;
    yourAction: {};
    nibedakName: string;
    applicantDate: string;
    applicationStatus: string;
    applicationAction: { formName: string };
    nameTransaferId: string;
    isRejected: string;
    id: string;
    naamsariStatus?: NamsariStatus
}

export interface ApplicationListObject {
    yourStatus: string;
    applicantNo: number;
    constructionType: string;
    applicantAddress: string;
    yourForm: string;
    applicantMs: string;
    applicationActionBy: string;
    applicationFormName: string;
    forwardTo: string;
    applicantName: string;
    applicantMobileNo: string;
    yourAction: {};
    nibedakName: string;
    applicantDate: string;
    applicationStatus: string;
    applicationAction: {};
    nameTransaferId: string;
    isRejected: string;
    id: string;
}

export interface ReportObject {
    position: number;
    label: string;
    count: string;
}