export interface FiscalYearOption {
    id: number,
    value: number,
    text: string,
}

export interface OptionType {
    id: string,
    value: string,
    text: string,
}