.underlined-centered-subject {
	text-align: center;
	margin: 0 0 3rem !important;
	span {
		border-bottom: 2px solid $btn-primaryBgColor;
	}
}

.signature-div {
	margin-top: 40px !important;
	margin-left: 20px !important;
	margin-right: 20px !important;

	p,
	div {
		margin: 0 !important;
	}
	.signature-date {
		.fields > .field {
			padding: 0 !important;
		}
	}
}

.signature-div.extra-top-margin {
	margin-top: 60px !important;
}

.ol-div {
	margin: 0.5em 0 0 1.5em;
}

span.ui.input.signature-placeholder {
	border-bottom: 1px dashed rgba(34, 36, 38, 0.5);
	min-width: 150px;
	margin-bottom: 5px;
	line-height: 1.21428571em;
}

.no-margin-field {
	.inline-group {
		margin: 0 -0.5em 0em !important;
	}
}

.no-margin-date {
	.inline-group {
		margin: 0 !important;
		// vertical-align: bottom;
		.dashedForm-control[type='text'] {
			margin: 0 !important;
		}
	}
}

.flex-item-flex-start {
	display: flex;
	// justify-content: flex-start;
	align-items: flex-start;
	flex-direction: column;

	span,
	p {
		padding: 0 !important;
		line-height: 1em !important;
	}

	.row {
		padding: 0.3rem 0 !important;
	}

	.column p {
		margin: 0.3rem 0 !important;
	}
}

.flex-item-flex-end {
	display: flex;
	// justify-content: flex-end;
	align-items: flex-end;
	flex-direction: column;

	span {
		padding: 0 !important;
		line-height: 1em !important;
		margin: 0 0 0 1em !important;
	}

	p {
		padding: 0 !important;
		line-height: 1em !important;
		margin: 0 0 1em !important;
	}
}

.flex-item-single-right {
	display: flex;
	justify-content: flex-end;
}

.flex-item-single-left {
	display: flex;
	justify-content: flex-start;
}

.header-underlined-wrap {
	text-align: center;
	h2 {
		display: inline-block;
		margin: 1.4rem 0 !important;
		border-bottom: 2px solid darkgray !important;
		padding: 0 0 0.2em !important;
	}
}

.no-margin-table {
	margin: 0.5em 0;
}

.no-margin-heading {
	margin: 0 !important;
}

.centered-underlined-heading {
	margin: 0 0 3rem;
	text-align: center;
	h3 {
		font-weight: 700 !important;
	}

	h5 {
		font-size: 1.2em !important;
	}

	h3,
	h5 {
		margin: 0 0 0.5rem !important;
		span {
			padding: 0 0 0.2rem;
		}
		.underline {
			text-decoration: underline;
		}
	}

	.underline-large {
		text-decoration: underline;
		font-size: 1.35em !important;
	}
}

.section-header {
	text-align: center;

	p,
	h1,
	h2,
	h3,
	h4 {
		margin: 0 !important;
		font-weight: bold !important;
	}

	.bottom-margin {
		margin-bottom: 0.5em !important;
	}

	.top-margin {
		margin-top: 0.5em !important;
	}

	.top-margin-1 {
		margin-top: 1em !important;
	}

	.normal {
		font-weight: normal !important;
	}

	.end-section {
		margin-bottom: 1em !important;
	}

	.end-section-minimal {
		margin-bottom: 0.4em !important;
	}

	p.left-align {
		text-align: left !important;
	}

	.left-align {
		text-align: left !important;
	}

	.underline {
		text-decoration: underline;
	}

	.large {
		font-size: large;
	}

	.medium {
		font-size: medium;
	}

	.red {
		color: #e6130b !important;
	}
}

.certificate-ui-table.ui.table {
	border: 1px solid black;
	border-collapse: collapse;
	border-radius: 0;
	border-spacing: none;
	background: none;
	text-align: center;

	thead th,
	td {
		padding: 0.5em 0.3em;
		border-bottom: 1px solid black;
		border-right: 1px solid black;
		border-spacing: none;
		background: none;
	}
}

.ui.fixed.table th {
	overflow: visible !important;
}

.left-ui-table.ui.table {
	border: 1px solid black;
	border-collapse: collapse;
	border-radius: 0;
	border-spacing: none;
	background: none;
	text-align: left;

	thead th {
		text-align: center;
	}

	thead th,
	td {
		padding: 0.3em 0.3em;
		border-bottom: 1px solid black;
		border-right: 1px solid black;
		border-spacing: none;
		background: none;
	}
}

.certificate-borderless-table.ui.table {
	border-collapse: collapse;
	border: none;
	border-radius: 0;
	border-spacing: none;
	background: none;
	text-align: left;
	margin: 0;

	thead th {
		text-align: center;
		padding: 0.1em 0;
	}

	td {
		padding: 0.1em 0;
	}

	thead th:first-child {
		text-align: left;
	}

	thead th,
	td {
		border: none;
		border-spacing: none;
		background: none;
	}
}

span.indent-span {
	text-indent: 2em;
	display: inline-block;
}

.indent {
	text-indent: 2em;
}

.div-indent {
	margin-left: 2em;
}

.div-indent-one {
	margin-left: 1em;
}

.div-indent-three {
	margin-left: 3em;
}

.margin-para {
	margin-bottom: 1.5em;
}

.margin-section {
	margin-bottom: 2em;
}

.margin-top-class {
	margin-top: 0.5em;
}

.icon-only-button.ui.basic.negative.button {
	box-shadow: none !important;
}

.floor-select {
	display: inline-block !important;
	padding: 0 !important;
	border: none !important;
	width: auto !important;
	font-family: $fontFamily;
}

.compact-input {
	width: 90px !important;
}

.ui.form .error.field.table {
	text-align: center;
}

.ui.dropdown.dashedForm-control {
	border: none;
	border-bottom: 1px dashed rgba(34, 36, 38, 0.5);
	border-radius: 0;
	background: none;
}

.flex-div {
	display: flex;
	div:first-child {
		margin-right: 0.5em;
	}
}

.no-padding-grid.ui.grid > .row {
	padding: 1em 0 0.5em;
}

.zero-padding-grid.ui.grid {
	padding: 1em 0;
}

.zero-padding-grid.ui.grid > .row {
	padding: 0;
}

.change-password-segment {
	.error .field {
		margin-bottom: 0 !important;
	}
	.field > label {
		text-align: left;
		padding-top: 0 !important;
	}
}

.structure-ui-table.ui.table {
	border: 1px solid black;
	border-collapse: collapse;
	border-radius: 0;
	border-spacing: none;
	background: none;
	// text-align: center;

	thead th,
	td {
		padding: 0.5em 0.3em;
		border-bottom: 1px solid black;
		border-right: 1px solid black;
		border-spacing: none;
		background: none;
	}
}

span.tiny-dashed {
	border-bottom: 1px dashed black;
	min-width: 35px;
	margin: 0 0.8rem;
	line-height: 1.21428571em;
	padding: 0;
	display: inline-flex;
	color: rgba(0, 0, 0, 0.87);
}

span.small-dashed {
	border-bottom: 1px dashed black;
	min-width: 60px;
	margin: 0 0.8rem;
	line-height: 1.21428571em;
	padding: 0;
	display: inline-flex;
	color: rgba(0, 0, 0, 0.87);
}

span.normal-dashed {
	border-bottom: 1px dashed black;
	min-width: 100px;
	margin: 0 0.8rem;
	line-height: 1.21428571em;
	padding: 0;
	display: inline-flex;
	color: rgba(0, 0, 0, 0.87);
}

.land-details-form {
	align-items: flex-end !important;
	.field {
		margin-bottom: 1em !important;
	}
	.fields {
		width: auto !important;
	}
}

input.c-tiny {
	width: 50px !important;
}

input.c-small {
	width: 125px !important;
}

.form-section {
	@media screen and (max-width: 1000px) {
		max-width: 630px;
	}
	@media screen and (max-width: 1200px) and (min-width: 1000px) {
		max-width: 750px;
	}
	@media screen and (max-width: 1400px) and (min-width: 1200px) {
		max-width: 900px;
	}
	// width: calc((100% / 1) - 2px);
}
